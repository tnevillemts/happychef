<?php

/*
 * Crawl through a specified directory that holds a list of products that have to be reindexed.
 * During the inventory sync feed, some products get out of stock. The IDs of such products are added to
 * a serialized array and then saved into small files picked up by this cron.
 */

require_once '../app/Mage.php';
Mage::init();

$path = Mage::getBaseDir('base')."/var/to_reindex";

if ($handle = opendir($path)) {

    $dateTimeStamp = date('Y-m-d H:i:s');

    echo("[" . $dateTimeStamp . "] File: " . __FILE__ . " ---------- Start \n");

    try {

        // Loop through all new files in a specified directory.
        while (false !== ($filename = readdir($handle))) {

            // Ignore '.', '..', '.lock' and '.done' files.
            if($filename != "." && $filename != ".." && $filename != "empty" && strpos($filename, "lock") === FALSE && strpos($filename, "done") === FALSE) {

                $fullName = $path."/".$filename;
                echo("Process file: [" . $fullName . "]\n");

                $productIds = unserialize(file_get_contents($fullName));

                echo("Have product Ids.\n");

                // Lock the file
                $lockedName = $fullName.".lock";
                rename($fullName, $lockedName);

                echo("lockedName [" . $lockedName . "]\n");

                $productIds = array_unique($productIds);

                echo("About to reIndex selected.\n");

                // Reindex products, surpress output of this call.
                ob_start();

                Mage::getModel('thumbindexer/indexer_thumbindexer')->reindexSelected($productIds);

                ob_end_clean();

                echo("Generated new product indexes (vim and tim) for specific products. Product Ids: [" . implode(', ', $productIds) . "]\n");

                // Mark file as completed
                $completedName = str_replace("lock", "done", $lockedName);
                rename($lockedName, $completedName);
        	}
    	}
	} catch (Exception $e) {
        echo("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " e->getMessage() [" . $e->getMessage() . "]\n");
    }

	closedir($handle);

    $dateTimeStamp = date('Y-m-d H:i:s');

	echo("[" . $dateTimeStamp . "] ---------- End\n");
}


