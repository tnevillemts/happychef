<?php

require_once '../app/Mage.php';
Mage::init();

if (in_array($_SERVER["REMOTE_ADDR"],array("127.0.0.1", "75.146.246.189"))) {
    // die("Not authorized.");
}

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');

$query = "SELECT entity_id, increment_id, count(*) as order_count, customer_email, created_at
            FROM mag_sales_flat_order WHERE confirmation IS NULL GROUP BY customer_email HAVING order_count > 1";

$orders = $readConnection->fetchAll($query);
foreach($orders as $order){

    $q = "SELECT count(*) as c, mag_sales_flat_order_address.*, mag_sales_flat_order.increment_id, mag_sales_flat_order.created_at  FROM mag_sales_flat_order_address
            INNER JOIN mag_sales_flat_order ON mag_sales_flat_order.entity_id = mag_sales_flat_order_address.parent_id
            WHERE email = '{$order["customer_email"]}' AND address_type = 'billing' AND confirmation IS NULL
            GROUP BY street HAVING c != {$order["order_count"]}";

    $suspects = $readConnection->fetchAll($q);
    if(count($suspects)){
        echo "Found ".count($suspects)." different addresses for [{$order["customer_email"]}]:<br />";

        foreach($suspects as $suspect){
            echo "Order #{$suspect["increment_id"]} [from {$suspect["created_at"]}] bills to {$suspect["firstname"]} {$suspect["lastname"]} ({$suspect["city"]}, {$suspect["region"]}, <span style='color:red'>{$suspect["street"]}</span>). Other orders with this address: [{$suspect["c"]}]<br />";
        }
        echo "<br />";
    }
}


/*

$collection = Mage::getResourceModel('catalog/product_collection')
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('type_id', 'simple')
    ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
    ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
    );

foreach ($collection as $product){

    $p = Mage::getModel('catalog/product_type_configurable');
    $parentIds = $p->getParentIdsByChild($product->getId());
    $pid = $parentIds[0];
    //$parentProduct = Mage::getModel('catalog/product')->load($pid);

    if ($pid > 0) {
        echo "{$product->getSku()} is simple, not visible, enabled, but belongs to configurable #{$pid}<br>";
    } else {
        echo "<div style='color: red'>{$product->getSku()} is simple, not visible, enabled, NOT part of any configurable</div>";
    }

}

*/
