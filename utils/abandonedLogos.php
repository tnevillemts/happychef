<?php

require_once '../app/Mage.php';
Mage::init();


/*
 * Checks for new abandoned logos appeared
 */
//protected function _checkAbandonedLogos()
//{
//    if (self::DEBUG_MODE) {
//        AW_Followupemail_Model_Log::log("Checking abandoned logos");
//    }
$resource = Mage::getSingleton('core/resource');
$read = $resource->getConnection('core_read');

// GET ALL QUOTE ITEMS WITH OPTIONS
$query1 = "SELECT qio.item_id, qio.value FROM mag_sales_flat_quote_item_option qio WHERE qio.code = 'embroidery_options'";
$logoItems = $read->fetchAll($query1);

//    print_r($logoItems);

foreach ($logoItems as $logoItem) {
    $hasLogo = 0;
    // GET THE ITEM ID AND LOGO IMAGE FOR ALL QUOTE ITEMS WITH LOGOS
    foreach ($logoItem as $option => $val) {
        if ($option == 'item_id') {
            $logoItemId = $val;
        }
        if ($option == 'value') {
            $optString = $val;
            $optsArray = explode(",", $optString);

            // GET ALL QUOTES ITEMS WITH LOGOS
            foreach ($optsArray as $itemOpt) {
                $cleanItemOpt = str_replace('"', "", $itemOpt);
                if (strpos($cleanItemOpt, "imageOriginal") !== false) {
                    $imgPath = substr($cleanItemOpt, strpos($cleanItemOpt, "embroidery_uploads"));
                    //$filename = end(explode('/', $imgPath));
                    $hasLogo = 1;
                }
            }
        }
    }
    if ($hasLogo == 1) {
        // GET THE CUSTOMER ID ASSOCIATED WITH THE QUOTE ITEM
        $query2 = "SELECT q.customer_id, q.created_at FROM mag_sales_flat_quote_item qi " .
            "INNER JOIN mag_sales_flat_quote q ON q.entity_id = qi.quote_id " .
            "WHERE qi.item_id = " . $logoItemId;
        $customerWithLogoQuote = $read->fetchAll($query2);

        // CREATE ARRAY WITH CUSTOMER ID AND FILENAME
        if ($customerWithLogoQuote[0][customer_id] != NULL) {
            $quoteLogos[] = array($customerWithLogoQuote[0][customer_id], $customerWithLogoQuote[0][created_at], $imgPath);
        }
    }
}

// UNIQUIFY quoteLogos ARRAY
$quoteLogos = array_map("unserialize", array_unique(array_map("serialize", $quoteLogos)));

//    echo ("uniqueQuoteLogos: ");
//    print_r($quoteLogos);

// GET CUSTOMER ID'S AND FILENAMES FOR ALL ORDERED LOGOS
$query3 = "SELECT cl.customer_id, cl.logo FROM mag_embroidery_customer_logo cl";
$customerLogosOrdered = $read->fetchAll($query3);

foreach ($customerLogosOrdered as $customerLogoOrdered) {
    $imgPath = $customerLogoOrdered[logo];
    $orderLogos[] = array($customerLogoOrdered[customer_id], $imgPath);
}

//    echo ("orderLogos: ");
//    print_r($orderLogos);

function check_diff_multi($array1, $array2){
    $results = array();
    foreach($array1 as $key1 => $val1) {
        $match = 0;
        foreach($array2 as $key2 => $val2) {
            if($val1[0] == $val2[0] && $val1[2] == $val2[1]) {
                $match = 1;
            }
        }
        if ($match == 1) {
            continue;
        } else {
            $results[] = array_merge($val1);
        }
    }
    return $results;
}

$results = check_diff_multi($quoteLogos, $orderLogos);

// REPLACE CUSTOMER ID WITH EMAIL ADDRESS
foreach ($results as $result => $val) {
    if (!count($val)) {
        unset($results[$result]);
    } else {
        $query4 = "SELECT e.email FROM mag_customer_entity e " .
            "WHERE e.entity_id = " . $val[0];
        $rec = $read->fetchOne($query4);
        $results[$result][0] = $rec;
        $results[$result][2] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $results[$result][2];
    }
}

// FORMAT OUTPUT
foreach ($results as $result => $val) {
//    if (file_exists($val[2])) {
    $fileoutput = "";
    if (file($val[2]) !== false && $val[0] != "") {

        foreach ($val as $key => $entry) {
            if ($key < count($val) - 1) {
                $fileoutput = $fileoutput . $entry . ", ";
            } else {
                $fileoutput = $fileoutput . $entry . "\n";
            }
        }
    }
    file_put_contents('abandoned_logos.txt', $fileoutput, FILE_APPEND);
//    else {
//        unset($results[$result]);
//    }
}

//if (!count($results)) {
//    return;
//} else {
////        file_put_contents('abandoned_logos.txt', $fileoutput);
//    echo $fileoutput;
//}

/******************************************************************************/

//        $queue = Mage::getResourceModel('followupemail/queue');
//        foreach ($logos as $logo) {
//            $queue->cancelByEvent(
//                $logo['customer_email'], AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_ABANDONED_LOGO_NEW,
//                $logo['quote_id']
//            );
//        }
//
//        $ruleIds = Mage::getModel('followupemail/mysql4_rule')->getRuleIdsByEventType(
//            AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_ABANDONED_LOGO_NEW
//        );
//
//        if (!count($ruleIds)) {
//            return;
//        }
//
//        $product = Mage::getModel('catalog/product');
//
//        $select = $read->select()
//            ->distinct()
//            ->from($resource->getTableName('sales/quote_item_option'), 'value')
//            ->where('code="product_type"');
//
//        foreach ($results as $result) {
//            $params = array();
//            $params['customer_id'] = $result['customer_id'];
//            $params['customer_email'] = $result['customer_email'];
//            $params['logo_path'] =
//
//            foreach ($ruleIds as $ruleId) {
//                $queueCollection = Mage::getModel('followupemail/queue')->getCollection();
//                $queueCollection->getSelect()->where('object_id = ?', $cart['quote_id']);
//                $queueCollection->getSelect()->where('rule_id = ?', $ruleId);
//
//                if (sizeof($queueCollection) > 0) {
//                    $isDublicated = false;
//                    foreach ($queueCollection->getItems() as $email) {
//                        $emailParams = $email->getParams();
//
//                        $arrayDiff = !array_diff($emailParams['product_ids'], $params['product_ids'])
//                            && !array_diff($params['product_ids'], $emailParams['product_ids']);
//
//                        if ($arrayDiff) {
//                            $isDublicated = true;
//                        }
//                    }
//                    if ($isDublicated) {
//                        AW_Followupemail_Model_Log::log(
//                            'dublicated carts abandoned quoteId=' . $cart['quote_id'] . ' , ruleId=' . $ruleId
//                        );
//                        continue;
//                    }
//                }
//                AW_Followupemail_Model_Log::log(
//                    'carts abandoned quoteId=' . $cart['quote_id'] . ' validating, ruleId=' . $ruleId
//                );
//                $objects = array();
//                $objects['customer_firstname'] = $cart['customer_firstname'];
//                $objects['customer_middlename'] = $cart['customer_middlename'];
//                $objects['customer_lastname'] = $cart['customer_lastname'];
//                $objects['updated_at'] = $cart['updated_at'];
//                $objects['customer_is_guest'] = (int)!$cart['customer_id'];
//
//                Mage::getModel('followupemail/rule')->load($ruleId)->process($params, $objects);
//            }
//        }


//}