<?php

require_once '../app/Mage.php';
Mage::init();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
Mage::getModel('colormapping/observer')->adjustColorgroupAttributes();
