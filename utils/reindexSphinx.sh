#!/bin/bash
###########################################################
#
# 	Stop Sphinx process, regenerate indexes, start Sphinx
#
###########################################################

cd $(dirname "${BASH_SOURCE[0]}")

NOW=$(date +"%m-%d-%Y %T")

echo "-----------------------------"
echo "Script started on $NOW"
echo "-----------------------------"

WEBROOT="$(dirname "$(pwd)")"

if [ ! -d "$WEBROOT" ]; then
  echo "Error: directory $WEBROOT doesn't exist."
  exit
fi

# Reindex
indexer --config $WEBROOT/sphinx/etc/sphinx.conf --rotate --all


#####################################
#
# No need to stop and start sphinx. reindexing with --rotate option restarts the daemon
#
#####################################

# Stop Sphinx
#searchd --stop --config $WEBROOT/sphinx/etc/sphinx.conf

# Start Sphinx
#searchd --config $WEBROOT/sphinx/etc/sphinx.conf
