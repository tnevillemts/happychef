<?php

require_once '../app/Mage.php';
Mage::init();

$products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
$products->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
$products->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
$products->addAttributeToFilter('type_id', array('eq' => 'configurable'));

$i = 0;
$productList = "";

foreach($products as $product) {

    $ids = Mage::getResourceSingleton('catalog/product_type_configurable')
        ->getChildrenIds($product->getId());

    $_subproducts = Mage::getModel('catalog/product')
        ->getCollection()
        ->addIdFilter ($ids)
        ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
        ;

    if (!count($_subproducts)){
        $i++;
        $productList .= "{$i}. <a href='".Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit', array('id' => $product->getId()))."' target='_blank'>{$product->getSku()}</a> - {$product->getName()}<br/>";
    }
}

if($productList){

    $body = "Hello there!<br><br>The following configurable products don't have children assigned or all children are disabled:<br><br>";
    $body .= $productList;
    $body .= "<br>--<br>The Eye of Sauron<br>(aka Ripen Dev Team)";

    $emailTo = "merchandising@ri.pn";

    $mail = Mage::getModel('core/email');
    $mail->setToName('Ripen');
    $mail->setToEmail($emailTo);
    $mail->setBody($body);
    $mail->setSubject('Configurable products missing children!');
    $mail->setFromEmail('dev@ri.pn');
    $mail->setFromName("Ripen Monitoring");
    $mail->setType('html');
    try {
        $mail->send();
        Mage::log("Email sent to [{$emailTo}] reporting [{$i}] products with missing children", null, 'parentsWithNoChildren.log');

    }
    catch (Exception $e) {
        Mage::log("Error sending email to [{$emailTo}]: {$e->getMessage()}", null, 'parentsWithNoChildren.log');
    }
}


