<?php

require_once '../app/Mage.php';
Mage::init();


//<crontab>
//        <jobs>
//            <catalog_product_alert>
//				<schedule>
//                    <cron_expr>*/10 * * * *</cron_expr>
//                </schedule>
//                <run>
//                    <model>productalert/observer::process</model>
//                </run>
//            </catalog_product_alert>

//First we load the model

$model = Mage::getModel('productalert/observer');
$model->process();

