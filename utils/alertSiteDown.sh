#!/bin/bash

###########################################################
#
# 	Check if the home page loads and renders expected content
#
###########################################################

NOW=$(date +"%m-%d-%Y %T")
TIMEOUT=30
EMAIL="mmedvedev@ri.pn"
HOMEPAGE="happychef.local"
PATTERN="HC-footer-logo.png";
SUBJECT="HappyChef site is down - $NOW"

/usr/bin/wget $HOMEPAGE --timeout $TIMEOUT -O - 2>/dev/null | grep $PATTERN || /usr/bin/wget $HOMEPAGE --timeout $TIMEOUT -O - 2>/dev/null | /usr/bin/mail -v -s "$SUBJECT" $EMAIL

