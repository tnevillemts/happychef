<?php
if($_GET["lollipop"]){

    require_once '../app/Mage.php';
    Mage::init();

    // Rename logos
    /*
    $directory = 'logos/';
    if ($handle = opendir($directory)) {
        while (false !== ($fileName = readdir($handle))) {

            $a = explode(".", $fileName);
            $newName = md5($a[0]).".jpg";

            echo $fileName." -> " .$newName."<br/>";
            rename($directory.$fileName, $directory.$newName);
        }
        closedir($handle);
    }
    */

    /*
     * Loop through recs
     * Find customer id by order id
     * create record in logos
     */


    $ordersCount = 0;
    $ordersNoCustomerCount = 0;

    if (($handle = fopen("logos.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $num = count($data);
            $incrementId = $data[0];
            $logoSku = $data[1];
            $stitchCount = $data[2];
            $fileName = $data[3];

            $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
            if ($order->getId()){
                $ordersCount++;
                //echo "Found order #{$incrementId}<br/>";

                $websiteId = Mage::app()->getWebsite()->getId();
                $store = Mage::app()->getStore();
                $customerId = $order->getCustomerId();
                $orderCustomer = Mage::getModel('customer/customer')
                    ->setWebsiteId($websiteId)
                    ->loadByEmail($order->getCustomerEmail());


                if(!$customerId && !$orderCustomer->getId()){

                    $customer = Mage::getModel("customer/customer");
                    $customer   ->setWebsiteId($websiteId)
                        ->setStore($store)
                        ->setFirstname($order->getCustomerFirstname())
                        ->setLastname($order->getCustomerLastname())
                        ->setEmail($order->getCustomerEmail())
                        ->setPassword('123456');

                    try {
                        $customer->save();
                        $customerId = $customer->getId();
                        echo "New customer created: {$customer->getEmail()}[{$customer->getId()}]<br/>";
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }

                } else if (!$customerId && $orderCustomer->getId()){
                    $customerId = $orderCustomer->getId();
                    echo "Found existing customer: {$orderCustomer->getEmail()}[{$orderCustomer->getId()}]<br/>";
                }

                if($customerId){

                    $a = explode(".", $fileName);
                    $newName = "embroidery_uploads/2017-05-18/".md5($a[0]).".jpg";

                    $data = array(
                        'customer_id' => $customerId,
                        'name' => trim(str_replace("LOGO-", "", $logoSku)),
                        'logo' => $newName,
                        'digitized_logo' => $newName,
                        'sku' => trim($logoSku),
                        'stitches_count' => $stitchCount,
                        'status' => 1
                    );

                    $logoCollection = Mage::getModel('embroidery/logo')->getCollection()
                        ->addFieldToFilter('sku', trim($logoSku))
                        ->addFieldToFilter('customer_id', $customerId)
                    ;

                    if($logoCollection->getSize()){
                        echo "Logo {$newName} has been already saved. Skipping...<br/>";
                    } else {
                        $logo = Mage::getModel('embroidery/logo');
                        try {
                            $logo->setData($data)->save();
                            echo "Logo {$newName} saved for customer {$customerId}<br/>";
                        } catch (Exception $e) {
                            echo $e->getMessage()."<br/>";
                        }
                    }

                } else {
                    $ordersNoCustomerCount++;
                    echo "Customer account not found for order #{$incrementId}<br/>";
                }
            } else {
                //echo "NOT Found order #{$incrementId}<br/>";
            }

        }

        //echo "Orders: ".$ordersCount;
        echo "Orders without customer account: ".$ordersNoCustomerCount;

        fclose($handle);
    }



}



