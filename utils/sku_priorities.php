<?php

require_once '../app/Mage.php';
Mage::init();

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=skus.csv');

$products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
$products->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
$products->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);

$out = fopen('php://output', 'w');

$fields = array("Sku", "Priority", "Name", "Reviews");
fputcsv($out, $fields);

foreach($products as $product) {
    $summaryData = Mage::getModel('review/review_summary')->setStoreId($storeId)->load($product->getId());
    $reviewsCount = $summaryData["reviews_count"] ? $summaryData["reviews_count"] : 0;
    $fields = array($product->getSku(), $product->getReviewPriority(), $product->getName(), $reviewsCount);
    fputcsv($out, $fields);
}
fclose($out);

