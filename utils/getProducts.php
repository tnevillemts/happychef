<?php

define('__ROOT__', dirname(dirname(__FILE__)));
require_once("/var/www/html/happychef-beta/app/Mage.php");

Mage::app('admin');

try {
        Mage::init();
        $magentoVersion = Mage::getVersion();
        echo("\nMagento Version [" . $magentoVersion . "].\n");

        echo("\nStart of script.\n");

 	$configurableCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
	    ->addAttributeToFilter('status', array('eq' => 1))
            ->addAttributeToFilter('type_id','configurable'); 

	$configurableCount = 0;
        if($configurableCollection->count() > 0) {
                foreach($configurableCollection as $configurable) {

			$configurableId   = $configurable->getId();
			$configurableSku  = $configurable->getSku();
			$configurableName = $configurable->getName();

			$cStr = "\nConfigurable product Id [" . $configurableId . "], Sku [" . $configurableSku . "], Name [" . $configurableName . "]\n";

			$configurableAttributes = $configurable->getTypeInstance(true)->getConfigurableAttributes($configurable);
			$configurablesCount     = count($configurableAttributes);

    			foreach($configurableAttributes as $configurableAttribute){
				$cStr .= "\tAttribute code [" . $configurableAttribute->getProductAttribute()->getAttributeCode() . "]\n";
    			}

			$sStr = '';
			$simplesCollection = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $configurable);
			foreach($simplesCollection as $simple) {

				$simpleId   = $simple->getId();
				$simpleSku  = $simple->getSku();
				$simpleName = $simple->getName();

				$simpleAttributeValues = array();
				foreach($configurableAttributes as $configurableAttribute) {	

					$configurableAttributeCode = $configurableAttribute->getProductAttribute()->getAttributeCode();
    					$simpleAttributeText       = $simple->getAttributeText($configurableAttributeCode);

					if(! empty($simpleAttributeText)) {
						$simpleAttributeValues [] = $simpleAttributeText;
					}
				}
				$simpleAttributeOptionsValuesStr = implode(',', $simpleAttributeValues);
				if(count($simpleAttributeValues) == $configurablesCount) {
					//$sStr .= "\t\tSimple product Id [" . $simpleId . "], Sku [" . $simpleSku . "] Complete [" . $simpleAttributeOptionsValuesStr . "]\n";
				} else {
					$sStr .= "\t\tSimple product Id [" . $simpleId . "], Sku [" . $simpleSku . "], Values [" . $simpleAttributeOptionsValuesStr . "]\n";
				}
			}
			if(! empty($sStr)) {
				echo($cStr . $sStr);
			}

                        $configurableCount++;
                }
        } else {
                echo("\n No data.");
        }

        echo("\n\nTotal configurableCount [" . $configurableCount . "]");

        echo("\nEnd of script.");
} catch(Exception $e) {
        echo("\n\t\t" . __FILE__ . " " . __FUNCTION__ . " Exception caught [" . $e->getMessage() . "].");
}

exit;

?>

