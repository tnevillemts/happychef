#!/bin/bash
###########################################################
#
#
#    Crawl through a specified directory that holds a list of products that have to be reindexed.
#    During the inventory sync feed, some products get out of stock. The IDs of such products are added to
#    a serialized array and then saved into small files picked up by this cron.
#
###########################################################

cd $(dirname "${BASH_SOURCE[0]}")

nohup /usr/bin/php reindexProducts.php >> ../var/log/reindex.log &
