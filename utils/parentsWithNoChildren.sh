#!/bin/bash
###########################################################
#
#  Generate a list of configurable product that don't have
#  assigned simple products or all assigned products are disabled
#
###########################################################

cd $(dirname "${BASH_SOURCE[0]}")

nohup /usr/bin/php parentsWithNoChildren.php >/dev/null 2>&1 &
