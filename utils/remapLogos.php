<?php

require_once '../app/Mage.php';
Mage::init();

$logos = Mage::getModel('embroidery/logo')->getCollection();
foreach( $logos as $logo ) {
    $newLogoArray = array();
    if($logo->getDigitizedLogo()){
        $newLogoArray[$logo->getSku()] = array(
            "stitches_count"    => $logo->getStitchesCount(),
            "status"            => $logo->getStatus(),
            "last_modified"     => $logo->getLastModified(),
            "digitized_logo"    => $logo->getDigitizedLogo(),
            'category_id'        => 0
        );

        $logo->setDigitizedLogos(json_encode($newLogoArray));
        $logo->save();
    }
}
