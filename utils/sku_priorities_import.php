<?php

require_once '../app/Mage.php';
Mage::init();
?>
    <table width="600">
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">

            <tr>
                <td width="20%">Select file</td>
                <td width="80%"><input type="file" name="file" id="file" /></td>
            </tr>

            <tr>
                <td>Submit</td>
                <td><input type="submit" name="submit" /></td>
            </tr>

        </form>
    </table>

<?php

if ( isset($_POST["submit"]) ) {

    if ( isset($_FILES["file"])) {

        //if there was an error uploading the file
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";

        }
        else {
            //Print file details
            echo "Upload: " . $_FILES["file"]["name"] . "<br />";
            echo "Type: " . $_FILES["file"]["type"] . "<br />";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";

            $csv = array_map('str_getcsv', file($_FILES["file"]["tmp_name"]));
            array_walk($csv, function(&$a) use ($csv) {
                $a = array_combine($csv[0], $a);
            });
            array_shift($csv);


            foreach($csv as $record){
                $product = Mage::getModel('catalog/product')->load($record['id']);
                if($product) {
                    $product->setReviewPriority($record['priority']);
                    $product->getResource()->saveAttribute($product, 'review_priority');
                    echo "Product [{$record['sku']}] - [{$product->getId()}] saved<br />";
                } else {
                    echo "Product [{$record['sku']}] - [{$product->getId()}] NOT saved<br />";
                }

            }


        }
    } else {
        echo "No file selected <br />";
    }
}

/*
$products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
$products->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
$products->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
$out = fopen('php://output', 'w');

$fields = array("Sku", "Priority", "Name", "Reviews");
fputcsv($out, $fields);

foreach($products as $product) {
    $summaryData = Mage::getModel('review/review_summary')->setStoreId($storeId)->load($product->getId());
    $reviewsCount = $summaryData["reviews_count"] ? $summaryData["reviews_count"] : 0;
    $fields = array($product->getSku(), $product->getReviewPriority(), $product->getName(), $reviewsCount);
    fputcsv($out, $fields);
}
fclose($out);
*/