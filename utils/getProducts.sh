#!/bin/bash
#############################################
#
#       getProducts.sh
#
#############################################

cd $(dirname "${BASH_SOURCE[0]}")

/usr/bin/php ./getProducts.php > /var/data/getProducts.out;

if [ -f /var/data/getProducts.out ]; then
 mail -s "Product attribute info." drichter@ri.pn < /var/data/getProducts.out;
 mail -s "Product attribute info." merchandising@ri.pn < /var/data/getProducts.out;
fi
