<?php

require_once("../../app/Mage.php");
const BUFFERSIZE = 16384;

Mage::app();

function importGiftCards() {

	try {

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');

		$data = array();

		$currentRow = 0;
        $log = "";

		$fileHandle = fopen("gc_samples.csv", "r");
		while(($line = fgetcsv($fileHandle, BUFFERSIZE, ",")) !== FALSE ) {

            $numberOfColumns = count($line);
			if($currentRow < 1) {
				$headings = $line;
			} else {

                for($i = 0; $i < $numberOfColumns; $i++) {
                    $data[$headings[$i]] = $line[$i];
                }

                $sql = sprintf("SELECT count(*) AS num_result FROM " . Mage::getConfig()->getTablePrefix() . "aw_giftcard
                WHERE code = '%s'", $data["GiftCertID"]);

                $numResult = $read->fetchOne($sql);

                if( $data["GiftCertID"] && $data["Valid"] && !$numResult) {

                    $sql = "INSERT INTO " . Mage::getConfig()->getTablePrefix() . "aw_giftcard SET
                            code = :code,
                            status = 1,
                            created_at = :created_at,
                            website_id = :store_id,
                            balance = :balance,
                            state = 1;
                        ";

                    $binds = array(
                        'code' => $data["GiftCertID"],
                        'created_at' => $data["Date"],
                        'store_id' => Mage::app()->getWebsite()->getId(),
                        'balance' => $data["Amount"]
                    );


                    if($write->query($sql, $binds)) {
                        $log .= "Certificate #{$data["GiftCertID"]} successfully inserted.<br>\n";
                    } else {
                        $log .= "Error inserting certificate #{$data["GiftCertID"]}<br>\n";
                    }

                } else {

                    if (!$data["GiftCertID"] || !$data["Valid"])
                        $reason = "Invalid GC number or GC status";
                    if ($numResult)
                        $reason = "Duplicate";

                    $log .= "Certificate #{$data["GiftCertID"]} was NOT inserted ({$reason})<br>\n";
                }

			}


			$currentRow++;
		}

		fclose($fileHandle);
        echo $log;

	} catch(Exception $e) {
		echo("Exception caught [" . $e->getMessage() . "].\n");
	}
}

importGiftCards();
