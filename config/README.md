# Config Directory

This directory is used in conjunction with the `Zookal/HarrisStreet-ImpEx` magerun addon module to track non-sensitive environment-specific configuration values.

It is also currently used by the Ripen `importdata` and `exportdata` scripts to sync shared configuration values from production to dev and staging systems. (These will import the base configuration into the otherwise empty `base/` subdirectory.)
