<?php
    require_once ( "app/Mage.php" ); //Include Magento application
    umask(0); 
    Mage::app("default");
    
    $resource = Mage::getSingleton('core/resource');
    $externaldb_read = $resource->getConnection('adtrack_read');
    $externaldb_write = $resource->getConnection('adtrack_write');
    $externalDb = (string)Mage::getConfig()->getNode('global/resources/adtrack_database/connection/dbname');
    
    $sql1 = sprintf("ALTER TABLE " . Mage::getConfig()->getTablePrefix() . "adtrack_log ADD redirect_id INT( 11 ) UNSIGNED NULL DEFAULT NULL AFTER adid_id;
                    ALTER TABLE " . Mage::getConfig()->getTablePrefix() . "adtrack_log ADD INDEX ( `redirect_id` ) ;");
    $sql2 = sprintf("ALTER TABLE " . Mage::getConfig()->getTablePrefix() . "adtrack_log ADD FOREIGN KEY (`redirect_id`) REFERENCES %s." . Mage::getConfig()->getTablePrefix() . "adtrack_redirect(`redirect_id`) 
                        ON DELETE CASCADE ON UPDATE CASCADE;",$externalDb);
    try {
        $externaldb_write->query($sql1);
        $externaldb_write->query($sql2);
        echo "redirect_id column is created successfully";
    } catch(Exception $e) {
        echo "redirect_id column is not created successfully <br>";
        echo $e->getMessage();
    }
?>