var $j = jQuery.noConflict(); // //
$j(document).ready(function () {

// CONFIRM SAVE CLICK ################################################################## //

    var addToUrl = "preloadedimg";
    var preloadedImg = "/no";
    var theAction = $j("#product_addtocart_form").attr("action");
    $j("#product_addtocart_form").attr("action", theAction + addToUrl + preloadedImg);

    // Add To Cart button - form submission
    $j(".button.btn-cartf").click(function (e) {
        try {
            // Facebook Add To Cart Dynamic Element
            if (typeof fbPixelManager !== "undefined") {
                fbPixelManager.firePixel("AddToCart");
            }
        } catch (e) {
            // Non-fatal
        }

        // if ($j('h2.selected-menu').hasClass('has-embroidery')) {
        //     /*-- GOOGLE ANALYTICS --------------------------------*/
        //     _gaq.push(['_trackEvent', 'customization', 'added-to-cart']);
        //     /*-- GOOGLE ANALYTICS --------------------------------*/
        // }


        var errorMessage = "";
        var err = false;

        if (($j("#aw-gc-amount").val() == "")) {
            errorMessage = errorMessage + "Please select an amount\n";
            err = true;
        }
        if (($j("#attribute92").val() == "")) {
            errorMessage = errorMessage + "Please select color\n";
            err = true;
        }  
        if($j("#attribute213").length) {
            if ($j("#attribute213").val() == "") {
                errorMessage = errorMessage + "Please select size\n";
                err = true;
            }  
        }
        if($j("#attribute267").length) {
            if ($j("#attribute267").val() == "") {
                errorMessage = errorMessage + "Please select waist\n";
                err = true;
            }  
        }
        if($j("#attribute220").length) {
            if ($j("#attribute220").val() == "") {
                errorMessage = errorMessage + "Please select length\n";
                err = true;
            }
        }
        if($j("#attribute386").length) {
            if ($j("#attribute386").val() == "") {
                errorMessage = errorMessage + "Please select sleeve length\n";
                err = true;
            }
        }
        // SHOE SIZE
        if ($j("#attribute380").val() == "") {
            errorMessage = errorMessage + "Please select size\n";
            err = true;
        }
        // SHOE GENDER
        if ($j("#attribute381").val() == "") {
            errorMessage = errorMessage + "Please select gender\n";
            err = true;
        }
        if (!/^\d+$/.test($j("#qty").val()) || $j("#qty").val() == 0) {
            errorMessage = errorMessage + "Please specify valid quantity\n";
            err = true;
        }
        if (err) {
            $j(document).trigger('add-to-cart-alert');
            alert(errorMessage);
            event.preventDefault();
            return false;
        } else {
            if (document.location.href.match(/\bgiftcard\b/g)) {
                var productAddToCartForm = new VarienForm('product_addtocart_form');
                if (productAddToCartForm.validator.validate()) {
                    $j("#product_addtocart_form").submit();
                }
            } else {
                $j("#product_addtocart_form").submit();
            }
        }

    });


}); // END DOC READY
