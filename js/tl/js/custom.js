var $j = jQuery.noConflict();

$j(document).ready(function(){
    $j("#product_info_tabs_customer_options span").html("Embroidery Options");
    $j(".adminhtml-catalog-product-edit .side-col").show();
    $j(".opt-req .select").each(function(){
        $j(this).val("0");
    });
    $j("button.save").on("click", function (){
        $j(".opt-req .select").each(function(){
            $j(this).val("0");
        });
    });
    $j(".opt-req .select").each(function(){
        $j(this).val("0");
    });
    if ($j(".tl-customoptions-options-edit")[0]){
        $j("#add_new_defined_option").on("click", function() {
            var titlevalue = $j("#title").val();
            titlevalue = titlevalue.toLowerCase().replace(/[^a-zA-Z0-9]/g,'-');
            titlevalue = titlevalue.replace(/--/g, '-');
            titlevalue = titlevalue.replace(/1/g, 'one');
            titlevalue = titlevalue.replace(/2/g, 'two');
            titlevalue = titlevalue.replace(/3/g, 'three');
            titlevalue = titlevalue.replace(/4/g, 'four');
            titlevalue = titlevalue.replace(/5/g, 'five');
            titlevalue = titlevalue.replace(/6/g, 'six');
            titlevalue = titlevalue.replace(/7/g, 'seven');
            titlevalue = titlevalue.replace(/8/g, 'eight');
            titlevalue = titlevalue.replace(/9/g, 'nine');
            titlevalue = titlevalue.replace(/0/g, 'zero');
            $j("#product_options_container .option-box").eq(0).find(".css_class").val(titlevalue);
            $j(".opt-req .select").each(function(){
                $j(this).val("0");
            });
        });
        $j("#title").on("keyup", function() {
            value = $j(this).val();
            value = value.toLowerCase().replace(/[^a-zA-Z0-9]/g,'-');
            value = value.replace(/--/g, '-');
            value = value.replace(/1/g, 'one');
            value = value.replace(/2/g, 'two');
            value = value.replace(/3/g, 'three');
            value = value.replace(/4/g, 'four');
            value = value.replace(/5/g, 'five');
            value = value.replace(/6/g, 'six');
            value = value.replace(/7/g, 'seven');
            value = value.replace(/8/g, 'eight');
            value = value.replace(/9/g, 'nine');
            value = value.replace(/0/g, 'zero');
            
            
            
           
            
             $j(".css_class").each(function(){
                
				var idval = $j(this).attr('id');
            
				var res = idval.split("_"); 
            
				var assignTo = $j("#product_option_"+res[2]+"_div_group").val();
            
				var mainV = value+" "+assignTo;
                      
                $j(this).val(mainV);
                
            });
            
            $j(".opt-req .select").each(function(){
                $j(this).val("0");
            });
        });
        $j("#add_new_defined_option").on("click", function() {
            $j("#product_options_container .option-box").eq(0).show();
        });
        if($j('h3.head-customoptions-options:contains("Edit")').length > 0) {
            $j(".content-header .save").remove();
            $j(".delete-product-option").remove();
            $j("#add_new_defined_option").remove();
            $j('#product_options_container input[type=text]').attr("disabled",true);
            $j('#product_options_container select').prop('disabled', 'disabled');
        }
    }
    $j(".content-header").show();

    $j(".opt-req .select").each(function(){
        $j(this).val("0");
    });

    $j(document).on("click","#show_embroidery_option_button", function(){
        $j('.select-product-option-type').each(function(){
            var selected = $j(':selected', this);
            if ($j(this).val() == "field" ) {
                $j(this).closest(".option-header").find(".group-value").show();
                $j(this).closest(".option-header").find(".opt-desc").show();
                $j(this).closest(".option-header").find(".opt-sort-order").show();
                $j(this).closest(".option-header").find(".group-value").children("input").addClass("required-entry");
            } else if ($j(this).val() == "drop_down" ) {
                $j(this).closest(".option-header").find(".group-value").show();
                $j(this).closest(".option-header").find(".opt-desc").show();
                $j(this).closest(".option-header").find(".opt-sort-order").show();
                $j(this).closest(".option-header").find(".group-value").children("input").addClass("required-entry");
            } else if ($j(this).val() == "file" ) {
                $j(this).closest(".option-header").find(".opt-desc").hide();
                $j(this).closest(".option-header").find(".group-value").hide();
                $j(this).closest(".option-header").find(".opt-sort-order").show();
                $j(this).closest(".option-header").find(".option-title").attr("value","Art").attr("readonly",true);
            } else if ($j(this).val() == "swatch" && selected.closest('optgroup').attr('label') == "Image Upload") {
                $j(this).closest(".option-header").find(".opt-desc").hide();
                $j(this).closest(".option-header").find(".group-value").hide();
                $j(this).closest(".option-header").find(".opt-sort-order").hide();
                $j(this).closest(".option-header").find(".option-title").attr("value","Choose Art").attr("readonly",true);
                $j(this).closest(".option-header").find(".group-value").children("input").removeClass("required-entry")
                $j(this).closest(".option-header").find("select.select-product-option-type option:contains('Choose your Art')").prop('selected', true);
            } else {
                $j(this).closest(".option-header").find(".group-value").hide();
                $j(this).closest(".option-header").find(".opt-desc").show();
                $j(this).closest(".option-header").find(".opt-sort-order").show();
                $j(this).closest(".option-header").find(".group-value").children("input").removeClass("required-entry");
            } if ($j(this).closest(".option-header").find(".option-title").attr("value")=="Choose Art") {
                $j(this).closest(".option-header").find("select.select-product-option-type option:contains('Choose your Art')").prop('selected', true);
            } if ($j(this).closest(".option-header").find(".option-title").attr("value")=="Choose Art") {
                $j(this).closest(".option-header").find("select.select-product-option-type option:contains('Choose your Art')").prop('selected', true);
            }
        });
        if ($j(".tl-customoptions-options-edit")[0]){
            $j(".css-class-value input").each(function(){
                var theValue = $j(this).val();
                var last = theValue.split(" ").pop();
                var leng = theValue.split(' ');
                var theLeng = leng.length;
                if (theLeng == 2) {
                    $j(this).parent("td").prev("td").children("input").val(last);
                }
            });
            if ($j(".option-box")[0]){
                $j(".option-box").show();
            } else {
                alert("You need to first create the Embroidery Options using the button on the right.");
                $j(".option-box").show();
            }
        } else {
            $j(".css-class-value input").each(function(){
                var theValue = $j(this).val();
                var last = theValue.split(" ").pop();
                var leng = theValue.split(' ');
                var theLeng = leng.length;
                if (theLeng == 2) {
                    $j(this).parent("td").prev("td").children("input").val(last);
                }
            });
            if ($j(".option-box")[0]){
                $j(".option-box").show();
            } else {
                alert("You need to first create an Embroidery Template and associate it with the product using the select box in this tab.")
            }
        }
    });
    $j(document).on('keyup','.group_value', function() {
        var nextValue = $j(this).parent("td").next("td").children("input").val();
        var words = nextValue.split(" ");
        var titleValue = words[0];
        var thisValue = $j(this).val();
        $j(this).parent("td").next("td").children("input").val(titleValue+' '+thisValue);
    });
    $j(document).on('change','.select-product-option-type',function(){
        var selected = $j(':selected', this);
        if ($j(this).val() == "field" ) {
            $j(this).closest(".option-header").find(".group-value").show();
            $j(this).closest(".option-header").find(".opt-desc").show();
            $j(this).closest(".option-header").find(".opt-sort-order").show();
            $j(this).closest(".option-header").find(".group-value").children("input").addClass("required-entry");
            $j(this).closest(".option-header").find(".option-title").attr("value","").attr("readonly",false);
        } else if ($j(this).val() == "drop_down" ) {
            $j(this).closest(".option-header").find(".group-value").show();
            $j(this).closest(".option-header").find(".opt-desc").show();
            $j(this).closest(".option-header").find(".opt-sort-order").show();
            $j(this).closest(".option-header").find(".group-value").children("input").addClass("required-entry");
            $j(this).closest(".option-header").find(".option-title").attr("value","").attr("readonly",false);
        } else if ($j(this).val() == "file" ) {
            $j(this).closest(".option-header").find(".opt-desc").hide();
            $j(this).closest(".option-header").find(".group-value").hide();
            $j(this).closest(".option-header").find(".opt-sort-order").show();
            $j(this).closest(".option-header").find(".option-title").attr("value","Art").attr("readonly",true);
            $j(this).closest(".option-header").find(".group-value").children("input").removeClass("required-entry")
        } else if ($j(this).val() == "swatch" && selected.closest('optgroup').attr('label') == "Image Upload") {
            $j(this).closest(".option-header").find(".opt-desc").hide();
            $j(this).closest(".option-header").find(".group-value").hide();
            $j(this).closest(".option-header").find(".opt-sort-order").hide();
            $j(this).closest(".option-header").find(".option-title").attr("value","Choose Art").attr("readonly",true);
            $j(this).closest(".option-header").find(".group-value").children("input").removeClass("required-entry")
        } else {
            $j(this).closest(".option-header").find(".group-value").hide();
            $j(this).closest(".option-header").find(".opt-desc").show();
            $j(this).closest(".option-header").find(".opt-sort-order").show();
            $j(this).closest(".option-header").find(".group-value").children("input").removeClass("required-entry");
            $j(this).closest(".option-header").find(".option-title").attr("value","").attr("readonly",false);
        }
    });
});
