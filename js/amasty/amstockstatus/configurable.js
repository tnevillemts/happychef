/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2012 Amasty (http://www.amasty.com)
* @package Amasty_Stockstatus
*/

currentProductId = 0;
defaultProductId = 0;

StockStatus = Class.create();

StockStatus.prototype = 
{
    options : null,
    configurableStatus : null,
    
    initialize : function(options)
    {
        this.options = options;
    },

    onConfigure : function(key, settings)
    {
        if ('undefined' != typeof(this.options[key])) {
            $$('.add-to-cart').each(function(elem) {
                elem.show();
            });

            if (this.options[key]['product_id']) {
                currentProductId = this.options[key]['product_id'];
            } else {
                currentProductId = 0;
            }
        }

        keyParts = explode(',', key);
        if ("" == keyParts[keyParts.length-1]) { // this means we have something like "28," - the last element is empty - config is not finished
            needConcat  = true;
            selectIndex = keyParts.length-1;
        } else {
            needConcat  = false;
            selectIndex = keyParts.length;
        }
    },
};

Product.Config.prototype.configure = function(event){
    var element = Event.element(event);
    this.configureElement(element);
    var key = '';
    this.settings.each(function(select){
        // will check if we need to reload product information when the first attribute selected
        if (!select.value && 'undefined' != typeof(confData) && confData.oneAttributeReload && "undefined" != select.options[1] && !confData.isResetButton)
        {
            // if option is not selected, and setting is set to "Yes", will consider it as if the first attribute was selected (0 - is "Choose ...")
            key += select.options[1].value + ',';
        } else
        {
            key += select.value + ',';
        }
    });
    key = key.substr(0, key.length - 1);
    stStatus.onConfigure(key, this.settings);
};

Product.Config.prototype.loadStatus = function() {
    var key = '';
    stStatus.onConfigure(key, this.settings);
}

Product.Config.prototype.configureElement = function(element) {
    this.reloadOptionLabels(element);

    if(element.value){
        this.state[element.config.id] = element.value;
        if(element.nextSetting){
            element.nextSetting.disabled = false;
            this.fillSelect(element.nextSetting);
            this.resetChildren(element.nextSetting);
        }
    } else {
        this.resetChildren(element);
    }

    this.reloadPrice();

    //Amasty code for Automatically select attributes that have one single value
    if(('undefined' != typeof(amConfAutoSelectAttribute) && amConfAutoSelectAttribute) ||('undefined' != typeof(amStAutoSelectAttribute) && amStAutoSelectAttribute)){
        var nextSet = element.nextSetting;
        if(nextSet && nextSet.options.length == 2 && !nextSet.options[1].selected && element && !element.options[0].selected){
            nextSet.options[1].selected = true;
            this.configureElement(nextSet);
        }
    }
}

Product.Config.prototype.configureForValues =  function () {
    if (this.values) {
        this.settings.each(function(element){
            var attributeId = element.attributeId;
            element.value = (typeof(this.values[attributeId]) == 'undefined')? '' : this.values[attributeId];
            this.configureElement(element);
        }.bind(this));
    }
    //Amasty code for Automatically select attributes that have one single value
     if(('undefined' != typeof(amConfAutoSelectAttribute) && amConfAutoSelectAttribute) ||('undefined' != typeof(amStAutoSelectAttribute) && amStAutoSelectAttribute)){
        var select  = this.settings[0];
        if(select && select.options.length == 2 && !select.options[1].selected){
            select.options[1].selected = true;
            this.configureElement(select);
        }
     }
}

function explode (delimiter, string, limit) 
{
    var emptyArray = { 0: '' };
    
    // third argument is not required
    if ( arguments.length < 2 ||
        typeof arguments[0] == 'undefined' ||
        typeof arguments[1] == 'undefined' ) {
        return null;
    }
 
    if ( delimiter === '' ||
        delimiter === false ||
        delimiter === null ) {
        return false;
    }
 
    if ( typeof delimiter == 'function' ||
        typeof delimiter == 'object' ||
        typeof string == 'function' ||
        typeof string == 'object' ) {
        return emptyArray;
    }
 
    if ( delimiter === true ) {
        delimiter = '1';
    }
    
    if (!limit) {
        return string.toString().split(delimiter.toString());
    } else {
        // support for limit argument
        var splitted = string.toString().split(delimiter.toString());
        var partA = splitted.splice(0, limit - 1);
        var partB = splitted.join(delimiter.toString());
        partA.push(partB);
        return partA;
    }
}

function implode (glue, pieces) {
    var i = '', retVal='', tGlue='';

    if (arguments.length === 1) {
        pieces = glue;
        glue = '';
    }

    if (typeof(pieces) === 'object') {
        if (pieces instanceof Array) {
            return pieces.join(glue);
        } else {
            for (i in pieces) {
                retVal += tGlue + pieces[i];
                tGlue = glue;
            }
            return retVal;
        }
    } else {
        return pieces;
    }
}

function strpos (haystack, needle, offset) {
    var i = (haystack+'').indexOf(needle, (offset ? offset : 0));
    return i === -1 ? false : i;
}

Event.observe(window, 'load', function(){
    defaultProductId = document.getElementsByName('product')[0].value;
});
