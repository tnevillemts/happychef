StockStatusMessages = Class.create();
StockStatusMessages.prototype = {
    options : null,
    productIdInit: 0,
    qtyInit: 1,
    stockStatusConfig: null,
    initialize: function(productId, qtyInit) {
        var self = this;
        self.productIdInit = productId;
        self.qtyInit = qtyInit;
        self.stockStatusConfig = window.stStatus;
        self.bindSelectionUpdates();
    },
    updateStockStatusMessage: function() {
        var self = this;
        var activeProductId = self.getActiveProductId();
        var pdpQty = self.getPdpQty();
        self.getStockStatusMessage(activeProductId, pdpQty);
    },
    bindSelectionUpdates: function() {
        var self = this;
        // Delegation needed as options are appended to the DOM upon selection
        jQuery('#product-options-wrapper').on('click', '.amconf-image-container', function(){
            self.updateStockStatusMessage();
        });
        jQuery('#qty').on('keyup', function(e) {
            // embroidery.viewModel triggers a keyup, only request stock status on a true keypress
            // Ensure one AJAX request
            if(typeof e.isTrigger == 'undefined') {
                self.updateStockStatusMessage();
            }
        });
    },
    getActiveProductId: function() {
        var self = this;
        var configKey = self.getConfigKey();
        return (typeof self.stockStatusConfig !== "undefined" && typeof self.stockStatusConfig.options[configKey] !== "undefined") ? self.stockStatusConfig.options[configKey].product_id : self.productIdInit ;
    },
    getConfigKey: function() {
        var selectedAttributes = [];
        jQuery('.super-attribute-select').each(function(){
            var value = jQuery(this).val();
            if(value.length > 0) {
                selectedAttributes.push(value);
            }
        });
        return selectedAttributes.join(',');
    },
    getPdpQty: function() {
        var pdpQty = jQuery('#qty').val();
        return (pdpQty > 0) ? pdpQty : 1 ;
    },
    getStockStatusMessage: function(productId, qty) {
        jQuery.ajax({
            url: EM.QuickShop.BASE_URL + "/stockstatuscontext/index/getStockStatusData",
            data: {productId: productId, qty: qty},
            type: "POST",
            success: function (res) {
                var stockStatusType = res.stockStatusContext.type;
                var isNewProduct = res.isNewProduct;
                var sku = res.stockStatusContext.sku;
                var messages = res.stockStatusContext.stockStatusMessages;
                jQuery('div.custom_status').html("<span id='amstockstatus-status' class='"+stockStatusType+"'>" + messages.verbose + "</span>");
                if (stockStatusType == "nostock" && isNewProduct == true) {
                    if (jQuery('.pdp-add-to-cart-wrap > button').hasClass('add-to-cart-with-embroidery')) {
                        jQuery('#add-cart-text').text('PRE-ORDER WITH EMBROIDERY');
                    } else {
                        jQuery('#add-cart-text').text("PRE-ORDER");
                        if (!jQuery('.pdp-add-to-cart-wrap > button').hasClass('btn-black')) {
                            jQuery('.pdp-add-to-cart-wrap > button').addClass('btn-black');
                        }
                    }

                    if (!jQuery('.custom_status').hasClass('preorder')) {
                        jQuery('.custom_status').addClass('preorder')
                    }
                } else {
                    if (jQuery('.pdp-add-to-cart-wrap > button').hasClass('add-to-cart-with-embroidery')) {
                        jQuery('#add-cart-text').text("ADD TO CART W/ EMBROIDERY");
                    } else {
                        jQuery('#add-cart-text').text("ADD TO CART");
                    }
                    if (jQuery('.pdp-add-to-cart-wrap > button').hasClass('btn-black')) {
                        jQuery('.pdp-add-to-cart-wrap > button').removeClass('btn-black');
                    }
                    if (jQuery('.custom_status').hasClass('preorder')) {
                        jQuery('.custom_status').removeClass('preorder')
                    }
                }
                _gaq.push(['_trackEvent', 'stock status', 'PDP - ' + messages.conscise, sku]);
            },
            error: function (res) {
                console.log(res);
            }
        });
    }
};