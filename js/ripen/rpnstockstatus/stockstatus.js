StockStatusMessages = Class.create();
StockStatusMessages.prototype = {
    options : null,
    productIdInit: 0,
    qtyInit: 1,
    stockStatusConfig: null,
    initialize: function(productId, qtyInit) {
        var self = this;
        self.productIdInit = productId;
        self.qtyInit = qtyInit;
        self.stockStatusConfig = window.stStatus;
        self.bindSelectionUpdates();
        self.updateStockStatusMessage();
    },
    updateStockStatusMessage: function() {
        var self = this;
        var activeProductId = self.getActiveProductId();
        var pdpQty = self.getPdpQty();
        self.getStockStatusMessage(activeProductId, pdpQty);
    },
    bindSelectionUpdates: function() {
        var self = this;
        // Delegation needed as options are appended to the DOM upon selection
        jQuery('#product-options-wrapper').on('click', '.amconf-image-container', function(){
            self.updateStockStatusMessage();
        });
        jQuery('#qty').on('keyup change', function() {
            self.updateStockStatusMessage();
        });
        jQuery('.pdp-add-to-cart-wrap .btn-cartf').on('click', function() {
            self.updateStockStatusMessage();
        });
    },
    getActiveProductId: function() {
        var self = this;
        var configKey = self.getConfigKey();
        return (typeof self.stockStatusConfig !== "undefined" && typeof self.stockStatusConfig.options[configKey] !== "undefined") ? self.stockStatusConfig.options[configKey].product_id : self.productIdInit ;
    },
    getConfigKey: function() {
        var selectedAttributes = [];
        jQuery('.super-attribute-select').each(function(){
            var value = jQuery(this).val();
            if(value.length > 0) {
                selectedAttributes.push(value);
            }
        });
        return selectedAttributes.join(',');
    },
    getPdpQty: function() {
        var pdpQty = parseInt(jQuery('#qty').val(), 10);
        return (pdpQty > 0) ? pdpQty : 1 ;
    },
    getStockStatusMessage: function(productId, qtyRequested) {
        new Ajax.Request('/stockstatusajax/index/index', {
            parameters: { productId: productId, qtyRequested: qtyRequested },
            onComplete: function(response) {
                if (response.responseJSON != null) {
                    var skuModel = response.responseJSON.skuModel;
                    var statusText = response.responseJSON.statusText;
                    var availQty = response.responseJSON.availQty;
                    var pdpQty = response.responseJSON.pdpQty;
                    var cartQty = response.responseJSON.cartQty;
                    var totQtyRequested = pdpQty + cartQty;
                    var remainingQty = availQty - cartQty;
                    var statusType = "hasstock";
                    var statusMessage = "In Stock";
                    var statusWrap = jQuery('.custom_status');

                    if (totQtyRequested > availQty && availQty > 0) {
                        statusType = "partial";

                        if (cartQty > 0) {
                            if (cartQty > availQty) {
                                statusMessage = statusText + " Your cart contains " + cartQty + " of this item. To ship today, order a total of " + availQty + " or less."
                            } else {
                                statusMessage = statusText + " Your cart contains " + cartQty + " of this item. To ship today, select " + remainingQty + " or less."
                            }
                        } else {
                            var trimmedStatusText = statusText.slice(0, -1);
                            statusMessage = trimmedStatusText + " for your order of " + pdpQty + ". To ship today, order " + availQty + " or less.";
                        }
                    } else if (totQtyRequested > availQty && availQty <= 0) {
                        statusType = "nostock";
                        statusMessage = statusText;
                    }

                    jQuery.each(statusWrap, function () {
                        if (jQuery(this).find(jQuery('#amstockstatus-status')).length) {
                            jQuery(this).find(jQuery('#amstockstatus-status')).remove();
                        }
                        span = document.createElement('span');
                        span.id = 'amstockstatus-status';
                        span.innerHTML = statusMessage;
                        span.className = statusType;
                        jQuery(this).prepend(span);
                    });

                    if (totQtyRequested > availQty) {
                        _gaq.push(["'_trackEvent', 'stock status', '" + response.responseJSON.pageType + " - " + statusText + "', '" + skuModel + "'"]);
                    }
                }
            }
        });
    }
};