
function send_alert_guest_email(url, button) {
    var buttonId = button.id;
    var f = document.createElement('form');
    var productId = button.id.replace(/\D+/g, "");
    var block = button.up('.amxnotif-block');
    var notification = $('amxnotif_guest_email-' + productId);
    if (notification) {
        notification.addClassName("validate-email required-entry");
    }

    if (block) {
        block.childElements().each(function (child) {
            f.appendChild(child.cloneNode(true));
        });
    }

    var validator = new Validation(block);
    if (validator.validate()) {
        f.action = url;
        f.hide();
        $$('body')[0].appendChild(f);
        f.setAttribute("method", 'post');
        f.id = 'am_product_addtocart_form';
        if ($(buttonId) != undefined) {
            showButtonLoader(button);
        }

        new Ajax.Request(
            f.action,
            {
                method: 'post',
                asynchronous: true,
                evalScripts: false,
                parameters: f.serialize(true),
                onComplete: function (request) {
                    var response = request.responseJSON.message;
                    if ($(buttonId) != undefined) {
                        hideButtonLoader(button);
                    }
                    var alertStockResponse = document.createElement('p');
                    alertStockResponse.innerHTML = response;
                    alertStockResponse.className = 'alert-stock-response';
                    block.childElements().each(function(child) {
                        child.remove();
                    });
                    block.appendChild(alertStockResponse);
                }
            }
        );
    }

    if (notification) {
        notification.removeClassName("validate-email required-entry");
    }
}

function send_alert_user_email(url) {
    var block = $('amstockstatus-stockalert');

    new Ajax.Request(
        url,
        {
            method: 'post',
            asynchronous: true,
            evalScripts: false,
            onComplete: function (request) {
                var response = request.responseJSON.message;
                var alertStockResponse = document.createElement('p');
                alertStockResponse.innerHTML = response;
                alertStockResponse.className = 'alert-stock-response';
                block.childElements().each(function(child) {
                    child.remove();
                });
                block.appendChild(alertStockResponse);
            }
        }
    );
}

function checkIt(evt, url, button) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 13) {
        send_alert_guest_email(url, button);
        return false;
    }
    return true;
}

var openPopup = null;

function showSubscribePopup(productId) {
    openPopup = $('subcribe-' + productId);
    openPopup.show();
    window.onclick = function(event) {
        if (event.target == openPopup) {
            closeSubscribePopup();
        }
    }
}

function closeSubscribePopup() {
    openPopup.hide();
    openPopup = null;
}
