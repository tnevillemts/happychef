var ShoppingList = Class.create();

ShoppingList.prototype = {
  initConfig: {
    addToShoppingListPath: null,
    addToShoppingListLabel: null,
    isProductPage: false,
    needSaveForLater: false,
    save_for_later_label: null,
    save_for_later_url: null
  },

  initialize: function (initConfig) {
    this.initConfig = initConfig;
    this.createShoppingListLinks();
    this.insertSaveForLater();
  },

  getInitConfig: function (config_name) {
    return this.initConfig[config_name];
  },

  insertSaveForLater: function () {
    if ((this.getInitConfig('needSaveForLater'))) {
      var save_for_later_html = "<a href='"+this.getInitConfig('save_for_later_url')+"' class='create-new-group-in-cart'><button type='button' " +
        "title='" + this.getInitConfig('save_for_later_label') + "'" +
        "class='button btn-save-later' style='padding-right:10px;' >" + "<span><span>" +
        this.getInitConfig('save_for_later_label') +
        "</span></span></button></a>";

	if($('empty_cart_button')){
      		$('empty_cart_button').insert({
        		before: save_for_later_html
      		});
	}
    }
  },
  

  createShoppingListUrl: function (addToLinksElement) {
    var url_from_wishlist = addToLinksElement.select('.link-wishlist').pop().readAttribute('href')
      .replace("wishlist/index/add", this.getInitConfig('addToShoppingListPath'));
    if (url_from_wishlist) return url_from_wishlist;

    var url_from_compare = addToLinksElement.select('.link-compare').pop().readAttribute('href')
      .replace("catalog/product_compare/add", this.getInitConfig('addToShoppingListPath'));
    if (url_from_compare) return url_from_compare;
  },

  createShoppingListLinkHtml: function (shopping_list_url) {
//  Functionality suspended
//    var shopping_list_link_html =
//        '<li>' +
//          '<a href="' + shopping_list_url + '" class="link-shoppinglist">' +
//          this.getInitConfig('addToShoppingListLabel') +
//          '</a>' +
//          '</li>'
//      ;
//    return shopping_list_link_html;
      return '';
  },

  createShoppingListLinks: function () {
    if (!this.getInitConfig('isProductPage')) {
      $$('.add-to-links').each(function (addToLinksElement) {
        var shopping_list_url = this.createShoppingListUrl(addToLinksElement);
        var shopping_list_link_html = this.createShoppingListLinkHtml(shopping_list_url);
        addToLinksElement.insert(shopping_list_link_html);
      }.bind(this));
    }
  }
}

  
