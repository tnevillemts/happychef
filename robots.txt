#
# This file is used to allow crawlers to index our site. 
#
# List of all web robots: http://www.robotstxt.org/wc/active/html/index.html
#
# Check robots.txt at:
# http://www.searchengineworld.com/cgi-bin/robotcheck.cgi
#

User-agent: Baiduspider
User-agent: Baiduspider-video
User-agent: Baiduspider-image
Disallow: /

User-agent: *
Disallow: /translate/
Disallow: /wishlist/
Disallow: /catalogsearch/
Disallow: /lookbook/
