jQuery.noConflict();
(function ($) {

    $(document).ready(function($) {

        $('.set-embroidery-options').click(function(){

            var locationId = this.id.split('-')[1];

            $('.embroidery-options').each(function(){
                if($(this).find('input:checked').length == 0){
                    $(this).hide();
                }
            });
            $('#embroidery-options-' + locationId).show();

            //console.log(locationId + " - " + productId);

        });

    });

})(jQuery);
