/**
 * Artwork Manager Init JS
 *
 * @author Ripen eCommerce
 * @package Ripen_Embroidery
 */
(function ($) {
    $(document).ready(function($) {
        var artworkManager = new ArtworkManagerModel(window.EMBROIDERY_ARTWORK_MANAGER_GLOBAL_CONFIG);
    });
})(jQuery);