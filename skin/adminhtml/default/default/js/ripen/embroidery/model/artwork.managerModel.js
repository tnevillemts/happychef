/**
 * Artwork Manager Model
 *
 * @author jhorvath@ripen.com - RIPEN
 * @package Ripen_Embroidery
 */
(function ($) {
    $(document).ready(function($) {

        /**
         * ArtworkManager Object
         *
         * @constructor
         */
        window.ArtworkManagerModel = function (config) {

            var self = this;

            self.managerScopeElementKey = 'embroidery-artwork-manager';

            self.formKey = '';

            self.baseMediaUrl = '';

            self.ajaxUrls = {};

            self.alertTimeout = null;

            self.uiState = {
                categories: {
                    activeViewed: ko.observable(false),
                    activeTitleText: ko.observable(''),
                    options: ko.observableArray([])
                },
                applicationAlert: {
                    message: ko.observable(''),
                    lifetime: 12000
                },
                modal: {
                    active: ko.observable(false),
                    title: ko.observable(''),
                    context: ko.observable(''),
                    proxySubmit: {
                        targetFormId: ko.observable(''),
                        text: ko.observable('')
                    },
                    data: ko.observable({}),
                    toggleBodyScroll: ko.computed({
                        read: function () {
                            if(self.uiState.modal.active()) {
                                $('body').addClass('modal-active');
                            } else {
                                $('body').removeClass('modal-active');
                            }
                        },
                        deferEvaluation: true
                    })
                },
                isSaving: ko.observable(false)
            };

            self.addArtworkForm = {
                userFields: {
                    artworkUploadImage: ko.observable(''),
                    name: ko.observable(''),
                    nameOld: ko.observable(''),
                    artworkCategoryId: ko.observable(''),
                    featured: ko.observable(0),
                    enabled: ko.observable(0)
                },
                submitFields: ko.observableArray([
                    'form_key',
                    'artwork_upload_image',
                    'name',
                    'name_old',
                    'artwork_category_id',
                    'featured',
                    'enabled'
                ])
            };

            self.updateArtworkForm = {
                submitFields: ko.observableArray([
                    'form_key',
                    'type_option_id',
                    'artwork_upload_image'
                ])
            };

            self.addCategoryForm = {
                submitFields: ko.observableArray([
                    'form_key',
                    'name',
                    'featured_text',
                    'featured',
                    'enabled'
                ])
            };

            /**
             * Categories data for rendering and update categories listing
             */
            self.categories = ko.observableArray([]);

            /**
             * Artwork options data for rendering and update categories listing
             */
            self.artworkOptions = ko.observableArray([]);

            /**
             * Construct
             * @private
             */
            self.__construct = function (config) {
                self.formKey = config.formKey;
                self.baseMediaUrl = config.baseMediaUrl;
                self.ajaxUrls = config.ajaxUrls;
                self.categories(config.categories);
                self.artworkOptions(config.artworkOptions);
                self.initCategoriesSorting();
                self.applyScopeElementBinding();
                self._initTitleBar();
            };

            /**
             * Container element for ko bindings of this viewModel
             * @returns {string}
             * @private
             */
            self._managerScopeElementId = function () {
                return '#' + self.managerScopeElementKey;
            };

            /**
             * Apply sortable functionality to category and artwork lists
             */
            self.initCategoriesSorting = function () {
                self.initSortableList('categories-list');
                self.initSortableList('category-artwork-options');
            };

            /**
             * Replace html element a text input
             * @param data
             * @param event
             */
            self.editText = function (data, event) {
                var $editElem = $(event.delegateTarget),
                    textValue = $editElem.text(),
                    $inputElem = self._getEditTextInput(textValue);
                if($editElem.attr('data-input-data-bind')) {
                    var inputDataBind = $editElem.attr('data-input-data-bind');
                    $inputElem.attr('data-bind', inputDataBind);
                    ko.applyBindings(self, $inputElem[0]);
                }
                $editElem.after($inputElem);
                $editElem.detach();
                var transferEdit = function() {
                    var transferText = $inputElem.val(),
                        dataFieldKey =  $editElem.attr('data-field-key'),
                        dataFieldValueKey = 'data-field-' + dataFieldKey.replace(' ', '-');

                    if($editElem.text() !== transferText) {
                        $editElem.addClass('value-changed');
                        $editElem.attr(dataFieldValueKey, transferText);
                        $editElem.text(transferText);
                    }

                    $inputElem.replaceWith($editElem);
                    $inputElem.remove();

                    return true;
                };
                $inputElem.one('blur', transferEdit).focus();
            };

            /**
             * Get the input element to be used with self.editText()
             * @param textValue
             * @returns {*|jQuery}
             * @private
             */
            self._getEditTextInput = function(textValue) {
                var $inputElem = $('<input/>').val(textValue);
                $inputElem.addClass('data-field-edit-input');
                $inputElem.bind('blur keyup', function(e) {
                    if($(this).val() === '') {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }
                    if ((e.keyCode === 10 || e.keyCode === 13)) {
                        $(this).blur();
                    }
                });
                return $inputElem
            };

            /**
             * Mainly for editing 'Featured Text' which requires slightly different validation, and text replacement
             * @param data
             * @param event
             */
            self.editFeaturedText = function (data, event) {
                var $editElem = $(event.delegateTarget),
                    dataFieldKey = $editElem.attr('data-field-key'),
                    dataFieldValueKey= 'data-field-' + dataFieldKey.toLowerCase().replace(' ', '-'),
                    textValue = $editElem.attr(dataFieldValueKey),
                    $inputElem = self._getEditFeaturedTextInput(textValue);
                if($editElem.attr('data-input-data-bind')) {
                    var inputDataBind = $editElem.attr('data-input-data-bind');
                    $inputElem.attr('data-bind', inputDataBind);
                    ko.applyBindings(self, $inputElem[0]);
                }
                $editElem.after($inputElem);
                $editElem.detach();
                var transferEdit = function() {
                    var transferValue = $inputElem.val(),
                        transferText = (transferValue === '') ? '<em>- None -</em>' : transferValue ;

                    if(transferValue !== textValue) {
                        $editElem.addClass('value-changed');
                        $editElem.attr(dataFieldValueKey, transferValue);
                        $editElem.html(transferText);
                    }

                    $inputElem.replaceWith($editElem);
                    $inputElem.remove();

                    return true;
                };
                $inputElem.one('blur', transferEdit).focus();
            };

            /**
             * Featured text input element
             * @param textValue
             * @returns {*|jQuery}
             * @private
             */
            self._getEditFeaturedTextInput = function (textValue) {
                var $inputElem = $('<input/>').val(textValue);
                $inputElem.attr('placeholder', 'Enter featured text');
                $inputElem.addClass('data-field-edit-input');
                $inputElem.attr('maxlength','20');
                $inputElem.bind('blur keyup', function(e) {
                    if ((e.keyCode === 10 || e.keyCode === 13)) {
                        $(this).blur();
                    }
                });
                return $inputElem
            };

            /**
             * Replace element with a category select option list
             * @param data
             * @param event
             */
            self.artworkCategorySelect = function (data, event) {
                var $editElem = $(event.delegateTarget),
                    currentCategoryElemId = $editElem.attr('data-field-artwork-category-id'),
                    $selectElem = self._getArtworkCategorySelect(currentCategoryElemId);
                $editElem.after($selectElem);
                $editElem.detach();
                var transferSelection = function () {
                    var selectedId = $selectElem.val();
                    if(selectedId !== currentCategoryElemId) {
                        var fromCategoryName = self.getCategoryNameById(self.uiState.categories.activeViewed()),
                            toCategoryName = self.getCategoryNameById(selectedId);
                        $editElem.attr('data-field-artwork-category-id', selectedId);
                        $editElem.removeClass('value-changed');
                        $editElem.find('.move-notification').remove();
                        if(selectedId !== self.uiState.categories.activeViewed()) {
                            $editElem.html('Changed to: <strong>' + toCategoryName + '</strong>');
                            var $moveNotification = $('<span></span>'),
                                moveCategoryNotificationText = 'Moving from <em>' + fromCategoryName + '</em> to <em>' + toCategoryName + '</em> pending.';
                            $editElem.addClass('value-changed');
                            $moveNotification.addClass('move-notification');
                            $moveNotification.html(moveCategoryNotificationText);
                            $editElem.append($moveNotification);
                        } else {
                            $editElem.text(toCategoryName);
                        }

                        $editElem.attr('data-field-artwork-category-id', selectedId);
                        $editElem.text();
                    }
                    $selectElem.replaceWith($editElem);
                    $selectElem.remove();
                };
                $selectElem.one('blur', transferSelection).focus();

            };

            /**
             * Artwork category select element
             * @param currentCategoryId
             * @returns {*|jQuery|HTMLElement}
             * @private
             */
            self._getArtworkCategorySelect = function (currentCategoryId) {
                var $selectElem = $('<select></select>');
                $selectElem.addClass('editable-select');
                $.each(self.categories(), function (key, category) {
                    var $appendOption = $("<option></option>")
                        .attr('value', category.categoryId)
                        .text(category.name);
                    if(currentCategoryId === category.categoryId) {
                        $appendOption.prop('selected', true);
                    }
                    $selectElem.append($appendOption);
                });
                $selectElem.bind('blur keyup', function(e) {
                    if ((e.keyCode === 10 || e.keyCode === 13)) {
                        $(this).blur();
                    }
                });
                return $selectElem;
            };

            /**
             * Replace element with Yes/No select for setting binary flags
             * @param data
             * @param event
             */
            self.yesNoSelect = function (data, event) {
                self.sequentialSelect(event.delegateTarget, ['No', 'Yes'])
            };

            /**
             * Replace element Featured/Not Featured select for setting the flag of 'featured'
             * @param data
             * @param event
             */
            self.featuredSelect = function (data, event) {
                self.sequentialSelect(event.delegateTarget, ['Not Featured', 'Featured'])
            };

            /**
             * Replace element Disabled/Enabled select for setting binary flag
             * @param data
             * @param event
             */
            self.enabledSelect = function (data, event) {
                self.sequentialSelect(event.delegateTarget, ['Disabled', 'Enabled']);
            };

            /**
             * Replace element with select options that are passed in. Use this for custom option list.
             * @param elem
             * @param options
             */
            self.sequentialSelect = function (elem, options) {
                var $editElem = $(elem),
                    dataFieldKey = $editElem.attr('data-field-key'),
                    dataFieldValueKey = 'data-field-' + dataFieldKey,
                    dataFieldExistingValue = $editElem.attr(dataFieldValueKey),
                    $selectElem = self._getSequentialSelectElem(dataFieldExistingValue, options);
                $editElem.after($selectElem);
                $editElem.detach();
                var transferSelection = function () {
                    var selectedValue = $selectElem.val(),
                        selectedText = $selectElem.children('option').filter(':selected').text();
                    if(selectedValue !== dataFieldExistingValue) {
                        $editElem.addClass('value-changed');
                    }
                    $editElem.attr(dataFieldValueKey, selectedValue);
                    $editElem.text(selectedText);
                    $selectElem.replaceWith($editElem);
                    $selectElem.remove();
                };
                $selectElem.one('blur', transferSelection).focus();
            };

            /**
             * Get select element matching options passed in through self.sequentialSelect()
             * @param selectedValue
             * @param options
             * @returns {*|jQuery|HTMLElement}
             * @private
             */
            self._getSequentialSelectElem = function (selectedValue, options) {
                var $selectElem = $('<select></select>');
                $selectElem.addClass('editable-select');
                $.each(options, function (value, text) {
                    var $option = $("<option></option>")
                        .attr('value', value)
                        .text(text);
                    if(+selectedValue === +value) {
                        $option.prop('selected', true);
                    }
                    $selectElem.append($option);
                });
                $selectElem.bind('blur keyup', function(e) {
                    if ((e.keyCode === 10 || e.keyCode === 13)) {
                        $(this).blur();
                    }
                });
                return $selectElem;
            };

            /**
             * Switch between category listing, and category view
             * @param categoryId
             * @returns {boolean}
             */
            self.toggleViewedCategory = function (categoryId) {
                self.scrollToTop();
                if(categoryId === self.uiState.categories.activeViewed()) {
                    self.uiState.categories.activeViewed(false);
                    self.uiState.categories.activeTitleText('');
                    self.uiState.categories.options([]);
                    return false;
                }
                self.uiState.categories.activeViewed(categoryId);
                var activeTitle = self.categoryTitleText(categoryId),
                    activeContents = (typeof self.artworkOptions()[categoryId] !== 'undefined') ?self.artworkOptions()[categoryId] : [];
                self.uiState.categories.activeTitleText(activeTitle);
                self.uiState.categories.options(activeContents);
            };

            /**
             * Save the current order and data in category listing view
             * @param listSelector
             */
            self.saveCategoriesOrder = function (listSelector) {
                var sortOrderCounter = 1;
                var categoriesUpdateData = [];
                $(listSelector + ' li[data-record-type]').each(function() {
                    var currentSortRecord = {
                        categoryId: +$(this).attr('data-record-id'),
                        name: $(this).find('[data-field-name]').attr('data-field-name'),
                        sortorder: +sortOrderCounter,
                        featured: +$(this).find('[data-field-featured]').attr('data-field-featured'),
                        featuredText: $(this).find('[data-field-featured-text]').attr('data-field-featured-text'),
                        enabled: +$(this).find('[data-field-enabled]').attr('data-field-enabled')
                    };
                    categoriesUpdateData.push(currentSortRecord);
                    sortOrderCounter++;
                });
                var formData = new FormData();
                formData.append('form_key', self.formKey);
                formData.append('categories_update_data', JSON.stringify(categoriesUpdateData));
                $.ajax({
                    type: 'POST',
                    url: self.ajaxUrls.saveCategoriesOrder,
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        self.categories(response.categories);
                        self.triggerApplicationAlert('Categories listing saved.');
                    },
                    error: function (errorResponse) {
                        console.error(errorResponse);
                    }
                });
            };

            /**
             * Save the artwork options order and data in current viewed category
             */
            self.saveViewedCategory = function () {
                var sortOrderCounter = 1,
                    artworkOptionsUpdateData = [],
                    currentCategoryId = self.uiState.categories.activeViewed(),
                    moveLogMessage = '';

                $('.category-artwork-options li[data-record-type]').each(function () {
                    var isCategoryMove = ($(this).find('[data-field-artwork-category-id]').attr('data-field-artwork-category-id') !== currentCategoryId),
                        currentSortorder = (isCategoryMove) ? '0' : sortOrderCounter,
                        artworkCategoryId = +$(this).find('[data-field-artwork-category-id]').attr('data-field-artwork-category-id'),
                        artworkName = $(this).find('[data-field-name]').attr('data-field-name').trim(),
                        featured = +$(this).find('[data-field-featured]').attr('data-field-featured').trim(),
                        enabled = +$(this).find('[data-field-enabled]').attr('data-field-enabled').trim();
                    if(isCategoryMove) {
                        moveLogMessage += '<div class="move-log-item">-- Moved "' + artworkName + '" to <em>' + self.getCategoryNameById(artworkCategoryId) + '</em></div>';
                    }
                    var currentRecord = {
                        typeOptionId: $(this).attr('data-record-id'),
                        artworkCategoryId: artworkCategoryId,
                        name: artworkName,
                        nameOld: $(this).find('[data-field-name-old]').attr('data-field-name-old').trim(),
                        sortorder: currentSortorder,
                        featured: featured,
                        enabled: enabled
                    };
                    artworkOptionsUpdateData.push(currentRecord);
                    if(!isCategoryMove) {
                        sortOrderCounter++;
                    }

                });
                var formData = new FormData();
                formData.append('form_key', self.formKey);
                formData.append('current_category_id', currentCategoryId);
                formData.append('artwork_options_update_data', JSON.stringify(artworkOptionsUpdateData));
                $.ajax({
                    type: 'POST',
                    url: self.ajaxUrls.saveArtworkOptionsOrder,
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        var currentCategoryId = self.uiState.categories.activeViewed();
                        self.categories(response.categories);
                        self.artworkOptions(response.artworkOptions);
                        self.uiState.categories.options(self.artworkOptions()[currentCategoryId]);
                        var categoryName = self.getCategoryNameById(currentCategoryId),
                            alertMessage = 'Artwork options saved for ' + categoryName + '.' + moveLogMessage;
                        self.resetUiState();
                        self.toggleViewedCategory(currentCategoryId);
                        self.triggerApplicationAlert(alertMessage);
                    },
                    error: function (errorResponse) {
                        console.error(errorResponse);
                    }
                });
            };

            /**
             * Open Add Category Modal
             */
            self.addCategoryModal = function () {
                self.uiState.modal.active(true);
                self.uiState.modal.title('Add Category');
                self.uiState.modal.context('add-category');
                self.uiState.modal.proxySubmit.targetFormId('#add-category-form');
                self.uiState.modal.proxySubmit.text('Submit Add Category');
            };

            /**
             * Submit the Add Category form
             * @param formElement
             * @returns {boolean}
             */
            self.addCategorySubmit = function (formElement) {
                var $form = $(formElement),
                    isValidForm = self.validateForm($form);

                if(!isValidForm) {
                    return false;
                }
                var formData = new FormData();
                $.each(self.addCategoryForm.submitFields(), function (key, fieldName) {
                    var fieldValue = $form.find('[name="' + fieldName + '"]').val();
                    formData.append(fieldName, fieldValue);
                });
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        self.categories(response.categories);
                        self.artworkOptions(response.artworkOptions);
                        self.closeModal();
                        $form.trigger('reset');
                        self.resetUiState();
                        self.triggerApplicationAlert(response.alertMessage);
                    },
                    error: function (errorResponse) {
                        console.error(errorResponse);
                    }
                });
            };

            /**
             * Open Delete Category modal
             * @param data
             * @param event
             */
            self.deleteCategoryModal = function (data, event) {
                var categoryName = self.getCategoryNameById(self.uiState.categories.activeViewed());
                self.uiState.modal.active(true);
                self.uiState.modal.title('Delete Category - ' + categoryName);
                self.uiState.modal.context('delete-category');
                self.uiState.modal.proxySubmit.targetFormId('#delete-category-form');
                self.uiState.modal.proxySubmit.text('Confirm Delete Category');
            };

            /**
             * Submit Delete Category
             * @param formElement
             */
            self.deleteCategorySubmit = function (formElement) {
                var $form = $(formElement),
                    formData = new FormData();

                formData.append('form_key', $form.find('[name="form_key"]').val());
                formData.append('category_id', $form.find('[name="category_id"]').val());
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        self.categories(response.categories);
                        self.artworkOptions(response.artworkOptions);
                        self.closeModal();
                        $form.trigger('reset');
                        self.resetUiState();
                        self.triggerApplicationAlert(response.alertMessage);
                    },
                    error: function (errorResponse) {
                        console.error(errorResponse);
                    }
                });
            };

            /**
             * Open Add Artwork Modal
             */
            self.addArtworkModal = function () {
                self.uiState.modal.active(true);
                self.uiState.modal.title('Add Artwork');
                self.uiState.modal.context('upload-artwork-form');
                self.uiState.modal.proxySubmit.targetFormId('#add-artwork-form');
                self.uiState.modal.proxySubmit.text('Submit New Artwork');
            };

            /**
             * Submit Add Artwork form
             * @param formElement
             * @returns {boolean}
             */
            self.addArtworkFormSubmit = function (formElement) {
                var $form = $(formElement),
                    artworkUploadImageFile = $form.find('[name="artwork_upload_image"]').prop('files')[0],
                    formData = new FormData(),
                    isValidForm = self.validateForm($form);

                if(!isValidForm) {
                    return false;
                }

                $.each(self.addArtworkForm.submitFields(), function (key, fieldName) {
                    var fieldValue = $form.find('[name="' + fieldName + '"]').val();
                    formData.append(fieldName, fieldValue);
                });

                formData.append('artwork_upload_image_file', artworkUploadImageFile);
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        self.categories(response.categories);
                        self.artworkOptions(response.artworkOptions);
                        self.closeModal();
                        $form.trigger('reset');
                        var categoryId = response.addedToCategoryId,
                            categoryName = self.getCategoryNameById(categoryId),
                            artworkAddedName = response.artworkAddedName,
                            artworkAddedNameOld = response.artworkAddedNameOld,
                            alertMessage = 'Artwork successful added to <em>' + categoryName + '</em> as <em>' +artworkAddedName + ' (' + artworkAddedNameOld + ')</em>.';
                        self.triggerApplicationAlert(alertMessage);
                        self.resetUiState();
                        self.toggleViewedCategory(categoryId);
                    },
                    error: function(errorResponse) {
                        console.error(errorResponse);
                        $form.trigger('reset');
                        self.closeModal();
                        self.resetUiState();
                        self.triggerApplicationAlert(errorResponse.exceptionError);
                    }
                });
            };

            /**
             * Open Artwork Image modal
             * @param typeOptionId
             */
            self.updateArtworkImageModal = function(typeOptionId) {
                var artworkInfo = self.getArtworkInfo(typeOptionId);
                self.uiState.modal.active(true);
                self.uiState.modal.context('update-artwork-image');
                self.uiState.modal.title('Update ' + artworkInfo.name + ' Image');
                self.uiState.modal.data(artworkInfo);
                self.uiState.modal.proxySubmit.targetFormId('#update-artwork-image-form');
                self.uiState.modal.proxySubmit.text('Confirm Update Artwork');

            };

            /**
             * Submit Update Artwork Image
             * @param formElement
             * @returns {boolean}
             */
            self.updateArtworkImageSubmit = function (formElement) {
                var $form = $(formElement),
                    artworkUploadImageFile = $form.find('[name="artwork_upload_image"]').prop('files')[0],
                    formData = new FormData(),
                    isValidForm = self.validateForm($form);

                if(!isValidForm) {
                    return false;
                }

                $.each(self.updateArtworkForm.submitFields(), function (key, fieldName) {
                    var fieldValue = $form.find('[name="' + fieldName + '"]').val();
                    formData.append(fieldName, fieldValue);
                });
                formData.append('artwork_upload_image_file', artworkUploadImageFile);

                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        var currentCategoryId =  self.uiState.categories.activeViewed();
                        self.categories(response.categories);
                        self.artworkOptions(response.artworkOptions);
                        var updatedTypeOptionId = response.updatedTypeOptionId,
                            cacheBustImage = response.cacheBustImageName;
                        self.setCategoryArtworkOptionImage(currentCategoryId, updatedTypeOptionId, cacheBustImage);
                        self.closeModal();
                        $form.trigger('reset');
                        self.triggerApplicationAlert(response.alertMessage);
                        self.resetUiState();
                        self.toggleViewedCategory(currentCategoryId);
                    },
                    error: function (errorResponse) {
                        console.error(errorResponse);
                    }
                });
            };

            /**
             * Update current data with an artwork image data by categoryId and optionId
             * The main reason for adding this method was to bust the cache of the image on updating it
             * @param categoryId
             * @param typeOptionId
             * @param imageName
             */
            self.setCategoryArtworkOptionImage = function (categoryId, typeOptionId, imageName) {
                $.each(self.artworkOptions()[categoryId], function (key, artworkOption) {
                    if(+artworkOption.typeOptionId === +typeOptionId) {
                        artworkOption.image = imageName;
                        return false;
                    }
                });
            };

            /**
             * Open Delete Artwork Modal
             * @param typeOptionId
             */
            self.deleteArtworkModal = function (typeOptionId) {
                var artworkInfo = self.getArtworkInfo(typeOptionId);
                self.uiState.modal.active(true);
                self.uiState.modal.context('delete-artwork');
                self.uiState.modal.title('DELETE ' + artworkInfo.name + '?');
                self.uiState.modal.data(artworkInfo);
            };

            /**
             * Get the data of an artwork option by typeOptionId
             * @param typeOptionId
             */
            self.getArtworkInfo = function (typeOptionId) {
                var categoryId = self.uiState.categories.activeViewed(),
                    artworkInfo = {};
                $.each(self.artworkOptions()[categoryId], function (key, artworkOption) {
                    if(typeOptionId === artworkOption.typeOptionId) {
                        artworkInfo = artworkOption;
                        return false;
                    }
                });
                return artworkInfo;
            };

            /**
             * Delete artwork via post request, does not use a form in these case
             * @param typeOptionId
             */
            self.deleteArtwork = function (typeOptionId) {
                var formData = new FormData();
                formData.append('form_key', self.formKey);
                formData.append('type_option_id', typeOptionId);
                $.ajax({
                    type: 'POST',
                    url: self.ajaxUrls.deleteArtwork,
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        var currentCategoryId = self.uiState.categories.activeViewed();
                        self.categories(response.categories);
                        self.artworkOptions(response.artworkOptions);
                        self.closeModal();
                        self.resetUiState();
                        self.toggleViewedCategory(currentCategoryId);
                        self.triggerApplicationAlert(response.alertMessage);
                    },
                    error: function (error) {
                        console.error(error);
                    }
                })
            };

            /**
             * Close currently option modal
             */
            self.closeModal = function () {
                self.uiState.modal.active(false);
                self.uiState.modal.title('');
                self.uiState.modal.context('');
                self.uiState.modal.data({});
                self.uiState.modal.proxySubmit.targetFormId('');
                self.uiState.modal.proxySubmit.text('');
            };

            /**
             * Get the currently viewed category name
             * @returns {string}
             */
            self.currentCategoryName = function () {
                var categoryName = '';
                $.each(self.categories(), function (key, categoryData) {
                    if(self.uiState.categories.activeViewed() === categoryData.categoryId) {
                        categoryName = categoryData.name;
                        return false;
                    }
                });
                return categoryName;
            };

            /**
             * The count of artwork within a specific category
             * @param categoryId
             * @returns {*}
             */
            self.categoryArtworkCount = function (categoryId) {
                if(typeof self.artworkOptions()[categoryId] === 'undefined') {
                    return 0;
                }
                return self.artworkOptions()[categoryId].length;
            };

            /**
             * Get a category name by Id
             * @param categoryId
             * @returns {string}
             */
            self.getCategoryNameById =  function (categoryId) {
                var categoryName = '';
                $.each(self.categories(), function (key, categoryData) {
                    if(+categoryId === +categoryData.categoryId) {
                        categoryName = categoryData.name;
                        return false;
                    }
                });
                return categoryName;
            };

            /**
             * Get the full title text of a currently viewed category
             * @param categoryId
             * @returns {string}
             */
            self.categoryTitleText = function (categoryId) {
                var categoryName = self.getCategoryNameById(categoryId),
                    categoryCount = self.categoryArtworkCount(categoryId);
                return categoryName + ' (' + categoryCount + ')';
            };

            /**
             * Get the full image url of artwork
             * @param relativeImagePath
             * @returns {*}
             */
            self.mediaImageUrl = function (relativeImagePath) {
                return self.baseMediaUrl + relativeImagePath;
            };

            /**
             * Initialize sortable drag and drop list
             * @param selectorClass
             */
            self.initSortableList = function (selectorClass) {
                var listSelector = '.' + selectorClass;
                $(listSelector).sortable({
                    delay: 200,
                    group: 'simple_with_animation',
                    pullPlaceholder: false,
                    onDrop: function  ($item, container, _super) {
                        var $clonedItem = $('<li/>').css({'height': $item.height() + 'px'});
                        $item.before($clonedItem);
                        $clonedItem.animate({'height': $item.height()});
                        $('.placeholder').height(0);

                        $item.animate($clonedItem.position(), function  () {
                            $clonedItem.detach();
                            _super($item, container);
                        });
                    },
                    onDragStart: function ($item, container, _super) {
                        var offset = $item.offset(),
                            pointer = container.rootGroup.pointer;

                        adjustment = {
                            left: pointer.left - offset.left,
                            top: pointer.top - offset.top
                        };

                        _super($item, container);
                    },
                    onDrag: function ($item, position) {
                        $('.placeholder').height($item.height());
                        $item.css({
                            left: position.left - adjustment.left,
                            top: position.top - adjustment.top
                        });
                    }
                });
            };

            /**
             * Export a full list of artwork in CSV format
             * @param data
             * @param event
             * @returns {boolean}
             */
            self.exportArtworkSkuCSV = function (data, event) {
                var $exportLink = $(event.delegateTarget),
                    date = new Date(),
                    month = (date.getMonth()+1),
                    monthString = (month < 10 ) ? '0' + month : month,
                    exportDateString = date.getFullYear() + '-' + monthString + '-' + date.getDate() + '-' +
                    date.getHours() + ':' + date.getMinutes(),
                    exportFilename = 'embroidery-artwork-sku-list_' + exportDateString + '.csv',
                    csvData = self._convertArtworkOptionsToCSV(),
                    encodedData = encodeURI(csvData);
                $exportLink.attr('href', encodedData);
                $exportLink.attr('download', exportFilename);
                return true;
            };

            /**
             * Convert the artwork options data to CSV format
             * @returns {string}
             * @private
             */
            self._convertArtworkOptionsToCSV = function () {
                var csvData = 'data:text/csv;charset=utf-8,',
                    csvHeader = [
                        'Type Option ID',
                        'Artwork Category ID',
                        'Category Name',
                        'Artwork SKU',
                        'Name',
                        'Is Featured',
                        'Enabled'
                    ];
                csvData += csvHeader.splice(',') + "\n";
                $.each(self.artworkOptions(), function (categoryId, artworkData) {
                    $.each(artworkData, function (key, artworkOptionData) {
                        var rowData = [
                            artworkOptionData.typeOptionId,
                            categoryId,
                            self.getCategoryNameById(categoryId),
                            artworkOptionData.nameOld,
                            artworkOptionData.name,
                            (artworkOptionData.featured !== 1) ? 'No' : 'Yes',
                            (artworkOptionData.enabled !== 1) ? 'No' : 'Yes'
                        ];
                        csvData += rowData.splice(',') + "\n";
                    });
                });
                 return csvData;
            };

            /**
             * Trigger the application alert
             * @param message
             */
            self.triggerApplicationAlert = function (message) {
                self.uiState.applicationAlert.message(message);
                self.scrollToTop();
                self.alertTimeout = setTimeout(function () {
                    self.uiState.applicationAlert.message('');
                }, self.uiState.applicationAlert.lifetime);
            };

            /**
             * Scroll to top
             */
            self.scrollToTop = function () {
                $('html, body').animate({
                    scrollTop: 0
                }, 700);
            };

            /**
             * Make title bar switch between fixed position toolbar mode
             * @private
             */
            self._initTitleBar = function () {
                var toolbarModeThreshold = $('#embroidery-artwork-manager').offset().top,
                    $artworkTitleBar = $('#embroidery-artwork-manager-title-bar'),
                    toolbarPlaceHolderHeight = $artworkTitleBar.height(),
                    $toolbarPlaceHolderElem = $('<div></div>')
                                                .addClass('toolbar-placeholder')
                                                .css('height', toolbarPlaceHolderHeight);

                $(window).scroll(function() {
                    if ($(this).scrollTop() > toolbarModeThreshold) {
                        $artworkTitleBar.after($toolbarPlaceHolderElem);
                        $artworkTitleBar.addClass('toolbar-mode');
                    } else {
                        $toolbarPlaceHolderElem.detach();
                        $artworkTitleBar.removeClass('toolbar-mode');
                    }
                });
            };

            /**
             * Element that gets appended within forms to dsiplay validation errors
             * @param errorMessage
             * @returns {*|jQuery|HTMLElement}
             */
            self.validateErrorElement =  function (errorMessage) {
                var $errorMsgElem = $('<div></div>');
                $errorMsgElem.addClass('validate-error');
                $errorMsgElem.text(errorMessage);
                return $errorMsgElem;
            };

            /**
             * Submit target form
             * @param data
             * @param event
             */
            self.targetFormSubmit = function (data, event) {
                var targetFormSelector = self.uiState.modal.proxySubmit.targetFormId();
                $(targetFormSelector).submit();
            };

            /**
             * Form Validation
             * Will run on form elements with 'data-validate' and check self.validationRules methods based on tag data
             */
            self.validateForm = function ($form) {
                var isValid = true;
                $form.find('[data-validate]').each(function (){
                    var $elem = $(this);
                    var dataValidate = $elem.data('validate'),
                        elemValue = $elem.val().trim(),
                        $errorMsg = null;

                    $elem.next('div.validate-error').remove();
                    if(dataValidate.required && elemValue === '') {
                        $errorMsg = self.validateErrorElement('This field is required.');
                        $elem.after($errorMsg);
                        isValid = false;

                    }
                    if((typeof dataValidate.rule !== 'undefined') && (elemValue !== '')) {
                        $elem.next('div.validate-error').remove();

                        var ruleCallback = dataValidate.rule,
                            ruleResults = self.validationRules[ruleCallback](elemValue);

                        if(!ruleResults.result) {
                            $errorMsg = self.validateErrorElement(ruleResults.message);
                            $elem.after($errorMsg);
                            isValid = false;
                        }
                    }
                });

                return isValid;
            };

            /**
             * Validation Rules
             * Example tag attribute to use these rules data-validate='{"required":true, "rule": "validImageExtension" }
             * @type {{checkSku: (function(*=): {result: boolean, message: string}), validImageExtension: (function(*): {result: boolean, message: string})}}
             */
            self.validationRules = {
                checkSku: function (inputValue) {
                    var checkSku = true;
                    $.each(self.artworkOptions(), function (setKey, optionSets) {
                        $.each(optionSets, function (optionKey, artwork) {
                            if(artwork.nameOld.trim() === inputValue) {
                                checkSku = false;
                                return false;
                            }
                        });
                    });

                    return {
                        result: checkSku,
                        message: 'Sku already exists! Please enter another.'
                    };
                },
                validImageExtension: function (inputValue) {
                    var validExtensions = ['jpg', 'gif', 'png'],
                        inputExtension = inputValue.split('.').pop(),
                        validExtension = validExtensions.indexOf(inputExtension) > -1;

                    return {
                        result: validExtension,
                        message: 'Invalid image extension. Please use: ' + validExtensions.join(', ')
                    };

                }
            };

            /**
             * Make sure the sku pattern of uppercase, numbers, and dashes is only allowed LOGO-UPPERCASE1
             * @param data
             * @param event
             */
            self.sanitizeSkuField = function(data, event) {
                var sanitizedVal = $(event.delegateTarget).val().toUpperCase().replace(/[^a-z\d\-]/ig, '');
                $(event.delegateTarget).val(sanitizedVal);
            };

            self.resetUiState = function () {
                self.uiState.categories.activeViewed(false);
                self.uiState.categories.activeTitleText('');
                self.uiState.categories.options([]);
            };

            /**
             * Apply Bindings to View Model
             */
            self.applyScopeElementBinding = function () {
                ko.applyBindings(self, $(self._managerScopeElementId())[0]);
            };

            /**
             * Anonymous - Self invoke for PHP style construction.
             */
            (function() {
                self.__construct(config);
            })();
        };

    });
})(jQuery);
