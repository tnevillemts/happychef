/**
 * Main JS
 *
 * @author Ripen eCommerce
 * @package Ripen_MailFoundry
 */
(function ($) {
    $(document).ready(function($) {

        var awsFilesystemConfig = {
            viewPane: {
                title: 'Live',
                id: '#remote-file-view-pane'
            },
            filesystem: {
                type: 'aws',
                path: []
            },
            request: {
                url: MAILFOUNDRY_GLOBALS.urls.filelist,
                method: 'GET'
            }
        };

         var awsFilesystem = new MailFoundry.Filesystem(awsFilesystemConfig);

        var mailFoundry = new MailFoundry(awsFilesystem);
    });
})(jQuery);