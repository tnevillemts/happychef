/**
 * Filesystem View Model
 *
 * @author Ripen eCommerce
 * @package Ripen_MailFoundry
 */

(function ($) {
    $(document).ready(function($) {
        if (typeof window.MailFoundry === 'undefined') {
            window.MailFoundry = {};
        }

        /**
         * Filesystem View Model
         *
         * @param config
         * @constructor
         */
        window.MailFoundry.Filesystem = function (config) {

            var self = this;

            /**
             * View Pane Properties
             */
            config.viewPane = (typeof config.request === 'undefined') ? self.missingConfigKeys.push('config.viewPane') : config.viewPane;
            self.viewPane = {
                title: (typeof config.viewPane.title === 'undefined') ? 'MailFoundry.Fileystem' : config.viewPane.title,
                id: (typeof config.viewPane.id === 'undefined') ? self.missingConfigKeys.push('config.viewPane.id') : config.viewPane.id,
            };

            /**
             * Request Properties
             */
            config.request = (typeof config.request === 'undefined') ? self.missingConfigKeys.push('config.request') : config.request;
            self.request = {
                url: (typeof config.request.url === 'undefined') ? self.missingConfigKeys.push('config.request.url') : config.request.url,
                method: (typeof config.request.method === 'undefined') ? 'POST' : config.request.method
            };

            /**
             * Filesystem Properties
             */
            config.filesystem = (typeof config.filesystem === 'undefined') ? self.missingConfigKeys.push('config.filesystem') : config.filesystem;
            self.filesystem = {
                type: ko.observable((typeof config.filesystem.type === 'undefined') ? 'local' : config.filesystem.type),
                path: ko.observableArray((typeof config.filesystem.path === 'undefined') ? [] : config.filesystem.path),
                delimiter: (typeof config.filesystem.delimiter === 'undefined') ? '/' : config.filesystem.delimiter,
                fileListing:  ko.observableArray()
            };

            /**
             * Required Components
             */
            config.requiredComponents = (typeof config.requiredComponents === 'undefined') ? {} : config.requiredComponents;
            self.requiredComponents = {
                yml: (typeof config.requiredComponents.yml === 'undefined') ? 'campaign.yml' : config.requiredComponents.yml
            };

            /**
             * Store missing configuration keys for console error reporting
             *
             * @type {Array}
             */
            self.missingConfigKeys = [];

            /**
             * Is Sub Level
             */
            self.isSubLevel = ko.observable(false);

            /**
             * Has Components For Generation
             */
            self.hasComponentsForGeneration = ko.observable(false);

            /**
             * Pane Title
             */
            self.paneTitle = ko.observable(self.viewPane.title);

            /**
             * Environment Values
             *
             * @type {{}}
             */
            self.environment = {};

            /**
             * Visible Images
             *
             * @type {string[]}
             */
            self.visibleImages = ['jpg', 'png', 'gif'];

            /**
             *  Construct
             *
             * @param config
             * @private
             */
            self.__construct = function(config) {
                try {
                    self.validateConfig();
                    self.getEnvironment();
                    self.getFileListing();
                    self.applyFilePaneBinding();
                } catch (err) {
                    console.error(err, { "missingConfigKeys" : self.missingConfigKeys, "MailFoundry.Filesystem": self});
                }
            };

            /**
             * Get Environment Values
             */
            self.getEnvironment = function() {
                var requestParams = {
                    "env": self.filesystem.type()
                };
                $.ajax({
                    url: MAILFOUNDRY_GLOBALS.urls.environment,
                    method: self.request.method,
                    data: requestParams,
                    cache: false,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(data) {
                        self.environment = data;
                    }
                });
            };

            /**
             * Refresh To Path
             *
             * @param [pathArray]
             */
            self.refreshToPath = function (pathArray) {
                self.setPath(pathArray);
                self.getFileListing();
            };

            /**
             * Get File Listing
             */
            self.getFileListing = function() {
                var viewPaneButtons =  self.viewPane.id + ' button';
                $(viewPaneButtons).attr('disabled', true);

                var requestParams = {
                    "path": self.requestPath(),
                    "type": self.filesystem.type()
                };
                
                $.ajax({
                    url: self.request.url,
                    method: self.request.method,
                    data: requestParams,
                    cache: false,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(data) {
                        data.reverse();
                        self.filesystem.fileListing(data);
                        self.setIsSubLevel();
                        self.setPaneTitle();
                        $(viewPaneButtons).attr('disabled', false);
                    }
                });
            };

            /**
             * View File
             */
            self.viewFile = function(filepath) {
                var viewUrl = self.environment.baseUrl + filepath;
                window.open(viewUrl);
            };

            /**
             * Thumbnail Style Value
             */
            self.thumbnailStyleValue = function(fileItem) {
                if(!self.isImage(fileItem)) {
                    return false;
                }
                return 'background: url(' + self.environment.baseUrl + fileItem.path + '); display: block;';
            };

            /**
             * Is Image
             */
            self.isImage = function(fileItem) {
                if(typeof fileItem.extension === 'undefined') {
                    return false;
                }
                return (self.visibleImages.indexOf(fileItem.extension) > -1);
            };

            /**
             * Get File Item CSS
             *
             * @param fileItem
             * @return cssString
             */
            self.getFileItemCss = function(fileItem) {
                var cssString = fileItem.type;
                cssString += (typeof fileItem.extension === 'undefined') ? '' : ' ' + fileItem.extension;
                return cssString;
            };

            /**
             * Go To Child Dir
             * Change path level, and request directory contents.
             *
             * @param dirname
             */
            self.goToChildDir =  function(dirname) {
                self.addPathLevel(dirname);
                self.getFileListing();
            };


            /**
             * Go Up Parent Dir
             */
            self.goToParentDir = function() {
                try{
                    self.removePathLevel();
                    self.getFileListing();
                } catch (err) {
                    console.error(err, { "Path" : self.filesystem.path(), "MailFoundry.Filesystem": self});
                }
            };

            /**
             * Set Pane Title
             * The display title for the file pane. Changes based on directory viewed.
             *
             */
            self.setPaneTitle = function() {
                var basePaneTitle = self.viewPane.title;
                var displayDelimiter = ' ' + self.filesystemDelimiter() + ' ';
                var relativePaneTitle = (!!self.filesystem.path().length) ? displayDelimiter + self.filesystem.path().join(displayDelimiter) : '' ;
                self.paneTitle(basePaneTitle + relativePaneTitle);
            };

            /**
             * Set Is Sub Level
             * @returns {boolean}
             */
            self.setIsSubLevel = function() {
                self.isSubLevel(!!self.filesystem.path().length);
            };

            /**
             * Set Path
             *
             * @param [pathArray]
             */
            self.setPath = function(pathArray) {
                self.filesystem.path(pathArray);
            };

            /**
             * Add Path Level
             */
            self.addPathLevel = function(dirname) {
                self.filesystem.path().push(dirname);
            };

            /**
             * Remove Path Level
             *
             * @throws err
             */
            self.removePathLevel = function() {
                if(self.filesystem.path().length === 0) {
                    throw "Path array can not be popped.";
                }
                self.filesystem.path().pop();
            };

            /**
             * Request Path
             * Typically used at the filesystem level to get directory contents
             *
             * @returns {string | * | void}
             */
            self.requestPath = function() {
                return self.filesystem.path().join(self.filesystemDelimiter());
            };

            /**
             * Preview Path
             * Used for when generating a preview from a different path than the one being viewed.
             *
             * @param dirname
             * @returns {string | * | void}
             */
            self.previewPath = function(dirname) {
                var basePreviewPath = [];
                basePreviewPath.concat(self.filesystem.path());
                (dirname !== '') ? basePreviewPath.push(dirname) : false;
                return basePreviewPath.join(self.filesystemDelimiter());
            };

            /**
             * Filesystem Delimiter
             *
             * @returns {*}
             */
            self.filesystemDelimiter = function() {
                return self.filesystem.delimiter;
            };

            /**
             * Apply File Pane Binding
             * self/this bound with knockout  to view pane
             */
            self.applyFilePaneBinding = function() {
                ko.applyBindings(self, $(self.viewPane.id)[0]);
            };

            /**
             * Validate Config
             * Check if any necessary config values are missing.
             */
            self.validateConfig = function() {
                 if(self.missingConfigKeys.length > 0) throw "Missing configuration";
            };

            /**
             * Anonymous - Self invoke for PHP style construction.
             */
            (function() {
                self.__construct(config);
            })(config);
        };
    });
})(jQuery);