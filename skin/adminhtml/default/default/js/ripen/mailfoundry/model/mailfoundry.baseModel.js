/**
 * Main JS
 *
 * @author Ripen eCommerce
 * @package Ripen_MailFoundry
 */
(function ($) {
    $(document).ready(function($) {

        /**
         * MailFoundry Object
         *
         * @constructor
         */
        window.MailFoundry = function (fileSystem) {
            var self = this;

            /**
             * Upload Pane Id
             */
            self.uploadPaneId = '#upload-pane';

            /**
             * FileSystem
             *
             * Corresponding Filesystem for the upload pane
             */
            self.fileSystem = false;

            /**
             * Upload Status Message
             */
            self.uploadStatusMessage = ko.observable('');

            /**
             * Message Type
             */
            self.messageType = ko.observable('');

            /**
             * Message Display Time
             */
            self.messageDisplayTime = 10000;


            /**
             * Current Selected Campaign
             */
            self.currentSelectedCampaign = ko.observable();

            /**
             * Campaign Form Data
             * Options to populate dropdowns etc.
             *
             * @type {{campaignOptions: *, templateOptions: *, ymlData: *}}
             */

            self.campaignFormData = {
                campaignOptions: ko.observableArray(),
                templateOptions: ko.observableArray(),
                ymlData: ko.observable(),
                ymlInputs: ko.observable('')
            };

            /**
             * Messages
             *
             * @type {{newCampaign: *}}
             */
            self.messages = {
                newCampaign: ko.observable(),
                uploadFile: ko.observable(),
                loadedCampaignText: ko.observable()
            };

            /**
             * Enable Submit Flag
             */
            self.enableSubmit = ko.observable(false);

            /**
             * Enable Template Flag
             */
            self.enableTemplate = ko.observable(true);

            /**
             * Enable Yml Data Flag
             */
            self.enableYmlData = ko.observable(true);

            /**
             * Save Overlay Flag
             */
            self.saveOverlayFlag = ko.observable(false);

            /**
             * Saving Message
             */
            self.savingMessage = ko.observable('');

            /**
             * Construct
             * @private
             */
            self.__construct = function() {
                self.fileSystem = (typeof fileSystem === 'undefined') ? false : fileSystem;
                if(!self.fileSystem) {
                    console.error('No fileSystem passed to MailFoundry Base Model')
                }
                self.initCampaignGenerationForm();
                self.bindCampaignFormSubmit();
                self.applyUploadPaneBinding();
            };

            /**
             * Initialize Email Upload Form
             */
            self.initCampaignGenerationForm = function() {
                self.enableYmlData(false);
                self.campaignFormData.ymlData('Loading...');
                $.ajax({
                    type: 'GET',
                    url: MAILFOUNDRY_GLOBALS.urls.campaignformvals,
                    data: {},
                    cache: false,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(data) {

                        var filteredCampaignOptions = self.setOptions(data.campaignOptions).filter(function(e) { return e.value !== 'shared' });
                        self.campaignFormData.campaignOptions(filteredCampaignOptions);
                        self.campaignFormData.templateOptions(self.setOptions(data.templateOptions));

                        $(self.uploadForm().fields.newCampaign).val('');
                        $(self.uploadForm().fields.campaign).val(Object.keys(data.campaignOptions)[0]);
                        $(self.uploadForm().fields.template).val(Object.keys(data.templateOptions)[0]);

                        self.enableTemplate(true);
                        self.currentSelectedCampaign('new');

                        self.campaignFormData.ymlData(data.ymlTemplateData);
                        self.campaignFormData.ymlInputs(data.ymlInputs);
                        self.enableYmlData(true);
                        self.validateForm();

                    }
                });

            };

            /**
             * Bind Campaign Form Submit
             * Assemble data for ajax form submission, and status messaging
             */
            self.bindCampaignFormSubmit = function() {
                $(self.uploadForm().id).submit(function(e) {
                    var form = $(this);
                    var formKey = $(self.uploadForm().fields.formKey).val();
                    var campaign = $(self.uploadForm().fields.campaign).val();
                    var newCampaign = $(self.uploadForm().fields.newCampaign).val();
                    var template = $(self.uploadForm().fields.template).val();
                    var ymlData = $(self.uploadForm().fields.ymlData).val();
                    var assetFile = $(self.uploadForm().fields.assetFile).prop('files')[0];

                    var formData = new FormData();
                    formData.append('form_key', formKey);
                    formData.append('campaign', campaign);
                    formData.append('new_campaign', newCampaign);
                    formData.append('template', template);
                    $('.yml-inputs input').each( function() {
                        formData.append($(this).attr('name'), $(this).val());
                    });
                    formData.append('asset_file', assetFile);

                    e.preventDefault();

                    self.setSavingMessage();
                    self.saveOverlayFlag(true);
                    $('html, body').animate({ scrollTop: 0 }, 250);
                    $.ajax({
                        type: form.attr('method'),
                        url: form.attr('action'),
                        data: formData,
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function(data) {

                            self.uploadStatusMessage(self.currentCampaignName() +' has been successfully saved.');
                            self.messageType('success-msg');
                            setTimeout(function() {
                                self.uploadStatusMessage('');
                                self.messageType('');
                            }, self.messageDisplayTime);

                            self.resetFormAfterSave();
                            self.saveOverlayFlag(false);
                            self.savingMessage('');
                            var pathToDisplay = (newCampaign !== '') ? newCampaign : campaign;
                            self.fileSystem.refreshToPath([pathToDisplay]);
                        },
                        error: function(data) {
                            console.error(data);
                        }
                    });
                });
            };

            /**
             * Set Saving Message
             */
            self.setSavingMessage = function() {
                var campaign = self.currentCampaignName();
                var prefix = (self.formVal('campaign') === 'new') ? 'Creating' : 'Saving';
                self.savingMessage(prefix + ' campaign ' + campaign);
            };

            /**
             * Campaign Change
             * Callbacks to retrieve appropriate data to populate the form
             */
            self.campaignChange = function() {
                var campaign = self.formVal('campaign');
                self.campaignFormData.ymlData('Loading...');
                (campaign === 'new') ? self.initCampaignGenerationForm() : self.loadCampaign(campaign);
                self.validateUploadFileName();
            };

            /**
             * Get Current Campaign Name
             */
            self.currentCampaignName = function() {
                return (self.formVal('campaign') === 'new') ? self.formVal('newCampaign') :  self.formVal('campaign');
            };

            /**
             * Load Campaign
             * Relevant campaign data is populated into the form.
             *
             * @param campaign
             */
            self.loadCampaign = function(campaign) {
                self.enableYmlData(false);
                self.enableSubmit(false);
                self.messages.loadedCampaignText('Loading...');
                $('.yml-inputs input').each( function() {
                     $(this).val('');
                     $(this).attr('disabled', true);
                });
                var requestData = {
                    campaign: campaign
                };
                $.ajax({
                    type: 'GET',
                    url: MAILFOUNDRY_GLOBALS.urls.loadcampaign,
                    data: requestData,
                    cache: false,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(data) {
                        $(self.uploadForm().fields.template).val(data.ymlFilename);
                        self.messages.loadedCampaignText(self.loadedTemplateDisplayName(data.ymlFilename));
                        self.campaignFormData.ymlData(data.ymlData);
                        self.campaignFormData.ymlInputs(data.ymlInputs);
                        self.enableYmlData(true);
                        self.validateForm();
                    }
                });
            };


            /**
             * Loaded Template Display Name
             * Friendly display of loaded template for existing campaigns
             *
             * @param template
             * @returns {string}
             */
            self.loadedTemplateDisplayName = function(template) {
                template = template.replace(/_/g, ' ');
                return 'Saved as: ' + template.replace(/\w+/g, function(t){
                    return t.charAt(0).toUpperCase() + t.slice(1).toLowerCase()
                });
            };

            /**
             * Template Change
             * Form is set to loading status
             */
            self.templateChange = function() {
                var template = self.formVal('template');
                self.campaignFormData.ymlData('Loading...');
                self.loadYmlTemplate(template);
                self.validateUploadFileName();
            };

            /**
             * Load YML Template
             * Retireves YML template data and popluates the form.
             *
             * @param ymlTemplate
             */
            self.loadYmlTemplate = function(ymlTemplate) {
                self.enableYmlData(false);
                var requestData = {
                    ymlTemplate: ymlTemplate
                };
                $.ajax({
                    type: 'GET',
                    url: MAILFOUNDRY_GLOBALS.urls.loadymltemplate,
                    data: requestData,
                    cache: false,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(data) {
                        self.campaignFormData.ymlData(data.ymlData);
                        self.campaignFormData.ymlInputs(data.ymlInputs);
                        self.enableYmlData(true);
                        self.validateForm();
                    }
                });
            };

            /**
             * Current Selected Campaign Subscription
             */
            self.currentSelectedCampaign.subscribe(function(value) {
                self.currentSelectedCampaign(value);
                if(self.currentSelectedCampaign() !== 'new') {
                    $(self.uploadForm().fields.newCampaign).val('');
                    self.enableTemplate(false);
                } else {
                    self.enableTemplate(true);
                }
            });

            /**
             * Set Options
             * Usefule for key/value pair options to populate <select/> inputs
             *
             * @param optionsData
             * @returns {Array}
             */
            self.setOptions = function(optionsData) {
                var options = [];
                $.each(optionsData, function(key, value) {
                    options.push({
                        value: key,
                        text: value
                    });
                });
                return options;
            };

            /**
             * Existing Campaigns Index
             * Derived from options loaded into the form, for ease of indexOf() checks etc.
             *
             * @returns {Array} existingCampaignsIndex
             */
            self.existingCampaignsIndex = function() {
                var campaignOptions = self.campaignFormData.campaignOptions();
                var existingCampaignsIndex = [];
                $.each(campaignOptions, function(key, option){
                    existingCampaignsIndex.push(option.value.toUpperCase());
                });
                return existingCampaignsIndex;
            };

            /**
             * Validate Form
             * Set message and flags, enable/disable submission
             */
            self.validateForm = function() {
                var errorFound = false;
                if(!self.validateNewCampaign()){
                    errorFound = true;
                }
                if(!self.validateUploadFileName()){
                    errorFound = true;
                }
                self.enableSubmit(!errorFound);
            };

            /**
             * Validate New Campaign
             * Ensures the user is unable to submit a new campaign name that would cause conflicts
             *
             * @returns {boolean}
             */
            self.validateNewCampaign = function() {
                self.messages.newCampaign('');
                if(self.formVal('campaign') === 'new') {
                    self.sanitizeNewCampaign();
                    if (self.formVal('newCampaign') === '') {
                        self.messages.newCampaign('Please enter a unique name.');
                        return false;
                    }
                    if (self.newCampaignValueExists()) {
                        self.messages.newCampaign('This campaign already exists.');
                        return false;
                    }
                }
                return true;
            };

            /**
             * Sanitize New Campaign
             */
            self.sanitizeNewCampaign = function() {
                var sanitizedVal = self.formVal('newCampaign').toUpperCase().replace(/[^a-z\d\-]/ig, '');
                $(self.uploadForm().fields.newCampaign).val(sanitizedVal);
            };

            /**
             * Campaign Value Exists
             * When user is typing a new campaign name, check if it exists
             *
             * @returns {boolean}
             */
            self.newCampaignValueExists = function () {
                return self.existingCampaignsIndex().indexOf(self.formVal('newCampaign').toUpperCase()) > -1
            };

            /**
             * Validate Upload File Name
             */
            self.validateUploadFileName = function() {
                var assetFile = $(self.uploadForm().fields.assetFile).prop('files')[0];
                if(typeof assetFile !== 'undefined') {
                    if (!(assetFile.name.toUpperCase().indexOf('.ZIP') > -1) && (assetFile.name !== '')) {
                        self.enableSubmit(false);
                        self.messages.uploadFile('Only ZIP files are allowed');
                        return false;
                    }
                }
                self.messages.uploadFile('');
                self.enableSubmit(true);
                return true;
            };

            /**
             * Form Val
             * Gets form values based on self.uploadForm().fields keys
             *
             * @param fieldKey
             * @returns {*|jQuery}
             */
            self.formVal = function(fieldKey) {
                var fields = self.uploadForm().fields;
                return $(fields[fieldKey]).val();
            };


            /**
             * Reset Form After Save
             * Encapsulate process of resetting after save
             */
            self.resetFormAfterSave = function() {
                $(self.uploadForm().id)[0].reset();
                self.initCampaignGenerationForm();
            };

            /**
             * Upload Form Properties
             *
             * @returns {{id: string, fields: {formKey: string, assetFile: string}}}
             */
            self.uploadForm = function() {
                return {
                    id: '#generation-email-upload',
                    fields: {
                        campaign: '#campaign',
                        newCampaign: '#new-campaign',
                        template: '#template',
                        ymlData: '#yml-data',
                        formKey: '#form-key',
                        assetFile: '#asset-file'
                    },
                    alert: {
                        selectUpload: 'Please select a file to upload.'
                    }
                }
            };

            /**
             * Apply Upload Pane Binding
             * self/this bound with knockout  to view pane
             */
            self.applyUploadPaneBinding = function() {
                ko.applyBindings(self, $(self.uploadPaneId)[0]);
            };
            /**
             * Anonymous - Self invoke for PHP style construction.
             */
            (function() {
                self.__construct();
            })();
        };

    });
})(jQuery);