/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2016 Ripen eCommerce
 * @package Ripen_Dynamicoffers
 */

(function ($) {

    $.DynamicOffer = function(){};
    $.DynamicOffer.prototype = {

        paramName: 'offer',
        cookiePrefix: 'dynamicOffer_',
        offerContentContainerId: 'dynamicoffer-content',
        //offerContentContainerClass: 'dynamicoffer-popup',
        offerContentContainerClass: 'dynamicoffer-content',
        popupWidth: '60%',
        cookieDurationDays: 30,
        cookieName: '',

        /**
         * @desc Checks offer parameter in the url, checks cookie and triggers a modal window to display an offer
         */
        checkOffer: function () {
            if (this.checkURLParameter(this.paramName)) {
                var offerCode = this.getURLParameter(this.paramName);
                this.cookieName = this.cookiePrefix + offerCode;
                var dynamicOffer = this.getCookie(this.cookieName);
                //if (dynamicOffer != 1 && $.inArray(offerCode, this.validOffers) != -1) {
                if (dynamicOffer != 1) {
                    this.openPopup();
                    //don't set cookie here. Set it after form submission
                }
            }
        },

        /**
         * @desc Opens a lightbox modal window
         */
        openPopup: function () {
            $.fancybox({
                'width': this.popupWidth,
                'height': 'auto',
                href: '#' + this.offerContentContainerId,
                modal: false,
                afterLoad: function () {
                    $(".fancybox-wrap").addClass(this.offerContentContainerClass);
                    $('.fancybox-wrap').addClass('offer-detail-wrap');
                }
            });
        },

        /**
         * @desc retrieves value of a GET parameter
         * @param string paramName - the name of the parameter
         * @return string - value of the parameter
         */
        getURLParameter: function (paramName) {
            return decodeURIComponent((new RegExp('[?|&]' + paramName + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        },

        /**
         * @desc checks if requested parameter is present in the URL
         * @param string paramName - the name of the parameter
         * @return bool - true or false
         */
        checkURLParameter: function (paramName) {
            return (!new RegExp("[?&]" + paramName).test(location.search)) ? false : true;
        },

        /**
         * @desc sets a new cookie
         * @param string cname - the cookie name
         * @param string cvalue - the cookie value
         * @param string exdays - the cookie expiration
         */
        setCookie: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        },

        /**
         * @desc retrieves a specified cookie value
         * @param string cname - the cookie name
         * @return string - cookie value or empty
         */
        getCookie: function (cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        }
    };

})(jQuery);


