<?php
foreach($_GET as $typo => $value){
	if($typo != 'custom_css_files')
		echo str_replace('\\','',"$typo:$value;");
}
?>
@import "variables.less";
/*----------------------------------------------CSS DECLARATION-------------------------------------------*/

body {.changeBkg(@body_bgcolor1); .changeColor(@body_text_color6); .changeFonts(@general_font); .changeBkgImg(@body_bgimage,@body_bgfile); background-repeat:@body_bgrepeat; background-position:@body_bgposition; }
h1, .h1 {.changeFonts(@h1_font); }
h2, .h2 {.changeFonts(@h2_font); }
h3, .h3 {.changeFonts(@h3_font); }
h4, .h4 {.changeFonts(@h4_font); }
h5, .h5 {.changeFonts(@h5_font); }
h6, .h6 {.changeFonts(@h6_font); }

a { .changeColor(@body_text_color6);}
a:hover{.changeColor(@body_text_color1); text-decoration:none;}
a:active {background-color:transparent;}
ul ul,
ol ol,
ul ol,
ol ul {.changeColor(@body_text_color6);}
dt {.changeColor(@body_text_color6);}
code {.changeColor(@body_text_color6);}
blockquote {.changeColor(@body_text_color6);}
label {.changeFonts(@h6_font);}

.primary {.changeColor(@body_text_color1);}
.normal { .changeColor(@body_text_color6);}
.secondary { .changeColor(@body_text_color5);}
.secondary2 { .changeColor(@body_text_color3);}
.std .secondary { .changeColor(@body_text_color2);}
.desc .link-learn{.changeColor(@body_text_color1);}

input.input-text{.input_transition(0.2ms); .changeLine(border;1px; solid ; @body_line5); }
input.input-text,select, textarea, .validation-advice {.addBorderBox();}
.box { .changeBkg(@body_bgcolor1); .changeLine(border; 1px; solid ; @body_line5); .changeBorderRadius(@rounded_corner);}
.border {.changeLine(border;1px; solid ; @body_line5);}
.subtitle, .sub-title{.changeColor(@body_text_color6);}
.asdaspage {.changeBkg(@body_bgcolor1);}

.success-msg{ 
	.changeColor(@body_text_color2);  
	span{ 
		.changeColor(@body_text_color2); 
		a{ .changeColor(@body_text_color1);}
		a:hover{ text-decoration:underline;}
	}
}

/*========== Tabs ==========*/
.ui-tabs .ui-tabs-nav li.ui-tabs-selected a ,.ui-tabs .ui-tabs-nav li a:hover{.changeColor(@body_text_color6);}

/*========== Table ==========*/
.data-table {.changeLine(border;1px; solid ; @body_line2);}
.data-table th {.changeLine(border-bottom;1px; solid ; @body_line2); .changeBkg(@body_bgcolor1); .changeColor(@body_text_color1); text-transform:uppercase;   vertical-align:middle;}
.data-table tbody th,
.data-table tbody td {.changeLine(border-top;1px; dashed ; @body_line2); .changeColor(@body_text_color6);   border-bottom:none; }
.data-table tr.first td {border-top:none;}

input.input-text, textarea {.changeLine(border;1px; solid ; @body_line5); .changeColor(@body_text_color6); .changeFonts(@general_font); .lighterFont()}
select{.changeLine(border;1px; solid ; @body_line5); .changeColor(@body_text_color6); .changeFonts(@general_font);}

#containerDiv {.changeLine(border;1px; solid ; @body_line5);}
#containerDiv .ajaxcart{.changeBkg(@body_bgcolor1); .changeLine(border;1px; solid ; @body_line5);}
#containerDiv .ajaxcart .row2 a {.changeLine(border;1px; solid ; @button1_line); .changeColor(@button1_color); .changeBkg(@button1_bgcolor); .changeFonts(@general_font);  line-height:19px;}

.desc, .short-description {.changeColor(@body_text_color6);  font-style:normal;}
.products-list .desc{.changeColor(@body_text_color6);}

/*========== Button Quick shop ==========*/
#em_quickshop_handler {.changeLine(border;1px; solid ; @button1_line); .changeColor(@button1_color); .changeBkg(@button1_bgcolor);}

/*========== Rating ==========*/

.no-rating,.ratings,
.no-rating a,.ratings a{.changeColor(@body_text_color6);  }
.no-rating a:hover,.ratings a:hover{.changeColor(@body_text_color1); }

/*========== Product ==========*/
.price {.changeColor(@body_text_color6);}
.old-price .price{.changeColor(@body_text_color6);}
.price-from, .price-to {.changeColor(@body_text_color6);}

.add-to-links .link-wishlist,a.link-wishlist,
.add-to-links .link-compare,
.my-account .data-table tbody td.last a.link-wishlist,
.em_nav .add-to-links .link-wishlist,
.em_nav .add-to-links .link-compare{.changeColor(@body_text_color6); font-size:92%;}
.add-to-links .link-wishlist:hover, 
a.link-wishlist:hover, .add-to-links .link-compare:hover,
.my-account .data-table tbody td.last a.link-wishlist:hover,
.em_nav .add-to-links .link-wishlist:hover,
.em_nav .add-to-links .link-compare:hover {.changeColor(@body_text_color1); text-decoration:none;}


/*========== Breadcrumbs ==========*/
.breadcrumbs li a{.changeColor(@body_text_color1);  }
.breadcrumbs li a:hover{text-decoration:underline;}
.breadcrumbs li strong {.changeColor(@body_text_color1);  }

/* Form list */
.form-list label {.changeFonts(@general_font);  .changeColor(@body_text_color4);}
.form-list input.input-text {}

/* Item options */
.item-options dt {.changeColor(@body_text_color4);  }
.item-options dt:after {content:":"}

/*========== Erea Menu Horizontal ==========*/
.em_nav, .nav-container{.changeBkg(@body_bgcolor1);  }

.em_nav .hnav {.changeColor(@body_text_color6);}
.em_nav a {.changeColor(@body_text_color6);}
/*.em_nav .hnav >li.menu-item-parent:hover, 
.em_nav .hnav >li.menu-item-parent:active{.changeBkg(@button1_bgcolor);}*/
.em_nav .product-shop .product-name a:hover{.changeColor(@body_text_color1) !important;}
.em_nav h5, .em_nav .widget-title h2, .em_nav .widget-title h3 {.changeColor(@menu_drop_text_color);}
.em_nav .primary {.changeColor(@menu_drop_text_color); .changeFonts(@general_font); }
.em_nav p.text {.changeFonts(@general_font); .changeColor(@menu_drop_link_color); }
.em_nav .secondary {line-height:115%;margin-bottom:16px; .changeColor(@body_text_color4);}

/* level 2+ */
.em_nav .hnav .menu-item-link > ul,
.em_nav .hnav .em-catalog-navigation ul,
#nav li ul.shown-sub, #nav li div.shown-sub {.changeBoxShadow(inset;0;2px;6px;0;@box_shadow); .changeBkg(@menu_drop_bgcolor); .changeLine(border;1px; solid ; @menu_drop_line1);}
.em_nav .hnav .menu-item-link.menu-item-depth-0 > ul/*,
.em_nav .hnav .em-catalog-navigation li.level0 ul*/,
.em_nav .hnav .menu-item-text.menu-item-depth-0  li.level0:hover > ul,
#nav li.level-top > ul.shown-sub, #nav.level-top li > div.shown-sub {border-top:none; margin-left:-1px;}
.em_nav .menu-item-depth-1 a, .em_nav .em-catalog-navigation li li a, .em_nav code {.changeColor(@menu_drop_link_color);}
.em_nav ul.level0 a:hover,
.em_nav .hnav .menu-item-link.menu-item-depth-0 ul a:hover,
.em_nav .hnav .menu-item-depth-1 li:hover > a, 
.em_nav .hnav .menu-item-depth-1 li.active > a,
.em_nav .hnav .em-catalog-navigation li li:hover > a,
.em_nav .hnav .em-catalog-navigation li li.active > a {.changeColor(@body_text_color3) !important;}
.em_nav .hnav .menu-item-hbox .menu-item-text{.changeColor(@menu_drop_link_color);}

/* Default Menu */
.top-menu .em_nav, .top-menu .nav-container {}
.top-menu .hnav, .top-menu #nav {.header_gradient_pie(@menu_top_bgcolor);.changeLine(border;1px; solid ; @menu_top_bgcolor);  .changeBorderRadius(@rounded_corner); border-bottom:none; .changeBoxShadow("";1px;3px;5px;0;@box_shadow); position:relative;}
.top-menu .hnav:before, .top-menu #nav:before { .changeLine(border-top;1px; solid ; darken(@menu_line,12%));content:""; position:absolute; top:0; bottom:auto; left:0; right:0; height:1px;}

#nav li ul a span {}
.hnav .menu-item-link.menu-item-depth-0:hover ,
.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:hover, 
#nav > li.level-top:hover , 
#nav > li.level-top.active {.changeLine(border-top;1px; solid ; @menu_top_bgcolor); .header_gradient_opposite_pie(@menu_top_bgcolor); margin-top:-1px;}

.hnav .menu-item-depth-0,
.nav-container li.level-top{}
.hnav .menu-item-depth-0 > a,
.nav-container li.level-top a.level-top,
.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li > a {.changeFonts(@h4_font); .changeColor(@menu_top_text_color);}
.hnav .menu-item-depth-0 > a span,
.nav-container li.level-top a.level-top span,
.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li > a span {line-height:23px;}
.hnav .menu-item-depth-0.first,
.nav-container li.level-top.first {}
.hnav .menu-item-depth-0.last,
.nav-container li.level-top.last {}

.nav-container li.level-top.last a.level-top:after,
.nav-container li.level-top.last a.level-top:before,
.hnav .menu-item-depth-0.last:after,
.hnav .menu-item-depth-0.last:before {width:0; border-right:none;}
.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:after,
.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:before,
.nav-container li.level-top a.level-top:before,
.nav-container li.level-top a.level-top:after,
.hnav .menu-item-depth-0:before,
.hnav .menu-item-depth-0:after {content:""; position:absolute; top:0; bottom:0; right:0; width:1px; .menu_gradient(@menu_line; @menu_top_bgcolor); border-color:darken(@menu_line,14%); }

/* Nav left */
.vnav.nav-left, .vnav.nav-right, .col-right .vnav, .col-left .vnav {
	.changeBoxShadow("";1px;3px;5px;0;@box_shadow); 
	.changeLine(border;1px; solid ; @menu_drop_line1); z-index:999;.changeBorderRadius(@rounded_corner);
	.menu-item-depth-0 > .menu-container,
	.em-catalog-navigation li > ul,
	.menu-item-depth-0 .menu-item-link:hover > .menu-container  {.changeLine(border;1px; solid ; @menu_drop_line1);top:-16px;.changeBkg(@body_bgcolor1);}
	.menu-item-link.menu-item-depth-0:hover > .menu-container {padding:15px 0;}
	.menu-item-depth-0 a:hover span {.changeColor(@body_text_color3) !important;}
	
}
.col-right .vnav, .col-left .vnav {
	.changeLine(border;1px; solid ; @body_line1); .changeBoxShadow("";1px;3px;5px;0;@box_shadow); z-index:999; .changeBorderRadius(@rounded_corner); .addBorderBox();
	.menu-item-depth-0 > .menu-container,
	.em-catalog-navigation li > ul,
	.menu-item-depth-0 .menu-item-link:hover > .menu-container  {.changeLine(border;1px; solid ; @body_line1) !important; top:-16px;.changeBkg(@body_bgcolor1); }
	.menu-item-link.menu-item-depth-0:hover > .menu-container {padding:15px 0;}
	.menu-item-depth-0 a:hover span {.changeColor(@body_text_color3) !important;}
}

.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li > a.arrow:after,
.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li > a.arrow:before,
.hnav .menu-item-depth-0:hover > a.arrow:before,
.hnav .menu-item-depth-0:hover > a.arrow:after {width:0; background:none !important; border:none !important;}

#nav > li.level-top:hover a.level-top:after,
.hnav .menu-item-depth-0.menu-item-parent:hover > a:after ,
.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:hover  > a:after {content:""; position:absolute; top:auto; bottom:-6px; left:46%; right:auto; border-left:6px solid transparent; .changeLine(border-top;6px; solid ; lighten(@menu_top_bgcolor,8%)); border-right:6px solid transparent; background:none; filter:none; -ms-filter:none; z-index:1001;}

/* Menu left */
.menuleftText {
	.header_gradient_pie(@menu_left_bgcolor); cursor:pointer; .changeLine(border;1px; solid ; @menu_left_bgcolor); border-bottom:none;
	span {text-transform:uppercase; .changeFonts(@h3_font); .changeColor(@menu_top_text_color);}
}
.menuleftText:after {content:""; position:absolute; top:0; bottom:auto; left:0; right:0; height:1px; .changeLine(border-top;1px; solid ; lighten(@menu_left_bgcolor,20%)); }
	
.category-left {
	.changeBoxShadow("";1px;3px;5px;0;@box_shadow);
	.mega-menu {
		.changeBoxShadow("";1px;3px;5px;0;@box_shadow);
		position:absolute; z-index:101; width:100%; top:39px; left:0;
		.em_nav {
			.changeLine(border;1px; solid ; @menu_drop_line2); border-top:none; 
			.changeBkg(@menu_left_content_bgcolor); padding-top:4px;
		}
	}
}

.cms-index-index .category-left {
	.changeBoxShadow("";1px;0;3px;1px;@box_shadow); cursor:default;
	.mega-menu{position:static; width:auto;}
}

.category-left 
.category-left .em_nav {background:none;margin-bottom:12px;}
.category-left .vnav {
	margin-bottom:9px;
	.menu-item-depth-0 {margin:0 0 2px; padding:0;}
	.menu-item-depth-0  > a {/*margin:0 14px /* !important*/; }
	/*.menu-item-depth-0:first-child > a {border-top:none; padding-top:3px;}*/
	.menu-item-depth-0 > a,  .menu-item-depth-0 > .em-catalog-navigation  .level0 > a{
		padding:5px 30px 5px 15px; margin:0 !important;
		span { .changeColor(@menu_left_text_color);}
	}
	.menu-item-depth-0:hover > a, .menu-item-depth-0 > .em-catalog-navigation  .level0:hover > a {
		.changeBkg(@menu_left_bgcolor); text-decoration:none;
		span{.changeColor(@menu_active_text_color); }
	}
	.menu-item-depth-0 > .menu-container {padding:15px 0 15px 15px; top:-15px; box-shadow:none;}
	.menu-item-depth-0 > .menu-container > li{margin-left:0;}
	.menu-item-depth-1 a { padding:3px 0 3px 0; margin-right:30px !important;}
	.menu-item-depth-1 li:hover > a { text-decoration:underline;}
	.menu-item-depth-1 li.menu-item-link:hover > ul {top:-15px;}
	/*.menu-item-depth-1 > a {
		padding:3px 14px;
		span{ .changeColor(@body_text_color6);}
	}*/
	.menu-item-hbox .menu-item-text, code {.changeColor(@menu_active_text_color);}
	li.menu-item-link:hover > ul,
	.em-catalog-navigation  li:hover > ul {.changeBkg(@menu_left_bgcolor); left:100%; top:0; box-shadow:none; padding:15px 0 15px 15px;}
	.em-catalog-navigation  li:hover > ul {top:-15px; box-shadow:none; padding:15px 0 15px 15px;}
	.menu-item-depth-0 > .em-catalog-navigation  .level0:hover > ul {top:0;}
	.em-catalog-navigation  li:hover > a {text-decoration:underline;.changeColor(@menu_active_text_color) !important; }
	.em-catalog-navigation  li:hover > a.arrow {text-decoration:none;}
	ul li a{.changeColor(@menu_active_text_color); }

}

/* Product label */
/*.productlabels_icons .label{.changeBkg(@body_bgcolor1);}
*/
.productlabels_icons .label {.changeLine(border;5px; solid ; @body_bgcolor1;)}
.productlabels_icons .label p{.changeColor(@body_text_color5); .changeFonts(@h4_font); padding:14px 5px; text-transform:uppercase; .changeBoxShadow(inset;1px;1px;2px;0;darken(@box_shadow, 40%));}
.productlabels_icons .label.sale{.changeBkg(#7f7fbe);}
.productlabels_icons .label.new{.changeBkg(@body_bgcolor3);}
.productlabels_icons .label.hot{.changeBkg(lighten(@body_bgcolor2, 6%));}



/* $Header */
.container-header-top, .header-container {.changeBkg(@header_bgcolor1); .changeBkgImg(@header_bgimage,@header_bgfile); background-repeat:@header_bgrepeat; background-position:@header_bgposition;}
.quick-access {
	.links li {
		.changeLine(border-left;1px; solid ; @header_line2); 
		a{.changeColor(@header_text_color1);  white-space:nowrap;}
		a:hover{.changeColor(@header_text_color2);}
	}
	.links li:first-child {border-left:none;}
	.welcome-msg {.changeColor(@header_text_color1); }
}
.dropdown-cart {
	margin-left:15px; .transition(400ms);
	.dropdown-cart-content{.changeLine(border;1px; solid ; @button1_line); float:right;}
	.dropdown-cart-content-top {.changeLine(border-top;1px; solid ; lighten(@button1_line, 17%)); padding:5px 0 5px 7px; .header_gradient_pie(@button1_bgcolor); .transition(0.5ms);}
	&:hover .dropdown-cart-content-top:after {content:""; position:absolute; top:auto; bottom:-7px; left:48%; z-index:999; .changeLine(border-top;6px; solid ; @button1_bgcolor);.changeLine(border-left;6px; solid ; transparent);.changeLine(border-right;6px; solid ; transparent); border-bottom:none;}
	p.amount {
		margin-bottom:0;
		a {.changeColor(@button1_color); .changeFonts(@h5_font); font-size:100%;}	
	}
}

.form-search {
	button.button {
		.changeBkg(@header_bgcolor2); filter:none; -ms-filter:none; background-image:none;.changeBorderRadius(0); 
		&:hover {background-image:none; .changeBkg(@header_bgcolor2); filter:none; -ms-filter:none; }
		span span{.changeLine(border-left;1px; solid ; @body_line5); }
	}	
	.input_search {.changeLine(border;1px; solid ; @body_line5);}
}


/* Form Search */
.header .form-search{.transition(0.2ms);}
.header .form-search input.input-text{.changeColor(@header_text_color1);}
.search-autocomplete{ 
	.changeBkg(@header_bgcolor1);
	li{.changeLine(border;1px; solid ; @body_line5); border-top:none; }
	li.first{.changeLine(border-top;1px; solid ; @body_line5);}
}

/*========== Dropdown Cart ==========*/
.block-cart-top .block-title {
	a{.changeColor(@header_text_color1);  .changeLine(border;1px; solid ; @header_line1); .changeBkg(@header_bgcolor1);}
	.summary {
		.changeLine(border;1px; solid ; @header_line1);
		span{.changeColor(@body_text_color6);}
	} 
}
.block-cart-top .block-content .cart-popup {.changeBoxShadow("";1px;2px;3px;1px;lighten(@box_shadow, 5%));  }
.block-cart-top .block-content .cart-popup .cart-popup-content {.changeBkg(@body_bgcolor1); .changeLine(border;1px; solid ; @header_line1);.transition(0.5ms);}
.block.block-cart-top .actions{
	.changeLine(border-bottom; 1px ; dashed ; @header_line1); text-align:left;
	.subtotal {
		margin-bottom:14px; text-align:left;
		span.label { .changeColor(@header_text_color1); }
	}
	.paypal-or { display: inline-block; margin: 12px 0 0 10px;}
}
.block.block-cart-top .block-subtitle {
	 margin-bottom:8px;.changeColor(@header_text_color1); 
	a:hover { .changeColor(@header_text_color1); };
	a {.changeColor(@header_text_color2)}
}
.block.block-cart-top .cart-subtitle { text-transform:uppercase; .changeColor(@header_text_color2)}
.block.block-cart-top #cart-sidebar {
	li.item {position:relative; .changeLine(border-bottom;1px; dashed ; @header_line3); padding-bottom:14px;}
	li.item.last {padding-bottom:0; border-bottom:none;}
	/*.product-details {overflow:hidden;}*/
	.product-name {margin-top:-2px; padding-right:20px;}
	.product-image {float:left;.changeLine(border;1px; solid ; @body_line4); .changeBorderRadius(@rounded_corner); margin-right:9px; }
	/*.product-image:hover {.changeLine(border;1px; solid ; @body_line1);}*/
	.product-details {}
}

.form-language .block-title strong, .block-currency .title-currency {.changeColor(@header_text_color1); }

/* Widget */
.em_main .widget-title {margin-bottom:15px; padding-top:0;}
.em_main .widget-title h2,.em_main .widget-title h3,
.em_main .widget-title h2 span, .em_main .widget-title h3 span{.changeFonts(@h3_font); .changeColor(@body_text_color1); text-transform:uppercase;}

/*========== Sidebar Left & Right Column Area ==========*/
.sidebar .widget-title h2,.sidebar .widget-title h3 {text-transform:uppercase; .changeFonts(@h3_font);}

.sidebar .block {.changeLine(border;1px; solid ; @body_line1); .changeBorderRadius(@rounded_corner); .changeBkg(@body_bgcolor1); margin-bottom:20px;}
.sidebar.col-left .block {.changeBoxShadow("";1px;2px;3px;1px;lighten(@box_shadow, 5%));}
.sidebar.col-right .block {.changeBoxShadow("";-1px;-2px;3px;1px;lighten(@box_shadow, 5%));}
.sidebar .block .block-title {
	.body_gradient(@body_bgcolor2);.changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); padding:4px 18px 7px; 
	strong span {text-transform:capitalize; .changeFonts(@h4_font); .changeColor(@body_text_color5); }
}
.sidebar .block .block-content {margin:0 18px;  }
.sidebar .block .block-subtitle {.changeColor(@body_text_color4); }
.sidebar .block .actions a{ .changeColor(@body_text_color3); font-size:92%; float:left;}
.sidebar .block .actions a:hover{.changeColor(@body_text_color1);}

/* Recent views block */
.grid_24.recent_views_fixed .recently_viewed > div{
	.changeBorderRadius(@rounded_corner); .changeBkg(@body_bgcolor1); .changeLine(border;1px; solid ; @body_line5); border-top:none; clear:both; 
	.desc {.body_gradient(@body_bgcolor2);.changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); padding:4px 3px 7px; .changeFonts(@h4_font); .changeColor(@body_text_color5); font-size:100%; text-align:center; .changeLine(border;1px; solid ; @body_line1); margin-left:-1px; margin-right:-1px;}
	.widget-products {
		margin:0; padding:4px 4px 0;  text-align:center; 
		ul {margin-bottom:3px;}
		ul.last {margin-bottom:0;}
		li.item {margin-bottom:3px;}
		.product-image {margin-bottom:0; display:inline; position:static;}
		.product-name a{font-size:84%;}
	}
}

/* Menu Left & Right */
.sidebar .menu-title {
	.body_gradient(@body_bgcolor2);.changeLine(border;1px;solid;@body_bgcolor2); padding:4px 18px 7px; position:relative;
	&:after {.changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); bottom:auto;content:"";left:0;position:absolute;right:0;top:0;}
	a {text-transform:capitalize; .changeFonts(@h4_font); .changeColor(@body_text_color5); }
	span.option {text-indent:-999em; text-align:left; display:inline-block; width:7px; height:4px; position:absolute; right:15px; top:15px;}
}

.sidebar .menu-item-link > .menu-container{.changeBkg(@body_bgcolor1);}


.mini-products-list .product-image {float:left;.changeLine(border;1px; solid ; @body_line4); }
/*.mini-products-list .product-image:hover {.changeLine(border;1px; solid ; @body_line1);}*/

.block-layered-nav {
	dt {.changeColor(@body_text_color1);  text-transform:uppercase; padding-right:12px;}
	dd {.changeLine( border-bottom; 1px; dashed ; @body_line2); padding-bottom:5px; margin-bottom:16px;}
	.label, .price{.changeColor(@body_text_color6);  }
	a:hover > .price {.changeColor(@body_text_color1);}
}
.block-poll .label{.changeColor(@body_text_color6);}
.block-progress .block-title{.changeColor(@body_text_color6);}
.block-progress dl {
	dt a{.changeColor(@body_text_color6);}
	dd .price{.changeColor(@body_text_color6);}
}
.block-account ul li.current{.changeColor(@body_text_color6);}

.block-list  {
	li.item {position:relative; .changeLine(border-bottom;1px; dashed ; @body_line2); }
	li.item.last {border-bottom:none; margin-bottom:10px;}
	a.product-image {float:left;.changeLine(border;1px; solid ; @body_line4); }
	/*a.product-image:hover {.changeLine(border;1px; solid ; @body_line1);}*/
	.price-box {margin-bottom:0;};
}

/* Lastest review */
.widget-recentreview-products {
	.changeLine(border;1px; solid ; @body_line1); .changeBkg(@body_bgcolor1); margin-bottom:20px; display:block;.changeBoxShadow("";1px;2px;3px;1px;lighten(@box_shadow, 5%));.changeBorderRadius(@rounded_corner); 
	.widget-title {
		.body_gradient(@body_bgcolor2);.changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); padding:4px 18px 7px; 
		h2, h3 {
			margin:0;
			span {text-transform:capitalize; .changeFonts(@h4_font); .changeColor(@body_text_color5); }
		}
	}
	.widget-products {margin:0 18px;  padding:15px 0;}
	.review-product-detail {.changeColor(@body_text_color3); }
}

.block-gr-search .block-title h2 {.changeColor(@body_text_color5);}

.sidebar .box-reviews {
	.changeLine(border;1px; solid ; @body_line1); .changeBkg(@body_bgcolor1); margin-bottom:20px; display:block;.changeBoxShadow("";1px;2px;3px;1px;lighten(@box_shadow, 5%));.changeBorderRadius(@rounded_corner); 
	h2 {
		.body_gradient(@body_bgcolor2); border:none; .changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); padding:4px 18px 7px; margin-bottom:0;
		&:after {width:0; display:none; border:none;}
		span {text-transform:capitalize; .changeFonts(@h4_font); .changeColor(@body_text_color5); }
	}
	ul{margin:0 18px;  padding:15px 0 0;}
	.review-product-detail {.changeColor(@body_text_color3); }
}


.sidebar .block-related {float:none !important; display:block;}
.block.block-related {
	li.item {position:relative; z-index:0; .changeLine(border-bottom;1px; dashed ; @body_line2); padding-bottom:10px;}
	li.item.last {border-bottom:none; margin-bottom:0;}
	.price-box {margin-bottom:0;};
}

.product-shop .block-related {
	.changeLine(border;1px; solid ; @body_line1); .changeBorderRadius(@rounded_corner); .changeBkg(@body_bgcolor1); margin-bottom:20px;.changeBoxShadow("";-1px;-2px;3px;1px;lighten(@box_shadow, 5%));float:right;
	.block-title {
		.body_gradient(@body_bgcolor2);.changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); padding:4px 18px 7px; 
		strong span {text-transform:capitalize; .changeFonts(@h4_font); .changeColor(@body_text_color5); }
	}
	.block-content {margin:0 18px;  }
	.block-subtitle {.changeColor(@body_text_color4); }
	.actions a{ .changeColor(@body_text_color3); font-size:92%; float:left;}
	.actions a:hover{.changeColor(@body_text_color1);}
}

/*========== My Account ==========*/


.box-head {
	.body_gradient(@body_bgcolor2); .changeLine(border;1px; solid ; @body_bgcolor2); position:relative; .changeBorderRadius(@rounded_corner); border-bottom:none; overflow:hidden;margin-bottom:24px; 
	&:after {content:""; position:absolute; height:1px; .changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); top:0; bottom:auto; left:0; right:0;}
	h2 {.changeColor(@body_text_color5); text-transform:uppercase; .changeFonts(@h4_font); padding:5px 15px 7px; margin:0;}
}

.box-info .col2-set.adress .box > .box-title {
	.body_gradient(@body_bgcolor2); .changeLine(border;1px; solid ; @body_bgcolor2); position:relative; .changeBorderRadius(@rounded_corner); border-bottom:none; overflow:hidden;margin-bottom:24px;
	&:after {content:""; position:absolute; height:1px; .changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); top:0; bottom:auto; left:0; right:0;}
	h3 {.changeColor(@body_text_color5); text-transform:uppercase; .changeFonts(@h4_font); padding:5px 15px 7px; margin:0;}
}
.box-info .col2-set.adress .box-content a{.changeColor(@body_text_color3); font-size:92%; }

.my-account .box-account .btn-view-all {
	.body_gradient_opposite(@button1_bgcolor); margin-top:-1px !important;position:relative;
	> span {display:block; .changeLine(border-top;1px; solid ; @button1_bgcolor); padding:9px 10px 9px 34px;}
	&:hover {display:block; .body_gradient(@button1_bgcolor); text-decoration:none;}
	&:hover > span {.changeLine(border-top;1px; solid ; lighten(@button1_line,16%)); }
	span span {padding:0; .changeFonts(@general_font); .changeColor(@button1_color); }
	&:after, &:before {content:"" !important;}
	&:before {position:absolute; top:0; bottom:0; left:0; right:auto; width:1px; .menu_gradient(lighten(@button1_bgcolor,20%); lighten(@button1_bgcolor,12%));}
}

.title-buttons button.button {
	.body_gradient_opposite(@button1_bgcolor); position:relative; z-index:99; margin-top:-1px !important;
	> span {display:block; .changeLine(border-top;1px; solid ; @button1_bgcolor); padding:6px 10px 1px 34px;}
	&:hover {display:block; .body_gradient(@button1_bgcolor); text-decoration:none;}
	&:hover > span {.changeLine(border-top;1px; solid ; lighten(@button1_line,16%)); }
	span span {padding:0; .changeFonts(@general_font); .changeColor(@button1_color); }
	&:before {position:absolute; top:0; bottom:0; left:0; right:auto; width:1px; .menu_gradient(lighten(@button1_bgcolor,22%); lighten(@button1_bgcolor,16%);)}
}
/*.box-head {.changeBkg(@body_bgcolor1);}
.box-head h2,.box-head a{.changeColor(@body_text_color6);}*/

.dashboard .welcome-msg > p {}
.dashboard .welcome-msg > p.select {.changeColor(@body_text_color3);}

.my-account .data-table {
	tbody td.last a{.changeColor(@body_text_color1); font-size:92%; font-weight:normal;}
	tbody td.last a:hover{text-decoration:underline;}
	tbody td em{.changeColor(@body_text_color6);}	
	.separator {}
}

.my-account .box-title {
	h3 {.changeFonts(@general_font); .changeColor(@body_text_color1); text-transform:uppercase; }
	a{.changeColor(@body_text_color3); font-size:92%; }
	a:hover{text-decoration:underline;}
	a:after {content:"]"}
	a:before {content:"["}

}

.my-account .box-content h4 {.changeFonts(@general_font); .changeColor(@body_text_color1); text-transform:uppercase; }

.box-content a{.changeColor(@body_text_color6);}
.box-tags .tags a{.changeColor(@body_text_color6);}
.box-tags .tags a:hover{.changeColor(@body_text_color1);}
.addresses-list h2,.addresses-list h3{.changeColor(@body_text_color6);}
.addresses-list a{.changeColor(@body_text_color6);}

#my-orders-table {
	.changeLine(border;1px; solid ; @body_line2);
	.border {border:none;.changeLine(border-top;1px; dashed ; @body_line2);}
	th, td {padding:11px 13px;}
	td .price,tfoot td .price{.changeColor(@body_text_color4);}
	.link-reorder {.changeColor(@body_text_color3); margin-top:0;}
	tfoot td {border:none}
	tfoot tr.first td {.changeLine(border-top;1px; dashed ; @body_line2);}
}

.customer-account-forgotpassword .em_main #form-validate .fieldset .legend{.changeColor(@body_text_color2);}

/* @@ Stone */
/*================= Widgets, Blocks ==============*/
.widget-static-block h2, .widget-title h2, .widget-title h3, .today_sale h3 {.changeFonts(@h3_font); margin-bottom:7px; }

/*------------- Slideshow Area ------------------*/
/* slideshow */
.rev_slider_wrapper.fullwidthbanner-container {.addBorderBox(); /* IE will get error*/z-index:20;}
.tp-bullets.simplebullets.round .bullet {width:18px; height:18px;}
.tp-caption a:hover {.changeColor(@button1_color);}


/* Best seller & Today sale */
.best-seller { margin-top:17px; margin-bottom:18px;}
.today_sale {margin-top:17px; margin-bottom:8px;}
.best-seller {
	margin-left:0 !important;
	.widget-title h3 span {.changeColor(@body_text_color2);}
	.widget-products {
		.changeLine(border;1px; solid ; @body_line2);.changeBorderRadius(@rounded_corner);
		.changeBorderRadius(@rounded_corner);
		.product-image{
			margin:0 0 0 10px; overflow:hidden; 
			img {border:none;}
			&:hover{border:none;}
		}
		.products-grid {
			margin:0;
			&.even {.changeLine( border-bottom; 1px ; dashed ; @body_line2);}
			/*&.first.last {border-top:none;}*/
			li.item{
				.changeLine( border-left; 1px ; dashed ; @body_line2); margin:0; padding:14px 15px; .addBorderBox();
				.product-shop {margin-right:0;overflow:hidden; margin-top:4px; text-align:left; min-width:100px;}
				.product-name {margin-bottom:8px;}
				.product-name a{.changeFonts(@general_font); text-transform:uppercase; font-size:110%; }
				.product-shop button.btn-cart {margin:0;}
				.product-image{float:right;}
				.add-to-links li{margin-left:0;}
				/*.actions-cart {position:static; display:block !important;}*/
			}
			li.item:first-child{border-left:none;}			
		}
		.products-list {
			li.item { .addBorderBox(); .changeLine( border-top; 1px ; dashed ; @body_line2); }
			li.item:first-child{border-top:none;}	
			.product-shop {margin-left:82px;overflow:hidden; margin-top:4px; text-align:left; clear:none;}
		}
		
	}
}

.today_sale {
	margin-right:0 !important;
	.block_title h3 span {.changeColor(@body_text_color1); .changeFonts(@h3_font) }
}

.slideshow_area {position:relative; z-index:1;}
/*.slideshow_area .shopping-feature {
	.changeBkg(lighten(@body_bgcolor3, 28%));
	> div {.changeLine(border;1px; dashed ; @body_line3);.changeBorderRadius(@rounded_corner); display:block; overflow:hidden;}
	.products-grid {
		margin:0;
		li.item {
			margin:0; padding:3px; float:none;	
			.product-shop {text-align:left;}
			.product-shop button.btn-cart {margin:15px 0 0;display:block;}
		}
		a{.changeFonts(@general_font); text-transform:uppercase; font-size:110%; }
		.product-image {
			width:50%;
			img{ border:none; width:100%; height:auto;}
		}	
		.product-image:hover img{border:none}	
	}
}
.slideshow_area .feature-right {
	.products-grid {
		margin:0;.changeLine(border;1px; dashed ; @body_line1);.changeBorderRadius(@rounded_corner);
		li.item {
			margin:0; padding:10px; .changeBkg(darken(@body_bgcolor1, 5%);); text-align:center; .addBorderBox();
			.product-shop, .product-name {text-align:center}
			.price-box {margin-bottom:3px;}
			.product-image {
				width:100%;
				img{ border:none; width:100%; height:auto;}
			}
			.product-image:hover img {border:none}
		}
	}
}*/

.four-banner {margin-bottom:25px; display:block; }

/* */
.em_fashion, .em_shoes_bags, .em_accessories, .em_beauty_health, .em_mobiles {
	display:block; margin-bottom:5px;
	.block_title {
		.changeLine(border-bottom;1px; dashed ; @body_line2); margin-bottom:20px; .changeColor(@body_text_color2); 
		h3 {margin-bottom:6px;}
	}
 
}

.std .slider_container .widget-title {
	display:block; .changeLine(border-bottom;1px; dashed ; @body_line2); margin-bottom:42px; .changeColor(@body_text_color2); 
	h2 {margin-bottom:6px;}
}
.std .slider_container .jcarousel-skin-tango .jcarousel-next-vertical,
.std .slider_container .jcarousel-skin-tango .jcarousel-prev-vertical,
.std .slider_container .jcarousel-skin-tango .jcarousel-next-horizontal,
.std .slider_container .jcarousel-skin-tango .jcarousel-prev-horizontal {top:-32px;}

/* Footer */
.container-footer {.changeBkg(@footer_bgcolor1); .changeBkgImg(@footer_bgimage,@footer_bgfile); background-repeat:@footer_bgrepeat; background-position:@footer_bgposition; }
.bottom_slider_link {.changeLine(border-bottom;1px; dashed ; @footer_line1); padding:12px 0 2px; margin-bottom:12px; .changeColor(@footer_text_color3);}

.bottom_slider_link, .container-footer {
	p.title, .block-title strong span {.changeFonts(@h3_font); .changeColor(@footer_text_color1); margin-bottom:8px;}
}
.container-footer { .changeColor(@footer_text_color3);}

.brand_slider {
	.changeLine(border-bottom;1px; dashed ; @footer_line1); padding-bottom:30px; margin-bottom:13px;
	> .widget-static-block {.changeLine(border;1px; solid ; @footer_line2); padding:21px 30px;}
}

.category_link_ad {
	margin-top:7px;
	.content {.changeLine(border; 1px; solid ; @footer_line2); .changeBorderRadius(@rounded_corner);}
	ul {overflow:hidden; margin:0;}
	ul:first-child {.changeLine(border-bottom; 1px; dashed ; lighten(@footer_line2,10%)); }
	li {.changeLine(border-left;1px; dashed ; @footer_line2); }
	li:first-child {border-left:none; }
	p.title {.changeFonts(@general_font); .changeColor(@footer_text_color2); margin-bottom:0; text-transform:uppercase; }
	.icon {float:left; margin-right:11px; margin-top:5px;}
	p {margin-left:48px; margin-bottom:0; font-size:92%;}
}

.category_link_info {
	p { margin-bottom:3px;}
	p span {.changeColor(@footer_text_color2); }
	p span:first-child {margin-right:10px; margin-top:-2px;}
}

.container-footer .footer-links{
	.changeLine(border;1px; solid ; @footer_bgcolor2); overflow:hidden; .changeBorderRadius(@rounded_corner);
	display:block; margin-bottom:27px; border-bottom:none; 
	ul {
		.header_gradient(@footer_bgcolor2); position:relative;
		.changeLine(border-top;1px; solid ; lighten(@footer_bgcolor2,20%));
		margin-bottom:0; text-align:center; overflow:hidden; padding:5px 0;
		li {
			display:inline-block; padding:0 20px; margin:5px 0; .changeLine(border-left;1px; solid ;@footer_link_color); 
			a {.changeFonts(@general_font); .changeColor(@footer_link_color); font-size:110%;}
			a:hover {text-decoration:underline;}
			&:first-child {border-left:none;}
		}
	}
}

.footer-sample {
	a {margin-bottom:10px; display:block;}
	p { .changeColor(lighten(@footer_text_color3,8%)); line-height:140%;}
}

.footer-container .footer, .footer-container .footer a {.changeColor(lighten(@footer_text_color3,8%)); }
.footer-container .footer a:hover {.changeColor(@body_text_color1);}

/* Slider */
.jcarousel-skin-tango .jcarousel-next,
.jcarousel-skin-tango .jcarousel-prev {.changeLine(border;1px; solid ; @body_line2);}

/* Button */

/*button.button, a.btn-cart, a.em_quickshop_handler {.transition(0.2ms);}*/
#em_quickshop_handler,button.button,
.block-subscribe button.button,
.block .actions button.button, #review-form button.button, .buttons-set button.button, 
.box-tags button.button, .buttons-set button.button, .cart-table tfoot td button.btn-empty, 
.cart-table tfoot td button.btn-continue{.changeButtonStyle(@button1_bgcolor; @button1_line; @button1_color); .changeBorderRadius(@rounded_corner); height:28px;}



.checkout-types button.btn-checkout , .opc-col3 button.btn-checkout {.changeButtonStyle(@button2_bgcolor; @button2_line; @button2_color);.changeBorderRadius(@rounded_corner); height:44px;}
.cart-table tfoot td button.btn-update , #checkout-step-login .col-2 .buttons-set button.button , .registered-users .buttons-set button.button{.changeButtonStyle(@button2_bgcolor; @button2_line; @button2_color);.changeBorderRadius(@rounded_corner); height:28px;}
#em_quickshop_handler > span {padding:0 10px 0 11px;}
#em_quickshop_handler span span{line-height:24px;}
.cart-table tfoot td button.btn-update > span, 
.checkout-types button.btn-checkout > span, .opc-col3 button.btn-checkout > span{padding:0 10px 0;}

a.btn-cart {.changeButtonStyle_pie(@button2_bgcolor; @button2_line; @button2_color); position:relative; filter:none; -ms-filter:none; .changeBorderRadius(@rounded_corner); font-size:100%;}
button.btn-cart {.changeButtonStyle_pie(@button2_bgcolor; @button2_line; @button2_color); position:relative; filter:none; -ms-filter:none; .changeBorderRadius(@rounded_corner); height:28px;}
a.btn-cart:hover, button.btn-cart:hover {filter:none; -ms-filter:none;}

.paypal-express-review #review-buttons-container.buttons-set .btn-checkout.no-checkout {
	&:hover {.header_gradient(@button1_bgcolor);}
	&:hover > span {.changeLine(border-top;1px; solid ; lighten(@button1_line,16%));}
}
	

.buttons-set p.back-link {.changeBacklinkstyle(@button2_bgcolor; @button2_line; @button2_color);.changeBorderRadius(@rounded_corner); }

a.btn-cart span  {padding:3px 10px 6px;}
a.btn-cart span span {padding:0; .changeFonts(@general_font);  font-size:150%;}


.title-buttons {
	.link-reorder, .link-order-print, .separator {
		.changeFonts(@general_font);  .changeColor(@body_text_color5);
		&:hover {text-decoration:underline;}
	}
}

/* Category */
.page-title, .blog-category-view .page-title.category-title {
	 z-index:1; .body_gradient(@body_bgcolor2); .changeLine(border;1px; solid ; @body_bgcolor2); border-bottom:none; position:relative; overflow:hidden;.changeBorderRadius(@rounded_corner);
	&:after {content:""; position:absolute; height:1px; .changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); top:0; bottom:auto; left:0; right:0;}
	h1 {.changeColor(@body_text_color5); text-transform:uppercase; .changeFonts(@h4_font); padding:6px 15px 7px; }
}

.page-title.category-title {
	border:none; background:none; filter:none; -ms-filter:none; margin:0;
	&:after {border:none; height:0;}
	h1 {.changeColor(@body_text_color3); border:none; padding:6px 15px 5px 0;}
}

.product-name h1 {}
.product-name , .product-name a {.changeFonts(@general_font); .changeColor(@body_text_color1) !important;}
.product-name a:hover {.changeColor(@body_text_color3) !important;}

.products-grid .product-image > img, .products-list .product-image  > img{.changeLine(border;1px; solid ; @body_line4); .changeBorderRadius(@rounded_corner);
-webkit-transition:opacity 1s ease-in-out;
  -moz-transition:opacity 1s ease-in-out;
  -o-transition:opacity 1s ease-in-out;
  transition:opacity 1s ease-in-out;}
  
.category-products .products-grid li.item .product-item .product-image > img { .addBorderBox();}

.price,.price-box .price,.regular-price .price,.special-price .price{.changeFonts(@h6_font); .changeColor(@body_text_color3); }
.old-price .price{.changeFonts(@general_font); .changeColor(@body_text_color6);}
.minimal-price-link .label { .changeColor(@body_text_color6);}


/* Product detail */
.product-view .product-name h1, .product-view h2, .product-review .product-name {.changeColor(@body_text_color1); .changeFonts(@h2_font);}
.product-view .sku {.changeColor(@body_text_color4);}
.product-view .cloud-zoom-big {.changeLine(border;1px; solid ; #ccc);}
.product-view .cloud-zoom-title {background:none repeat scroll 0 0 #FFFFFF;color:#111111;padding:5px;}
.product-view .cloud-zoom-lens {background-color:#FFFFFF;border:0 none;margin:0;}
.availability {
	.changeColor(@body_text_color4); 
	span {.changeColor(@body_text_color1);}
}
.product-options .options-list .label {.changeColor(@body_text_color4);}
.product-options .options-list .label label{font-size:100%;}
.review-by span {.changeColor(@body_text_color4);}

/* Product options */
.product-options {
	.changeLine(border;1px; solid ; @body_line5;); padding:10px;
	dt label,
	dd .input-box,
	.qty-holder label {.changeColor(@body_text_color4); .changeFonts(@general_font); }
}
.product-options-bottom {.changeLine(border;1px; solid ; @body_line5;); border-top:none;}
.product-view .product-shop .data-table th,.product-view .product-shop .data-table td {.changeColor(@body_text_color4); }

.product-view {
	.short-description h2{.changeLine(border-top;1px; dashed ; @body_line2); padding-top:13px; }
	.price-box {
		margin:12px 0 5px;
		.label {.changeColor(@body_text_color4);}
	}
	.price-label {}
	.product-essential .price-label {.changeColor(@body_text_color3);}
	.price {.changeColor(@body_text_color3);}
	.add-to-cart {
		.qty-ctl .increase, 
		.qty-ctl .decrease {.changeLine(border;1px; solid ; @body_line5); cursor:pointer; background-color:none;}
		.qty-ctl .increase:hover, 
		.qty-ctl .decrease:hover {.changeLine(border;1px; solid ; @body_line1);}
		label {.changeColor(@body_text_color4);}
	}
	.add-to-links li {.changeLine(border;1px; solid ; @body_line5);}
	.add-to-links li:hover {.changeLine(border;1px; solid ; @body_line2); position:relative;}
	.product-img-box {
		.product-image {.changeLine(border;1px; solid ; @body_line5)}
		.more-views li img{.changeLine(border;1px; solid ; @body_line5); .transition (400ms); .changeBorderRadius(@rounded_corner); }
		.more-views li:hover img{}
		.label {.changeColor(@body_text_color4);}
	}
	.product-shop {
		#product-options-wrapper, .product-options-bottom {border:none; padding:0;}
	}
}

/* UI Tabs */
.ui-tabs { 
	padding:0; border:none;
	.ui-tabs-nav li {margin:0 -1px 1px 0; .changeLine(border;1px; solid ; @body_line1;); border-left:none; display:block; .changeBorderRadius(@rounded_corner); padding:0; position:relative;}
	.ui-tabs-nav li h2{padding:5px 18px 6px; margin:0;.header_gradient(@body_bgcolor2); .changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); .changeLine(border-left;1px; solid ; lighten(@body_bgcolor2,16%)); position:relative;}
	.ui-tabs-nav li:first-child h2{.changeLine(border-left;1px; solid ; @body_line1;);}
	.ui-tabs-nav li a{.changeColor(lighten(@body_text_color1,20%)); .changeFonts(@h4_font); text-transform:uppercase; display:block; padding:0; float:none;}
	.ui-tabs-nav li:hover h2,
	.ui-tabs-nav li.ui-tabs-selected h2{.header_gradient_opposite(@body_bgcolor2);padding:6px 18px 6px; border-top:none }
	.ui-tabs-nav li:hover a, 
	.ui-tabs-nav li.ui-tabs-selected a {.changeColor(@body_text_color5);}
	.tab_content {padding:15px 0;  line-height:134%;}
	
}

.box-collateral {
	clear:both; display:block; overflow:hidden;
	h2 { margin:0 0 15px;.header_gradient(@body_bgcolor2); .changeColor(@body_text_color5); .changeLine(border;1px; solid ; @body_line1;); text-transform:uppercase; padding:4px 65px 4px 18px;display:block; position:relative;}
	h2:after { .changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); content:""; position:absolute; top:0; left:0; right:0; bottom:auto;}
	#product-attribute-specs-table {
		th, td {padding:10px;}
		td {}
		tr.first td {border-top:none;}
	}
	&.box-reviews h3, &.box-reviews .h3, #review-form h4 {.changeFonts(@general_font);  .changeColor(@body_text_color4); }
}

/* Reviews */
#review-form ul.form-list {margin-bottom:17px;}
#review-form .data-table {
	border:none;
	th , td { .changeColor(@body_text_color4); .changeLine(border-top;1px; dashed ; @body_line2;);text-transform:capitalize;}
	thead th {.changeColor(@body_text_color1); border-top:none;  text-transform:lowercase;}
}
.box-reviews {
	dt {.changeColor(@body_text_color1);}
	dd {
		
		span.review, span.detail {font-size:92%;}
		span.review > span {.changeColor(@body_text_color3);}
	}
}
.ratings-table th {}

/* Box tags */
.box-tags {
	h3 { .changeFonts(@general_font);}
	.product-tags {overflow:hidden;}
}

/* Up sell */
.box-up-sell .jcarousel-skin-tango .jcarousel-prev-horizontal, 
.box-up-sell .jcarousel-skin-tango .jcarousel-next-horizontal,
.cart .crosssell .jcarousel-skin-tango .jcarousel-prev-horizontal, 
.cart .crosssell .jcarousel-skin-tango .jcarousel-next-horizontal {.changeLine(border;1px;solid;@body_line2;); .changeBkg(@body_bgcolor1);}
.box-up-sell .jcarousel-skin-tango .jcarousel-prev-disabled-horizontal,
.box-up-sell .jcarousel-skin-tango .jcarousel-next-disabled-horizontal,
.cart .crosssell .jcarousel-skin-tango .jcarousel-prev-disabled-horizontal,
.cart .crosssell .jcarousel-skin-tango .jcarousel-next-disabled-horizontal {.changeBkg(transparent);}

/* Multi checkout */
#multiship-addresses-table {
	border-left:none; border-right:none;
	th:first-child, td:first-child  {.changeLine(border-left;1px;solid;@body_line2;);}
	th.last, td.last  {.changeLine(border-right;1px;solid;@body_line2;);}
	tfoot td {.changeLine(border;1px;solid;@body_line2;); }
}


/* Toolbar */
.view-mode .grid, .view-mode .list {.transition(400ms);}
.toolbar { .changeLine(border;1px; solid ; @body_line2); .changeBorderRadius(@rounded_corner); }
.pager label, .sorter label {.changeColor(@body_text_color2); .changeFonts(@general_font);  }
.pager .amount,.pager .pages li a{.changeColor(@body_text_color6);  font-size:92%;}
.pager .pages li.current, .pager .pages li a:hover {.changeColor(@body_text_color1);  font-size:92%; text-decoration:underline;}
.sorter .view-mode label  {text-transform:uppercase; .changeFonts(@general_font); }
.category-products .toolbar-dropdown span.current, 
.pager .toolbar-dropdown span.current, 
.toolbar-dropdown span.current  {.changeColor(@body_text_color6); .changeLine(border;1px; solid ; @body_line5); }

.toolbar-option .pages .current , 
.toolbar-option .pages li:hover, 
.pager .pages li:hover,
.pager .pages .current {.changeColor(@body_text_color6); padding:0;}

.category-products .toolbar-dropdown span.current , 
.pager .toolbar-dropdown span.current, .toolbar-dropdown span.current {.changeLine(border;1px; solid ; @body_line5); }

.category-products .toolbar .toolbar-dropdown ul, 
.pager .toolbar-dropdown ul, .toolbar-dropdown ul{.changeLine(border;1px; solid ; @body_line5); .changeBkg(@body_bgcolor1); }

.toolbar-option .amount strong {.changeColor(@body_text_color6);}

.review-product-list #customer-reviews .pager { .changeLine(border;1px; solid ; @body_line2); .changeBorderRadius(@rounded_corner); padding:3px 10px; margin:15px 0;}
#customer-reviews a.viewall {.changeColor(@body_text_color1);  display:inline-block; clear:both; margin-bottom:20px;}
#customer-reviews a.viewall:hover {.changeColor(@body_text_color3);}

/* Shopping cart */
.cart-table {
	border:none;
	.product-image > img{.changeLine(border;1px; solid ; @body_line5); }
	thead , tbody {.changeLine(border-left;1px; solid ; @body_line2); }
	thead th.last, tbody td.last{.changeLine(border-right;1px; solid ; @body_line2); }
	thead tr {.changeLine(border-top;1px; solid ; @body_line2);}
	tbody {.changeLine(border-bottom;1px; solid ; @body_line2);}
	td {padding:20px 14px;}
	
	tfoot td{padding:30px 0 0 !important;}
}

.cart .totals #shopping-cart-totals-table{
	tbody td:first-child { .changeColor(@body_text_color4);}
	tbody td span{.changeColor(@body_text_color3);}
	tfoot td:first-child { .changeColor(@body_text_color6); font-size:134%; } 
	tfoot td span {.changeFonts(@general_font);.changeColor(@body_text_color4); font-size:134%;}
}

.cart .cart-collaterals h2 { .changeFonts(@h4_font); text-transform:uppercase; .changeColor(@body_text_color1); .changeLine(border-bottom;1px; solid ; @body_line1); padding-bottom:5px; margin-bottom:16px;}
.cart .discount label, .cart .shipping-form p {.changeColor(@body_text_color6);  margin-bottom:8px;}
.checkout-types button.btn-checkout span span, .opc-col3  button.btn-checkout span span{ .changeFonts(@h4_font); text-transform:uppercase; display:inline-block; padding:0 44px 0 0;  float:none}
.checkout-types button.btn-checkout > span, .opc-col3  button.btn-checkout > span{ padding:7px 0; float:none}
.paypal-or { .changeColor(@body_text_color1);} 
.cart .totals .checkout-types .paypal-or {float:right;} 

.cart .cart-collaterals .crosssell {
	h2 { margin:0 0 20px;.header_gradient(@body_bgcolor2); .changeColor(@body_text_color5); .changeLine(border;1px; solid ; @body_line1;); text-transform:uppercase; padding:0 0 2px; }
	h2 span {padding:4px 65px 4px 18px; .changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); display:block;}
}
.crosssell {
	li.item {
		text-align:center; margin-left:0 !important;
		.product-image > img{.changeLine(border;1px; solid ; @body_line4); }
		/*.product-image:hover > img{.changeLine(border;1px; solid ; @body_line1);}*/
	}
	.jcarousel-skin-tango .jcarousel-prev-horizontal, 
	.jcarousel-skin-tango .jcarousel-next-horizontal {.changeLine(border;1px;solid;@body_text_color5);}
}

/* Block Checkout Progress */
.block-progress dl {
	dt  {}
	dt.complete {.changeColor(@body_text_color1); text-transform:uppercase;}
	dd.complete {
		.changeLine(border-bottom;1px;dashed ; @body_line2;);margin-bottom:17px;padding-bottom:17px;
		a {.changeFonts(@general_font); font-size:92%;.changeColor(@body_text_color3); margin-top:5px; display:inline-block;}
		a:hover {text-decoration:underline;}
	}
	dd.complete.last {border:none; margin-bottom:0; padding-bottom:0;}
}


/*========== Checkout ==========*/
.cart .cart-collaterals .checkout-types a span {.changeColor(@body_text_color6);}

.opc .step-title {.changeBkg(@body_bgcolor1); .changeLine(border;1px; solid ; @body_line1;);  }
.opc .step-title  .step-title-wrapper {padding:0 20px 0 13px; }
.opc .step-title h2, .opc .step-title .number{.changeColor(@body_text_color2); text-transform:uppercase; .changeFonts(@h2_font); padding:4px 0; }
.opc .step-title .number {padding-right:12px; .changeLine(border-right;1px; solid ; @body_line1;);   }
.opc .active .step-title, .opc .allow .step-title:hover{.header_gradient(@body_bgcolor2); }
.opc .active .step-title .step-title-wrapper,
.opc .allow .step-title:hover .step-title-wrapper{.changeLine(border-top;1px; solid ; lighten(@body_text_color1,16%));}
.opc .active .step-title h2, .opc .active .step-title .number,
.opc .allow:hover .step-title h2, .opc .allow .step-title:hover .number{.changeColor(@body_text_color5); position:relative; border:none; padding:3px 0 4px;}
.opc .active .step-title .number,.opc .allow .step-title:hover .number {padding-right:13px;}
.opc .active .step-title .number:before, .opc .allow .step-title:hover .number:before{position:absolute; content:""; right:0; top:0; bottom:0; width:1px; .menu_gradient(lighten(@body_text_color1,30%); @body_bgcolor2);}

#review-buttons-container.buttons-set .f-right a{.changeColor(@body_text_color6); margin-left:4px;}
.checkout-onepage-success p a{.changeColor(@body_text_color1); }
.checkout-onepage-success p a:hover{.changeColor(@body_text_color4);}
.checkout-onepage-success p.id_order a{.changeColor(@body_text_color6);}
.sp-methods .price{.changeColor(@body_text_color6);}
.sp-methods {
	dt { .changeFonts(@general_font);.changeColor(@body_text_color4);  }
	dd label,
	dd span.price{.changeFonts(@general_font); .changeColor(@body_text_color3);  }
}

#opc-payment .sp-methods {
	dd label {.changeColor(@body_text_color4); }
}
.opc-col .step-title {.changeBkg(@body_bgcolor1);}
.opc-col .step-title h2{.changeColor(@body_text_color6);}	
.opc-col .step-title .number {.changeColor(@body_text_color6); }

.order-review .data-table {
	margin-top:7px;
	.product-name {.changeFonts(@general_font);.changeColor(@body_text_color1); font-size:109%;}
	td.qty {font-weight:normal;}
	tfoot {
		tr.first td {.changeLine(border-top;1px; dashed ; @body_line2;);  }
		td .price {.changeColor(@body_text_color3); }
		td:first-child {.changeColor(@body_text_color4); }
		tr.last td, tr.last td .price {.changeColor(@body_text_color6); font-size:134%; }
	}
}
#review-buttons-container p.f-left {
	font-size:92%; margin-top:8px;
	a {.changeColor(@body_text_color1); margin-left:4px;}
	a:hover {text-decoration:underline;}
	
}

/* Multi checkout */
.checkout-progress {
	li.active {
		border-top-color:@body_bgcolor2;
		span {.changeColor(@body_text_color1);}
	}
	li span {margin-top:5px; display:block; .changeFonts(@h5_font); text-transform:uppercase; .changeColor(@body_text_color6);}
}

.multiple-checkout h2, .multiple-checkout h3, .multiple-checkout h4 {.changeFonts(@h5_font);}
.multiple-checkout #payment_form_ccsave {width:360px;}
.multiple-checkout #payment_form_ccsave .input-box select,
.multiple-checkout #payment_form_ccsave input.input-text{width:100%;}

.checkout-multishipping-overview  .multiple-checkout .data-table{
	tfoot tr.first td {.changeLine(border-top;1px; dashed ; @body_line2); padding-top:12px;}
	tfoot tr td {border:none; vertical-align:bottom; padding:5px 14px}
	tfoot tr.last td {padding-bottom:25px;}
	
}

/* Checkout one step */
.opc-col .step-title{
	.body_gradient(@body_bgcolor2); position:relative;
	&:after{.changeLine(border-top;1px; solid ; lighten(@body_text_color1,16%)); content:""; position:absolute; height:1px; top:1px; bottom:auto; left:1px; right:1px; }
	h2{.changeColor(@body_text_color5); position:relative; border:none; margin:0 0 0 7px;}
	h2, .number{.changeColor(@body_text_color5); position:relative; border:none;}
	.number:before{position:absolute; content:""; right:0; top:1px; bottom:0; width:1px; .menu_gradient(lighten(@body_text_color1,30%); @body_bgcolor2);}
}

.opc-col3 #checkout-review-table{
	border:none;
	th:first-child, td:first-child {padding-left:0;}
	th.last, td.last {padding-right:0;}
	tfoot {
		tr.first td {.changeLine(border-top;1px; dashed ; @body_line5;);  }
		td:first-child {text-align:left !important;}
		tr.last td .price {font-weight:normal;}
	}
}
.opc-col #payment_form_ccsave.form-list {
	.cvv-what-is-this {font-size:92%;.changeColor(@body_text_color1);text-decoration:underline; }
	.cvv-what-is-this:hover {text-decoration:none; }
}
.gift-messages-form .item .details .product-name {}

/*========== Create, Login Account ==========*/
#login-form {}
#opc-login h4 {.changeFonts(@general_font); }
.opc #opc-login h3 {.changeColor(@body_text_color2);}
.account-login h2,  .account-create h2{.changeColor(@body_text_color2);.changeFonts(@h4_font);}
 #opc-login .col-1 {
	h4  {.changeColor(@body_text_color1);.changeFonts(@h5_font); clear:both; float:none; text-transform:uppercase; margin:19px 0 5px;}
	ul li  {.changeColor(@body_text_color4);   margin-bottom:5px; float:none;}
	ul.ul {display:block;}
	ul.ul li  {margin-bottom:0;}
	li em {.changeColor(@body_text_color1);margin-right:11px;float:left;height:10px;margin-top:3px;}
}

/* Checkout */
#checkout-step-login  .col-1 p{}

/* My account */
.block-account ul li.current strong {.changeColor(@body_text_color3);}
.block-account ul li:hover,
.block-account ul li a:hover {.changeColor(@body_text_color3);}

.dashboard .box-reviews .number, 
.dashboard .box-tags .number {.changeColor(@body_text_color1); margin-top:0;}
.dashboard .box-reviews .number:after, 
.dashboard .box-tags .number:after{content:".";}

.dashboard .box-reviews, .dashboard .box-tags {
	.product-name, .product-name a, .number {line-height:1.35em;}
}

.box-info .box-content a {
	.changeColor(@body_text_color1); font-size:92%;
	&:hover {text-decoration:underline;}
}

.customer-account-edit .my-account{
	.fieldset .legend {.changeColor(@body_text_color2);.changeFonts(@h4_font); margin:0 0 10px;}
	.buttons-set {padding-top:0;}
	.fieldset {margin-bottom:0;}
	.form-list li {margin-bottom:5px;}
	.form-list li.control {
		margin-bottom:25px;
		input.checkbox {margin-top:0;}
	}
	.form-list .field {width:45%;}
	.form-list .input-box {
		input.input-text, .validation-advice {width:345px;}
	}
}

.customer-address-index .my-account {
	.addresses-list {
		h2 {.changeColor(@body_text_color3); text-transform:uppercase;.changeFonts(@h4_font); margin-top:0;}
		h3 {.changeColor(@body_text_color1); text-transform:uppercase;.changeFonts(@general_font); }
		a {.changeColor(@body_text_color1); font-size:92%;}
		a:hover {.changeColor(@body_text_color3);}
	}
}

.order-info,.order-date {}
.order-info-box h2 {.changeColor(@body_text_color1); text-transform:uppercase;.changeFonts(@general_font); }
.order-details .table-caption {.changeColor(@body_text_color3); text-transform:uppercase;.changeFonts(@h4_font); }
#my-orders-table .product-name {.changeColor(@body_text_color1); .changeFonts(@general_font); }

/*========== Compare Page ==========*/
.catalog-product-compare-index .page-title a{.changeColor(@body_text_color5); padding-right:20px;}
.catalog-product-compare-index .buttons-set button.button{.changeColor(@body_text_color6);}
.catalog-product-compare-index .buttons-set button.button:hover{.changeColor(@body_text_color6);}
.compare-table {
	border:none;
	.image-remove {margin-bottom:15px; display:inline-block;}
	/*.image-remove > .product-image:hover{.changeLine(border;1px; solid ; @body_line1);}*/
	.image-remove > .product-image {.changeLine(border;1px; solid ; @body_line5); }
	.price-box {overflow:hidden; display:inline-block;}
	.old-price, .special-price {float:left;}
	.old-price {margin-right:15px;}
	.price {.changeColor(@body_text_color3) !important; font-size:117%;}
	.old-price .price {.changeColor(@body_text_color6) !important; }
	.special-price {margin-top:1px;}
	tr.product-shop-row.first th, tr.product-shop-row.first td {.changeLine(border-bottom;1px; dashed ; @body_line1); }
	th {border:none; font-size:117%;}
	td {text-align:left;}
	tr.add-to-row.last {
		button.btn-cart {float:left;}
		.add-to-links {float:left; margin-left:25px; margin-top:7px;}
	}
	th, td {padding:14px;}
	tbody th {border:none; .changeColor(@body_text_color1);}
}

/*========== Blog ==========*/
.post-title h2 a,.post-content .post-title h2,.post-by span,.comment-count a:hover,.em_block-recent-post .post-title,.em_block-recent-comments li.item .comment-title,#allcomments span.comment-by span, .block .post-title, .block .comment-title{.changeColor(@body_text_color1); }
.our_blog .em_block-recent-post .post-title{font-weight:normal;}
.post-title h2{.changeFonts(@general_font);}
.comment-count a, .comments .post-footer .comment-count, .em_block-recent-comments .comment-by span{.changeColor(@body_text_color3);}
.em_blog-cat li a:hover,.em_block-recent-post li.item span.post-by span, .block-related li.item span.post-by span{.changeColor(@body_text_color3);}
.em_blog-cat li.current a{.changeColor(@body_text_color3); cursor:default;}
.em_post-item .post-item-time span{.changeFonts(@h1_font);}
.em_post-item .post-item-time span.time-day{.header_gradient(@button2_bgcolor); .changeColor(@body_text_color5); .changeLine(border;1px; solid ;@button2_bgcolor ); position:relative; }
.em_post-item .post-item-time span.time-day:after {content:""; position:absolute; top:0; bottom:auto; left:0; right:0;.changeLine(border-top;1px; solid ; lighten(@button2_bgcolor,16%)); }
.em_block-recent-post.block .block-content li.item,
.em_block-recent-comments .block-content li.item,
#allcomments .comment-item{.changeLine(border-top;1px; dashed ;@body_line2); }
.comment-item-header .time-stamp {.changeLine(border-left;1px; solid ; darken(@body_line5,10%));}
.em_post-item .post-item-time span.time-month{.changeColor(@body_text_color1)}
.post-footer{.changeLine(border;1px; solid ; @body_line2);}
.em_post_title h2{.header_gradient(@body_bgcolor2);.changeLine(border-top;1px; solid ; lighten(@body_bgcolor2,16%)); padding:4px 18px 7px; text-transform:uppercase; .changeFonts(@h4_font); .changeColor(@body_text_color5); margin:0;}

.form-comment-container h3,#comments-header h3.form-title{.header_gradient(@body_bgcolor2);.changeLine(border;1px; solid ; @body_line1); padding:5px 18px 6px;text-transform:uppercase; .changeFonts(@h4_font); .changeColor(@body_text_color5); margin:35px 0 0; position:relative; }	
.form-comment-container h3:after,#comments-header h3.form-title:after {content:""; position:absolute; top:0; bottom:auto; left:0; right:0;.changeLine(border-top;1px; solid ;lighten(@body_bgcolor2,16%)); }
.view-mode .detail, .view-mode .simple {.changeColor(@body_text_color2); .changeFonts(@general_font); float:left;margin:5px 0 0 7px;text-transform:uppercase;}
.view-mode strong.detail, .view-mode strong.simple {.changeColor(@body_text_color1);}
.em_post-action .tags span, .em_post-action .tags li a {.changeColor(@body_text_color6); }
.em_post-action .tags li a:hover {.changeColor(@body_text_color1);}
.block.block-related .mini-posts-list li.item {.changeLine(border-top;1px; dashed ; @body_line2); border-bottom:none;}
.blog-index-index .toolbar, .blog-category-view .toolbar { .addBorderBox(); border:none; padding:6px 0 0;}	
.em_post_title{.changeLine(border;1px; solid ; @body_line1); margin-bottom:20px;}
.comments .required,.em_post-action .post-share span{.changeColor(@body_text_color4);}

.blog-post-view .post-title h2 {.changeColor(@body_text_color1); font-size:168%;}
.blog-index-index .toolbar .sorter, .blog-category-view .toolbar .sorter, .blog-tag-view .toolbar .sorter{ .changeLine(border-top;1px; solid ; @body_line2); padding:6px 0 0; }

/* Footer */
.footer-wrapper{.changeBkg(@body_bgcolor1);}
.block-subscribe .input-box input.input-text{.changeLine(border;1px; solid ; @body_line5); .addBorderBox(); border-right:none;}

/* Stores view*/
.store_switcher_variation .storediv .store_content li .storename {.changeColor(@body_text_color1);  margin-top:10px; text-align:center;}

/* Resize 2 */
/*.adapt-2 .category-left .vnav .menu-item-depth-0 a.arrow {top:5px;}*/
.adapt-2 .category-left .vnav .menu-item-depth-0 > a, 
.adapt-2  .category-left .vnav .menu-item-depth-0 > .em-catalog-navigation .level0 > a {padding-top:4px; padding-bottom:4px;}
.adapt-2 .em_nav h5, .adapt-2 .em_nav .widget-title h2, .adapt-2 .em_nav .widget-title h3 {text-transform:capitalize;}

/* Resize 1 */
.adapt-1 .block-subscribe {
	.input-box {float:none;} 
	.input-box input.input-text {.changeLine(border-right;1px; solid ; @body_line5); width:220px } 
	.action{margin-top:6px; clear:both; float:none;}
}

.adapt-1 .best-seller .widget-products .products-grid li.item .product-image {float:none; display:inline-block;}

.adapt-1 .category_link_ad li:first-child {.changeLine(border-bottom; 1px; dashed ; lighten(@footer_line2,10%)); width:200px }
.adapt-1 .our_store img{margin-bottom:7px;}
.adapt-1 .our_store .primary {clear:both; display:block;}

.adapt-1 .category-left .vnav {padding-bottom:0;}
.adapt-1 .category-left .vnav .menu-item-depth-0 a.arrow {top:4px;}
.adapt-1 .category-left .em_nav .menu-item-vbox .menu-container .menu-item-link > a.arrow {top:8px;}
.adapt-1 .category-left .vnav .menu-item-depth-0 > a, 
.adapt-1  .category-left .vnav .menu-item-depth-0 > .em-catalog-navigation .level0 > a {padding-top:2px; padding-bottom:2px;}
.adapt-1 .em_nav h5, .adapt-1 .em_nav .widget-title h2, .adapt-1 .em_nav .widget-title h3 {text-transform:capitalize;}

/* Resize 0 */
@media (max-width:767px) {
	/* reset */
	.top-menu .em_nav, .top-menu .nav-container,
	.top-menu .hnav, .top-menu #nav {border:none; background:none; filter:none; -ms-filter:none; box-shadow:none; margin-top:3px;}
	.top-menu .hnav:before, .top-menu #nav:before {border:none; height:0;}
	.nav-container li.level-top a.level-top:before, .nav-container li.level-top a.level-top:after, .hnav .menu-item-depth-0:before, .hnav .menu-item-depth-0:after, #nav > li.level-top:hover a.level-top:after, .hnav .menu-item-depth-0.menu-item-parent:hover > a:after {width:0; height:0; background:none; filter:none; -ms-filter:none; border:none;}
	
	.top-menu .menu-title {
		.header_gradient_pie(@menu_left_bgcolor);.changeLine(border;1px; solid ; @menu_left_bgcolor);  border-bottom:none; .changeBoxShadow("";1px;3px;5px;0;@box_shadow); position:relative;padding:10px;
		&:before { .changeLine(border-top;1px; solid ; lighten(@menu_left_bgcolor,12%));content:""; position:absolute; top:0; bottom:auto; left:0; right:0; height:1px;}
		a {.changeColor(@menu_top_text_color); .changeFonts(@h5_font); text-transform:capitalize; }
		
	}
	.top-menu .menu-item-text.menu-item-depth-0 > .em-catalog-navigation .level0 > a span,
	.category-left .menu-item-text.menu-item-depth-0 > .em-catalog-navigation .level0 > a span {.changeFonts(@h5_font); text-transform:capitalize;}
	.hnav .menu-item-depth-0 > a, .nav-container li.level-top a.level-top, .hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li > a {.changeFonts(@h5_font);}
	.hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:after, .hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:before, .nav-container li.level-top a.level-top:before, .nav-container li.level-top a.level-top:after, .hnav .menu-item-depth-0:before, .hnav .menu-item-depth-0:after, #nav > li.level-top:hover a.level-top:after, .hnav .menu-item-depth-0.menu-item-parent:hover > a:after, .hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:hover > a:after{width:0; position:static;display:none;}
	
	.em_nav .hnav {.changeColor(@body_text_color6);}
	.hnav .menu-item-link.menu-item-depth-0:hover, .hnav .menu-item-text.menu-item-depth-0 > .em-catalog-navigation > li:hover, #nav > li.level-top:hover, #nav > li.level-top.active {margin-top:0; border-top:none;}
	.hnav .menu-item-depth-0 > a,
	.hnav .menu-item-depth-0 > .em-catalog-navigation > li > a,
	.nav-container li.level-top > a {.changeBkg(@menu_left_bgcolor);background-image:none; }
	.hnav .menu-item-depth-0:hover > a, #nav > li.level-top:hover a.level-top, #nav > li.level-top.active a.level-top,
	.hnav .menu-item-depth-0:hover > a,
	.hnav .menu-item-depth-0.active > a,
	.hnav .menu-item-depth-0 > .em-catalog-navigation > li:hover > a,
	.hnav .menu-item-depth-0 > .em-catalog-navigation > li.active > a,
	.nav-container li.level-top > a:hover	{.changeBkg(@menu_top_bgcolor); margin-top:0; background-image:none; border-top:none;}
	.hnav .menu-item-depth-0 > a, .nav-container li.level-top a.level-top { text-transform:capitalize; font-size:120%;}
	.hnav .menu-item-depth-0 > a span ,
	.hnav .menu-item-depth-0 > .em-catalog-navigation > li > a span {.changeColor(@menu_top_text_color);}
	.hnav .menu-item-depth-0 > a.arrow ,
	.hnav .menu-item-depth-0:hover > a.arrow ,
	.hnav .menu-item-depth-0 > .em-catalog-navigation > li > a.arrow {.changeLine(border-left;1px; solid ; @menu_left_content_bgcolor) !important;}
	.hnav .menu-item-depth-0:hover > a.arrow, #nav > li.level-top:hover a.arrow, #nav > li.level-top.active a.arrow {top:0;}
	
	.em_nav .menu-item-link.menu-item-depth-0 > ul, 
	#nav li.level-top > ul.shown-sub, #nav.level-top li > div.shown-sub {border:none !important;  .changeBoxShadow("";0;0;5px;0;#888); margin-top:0;}
	.em_nav .hnav .menu-item-link > ul, .em_nav .hnav .em-catalog-navigation ul, #nav li ul.shown-sub, #nav li div.shown-sub {.changeBoxShadow("";0;0;5px;0;#888) !important; border:none !important;}
	
	.em_nav .hnav .menu-item-link.menu-item-depth-0 > ul, .em_nav .hnav .menu-item-text.menu-item-depth-0 li.level0:hover > ul, #nav li.level-top > ul.shown-sub, #nav.level-top li > div.shown-sub {margin-left:0;}
	.category-left .vnav li.menu-item-link> ul, .category-left .vnav .em-catalog-navigation li> ul {.changeBkg(@menu_top_bgcolor) !important;}
	
	/* menu categories */	
	.menuleftText {.header_gradient_pie(@menu_top_bgcolor); .changeLine(border;1px; solid ; @menu_top_bgcolor); position:relative;}	
	.menuleftText:after {.changeLine(border-top;1px; solid ; lighten(@menu_top_bgcolor,20%)); }
	/*.cms-index-index .category-left {z-index:0;}*/
	.category-left {
		box-shadow:none; margin-bottom:12px;
		.mega-menu {
			border:none; background:none;box-shadow:none; position:static; display:block !important;
			.vnav {margin-bottom:0;}
		}
		.menuleftText span {.changeFonts(@h5_font); text-transform:capitalize; width:100%;  .addBorderBox(); display:block;}
	}	
	/*.category-left .vnav .menu-item-depth-0 > .menu-container,
	.category-left .vnav li.menu-item-link:hover > ul, .category-left .vnav .em-catalog-navigation li:hover > ul	{padding-right: 0; padding-left: 0;}*/
	.category-left .mega-menu .em_nav {box-shadow:none; background:none; filter:none; -ms-filter:none; border:none; padding-top:3px;}
	
	/* Fix padding sub menu */
	.category-left .vnav .menu-item-depth-0 > .menu-container	{padding:20px 0;}
	.vnav .menu-item-depth-0 a {padding: 5px 10px 5px 0;}
	.category-left .vnav li.menu-item-link > ul, .category-left .vnav li.menu-item-link:hover > ul, .category-left .vnav .em-catalog-navigation li:hover > ul {padding:20px 0;}
	.vnav .menu-item-link .em-catalog-navigation li a {padding-left:0;}
	/* end fix padding */
	
	.category-left .vnav {
		padding-top:0;
		.menu-item-link.menu-item-depth-0 {
			.changeBkg(@menu_left_bgcolor); .changeBkg(@menu_top_bgcolor);
			> a {border:none; margin:0 0 3px; padding:9px 40px 8px 10px; }
			> a span{.changeColor(@menu_top_text_color);.changeFonts(@h5_font); text-transform:capitalize; }
			> a.arrow {.changeLine(border-left;1px; solid ; @menu_left_content_bgcolor);}
		}
		.menu-item-depth-0 {margin:0 0 3px;}
		.menu-item-text.menu-item-depth-0 {margin-bottom:0;}
		.menu-item-text.menu-item-depth-0 > .em-catalog-navigation  .level0 {
			.changeBkg(@menu_left_bgcolor); .changeBkg(@menu_top_bgcolor);
			> a {border:none; margin:0 0 3px; padding:9px 40px 8px 10px; }
			> a span{.changeColor(@menu_top_text_color);.changeFonts(@h5_font); text-transform:capitalize; }
			> a.arrow {.changeLine(border-left;1px; solid ; @menu_left_content_bgcolor);}
		}
		.menu-item-link.menu-item-depth-0:hover {.changeBkg(@menu_left_bgcolor);}
		.menu-container, .em-catalog-navigation ul {border:none;}
	}
	.sidebar .menu-title {margin-bottom:20px;}
	.sidebar .em_nav {margin-top:-20px;}
	
	.col-right .vnav, .col-left .vnav {border:none !important;}
	.col-right .vnav, .col-left .vnav {
		.menu-item-depth-0 > .menu-container,
		.em-catalog-navigation li > ul,
		.menu-item-depth-0 .menu-item-link:hover > .menu-container  {border:none !important;}
	}
	
	/* Slider */	
	.fashion_content, .cosmetic_content, .shoes_content, .accessories_content, .electronics_content {
		.jcarousel-skin-tango .jcarousel-next-horizontal,
		.jcarousel-skin-tango .jcarousel-prev-horizontal {top:-25px !important;}
	}
	
	/* Dropdown cart */
	.dropdown-cart:hover .dropdown-cart-content-top:after {display:none; position:static; border:none;}
	
	/* Block subscribe */
	.block-subscribe .input-box{float:none}
	.block-subscribe .input-box input.input-text {.changeLine(border-right;1px; solid ; @body_line5);}
	.block-subscribe .validation-advice {width:100%;}
	.block-subscribe .action {margin-top:8px; float:none;}
	
	/* Footer */
	.container-footer .footer-links ul li {.changeLine(border-right;1px; solid ;@footer_link_color); border-left:none;}
	.container-footer .footer-links ul li:last-child {border-right:none;}
	
	.cart .cart-collaterals .crosssell h2 {text-transform:capitalize;}
	
	/* Product grid */
	.products-grid li.item { .addBorderBox();}
}

/*Fix for Ipod*/
@media only screen and (min-device-width:320px) and (max-device-width:480px){
	.cart-table {
		thead , tbody {.changeLine(border-right;1px; solid ; @body_line2); }
	}
}

/*Fix for Ipod portrait orientation */
@media only screen and (min-device-width:320px) and (max-device-width:480px) and (orientation:portrait) {
/*@media (max-width:320px) {*/
 	/* Footer */
 	.category_link_ad ul li {width:93%; border-left:none;}
 	.category_link_ad ul li:first-child {.changeLine(border-bottom; 1px; dashed ; lighten(@footer_line2,10%));}
	#opc-login-popup {width:245px;top:20px !important;}
}

<?php if(isset($_GET['custom_css_files'])):?>
	<?php foreach(explode(',',$_GET['custom_css_files']) as $file):?>
		@import "<?php echo $file;?>";
	<?php endforeach;?>
<?php endif;?>