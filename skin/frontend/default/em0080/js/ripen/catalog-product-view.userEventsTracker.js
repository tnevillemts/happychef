jQuery(document).ready(function($){

    var userEventsTracker = {
        selectedOptions: {},
        init: function() {
            var self = this;
            self.bindOptions();
            self.initEmbroideryAlertListener();
            self.getInitialSelection();
        },
        bindOptions: function() {
            var self = this;
            $('dd[data-option-label]').each(function(){
                var optionLabel = $(this).attr('data-option-label');
                $(this).find('.input-box').on('click', '.amconf-image-container img.amconf-image', function(e){
                    if(e.isTrigger) {
                        self.selectedOptions[optionLabel] = "Default";
                    } else {
                        self.selectedOptions[optionLabel] = "Selected";
                    }
                });
            });
        },
        getInitialSelection: function () {
            var self = this;
            $('dd[data-option-label]').each(function(){
                var optionLabel = $(this).attr('data-option-label');
                self.selectedOptions[optionLabel] = "No Selection";
                $(this).find('.amconf-image-container img.amconf-image-selected').each(function(){
                    self.selectedOptions[optionLabel] = "Default";
                });
            });
        },
        initEmbroideryAlertListener: function() {
            var self = this;
            $(document).on('embroidery-alert add-to-cart-alert', function(){
                self.trackIncompleteOptionSelection()
            });
        },
        trackIncompleteOptionSelection: function() {
            var self = this;
            var currentSelections = [];
            $.each(self.selectedOptions, function(i, v) {
                currentSelections.push(i + ':' + v);
            });
            _gaq.push(['_trackEvent', 'product-alert', 'product-options', currentSelections.join(',')]);
        }
    }.init();

});