jQuery.noConflict();

jQuery(document).ready(function () {

    var $ = jQuery;
    var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|webOS|iPod|BlackBerry|hpwos/i.test(navigator.userAgent);
    PAGE_TIMER = null;

    // Reveal input box for a promocode if link is clicked
    $(document).on('click', '#cart-promocode-link, #cart-change-discount', function () {
        $('#cart-promocode-link').hide();
        $("#cart-promocode-content").show()
    });

    // Retrieve regions for selected country and render a regions dropdown
    loadRegions = function(setup, country) {
        if (country) {
            renderCanadianNote(country);
            new Ajax.Request('/bettercheckout/estimate/getRegions', {
                method: 'post',
                parameters: {"country_id": country},
                onComplete: function (response) {

                    if(country == 'PR') {
                        $('#country').val("PR");
                    } else if (country == 'VI') {
                        $('#country').val("VI");
                    }

                    var result = $.parseJSON(response.responseText);
                    if(country != 'PR' && country != 'VI') {
                        $('#cart-estimate-region').html(result.dropdown);

                        // Retrieve and update delivery methods dropdown
                        if ($('#region_id').val() > 0 || !$('#region_id').length) {
                            loadShippingMethods(false, false, country, $('#region_id').val());
                        }
                    } else {
                        if(setup) {
                            loadShippingMethods(true, false, country);
                        } else {
                            loadShippingMethods(false, true, country);
                        }
                    }
                }
            });
        }
    }

    // Renders totals box
    renderTotals = function (result) {
        $("#totals-container").html(result.totals);
        if ($("#checkout-mobile-total-amount").length) {
            $("#checkout-mobile-total-amount").html(result.grandtotal);
        }
    }

    // Remove applied gift card with ajax
    $(document).on('click', '.remove-giftcard', function () {
        new Ajax.Request('/bettercheckout/estimate/removeGiftCard', {
            method: 'post',
            parameters: {'giftcard_code': $('#giftcard_code').val()},
            onComplete: function (response) {
                var result = $.parseJSON(response.responseText);
                renderTotals(result);
                loadRegions(false, $('#country').val());
            }
        });
    });

    $('#apply-promo-form').submit(function (event) {
        $('#promocode-submit-button').trigger('click');
        event.preventDefault();

    });

    // Apply promo code of gift card
    $(document).on('click', '#promocode-submit-button', function () {

        if ($("#promocode").val().length > 0) {
            hcPromo.manualEntry($("#promocode").val());
        } else {
            $(".promocode-response-message").hide().removeClass('success').addClass("error").html('Please enter your coupon code above.').fadeIn();
        }
    });

    // Open shipping info fancybox
    $(document).on('click', '.shipping-info-link', function () {
        var w = (isMobile) ? '100%' : '60%';
        jQuery.fancybox({
            'width': w,
            'height': 'auto',
            href: '#popup-shipping-info',
            modal: false,
            'autoSize': false
        });
    });

    // Show shipping dropdowns if user clicks on "Calculate" link
    $(document).on('click', '#calculate-shipping-link', function () {
        $(".cart-estimate-shipping-row").show();
    });
    // Populate regions dropdowns when country is selected
    $(document).on('change', '#country', function () {

        // Reset previously populated data
        $('#cart-estimate-shipping-method').empty();
        $('#cart-estimate-region').empty();

        // Retrieve regions for specific country
        loadRegions(true, $('#country').val());

        if ($('#country').val() != 'US' && $('#country').val() != 'CA' && $('#country').val() != '') {
            $('#cart-estimate-shipping-method').append("<select><option>Loading...</option></select>");

            // Retrieve delivery methods for countries that don't have regions
            loadShippingMethods(true, false, $("#country").val());
        }
    });

    // Populate delivery methods when region is selected
    $(document).on('change', '#region_id', function () {
        $('#cart-estimate-shipping-method').empty();
        if ($("#region_id").val() || $("#region_id").length == 0) {
            loadShippingMethods(true, false, $("#country").val(), $("#region_id").val());
        }
    });

    // Render regions dropdown if country has been previously selected
    if ($('#country').length) {
        loadRegions(false, $('#country').val());
    }

    // Show and hide applied embroidery for cart items
    if ($('.cart-view-embroidery-link').length) {
        $('.cart-view-embroidery-link').click(function () {
            $(this).siblings('.descript').first().toggle('slow');
            $(this).html(($(this).text() == 'View') ? 'Hide' : 'View');
        });
    }

    $(".shopcart-col.shopcart-col-qty .shopcart-item-qty input").mouseup(function (e) {
        e.preventDefault();
    });

    $(".shopcart-col.shopcart-col-qty .shopcart-item-qty input").keyup(function () {
        var $this = $(this);
        // chop numbers greater than 4 digits down to 4 digits
        if ($this.val().length > 4) {
            $this.val($this.val().slice(0, 4));
        }
        // if there's a leading 0 AND the number isn't 0, trim off the leading 0's
        if ($this.val().charAt(0) == 0 && $this.val().length > 1) {
            var valueString = $this.val() + '';
            $this.val(parseInt(valueString, 10));
        }
    });

    $('#need-assistance').click(function () {
        if (isMobile || $(window).width() < 568) {
            if ($('.cart-contacts-container').css('display') == 'none') {
                $('.cart-contacts-container').css('display', 'flex');
                $('html, body').animate({
                    scrollTop: $("#need-assistance").offset().top
                }, 500);

            } else {
                $('.cart-contacts-container').css('display', 'none');
            }
        }
    });

    if (isMobile) {

        $(".shopcart-col.shopcart-col-qty .shopcart-item-qty input").on("click", function () {
            currentValue = $(this).val();
            $(this).val('');
        });

        $(".shopcart-col.shopcart-col-qty .shopcart-item-qty input").on("blur", function () {
            if ($(this).val() == '') {
                $(this).val(currentValue);
            }
        });

        $(document).on('click', '#return-policy-link', function () {

            jQuery.fancybox({
                'width': '100%',
                'height': 'auto',
                href: '#popup-return-policy',
                modal: false,
                'autoSize': false
            });
        });
    }

    // Retrieve delivery methods and render totals
    function loadShippingMethods(setup, refresh, country, region) {
        new Ajax.Request('/bettercheckout/estimate', {
            method: 'post',
            parameters: {"country_id": country, "region_id": region},
            onComplete: function (response) {
                var result = $.parseJSON(response.responseText);
                $('#cart-estimate-shipping-method').html(result.dropdown);
                if ($('#estimate_method').selected().val() == '' || $("#estimate_method option:selected").text().indexOf('Custom Quote') == -1) {
                    $('.shipping-alert-message').remove();
                }

                if (result.newShippingRates == true) {
                    updateShippingTotals(country, $("#estimate_method").val());
                }

                if ($('#estimate_method').length) {
                    $("#estimate_method").change(function () {
                        updateShippingTotals(country, $("#estimate_method").val());
                    });
                    if (result.freeShippingSelected) {
                        $('#cart-selected-shipping-method').html('Free &nbsp;&nbsp;<div class="calculate-shipping-link" id="calculate-shipping-link">Edit</div>');
                    }
                } else if ($('#checkout-recap-totals')) {
                    applyShippingMethod($("#estimate_method").val());
                }

                if (setup) {
                    $("#estimate_method").val($("#estimate_method option:first").val());
                }
            }
        });
    }

    function updateShippingTotals(country, estimate_method) {
        new Ajax.Request('/bettercheckout/estimate/estimateUpdate', {
            method: 'post',
            parameters: {"estimate_method": estimate_method},
            onComplete: function (response) {
                var result = $.parseJSON(response.responseText);
                if (result.status) {
                     renderTotals(result);
                     loadRegions(false, country);
                }
            }
        });
    }

    function applyShippingMethod(estimate_method) {
         new Ajax.Request('/bettercheckout/checkout/applyShipping', {
            method: 'post',
            parameters: {"estimate_method": $("#estimate_method").val()},
            onComplete: function (response) {
                var result = $.parseJSON(response.responseText);
                renderTotals(result);
            }
        });
    }

    // TEXT INPUT FOR QTY >= 10
    $('.cart-item-qty-input input').on('change', function () {
        var itemId = $(this).attr("id").split("-")[1];
        var cap = parseInt($('#cart-' + itemId + '-qty-cap').val());
        var value = $(this).val().replace(/\D/g, '');

        if (value.charAt(0) == 0) {
            value = value.slice(1, 4);
        }

        // make sure there's at least 1 qty.
        if (value == 0) {
            value = 1;
        }

        $(this).val(value);

        // Start the timer and price loaders
        if(PAGE_TIMER != 'undefined' && PAGE_TIMER != null) {
            clearTimeout(PAGE_TIMER);
        }

        showInlineLoader($('#cart-item-total-price-' + itemId + ' .price'));

        PAGE_TIMER = setTimeout(function () {
            var qty = $('#cart-' + itemId + '-qty').val();
            if (qty) {
                updateCartItemQty(itemId, qty);
            }
        }, 1500);
    });


    function updateCartItemQty(itemId, qty) {

        new Ajax.Request('/bettercheckout/cart/UpdateCartAjax', {
            method: 'post',
            parameters: {"itemId": itemId, "qty": qty},
            onComplete: function (response) {

                location.reload(true);

                var result = $.parseJSON(response.responseText);

                $('#cart-item-total-price-' + itemId + ' .price').html(result.newItemTotal);
                $('#cart-stock-status-' + itemId).html(result.stockStatus);
                $('#cart-embroidery-message-' + itemId).html(result.embroideryDisclaimer);

                // if change affected other items in the cart, update their prices as well
                $(".cart-item-total-price").each(function(){

                    var id = $(this).attr("id").split(/[-]+/).pop();
                    if(Object.keys(result.updatedLineItemPrices).length > 0 && typeof result.updatedLineItemPrices[id] != 'undefined') {
                        $('#cart-item-total-price-' + id + ' .price').html(result.updatedLineItemPrices[id].newItemTotal);
                    }
                });

                $(".cart-item-price").each(function(){
                    var id = $(this).attr("id").split(/[-]+/).pop();
                    if(Object.keys(result.updatedLineItemPrices).length > 0 && typeof result.updatedLineItemPrices[id] != 'undefined') {
                        $('#cart-item-price-' + id + ' .price').html(result.updatedLineItemPrices[id].newItemUnitPrice);
                    }
                });

                var ruleNotAppliedMessage = $('#promocode').val(result.ruleNotAppliedMessage);

                $(".promocode-applied-response-message").val(result.ruleNotAppliedMessage);

                if(ruleNotAppliedMessage != '') {
                    $(".promocode-applied-response-message").show().html(result.ruleNotAppliedMessage).fadeIn();
                } else {
                    $(".promocode-applied-response-message").hide().html('').fadeOut();
                }

                renderTotals(result);
                loadRegions(false, $('#country').val());

                //location.reload(true);
            }
        });
    };

    function renderCanadianNote(countryId){
        if (!isMobile){
            if ($('.canadian-note').length){
                if(countryId == 'CA') {
                    $('.canadian-note').show();
                } else {
                    $('.canadian-note').hide();
                }
            }
        } else {
            if ('.canadian-note-mobile'){
                if(countryId == 'CA') {
                    $('.canadian-note-mobile').show();
                    $('.canadian-note-mobile-link').on('click', function(){
                        $('.canadian-note-mobile-extended').toggle();
                    })

                } else {
                    $('.canadian-note-mobile').hide();
                }
            }
        }
    };

});
