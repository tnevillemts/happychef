// The following line enables form validation for individual fields
// using change and onblur events
Validation.defaultOptions.immediate = true;

jQuery.noConflict();

//************************************************************/
// EXTEND addClass AND removeClass TO ACCEPT CALLBACKS
//************************************************************/
(function ($) {
    var oAddClass = $.fn.addClass;
    $.fn.addClass = function () {
        for (var i in arguments) {
            var arg = arguments[i];
            if (!!(arg && arg.constructor && arg.call && arg.apply)) {
                setTimeout(arg.bind(this));
                delete arguments[i];
            }
        }
        return oAddClass.apply(this, arguments);
    }
})(jQuery);

(function ($) {
    var oRemClass = $.fn.removeClass;
    $.fn.removeClass = function () {
        for (var i in arguments) {
            var arg = arguments[i];
            if (!!(arg && arg.constructor && arg.call && arg.apply)) {
                setTimeout(arg.bind(this));
                delete arguments[i];
            }
        }
        return oRemClass.apply(this, arguments);
    }
})(jQuery);
//************************************************************/
// EXTEND addClass AND removeClass TO ACCEPT CALLBACKS
//************************************************************/

function getCursorPosition(field) {
    var cursorPos = 0;

    // IE Support
    if (document.selection) {
        field.focus();

        // To get cursor position, get empty selection range
        var sel = document.selection.createRange();
        sel.moveStart('character', -field.value.length);
        cursorPos = sel.text.length;
    }

    // Firefox support
    else if (field.selectionStart || field.selectionStart == '0')
        cursorPos = field.selectionStart;

    return cursorPos;
}

(function($){
    $.event.special.destroyed = {
        remove: function(o) {
            if (o.handler && o.type == 'destroyed') {
                o.handler();
            }
        }
    }
})(jQuery)

INLINE_LOADER_INTERVAL = null;

//************************************************************/
// SHOW INLINE LOADER
//************************************************************/
function showInlineLoader($containerEl, updateText) {
    var $ = jQuery;
    if ($containerEl.length) {
        clearInterval(INLINE_LOADER_INTERVAL);
        var dots = 0;

        $containerEl.each(function(){
            var thisContainerEl = $(this);

            if (updateText) {
                var staticPrefix = updateText;
            } else {
                var staticPrefix = "Updating";
            }

            var animatedDots = $('<div></div>')
                .attr({ class: 'animated-dots' });

            var loader = $('<span></span>')
                .attr({ class: 'inline-loader-wrap' })
                .text(staticPrefix)
                .append(animatedDots);

            thisContainerEl.empty().append(loader);

            thisContainerEl.children().first().on('destroyed', function(){
                clearInterval(INLINE_LOADER_INTERVAL);
            });

            INLINE_LOADER_INTERVAL = window.setInterval(function () {
                if (dots < 3) {
                    animatedDots.append('.');
                    dots++;
                } else {
                    animatedDots.html('');
                    dots = 0;
                }
            }, 600);
        });
    }
}


//************************************************************/
// SHOW BUTTON LOADER
//************************************************************/
function showButtonLoader(button) {
    var $ = jQuery;
    if (!$(button).children('.btn-overlay').length) {
        var BASE_URL = getBaseUrl(location.href);
        var btnOverlay = $('<div class="btn-overlay"><svg class="btn-loader" viewBox="0 0 26 26"><use xlink:href="' + BASE_URL + '/skin/frontend/default/em0080/images/loader/btn-loader.svg#ringloader">LOADING</use></svg></div>');
        $(btnOverlay).outerHeight($(button).outerHeight()).outerWidth($(button).outerWidth());
        $(btnOverlay).css({
            'top': $(button)[0].offsetTop,
            'left': $(button)[0].offsetLeft
        });
        $(btnOverlay).on('click', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
        });
        $(button).append(btnOverlay);
    }
    if (!$(button).hasClass('disabled')) {
        $(button).addClass('disabled');
        if ($(button).hasOwnProperty('disabled')) {
            $(button).prop('disabled', true);
        }
    }
}

function hideButtonLoader(button) {
    var $ = jQuery;
    if ($(button).hasClass('disabled')) {
        $(button).removeClass('disabled');
        if ($(button).hasOwnProperty('disabled')) {
            $(button).prop('disabled', false);
        }
    }
    $(button).find($('.btn-overlay')).remove();
}

//************************************************************/
// PROMPT FOR CATALOG SUBSCRIPTION
//************************************************************/
function showCatalogRequestTab() {
    var $ = jQuery;
    if (!$('#emarea16_close').hasClass('active')) {
        $('#emarea16_close').addClass('active');
    }
}

function hideCatalogRequestTab() {
    var $ = jQuery;
    if ($('#emarea16_close').hasClass('active')) {
        $('#emarea16_close').removeClass('active');
    }
}

jQuery(document).ready(function() {

    var $ = jQuery;
    var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|webOS|iPod|BlackBerry|hpwos/i.test(navigator.userAgent);
    var OVERLAY = $('#full-page-overlay');

    $(document).on('click', '.amxnotif-msg', function() {
        $(this).siblings('.amxnotif-form').slideToggle();
    })

    //************************************************************/
    // VARIEN FORMS - VALIDATE EMAIL INPUTS
    //************************************************************/
    $(document).on('keyup touchend paste blur', '.validate-email', function() {
        $('.validate-email').each(function() {
            $(this).on('paste', function (e) {
                e.preventDefault();
                var currVal = "";
                if (window.clipboardData && window.clipboardData.getData) { // IE
                    currVal = window.clipboardData.getData('Text');
                } else {
                    var clipboardData = (e.originalEvent || e).clipboardData;
                    if (clipboardData && clipboardData.getData) {
                        currVal = clipboardData.getData('text/plain');
                    }
                }
                Validation.validate(this);
            });

            $(this).on('blur', function () {
                if (this.value != "") {
                    Validation.validate(this);
                }
            });
        });
    });

    //************************************************************/
    // VARIEN FORMS - PHONE INPUTS
    //************************************************************/
    $('.validate-phoneLax').each(function(){
        var input = this;
        $(input).on('blur paste', function(){
            Validation.validate(input);
        });

        var form = $(input).parents('form:first');
        form.on('submit', function(){
            Validation.validate(input);
        })
    });

    //************************************************************/
    // PRODUCT SWATCHES
    //************************************************************/
    //make this only for current item's swatches - events should still propagate on other items' swatches
    $('img[id^="amconf-image-"]').click(function (e) {
        if ($(this).closest('li.item.swatchExpand').length) {
            e.stopPropagation();
        }
    });

    $('input').each(function(){
        $(this).on('blur paste', function(){
            Validation.validate(this);
        });
    });

    //************************************************************/
    // PDP QTY INPUTS
    //************************************************************/
    $('.qty-val').each(function(){
        var minBtn = $(this).parent().children('.qtymin')[0];

        if ($(this).attr('value') == 1) {
            disableQtyButton(minBtn);
        } else {
            enableQtyButton(minBtn);
        }
    })

    function disableQtyButton(minButton) {
        $(minButton).addClass('disabled');
    }

    function enableQtyButton(minButton) {
        $(minButton).removeClass('disabled');
    }

    $('input.qty-val').on('keyup change', function (e) {
        var minBtn = $(this).parent().children('.qtymin')[0];
        var value = $(this).attr('value').replace(/\D/g, '');

        $(this).attr('value', (isNaN(parseInt(value)) ? "0" : parseInt(value)));

        if (value.charAt(0) == 0) {
            value = value.slice(1, 4);
        }

        if (value <= 1) {
            disableQtyButton(minBtn);
        } else {
            enableQtyButton(minBtn);
        }


        if($("#product-embroidery-options").length) {
            if ($("#product-embroidery-options").val().length !== 0 && value.length !== 0) {

                // Get embroidery data and embroidery tier pricing from DOM.
                this.productEmbroideryOptionsObj = $.parseJSON($("input[id=product-embroidery-options]").val());
                this.embroideryTierPrices = $.parseJSON($("input[id=embroidery-tier-prices]").val());
                var self = this;
                this.qty = value;
                if (this.productEmbroideryOptionsObj != "undefined" && this.embroideryTierPrices != "undefined") {

                    // Fix embroidery data based upon changed qty.
                    // This applies embroidery discounts correctly when updating the qty in the product detail page.
                    var embTotal = 0;
                    var isPriceUpdate = false;
                    var productId = $("input[id=product-id]").val();
                    $.each(this.productEmbroideryOptionsObj[productId].types, function (typeId, type) {

                        this.typeName = type.name;
                        this.typeId = typeId;
                        var self2 = this;
                        if (type.locations != "undefined") {

                            $.each(type.locations, function (locationId, location) {
                                if (location.selectedOption != "undefined" && location.selectedOption.length > 0) {

                                    $.each(location.selectedOption, function (index, option) {

                                        var optionObject = {};
                                        optionObject.id = option.id;
                                        optionObject.name = option.name;
                                        optionObject.price = option.price;
                                        optionObject.stitches = option.stitches;

                                        // Once we have the price we need to modify, find the tier price for the qty and change accordingly.
                                        for (var i = 0; i < self.embroideryTierPrices.length; i++) {
                                            if (self.embroideryTierPrices[i]['name'] == self2.typeName) {
                                                for (var j = 0; j < self.embroideryTierPrices[i]['tier_prices'].length; j++) {
                                                    if (parseInt(self.embroideryTierPrices[i]['tier_prices'][j]['qty_from']) <= parseInt(self.qty) && parseInt(self.qty) <= parseInt(self.embroideryTierPrices[i]['tier_prices'][j]['qty_to'])) {
                                                        if (parseInt(self.embroideryTierPrices[i]['tier_prices'][j]['stitches_count_from']) == 0 && parseInt(self.embroideryTierPrices[i]['tier_prices'][j]['stitches_count_to']) == 0) {
                                                            if (parseFloat(optionObject.price) != parseFloat(self.embroideryTierPrices[i]['tier_prices'][j]['price'])) {
                                                                option.price = self.embroideryTierPrices[i]['tier_prices'][j]['price'];
                                                                isPriceUpdate = true;
                                                            }
                                                        } else {
                                                            if (parseInt(self.embroideryTierPrices[i]['tier_prices'][j]['stitches_count_from']) <= parseInt(optionObject.stitches) && parseInt(optionObject.stitches) <= parseInt(self.embroideryTierPrices[i]['tier_prices'][j]['stitches_count_to'])) {
                                                                if (parseFloat(optionObject.price) != parseFloat(self.embroideryTierPrices[i]['tier_prices'][j]['price'])) {
                                                                    option.price = self.embroideryTierPrices[i]['tier_prices'][j]['price'];
                                                                    isPriceUpdate = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                // Store embroidery data back in DOM.
                if (isPriceUpdate == true) {
                    $("input[id=product-embroidery-options]").val(JSON.stringify(this.productEmbroideryOptionsObj));
                }

                // Price will need to be updated.
                optionsPrice.reload();
            }
        }
    });

    //************************************************************
    // BLUR INPUTS ON IOS ON OFF-CLICK
    //************************************************************/
    function isInput(node) {
        return ['INPUT'].indexOf(node.nodeName) !== -1;
    }
    document.addEventListener('touchstart', function(e) {
        if (!isInput(e.target) && isInput(document.activeElement)) {
            document.activeElement.blur();
        }
    }, false);
    $('input').keyup(function(event) {
        if (event.which === 13) {
            $(this).blur();
        }
    });

    //************************************************************
    // MOBILE QUICK SHOP
    //************************************************************/
    $('.mob-quick-shop').on('click touchstart', function(e){
            e.preventDefault;
            var path = $(this).attr('href');
            $.fancybox({
                'width': EM.QuickShop.QS_FRM_WIDTH,
                'height': EM.QuickShop.QS_FRM_HEIGHT,
                'autoScale': false,
                'padding': 20,
                'margin': 20,
                'type': 'iframe',
                'href': path,
                'onComplete': function () {
                    $.fancybox.showActivity();
                    $('#fancybox-frame').unbind('load');
                    $('#fancybox-frame').bind('load', function () {
                        $.fancybox.hideActivity();
                    });
                }
            });
            e.stopPropagation();
            return false;
        }
    );
    //************************************************************
    // MOBILE QUICK SHOP
    //************************************************************/

    //************************************************************/
    // PROMPT FOR CATALOG SUBSCRIPTION
    //************************************************************/
    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
    }

    function checkURLParameter(name) {
        return (new RegExp("[?&]" + name).test(location.search)) ? true : false;
    }

    function checkSubdomain(prefix) {
        return (new RegExp("[?&]" + prefix).test(location.href)) ? true : false;
    }

    /**
     * Prompt the user to sign in with the sign in drawer
     * This helps when the user has clicked any link that is account related and they are no longer logged in.
     */
    if((getURLParameter('prompt') == 'signin') && (!$('#myaccount').length > 0)) {
        var BASE_URL = getBaseUrl(location.href);
        createSidebarSignin(this, '' + BASE_URL + '/customer/account/login/');
    }
    // Suppress Catalog Request Popup on cart and checkout.
    if( window.location.pathname.indexOf('checkout/cart') < 0 &&
        window.location.pathname.indexOf('checkout/onepage') < 0
    ) {
        var catalogPopupCookie = (getCookie("catalogPopup")) ? getCookie("catalogPopup") : 0;
        var catalogOrderedCookie = (getCookie("catalogOrdered")) ? getCookie("catalogOrdered") : 0;
        var popupAllowed = !checkSubdomain("sage");
        var overridingOffer = checkURLParameter("offer") || checkURLParameter("offer_code");

        catalogPopupCookie++;
        setCookie("catalogPopup", catalogPopupCookie, 30);

        if (popupAllowed) {
            if (catalogOrderedCookie == 1) {
                hideCatalogRequestTab();
            } else {
                if (catalogPopupCookie != 1) {
                    showCatalogRequestTab();
                } else {
                    if (!overridingOffer) {
                        setTimeout(showMiniCatForm, 3000);
                    }
                }
            }

            if (checkURLParameter("catalog") && !overridingOffer) {
                showExpandedCatForm();
            }
        }
    }

    //************************************************************/
    // EQUALIZE RELATED PRODUCT HEIGHTS
    //************************************************************/
    (function(){
       var relatedProd, prodEls = [];
       var curHt = 0;
        var maxHt = 0;
        var i = 0;
        relatedProd = $('.aw-arp-item .product-info');

        relatedProd.each(function () {
            prodEls[i] = $(this);
            curHt = prodEls[i].height();
            if (curHt > maxHt) {
                maxHt = curHt;
            }
            i++;
        });

        for (i = 0; i < prodEls.length; i++) {
            prodEls[i].height(maxHt);
        }
    })();
    //************************************************************/
    // EQUALIZE RELATED PRODUCT HEIGHTS
    //************************************************************/

    //************************************************************/
    // EQUALIZE REVIEWS HEIGHTS
    //************************************************************/
    (function(){
        var reviews, heights = [];
        var i = 0;
        var k = 0;
        var m = 0;

        var cols = !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? 2 : 1;
        var initialLoadCount = !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? 2 : 2;
        var initialHeight = 0;
        var maxHeight = 0;

        reviews = $('#advancereviews-filteredReviews .grid_12 .content_review');

        if(reviews.length && reviews.length > initialLoadCount){
            var extraMargin = !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? 150 : 100;
        }

        reviews.each(function () {
            i++;
            heights.push($(this).height());

            if(i%cols == 0){
                k++;
                maxHeight = Math.max.apply(null, heights);
                reviews.slice(m, m + cols).each(function(){
                    $(this).height(maxHeight);
                });
                heights = [];
                m = m + cols;

                if(k <= initialLoadCount/cols){
                    initialHeight += maxHeight + parseInt($(this).css('marginTop')) + parseInt($(this).css('marginBottom'));
                }
            }
        });

        $("#advancereviews-filteredReviews").height(initialHeight + extraMargin);

        if (reviews.length <= initialLoadCount){
            $('#moreReviews').hide();
            $('#fadingBackHolder').hide();
        }

    })();
    //************************************************************/
    // EQUALIZE REVIEWS HEIGHTS
    //************************************************************/

    //************************************************************/
    // EXPAND VISIBLE PRODUCT REVIEWS
    //************************************************************/
    (function () {
        $("#moreReviews").click(function(){

            var addedHeight = 0;
            var extraDisplayCount = 8;
            var cols = !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? 2 : 1;
            var initialReviewsCount = !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? 2 : 2;

            var reviews = $('#advancereviews-filteredReviews .grid_12');

            var clickCount = $(this).data('clickCount') || 0;
            $(this).data('clickCount', clickCount + 1);
            var i = 0;

            var visibleReviewsCount = ($(this).data('clickCount')) * extraDisplayCount + initialReviewsCount;
            if (visibleReviewsCount > reviews.length) {
                visibleReviewsCount = reviews.length
            }

            reviews.slice(0, visibleReviewsCount).each(function(){
                i++;
                if( i%cols == 0 || (i%cols != 0 && i == visibleReviewsCount)) {
                    addedHeight += $(this).outerHeight() + parseInt($(this).css('marginTop')) + parseInt($(this).css('marginBottom'));
                }
            });

            if( visibleReviewsCount >=  reviews.length){
                $('#moreReviews').hide();
                $('#fadingBackHolder').hide();
                var newHeight = addedHeight;

            } else {
                var newHeight = addedHeight + 120 + 40;
            }

            $("#advancereviews-filteredReviews").animate({height:newHeight}, 200);

        });
    })();
    //************************************************************/
    // EXPAND VISIBLE PRODUCT REVIEWS
    //************************************************************/

    (function () {
        var selectedColor = decodeURIComponent((new RegExp('[?|&]swatch=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        $("#amconf-image-" + selectedColor).trigger( "click" );
    })();

    //************************************************************/
    // SHOW PASSWORD BUTTON
    //************************************************************/
    $('body').on('click', '.password-switch', function(){
        var passwordField = $(this).closest('.password-wrapper').find('.password-field');
        if($(this).text() == 'Show'){
            passwordField.prop('type', 'text');
            $(this).text('Hide');
        } else {
            passwordField.prop('type', 'password');
            $(this).text('Show');
        }
        passwordField.focus();
    });

    //************************************************************/
    // CHECK IF ARRIVING FROM EMAIL LINK PROD-1912
    //************************************************************/
    if (checkURLParameter('utm_campaign') && (!$('#myaccount').length > 0)) {
        var utmCampaign = getURLParameter('utm_campaign');
        if (utmCampaign != null && utmCampaign.match('^cart')) {
            $.fancybox.close();
            var BASE_URL = getBaseUrl(location.href);
            createSidebarSignin(this, '' + BASE_URL + '/customer/account/login/');
        }
    }

    $('.content_close').click(function() {
        showExpandedCatForm();
    });

    $('#catalogSlideoutCloser').on('click', function(){
        if ($('#catalogrequest_slideout').hasClass('expanded')) {
            hideExpandedCatForm();
        } else {
            hideMiniCatForm();
        }
    });

});
//************************************************************/
//END DOCUMENT READY
//************************************************************/

var getSortOrder = function (location, itemId) {
    var $ = jQuery;
    var items = (location == 'savecart') ? $('.saved-cart-container .shopcart-item-container') : $('.bettercheckout .shopcart-item-container');
    var sortOrder = '';

    $(items).each(function(i){
        if ($(this).attr('id') != '' && $(this).attr('id') != undefined) {
            if ($(this).attr('id').split('-')[2] == itemId) {
                sortOrder = i;
            }
        }
    })

    return sortOrder;
}

// function addToShoppinglist(that, itemId, qty) {
function addToShoppinglist(that, qty, sortOrder) {
    var $ = jQuery;
    var itemElId = $(that).closest('.shopcart-item-container').attr('id');
    var location = itemElId.split('-')[0];
    var itemId = itemElId.split('-')[2];

    if (sortOrder == undefined || sortOrder == '') {
        sortOrder = getSortOrder(location, itemId);
    }

    var $ = jQuery;
    var requestParams = {
        "item_id": itemId,
        "qty": qty,
        "sort_order": sortOrder
    };

    $.ajax({
        url: BASE_URL + '/shoppinglist/index/additem/',
        type: 'POST',
        data: requestParams,

        complete: function (response) {
            // to-do: add response message to session
            window.location.reload(true);
        }
    });
}

function removeFromShoppinglist(that, groupId, id, sortOrder) {

    // Use cust ID for groupName
    var $ = jQuery;

    var itemElId = $(that).closest('.shopcart-item-container').attr('id');
    var location = itemElId.split('-')[0];
    var itemId = itemElId.split('-')[2];

    if (sortOrder == undefined || sortOrder == '') {
        sortOrder = getSortOrder(location, itemId);
    }

    var requestParams = {
        "group_id": groupId,
        "id": id,
        "sort_order": sortOrder,
        "is_remove": true
    };

    $.ajax({
        url: BASE_URL + '/shoppinglist/group/removeitem/',
        type: 'POST',
        data: requestParams,

        complete: function (response) {
            // to-do: add response message to session
            window.location.reload(true);
        }
    });
}

function listItemToCart(that, groupId, items, sortOrder) {
    // Use cust ID for groupName
    var $ = jQuery;

    var itemElId = $(that).closest('.shopcart-item-container').attr('id');
    var location = itemElId.split('-')[0];
    var itemId = itemElId.split('-')[2];

    if (sortOrder == undefined || sortOrder == '') {
        sortOrder = getSortOrder(location, itemId);
    }

    var requestParams = {
        "group_id": groupId,
        "items": items,
        "sort_order": sortOrder
    };

    $.ajax({
        url: BASE_URL + '/shoppinglist/group/cart/',
        type: 'POST',
        data: requestParams,

        complete: function (response) {
            // to-do: add response message to session
            window.location.reload(true);
        }
    });
}

function submitFormViaAjax(formId, postUrl, successDivId, spinnerDivId, submitBtn) {
    var myForm = new VarienForm(formId, true);

    if (myForm.validator.validate()) {
        if (submitBtn) {
            showButtonLoader(submitBtn);
        } else {
            jQuery.each(myForm.form, function (index, value) {
                if (value['type'] == 'submit') {
                    submitBtn = jQuery(this);
                    showButtonLoader(submitBtn);
                }
            });
        }

        new Ajax.Updater(
            {success: successDivId},
            postUrl,
            {
                method: 'post',
                asynchronous: true,
                evalScripts: false,
                onComplete: function (request) {

                    var response = request.responseJSON;

                    if (formId == 'catalogrequest') {
                        if (response.error == 0) {
                            jQuery('#' + successDivId).html("");
                            window.location.assign(response.url);
                            setCookie('catalogOrdered', 1, 30);
                            if ($('html, body').hasClass("noscroll")) {
                                $('html, body').removeClass("noscroll");
                            }
                            hideCatalogRequestTab();
                        } else {
                            $$('catalog-request-form').select('input[type="image"]').invoke('enable');

                            jQuery('#' + spinnerDivId).fadeOut(function () {
                                jQuery('#' + successDivId).html(response.message).fadeIn();
                            });
                            setCookie('catalogOrdered', 0, 30);
                            showCatalogRequestTab();
                        }
                    } else if (formId == 'catalogrequestSlideout') {
                        // hideButtonLoader($('#catalogSubmit'));

                        if (response.error == 0) {
                            jQuery('#' + successDivId).html("");
                            setCookie('catalogOrdered', 1, 30);
                            hideCatalogRequestTab();
                            window.location.assign(response.url);
                        } else {
                            setCookie('catalogOrdered', 0, 30);
                            showCatalogRequestTab();
                            jQuery('#' + successDivId).html(response.message).fadeIn();
                        }
                    } else {
                        jQuery('#' + spinnerDivId).fadeOut(function () {
                            jQuery('#' + successDivId).fadeIn();
                            jQuery('#' + formId).find("input[type=text], textarea").val("");
                        });
                    }
                    if (submitBtn.length) {
                        hideButtonLoader(submitBtn);
                    }
                },
                onLoading: function (request, response) {
                    if (formId != 'catalogrequestSlideout') {
                        jQuery('#' + spinnerDivId).fadeIn();
                    }
                },
                parameters: $(formId).serialize(true)
            }
        );
    } else {
        if( $$('catalog-request-form').length){
            $$('catalog-request-form').select('input[type="image"]').invoke('enable');
        }
    }
}

function showExpandedCatForm() {
    var $ = jQuery;
    var slideout = $('#catalogrequest_slideout');

    if (!slideout.hasClass('expanded')) {
        if (!$('html, body').hasClass("noscroll")) {
            $('html, body').addClass("noscroll");
        }

        hideCatalogRequestTab();
        if (!slideout.hasClass('open')) {
            slideout.addClass('open');
            slideout.addClass('expanded', function () {
                $('#catFormContent').slideDown(100, function(){
                    if ($('.grid_16_rpn').css('width') === '620px') {
                        slideout.css({
                            bottom: 0,
                            right: slideout.outerWidth() * (-1)
                        })
                            .animate({right: 0}, 500);
                    } else {
                        slideout.css({
                            right: 0,
                            bottom: slideout.outerHeight() * (-1)
                        })
                            .animate({bottom: 0}, 500);
                    }
                });
            });
        } else {
            slideout.addClass('expanded', function () {
                $('#catFormContent').slideDown(500);
            });
        }
    }

    $('#catalogSubmit').off().on('click', function(){
        submitFormViaAjax('catalogrequestSlideout', $('#catalogrequestSlideout').prop('action'), 'formSuccessMessageSlideout', 'loadSpinnerSlideout', $('#catalogSubmit'));
    });

}

function hideExpandedCatForm() {
    var $ = jQuery;
    var slideout = $('#catalogrequest_slideout');
    var catalogOrderedCookie = (getCookie("catalogOrdered")) ? getCookie("catalogOrdered") : 0;

    if ($('html, body').hasClass("noscroll")) {
        $('html, body').removeClass("noscroll");
    }

    if ($('.grid_16_rpn').css('width') === '620px') {
        slideout.animate({right: slideout.outerWidth()*(-1)}, 500, function(){
            slideout.removeClass('expanded').removeClass('open');
            $('#catFormContent').delay(100).slideUp(500);
        });
    } else {
        slideout.animate({bottom: (slideout.outerHeight()*(-1))}, 500, function(){
            slideout.removeClass('expanded').removeClass('open');
            $('#catFormContent').delay(100).slideUp(500);
        });
    }

    if (catalogOrderedCookie == 0) {
        showCatalogRequestTab();
    }
}

function showMiniCatForm() {
    var $ = jQuery;
    var slideout = $('#catalogrequest_slideout');

    if (!slideout.hasClass('open')) {
        hideCatalogRequestTab();
        $('#catFormContent').css('display', 'none');
        slideout.removeClass('expanded').addClass('open', function(){

            if ($('.grid_16_rpn').css('width') === '620px') {
                slideout.css({
                    bottom: 0,
                    right: slideout.outerWidth() * (-1)
                })
                    .animate({right: 0}, 500);
            } else {
                slideout.css({
                    right: 0,
                    bottom: slideout.outerHeight() * (-1)
                })
                    .animate({bottom: 0}, 500);
            }

            $('#catalogSubmit').off().on('click', function(){
                showExpandedCatForm();
            });
        });
    }
}

function hideMiniCatForm() {
    var $ = jQuery;
    var slideout = $('#catalogrequest_slideout');
    var catalogOrderedCookie = (getCookie("catalogOrdered")) ? getCookie("catalogOrdered") : 0;

    if ($('html, body').hasClass("noscroll")) {
        $('html, body').removeClass("noscroll");
    }

    if ($('.grid_16_rpn').css('width') === '620px') {
        slideout.animate({right: slideout.outerWidth()*(-1)}, 500, function(){
            slideout.removeClass('open');
        });
    } else {
        slideout.animate({bottom: slideout.outerHeight()*(-1)}, 500, function(){
            slideout.removeClass('open');
        });
    }

    if (catalogOrderedCookie == 0) {
        showCatalogRequestTab();
    }
}

function setCookie(cname, cvalue, exdays) {
    if (Object.prototype.toString.call(exdays) === '[object Date]') {
        var d = exdays;
    } else {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    }
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";path=/;" + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function loadAltImage(imageSrc, imageSrcZoom) {
    var $ = jQuery;

    $('#amasty_zoom,#carousel-prod-main-img').attr('src', imageSrc);
    $('#amasty_zoom,#carousel-prod-main-img').attr('data-zoom-image', imageSrcZoom);
    $('#amasty_zoom,#carousel-prod-main-img').attr('data-image-src', imageSrcZoom);
    $('.zoomLens').css("background-image", 'url(' + imageSrcZoom + ')');

    var ez = $("#amasty_zoom").data('elevateZoom');
    if (typeof ez != "undefined") {
        ez.imageSrc = imageSrcZoom;
        ez.zoomImage = imageSrcZoom;
    }
}

//************************************************************/
// COUNTDOWN TIMER
//************************************************************/
function getTimeRemaining(endtime){
    var tdiff = Date.parse(endtime) - Date.parse(new Date());
    var tdiffInHours = Math.floor( (tdiff/(1000*60*60)));
    var days = Math.floor( tdiff/(1000*60*60*24) );
    var hours = Math.floor( (tdiff/(1000*60*60)) % 24 );
    var minutes = Math.floor( (tdiff/1000/60) % 60 );
    var seconds = Math.floor( (tdiff/1000) % 60 );

    if (tdiff <= 0) {
        return false;
    } else {
        if (tdiffInHours > 36) {
            return {
                'days': {'label': 'd', 'value': days},
                'hours': {'label': 'h', 'value': hours},
                'minutes': {'label': 'm', 'value': minutes},
                'seconds': {'label': 's', 'value': seconds}
            };
        } else if (tdiffInHours >= 1) {
            return {
                'hours': {'label': 'h', 'value': tdiffInHours},
                'minutes': {'label': 'm', 'value': minutes},
                'seconds': {'label': 's', 'value': seconds}
            };
        } else if (tdiffInHours < 1) {
            return {
                'minutes': {'label': 'm', 'value': minutes},
                'seconds': {'label': 's', 'value': seconds}
            };
        }
    }
}

function initializeCountdown(id, endtime){
    var $ = jQuery;
    var clock = $('#' + id);

    function updateCountdown(){
        var t = getTimeRemaining(endtime);
        var countdownInterval = setTimeout(updateCountdown, 1000);
        var freshGuts = $('<span></span>').addClass('countdown-wrap');
        if (t) {
            for (key in t) {
                if (typeof(t[key]) == 'object' && t.hasOwnProperty(key)) {
                    unitWrap = $('<span></span>').addClass(key + '-wrap unit-wrap');
                    for (item in t[key]) {
                        if ((t[key]).hasOwnProperty(item)) {
                            if (item == 'value') {
                                unitVal = $('<span></span>').addClass('count');
                                if (key == 'days') {
                                    unitVal.html(t[key][item]);
                                } else {
                                    unitVal.html(('0' + t[key][item]).slice(-2));
                                }
                            }
                            if (item == 'label') {
                                unitLbl = $('<span></span>').addClass('label');
                                unitLbl.html(t[key][item]);
                            }
                        }
                    }
                    unitWrap.append(unitVal, unitLbl);
                }
                freshGuts.append(unitWrap);
            }
            clock.empty().append(freshGuts);
        } else {
            clock.empty().html('EXPIRED');
            clearTimeout(countdownInterval);
        }
    }

    updateCountdown(); // run function once at first to avoid delay
}


//************************************************************/
// SUBMIT OFFER CODE
//************************************************************/
function checkOfferParameter(name) {
    return (!new RegExp("[?&]" + name).test(location.search)) ? false : true;
}

function getOfferParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
}

/*-- GOOGLE ANALYTICS --------------------------------*/
//************************************************************/
jQuery(document).on('login_logindrawer', function(){
    _gaq.push(['_trackEvent', 'Login', 'Login Drawer']);
});

jQuery(document).on('createaccount_logindrawer', function(){
    _gaq.push(['_trackEvent', 'Create Account Open', 'Login Drawer']);
});

jQuery(document).on('forgotpassword_logindrawer', function(){
    _gaq.push(['_trackEvent', 'Forgot Password Open', 'Login Drawer']);
});

jQuery(document).on('login_page_view', function(){
    _gaq.push(['_set', 'title', 'Customer Login']);
    _gaq.push(['_trackPageview', '/customer/account/login/']);
});

