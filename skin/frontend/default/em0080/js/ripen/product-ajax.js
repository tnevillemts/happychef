
jQuery(document).ready(function() {
    //loadProductWithAjax();
    //loadProductPriceWithAjax();
});

function loadProductWithAjax(){

    jQuery('.product-item').each(function(){
            var productId = jQuery(this).attr("id").substring(16);
            //catalog_product_XXX

            var url = jQuery('#myCartLinkId').attr('href');
            var tmp	=	url.search("checkout/cart/");
            var baseurl		=	url.substr(0,tmp);

            jQuery.ajax({
                url: baseurl + "/amshopby/index/loadProductThumb",
                data: {id: productId},
                type: "POST",
                success: function(data) {
                    jQuery('#catalog_product_'+productId).html(data);
                },
                error: function(data) {
                    //alert("Test");
                }
            });

        });
}

function loadProductPriceWithAjax(){

    jQuery('.product-item-price').each(function(){
        var productId = jQuery(this).attr("id").substring(22);
        //catalog_product_price_XXX

        var url = jQuery('#myCartLinkId').attr('href');
        var tmp	=	url.search("checkout/cart/");
        var baseurl		=	url.substr(0,tmp);

        jQuery.ajax({
            url: baseurl + "/amshopby/index/loadProductPrice",
            data: {id: productId},
            type: "POST",
            success: function(data) {
                jQuery('#catalog_product_price_'+productId).html(data);
            },
            error: function(data) {
                //alert("Test");
            }
        });

    });
}