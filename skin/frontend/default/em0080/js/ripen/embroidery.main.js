jQuery.noConflict();
(function ($) {

    $(document).ready(function($) {

        initializeEmbroidery = function() {

            // Initialize embroidery for product page
            if ($('.add-to-cart #qty').length) {

                //open the lateral panel
                // $('.open-embroidery-btn').on('click', function (event) {
                //     event.preventDefault();
                //     openSlideIn();
                // });

                var productId = $('#product-id').val();
                var quoteItemId = $('#quote-item-id').val();

                new Ajax.Request('/embroidery/index/getEmbroideryData', {
                    method: 'post',
                    dataType: 'json',
                    parameters: {'id': productId, 'quoteItemId': quoteItemId},
                    onComplete: function (response) {

                        var embroideryData = response.responseJSON;
                        var container = $("#add-to-cart-area")[0];

                        // Create embroidery view model
                        var embroideryViewModel = new EmbroideryViewModel(productId, quoteItemId, embroideryData);

                        // Initialize embroidery view model
                        embroideryViewModel.initialize();

                        // Save embroidery tier price information in DOM.
                        var embroideryDataTypes = embroideryData['types'];
                        $('#bind-embroidery').append('<input type="hidden" id="embroidery-tier-prices" id="embroidery-tier-prices" value="" />');
                        $("input[id=embroidery-tier-prices]").val(JSON.stringify(embroideryDataTypes));

                        $('.add-to-cart #qty').keyup(function () {
                            if (embroideryViewModel.keyupIsTriggered() == false) {
                                embroideryViewModel.updateEmbroideryPrice();
                                embroideryViewModel.keyupIsTriggered(false);
                            }
                        });

                        // Bind view models to view
                        ko.applyBindings(embroideryViewModel, container);

                        signinEmailValidation = embroideryViewModel.signInEmail.subscribe(function (value) {
                            embroideryViewModel.signInEmail(value);
                        });

                        createAccountEmailValidation = embroideryViewModel.createAccountEmail.subscribe(function (value) {
                            embroideryViewModel.createAccountEmail(value);
                        });

                        forgotPwdEmailValidation = embroideryViewModel.passwordEmail.subscribe(function (value) {
                            embroideryViewModel.passwordEmail(value);
                        });

                        hasEmbroidery = embroideryViewModel.hasEmbroidery.subscribe(function (ischosen) {
                            if (ischosen) {
                                $('#addEmbWrap').removeClass('active');
                                $('#editEmbWrap').addClass('active');
                            } else {
                                $('#editEmbWrap').removeClass('active');
                                $('#addEmbWrap').addClass('active');
                            }
                        });

                        embroideryOpen = embroideryViewModel.isOpen.subscribe(function (isopen) {
                            if (isopen) {
                                $('html, body').addClass('noscroll');
                                // If the current embroidery panel is nested within an iframe, hide the parent slide-in's header
                                if ($('.cart-sidebar-edit', window.parent.document) != 'undefined' && $('.cart-sidebar-edit', window.parent.document).length) {
                                    $('.cart-sidebar-edit .si-header', window.parent.document).css('display', 'none');
                                }
                            } else {
                                $('html, body').removeClass('noscroll');
                                // If the current embroidery panel is nested within an iframe, show the parent slide-in's header
                                if ($('.cart-sidebar-edit', window.parent.document) != 'undefined' && $('.cart-sidebar-edit', window.parent.document).length) {
                                    $('.cart-sidebar-edit .si-header', window.parent.document).css('display', 'block');
                                }
                            }
                        });

                        gaEmbLblsComplete = embroideryViewModel.gaLabelsComplete.subscribe(function (galabelscomplete) {
                            if (galabelscomplete) {
                                $('.btn-cart.btn-cartf').prop('disabled', false).removeClass('disabled');
                            } else {
                                $('.btn-cart.btn-cartf').prop('disabled', true).addClass('disabled');
                            }
                        });

                        gaEmbAddToCart = embroideryViewModel.gaLabels.subscribe(function (galabels) {
                            $('.btn-cartf').on('click', function() {
                                if ($('.emb-btn-wrap.btn-visible #updateEmbroidery').length) {
                                    /*-- GOOGLE ANALYTICS --------------------------------*/
                                    _gaq.push(['_trackEvent', 'customization', 'added-to-cart', galabels]);
                                    /*-- GOOGLE ANALYTICS --------------------------------*/
                                }
                            });
                        });

                        $(".emb-toggle, #editEmb").on('click', function () {
                            embroideryViewModel.validateSuperAttributesAndOpen();
                        });

                        $('#removeEmb').on('click', function () {
                            embroideryViewModel.removeEmbroidery();
                        });

                    }
                });
            }

            // Initialize embroidery for cart page
            if ($('#bettercheckout-cart-form').length) {

                // $('.open-cart-embroidery').on('click', function (event) {
                //
                //     event.preventDefault();
                //
                //     $('.si-rt').removeClass('vis');
                //     var quoteItemId = this.id.split('-')[2];
                //     $('#bind-embroidery-' + quoteItemId + ' .si-rt').addClass('vis');
                //
                // });

                $('.open-cart-embroidery').each(function () {

                    var productId = this.id.split('-')[1];
                    var quoteItemId = this.id.split('-')[2];
                    var isSavedItem = $(this).hasClass('saved-item');

                    new Ajax.Request('/embroidery/index/getEmbroideryData', {
                        method: 'post',
                        dataType: 'json',
                        parameters: {
                            'id': productId,
                            'quoteItemId': quoteItemId,
                            'isSavedItem': isSavedItem
                        },
                        onComplete: function (response) {

                            var embroideryData = response.responseJSON;
                            var container = $("#bind-embroidery-" + quoteItemId)[0];

                            // Create embroidery view model
                            var embroideryViewModel = new EmbroideryViewModel(productId, quoteItemId, embroideryData);

                            // Initialize embroidery view model
                            embroideryViewModel.initialize();

                            // Bind view models to view
                            ko.applyBindings(embroideryViewModel, container);

                            embroideryOpen = embroideryViewModel.isOpen.subscribe(function (isopen) {
                                if (isopen) {
                                    $('html, body').addClass('noscroll');
                                    // If the current embroidery panel is nested within an iframe, hide the parent slide-in's header
                                    if ($('.cart-sidebar-edit', window.parent.document) != 'undefined' && $('.cart-sidebar-edit', window.parent.document).length) {
                                        $('.cart-sidebar-edit .si-header', window.parent.document).css('display', 'none');
                                    }
                                } else {
                                    $('html, body').removeClass('noscroll');
                                    // If the current embroidery panel is nested within an iframe, show the parent slide-in's header
                                    if ($('.cart-sidebar-edit', window.parent.document) != 'undefined' && $('.cart-sidebar-edit', window.parent.document).length) {
                                        $('.cart-sidebar-edit .si-header', window.parent.document).css('display', 'block');
                                    }
                                }
                            });
                        }
                    });
                });


                $('.open-saved-cart-embroidery').each(function () {

                    var productId = this.id.split('-')[1];
                    var quoteItemId = this.id.split('-')[2];
                    var isSavedItem = $(this).hasClass('saved-item');

                    new Ajax.Request('/embroidery/index/getEmbroideryData', {
                        method: 'post',
                        dataType: 'json',
                        parameters: {'id': productId, 'quoteItemId': quoteItemId, 'isSavedItem': isSavedItem},
                        onComplete: function (response) {

                            var embroideryData = response.responseJSON;
                            var container = $("#bind-embroidery-" + quoteItemId)[0];

                            // Create embroidery view model
                            var embroideryViewModel = new EmbroideryViewModel(productId, quoteItemId, embroideryData);

                            // Initialize embroidery view model
                            embroideryViewModel.initialize();

                            // Bind view models to view
                            ko.applyBindings(embroideryViewModel, container);

                            embroideryOpen = embroideryViewModel.isOpen.subscribe(function (isopen) {
                                if (isopen) {
                                    $('html, body').addClass('noscroll');
                                    // If the current embroidery panel is nested within an iframe, hide the parent slide-in's header
                                    if ($('.cart-sidebar-edit', window.parent.document) != 'undefined' && $('.cart-sidebar-edit', window.parent.document).length) {
                                        $('.cart-sidebar-edit .si-header', window.parent.document).css('display', 'none');
                                    }
                                } else {
                                    $('html, body').removeClass('noscroll');
                                    // If the current embroidery panel is nested within an iframe, show the parent slide-in's header
                                    if ($('.cart-sidebar-edit', window.parent.document) != 'undefined' && $('.cart-sidebar-edit', window.parent.document).length) {
                                        $('.cart-sidebar-edit .si-header', window.parent.document).css('display', 'block');
                                    }
                                }
                            });
                        }
                    });
                });

            }

            $('.bind-embroidery-cart-filter').each(function () {
                var container = $(this);
                var setupQuoteItemId = container.attr('data-setup-quote-item-id');

                $(this).find('.filter-embroidery-open-alert').on('click', function() {
                    $.fancybox({
                        href: "#filter-embroidery-alert-" + setupQuoteItemId,
                        modal: true,
                        maxWidth: 280,
                        transitionIn: 'fade',
                        transitionOut: 'fade',
                        showCloseButton: false,
                        afterLoad: function () {
                            $(".fancybox-wrap").addClass("si-alert-wrap");
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                            _gaq.push(['_trackEvent', 'embroidery-dialog-shown', 'Cart - Remove Custom Logo', 'Removing fee will affect all products with this custom logo applied.']);
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                        }
                    });
                });

                $('#filter-embroidery-alert-' + setupQuoteItemId + ' .confirm-btn').on('click', function() {
                    new Ajax.Request('/bettercheckout/cart/removeEmbroideryCustomLogo', {
                        method: 'post',
                        parameters: {"setupQuoteItemId": setupQuoteItemId},
                        onSuccess: function (response) {
                            location.reload();
                        }
                    });
                });

                $('#filter-embroidery-alert-' + setupQuoteItemId + ' .cancel-btn').on('click', function() {
                    $.fancybox.close();
                });
            });
            $('.shopcart-cols-container .info-tooltip-icon').each(function() {
                var tooltipId = $(this).attr('data-tooltip-id');
                $(this).on('mouseenter', function() {
                    $('.info-tooltip-content').removeClass('show-info-tooltip-content');
                    $('#info-tooltip-content-' + tooltipId).toggleClass('show-info-tooltip-content');
                    _gaq.push(['_trackEvent', 'Cart - Tooltip Show', 'Embroidery Logo', 'Icon Mouseenter']);
                    return false;
                });
                $(this).on('mouseleave', function() {
                    $('#info-tooltip-content-'+tooltipId).toggleClass('show-info-tooltip-content');
                    _gaq.push(['_trackEvent', 'Cart - Tooltip Hide', 'Embroidery Logo', 'Icon Mouseleave']);
                    return false;
                });
                $(this).on('touchstart click', function() {
                    var detailsIconClickAction = (!$('#info-tooltip-content-' + tooltipId).hasClass('show-info-tooltip-content')) ? 'Show' : 'Hide' ;
                    $('.info-tooltip-content').removeClass('show-info-tooltip-content');
                    if(detailsIconClickAction == 'Show') {
                        $('#info-tooltip-content-' + tooltipId).addClass('show-info-tooltip-content');
                    }
                    _gaq.push(['_trackEvent', 'Cart - Tooltip ' + detailsIconClickAction, 'Embroidery Logo', 'Icon Click ']);
                    return false;
                });
                $('#info-tooltip-content-' + tooltipId + ' .info-tooltip-close-content').on('touchstart click', function() {
                    $('#info-tooltip-content-' + tooltipId).toggleClass('show-info-tooltip-content');
                    _gaq.push(['_trackEvent', 'Cart - Tooltip Hide', 'Embroidery Logo', 'Icon Details Close']);
                    return false;
                });
            });
            $('.embroidery-details-toggle').each(function() {
                var detailsId = $(this).attr('data-embroidery-details-id');
                var toggleElement = this;
                $(this).on('click', function() {
                    var detailsAction = (!$('#embroidery-details-' + detailsId).hasClass('embroidery-details-expanded')) ? 'Expand' : 'Contract' ;
                    $(toggleElement).toggleClass('embroidery-toggle-expanded');
                    $('#embroidery-details-' + detailsId).toggleClass('embroidery-details-expanded');
                    _gaq.push(['_trackEvent', 'Cart - Embroidery Breakdown', 'Embroidery', detailsAction]);
                });
            });
        };

        /*-- GOOGLE ANALYTICS --------------------------------*/
        $(document).on('embroidery-logo-login', function() {
            _gaq.push(['_trackEvent', 'Login Open', 'Embroidery', 'Logo Panel']);
        });

        $(document).on('embroidery-createaccount-login', function() {
            _gaq.push(['_trackEvent', 'Login Open', 'Embroidery', 'Create Account Panel']);
        });

        $(document).on('embroidery-go-back-icon', function(){
            _gaq.push(['_trackEvent', 'embroidery-navigation', 'navigation', 'embroidery-go-back']);
        });

        $(document).on('embroidery-close-panel-icon', function(){
            _gaq.push(['_trackEvent', 'embroidery-navigation', 'navigation', 'embroidery-close-icon']);
        });

        $(document).on('click', '#addEmbBtn', function(){
            _gaq.push(['_trackEvent', 'embroidery-navigation', 'navigation', 'embroidery-add-more']);
        });

        $(document).on('click', '.save-emb-btn', function(){
            _gaq.push(['_trackEvent', 'embroidery-navigation', 'navigation', 'embroidery-done']);
        });

        /*-- GOOGLE ANALYTICS --------------------------------*/
        initializeEmbroidery();
    });

})(jQuery);
