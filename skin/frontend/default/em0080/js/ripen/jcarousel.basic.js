/**
 * Created by zcox on 8/10/17.
 */
// (function($) {
jQuery(document).ready(function($){
    $(function() {
        // $('.jcarousel').jcarousel();
        $('.jcarousel').jcarousel().jcarouselSwipe();

        // $('.jcarousel-control-prev')
        //     .on('jcarouselcontrol:active', function() {
        //         $(this).removeClass('inactive');
        //     })
        //     .on('jcarouselcontrol:inactive', function() {
        //         $(this).addClass('inactive');
        //     })
        //     .jcarouselControl({
        //         target: '-=1'
        //     });
        //
        // $('.jcarousel-control-next')
        //     .on('jcarouselcontrol:active', function() {
        //         $(this).removeClass('inactive');
        //     })
        //     .on('jcarouselcontrol:inactive', function() {
        //         $(this).addClass('inactive');
        //     })
        //     .jcarouselControl({
        //         target: '+=1'
        //     });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination();

        $('#amconf-images-92 .amconf-image').on('click', function(){
            $('.jcarousel').jcarousel('scroll', 0);
        })

        $('.jcarousel').jcarouselSwipe({
            perSwipe: 1
        });


    });
});
// })(jQuery);
