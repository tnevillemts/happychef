jQuery(document).ready(function($){

    // This button will increment the value
    $('.qtyplus, .qtymin').click(function(e){
        var qtyButton = $(e.target);
        var minBtn = qtyButton.parent().children('.qtymin')[0];
        var targetInput = qtyButton.parent().children('.qty-val')[0];
        var targetInput = $(targetInput);
        // Stop acting like a button
        e.preventDefault();
        // Get its current value
        var currentVal = parseInt(targetInput.val());
        // Increment
        if (qtyButton.hasClass('qtyplus')) {
            targetInput.attr("value", currentVal + 1);
        }
        // Decrement
        else if (qtyButton.hasClass('qtymin')) {
            if (currentVal > 1) {
                targetInput.attr("value", currentVal - 1);
            }
        }
        targetInput.trigger('change');
    })

});






