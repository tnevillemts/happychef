const forgotPasswordIframeId = 'forgotIframe';

function parseUrl(url) {
    var pattern = RegExp("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
    var matches = url.match(pattern);

    return matches;
}

function getBaseUrl(url) {
    var matches = parseUrl(url);
    return matches[1] + matches[3];
}

function closeSidebarSignin() {
    var $ = jQuery;
    if ($('.sidebar-signin') != 'undefined' && $('.sidebar-signin').length) {
        if ($('#sidebar-iframe-overlay').hasClass('active')) {
            $('#sidebar-iframe-overlay').removeClass('active');
        }
        if ($('.si').hasClass('vis')) {
            $('.si').removeClass('vis');
        }
        if ($('html, body').hasClass('noscroll')) {
            $('html, body').removeClass('noscroll');
        }
        if ($('#full-page-overlay').hasClass('active')) {
            $('#full-page-overlay').removeClass('active').off('click', closeSidebarSignin());
        }

        destroySidebarSignin();
    }
}

function destroySidebarSignin() {
    var $ = jQuery;

    if ($('.sidebar-signin') != 'undefined' && $('.sidebar-signin').length) {
        $('.sidebar-signin').remove();
    }
}

function openSidebarSignin() {
    var $ = jQuery;

    if(typeof NAV_WRAP === 'undefined') {
        NAV_WRAP = $('#topmenu-wrap');
    }

    if (NAV_WRAP.length && NAV_WRAP.hasClass('open')) {
        NAV_WRAP.removeClass('open');
    }

    if ($('.dropdown-cart').length && $('.dropdown-cart').hasClass('open')) {
            $('.dropdown-cart').removeClass('open');
    }

    if ($('.sidebar-signin')[0] && !$('.sidebar-signin').hasClass('vis')) {
        setTimeout(function () {
            $('.sidebar-signin').addClass('vis');
        }, 250);
    }
    if ($('#full-page-overlay') && !$('#full-page-overlay').hasClass('active')) {
        $('#full-page-overlay').addClass('active');
    }

    $('#full-page-overlay').on('click', function(){closeSidebarSignin()});

    $('html, body').addClass('noscroll');
    // setTimeout(createSlidein, 5000);
}

function createSidebarSignin(el, url) {
    var $ = jQuery;

    if (!$(el).hasClass('login-link')) {
        /*-- HOTJAR TAG --------------------------------*/
        hj('tagRecording', ['Sign In - Checkout']);
        /*-- HOTJAR TAG --------------------------------*/
    } else {
        /*-- HOTJAR TAG --------------------------------*/
        hj('tagRecording', ['Sign In - ROS']);
        /*-- HOTJAR TAG --------------------------------*/
    }

    if ($('.sidebar-signin') == 'undefined' || !$('.sidebar-signin').length) {
        var baseUrl = getBaseUrl(url);
        var html = '<div id="sidebar-signin" class="si si-rt sidebar-iframe-container sidebar-signin">' +
            '<div id="sidebar-iframe-overlay" class="sidebar-iframe-overlay"></div>' +
            '<div class="si-viewport">' +

            '<div id="signinPnl" class="si-panel slideRight currPnl" data-panel-level="1">' +
            '<div class="si-header">' +
            '<div class="row-l1">' +
            '<div class="row-content">' +
            '<div class="si-panel-title">SIGN IN</div>' +
            '</div>' +
            '<div class="si-icon-wrap vcenter si-icon-wrap-rt">' +
            '<svg id="sidebar-signin-close" class="si-close" onclick="closeSidebarSignin();" viewBox="0 0 26 26">' +
            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + baseUrl + '/skin/frontend/default/em0080/images/icons/si-icon.svg#close"></use>' +
            '</svg>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="sidebar-iframe-wrap sidebar-signin-wrap">' +
            '<iframe id="signinIframe" class="sidebarIframe" src="' + url + '"><div class="iframe-loading"></div></iframe>' +
            '</div>' +
            '</div>' +

            '<div id="registerPnl" class="si-panel prevPnl slideRight" data-panel-level="2">' +
            '<div class="si-header">' +
            '<div class="row-l1">' +
            '<div id="registerBack" class="si-icon-wrap vcenter si-icon-wrap-lt">' +
            '<svg class="si-arrow-lt" viewBox = "0 0 26 26">' +
            '<use xlink:href="' + baseUrl + '/skin/frontend/default/em0080/images/icons/si-icon.svg#arrow"></use>' +
            '</svg>' +
            '</div>' +
            '<div class="row-content">' +
            '<div class="si-panel-title">CREATE ACCOUNT</div>' +
            '</div>' +
            '<div class="si-icon-wrap vcenter si-icon-wrap-rt">' +
            '<svg id="sidebar-register-close" class="si-close" onclick="closeSidebarSignin();" viewBox="0 0 26 26">' +
            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + baseUrl + '/skin/frontend/default/em0080/images/icons/si-icon.svg#close"></use>' +
            '</svg>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="sidebar-iframe-wrap sidebar-register-wrap">' +
            '<iframe id="registerIframe" class="sidebarIframe" src="' + baseUrl + '/customer/account/create"><div class="iframe-loading"></div></iframe>' +
            '</div>' +
            '</div>' +

            '<div id="forgotPnl" class="si-panel prevPnl slideRight" data-panel-level="2">' +
            '<div class="si-header">' +
            '<div class="row-l1">' +
            '<div id="forgotBack" class="si-icon-wrap vcenter si-icon-wrap-lt">' +
            '<svg class="si-arrow-lt" viewBox = "0 0 26 26">' +
            '<use xlink:href="' + baseUrl + '/skin/frontend/default/em0080/images/icons/si-icon.svg#arrow"></use>' +
            '</svg>' +
            '</div>' +
            '<div class="row-content">' +
            '<div class="si-panel-title">RESET PASSWORD</div>' +
            '</div>' +
            '<div class="si-icon-wrap vcenter si-icon-wrap-rt">' +
            '<svg id="sidebar-forgot-close" class="si-close" onclick="closeSidebarSignin();" viewBox="0 0 26 26">' +
            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + baseUrl + '/skin/frontend/default/em0080/images/icons/si-icon.svg#close"></use>' +
            '</svg>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="sidebar-iframe-wrap sidebar-forgot-wrap">' +
            '<iframe id="'+forgotPasswordIframeId+'" class="sidebarIframe" src="' + baseUrl + '/customer/account/forgotpassword"><div class="iframe-loading"></div></iframe>' +
            '</div>' +
            '</div>' +

            '</div>' +
            '</div>';

        $('body').append(html);
    }

    $('#registerBack').on('click', function(){
        if ($('#signinIframe') != 'undefined' && $('#signinIframe').length) {
            $('#registerPnl').addClass('inactivePnl').addClass('prevPnl').removeClass('currPnl').removeClass('slideRight').addClass('slideLeft');
            $('#forgotPnl').addClass('inactivePnl').addClass('prevPnl').removeClass('currPnl').removeClass('slideRight').addClass('slideLeft');
            $('#signinPnl').removeClass('inactivePnl').addClass('currPnl').removeClass('prevPnl').removeClass('slideLeft').addClass('slideRight');
        }
    });

    $('#forgotBack').on('click', function() {
        if ($('#signinIframe') != 'undefined' && jQuery('#signinIframe').length) {
            $('#registerPnl').addClass('inactivePnl').removeClass('prevPnl').removeClass('currPnl').removeClass('slideRight').addClass('slideLeft');
            $('#forgotPnl').addClass('inactivePnl').addClass('prevPnl').removeClass('currPnl').removeClass('slideRight').addClass('slideLeft');
            $('#signinPnl').removeClass('inactivePnl').addClass('currPnl').removeClass('prevPnl').removeClass('slideLeft').addClass('slideRight');
        }
    })

    openSidebarSignin();
}

function internalCloseSidebarSignin(redirect, $form) {
    var $ = jQuery;
    if ($('#sidebar-signin', window.parent.document) != 'undefined' && $('#sidebar-signin', window.parent.document).length) {
        if ($('.si-overlay', window.parent.document).hasClass('active')) {
            $('.si-overlay', window.parent.document).removeClass('active');
        }

        if ($('.si', window.parent.document).hasClass('vis')) {
            $('.si', window.parent.document).removeClass('vis');
        }

        if ($('html, body', window.parent.document).hasClass('noscroll')) {
            $('html, body', window.parent.document).removeClass('noscroll');
        }
    }

    if (redirect != '' && redirect !== undefined) {
        // Only allow relative redirects (enforced by applying our own protocol & domain) for security.
        var baseUrl = getBaseUrl($form.prop('action'));
        var fullRedirect = (baseUrl + '/' + redirect);
        window.top.location.href = fullRedirect;
    } else {
        window.parent.location.reload();
    }
}

var signInCallback = function($form, err, redirect) {
    var $ = jQuery;
    var parentOverlay = window.parent.document.getElementById('full-page-overlay');
    var parentOverlayClone = parentOverlay.cloneNode(true);
    var parentClose = window.parent.document.getElementById('sidebar-signin-close');
    if (err == 'noerror') {
        parentOverlay.parentNode.replaceChild(parentOverlayClone, parentOverlay);
        parentClose.onclick = null;
        internalCloseSidebarSignin(redirect, $form);
    } else {
        if ($form.parent().find('ul.messages').length) {
            $form.parent().find('ul.messages').css('display', 'none').html('<li class="error-msg">' + err + '</li>');
            $form.parent().find('ul.messages').slideDown('fast');
        } else {
            $('<ul class="messages" style="display:none;"><li class="error-msg">' + err + '</li></ul>').insertBefore($form).slideDown('fast');
        }
        $('body').animate({scrollTop: $form.offset().top - 20}, 'slow');
        /*-- GOOGLE ANALYTICS --------------------------------*/
        _gaq.push(['_trackEvent', 'Login Error', 'Login Drawer', err]);
        /*-- GOOGLE ANALYTICS --------------------------------*/
    }
}

var resetPwdCallback = function($form, err) {
    var $ = jQuery;
    if (err == 'noerror') {
        $form.submit();
    } else {
        if ($form.parent().find('ul.messages').length) {
            $form.parent().find('ul.messages').css('display', 'none').html('<li class="error-msg">' + err + '</li>');
            $form.parent().find('ul.messages').slideDown('fast');
        } else {
            $('<ul class="messages" style="display:none;"><li class="error-msg">' + err + '</li></ul>').insertBefore($form).slideDown('fast');
        }
        $('body').animate({scrollTop: $form.offset().top - 20}, 'slow');
        /*-- GOOGLE ANALYTICS --------------------------------*/
        _gaq.push(['_trackEvent', 'Reset Password Error', 'Login Drawer', err]);
        /*-- GOOGLE ANALYTICS --------------------------------*/
    }
}

var forgotPwdCallback = function($form, err) {
    var $ = jQuery;
    var iframeUrl = window.parent.document.getElementById(forgotPasswordIframeId).getAttribute('src');
    var baseurl = getBaseUrl(iframeUrl);
    if (err == 'noerror') {
        $('#forgotPasswordBack', window.parent.document).remove();
        $('#'+forgotPasswordIframeId, window.parent.document).attr('src', baseurl + '/customer/account/login?referredFromForgotPwd=1');
    } else {
        if ($form.parent().find('ul.messages').length) {
            $form.parent().find('ul.messages').css('display', 'none').html('<li class="error-msg">' + err + '</li>');
            $form.parent().find('ul.messages').slideDown('fast');
        } else {
            $('<ul class="messages" style="display:none;"><li class="error-msg">' + err + '</li></ul>').insertBefore($form).slideDown('fast');
        }
        /*-- GOOGLE ANALYTICS --------------------------------*/
        _gaq.push(['_trackEvent', 'Forgot Password Error', 'Login Drawer', err]);
        /*-- GOOGLE ANALYTICS --------------------------------*/
        $('body').animate({scrollTop: $form.offset().top - 20}, 'slow');
    }
}

function disableSubmit(button) {
    var $ = jQuery;
    var dots = 0;

    if (button && button != 'undefined') {
        var btnPrefix = $(button).find('.static-prefix')
        var presubmitText = btnPrefix.text().toLowerCase();
        var submitText = '';
        switch (presubmitText) {
            case 'create account':
                submitText = 'creating account';
                break;
            case 'sign in':
                submitText = 'signing in';
                break;
            default:
                submitText = 'submitting';
        }

        if (!$(button).hasClass('disabled')) {
            $(button).addClass('disabled').attr('disabled', 'disabled');
        }

        $(button).find('.static-prefix').html(submitText + "<div class='animated-dots'></div>");
    }
}

function enableSubmit(button) {
    var $ = jQuery;
    if (button && button != 'undefined') {
        var btnPrefix = $(button).find('.static-prefix')
        var submitText = btnPrefix.text().toLowerCase();
        var presubmitText = '';
        switch (submitText) {
            case 'creating account':
                presubmitText = 'create account';
                break;
            case 'signing in':
                presubmitText = 'sign in';
                break;
            default:
                presubmitText = 'submit';
        }

        $(button).find('.static-prefix').html(presubmitText + "<div class='animated-dots'></div>");

        if ($(button).hasClass('disabled')) {
            $(button).removeClass('disabled').removeAttr('disabled');
        }
    }

    if (btnIntervalId) {
        clearInterval(btnIntervalId);
    }
}

function signinFormSubmit(button, varienFormObj, callback, redirect) {
    var $ = jQuery;
    var varienForm = varienFormObj;
    var $form = $(varienForm.form);
    var formAction = varienForm.form.action;

    if (varienForm.validator.validate()) {
        showButtonLoader(button);

        try {
            new Ajax.Request(formAction, {
                method: 'post',
                dataType: 'json',
                parameters: $form.serialize(),
                onComplete: function (response) {
                    var result = $.parseJSON(response.responseText);
                    hideButtonLoader(button);
                    callback($form, result.errMsg, redirect);
                }
            });
        } catch (e) {
            throw(e);
        }
    }
}


