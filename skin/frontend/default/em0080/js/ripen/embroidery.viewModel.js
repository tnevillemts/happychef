(function ($) {

    /**
     * Recursively search complex json object for a specific key
     * @param {Object} source
     * @param {String} k
     * @return {Object} Object associated with searched key
     * TO-DO: revise
     */
    function find(source, k) {

        var objects = [];
        for (var i in source) {

            if (!source.hasOwnProperty(i)) {
                continue;
            }

            if (typeof source[i] === 'object' && i != k) {
                objects = objects.concat(find(source[i], k));
            } else if (i === k && source[k].length > 0) {
                objects.push(source[k]);
            }
        }
        return objects;
    }

    /**
     * Search array of json objects for an element with property "id" matching given id
     * @param {Object} object
     * @param {String} id
     * @return {Object} Object associated with searched id
     */
    function filterById(object, id) {
        return object.filter(function (object) {
            return (object['id'] === id);
        })[0];
    }

    /**
     * Embroidery view model
     */
    window.EmbroideryViewModel = function (productId, quoteItemId, embroideryData) {

        var self = this;
        var allLocations = embroideryData.locations;
        var allArtworkCategories = embroideryData.artwork_categories;
        var allTypes = embroideryData.types;
        var allTextColors = embroideryData.colors;
        var allArtworkColors = embroideryData.colors;
        var allTextStyles = embroideryData.styles;
        var allLogos = embroideryData.logos;
        var embroideryQtyInCart = embroideryData.cart_embroidery_qty;
        var isSignedIn = embroideryData.is_signed_in;
        var store_id = embroideryData.store_id;

        self.productId = productId;
        self.quoteItemId = quoteItemId;

        if (self.quoteItemId !== "" || self.quoteItemId !== undefined) {
            self.alertId = "embroidery-alert-" + self.quoteItemId;
        } else if (self.productId !== "" || self.productId !== undefined) {
            self.alertId = "embroidery-alert-" + self.productId;
        } else {
            self.alertId = "embroidery-alert"
        }

        self.productOriginalData = $.extend(true, {}, embroideryData.productEmbroidery[productId]);
        self.productData = ko.observable(embroideryData.productEmbroidery[productId]);
        self.summaryData = ko.observableArray();

        self.locations = ko.observable(allLocations);
        self.artworkCategories = ko.observable(allArtworkCategories);
        self.types = ko.observable(allTypes);
        self.typesSortedByPopularity = ko.observable();
        self.textColors = ko.observable(allTextColors);
        self.artworkColors = ko.observable(allArtworkColors);
        self.textStyles = ko.observable(allTextStyles);
        self.isSignedIn = ko.observable(isSignedIn);

        self.ribbons = ko.observableArray();
        self.flags = ko.observableArray();
        self.licensedlogos = ko.observableArray();
        self.artworks = ko.observableArray();
        self.patches = ko.observableArray();
        self.logos = ko.observableArray(allLogos);
        self.logoInfoTierPrices = ko.observableArray();
        self.logoInfoName = ko.observable();
        self.logoInfoStitches = ko.observable();
        self.signInEmail = ko.observable();
        self.passwordEmail = ko.observable();
        self.signInPassword = ko.observable();
        self.isTempLogoSet = ko.observable(false);
        self.tempLogo = ko.observable();
        self.isSignInFailed = ko.observable(false);
        self.createAccountEmail = ko.observable();
        self.createAccountPassword = ko.observable();
        self.createAccountFirstname = ko.observable();
        self.createAccountLastname = ko.observable();
        self.isCreateAccountFailed = ko.observable(false);
        self.isSendPasswordFailed = ko.observable(false);
        self.isPasswordSent = ko.observable(false);
        self.isPasswordEmailValid = ko.observable(false);
        self.createAccountErrorMessage = ko.observable("Invalid Entry");

        self.textLines = ko.observableArray();
        self.artwork = ko.observableArray();
        self.alertData = ko.observable();
        self.embroideryPrice = ko.observable(0);
        self.isOpen = ko.observable();
        self.hasEmbroidery = ko.observable();
        self.isEditMode = ko.observable(false);
        self.embroideryGroupImage = ko.observable();
        self.tempInstructions = ko.observable();
        self.uploadButtonText = ko.observable("Upload File");
        self.disabledMessage = ko.observable();
        self.embroideryDiscountMessage = ko.observable();
        self.keyupIsTriggered = ko.observable(false);
        self.gaLabels = ko.observableArray();
        self.gaLabelsComplete = ko.observable(true);
        /**
         * App state
         */
        self.currentPanel = ko.observable();
        self.previousPanel = ko.observable();
        self.chosenLocation = ko.observable();
        self.chosenArtworkCategory = ko.observable();
        self.chosenType = ko.observable();
        self.chosenOption = ko.observableArray();
        self.chosenTextLine = ko.observable();
        self.chosenArtwork = ko.observable();
        self.chosenLogo = ko.observable();
        self.currPnlTitle = ko.observable();
        self.prevPnlTitle = ko.observable();
        self.isAlertBoxVisible = ko.observable();
        self.isInstructionsVisible = ko.observable(false);
        self.isDataSaved = ko.observable(true);
        self.isDataValid = ko.observable(false);
        self.isLogoValid = ko.observable(false);
        self.logoName = ko.observable();
        self.logoEditFieldVisible = ko.observable(false);
        self.isDataChanged = ko.observable();
        self.scrollPosition = ko.observable();
        self.originalText = ko.observable();
        self.closedAccordions = ko.observableArray(['si-accordion-tierpricingacf', 'si-accordion-tierpricingartwork', 'si-accordion-tierpricingpatches']);
        self.showTierPricing = ko.observable(false);
        self.relativePanelLevel = ko.observable();
        self.parentScrollableParentPanel = ko.observable('.si-content');
        self.actionPanelHideForMobileKeyboard = ko.observable('#si-actions-save-emb-container');
        self.productDataInstructions = ko.observable(self.productData().instructions);

        self.levels = {
            "types": 1,
            "locations": 2,
            "logo": 3,
            "createAccount": 4,
            "logos": 5,
            "logoInfo": 6,
            "artwork_categories": 3,
            "artwork": 4,
            "artworkColors": 5,
            "acfLogos": 3,
            "text": 3,
            "textColors": 4,
            "textStyles": 4,
            "ribbons": 3,
            "flags": 3,
            "summary": 6,
            "textInput": 7,
            "instructions": 7,
            "signin": 4,
            "password": 4
        };

        var sanitizeInput = function(length, regex) {
            return {
                init: function(element, valueAccessor, allBindingsAccessor, bindingContext) {
                    ko.bindingHandlers.textInput.init(element, valueAccessor, allBindingsAccessor, bindingContext);
                },
                update: function(element, valueAccessor) {
                    var value = ko.unwrap(valueAccessor());
                    if (regex.test(value)) {
                        value = value.replace(regex, '');
                    }
                    if (length != null && value.length > length) {
                        var trimmedValue = value.trim();
                        value = trimmedValue.substr(0, length);
                    }
                    valueAccessor()(value);
                    self.validateTextData();
                }
            }
        };

        // Must use character list rather than Unicode range to work with IE11.
        ko.bindingHandlers.sanitizeTextEmb = sanitizeInput(17, /[^ A-Z0-9_\d,?'"!@#$%^&*()=+;:<>|{}`~[\]\-\.\/\\]/gi);
        ko.bindingHandlers.sanitizeTextarea = sanitizeInput(600, /[^ A-Z0-9_\d,?'"!@#$%^&*()=+;:<>|{}`~[\]\-\.\/\\]/gi);

        /**
         * Edit Focus binding
         * Mainly used to scroll to the textarea getting covered by the mobile keyboard for mobile OS that do not scroll
         * The desired behavior is the hide the action buttons when the mobile keyboard pops up.
         * Once the user is done typing, the action buttons should be once again available.
         *
         * @type {{init: ko.bindingHandlers.editFocus.init}}
         */
        ko.bindingHandlers.editFocus = {
            init: function (element) {
                $(element).on('focus', function () {
                    var parentDiv = $(this).closest(self.parentScrollableParentPanel());
                    var scrollPosition = $(this).position().top + ($(this).height() / 2);
                    // In the case of the content not being taller than the screen, a delay needs to happen to wait for the mobile keyboard.
                    // Binding the scroll event outside of the resize for when the div has more content than screen height.
                    parentDiv.scrollTop(parentDiv.scrollTop() + scrollPosition);
                    $(window).on('resize', function(e){
                        if($(element).is(':focus')) {
                            $(self.actionPanelHideForMobileKeyboard()).hide();
                        }
                        setTimeout(function () {
                            parentDiv.scrollTop(parentDiv.scrollTop() + scrollPosition);
                            $(window).unbind(e);
                        }, 200);
                    });
                });
                $(element).on('blur', function () {
                    $(self.actionPanelHideForMobileKeyboard()).show();
                });
            }
        };

        ko.bindingHandlers.animate = {
            update: function (element, valueAccessor) {
                var newCurrLvl = self.levels[self.currentPanel()];
                var elId = $(element).attr('id');
                var prevElId = self.previousPanel();

                if (elId === prevElId) {
                    $(element).addClass("prevPnl");
                } else {
                    $(element).removeClass("prevPnl");
                }

                if (self.levels[elId] <= newCurrLvl) {
                    $(element).addClass('slideLeft');
                    $(element).removeClass('slideRight');
                } else {
                    $(element).addClass('slideRight');
                    $(element).removeClass('slideLeft');
                }

                if (valueAccessor()) {
                    $(element).addClass('vis');
                } else {
                    $(element).removeClass('vis');
                }
            }
        };

        ko.bindingHandlers.toggle = {
            init: function (element) {

                var triggerId = $(element).attr('id');
                var accId = triggerId.split('-', 3).join('-');
                var wrapperId = accId + '-wrap';

                if (self.closedAccordions().indexOf(accId) > -1) {
                    $('#' + wrapperId).removeClass('expanded');
                    $('#' + accId).hide();
                } else {
                    $('#' + wrapperId).addClass('expanded');
                    $('#' + accId).show();
                }
            },
            update: function (element) {

                var triggerId = $(element).attr('id');
                var accId = triggerId.split('-', 3).join('-');
                var wrapperId = accId + '-wrap';

                if (self.closedAccordions().indexOf(accId) > -1) {
                    $('#' + wrapperId).removeClass('expanded');
                    $('#' + accId).hide();
                } else {
                    $('#' + wrapperId).addClass('expanded');
                    $('#' + accId).show();
                }
            }
        };

        self.isVisibleImageRemoveButton = ko.observable(false);
        self.isVisibleImageUploadButton = ko.observable(true);

        self.scrollToInstructionsInput = function () {
            $('#summary .si-content').scrollTop($('#summary .si-content').scrollTop() + $('#instructions-scrollto').top);
        };

        self.switchPnls = function (newPnl) {

            self.previousPanel(self.currentPanel());
            self.prevPnlTitle(self.currPnlTitle());

            // unless specified below or hardcoded in the template,
            // the new panel title will be the name of the new panel
            switch (newPnl) {
                case "acfLogos":
                    self.currPnlTitle("ACF LOGOS");
                    break;
                case "artworkColors":
                case "textColors":
                    self.currPnlTitle("SELECT COLOR");
                    break;

                case "instructions":
                    self.currPnlTitle("SPECIAL INSTRUCTIONS");
                    break;

                case "logoInfo":
                    self.currPnlTitle("Process/Setup Details");
                    break;

                case "signin":
                    self.currPnlTitle("SIGN IN");
                    break;

                case "createAccount":
                    self.currPnlTitle("CREATE ACCOUNT");
                    break;

                case "textInput":
                    self.currPnlTitle("TEXT");
                    break;

                case "textStyles":
                    self.currPnlTitle("TEXT STYLES");
                    break;
            }

            self.currentPanel(newPnl);
        };

        self.isEmbroiderySetForProduct = function () {
            var data = self.productData().types;
            var result = find(data, 'selectedOption');
            return result.length;
        };

        self.isEmbroiderySetForType = function (type) {
            var data = self.productData().types[type.id];
            var result = find(data, 'selectedOption');
            return result.length;
        };

        self.isEmbroiderySetForLocation = function (location) {
            var toReturn = false;
            if (self.summaryData().length) {
                var locationData = filterById(self.summaryData(), location.id);
                if (typeof locationData !== "undefined") {
                    toReturn = true;
                }
            }
            return toReturn;
        };

        self.getTypesForLocation = function (location) {
            var types = [];
            if (self.summaryData().length) {
                var locationData = filterById(self.summaryData(), location.id);
                if (typeof locationData !== "undefined") {
                    $.each(locationData.types, function (index, type) {
                        types.push(filterById(allTypes, type.id));
                    })
                }
            }
            return types;
        };

        self.isTypeSetForLocation = function (location) {

            if (!self.chosenType())
                return false;

            var disabled = false;
            var crossTypes = [3, 5, 6, 7]; // artwork, acf logo, logo

            if (crossTypes.indexOf(parseInt(self.chosenType().id)) > -1) {
                $.each(crossTypes, function (index, el) {
                    if (typeof self.productData().types[el] !== 'undefined') {
                        var selectedOptions = find(self.productData().types[el]['locations'][location.id], 'selectedOption');
                        if (selectedOptions.length > 0) {
                            disabled = true;
                            self.disabledMessage("Only one logo/artwork is allowed per location.");
                            return false;
                        }
                    }
                })
            }

            if (disabled) {
                return true;
            }

            var toReturn = false;
            var data = self.productData().types[self.chosenType().id]['locations'][location.id];
            var result = find(data, 'selectedOption');

            if (self.chosenType().name === "text") {
                self.disabledMessage("You have added the maximum number of " + self.chosenType().name + " lines for this location.");
                toReturn = ( !result.length || result[0].length < data.limit) ? false : true;
            } else if (self.chosenType().name === "logo") {
                self.disabledMessage("You have added the maximum number of logos for this location.");
                toReturn = ( !result.length || result[0].length < data.limit) ? false : true;
            } else {
                self.disabledMessage("You have added the maximum number of " + self.chosenType().name + " for this location.");
                toReturn = result.length;
            }

            return toReturn;

        };

        self.isEmbroiderySetForOption = function (option) {
            if (!self.chosenLocation() || !self.chosenType())
                return false;

            var toReturn = false;
            var selectedOptions = self.getSelectedOption(self.chosenLocation(), self.chosenType());

            $.each(selectedOptions, function (index, el) {
                if (typeof el === 'object' &&
                    (
                        (typeof(el.id) === 'undefined' && el.sortorder === option.sortorder) ||
                        (el.id !== 0 && el.id === option.id ) ||
                        el.image === option.image
                    )
                ) {
                    toReturn = true;
                    return false;
                }
            });

            return toReturn;
        };

        self.limitTextInputMaxLength = function (inputId, maxLength) {
            var trimmedValue = $(inputId).val().substring(0, maxLength);
            $(inputId).attr('value', trimmedValue);
        };

        self.isSetArtworkColor = function (color) {

            if (!self.chosenLocation() || !self.chosenType())
                return false;

            var artworkOption = self.getSelectedOption(self.chosenLocation(), self.chosenType());
            return (typeof artworkOption[0] !== "undefined" && artworkOption[0].artworkColor === color.id);
        };

        self.makeTextLineObservable = function (textLineData) {
            if (ko.isObservable(textLineData.textLine)) return textLineData;

            textLineData.textLine = ko.observable(textLineData.textLine);
            return textLineData;
        };
        self.makeTextLinesObservable = function (textLines) {
            for (var i = 0; i < textLines.length; ++i) {
                textLines[i] = self.makeTextLineObservable(textLines[i]);
            }
            return textLines;
        };

        self.isTextOptionSetForTextLine = function (optionType, option) {

            if (!self.chosenLocation() || !self.chosenType() || !self.chosenTextLine())
                return false;

            var toReturn = false;
            var textOptions = self.getSelectedOption(self.chosenLocation(), self.chosenType());
            $.each(textOptions, function (index, el) {
                if (typeof el === 'object' && el.sortorder === self.chosenTextLine().sortorder && el[optionType] === option.id) {
                    toReturn = true;
                    return;
                }
            });
            return toReturn;
        };

        self.isAddTextLineVisible = function () {
            if (!self.chosenType() || !self.chosenLocation())
                return false;

            return self.textLines().length < self.productData().types[self.chosenType().id].locations[self.chosenLocation().id].limit;
        };

        self.textLineData = function (textAttribute, textLine) {

            var toReturn = textLine[textAttribute];

            if (textAttribute === "textColor") {
                var colorObject = filterById(self.textColors(), toReturn);
                toReturn = (typeof colorObject !== "undefined") ? colorObject.name : "Color";
            }
            if (textAttribute === "textStyle") {
                var styleObject = filterById(self.textStyles(), toReturn);
                toReturn = (typeof styleObject !== "undefined") ? styleObject.name : "Style";
            }

            return toReturn;
        };

        /**
         * Prepare applicable types. Load Types panel
         */
        self.loadTypes = function () {

            self.switchPnls('types');
            self.types(allTypes);

            $.each(allTypes, function (index, type) {
                if ($.isEmptyObject(self.productData().types[type.id])) {
                    self.types(
                        ($.grep(self.types(), function (el) {
                            return el.id !== type.id;
                        })));
                }
            });

            var arr = self.types();
            arr.sort(function (a, b) {
                return a.popularity - b.popularity;
            });
            self.typesSortedByPopularity(arr);

        };

        self.getTextColor = function (id) {
            return filterById(self.textColors(), id);
        };

        self.getTextStyle = function (id) {
            return filterById(self.textStyles(), id);
        };

        self.gotoType = function (type) {
            /*-- GOOGLE ANALYTICS --------------------------------*/
            _gaq.push(['_trackEvent', 'customization', 'navigation', type.name]);
            /*-- GOOGLE ANALYTICS --------------------------------*/

            self.chosenType(type);
            self.switchPnls('locations');
            self.embroideryGroupImage(self.productData().types[self.chosenType().id].templateImage);
            if (type.name === "acfLogos") {
                self.currPnlTitle("ACF LOGOS");
            } else {
                self.currPnlTitle(type.name);
            }
            self.locations([]);

            var locations = [];
            // Start with a list of all locations. Then narrow it down to types applicable to product, type
            $.each(allLocations, function (index, location) {
                var _productLocations = self.productData().types[type.id]["locations"][location.id];

                if (typeof _productLocations !== 'undefined') {

                    locations.push(location);

                    // remove text line from product data, if all properties are blank
                    // (if new lines were added, but not filled)

                    var textOptions = _productLocations["selectedOption"];
                    if (type.name === 'text' && textOptions.length) {

                        $.each(textOptions, function (index, el) {
                            if (typeof el !== "undefined" && !el.textLine && !el.textColor && !el.textStyle) {
                                textOptions.splice(index, 1);
                            }
                        })
                        // shift elements up by updating id properties
                        $.each(textOptions, function (index, el) {
                            el.sortorder = index + 1;
                        });
                        self.setSelectedOption(location, type, textOptions);
                        self.isDataSaved(true);
                    }
                }
                self.locations(locations);
            });
        };

        self.gotoCategory = function (category) {

            var artworksByCategory = self.chosenType().options;
            var filteredArtworks = [];

            // for some weird reason GREP didn't work for filtering, so use EACH
            $.each(artworksByCategory, function (i, el) {
                if (el.artwork_category_id === category.id) {
                    filteredArtworks.push(el);
                }
            });

            self.chosenArtworkCategory(category);
            self.artworks(filteredArtworks);
            self.switchPnls('artwork');
            self.currPnlTitle(category.name);
        };

        /**
         * Prepare applicable embroidery options. Load panel with options for specific location, type
         */
        self.gotoLocation = function (location) {

            self.textLines([]);
            self.chosenLocation(location);

            var currentPanel = self.chosenType().name;
            if (currentPanel === 'logo' && self.logos().length) {
                currentPanel = "logos";
            }
            if (currentPanel === 'artwork' && self.artworkCategories().length) {
                currentPanel = "artwork_categories";
            }

            self.switchPnls(currentPanel);
            if (self.chosenType().name === "acfLogos") {
                self.currPnlTitle(location.name + " - " + "ACF LOGOS");
            } else {
                self.currPnlTitle(location.name + " - " + self.chosenType().name);
            }

            switch (self.chosenType().name) {

                case "ribbons":
                    self.ribbons(self.chosenType().options);
                    break;
                case "flags":
                    self.flags(self.chosenType().options);
                    break;
                case "acfLogos":
                    self.licensedlogos(self.chosenType().options);
                    break;
                case "artwork":
                    self.artworks(self.chosenType().options);
                    break;
                case "patches":
                    self.patches(self.chosenType().options);
                    break;
                case "text":
                    var textOptions = self.getSelectedOption(self.chosenLocation(), self.chosenType());

                    if (textOptions.length) { // if there is at least one text line already set
                        self.textLines(self.makeTextLinesObservable(textOptions));
                    } else { // if no text line data was set yet, show only the first line

                        // initial value of the text line
                        var textLinePreset = "";

                        // check the chosenTextLine just in case we have text input that was not saved
                        var chosenTextLine = self.chosenTextLine();
                        if (typeof chosenTextLine !== 'undefined') {
                            textLinePreset = chosenTextLine.textLine(); // use what was in the chosenTextLine instead
                        }

                        self.textLines.push({
                            "textLine": ko.observable(textLinePreset),
                            "textColor": "",
                            "textStyle": "",
                            "sortorder": 1,
                            "price": self.chosenType().price
                        });
                    }

                    break;

                case "logo":

                    if (self.getLogo().length === 0) {
                        self.isVisibleImageRemoveButton(false);
                        self.isVisibleImageUploadButton(true);
                    } else {
                        self.isVisibleImageRemoveButton(true);
                        self.isVisibleImageUploadButton(false);
                        self.logoName(self.getLogo()[0].name);
                    }

                    break;

                default:
                    /**
                     * TO-DO
                     */
                    break;
            }
        };

        self.editArtworkColor = function (location, type, artwork) {
            self.getScrollPosition();
            self.isEditMode(true);
            self.chosenLocation(null);
            self.chosenType(null);

            var typeObject = filterById(self.types(), type.id);
            self.chosenType(typeObject);

            var locationObject = filterById(allLocations, location.id);
            self.chosenLocation(locationObject);

            self.chosenArtwork(artwork);
            self.gotoArtworkColor(artwork);
        };

        self.getTierData = function (tier, dataType) {

            if(self.chosenType() && self.chosenType().tier_prices && self.chosenType().tier_prices[tier])
                return self.chosenType().tier_prices[tier][dataType];
            else
                return false;
        };

        self.editOption = function (location, type, option) {

            self.getScrollPosition();
            self.isEditMode(true);

            var typeObject = filterById(allTypes, type.id);
            self.chosenType(typeObject);

            var locationObject = filterById(allLocations, location.id);
            self.chosenLocation(locationObject);

            if (type.name === 'artwork') {
                var artworks = self.chosenType().options,
                    artwork = filterById(artworks, option.id),
                    artworkCategories = self.artworkCategories(),
                    artworkCategory = filterById(artworkCategories, artwork.artwork_category_id);
                self.chosenArtworkCategory(artworkCategory);
                self.gotoCategory(artworkCategory);
                return false;
            }

            self.gotoLocation(location);
        };

        self.editTextLine = function (location, type, option) {
            self.getScrollPosition();
            self.isEditMode(true);
            var chosenTextLine = {};

            var textOptions = self.getSelectedOption(location, type);

            $.each(textOptions, function (index, textLine) {
                if (textLine.sortorder === option.textLineNumber) {
                    chosenTextLine = textLine;
                    return false;
                }
            })

            self.gotoTextInput(chosenTextLine);

        };

        self.editTextOption = function (location, type, option, optionType) {
            self.getScrollPosition();
            self.isEditMode(true);
            self.isDataSaved(false);
            self.chosenLocation(null);
            self.chosenType(null);

            var typeObject = filterById(self.types(), type.id);
            self.chosenType(typeObject);

            var locationObject = filterById(allLocations, location.id);
            self.chosenLocation(locationObject);

            var textOptions = self.getSelectedOption(self.chosenLocation(), self.chosenType());

            self.textLines(self.makeTextLinesObservable(textOptions));
            var chosenTextLine = {};
            $.each(self.textLines(), function (index, textLine) {
                if (textLine.sortorder === option.textLineNumber) {
                    chosenTextLine = textLine;
                    return false;
                }
            });

            if (optionType === 'color')
                self.gotoColor(chosenTextLine);
            else if (optionType === 'style')
                self.gotoStyle(chosenTextLine);

        };

        self.gotoTextInput = function (textLine) {
            self.originalText(ko.unwrap(textLine.textLine));
            self.chosenTextLine(self.makeTextLineObservable(textLine));
            self.switchPnls('textInput');
        };

        self.gotoInstructions = function () {
            self.getScrollPosition();
            self.tempInstructions(self.productData().instructions);
            self.switchPnls('instructions');
        };

        self.gotoSignIn = function () {
            self.getScrollPosition();
            self.switchPnls('signin');
        };

        $('#forgot-password-emb').on('click', function(){
            /*-- GOOGLE ANALYTICS --------------------------------*/
            _gaq.push(['_trackEvent', 'Forgot Password Open', 'Embroidery']);
            /*-- GOOGLE ANALYTICS --------------------------------*/
        })

        self.gotoCreateAccount = function () {
            self.getScrollPosition();
            self.switchPnls('createAccount');
        };

        self.sendPassword = function () {

            self.isSendPasswordFailed(false);
            var frontendValidationResult = Validation.validate($('#forgot_password')[0]);

            if (frontendValidationResult) {
                new Ajax.Request('/embroidery/index/resetPassword', {
                    method: 'post',
                    parameters: {"email": self.passwordEmail()},
                    onComplete: function (response) {
                        var result = $.parseJSON(response.responseText);
                        if (result.status) { // successful login
                            self.signInEmail(self.passwordEmail());
                            self.isPasswordSent(true);

                        } else { // fail to login
                            self.isSendPasswordFailed(true);
                        }
                    }
                });
            }
        }

        self.createAccount = function () {
            /*-- GOOGLE ANALYTICS --------------------------------*/
            _gaq.push(['_trackEvent', 'Create Account', 'Embroidery']);
            /*-- GOOGLE ANALYTICS --------------------------------*/

            self.isCreateAccountFailed(false);
            var frontendValidationResult = Validation.validate($('#createAccountEmail')[0]);

            if (frontendValidationResult) {
                new Ajax.Request('/embroidery/index/createAccount', {
                    method: 'post',
                    parameters: {
                        "email": self.createAccountEmail(),
                        "password": self.createAccountPassword(),
                        "firstName": self.createAccountFirstname(),
                        "lastName": self.createAccountLastname()
                    },
                    onComplete: function (response) {
                        var result = $.parseJSON(response.responseText);
                        if (result.status) { // success

                            if (self.tempLogo() == null) {
                                self.saveLogoData();
                            } else {
                                self.setLogo(self.tempLogo());
                                self.tempLogo(null);
                            }
                            self.logos(self.logos().concat(result.logos));

                            self.isSignedIn(true);
                            self.loadSummary();

                            self.updateMyAccountHtml(result.myAccountHtml);
                            self.updateBtnMobileDisplayDrawerHtml(result.btnMobileDisplayDrawerHtml);

                        } else { // fail
                            self.createAccountErrorMessage(result.message);
                            self.isCreateAccountFailed(true);
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                            _gaq.push(['_trackEvent', 'Create Account Error', 'Embroidery', result.message]);
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                        }

                    }
                });
            }

        };

        self.signIn = function () {

            self.isSignInFailed(false);
            var frontendValidationResult = Validation.validate($('#signInEmail')[0]);

            if (frontendValidationResult) {
                var btnSignin = $('#btn-signin');
                if (btnSignin.length) {
                    showButtonLoader(btnSignin);
                }

                new Ajax.Request('/embroidery/index/signIn', {
                    method: 'post',
                    parameters: {"email": self.signInEmail(), "password": self.signInPassword()},
                    onComplete: function (response) {
                        hideButtonLoader(btnSignin);
                        var result = $.parseJSON(response.responseText);

                        if (result.status) { // successful login

                            self.isSignedIn(true);
                            if (self.isTempLogoSet()) {
                                self.saveLogoData();
                                self.logos(self.logos().concat(result.logos));
                                self.loadSummary();
                            } else {
                                if (result.logos) {
                                    self.logos(self.logos().concat(result.logos));
                                }
                                self.gotoLocation(self.chosenLocation());
                            }

                            self.updateMyAccountHtml(result.myAccountHtml);
                            self.updateBtnMobileDisplayDrawerHtml(result.btnMobileDisplayDrawerHtml);

                        } else { // fail to login

                            self.isSignInFailed(true);
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                            _gaq.push(['_trackEvent', 'Login Error', 'Embroidery', result.message]);
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                        }

                    }
                });
            }

        };

        self.updateBtnMobileDisplayDrawerHtml = function(btnMobileDisplayDrawerHtml) {
            $('#btnMobileDisplayDrawer').html(btnMobileDisplayDrawerHtml);
            $('#cartSideBarSignIn').remove();
        };

        self.updateMyAccountHtml = function (myAccountHtml) {
            $("#account").hide();
            $("#account").replaceWith(myAccountHtml);
            window.hoverTopAccount();
        };

        self.onEnter = function (d, e) {
            if (e.keyCode === 13) {
                self.saveLogo();
                return false;
            } else {
                return true;
            }
        };

		self.onEnterSignIn = function (d, e) {
			if (e.keyCode === 13) {
				self.signIn();
				return false;
			} else {
				return true;
			}
		};

        self.gotoLogo = function () {
            self.switchPnls('logo');
        };

        self.gotoLogoInfo = function () {
            self.switchPnls('logoInfo');
        };

        self.gotoArtworkColor = function (artwork) {
            self.getScrollPosition();
            self.chosenArtwork(artwork);
            self.switchPnls('artworkColors');
            self.artworkColors(allArtworkColors);
        };

        self.gotoColor = function (textLine) {
            self.getScrollPosition();
            self.chosenTextLine(self.makeTextLineObservable(textLine));
            self.switchPnls('textColors');
            self.textColors(allTextColors);
        };

        self.gotoStyle = function (textLine) {
            self.getScrollPosition();
            self.chosenTextLine(self.makeTextLineObservable(textLine));
            self.switchPnls('textStyles');
            self.textStyles(allTextStyles);
        };

        self.goBack = function () {
            if ($.inArray(self.currentPanel(), ["ribbons", "flags", "acfLogos", "artwork_categories", "text", "art", "custom", "logo", "logos", "patches"]) > -1) { // go to types panel
                if (!self.isDataSaved() && $.inArray(self.currentPanel(), ["text"]) > -1) {
                    self.cancelTextEmbroidery();
                } else if (!self.isDataSaved() && $.inArray(self.currentPanel(), ["logo"]) > -1) {
                    self.cancelLogoEmbroidery();
                } else {
                    if (!self.isEditMode())
                        self.gotoType(self.chosenType());
                    else
                        self.loadSummary();
                }

            } else if ($.inArray(self.currentPanel(), ["artworkColors"]) > -1) {

                if (!self.isEditMode())
                    self.gotoCategory(self.chosenArtworkCategory());
                else
                    self.loadSummary();

            } else if ($.inArray(self.currentPanel(), ["textColors", "textStyles", "textInput"]) > -1) {

                if ((!self.isDataValid()) && $.inArray(self.currentPanel(), ["textInput"]) > -1) {
                    self.validateTextInputEmbroidery();
                } else if ((!self.isDataSaved()) && $.inArray(self.currentPanel(), ["textInput"]) > -1) {
                    self.cancelTextInputEmbroidery();
                } else {
                    if (!self.isEditMode())
                        self.gotoLocation(self.chosenLocation());
                    else
                        self.loadSummary();
                }

            } else if ($.inArray(self.currentPanel(), ["instructions"]) > -1) { // from types go back to summary panel

                if (!self.isDataSaved()) {
                    self.cancelInstructions();
                } else {
                    self.loadSummary();
                }

            } else if ($.inArray(self.currentPanel(), ["types"]) > -1) { // from types go back to summary panel

                self.loadSummary();
                if (self.isEditMode) {
                    self.scrollToLastEdited();
                }

            } else if ($.inArray(self.currentPanel(), ["logoInfo", "signin"]) > -1) {

                self.gotoLogo();

            } else if ($.inArray(self.currentPanel(), ["password"]) > -1) {

                self.gotoSignIn();

            } else if ($.inArray(self.currentPanel(), ["createAccount"]) > -1) {

                if (!self.isSignedIn()) {
                    self.logos([]);
                }
                self.gotoLogo();

            } else if ($.inArray(self.currentPanel(), ["artwork"]) > -1) {

                if (!self.isDataSaved()) {
                    self.setSelectedOption(self.chosenLocation(), self.chosenType(), []);
                    self.isDataSaved(true);
                    self.gotoLocation(self.chosenLocation());
                } else {
                    self.gotoLocation(self.chosenLocation());
                }

            } else if ($.inArray(self.currentPanel(), ["locations", "logo"]) > -1) { // from locations and logo go back to types panel

                self.loadTypes();
            }
        };

        /**
         * Go Back Icon
         * Trigger/hook for back icon in the embroidery panel.
         * Good for things like tracking and anything else that should be kept out of the model.
         */
        self.goBackIcon = function () {
            $(document).trigger('embroidery-go-back-icon');
            self.goBack();
        };

        self.doRemoveEmbroidery = function () {
            var data = self.productData();
            $.each(data.types, function (index, el) {
                $.each(el.locations, function (idx, e) {
                    e.selectedOption = [];
                });
            });

            data.instructions = "";

            self.productOriginalData = $.extend(true, {}, data);
            self.productData(data);
            self.summaryData(self.getSummaryData());
            self.loadTypes();

            // reset textLine to blank
            self.resetChosenText(true);

            // change for PROD-2317
            self.isDataChanged(true);

            self.doSaveAndExit();
            self.embroideryDiscountMessage('');
            self.hideAddtoCartWithEmbroideryBtnContext();

            if ($("#product-embroidery-options").length) { // for product page - update hidden field
                var optionsDomEl = $("#product-embroidery-options");
                optionsDomEl.val("");
            }
        };

        self.removeEmbroidery = function () {
            var data = {
                "title": "",
                "alertMessage": "Are you sure you want to remove your embroidery?",
                "btn1Label": "Yes",
                "btn1Action": "doRemoveEmbroidery",
                "btn2Label": "No",
                "btn2Action": ""
            }
            self.openAlertBox(data);
        };

        self.doCloseDoNotSave = function () {

            var el, data;
            if (self.quoteItemId > 0) {
                el = $("#product-embroidery-options-" + self.quoteItemId);
            } else {
                el = $("#product-embroidery-options");
            }

            if (el.length && el.val()) {
                var savedData = JSON.parse(el.val());
                data = savedData[self.productId];
            } else {
                // reset textLine to blank
                self.resetChosenText(true);
                data = self.deepClone(self.productOriginalData);
            }

            self.productData(data);
            self.summaryData(self.getSummaryData());
            self.isTempLogoSet(false);
            self.isEditMode(false);
            self.loadTypes();
            self.updateEmbroideryPrice();
            self.closeEmbroidery();
        };

        self.doCancelTextEmbroidery = function () {
            var originalTextData = self.deepClone(self.productOriginalData).types[self.chosenType().id].locations[self.chosenLocation().id].selectedOption;
            self.textLines(self.makeTextLinesObservable(originalTextData));
            self.setSelectedOption(self.chosenLocation(), self.chosenType(), originalTextData);
            // reset textLine to what it was before, or blank it out entirely
            self.resetChosenText();
            self.isDataSaved(true);
            self.isDataValid(false);
            self.goBack();
        };

        self.doCancelLogoEmbroidery = function () {

            self.doRemoveFile({"location": self.chosenLocation(), "type": self.chosenType()});
            var originalLogoData = self.deepClone(self.productOriginalData).types[self.chosenType().id].locations[self.chosenLocation().id].selectedOption;
            self.setSelectedOption(self.chosenLocation(), self.chosenType(), originalLogoData);
            self.summaryData(self.getSummaryData());
            self.isDataSaved(true);
            self.isDataValid(false);
            self.goBack();
        };

        self.cancelLogoEmbroidery = function () {
            if (self.isDataSaved()) {
                self.doCancelLogoEmbroidery();
            } else {
                var data = {
                    "title": "",
                    "alertMessage": "Are you sure you want to go back?<br />All unsaved updates will be lost.",
                    "btn1Label": "Yes, Go Back",
                    "btn1Action": "doCancelLogoEmbroidery",
                    "btn2Label": "No, Continue Editing",
                    "btn2Action": ""
                }
                self.openAlertBox(data);
            }
        };

        self.cancelTextEmbroidery = function () {
            if (self.isDataSaved()) {
                self.doCancelTextEmbroidery();
            } else {
                var data = {
                    "title": "",
                    "alertMessage": "Are you sure you want to go back?<br />All unsaved updates will be lost.",
                    "btn1Label": "Yes, Go Back",
                    "btn1Action": "doCancelTextEmbroidery",
                    "btn2Label": "No, Continue Editing",
                    "btn2Action": ""
                }
                self.openAlertBox(data);
            }
        };

        self.validateTextInputEmbroidery = function () {

            var data = {
                "title": "",
                "alertMessage": "Please enter text.",
                "btn1Label": "",
                "btn1Action": "",
                "btn2Label": "Continue Editing",
                "btn2Action": ""
            }
            self.openAlertBox(data);
        };

        self.doCancelTextInputEmbroidery = function () {
            self.resetChosenText();
            self.isDataSaved(true);
            self.isDataValid(true);
            self.loadSummary();
        };

        self.cancelTextInputEmbroidery = function () {

            var data = {
                "title": "",
                "alertMessage": "Are you sure you want to go back?<br />All unsaved updates will be lost.",
                "btn1Label": "Yes, Go Back",
                "btn1Action": "doCancelTextInputEmbroidery",
                "btn2Label": "No, Continue Editing",
                "btn2Action": ""
            }
            self.openAlertBox(data);
        };

        self.doCancelInstructions = function () {
            var originalInstructions = self.tempInstructions();
            var updatedData = self.productData();
            updatedData.instructions = originalInstructions;
            self.productData(updatedData);
            self.isDataSaved(true);
            self.goBack();
        };

        self.cancelInstructions = function () {
            if (self.isDataSaved()) {
                self.doCancelInstructions();
            } else {
                var data = {
                    "title": "",
                    "alertMessage": "Are you sure you want to go back?<br />All unsaved updates will be lost.",
                    "btn1Label": "Yes, Go Back",
                    "btn1Action": "doCancelInstructions",
                    "btn2Label": "No, Continue Editing",
                    "btn2Action": ""
                }
                self.openAlertBox(data);
            }
        };

        self.resetChosenText = function (resetToBlank) {
            var originalText = '';
            if (typeof self.originalText() !== 'undefined' && resetToBlank !== true) {
                originalText = self.originalText();
            }
            if(typeof self.chosenTextLine() !== 'undefined') {
                self.chosenTextLine().textLine(originalText);
            }
        };

        self.recordChanges = function () {

            var objectToSave = {};

            objectToSave[self.productId] = ko.toJS(self.productData());

            if ($("#product-embroidery-options").length) { // for product page - update hidden field
                var optionsDomEl = $("#product-embroidery-options");
                optionsDomEl.val(JSON.stringify(objectToSave));
                self.updateEmbroideryPrice();

            } else if ($('.bind-embroidery-cart').length) { // for cart page - update db

                new Ajax.Request('/bettercheckout/cart/updateCartAjax', {
                    method: 'post',
                    parameters: {"itemId": self.quoteItemId, "embroideryData": JSON.stringify(objectToSave)},
                    onComplete: function (response) {

                        var result = $.parseJSON(response.responseText);

                        if (result.requireReload) {
                            location.reload();
                        }

                        $('#cart-item-total-price-' + self.quoteItemId + ' .price').html(result.newItemTotal);
                        $('#cart-item-price-' + self.quoteItemId + ' .price').html(result.newItemUnitPrice);
                        $('#cart-stock-status-' + self.quoteItemId).html(result.stockStatus);
                        $('#cart-embroidery-message-' + self.quoteItemId).html(result.embroideryDisclaimer);

                        // if change affected other items in the cart, update their prices as well
                        $(".cart-item-total-price").each(function () {
                            var id = $(this).attr("id").split(/[-]+/).pop();
                            if (typeof result.updatedLineItemPrices[id] !== "undefined") {
                                $('#cart-item-total-price-' + id + ' .price').html(result.updatedLineItemPrices[id].newItemTotal);
                            }
                        });

                        $(".cart-item-price").each(function (i, el) {

                            var id = $(this).attr("id").split(/[-]+/).pop();
                            if (typeof result.updatedLineItemPrices[id] !== "undefined") {
                                $('#cart-item-price-' + id + ' .price').html(result.updatedLineItemPrices[id].newItemUnitPrice);
                            }
                        });

                        var optionsDomEl = $("#product-embroidery-options-" + self.quoteItemId);
                        optionsDomEl.val(JSON.stringify(objectToSave));

                        // Renders totals box. Functions from bettercheckout.js
                        renderTotals(result);
                        loadRegions(false, $('#country').val());

                    }
                });

            }


        };

        self.doSaveAndExit = function () {

            if (self.isDataChanged()) {
                self.recordChanges();
                self.gaLabelsComplete(false);
                if (typeof self.quoteItemId !== "undefined" && self.quoteItemId.length) {
                    self.showPulldown("Embroidery changes applied.");
                } else {
                    self.showAddToCartWithEmbroideryBtnContext();
                }

                /*-- GOOGLE ANALYTICS --------------------------------*/
                var embTypes = self.productData().types;

                $.each(embTypes, function (typeId, type) {
                    var res = find(type, 'selectedOption');
                    for (var i = 1; i <= res.length; i++) {
                        _gaq.push(['_trackEvent', 'customization', 'added-to-product', type.name]);

                        if (type.name === "artwork") {
                            $.each(res, function (idx, el) {
                                _gaq.push(['_trackEvent', 'customization', 'added-to-product', el[0]["name"]]);
                            })
                        }

                        if (window.location.href.indexOf("checkout/cart") > -1) {
                            //on cart page, emb is added to cart at same time as it is added to product
                            _gaq.push(['_trackEvent', 'customization', 'added-to-cart', type.name]);

                            if (type.name === "artwork") {
                                $.each(res, function (idx, el) {
                                    _gaq.push(['_trackEvent', 'customization', 'added-to-cart', el[0]["name"]]);
                                });
                            }

                        } else {
                            //push to an array that is observed on the pdp
                            if (type.name === 'artwork') {
                                $.each(res, function (idx, el) {
                                    self.gaLabels(el[0]["name"]);
                                });
                            }

                            self.gaLabels(type.name);
                        }

                    }

                });

                self.gaLabelsComplete(true);

                /*-- GOOGLE ANALYTICS --------------------------------*/
            }

            self.closeEmbroidery();

        };

        self.showAddToCartWithEmbroideryBtnContext = function () {
            $('button.btn-cart.btn-blue.btn-tall').addClass('add-to-cart-with-embroidery');
            if ($('.custom_status.preorder').length) {
                $('#add-cart-text').text('Pre-order with Embroidery');
            } else {
                $('#add-cart-text').text('Add to Cart w/ Embroidery');
            }
            $('#amstockstatus-status').attr('style', 'color: #000;');
            self.scrollToCentered('#add-cart-text');
        };

        self.hideAddtoCartWithEmbroideryBtnContext = function () {
            $('button.btn-cart.btn-blue.btn-tall').removeClass('add-to-cart-with-embroidery');
            if ($('.custom_status.preorder').length) {
                $('#add-cart-text').text('Pre-order');
            } else {
                $('#add-cart-text').text('Add to Cart');
            }

            var stockStatusEl = $('#amstockstatus-status');
            (stockStatusEl.text().trim().toLowerCase() === 'in stock') ? stockStatusEl.attr('style', 'color: green;') : stockStatusEl.removeAttr('style');
        };

        self.scrollToCentered = function (element) {
            var el = $(element);
            var elOffset = el.offset().top;
            var elHeight = el.height();
            var windowHeight = $(window).height();
            var offset;

            if (elHeight < windowHeight) {
                offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
            } else {
                offset = elOffset;
            }
            $('html, body').animate({
                scrollTop: offset
            }, 1500);
            return false;
        };

        self.showPulldown = function (message) {

            if ($('.system-message').length) {
                $('.system-message').remove();
            }
            var el = $("<div class='system-message'><div>" + message + "</div></div>");
            $('body').prepend(el);
            el.slideDown('slow').delay(8000).slideUp('slow');

        };

        self.showInstructions = function () {
            self.isInstructionsVisible(true);
        };

        self.hideInstructions = function () {
            self.isInstructionsVisible(false);
        };

        self.addTextLine = function () {

            if (self.isAddTextLineVisible()) {
                self.textLines.push({
                    "textLine": ko.observable(""),
                    "textColor": "",
                    "textStyle": "",
                    "sortorder": (self.textLines().length + 1),
                    "price": self.chosenType().price
                });
                self.textLines.sort(function (a, b) {
                    return a.sortorder - b.sortorder;
                });
                self.isDataValid(false);
                self.isDataSaved(false);
            }
        };

        self.removeInstructions = function () {
            var data = {
                "title": "",
                "alertMessage": "Are you sure you want to remove your note?",
                "btn1Label": "Yes",
                "btn1Action": "doRemoveInstructions",
                "btn2Label": "No",
                "btn2Action": "",
                "arguments": {}
            }
            self.openAlertBox(data);
        };

        self.doRemoveInstructions = function () {
            var updatedData = self.productData();
            updatedData.instructions = "";
            self.productData(updatedData);
            self.isDataChanged(true);
            self.loadSummary();
        };

        self.removeType = function (location, type) {

            var itemName;
            if (type.name === "acfLogos") {
                itemName = "ACF logo";
            } else {
                itemName = (type.name === "flags" || type.name === "ribbons") ? type.name.slice(0, -1) : type.name;
            }

            var data = {
                "title": "",
                "alertMessage": "Are you sure you want to remove this " + itemName + "?",
                "btn1Label": "Yes, Remove",
                "btn1Action": "doRemoveType",
                "btn2Label": "No, Continue Editing",
                "btn2Action": "",
                "arguments": {"location": location, "type": type}
            }
            self.openAlertBox(data);
        };

        self.doRemoveType = function (parameters) {

            var location = parameters.location;
            var type = parameters.type;

            // remove image files if applicable
            //if (type.name == 'logo'){
            //    self.doRemoveFile({ "location":location, "type":type });
            //}

            self.productData().types[type.id].locations[location.id].selectedOption = [];
            self.productData(self.productData());
            self.isDataChanged(true);

            // self.recordChanges();

            if (self.isEmbroiderySetForProduct()) {
                self.loadSummary();
            } else {
                var embroideryReverse = self.getSummaryData();
                self.summaryData(embroideryReverse);
                self.loadTypes();
            }

        };

        self.removeTextLine = function (location, type, lineNumber) {

            var data = {
                "title": "",
                "alertMessage": "Are you sure you want to remove this text?",
                "btn1Label": "Yes, Remove",
                "btn1Action": "doRemoveTextLine",
                "btn2Label": "No, Continue Editing",
                "btn2Action": "",
                "arguments": {"location": location, "type": type, "lineNumber": lineNumber}
            }

            self.openAlertBox(data);

        };

        self.doRemoveTextLine = function (parameters) {

            var location = parameters.location;
            var type = parameters.type;
            var lineNumber = parameters.lineNumber;

            // reset chosen textLine to blank
            self.resetChosenText(true);

            var textLines = self.productData().types[type.id].locations[location.id].selectedOption;
            $.each(textLines, function (index, textLine) {
                if (textLine.sortorder === lineNumber) {
                    textLines.splice(index, 1);
                    return false;
                }
            });

            $.each(textLines, function (index, el) {
                el.sortorder = (index + 1);
            })
            self.textLines(self.makeTextLinesObservable(textLines));
            self.isDataChanged(true);

            if (self.currentPanel() === 'summary') {
                if (self.isEmbroiderySetForProduct()) {
                    self.loadSummary();
                } else {
                    self.productData(self.productData());
                    var embroideryReverse = self.getSummaryData();
                    self.summaryData(embroideryReverse);
                    self.loadTypes();
                }
            } else {
                self.gotoLocation(self.chosenLocation());
                self.validateTextData();
            }

        };

        self.validateTextData = function () {

            var isValid = true;

            $.each(self.textLines(), function (index, textLine) {
                if (!textLine.textColor || !textLine.textStyle || !textLine.textLine()) {
                    isValid = false;
                    return false;
                }
            });
            self.isDataValid(isValid);
        };

        self.validateTextLine = function (textLine) {

            var isValid = true;

            if (!textLine.textLine) {
                isValid = false;
            }

            self.isDataSaved(false);
            self.isDataValid(isValid);

            if (isValid) {
                self.isDataChanged(true);
            }

        };

        self.applyTextEmbroidery = function () {

            self.isDataSaved(true);
            self.isDataChanged(true);
            self.loadSummary();

        };

        self.setTypeOption = function (option) {

            var price = self.chosenType().price;


            var newOption = [
                {
                    "id": option.id,
                    "name": option.name,
                    "price": price,
                    "image": option.image
                }
            ];

            self.setSelectedOption(self.chosenLocation(), self.chosenType(), newOption);
            self.isDataSaved(true);
            self.isDataChanged(true);
            self.loadSummary();
            if (self.isEditMode) {
                self.scrollToLastEdited();
            }

        };

        self.showLogoSetupAlert = function (logo) {

            // TODO: What is the point of the if() since this is always true?
            var disableSetupFeeAlert = true;
            if (logo.status === 1 || disableSetupFeeAlert) {

                if (!self.isSignedIn()) {
                    self.tempLogo(logo);
                    self.gotoCreateAccount();
                } else {
                    self.setLogo(logo);
                }

                return false;
            }

            var data = {
                "title": "",
                "alertMessage": "<strong>One-time logo setup fee required.</strong> <br/><br/>Using this logo will require a one-time logo setup fee (for most logos, the fee is $95). An embroidery expert will contact you to confirm your one time setup and product embroidery fee.",
                "btn1Label": "Continue with logo",
                "btn1Action": "setLogo",
                "btn2Label": "Select another logo",
                "btn2Action": "",
                "arguments": {"logo": logo}
            };

            self.openAlertBox(data);

        };

        self.showLogoTierInfo = function (logo) {

            var tierPrices = logo.tier_prices;
            tierPrices.sort(function (a, b) {
                return a.qty_from - b.qty_from;
            });

            self.logoInfoName(logo.name);
            self.logoInfoStitches(logo.stitches);
            self.logoInfoTierPrices(tierPrices);

            var tierPricesInfo = $('#logoTierPriceInfo').html();

            var data = {
                "title": "",
                "alertMessage": tierPricesInfo,
                "btn1Label": "Sounds Good",
                "btn1Action": "",
                "btn2Label": "",
                "btn2Action": ""
            };

            self.openAlertBox(data);
        };

        self.setLogo = function (logo) {

            // Looks weird right? Bt it's legit
            // check if object has a key "logo" and if so, replace the original logo object with the value of this key
            if ('logo' in logo) {
                logo = logo["logo"];
            }

            var selectedOption = self.getSelectedOption(self.chosenLocation(), self.chosenType());
            var originalLogo = self.deepClone(selectedOption[0]);
            var price = self.chosenType().price;

            originalLogo['id'] = logo['id'];
            originalLogo['name'] = logo['name'];
            originalLogo['price'] = price;
            originalLogo['image'] = logo['image'];
            originalLogo['stitches'] = logo['stitches'];

            self.setSelectedOption(self.chosenLocation(), self.chosenType(), [originalLogo]);

            if (self.isEditMode()) {
                self.isDataChanged(true);
                self.isDataSaved(true);
                self.loadSummary();
                self.scrollToLastEdited();
            } else {
                self.chosenLogo(originalLogo);
                self.isDataChanged(true);
                self.isDataSaved(false);
                self.loadSummary();
            }
        };

        self.setArtwork = function (artwork) {

            var selectedOption = self.getSelectedOption(self.chosenLocation(), self.chosenType());
            var originalArtwork = self.deepClone(selectedOption[0]);
            var price = self.chosenType().price;

            originalArtwork['id'] = artwork['id'];
            originalArtwork['name'] = artwork['name'];
            originalArtwork['price'] = price;
            originalArtwork['image'] = artwork['image'];

            self.setSelectedOption(self.chosenLocation(), self.chosenType(), [originalArtwork]);

            if (self.isEditMode()) {
                self.isDataChanged(true);
                self.isDataSaved(true);
                self.loadSummary();
                self.scrollToLastEdited();
            } else {
                self.chosenArtwork(originalArtwork);
                //self.isDataChanged(true);
                self.isDataSaved(false);
                self.gotoArtworkColor(originalArtwork);
            }
        };

        self.setArtworkColor = function (color) {

            var artwork = self.chosenArtwork();
            var originalArtwork = self.deepClone(artwork);
            originalArtwork['artworkColor'] = color['id'];
            originalArtwork['artworkColorValue'] = color['name'];
            originalArtwork['artworkColorImage'] = color['image'];

            self.setSelectedOption(self.chosenLocation(), self.chosenType(), [originalArtwork]);

            self.isDataChanged(true);
            self.isDataSaved(false);

            self.loadSummary();
            self.scrollToLastEdited();
        };

        self.setTextOption = function (option, key) {

            var textLine = self.chosenTextLine();
            var originalLine = self.deepClone(textLine);
            originalLine[key] = option['id'];

            // In addition to option id, save actual value (to be used for completed orders)
            var valueIndex = key + "Value";
            originalLine[valueIndex] = option['name'];
            var imageIndex = key + "Image";
            originalLine[imageIndex] = option['image'];

            self.textLines.remove(textLine);
            self.textLines.push(originalLine);
            self.textLines.sort(function (a, b) {
                return a.sortorder - b.sortorder;
            });

            // Originally: For the Sage site (store id = 2) apply the price only for first text line.
            // Set price to 0 for all other lines.
            //
            // PROD-1793: Happy Chef is updating their embroidery cost on the Sage site to $0. Over-riding
            // the previous decision to make line 1 $4.99. Keep this in case Sage decides to start charging
            // for the first embroidery text line again, which they might do.
            // if (index > 0 && store_id == 2) {
            $.each(self.textLines(), function (index, el) {
                if (store_id === 2) {
                    el.price = 0;
                }
            });

            self.setSelectedOption(self.chosenLocation(), self.chosenType(), self.textLines());
            self.isDataChanged(true);
            if (self.isEditMode()) {
                self.loadSummary();
            } else {
                self.gotoLocation(self.chosenLocation());
                self.validateTextData();
            }
            self.scrollToLastEdited();
        };

        self.closePanel = function () {

            if (!self.isDataValid() && typeof self.chosenType() !== "undefined" && self.productData().types[self.chosenType().id].name === "text" && self.isEditMode()) {
                self.goBack();
                return false;
            }

            if (self.currentPanel() === "logo" && !self.isLogoValid() && self.isEditMode()) {
                return false;
            } else if (self.currentPanel() === "logo" && self.isLogoValid() && self.isEditMode()) {
                self.saveLogo();
            }

            if (self.currentPanel() === 'types') {
                self.doSaveAndExit();
            } else if (self.isDataChanged()) {
                var data = {
                    "title": "",
                    "alertMessage": "Are you sure you want to exit?<br />All unsaved updates will be lost.",
                    "btn1Label": "Yes, Exit Without Saving",
                    "btn1Action": "doCloseDoNotSave",
                    "btn2Label": "No, Continue Editing",
                    "btn2Action": ""
                };

                self.openAlertBox(data);
            } else {
                self.doCloseDoNotSave();
            }
        };

        /**
         * Close Panel Icon
         * A trigger/hook for when the close panel icon is clicked to call self.closePanel(). Easier than chasing dynamically added elements with jQuery.
         * Brought about by the need to track specific elements rendered by knockout.
         */
        self.closePanelIcon = function () {
            $(document).trigger('embroidery-close-panel-icon');
            self.closePanel();
        };

        self.logoLogin = function() {
            $(document).trigger('embroidery-logo-login');
            self.gotoSignIn();
        };

        self.createaccountLogin = function() {
            $(document).trigger('embroidery-createaccount-login');
            self.gotoSignIn();
        };

        self.closeEmbroidery = function () {
            self.updateEmbroideryPrice();
            self.loadSummary();
            self.isDataChanged(false);
            self.keyupIsTriggered(false);
            self.isOpen(false);
            self.hasEmbroidery(self.isEmbroiderySetForProduct());
        };

        self.toggleTierPricing = function () {
            self.showTierPricing(!self.showTierPricing());
        };

        self.openAlertBox = function (data) {

            self.alertData(data);

            $.fancybox({
                href: "#embroidery-alert-" + self.quoteItemId,
                modal: true,
                maxWidth: 280,
                transitionIn: 'fade',
                transitionOut: 'fade',
                showCloseButton: false,
                afterLoad: function () {
                    $(".fancybox-wrap").addClass("si-alert-wrap");
                    /*-- GOOGLE ANALYTICS --------------------------------*/
                    _gaq.push(['_trackEvent', 'embroidery-dialog-shown', data.btn1Action, data.alertMessage]);
                    /*-- GOOGLE ANALYTICS --------------------------------*/
                }
            });

            self.isAlertBoxVisible(true);

        };

        self.callFunction = function (functionName, parametersObject) {

            var getType = {};

            // Invoke function if it exists
            if (self[functionName] && getType.toString.call(self[functionName]) === '[object Function]') {

                if (!$.isEmptyObject(parametersObject)) {
                    self[functionName](parametersObject);
                } else {
                    self[functionName]();
                }
            }
            // Close alert box
            $.fancybox.close();
            self.isAlertBoxVisible(false);

        };

        self.getInstructions = function () {
            return productDataInstructions;
        };

        self.getSelectedOption = function (location, type) {
            var data = self.productData();
            return data["types"][type.id]["locations"][location.id]["selectedOption"];
        };

        self.setSelectedOption = function (location, type, data) {
            var updatedData = self.productData();
            updatedData.types[type.id]['locations'][location.id]["selectedOption"] = data;
            self.productData(updatedData);

            if (type.name === 'text') {
                var arr = type.options;
                var textLines = arr.slice(0, data.length);
                self.textLines(self.makeTextLinesObservable(textLines));
            }
            self.isDataSaved(false);
        };

        self.deepClone = function (object) {
            return $.extend(true, {}, object);
        };

        self.markAsUnsaved = function () {
            self.isDataSaved(false);
        };

        self.getLineLabel = function (textLine) {
            return (typeof textLine !== 'undefined') ? "Line " + textLine.sortorder : "";
        };

        self.updateEmbroideryPrice = function () {

            self.applyTierPrices();
            // update value of hidden input field
            self.embroideryPrice(self.getEmbroideryPrice());

            if ($('.add-to-cart #qty').length) {
                // update tier prices
                $('.product-pricing .tier-price-data').each(function () {
                    var newPrice = parseFloat($(this).val()) + self.embroideryPrice();
                    $(this).next('.price').html('$' + newPrice.toFixed(2));
                });

                self.keyupIsTriggered(true);

                // update total price
                $('.add-to-cart #qty').trigger('keyup');


            }

        };

        self.getEmbroideryPrice = function () {

            var data = self.productData().types;
            var result = find(data, 'selectedOption');
            var price = 0;

            $.each(result, function (index, el) {
                $.each(el, function (idx, option) {
                    price += parseFloat(option.price);
                });
            });

            return price;

        };

        self.getSummaryData = function () {

            var embroideryReverse = [];
            var data = self.deepClone(self.productData());

            // Creates new object based on productData presented in reversed structure: Locations -> Types -> Options
            $.each(data.types, function (typeId, type) {

                var locationObject = {};

                $.each(type.locations, function (locationId, location) {
                    if (location.selectedOption.length > 0) {

                        var typeObject = {};
                        typeObject.name = type.name;
                        typeObject.id = typeId;
                        typeObject.sortorder = type.sortorder;

                        typeObject.options = [];

                        $.each(location.selectedOption, function (index, option) {
                            var optionObject = {};
                            optionObject.id = option.id;
                            optionObject.name = option.name;
                            optionObject.price = option.price;
                            optionObject.textLine = option.textLine;

                            optionObject.textColorId = option.textColor;
                            optionObject.textColorValue = option.textColorValue;

                            optionObject.artworkColorId = option.artworkColor;
                            optionObject.artworkColorValue = option.artworkColorValue;
                            optionObject.artworkColorImage = option.artworkColorImage;

                            optionObject.textStyleId = option.textStyle;
                            optionObject.textStyleValue = option.textStyleValue;
                            optionObject.textLineNumber = option.sortorder;

                            optionObject.image = option.image;
                            optionObject.originalImage = option.originalFilename;

                            typeObject.options.push(optionObject);
                        });

                        var addNewLocation = true;
                        $.each(embroideryReverse, function (idx, loc) {
                            if (loc.id === locationId) {
                                loc.types.push(typeObject);

                                // Resort the array of selected types to match the order of original types array
                                // The assumption is that the original types supplied properly sorted (by php controller)
                                /*
                                 var resortedArr = [];
                                 $.each(self.types(), function(idx, obj){
                                 $.each(loc.types, function(idx1, obj1){
                                 if(obj.name == obj1.name) {
                                 resortedArr.push(obj1);
                                 return false;
                                 }
                                 });
                                 });

                                 loc.types = resortedArr;
                                 */

                                loc.types.sort(function (a, b) {
                                    return a.sortorder - b.sortorder;
                                });

                                addNewLocation = false;
                                return false;
                            }
                        })

                        if (addNewLocation) {
                            locationObject = {};
                            locationObject.name = location.name;
                            locationObject.id = locationId;
                            locationObject.types = [];
                            locationObject.types.push(typeObject);

                            locationObject.types.sort(function (a, b) {
                                return a.sortorder - b.sortorder;
                            });

                            embroideryReverse.push(locationObject);
                        }
                    }
                });
            });
            self.summaryData(embroideryReverse);
            return embroideryReverse;
        };

        self.getQtyInCart = function (typeId, optionId) {

            var qty = 0;
            if (typeof embroideryQtyInCart[typeId] !== "undefined") {
                var opt = filterById(embroideryQtyInCart[typeId], optionId);
                if (typeof opt !== "undefined") {
                    qty = opt.count;
                }
            }

            return qty;


        };

        self.applyTierPrices = function () {

            var applicableTypes = ['3', '5', '6', '7'];
            var types = self.productData().types;
            var ids = {};
            var messageTracker = 0;

            // Count total qty for embroidery applications across all locations
            $.each(types, function (typeId, type) {
                if (applicableTypes.indexOf(typeId) > -1) {
                    ids[typeId] = [];
                    $.each(type.locations, function (idx, location) {
                        if (location.selectedOption.length) {
                            $.each(location.selectedOption, function (idx, option) {
                                var res = filterById(ids[typeId], option.id);
                                if (typeof res !== "undefined") {
                                    var newRes = {"id": option.id, "count": (res.count + 1)};
                                    ids[typeId] = $.grep(ids[typeId], function (e) {
                                        return e.id !== option.id;
                                    });
                                    ids[typeId].push(newRes);
                                } else {
                                    ids[typeId].push({"id": option.id, "count": 1});
                                }
                            });
                        }
                    });
                }
            });

            var productQty = 1;
            var totalQty = 0;

            if ($('#qty').length) {
                productQty = $('#qty').val();
            }
            if (typeof self.quoteItemId !== "undefined" && self.quoteItemId.length) {
                if ($('#cart-' + self.quoteItemId + '-qty-select').length) {
                    productQty = $('#cart-' + self.quoteItemId + '-qty-select').val();
                } else if ($('#cart-' + self.quoteItemId + '-qty')) {
                    productQty = $('#cart-' + self.quoteItemId + '-qty').val();
                }
            }

            $.each(ids, function (typeId, options) {
                $.each(options, function (index, option) {

                    var optionId = option.id;
                    var qtyAppliedNotInCart = option.count;
                    var stitchesCount = 0;

                    if (typeId === 5 && option.id !== 0 && typeof option.id !== 'undefined') { // logos
                        var logoObject = filterById(self.logos(), option.id);
                        stitchesCount = logoObject.stitches;
                    }

                    if (parseInt(qtyAppliedNotInCart) > 0) {
                        qtyAppliedNotInCart = parseInt(productQty) * parseInt(qtyAppliedNotInCart);
                        totalQty = qtyAppliedNotInCart;
                    }

                    var qtyInCart = self.getQtyInCart(typeId, optionId);
                    if (parseInt(qtyInCart) > 0) {
                        totalQty = parseInt(totalQty) + parseInt(qtyInCart);
                    }

                    var type = filterById(allTypes, typeId);
                    var tierPrices = type.tier_prices;
                    var price = type.price;
                    var originalTypePrice = type.price;

                    $.each(tierPrices, function (index, tierPrice) {
                        if (
                            (totalQty >= tierPrice.qty_from && totalQty <= tierPrice.qty_to) &&
                            ((stitchesCount === 0 && typeId !== 5) || (stitchesCount > 0 && (stitchesCount >= parseInt(tierPrice.stitches_count_from) && stitchesCount <= parseInt(tierPrice.stitches_count_to))))

                        ) {
                            price = tierPrice.price;

                            return false;
                        }
                    });

                    if (parseFloat(originalTypePrice) > parseFloat(price)) {
                        messageTracker++;
                    }

                    // change price for specific option on all locations
                    $.each(types, function (typeId, type) {
                        if (applicableTypes.indexOf(typeId) > -1) {
                            $.each(type.locations, function (idx, location) {
                                if (location.selectedOption.length) {
                                    var opt = filterById(location.selectedOption, optionId);
                                    if (typeof opt !== "undefined") {
                                        opt.price = price;
                                        location.selectedOption = $.grep(location.selectedOption, function (e) {
                                            return e.id !== optionId;
                                        });
                                        location.selectedOption.push(opt);
                                    }
                                }
                            });
                        }
                    });

                    var discountMessage = (messageTracker > 0) ? "Embroidery volume discount has been applied!" : "";
                    self.embroideryDiscountMessage(discountMessage);

                });
            });
        };

        self.loadSummary = function () {

            self.showTierPricing(false);
            self.isDataSaved(true);
            self.updateEmbroideryPrice();

            if (self.isEmbroiderySetForProduct()) {

                self.switchPnls('summary');
                self.isEditMode(false);
                var embroideryReverse = self.getSummaryData();
                self.summaryData(embroideryReverse);
                self.scrollToLastEdited();
            } else {
                self.loadTypes();
            }
        };

        self.toggleAccordion = function (elId) {
            var wrapperId = elId + '-wrap';
            if (self.closedAccordions().indexOf(elId) > -1) {
                $('#' + wrapperId).addClass('expanded');
                $('#' + elId).slideDown();
                $.each(self.closedAccordions(), function (index, openLoc) {
                    if (openLoc === elId) {
                        self.closedAccordions().splice(index, 1);
                    }
                });
            } else {
                $('#' + wrapperId).removeClass('expanded');
                $('#' + elId).slideUp();
                if (self.closedAccordions().indexOf(elId) === -1) {
                    self.closedAccordions().push(elId);
                }
            }

        };

        self.validateSuperAttributesAndOpen = function () {

            var message = "";
            var err = false;
            if (($("#attribute92").val() === "" || $("#attribute92").val() === "Choose an Option...") &&
                ($("#attribute213").val() === "" || $("#attribute213").val() === "Choose an Option...")) {
                err = true;
                message = "Please select a Color and Size before applying embroidery.";
            } else if ($("#attribute92").val() === "") {
                err = true;
                message = "Please select a Color before applying embroidery.";
            } else if ($("#attribute213").val() === "") {
                err = true;
                message = "Please select a Size before applying embroidery.";
            }

            if (err) {
                var data = {
                    "title": "",
                    "alertMessage": message,
                    "btn1Label": "Close",
                    "btn1Action": "",
                    "btn2Label": "",
                    "btn2Action": ""
                };
                $(document).trigger('embroidery-alert');
                self.openAlertBox(data);
                return false;

            } else {
                self.isDataChanged(false);
                self.isOpen(true);
                self.hasEmbroidery(self.isEmbroiderySetForProduct());
            }

        };

        self.priceAdvisory = function (type) {
            var advisoryText;
            var typeName = type.name;
            var typePrice = type.price;

            if (typeName === 'flags') {
                advisoryText = "($" + typePrice + " each)";
            } else if (typeName === 'ribbons') {
                advisoryText = "($" + typePrice + " each)";
            } else if (typeName === 'artwork') {
                advisoryText = "($" + typePrice + " each)";
            } else if (typeName === 'acfLogos') {
                advisoryText = "($" + typePrice + " each)";
            } else if (typeName === 'text') {
                if (store_id === 2) {
                    // For Sage, embroidery cost will be $0 until further notice.
                    advisoryText = "($0 per line)";
                } else {
                    advisoryText = "($" + typePrice + " per line)";
                }
            } else if (typeName === 'logo' && store_id !== 2) {
                advisoryText = "Free setup on orders of $150+";
            } else if (typeName === 'patches') {
                advisoryText = "($" + typePrice + " each)";
            } else {
                advisoryText = "";
            }
            return advisoryText;
        };

        self.getScrollPosition = function () {
            self.scrollPosition($('.si-content').scrollTop());
        };

        self.scrollToLastEdited = function () {
            var offset = self.scrollPosition();
            $('.si-content').scrollTop(offset);
        };

        self.showDisabledMessage = function () {
            if ($('.disabledMsg').length) {
                $('.disabledMsg').slideDown(function () {
                    setTimeout(function () {
                        $('.disabledMsg').slideUp();
                    }, 2000);
                });
            }
        };
        self.initDragDropLogo = function () {
            self.bindDropLogo('logo-dropzone');
        };

        self.bindDropLogo = function (dropzone) {
            $(document).on({
                dragstart: function(e) {
                    e.originalEvent.dataTransfer.effectAllowed = 'copyMove';
                },
                dragover: function(e) {
                    if($(e.target).hasClass(dropzone)) {
                        e.originalEvent.dataTransfer.dropEffect = 'copy';
                    }
                    return false;
                },
                drop: function(e) {
                    if($(e.target).hasClass(dropzone)) {
                        self.uploadImage(e.originalEvent.dataTransfer.files[0]);
                    }
                    return false;
                }
            });
        };

        self.chooseFile = function () {
            $('#choose-file').click();
        };

        self.removeFile = function (location, type) {
            var data = {
                "title": "",
                "alertMessage": "Are you sure you want to remove uploaded file?",
                "btn1Label": "Yes",
                "btn1Action": "doRemoveFile",
                "btn2Label": "No",
                "btn2Action": "",
                "arguments": {"location": location, "type": type}

            };
            self.openAlertBox(data);
        };

        self.doRemoveFile = function (parameters) {

            var location = parameters.location;
            var type = parameters.type;

            var filename = self.getLogo(location, type)[0].image;
            self.productData().types[type.id].locations[location.id].selectedOption = [];
            self.productData(self.productData());

            var embroideryReverse = self.getSummaryData();
            self.summaryData(embroideryReverse);

            self.isLogoValid(false);
            self.isTempLogoSet(false);

            new Ajax.Request('/embroidery/index/removeImage', {
                method: 'post',
                parameters: {"filename": filename},
                onComplete: function (response) {
                    //console.log("file removed");
                }
            });

            if (self.currentPanel() === 'logo') {
                self.gotoLocation(location);
            } else {
                self.loadSummary();
            }

        };

        self.getLogo = function (location, type) {
            location = location ? location : self.chosenLocation();
            type = type ? type : self.chosenType();

            return self.productData().types[type.id].locations[location.id].selectedOption;
        };

        self.uploadImage = function (file) {
            // MAXIMUM ALLOWED FILE SIZE (IN MB)
            var maxFileSize = 20;
            var alertMsg = '';

            if(!file) {
                alertMsg = "No file selected.";
            } else if (file.size / Math.pow(1024, 2) >= maxFileSize) {
                alertMsg = "File must be less than " + maxFileSize + "M.";
            } else if (!file.type.match(/image.*/) && !file.type.match(/pdf.*/) && !file.type.match(/postscript.*/)) {
                alertMsg = "Unsupported file type. Supported files include: EPS, AI, PDF, JPG, PNG, and GIF.";
            }

            if (alertMsg !== '') {
                $('#choose-file').val('');
                var data = {
                    "title": "",
                    "alertMessage": alertMsg,
                    "btn1Label": "Continue",
                    "btn1Action": "",
                    "btn2Label": "",
                    "btn2Action": "",
                    "arguments": {}
                }
                self.openAlertBox(data);
                return;
            }

            var formData = new FormData();
            var $imageUploadButton = $('.image-upload-button');
            formData.append('file', file);

            $imageUploadButton.css({"transform": "scaleX(0)"});
            $imageUploadButton.css({"background-color": "#167787"});
            $('.button-background').addClass("shrink");
            $imageUploadButton.removeClass("btn-tall");

            $.ajax({
                url: '/embroidery/index/uploadImage',
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', self.progress, false);
                    }
                    return myXhr;
                },
                complete: function (response) {
                    var result = JSON.parse(response.responseText);

                    if (result.status === false) {
                        $('#choose-file').val('');
                        var data = {
                            "title": "",
                            "alertMessage": result.message,
                            "btn1Label": "Continue",
                            "btn1Action": "",
                            "btn2Label": "",
                            "btn2Action": "",
                            "arguments": {}
                        };
                        self.openAlertBox(data);
                        return;
                    } else {
                        var logo = [
                            {
                                "name": file.name,
                                "image": result.previewFileName,
                                "imageOriginal": result.originalFileName,
                                "price": self.chosenType().price
                            }
                        ];

                        self.logoName(file.name);
                        self.setSelectedOption(self.chosenLocation(), self.chosenType(), logo);
                        self.isDataChanged(true);
                        self.isLogoValid(true);
                        setTimeout(function(){
                            self.isTempLogoSet(true);
                        }, 600);
                    }
                }
            });
        };

        self.progress = function (e) {

            if (e.lengthComputable) {
                var max = e.total;
                var current = e.loaded;

                var percentage = (current * 100) / max;

                if (percentage >= 0) {
                    $('.image-upload-button').css({"transition-duration": "1.2s"});
                }
                $('.image-upload-button').css({"transform": "scaleX("+ current / max +")"});
                if (percentage >= 100) {
                    $('.image-upload-button').css({"transform": "scaleX(1)"});
                    setTimeout(function () {
                        $('.button-background').addClass("fade-out");
                    }, 1200);
                }
            }
        };

        self.validateLogoName = function () {
            if (self.logoName().length > 0) {
                self.isLogoValid(true);
            } else {
                self.isLogoValid(false);
            }
            self.isDataSaved(false);
        };

        self.saveLogoData = function () {

            /*-- HOTJAR TAG --------------------------------*/
            hj('tagRecording', ['Embroidery - upload image']);
            /*-- HOTJAR TAG --------------------------------*/

            var data = self.productData();
            data.types[self.chosenType().id].locations[self.chosenLocation().id].selectedOption[0].name = self.logoName();
            var submittedLogo = data.types[self.chosenType().id].locations[self.chosenLocation().id].selectedOption[0];

            self.productData(data);
            self.logoEditFieldVisible(false);

            // add logo to logos array, so it can be reused
            var newLogo = {
                "id": 0,
                "image": submittedLogo.image,
                "name": submittedLogo.name,
                "max_price": 0,
                "min_price": 0,
                "sortorder": 0,
                "status": 0,
                "stitches": 0
            };

            self.logos().push(newLogo);
            self.logos.sort(function (a, b) {
                return a.sortorder - b.sortorder;
            });

            // save logo in session (server side), so if the page is refreshed it could be still available
            new Ajax.Request('/embroidery/index/addLogoToSession', {
                method: 'post',
                parameters: {"filename": submittedLogo.image, "name": submittedLogo.name},
                onComplete: function (response) {
                    //console.log("file saved");
                }
            });

            self.isTempLogoSet(false);
        };

        self.saveLogo = function () {

            // Get data from file
            var data = self.productData();
            var submittedLogo = data.types[self.chosenType().id].locations[self.chosenLocation().id].selectedOption[0];

            /*-- GOOGLE ANALYTICS --------------------------------*/
            _gaq.push(['_trackEvent', 'logo-upload', submittedLogo.name]);
            /*-- GOOGLE ANALYTICS --------------------------------*/

            if (!self.isSignedIn()) {
                self.gotoCreateAccount();
                /*-- GOOGLE ANALYTICS --------------------------------*/
                _gaq.push(['_trackEvent', 'Create Account Open', 'Embroidery', 'Logo Panel']);
                /*-- GOOGLE ANALYTICS --------------------------------*/
            } else {
                self.saveLogoData();
                self.loadSummary();

            }

        };

        self.trackAndDoAction = function (btn, data) {
            var buttonLabel = btn + "Label";
            var buttonAction = btn + "Action";
            /*-- GOOGLE ANALYTICS --------------------------------*/
            _gaq.push(['_trackEvent', 'embroidery-dialog', data.alertMessage, data[buttonLabel]]);
            /*-- GOOGLE ANALYTICS --------------------------------*/
            self.callFunction(data[buttonAction], data.arguments);
        };

        self.initialize = function () {
            self.loadTypes();
            self.initDragDropLogo();
            self.isOpen(false);
            self.hasEmbroidery(self.isEmbroiderySetForProduct());
        };

    }

})(jQuery);



