jQuery.noConflict();

(function chkHdrReady() {
    var $ = jQuery;
    if ($('#overwrap').length && $('.innerwrap').length) {
        var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|webOS|iPod|BlackBerry|hpwos/i.test(navigator.userAgent);
        HEADER_CONTAINER = $('#overwrap');
        HEADER_ELEMENT_WRAP = $('.header-container');
        if (isMobile) {
            HEADER_CONTAINER.removeClass('headfix');
            HEADER_CONTAINER.addClass('mobHdr');
        }
        else {
            HEADER_CONTAINER.removeClass('headfix');
        }
    }
    else {
        setTimeout(chkHdrReady, 100);
    }
})();

jQuery(document).ready(function() {
    var $ = jQuery;
    var mblBreak = 768;
    var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|webOS|iPod|BlackBerry|hpwos/i.test(navigator.userAgent);
    var isPhone = /iPhone|iPod|Phone|Mobile|Android|webOS|iPod|BlackBerry/i.test(navigator.userAgent);

    NAV_WRAP = $('#topmenu-wrap');
    OVERLAY = $('#full-page-overlay');
    HEADER_ELEMENT = $('.innerwrap');

    function pauseResize() {
        setTimeout(mobNavAppend, 75);
        setTimeout(hoverCart, 75);
        setTimeout(submenus, 75);
        setTimeout(deviceSearch, 75);
    }

    HEADER_CONTAINER_HEIGHT_DESK = "129";
    HEADER_CONTAINER_HEIGHT_MOBILE = "62";
    BASE_URL = getBaseUrl(location.href);
    var $header = $('.header-container');


    function deviceSearch() {
        if (isMobile) {
            $('#search-sbmt-btn').hide();
        } else {
            $('#search-sbmt-btn').show();
        }
    }

    function parseUrl(url) {
        var pattern = RegExp("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
        var matches = url.match(pattern);

        return matches;
    }

    function getBaseUrl(url) {
        var matches = parseUrl(url);
        // return {
        //     scheme: matches[2],
        //     authority: matches[4],
        //     path: matches[5],
        //     query: matches[7],
        //     fragment: matches[9],
        //     config_id: configId
        // };
        return matches[1] + matches[3];
    }

    // MOVE acct-links AND customer-services TO MENU BAR ON SCROLL AND TO MAIN NAV ON MOBILE **************************/
    function mobNavAppend() {
        // to be moved
        var $custServ = $('.customer-services');
        var $acctLinks = $('#myaccount');

        // destinations
        var $hdrLinksGp = $('.hdr-links-gp'); // desktop top-right nav menu
        var $navMobAppend1 = $('.nav-acct-links'); // mobile menu 'holder' for #myaccount
        var $navMobAppend2 = $('.nav-cust-serv');   // mobile menu 'holder' for .customer-services

        // only need to do this if we have the divs to be moved present
        if ($hdrLinksGp.length && $navMobAppend1.length && $navMobAppend2.length && ($custServ.length || $acctLinks.length)) {
            if ($(window).width() <= mblBreak || isMobile) {
                // if mobile, and menu is missing myaccount and customer-service items
                if (!$navMobAppend1.find('#myaccount').length && !$navMobAppend2.find('.customer-services').length) {
                    // attach myaccount link if present
                    if ($acctLinks.length) {
                        $navMobAppend1.append($acctLinks);
                        $navMobAppend1.css('display', 'block');
                    } else {
                        $navMobAppend1.css('display', 'none');
                    }
                    // attach customer-service link
                    $navMobAppend2.append($custServ);
                }
            } else {
                // if desktop, and myaccount link is present, prepend that first before customer-service link, so that
                // customer-service becomes the first link when done
                if ($acctLinks.length) {
                    if (!$hdrLinksGp.find('#myaccount').length
                        && !$hdrLinksGp.find('.customer-services').length) {
                        $hdrLinksGp.prepend($acctLinks).prepend($custServ);
                    }
                }
                // only need to prepend customer-service if myaccount link is not present
                else {
                    $hdrLinksGp.prepend($custServ);
                }
                if (NAV_WRAP.hasClass('open')) {
                    OVERLAY.removeClass('active');
                    NAV_WRAP.removeClass('open');
                }
            }
        }
    }


    (function(){

        var special = jQuery.event.special,
            uid1 = 'D' + (+new Date()),
            uid2 = 'D' + (+new Date() + 1);

        special.scrollstart = {
            setup: function() {

                var timer,
                    handler =  function(evt) {

                        var _self = this,
                            _args = arguments;

                        if (timer) {
                            clearTimeout(timer);
                        } else {
                            evt.type = 'scrollstart';
                            jQuery.event.handle.apply(_self, _args);
                        }

                        timer = setTimeout( function(){
                            timer = null;
                        }, special.scrollstop.latency);

                    };

                jQuery(this).bind('scroll', handler).data(uid1, handler);

            },
            teardown: function(){
                jQuery(this).unbind( 'scroll', jQuery(this).data(uid1) );
            }
        };

        special.scrollstop = {
            latency: 300,
            setup: function() {

                var timer,
                    handler = function(evt) {

                        var _self = this,
                            _args = arguments;

                        if (timer) {
                            clearTimeout(timer);
                        }

                        timer = setTimeout( function(){

                            timer = null;
                            evt.type = 'scrollstop';
                            jQuery.event.handle.apply(_self, _args);

                        }, special.scrollstop.latency);

                    };

                jQuery(this).bind('scroll', handler).data(uid2, handler);

            },
            teardown: function() {
                jQuery(this).unbind( 'scroll', jQuery(this).data(uid2) );
            }
        };

    })();

    (function() {
        $(window).bind('load resize scrollstop', function(e) {
            if (typeof HEADER_ELEMENT_WRAP !== 'undefined') {
                if ($(window).width() > mblBreak && !isMobile) {
                    var headerTop = $(window).scrollTop();
                    if (headerTop > HEADER_CONTAINER_HEIGHT_DESK && !HEADER_CONTAINER.hasClass('headfix')) {
                        HEADER_ELEMENT_WRAP.slideUp(function () {
                            HEADER_CONTAINER.addClass('headfix', function () {
                                HEADER_ELEMENT_WRAP.slideDown('slow');
                            });
                        });
                    }
                    else if (headerTop <= HEADER_CONTAINER_HEIGHT_DESK && HEADER_CONTAINER.hasClass('headfix')) {
                        HEADER_CONTAINER.removeClass('headfix', function () {
                            HEADER_ELEMENT_WRAP.css('display', 'block');
                        });
                    }
                }
                else if (isMobile) {
                    HEADER_CONTAINER.removeClass('headfix');
                    HEADER_CONTAINER.addClass('mobHdr');
                }
                else {
                    HEADER_CONTAINER.removeClass('headfix');
                }
            }
        });
    })();

    (function() {
        var filterWrap = $('.grid_6.sidebar.col-left');
        var mobFilterWrap = $('#mob-filter-wrap');
        var mobFilterContent = $('#mob-filter-content');
        var staticFilterTop;
        var windowTop;

        $(window).on('load resize', function() {
            if(filterWrap.length) {
                staticFilterTop = filterWrap.prev()[0].offsetTop + filterWrap.prev()[0].offsetHeight;
                windowTop = $(window).scrollTop();
                if ($(window).width() <= mblBreak && (windowTop >= (staticFilterTop - HEADER_CONTAINER_HEIGHT_MOBILE))) {
                    filterWrap.addClass('frozen');
                } else if (windowTop < (staticFilterTop - HEADER_CONTAINER_HEIGHT_MOBILE)) {
                    filterWrap.removeClass('frozen');
                }
                ;
            }
        });

        function filterFix() {
            if(filterWrap.length) {
                windowTop = $(window).scrollTop();
                if (windowTop >= (staticFilterTop - HEADER_CONTAINER_HEIGHT_MOBILE)) {
                    filterWrap.addClass('frozen');
                } else if (windowTop < (staticFilterTop - HEADER_CONTAINER_HEIGHT_MOBILE)) {
                    if (filterWrap.hasClass('frozen')) {
                        filterWrap.removeClass('frozen');
                    }
                }
                ;
            }
        }

        $(window).on('scroll', filterFix);

        $('#mob-filter-btn').on('click', function () {
            filterFix();
            if (mobFilterWrap.hasClass('expanded')) {
                mobFilterContent.slideUp();
                mobFilterWrap.removeClass('expanded');
            } else {
                var screenSpace = $(window).height() - mobFilterWrap[0].offsetTop - $('#mob-filter-btn').outerHeight(true) - 10;
                mobFilterWrap.css('max-height', screenSpace);
                mobFilterContent.slideDown();
                mobFilterWrap.addClass('expanded');
            }
        });

    })();


    //************************************************************/
    // COLLAPSE LAYERED NAV BY DEFAULT ON MOBILE
    //************************************************************/
    (function(){
        $(window).on('load resize', function(){
            if($(window).width() <= 768){
                $('.block-layered-nav.amshopby-collapse-enabled #narrow-by-list dt').addClass('amshopby-collapsed');
            }
        });
    })();
    //************************************************************/
    // COLLAPSE LAYERED NAV BY DEFAULT ON MOBILE
    //************************************************************/

    //************************************************************/
    // FIXED HEADER
    //************************************************************/


    //************************************************************
    // SHOW/HIDE MAIN MENU
    //************************************************************
    function menucollapse() {
        var mobMenuBtn = $('.mob-menu-btn');
        var ddCartBtn = $('.dropdown-cart');

        OVERLAY.on('click', function(){
            OVERLAY.removeClass('active');
            $('.not-header-overlay').removeClass('active');
            NAV_WRAP.removeClass('open');
            if ($('.dropdown-cart').hasClass('open')) {
                $('.dropdown-cart').removeClass('open');
            }
            $('html, body').removeClass('noscroll');
        })

        mobMenuBtn.on('click', function (e) {
            $('.myaccount-content').removeClass('open');
            $('.myaccount-content').stop(true, true).slideUp('fast');

            if (ddCartBtn.hasClass('open')) {
                ddCartBtn.removeClass('open');
            }
            $('html, body').addClass('noscroll');
            OVERLAY.addClass('active');
            NAV_WRAP.addClass('open');
        });
    }

    $(window).on('load resize', menucollapse);


    //************************************************************/
    // MAIN MENU SUBMENU DESKTOP AND MOBILE BEHAVIOR
    //************************************************************/
    function submenus() {
        if ($(window).width() <= mblBreak || isMobile) {

            // MAKE MENU ITEM SEGMENTS CLICKABLE
            $('.menu-item-link, .menu-item-text').each(function() {
                var ref = $(this).find('a').attr('href');
                if (ref) {
                    $(this).on('click', function (e) {
                        e.stopPropagation();
                        window.location.href = ref;
                    });
                }
            });

            if (!$('.curr-pnl').length) {
                $('.nav-si-panel[data-level=0]').removeClass('next-pnl prev-pnl').addClass('curr-pnl');
            }

            $('.menu-item-parent').off('mouseenter mouseleave click').on('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var $menuItem = $(this);
                var $parentPnl = $menuItem.closest('.nav-si-panel');
                var currLvl = $parentPnl.data('level');
                var nextLvl = currLvl + 1;
                var subMenuId = $menuItem.attr('id');

                $('.nav-si-panel').each(function() {
                    var $pnl = $(this);
                    var pnlLvl = $pnl.data('level');
                    if (nextLvl == pnlLvl) {
                        $pnl.children('.nav-si-content').each(function(){
                            $(this).hasClass(subMenuId) ? $(this).show() : $(this).hide();
                        })
                        $(this).removeClass('next-pnl prev-pnl').addClass('curr-pnl');
                    } else if (nextLvl < pnlLvl) {
                        $(this).removeClass('curr-pnl prev-pnl').addClass('next-pnl');
                    } else {
                        $(this).removeClass('curr-pnl next-pnl').addClass('prev-pnl');
                    }
                });
            });

            $('.mob_subhead').off('mouseenter mouseleave click').on('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var $menuItem = $(this);
                var $parentPnl = $menuItem.closest('.nav-si-panel');
                var currLvl = $parentPnl.data('level');
                var prevLvl = currLvl - 1;

                $('.nav-si-panel').each(function() {
                    var $pnl = $(this);
                    var pnlLvl = $pnl.data('level');
                    if (prevLvl == pnlLvl) {
                        $(this).removeClass('next-pnl prev-pnl').addClass('curr-pnl');
                    } else if (prevLvl < pnlLvl) {
                        $(this).removeClass('curr-pnl prev-pnl').addClass('next-pnl');
                    } else {
                        $(this).removeClass('curr-pnl next-pnl').addClass('prev-pnl');
                    }
                });
            });

        } else {
            //************************************************************
            // TOP LEVEL SUBMENU ITEMS
            //************************************************************
            $('.nav-si-panel').removeClass('next-pnl prev-pnl curr-pnl');
            $('html, body').removeClass('noscroll');

            $('.menu-item-parent').each(function() {
                var $subMenuWrap = $(this);
                var tm;

                if (!isMobile) {
                    $subMenuWrap.on('mouseenter', function () {
                        var $subMenu = $(this).find('.desksub');
                        $subMenuWrap.addClass('open');
                        clearTimeout(tm);
                        tm = setTimeout(function () {
                            $subMenu.stop(true, true).slideDown();
                        }, 200);
                    });
                    $subMenuWrap.on('mouseleave', function () {
                        var $subMenu = $(this).find('.desksub');
                        clearTimeout(tm);
                        tm = setTimeout(function () {
                            $subMenu.stop(true, true).slideUp(function(){
                                $subMenuWrap.removeClass('open');
                            });
                        }, 200);
                    });
                }
            });

        }
    }

    $('#moreReviews').on('click', function(){
        /*-- HOTJAR TAG --------------------------------*/
        hj('tagRecording', ['More Reviews (PDP) - reviews']);
        /*-- HOTJAR TAG --------------------------------*/
    })


    //************************************************************/
    // CART
    //************************************************************/
    function hoverCart() {
        var menuwrap  = $('.dropdown-cart');
        var btn = $('.dropdown-cart-btn-wrap');
        if ($(window).width() > mblBreak && !isMobile) {
            btn.off('click');
            var tm;
            menuwrap.on('mouseenter', function(){
                menuwrap.addClass('open');
                clearTimeout(tm);
                tm = setTimeout(function () {
                    $('.cart-popup').stop(true, true).slideDown();
                }, 200);
            });
            menuwrap.on('mouseleave', function(){
                menuwrap.removeClass('open');
                clearTimeout(tm);
                tm = setTimeout(function () {
                    $('.cart-popup').stop(true, true).slideUp();
                }, 1000);
            });
        }
        else {
            menuwrap.off('mouseenter mouseleave');
            btn.off('click').on('click', function(e){
                e.preventDefault();
                $('.not-header-overlay').removeClass('active');
                if (menuwrap.hasClass('open')) {
                    menuwrap.removeClass('open');
                    OVERLAY.removeClass('active');

                    $('html, body').removeClass('noscroll');
                }
                else {
                    menuwrap.addClass('open');
                    OVERLAY.addClass('active');
                    $('html, body').addClass('noscroll');

                    // if ($('#topsearch').hasClass('open')) {
                    //     $('#topsearch').removeClass('open');
                    // }
                    if ($('#topsearch-content').hasClass('open')) {
                        $('#topsearch-content').removeClass('open');
                    }

                    $('#topsearch-content').stop(true, true).slideUp('fast');
                }
            })
        }
    }
    //************************************************************/
    // CART
    //************************************************************/


    //************************************************************/
    // TOPSEARCH
    //************************************************************/
    $('.topsearch-link-wrap').on('click', searchToggle);
    $('.not-header-overlay').on('click', searchToggle);

    function searchToggle() {
        togglePlaceholder($('#search'));
        var menu = $('#topsearch-content');
        var searchGlass = $('.search-glass');
        var crossEls = $('*[class^="search-cross-"], *[class*=" search-cross-"]');
        var crossEl1 = $('.search-cross-1');
        var crossEl2 = $('.search-cross-2');
        searchGlass.stop(true, true);
        crossEls.stop(true, true);
        if (menu.hasClass('open')) {
            menu.removeClass('open');
            $('html, body').removeClass('noscroll');
            $('.not-header-overlay').removeClass('active');
            menu.stop(true, true).slideUp('fast');

            searchGlass.animate({
                opacity: '1',
                top: '0'
            }, 200)
                .queue(function() {
                    crossEl2.animate({
                        height: '0'
                    }, 300).queue(function() {
                        searchGlass.animate({
                            width: '60%',
                        }, 300);
                        crossEl1.animate({
                            height: '100%'
                        }, 300);
                    }).dequeue();
                }).dequeue();
        }
        else {
            menu.addClass('open');
            $('html, body').addClass('noscroll');
            $('.not-header-overlay').addClass('active');
            menu.stop(true, true).slideDown('fast');
            menu.find('#search').focus();
            if ($('.dropdown-cart').hasClass('open')) {
                $('.dropdown-cart').removeClass('open');
            }
            if ($(window).width() > mblBreak) {
                $('.cart-popup').stop(true, true).slideUp('fast');
            }
            /*-- HOTJAR TAG --------------------------------*/
            hj('tagRecording', ['Search button - search']);
            /*-- HOTJAR TAG --------------------------------*/

            searchGlass.animate({
                width: ['0', 'linear'],
                top: '20%'
            }, 200).queue(function() {
                    searchGlass.animate({
                        opacity: '0'
                    }, 200);
                    crossEls.animate({
                        height: '80%'
                    }, 300);
                    $(this).dequeue();
                });
        }
    }

    function searchAutocompleteHeight() {
        var autocompleteWrap = $('#search_autocomplete');
        var padding = parseInt(autocompleteWrap.css('padding-top').split('px')[0]) * 2;
        var precedingEl = $('#search-result_mini_form');
        var precedingElBottomPos = $('#topsearch-content').offset().top + precedingEl.outerHeight();
        var viewHeight = $(window).height();

        autocompleteWrap.css('max-height', viewHeight - precedingElBottomPos - padding);
    }

    $('#search').on('keyup', function(){
        searchAutocompleteHeight();
        togglePlaceholder($(this));
    });

    $('#search-result__input').on('keyup', function(){
        togglePlaceholder($(this));
    });

    $('.search-result__input-clear').on('click', function(){
        $(this).siblings('input[type="text"]').val('');
        $('.search-input-placeholder').css('display', 'block');
    });

    function togglePlaceholder(input) {
        if(input.val() != '') {
            $('.search-input-placeholder').css('display', 'none');
        }
        else {
            $('.search-input-placeholder').css('display', 'block');
        }
    }

    togglePlaceholder($('#search'));
    togglePlaceholder($('#search-result__input'));

    $('.search-input-placeholder').on('click', function(){
        $(this).prev('input').focus();
    })

    $('.referral-input-placeholder').on('click', function(){
        $(this).closest('.input-box').find('input').focus();
    })
    //************************************************************/
    // TOPSEARCH
    //************************************************************/


    //************************************************************/
    // ADJUST HEADER FOR NOTIFICATION BAR
    //************************************************************/
    $(window).on('load resize orientationchange', function () {
        if ($('#notification-bar').length) {
            if(window.innerWidth <= mblBreak) {
                $('.wrapper').css('padding-top', $('#notification-bar').outerHeight(true));
                $('.mob-menu-btn').css('padding-top', 0);
            } else {
                $('.wrapper, .mob-menu-btn').css('padding-top', $('#notification-bar').outerHeight(true));
            }
        } else {
            $('.wrapper, .mob-menu-btn').css('padding-top', 0);
        }
    });
    //************************************************************/
    // ADJUST HEADER FOR NOTIFICATION BAR
    //************************************************************/

    /************************************************************/
    // ADD TITLES TO COOKCOOL AND SMART NAV ICONS
    //************************************************************/
    NAV_WRAP.find('.logo-1').attr('title', 'CookCool®');
    NAV_WRAP.find('.logo-2').attr('title', '#SMART™');
    //************************************************************/
    // ADD TITLES TO COOKCOOL AND SMART NAV ICONS
    //************************************************************/

    $(window).on('load resize', pauseResize);
});
//************************************************************/
//END DOCUMENT READY
//************************************************************/
