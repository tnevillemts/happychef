var embroideryData = {
    92: {
        "locations": {
            10: {
                "types": {
                    121: {
                        "selectedOption": [1]
                    },
                    122: {
                        "selectedOption": [2]
                    },
                    123: {
                        "selectedOption": [1]
                    },
                    124: {
                        "selectedOption": [
                            {"artworkName": "my precious", "media": "/media/999.jpg"}
                        ]
                    },
                    125: {
                        "selectedOption": [
                            { 1 : {"textLine": "Hello1", "textColor": 3, "textStyle": 1}},
                            { 2 : {"textLine": "Hello2", "textColor": 2, "textStyle": 3}},
                        ]
                    }
                },
                "selectedInstructions": "don't mess up"
            },
            12: {
                "types": {
                    121: {
                        "selectedOption": [3]
                    },
                    122: {
                        "selectedOption": []
                    }
                },
                "selectedInstructions": ""
            }
        }
    }
};

var embroideryLocations = {
    10: {"name": "Left Chest", "sort":10},
    11: {"name": "Right Chest", "sort":20},
    12: {"name": "Center", "sort":30},
}

var embroideryTypes = {
    121:
    {
        "name": "ribbons",
        "price": 5.99,
        "size": "3 1/4",
        "options": {
            1: {"name": "Red White Green", "sort": 10, "image": "/media/blah.jpg"},
            2: {"name": "Red White Blue", "sort": 20, "image": "/media/blah.jpg"},
            3: {"name": "Red White Yellow", "sort": 30, "image": "/media/blah.jpg"},
        }
    },

    122: {
        "name": "flags",
        "price": 5.99,
        "size": "3 1/4",
        "options": {
            1: {"name": "Canada", "sort": 10, "image": "/media/blah.jpg"},
            2: {"name": "Mexico", "sort": 20, "image": "/media/blah.jpg"},
            3: {"name": "USA", "sort": 30, "image": "/media/blah.jpg"},
        }
    },

    123: {
        "name": "art",
        "price": 5.99,
        "size": "3 1/4",
        "options": {
            1: {"name": "Chef", "sort": 10, "image": "/media/blah.jpg"},
            2: {"name": "Fork", "sort": 20, "image": "/media/blah.jpg"},
            3: {"name": "Spoon", "sort": 30, "image": "/media/blah.jpg"}
        }
    },

    124: {
        "name": "custom",
        "price": 9.99,
        "size": "3 1/4",
        "options": {}
    },

    125: {
        "name": "text",
        "price": 4.99,
        "options": {
            1: {"name": "Line 1", "sort": 10},
            2: {"name": "Line 2", "sort": 20},
            3: {"name": "Line 3", "sort": 30}
        }
    }
};


var embroideryTextStyles = {
    1: {"name": "Block", "sort": 10, "image": "/media/blah.jpg"},
    2: {"name": "Script", "sort": 20, "image": "/media/blah.jpg"},
    3: {"name": "Gouda", "sort": 30, "image": "/media/blah.jpg"},
};

var embroideryTextColors = {
    1: {"name": "Red", "sort": 10, "image": "/media/blah.jpg"},
    2: {"name": "Orange", "sort": 20, "image": "/media/blah.jpg"},
    3: {"name": "Purple", "sort": 30, "image": "/media/blah.jpg"},
};

function objectToArray (obj, index) {
    var arr = [];
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            obj[key][index] = key;
            arr.push(obj[key]);
        }
    };
    return arr;
}


jQuery.noConflict();
jQuery(document).ready(function($){

    function cloneSimpleObject(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    function filterObject( applicableObj, sourceObj ) {
        var newObj = {};
        $.each(applicableObj, function (id) {
            newObj[id] = sourceObj[id];
        });
        return newObj;
    }

    function isEmbroiderySetForLocation ( types ){
        var toReturn = false;
        $.each(types, function(key, el){
            if (el.selectedOption.length){
                toReturn = true;
                return true;
            }
        });
        return toReturn;
    }

    function isEmbroiderySetForType ( selectedOption ){

        if (selectedOption.length){
            return true;
        }
        return false;
    }

    function isEmbroiderySetForOption ( selectedOption, index ){

        var toReturn = false;
        $.each(selectedOption, function(key, el){
            if ((typeof el == 'object' && Object.keys(el)[0] == parseInt(index)) || el == parseInt(index)){
                    toReturn = true;
                    return true;
            }
        });
        return toReturn;
    }

    function getSelectedTextLineById(options, id) {

        var toReturn = false;
        $.each(options, function(key, el){
            if(!$.isEmptyObject(el[id])) {
                toReturn = el[id];
                return;
            }
        })

        return toReturn;
    }

    function EmbroideryViewModel() {

        var self = this;
        var productId = 92;

        self.locations = ko.observable();
        self.chosenLocationId = ko.observable();
        self.types = ko.observable();
        self.chosenTypeId = ko.observable();
        self.embroideryOptions = ko.observable();

        // Behaviours
        self.loadLocations = function(productId) {

            self.locations(null);

            var appliedLocations = embroideryData[productId]['locations'];
            var _locations = filterObject(appliedLocations, embroideryLocations);

            $.each(_locations, function(index){
                _locations[index]["state"] = isEmbroiderySetForLocation(embroideryData[productId]['locations'][index]['types']);
            });

            self.locations(objectToArray(_locations, "id"));
        };

        self.loadTypes = function(location) {

            self.chosenLocationId(location);
            self.chosenTypeId(null);
            self.types(null);

            var appliedTypes = embroideryData[productId]['locations'][location.id]["types"];
            var _types = filterObject(appliedTypes, embroideryTypes);

            $.each(_types, function(index){
                _types[index]["state"] = isEmbroiderySetForType(embroideryData[productId]['locations'][location.id]['types'][index]["selectedOption"]);
            });

            self.types(objectToArray(_types, "id"));

        };

        self.loadEmbroideryOptions = function(type) {

            self.embroideryOptions(null);
            self.chosenTypeId(type);

            var appliedOptions = embroideryData[productId]['locations'][self.chosenLocationId().id]["types"][type.id]["selectedOption"];
            var _options = embroideryTypes[type.id]["options"];

            $.each(_options, function(index){

                _options[index]["state"] = isEmbroiderySetForOption(appliedOptions, index);

                if (type.name == 'text') {

                    var lineData = getSelectedTextLineById(appliedOptions, index);

                    var _styles = cloneSimpleObject(embroideryTextStyles);
                    for (var prop in _styles){
                        _styles[prop]["state"] = (lineData["textStyle"] == prop);
                    }

                    var _colors = cloneSimpleObject(embroideryTextColors);
                    $.each(_colors, function(index){
                        _colors[index]["state"] = (index == lineData["textColor"]);
                    })

                    var textData =
                        {
                            "textLabel": "Line " + index,
                            "textLine": lineData["textLine"],
                            "textStyles": objectToArray(_styles, "id"),
                            "textColors": objectToArray(_colors, "id")
                        };

                    _options[index]["textData"] = textData;
                }

            });

            self.embroideryOptions(objectToArray(_options, "id"));

            /*
            if (type.name == 'text') {

                //self.textStyles(objectToArray(embroideryTextStyles, "id"));
                //self.textColors(objectToArray(embroideryTextColors, "id"));

            } else {
                self.textStyles(null);
                self.textColors(null);
            }
            */

        };


        self.loadLocations(productId);


    };

    ko.applyBindings(new EmbroideryViewModel());



    //open the lateral panel
    $('.embroidery-btn').on('click', function(event){
        event.preventDefault();
        $('.cd-panel').addClass('is-visible');
    });
    //close the lateral panel
    $('.cd-panel-close').on('click', function(event){
            $('.cd-panel').removeClass('is-visible');
            event.preventDefault();
    });
});



/*
 function AppViewModel() {

 var self = this;
 var embroideryLocationsArr = objectToArray(embroideryLocations, "id");
 self.locations = ko.observableArray(embroideryLocationsArr);

 self.addItem =  function() {
 self.locations.push({"id": 13, "name": "Cuff", "sort":40});
 }

 self.deleteItem =  function(item) {
 self.locations.remove(item);
 }
 }

 ko.applyBindings(new AppViewModel());

 */