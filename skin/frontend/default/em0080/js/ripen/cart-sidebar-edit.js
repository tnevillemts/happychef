/**
 * Created by zcox on 7/24/17.
 */
// jQuery(document).ready(function() {
//     var $ = jQuery;

    function parseUrl(url) {
        var pattern = RegExp("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
        var matches = url.match(pattern);

        return matches;
    }

    function getConfigId(url) {
        var regExp = RegExp("^\/checkout\/cart\/configure\/id\/([^\/]+)\/");
        var matches = parseUrl(url);
        return matches[5].match(regExp)[1];
    }

    function getBaseUrl(url) {
        var regExp = RegExp("^\/checkout\/cart\/configure\/id\/([^\/]+)\/");
        var matches = parseUrl(url);
        // return {
        //     scheme: matches[2],
        //     authority: matches[4],
        //     path: matches[5],
        //     query: matches[7],
        //     fragment: matches[9],
        //     config_id: configId
        // };
        return matches[1] + matches[3];
    }

    function closeSidebarEdit() {
        var $ = jQuery;
        if ($('.cart-sidebar-edit') != 'undefined' && $('.cart-sidebar-edit').length) {
            if ($('.si-overlay').hasClass('active')) {
                $('.si-overlay').removeClass('active');
            }
            if ($('.si').hasClass('vis')) {
                $('.si').removeClass('vis');
            }
            if ($('html, body').hasClass('noscroll')) {
                $('html, body').removeClass('noscroll');
            }

            destroySidebarEdit();
        }
    }

    function destroySidebarEdit() {
        var $ = jQuery;

        if ($('.cart-sidebar-edit') != 'undefined' && $('.cart-sidebar-edit').length) {
            $('.cart-sidebar-edit').remove();
        }
    }

    function openSidebarEdit(configId) {
        var $ = jQuery;

        if ($('#' + configId)[0] && !$('#' + configId).hasClass('vis')) {
            setTimeout(function () {
                $('#' + configId).addClass('vis');
            }, 250);
        }

        if ($('.si-overlay')[0] && !$('.si-overlay').hasClass('active')) {
            $('.si-overlay').addClass('active');
        }

        $('html, body').addClass('noscroll');
    }

    function createSidebarEdit(url) {
        var $ = jQuery;

        /*-- HOTJAR TAG --------------------------------*/
        // hj('tagRecording', ['Cart - Edit Item']);
        /*-- HOTJAR TAG --------------------------------*/

        if ($('.cart-sidebar-edit') == 'undefined' || !$('.cart-sidebar-edit').length) {
            var configId = getConfigId(url);
            var baseUrl = getBaseUrl(url);
            var html = '<div id="' + configId + '" class="si si-rt cart-sidebar-edit">' +
                    '<div id="sidebarOverlay"></div>' +
                    '<div class="si-viewport">' +
                        '<div class="si-panel currPnl">' +
                            '<div class="si-header">' +
                                '<div class="row-l1">' +
                                    '<div class="row-content">' +
                                        '<div class="si-panel-title">EDIT</div>' +
                                    '</div>' +
                                    '<div class="si-icon-wrap vcenter si-icon-wrap-rt">' +
                                        '<svg id="cart-sidebar-edit-close" class="si-close" onclick="closeSidebarEdit();" viewBox="0 0 26 26">' +
                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + baseUrl + '/skin/frontend/default/em0080/images/icons/si-icon.svg#close"></use>' +
                                        '</svg>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="sidebarEdit-wrap">' +
                                '<iframe id="sidebarEdit_' + configId + '" class="sidebarEdit" src="' + url + '"></iframe>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            $('body').append(html);
        }

        var baseUrl = getBaseUrl(url);
        var configId = getConfigId(url);

        openSidebarEdit(configId);

        $('.si-overlay').on('click', closeSidebarEdit);
    }

// });



