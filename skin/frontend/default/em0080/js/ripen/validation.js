/*
 * Ripen Validation, functions defined here will override what's on Magento core prototype validation at js/prototype/validation.js
 *
 * Please use this function to make changes to existing validation functions, and or add new ones
 */

Validation.addAllThese([
    ['validate-cc-cvv', 'Please enter a valid credit card verification number.', function(v, elm) {
        var ccTypeContainer = document.getElementById('cc_type');

        if (!ccTypeContainer) {
            return true;
        }
        var ccType = ccTypeContainer.value;

        if (typeof Validation.creditCartTypes.get(ccType) == 'undefined') {
            return false;
        }

        var re = Validation.creditCartTypes.get(ccType)[1];

        if (v.match(re)) {
            return true;
        }

        return false;
    }]
]);
