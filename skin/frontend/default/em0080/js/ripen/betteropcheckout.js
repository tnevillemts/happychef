(function ($) {
    $.Bettercheckout = function () {
    };

    $.Bettercheckout.prototype = {
        baseSkinPath: '',
        baseSkinImagePath: '',
        cvvTooltip: {
            selectors: {
                icon: '#cvv-tooltip-icon',
                content: '#cvv-tooltip-content'
            },
            helpImages: {
                visa: 'security-code-visa.png',
                mastercard: 'security-code-default.png',
                amex: 'security-code-amex.png',
                discover: 'security-code-default.png'
            }
        },
        addressBook: {},
        paymentBook: {},
        defaultShippingParams: {
            regionId: null,
            street: null,
            shippingMethodId: null,
            countryId: null
        },
        inputMasks: {
            currentMask: false,
            defaultCcMask: 'visa',
            telephone: {
                default: '( 999 ) 999 - 9999'
            },
            postCode: {
                CA: 'a9a 9a9',
                US: '99999',
                PR: '99999',
                VI: '99999'
            },
            creditCard: {
                visa: {
                    number: '9999 9999 9999 9999',
                    cvv: '999'
                },
                mastercard: {
                    number: '9999 9999 9999 9999',
                    cvv: '999'
                },
                amex: {
                    number: '9999 999999 99999',
                    cvv: '9999'
                },
                discover: {
                    number: '9999 9999 9999 9999',
                    cvv: '999'
                }
            }
        },
        ccTypes: {'VI':'visa', 'AE':'amex', 'MC':'mastercard', 'DI':"discover"},
        isMobile: /iPhone|iPod|iPad|Phone|Mobile|Android|webOS|iPod|BlackBerry|hpwos/i.test(navigator.userAgent),

        setDefaultShippingParams: function(regionId, street, shippingMethodId, countryId) {
            var that = this;
            that.defaultShippingParams.regionId = regionId;
            that.defaultShippingParams.street = street;
            that.defaultShippingParams.shippingMethodId = shippingMethodId;
            that.defaultShippingParams.countryId = countryId;
        },

        renderCheckoutTotals: function (flag) {
            var that = this;
            new Ajax.Request('/bettercheckout/checkout/renderTotals', {
                method: 'post',
                onComplete: function (response) {
                    var result = $.parseJSON(response.responseText);

                    // Re-syncronize this portion of the code.
                    if ($('#estimate_method_set').val() != 'set') {
                        var checkSet = setInterval(function() {
                            var countryCode = $('#shipping\\:country_id').val();
                            if(countryCode == 'PR' || countryCode == 'VI') {
                                if(flag) {
                                    $("#checkout-recap-totals").html(result.totals);
                                }
                                clearInterval(checkSet);
                            }
                        }, 1000);
                    } else {
                        $("#checkout-recap-totals").html(result.totals);
                        that.enablePlaceOrderElements();
                    }

                    if (result.cartTotal > 0) {
                        $('.payment-not-required-box').hide();
                        $('.payment-not-required-box input').attr('disabled', 'disabled');
                        $('.checkout-cc-info').show();
                        $('.checkout-cc-info input, .checkout-cc-info select').removeAttr('disabled');
                        $('.gc-success-message-link, .checkout-gc-link').show();
                    } else {
                        $('.checkout-cc-info').hide();
                        $('.checkout-cc-info #cc_method, #cc_type, #cc_number, #cc_expiration, #cc_expiration_yr, #cvv').attr('disabled', 'disabled');
                        $('.payment-not-required-box').show();
                        $('.payment-not-required-box input').removeAttr('disabled');
                        $('.gc-success-message-link, .checkout-gc-link').hide();
                    }
                }
            });
        },

        setBillingState: function (regionId) {
            new Ajax.Request('/bettercheckout/checkout/setBillingState', {
                method: 'post',
                parameters: {"region_id": regionId}
            });
        },

        renderCanadianNote: function (countryId) {
            if (countryId == 'CA') {
                $('.canadian-note').show();
            } else {
                $('.canadian-note').hide();
            }
        },

        renderRegions: function (countryId, type, defaultRegionId, defaultShippingMethodId) {
            var that = this;
            that.disablePlaceOrderElements();

            if(defaultRegionId == "52") {
                countryId = 'PR';
            }
            if(defaultRegionId == "60") {
                countryId = 'VI';
            }

            that.formatPostcode(countryId, type);
            that.renderCanadianNote(countryId);
            $('#estimate-delivery-time').html("");

            new Ajax.Request('/bettercheckout/checkout/getRegionsJson', {
                method: 'post',
                parameters: {"country_id": countryId, "address_type": type},
                onComplete: function (response) {
                    var result = $.parseJSON(response.responseText);
                    var shippingStreet = $('#shipping\\:street1').val() ? $('#shipping\\:street1').val() : "";

                    if (countryId == 'CA') {
                        defaultRegionLabel = "Select a Province...";
                    } else if (countryId == 'PR' || countryId == 'VI') {
                        defaultRegionLabel = "Not Applicable";
                        $('#'+type+'\\:region_id').removeClass('validate-select conditional-required');

                        if (type == 'shipping') {
                            that.renderShippingMethods(null, shippingStreet, defaultShippingMethodId, countryId);
                        }
                    }


                    $('#'+type+'\\:region_id').empty();

                    $(result.regions).each(function () {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);

                        if (type == 'shipping') {
                            if (defaultRegionId == this.value) {
                                option.attr('selected', 'selected');

                                that.renderShippingMethods(defaultRegionId, shippingStreet, defaultShippingMethodId, countryId);
                            } else if ($('#shipping\\:region_id_default').val().length && $('#shipping\\:region_id_default').val() == this.label) {

                                option.attr('selected', 'selected');

                                that.renderShippingMethods(this.value, shippingStreet, defaultShippingMethodId, countryId);
                            } else if ($('#shipping\\:region_id_default').val().length && $('#shipping\\:region_id_default').val() == this.value) {
                                option.attr('selected', 'selected');

                                that.renderShippingMethods(this.value, shippingStreet, defaultShippingMethodId, countryId);
                            }
                        }

                        if (defaultRegionId == this.value) {
                            option.attr('selected', 'selected');
                        }

                        $('#'+type+'\\:region_id').append(option);
                    });

                    if (result.regions.length > 1 && $('#'+type+'\\:region_id').val() == "") {
                        if($('#is-shipping-as-billing').is(':checked') == false){
                            $('#'+type+'\\:region_id').addClass('default-selected validate-select');
                        }
                    }

                    if(($('shipping-address-select').length > 0) && !$('shipping-address-select').val()) {
                        that.enablePlaceOrderElements();
                    }
                    that.enablePlaceOrderElements();
                }
            });
        },

        initializeCheckout: function(config) {
            var that = this;
            that.baseSkinPath = config.baseSkinPath;
            that.baseSkinImagePath = config.baseSkinPath + 'images/';
            that.initCheckoutUI();
            that.disablePlaceOrderElements();


            new Ajax.Request('/bettercheckout/checkout/initializeCheckout', {
                method: 'post',
                onComplete: function (response) {
                    var result = $.parseJSON(response.responseText);

                    var countryId = result.addressData.shipping.countryId;
                    var defaultRegionId = result.addressData.shipping.regionId;

                    if(defaultRegionId == "52") {
                        countryId = 'PR';
                    }
                    if(defaultRegionId == "60") {
                        countryId = 'VI';
                    }

                    that.renderCanadianNote(countryId);
                    $('#estimate-delivery-time').html("");

                    that.renderRegionsOptions(result.addressData.shipping);
                    that.renderRegionsOptions(result.addressData.billing);
                    that.getCheckoutFormData('shipping', result.checkoutForm);
                    that.getCheckoutFormData('billing', result.checkoutForm);
                    that.renderShippingMethodsOptions(
                        that.defaultShippingParams.regionId,
                        that.defaultShippingParams.street,
                        that.defaultShippingParams.countryId,
                        result);
                    that.renderTotals(true, result.checkoutTotals);

                    that.enablePlaceOrderElements();
                }
            });
        },

        renderRegionsOptions: function(addressData) {
            var that = this;
            var type = addressData.type;
            if($('#' + type + '\\:region_id').length > 0) {
                var defaultRegionLabel = "Select a State...";
                var defaultRegionId = addressData.regionId;
                var shippingStreet = $('#shipping\\:street1').val() ? $('#shipping\\:street1').val() : "";
                var countryId = addressData.countryId;
                var defaultShippingMethodId = addressData.defaultShippingMethodId;

                that.formatPostcode(countryId, type);
                if (countryId == 'CA') {
                    defaultRegionLabel = "Select a Province...";
                } else if (countryId == 'PR' || countryId == 'VI') {
                    defaultRegionLabel = "Not Applicable";
                    $('#'+type+'\\:region_id').removeClass('validate-select conditional-required');

                    if (type == 'shipping') {
                        that.setDefaultShippingParams(null, shippingStreet, defaultShippingMethodId, countryId);
                    }
                }

                $('#'+type+'\\:region_id').empty();

                $(addressData.regions).each(function () {
                    // ORIGINAL ---------------------------------------------------------//
                    var option = $('<option />');
                    option.attr('value', this.value).text(this.label);

                    if (type == 'shipping') {
                        if (defaultRegionId == this.value) {
                            // ORIGINAL ---------------------------------------------------------//
                            option.attr('selected', 'selected');
                            // ---------------------------------------------------------//
                            // $(option + ' input').attr('checked', 'checked');
                            // option.children('input').attr('checked', 'checked');
                            // NEW ---------------------------------------------------------//
                            that.setDefaultShippingParams(defaultRegionId, shippingStreet, defaultShippingMethodId, countryId);
                        } else if ($('#shipping\\:region_id_default').val().length && $('#shipping\\:region_id_default').val() == this.label) {
                            // ORIGINAL ---------------------------------------------------------//
                            option.attr('selected', 'selected');
                            // ---------------------------------------------------------//
                            // $(option + ' input').attr('checked', 'checked');
                            // NEW ---------------------------------------------------------//
                            that.setDefaultShippingParams(this.value, shippingStreet, defaultShippingMethodId, countryId);
                        } else if ($('#shipping\\:region_id_default').val().length && $('#shipping\\:region_id_default').val() == this.value) {
                            // ORIGINAL ---------------------------------------------------------//
                            option.attr('selected', 'selected');
                            // ---------------------------------------------------------//
                            // $(option + ' input').attr('checked', 'checked');
                            // NEW ---------------------------------------------------------//
                            that.setDefaultShippingParams(this.value, shippingStreet, defaultShippingMethodId, countryId);
                        }
                    }

                    if (defaultRegionId == this.value) {
                        option.attr('selected', 'selected');
                    }

                    $('#'+type+'\\:region_id').append(option);
                });

                if (addressData.regions.length > 1 && $('#'+type+'\\:region_id').val() == "") {
                    if($('#is-shipping-as-billing').is(':checked') == false){
                        $('#'+type+'\\:region_id').addClass('default-selected validate-select');
                    }
                }

            }

        },

        renderShippingMethodsOptions: function(regionId, street, countryId, result) {
            $('.shipping-alert-message').remove();
            $('#estimate_method').children('.radio-row').remove();
            $('#estimate_method').removeClass('disabled');
            // NEW ---------------------------------------------------------//

            if (regionId || countryId == 'PR' || countryId == 'VI') {

                if (result.shippingData.specialNote) {
                    $('#cart-estimate-shipping-method').append($("<div class='shipping-alert-message'>" + result.shippingData.specialNote + "</div>"));
                }

                    var defaultShippingMethodTitle = "Standard";
                    var savedShipMethod = false;
                    var hasDefaultMethod = false;

                    $(result.shippingData.options).each(function () {
                        if (this.selected) {
                            savedShipMethod = true;
                        }
                        if (this.title == defaultShippingMethodTitle) {
                            hasDefaultMethod = true;
                        }
                    });

                    if (savedShipMethod == false && hasDefaultMethod == false) {
                        $(result.shippingData.options)[0].selected = true;
                        savedShipMethod = true;
                        bettercheckout.applyShipping(
                            result.appliedShipping,
                            result.deliveryTime,
                            result.checkoutTotals);
                    }

                    $(result.shippingData.options).each(function () {
                        var label = this.title + ' - ' + this.price;
                        var methodCode = this.code;

                        var option = $("<div class='radio-row clearfix'>" +
                            "<input type='radio' name='estimate_method' class='estimate-method' value='" + this.code + "' /><span class='method-type'>" + label + "</span>" +
                            "</div>");

                        if (savedShipMethod) {
                            if (this.selected) {
                                option.children('input').attr('checked', 'checked');
                                bettercheckout.applyShipping(
                                    result.appliedShipping,
                                    result.deliveryTime,
                                    result.checkoutTotals);
                            }
                        } else {
                            if (hasDefaultMethod) {
                                if (defaultShippingMethodTitle == this.title) {
                                    option.children('input').attr('checked', 'checked');
                                    $('#estimate_method').removeClass('default');
                                    $('#estimate_method').remove('.method-type-default');

                                    bettercheckout.applyShipping(
                                        result.appliedShipping,
                                        result.deliveryTime,
                                        result.checkoutTotals);
                                }
                            }
                        }

                        option.on('change', function () {
                            if ($(this).children('input').is(':checked')) {
                                bettercheckout.shippingMethodSelection(methodCode, label);
                            }
                        });

                        option.children('.method-type').on('click', function () {
                            $(this).parent().find('input.estimate-method').trigger('click');
                        });

                        $('#estimate_method').append(option);
                    });


                if (countryId == 'PR' || countryId == 'VI') {
                    $('#shipping\\:country_id option:selected').each(function () {
                        $(this).removeAttr("selected");
                    });
                    $('#shipping\\:country_id').val(countryId);
                    $('#shipping\\:country_id option[value=' + countryId + ']').attr('selected', 'selected');
                }

            } else {
                // ORIGINAL ---------------------------------------------------------//
                // $('#estimate_method').append("<option value=''>Select State or Province first...</option>");
                // ---------------------------------------------------------//
                // NOW IN CHECKOUTCONTROLLER (272)
                // NEW ---------------------------------------------------------//

                var option;
                $(result.shippingData.options).each(function () {
                    option = $("<div class='radio-row clearfix'>" +
                        "<span class='method-type'>" + this.title + "</span>" +
                        "</div>");
                });

                $('#estimate_method').append(option);
            }
            // Trip the semaphore.
            $('#estimate_method_set').val('set');

            if ($('#estimate_method input:checked').val() == '' || $("#estimate_method input:checked ~ span").text().indexOf('Custom Quote') === -1) {
                $('.shipping-alert-message').html('');
            } else if ($("#estimate_method input:checked ~ span").text().indexOf('Custom Quote') != -1) {
                $specialNote = "A shipping quote for your location is not available online at this time. Please complete your order and our customer service team will contact you with the shipping cost.";
                $('.shipping-alert-message').html($specialNote);
            }

        },

        renderShippingMethods: function (regionId, street, defaultShippingMethodId, countryId) {
            var that = this;
            that.disablePlaceOrderElements();
            $('#estimate_method').children('.radio-row').remove();
            $('#methodStatus').remove();
            $('#estimate_method').append("<div class='radio-row clearfix'>" +
                "<span class='method-type'>Loading...</span>" +
                "</div>")

            // Establish a semaphore.
            $('#estimate_method_set').val("set");
            if(countryId == 'PR') {
                $('#estimate_method_set').val('');
            }
            if(countryId == 'VI') {
                $('#estimate_method_set').val('');
            }

            that.renderCanadianNote($('#shipping\\:country_id').val());

            regionId = $.trim(regionId);
            street = $.trim(street);
            countryId = $.trim(countryId);

            if (regionId.length && countryId.length || countryId == 'VI' || countryId == 'PR') {
                new Ajax.Request('/bettercheckout/checkout/getShippingMethodsComponents', {
                    method: 'post',
                    parameters: {
                        "region_id": regionId,
                        "street": street,
                        "country": countryId
                    },
                    onComplete: function (response) {
                        var result = $.parseJSON(response.responseText);

                        $('.shipping-alert-message').remove();
                        $('#estimate_method').children('.radio-row').remove();
                        $('#estimate_method').removeClass('disabled');

                        if (regionId || countryId == 'PR' || countryId == 'VI') {

                            var defaultShippingMethodTitle = "Standard";
                            var savedShipMethod = false;
                            var hasDefaultMethod = false;

                            if (result.shippingData.specialNote) {
                                $('#cart-estimate-shipping-method').append($("<div class='shipping-alert-message'>" + result.shippingData.specialNote + "</div>"));
                            }

                            $(result.shippingData.options).each(function () {
                                if (this.selected) {
                                    savedShipMethod = true;
                                }
                                if (this.title == defaultShippingMethodTitle) {
                                    hasDefaultMethod = true;
                                }
                            });

                            if (savedShipMethod == false && hasDefaultMethod == false) {
                                $(result.shippingData.options)[0].selected = true;
                                savedShipMethod = true;
                                // bettercheckout.renderDeliveryTime(result.shippingData.deliveryTime);
                                bettercheckout.applyShipping(
                                    result.appliedShipping,
                                    result.deliveryTime,
                                    result.checkoutTotals);
                            }

                            $(result.shippingData.options).each(function () {
                                var label = this.title + ' - ' + this.price;
                                var methodCode = this.code;

                                var option = $("<div class='radio-row clearfix'>" +
                                    "<input type='radio' name='estimate_method' class='estimate-method' value='" + this.code + "' /><span class='method-type'>" + label + "</span>" +
                                    "</div>");

                                if (savedShipMethod) {
                                    if (this.selected) {
                                        option.children('input').attr('checked', 'checked');
                                        bettercheckout.applyShipping(
                                            result.appliedShipping,
                                            result.deliveryTime,
                                            result.checkoutTotals);
                                    }
                                } else {
                                    if (hasDefaultMethod) {
                                        if (defaultShippingMethodTitle == this.title) {

                                            option.children('input').attr('checked', 'checked');

                                            $('#estimate_method').removeClass('default');
                                            $('#estimate_method').remove('.method-type-default');
                                            bettercheckout.applyShipping(
                                                result.appliedShipping,
                                                result.deliveryTime,
                                                result.checkoutTotals);
                                        }
                                    }
                                }

                                option.on('change', function () {
                                    if ($(this).children('input').is(':checked')) {
                                        bettercheckout.shippingMethodSelection(methodCode, label);
                                    }
                                });

                                option.children('.method-type').on('click', function () {
                                    $(this).parent().find('input.estimate-method').trigger('click');
                                });

                                $('#estimate_method').append(option);
                            });

                            if (countryId == 'PR' || countryId == 'VI') {
                                $('#shipping\\:country_id option:selected').each(function () {
                                    $(this).removeAttr("selected");
                                });
                                $('#shipping\\:country_id').val(countryId);
                                $('#shipping\\:country_id option[value=' + countryId + ']').attr('selected', 'selected');
                            }

                        } else {

                            var option;
                            $(result.shippingData.options).each(function () {
                                option = $("<div class='radio-row clearfix'>" +
                                    "<span class='method-type'>" + this.title + "</span>" +
                                    "</div>");
                            });

                            $('#estimate_method').append(option);
                        }
                        // Trip the semaphore.
                        $('#estimate_method_set').val('set');

                        if ($('#estimate_method input:checked').val() == '' || $("#estimate_method input:checked ~ span").text().indexOf('Custom Quote') === -1) {
                            $('.shipping-alert-message').html('');
                        } else if ($("#estimate_method input:checked ~ span").text().indexOf('Custom Quote') != -1) {
                            $specialNote = "A shipping quote for your location is not available online at this time. Please complete your order and our customer service team will contact you with the shipping cost.";
                            $('.shipping-alert-message').html($specialNote);
                        }
                        that.enablePlaceOrderElements();
                    }

                });
            } else {
                var option;
                var optionText;
                if (!regionId.length) {
                    optionText = 'Select state or province first...'
                } else if (!countryId.length) {
                    optionText = 'Select country first...'
                }

                option = $("<div class='radio-row clearfix'>" +
                    "<span class='method-type'>" + optionText + "</span>" +
                    "</div>");

                $('.shipping-alert-message').remove();
                $('#estimate_method').children('.radio-row').remove();
                $('#estimate_method').removeClass('disabled');
                $('#estimate_method').append(option);
                that.enablePlaceOrderElements();
            }
        },

        shippingMethodSelection: function(methodCode, label) {
            var that = this;
            that.disablePlaceOrderElements();
            new Ajax.Request('/bettercheckout/checkout/shippingMethodSelection', {
                method: 'post',
                parameters: {
                    "method_code": methodCode,
                    "method_label": label
                },
                onComplete: function (response) {
                    var result = $.parseJSON(response.responseText);
                    bettercheckout.applyShipping(
                        result.appliedShipping,
                        result.deliveryTime,
                        result.checkoutTotals);

                    that.enablePlaceOrderElements();
                }
            });
        },

        renderDeliveryTime: function(deliveryTime) {
            $('#estimate-delivery-time').html(deliveryTime);
        },

        applyShipping: function(appliedShipping, deliveryTime, checkoutTotals) {
            var that = this;
            if (appliedShipping.status) {
                that.renderTotals(true, checkoutTotals);
                that.renderDeliveryTime(deliveryTime);
                if($('#estimate_method input:checked').val() == '' || $("#estimate_method input:checked ~ span").text().indexOf('Custom Quote') === -1) {
                    $('.shipping-alert-message').html('');
                } else if ($("#estimate_method input:checked ~ span").text().indexOf('Custom Quote') != -1) {
                    $specialNote = "A shipping quote for your location is not available online at this time. Please complete your order and our customer service team will contact you with the shipping cost.";
                    $('.shipping-alert-message').html($specialNote);
                }
            }
        },

        renderTotals: function(flag, checkoutTotals) {
            var that = this;
            if ($('#estimate_method_set').val() != 'set') {
                var checkSet = setInterval(function() {
                    var countryCode = $('#shipping\\:country_id').val();
                    if(countryCode == 'PR' || countryCode == 'VI') {
                        if(flag) {
                            $("#checkout-recap-totals").html(checkoutTotals.totals);
                            that.enablePlaceOrderElements();
                        }
                        clearInterval(checkSet);
                    }
                }, 1000);
            } else {
                $("#checkout-recap-totals").html(checkoutTotals.totals);
            }

            if (checkoutTotals.cartTotal > 0) {
                $('.payment-not-required-box').hide();
                $('.payment-not-required-box input').attr('disabled', 'disabled');
                $('.checkout-cc-info').show();
                $('.checkout-cc-info input, .checkout-cc-info select').removeAttr('disabled');
                $('.gc-success-message-link, .checkout-gc-link').show();
            } else {
                $('.checkout-cc-info').hide();
                $('.checkout-cc-info #cc_method, #cc_type, #cc_number, #cc_expiration, #cc_expiration_yr, #cvv').attr('disabled', 'disabled');
                $('.payment-not-required-box').show();
                $('.payment-not-required-box input').removeAttr('disabled');
                $('.gc-success-message-link, .checkout-gc-link').hide();
            }
        },

        getCheckoutFormData: function(section, checkoutData) {
            section = (typeof section !== 'undefined') ? section : 'shipping';
            if ($(checkoutData).length > 0) {
                var currentSection = checkoutData[section];
                for (formField in currentSection) {
                    if (currentSection[formField] && typeof currentSection[formField] !== "object") {
                        if ($('#' + section + '\\:' + formField).val() === '') {
                            $('#' + section + '\\:' + formField).val(currentSection[formField]);
                        }
                    } else {
                        if (typeof currentSection[formField] === "object") {
                            var elementCount = currentSection[formField].length;
                            var manualAddressFieldsExpanded = false;
                            while (elementCount > 0) {
                                if ($('#' + section + '\\:' + formField + elementCount).val() === '') {
                                    $('#' + section + '\\:' + formField + elementCount).val(currentSection[formField][elementCount-1]);
                                    if (manualAddressFieldsExpanded === false) {
                                        bettercheckout.showShippingAddressFields();
                                        manualAddressFieldsExpanded = true;
                                    }
                                }
                                elementCount--;
                            }
                        }
                    }
                }
            }
        },

        disablePlaceOrderElements: function() {
            $('.bettercheckout select').addClass('disabled');
            $('.bettercheckout select').prop('disabled', true);
            $('.bettercheckout select').prop('disabled', true);

            $('.checkout-submit-button').addClass('disabled');
            $('.checkout-submit-button').find('.static-prefix').html("Calculating<div class='animated-dots'></div>");
            var dots = 0;
            var dotInterval = window.setInterval(function () {
                if (dots < 3) {
                    $('.animated-dots').append('.');
                    dots++;
                } else {
                    $('.animated-dots').html('');
                    dots = 0;
                }
                return false;
            }, 600);
        },

        enablePlaceOrderElements: function() {
            $('.bettercheckout select').removeClass('disabled');
            $('.bettercheckout select').prop('disabled', false);
            $('.bettercheckout select').prop('disabled', false);
            $('.checkout-submit-button').removeClass('disabled');
            $('.checkout-submit-button').find('.static-prefix').html("Place Order");
        },

        submitOrder: function (el) {
            var that = this;
            that.removeCheckoutStepHighlights();
            if(!$('.checkout-submit-button').hasClass('disabled')) {
                /*-- GOOGLE ANALYTICS --------------------------------*/
                _gaq.push(['_trackEvent', 'One Page Checkout', 'Place Order Clicked']);
                /*-- GOOGLE ANALYTICS --------------------------------*/

                Validation.defaultOptions.immediate = true;
                var validator = new Validation('bettercheckout-form');
                var result = validator.validate();

                $.each(validator.form, function () {
                    if (this.advices != undefined) {
                        var fieldname = this.name;
                        $.each(this.advices, function (key, value) {
                            $.each(value, function (key, value) {
                                if (this.innerText != undefined && this.innerText != "") {
                                    var validationAdvice = value.innerText;
                                    /*-- GOOGLE ANALYTICS --------------------------------*/
                                    _gaq.push(['_trackEvent', 'Checkout Error', fieldname, validationAdvice]);
                                    /*-- GOOGLE ANALYTICS --------------------------------*/
                                }
                            })
                        })
                    }
                });


                $('.system-error').remove();

                if (result) {
                    $('.checkout-submit-button').addClass('disabled');
                    $('.checkout-submit-button').removeAttr('onclick').off('click');
                    $('.checkout-submit-button').find('.static-prefix').html("Processing<div class='animated-dots'></div>");

                    var dots = 0;
                    var refreshIntervalId = window.setInterval(function () {
                        if (dots < 3) {
                            $('.animated-dots').append('.');
                            dots++;
                        } else {
                            $('.animated-dots').html('');
                            dots = 0;
                        }
                    }, 600);

                    var data = $('#bettercheckout-form').serialize();
                    new Ajax.Request('/bettercheckout/checkout/placeOrder', {
                        parameters: {
                            "data": data
                        },
                        method: 'post',
                        onComplete: function (response) {
                            var result = $.parseJSON(response.responseText);
                            if (result.success) {
                                window.location.replace(result.redirectUrl);
                            } else {
                                $('.checkout-submit-button').removeClass('disabled');
                                $('.checkout-submit-button').attr('onclick', 'bettercheckout.submitOrder(this)');
                                $('.checkout-submit-button').find('.static-prefix').html("Place Order");
                                clearInterval(refreshIntervalId);
                                /*-- GOOGLE ANALYTICS --------------------------------*/
                                _gaq.push(['_trackEvent', 'Checkout Error', 'red_pulldown', result.message]);
                                /*-- GOOGLE ANALYTICS --------------------------------*/

                                that.handleErrorContext(result.message);

                            }
                        }
                    });
                }
            }

        },

        handleErrorContext: function (errorMessage) {
            var self = this;
            var errorCode = self.getErrorCode(errorMessage);
            if (errorCode !== null) {
                var errorContextHandler = new self.errorContextHandler(self);
                errorContextHandler[errorCode](errorMessage);
            } else {
                self.showErrors(errorMessage);
            }
        },

        errorContextHandler: function (betterCheckout) {
            var self = this;
            self.ERR001 = function (errorMessage) {
                betterCheckout.emphasizeBillingStep();
            };
            self.ERR002 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStep();
                betterCheckout.highlightPaymentElements();
            };
            self.ERR003 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStepPayment();
            };
            self.ERR004 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStepPayment();
            };
            self.ERR005 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
            };
            self.ERR006 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
            };
            self.ERR007 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStepPayment();
            };
            self.ERR008 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStepPayment();
            };
            self.ERR009 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
            };
            self.ERR010 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
            };
            self.ERR011 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStepPayment();
            };
            self.ERR012 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStepPayment();
            };
            self.ERR013 = function (errorMessage) {
                betterCheckout.emphasizeBillingStep();
            };
            self.ERR014 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
            };
            self.ERR015 = function (errorMessage) {
                betterCheckout.showErrors(errorMessage);
                betterCheckout.emphasizeBillingStepPayment();
            };
        },

        getErrorCode: function (errorMessage) {
            var errorCode = null,
                errorCodeIndex = {
                ERR001: 'Please review your billing information.',
                ERR002: 'Please review your billing information before submitting again.',
                ERR003: 'There was a problem with your credit card. It was declined by the processor which could be due to a number of things. Some possible causes include insufficient funds, or perhaps the billing address you entered is not the address associated with this credit card. You may want to double check your information, try another card or call us at 800-347-0288 to complete your order.',
                ERR004: 'Please enter a valid credit card verification number.',
                ERR005: 'Invalid shipping method.',
                ERR006: 'There was an error processing your order. Please try again later or contact us at 800.347.0288 from 9am to 5:30pm EST, Monday through Friday.',
                ERR007: 'Credit card type is not allowed for this payment method.',
                ERR008: 'Incorrect credit card expiration date.',
                ERR009: 'Invalid Shipping Method',
                ERR010: 'Please specify a shipping method.',
                ERR011: 'Credit card number mismatch with credit card type.',
                ERR012: 'The credit card number is invalid.',
                ERR013: 'Gateway error code I00001: Please review your billing information.',
                ERR014: '"Street Address" is a required value., "Street Address" length must be equal or greater than 1 characters., "City" is a required value., "City" length must be equal or greater than 1 characters., "Zip/Postal Code" is a required value.',
                ERR015: 'Credit card expiration date is invalid.'
            };
            $.each(errorCodeIndex, function (code, messageToCode) {
                if( messageToCode === errorMessage) {
                    errorCode = code;
                    return false;
                }
            });
            return errorCode;
        },

        emphasizeBillingStep: function () {
            var self = this,
                notificationMessage = 'Something doesn&#39;t match. Please review billing information before submitting again.';
            self.inlineBillingNotification(notificationMessage);
            self.expandBillingInfo();
            self.highlightBillingElements();
            self.scrollToBillingInformation();
        },

        emphasizeBillingStepPayment: function () {
            var self = this,
                notificationMessage = 'Something doesn&#39;t match. Please review billing information before submitting again.';
            self.inlineBillingNotification(notificationMessage);
            self.highlightPaymentElements();
            self.clearPaymentFromAutoFill();
            self.scrollToBillingInformation();
        },

        scrollToBillingInformation: function () {
            window.scrollTo(0, 0);
            var billingInformationHeadingOffset = $("#billing-information-heading").offset().top - 90;
            $('html, body').animate({
                scrollTop: billingInformationHeadingOffset
            }, 1000);
        },

        inlineBillingNotification: function (message) {
            var notificationContainer = $('#inline-billing-notification');
            notificationContainer.find('.message').html(message);
            notificationContainer.show();
        },

        highlightBillingElements: function () {
            var self = this,
                fieldSet = {
                    container: $('.checkout-billing-info'),
                    fields: [
                        '#billing\\:street1',
                        '#billing\\:region_id',
                        '#billing\\:country_id',
                        '#billing\\:city',
                        '#billing\\:postcode'
                    ]
                };
            self.highlightFieldSet(fieldSet);
        },

        highlightPaymentElements: function () {
            var self = this,
                fieldSet = {
                container: $('.checkout-cc-info'),
                fields: [
                    '#cc_number',
                    '#cc_expiration',
                    '#cc_expiration_yr',
                    '#cvv'
                ]
            };
            self.highlightFieldSet(fieldSet);
        },

        highlightFieldSet: function (fieldSet) {
            $.each(fieldSet.fields, function (key, fieldSelector) {
                fieldSet.container.find(fieldSelector).addClass('highlight-field');
            });
        },

        removeCheckoutStepHighlights: function () {
            $('.checkout-step').find('*').removeClass('highlight-field');
        },

        expandBillingInfo: function () {
            var self = this;
            $('#is-shipping-as-billing-container').hide();
            var $isSameShippingBillingCheckbox = $('.grid_1.alpha.checkbox').find('input[name="is-shipping-as-billing"]'),
                shippingFieldKeys = [
                    'firstname',
                    'lastname',
                    'company',
                    'street1',
                    'street2',
                    'region_id',
                    'country_id',
                    'city',
                    'postcode'
                ];
            if($isSameShippingBillingCheckbox.prop('checked') === true) {
                self.copyShippingAddressToBilling(shippingFieldKeys);
                $isSameShippingBillingCheckbox.trigger('click');
            } else {
                self.clearBillingFromAutoFill(shippingFieldKeys);
            }
        },

        clearBillingFromAutoFill: function (fieldKeys) {
            var $checkoutBillingInfo = $('.checkout-billing-info');
            $.each(fieldKeys, function(index, fieldKey) {
                var billingInputId = '#billing\\:' + fieldKey,
                    billingFieldValue = $checkoutBillingInfo.find(billingInputId).val();
                $checkoutBillingInfo.find(billingInputId).attr('autocomplete', 'new-password');
                $checkoutBillingInfo.find(billingInputId).val('');
                $checkoutBillingInfo.find(billingInputId).val(billingFieldValue);
            });
        },

        copyShippingAddressToBilling: function (fieldKeys) {
            var $checkoutShippingInfo = $('.checkout-shipping-info'),
                $checkoutBillingInfo = $('.checkout-billing-info');
            $.each(fieldKeys, function(index, fieldKey) {
                var shippingInputId = '#shipping\\:' + fieldKey,
                    billingInputId = '#billing\\:' + fieldKey,
                    billingFieldValue = $checkoutShippingInfo.find(shippingInputId).val();
                $checkoutBillingInfo.find(billingInputId).attr('autocomplete', 'new-password');
                $checkoutBillingInfo.find(billingInputId).val('');
                $checkoutBillingInfo.find(billingInputId).val(billingFieldValue);
            });
        },

        clearPaymentFromAutoFill: function () {
            var $checkoutCcInfo = $('.checkout-cc-info'),
                ccInfoFields = [
                    '#cc_number',
                    '#cc_expiration',
                    '#cc_expiration_yr',
                    '#cvv'
                ];
            $.each(ccInfoFields, function(index, fieldName) {
                var $paymentFieldElement = $checkoutCcInfo.find(fieldName),
                    paymentFieldValue = $paymentFieldElement.val();
                $paymentFieldElement.attr('autocomplete', 'new-password');
                $paymentFieldElement.val('');
                $paymentFieldElement.val(paymentFieldValue);
            });
        },

        validateSelect: function (el) {
            if ($(el).val() == "") {
                $(el).addClass('default');
            } else {
                $(el).removeClass('default');
            }

            if ($(el).hasClass('validation-failed') || $(el).hasClass('validation-passed')) {
                Validation.validate($(el).attr("id"));
            }
        },

        prepareBillingFields: function (el) {
            if (el == true || $(el).is(':checked') == false) {
                if($('#billing\\:country_id').val() == "PR" || $('#billing\\:country_id').val() == "VI"){
                    $('#billing\\:region_id').removeClass('conditional-required');
                } else {
                    $('#billing\\:region_id').removeClass('required-entry validation-passed');
                    $('#billing\\:region_id').addClass('conditional-required');
                }

                $('#billing\\:region_id').prop('required', true);

                $('.checkout-billing-info input.conditional-required').each(function () {
                    $(this).addClass('required-entry');
                    $(this).removeClass('validation-passed');

                });
                $('.checkout-billing-info select.conditional-required').each(function () {
                    $(this).addClass('validate-select');
                });

            } else {
                if($('#shipping\\:country_id').val() == "PR" || $('#shipping\\:country_id').val() == "VI"){
                    $('#billing\\:region_id').removeClass('validate-select conditional-required');
                } else {
                    $('#billing\\:region_id').addClass('conditional-required');
                }

                $('#billing\\:region_id').removeProp('required');

                $('.checkout-billing-info .conditional-required').each(function () {
                    $(this).removeClass('required-entry validate-select validation-failed validation-passed');
                });
                $('.checkout-billing-info .validation-advice').remove();
            }
        },

        applyGiftcard: function () {
            var that = this;

            $('#estimate_method_set').val("set");
            if ($("#giftcard").val()) {
                new Ajax.Request('/bettercheckout/checkout/applyGiftCard', {
                    method: 'post',
                    parameters: {"giftcard_code": $("#giftcard").val()},
                    onComplete: function (response) {
                        var result = $.parseJSON(response.responseText);
                        if (result.status) {
                            $("#checkout-gc-success-message-container .gc-success-message").html(result.message);
                            $('#checkout-gc-fields').hide();
                            $("#checkout-giftcard-message").hide();
                            $('#checkout-gc-success-message-container').show();
                            $("#checkout-giftcard-message").removeClass('error').addClass("success");

                            that.renderCheckoutTotals(true);

                        } else {
                            $("#checkout-giftcard-message").show();
                            $("#checkout-giftcard-message").html(result.message);
                            $("#checkout-giftcard-message").removeClass('success').addClass("error");
                        }
                    }
                });
            }
        },

        removeGiftcard: function (cardCode) {
            var that = this;
            $('#estimate_method_set').val("set");
            new Ajax.Request('/bettercheckout/checkout/removeGiftCard', {
                method: 'post',
                parameters: {'giftcard_code': cardCode},
                onComplete: function () {
                    $('#checkout-gc-success-message-container, #checkout-giftcard-message').hide();
                    that.renderCheckoutTotals(true);
                }
            });
        },

        checkGiftCardBalance: function () {
            if ($("#giftcard").val()) {
                new Ajax.Request('/bettercheckout/checkout/checkGiftCardBalance', {
                    method: 'post',
                    parameters: {"giftcard_code": $("#giftcard").val()},
                    onComplete: function (response) {
                        var result = $.parseJSON(response.responseText);
                        $("#checkout-gc-balance").show();
                        $("#checkout-gc-balance").html(result.balance);
                    }
                });
            }
        },

        resetGiftCardForm: function () {
            $("#checkout-gc-check-button").show();
            $('#giftcard').val('');
            $("#checkout-gc-balance").hide();
            $('#checkout-gc-fields').show();
            $('#checkout-gc-success-message-container').hide();
        },

        getCcType: function (el) {
            var self = this,
                ccNumber = $(el).val(),
                ccNumberCheck = self.getCcTypeByNumber(ccNumber),
                result = ccNumberCheck.result,
                ccType = ccNumberCheck.ccType;

            $('#cc_number').removeClass("visa amex mastercard discover");
            if (result !== false) {
                $('#cc_number').addClass(result);
                $('#cc_type').val(ccType);
            }

            return result;
        },

        getCcTypeByNumber: function (ccNumber) {
            var result = false;
            var ccType = '';

            //check for MasterCard
            if (/^(5[1-5]|2[234567])/.test(ccNumber))
            {
                result = "mastercard";
                ccType = "MC";
            }

            //check for Visa
            else if (/^4/.test(ccNumber))
            {
                result = "visa";
                ccType = "VI";
            }

            //check for AmEx
            else if (/^3[47]/.test(ccNumber))
            {
                result = "amex";
                ccType = "AE";

            }

            //check for Discover
            else if (/^6/.test(ccNumber))
            {
                result = "discover";
                ccType = "DI";

            }

            return {
                result: result,
                ccType: ccType
            };

        },

        prefillAddress: function (type) {
            var that = this;
            var elId = type + '-address-select';

            if ($('#'+elId).val() == '') {
                $('.checkout-' + type + '-info input').not('[disabled]').not('#save-'+type).val('');
                $('#' + type + '\\:telephone').prev().html('');
                $('#' + type + '\\:region_id').val('').trigger('change');
                $('#' + type + '\\:country_id').val('US').trigger('change');
                $('#' + type + '-save-for-future').show();
            } else {
                $('#autocomplete').val('');

                $('#' + type + '-save-for-future').hide();

                $.each(this.addressBook, function (addressId) {
                    if ($('#'+elId).val() == addressId) {
                        var regionId = that.addressBook[addressId].region_id;

                        $.each(this, function (field, value) {

                            var el  = '#'+type+'\\:'+field;
                            if ($(el).length && $(el).is('input') && $(el).attr('disabled') != 'disabled') {
                                $(el).val(value);
                            }
                            if ($(el).length && $(el).is('select') && $(el).attr('disabled') != 'disabled') {

                                if ($(el).attr('id') == type + ':country_id') {

                                    $('#' + type + '\\:region_id_default').val(regionId);
                                    $('#' + type + '\\:country_id').val(value).trigger('change');
                                }
                            }
                        });
                    }
                });
            }
        },

        setAddressBook: function (addressOptionsJson) {
            this.addressBook = addressOptionsJson;
        },

        setPaymentBook: function (cardsOptionsJson) {
            this.paymentBook = cardsOptionsJson;
        },

        prefillPayment: function () {
            if ($('#payment-select').length) {
                if ($('#payment-select').val() == '') {
                    $('#payment-save-for-future').show();
                    $('.checkout-new-cc-fields').show();
                    $('.checkout-new-cc-fields input, .checkout-new-cc-fields select').removeAttr("disabled");
                    $('.checkout-new-cc-fields input.conditional-required').each(function () {
                        $(this).addClass('required-entry');
                    });
                    $('.checkout-new-cc-fields select.conditional-required').each(function () {
                        $(this).addClass('validate-select');
                    });
                    $('#cc_expiration').addClass('validate-cc-exp');
                    $('#cvv').addClass('validate-number');
                    $('#cc_number').addClass('validate-cc-number');
                    $('.checkout-cc-info input').not('#cc_method').not('#save-payment').val('');
                    $('.checkout-cc-info select').not('#payment-select').val('');
                    $('#cc_number').removeClass("visa amex mastercard discover");
                } else {
                    $('#payment-save-for-future').hide();
                    $('.checkout-new-cc-fields').hide();
                    $('.checkout-new-cc-fields input, .checkout-new-cc-fields select').not("#cc_method").attr("disabled", "disabled");
                    $('.checkout-new-cc-fields .conditional-required').each(function () {
                        $(this).removeClass('validate-cc-number validate-number validate-cc-exp required-entry validation-failed validation-passed');
                    });
                    $('.checkout-new-cc-fields .validation-advice').remove();
                }
            } else {
                $('#payment-save-for-future').show();
            }
        },

        signIn: function () {
            var w = (this.isMobile) ? '100%' : '300px';

            jQuery.fancybox({
                'width': w,
                'height': 'auto',
                'href': '#popup-sign-in',
                'modal': false,
                'autoSize': false,
                'afterLoad': function(){
                    jQuery('.fancybox-skin').css({'top':'30px', 'bottom':'auto'});
                }
            });
        },

        doSignIn: function () {
            var that = this;
            Validation.defaultOptions.immediate = true;
            var validator = new Validation('bettercheckout-signin-form');
            var result = validator.validate();
            if (result) {
                $('#checkout-sign-in-action-button').addClass('disabled');
                new Ajax.Request('/bettercheckout/checkout/signIn', {
                    method: 'post',
                    parameters: {"username": $("#username").val(), "password": $("#password").val()},
                    onComplete: function (response) {
                        var result = $.parseJSON(response.responseText);
                        if (result.status) {
                            location.reload();
                        } else {
                            that.showErrors(result.message);
                            $('#checkout-sign-in-action-button').removeClass('disabled');
                        }
                    }
                });
            }
        },

        doSignOut: function () {
            new Ajax.Request('/bettercheckout/checkout/signOut', {
                method: 'post',
                onComplete: function () {
                    location.reload();
                }
            });
        },

        getSavedFields: function (section) {
            section = (typeof section !== "undefined") ? section : "shipping";
            new Ajax.Request('/bettercheckout/checkout/getCheckoutData', {
                method: 'get',
                onComplete: function (data) {
                    if (data) {
                        var response = data.responseJSON;
                        var currentSection = response[section];
                        for (formField in currentSection) {
                            if (currentSection[formField] && typeof currentSection[formField] !== "object") {
                                if ($('#' + section + '\\:' + formField).val() === '') {
                                    $('#' + section + '\\:' + formField).val(currentSection[formField]);
                                }
                            } else {
                                if (typeof currentSection[formField] === "object") {
                                    var elementCount = currentSection[formField].length;
                                    var manualAddressFieldsExpanded = false;
                                    while (elementCount > 0) {
                                        if ($('#' + section + '\\:' + formField + elementCount).val() === '') {
                                            $('#' + section + '\\:' + formField + elementCount).val(currentSection[formField][elementCount-1]);
                                            if (manualAddressFieldsExpanded === false) {
                                                bettercheckout.showShippingAddressFields();
                                                manualAddressFieldsExpanded = true;
                                            }
                                        }
                                        elementCount--;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        },

        setSavedFields: function () {
            var that = this;
            // Do not save any payment information in the session (identified by being contained in a .cc-field element)
            var sanitizedFormParams = $("#bettercheckout-form").find("input, select").filter(function(){
                return $(this).parents(".cc-field").length !== 1;
            }).serialize();
            new Ajax.Request('/bettercheckout/checkout/setCheckoutData', {
                method: 'post',
                parameters: sanitizedFormParams
            });
        },

        createAccount: function () {
            var that = this;
            Validation.defaultOptions.immediate = true;
            var validator = new Validation('bettercheckout-create-account');
            var result = validator.validate();
            if (result) {
                $('#create-account-button').addClass('disabled');
                $('#create-account-button').prop('onclick', null).off('click');


                new Ajax.Request('/bettercheckout/account/createAfterCheckout', {
                    method: 'post',
                    parameters: {
                        "firstname": $('#register_firstname').val(),
                        "lastname": $('#register_lastname').val(),
                        "email": $('#register_email').val(),
                        "is_subscribed": $('#register_is_subscribed').val(),
                        "form_key": $('#register_form_key').val(),
                        "increment_id": $('#register_increment_id').val(),
                        "password": $('#password').val(),
                        "confirmation": $('#confirmation').val()
                    },

                    onComplete: function (response) {
                        var result = $.parseJSON(response.responseText);
                        if (result.status) {
                            redirectUrl = result.redirectUrl;
                            var timer = setTimeout(function(){ window.location.replace(redirectUrl)},5000);
                            $('#my-account-button').on('click', function(){
                                clearTimeout(timer);
                                window.location.replace(redirectUrl)
                            });
                            $('#before-register').hide();
                            $('#after-register').show();
                        } else {
                            that.showErrors(result.message);
                            $('#create-account-button').removeClass('disabled');
                            $('#create-account-button').prop('onclick', 'bettercheckout.createAccount()');
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                            _gaq.push(['_trackEvent', 'Login Error', 'Checkout success page', result.message]);
                            /*-- GOOGLE ANALYTICS --------------------------------*/
                        }
                    }
                });
            }
        },

        showErrors: function (errors) {
            if ($('.system-error').length) {
                $('.system-error').remove();
            }
            var el = $("<div class='system-error'><div>" + errors + "</div></div>");
            el.on('click touchstart', function (e) {
                el.slideUp();
            });
            $('body').prepend(el);

            /*-- HOTJAR TAG --------------------------------*/
            hj('tagRecording', ['Checkout - error']);
            /*-- HOTJAR TAG --------------------------------*/

            el.slideDown();
        },

        doForgotPassword: function () {
            window.location.replace('/customer/account/forgotpassword/');
        },

        formatPostcode: function (country, type) {
            var el = '#'+type+'\\:postcode';
            $(el).removeClass('validate-zip validate-zip-canada');

            if (country == 'CA') {
                $(el).attr('maxlength','7');
                $(el).prop('type', 'text');
                $(el).removeAttr('onkeypress');
                $(el).removeAttr('pattern');
                $(el).addClass('validate-zip-canada');
            } else {
                $(el).attr('maxlength','5');
                $(el).prop('type', 'tel');
                $(el).addClass('validate-zip');
            }
        },

        formatNumeric: function (string) {
            return string.replace(/\D/g,'');
        },

        stickRecapBox: function () {
            var el = this.isMobile ? $('#checkout-recap-cart-mobile') : $('#checkout-recap-cart');
            if ($(el).length && !this.isMobile) {
                var displayedCartItems = 3;
                var itemsInCart = $(el).find('.shopcart-item-container').length;

                if (itemsInCart > displayedCartItems) {

                    // FIND THE COMBINED HEIGHT OF THE CART ITEMS THAT ARE INITIALLY DISPLAYED
                    var cartItems = $(el).find('.shopcart-item-container');
                    var cartItemsCollection = $(el).find('.shopcart-items-collection');
                    var visItemHeight = 0;
                    for (var i=0; i < displayedCartItems; i++) {
                        visItemHeight = visItemHeight + cartItems[i].clientHeight;
                    }
                    cartItemsCollection.height(visItemHeight);
                }
            }
        },

        showShippingAddressFields: function () {
            $('#autocomplete').val('');
            $('.shipping-address-fields').show();
        },

        initCheckoutUI: function () {
            var self = this;
            self.initCvvTooltip();
            self.telephoneMaskHandler();
            self.resetCCFields(); // empty out CC fields on page load/reload
            self.cvvNumericHandler();
        },
        initCvvTooltip : function () {
            var self = this;
            self.cardTooltipHandler();
            self.bindCvvTooltip();
            self.setCvvTooltipImage();
        },
        telephoneMaskHandler: function () {
            var self = this,
                $telephoneInput = $('#shipping\\:telephone');
            $telephoneInput.mask(self.inputMasks.telephone.default, { autoclear: false });
            $telephoneInput.on('keydown', function (e) {
                var telephoneNumberVal = self.unmaskNumber($telephoneInput.val());
                if (telephoneNumberVal === '' && e.key === '1') {
                    e.preventDefault();
                }
            });
            $telephoneInput.on('paste', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var pasteData = e.originalEvent.clipboardData.getData('text'),
                    sanitizedNumber = self.sanitizePhoneNumber(self.unmaskNumber(pasteData));
                $telephoneInput.val(sanitizedNumber);
            });
        },

        resetCCFields: function() {
            $('#cc_number').val('');
            $('#cc_type').val('');
            $('#cc_expiration').val('');
            $('#cc_expiration_yr').val('');
            $('#cvv').val('');
        },

        creditCardMaskHandler: function () {
            var self = this,
                $ccNumberInput = $('#cc_number'),
                $cvvInput = $('#cvv');
            $ccNumberInput.on('keypress', function (e) {
                var ccNumberValue = self.unmaskNumber($ccNumberInput.val());
                if (ccNumberValue === '' && ! self.validFirstCcNumber(e.key)) {
                    e.preventDefault();
                }
            });
            $ccNumberInput.on('keyup', function (e) {
                var ccType = self.getCcType($(this)),
                    ccNumberMask,
                    cvvMask,
                    ccNumberValue = $ccNumberInput.val(),
                    cNumberUnMasked = ccNumberValue.match(/\d+/g) === null ? '' : ccNumberValue.match(/\d+/g).join('');

                if (cNumberUnMasked === '') {
                    $ccNumberInput.unmask();
                    $ccNumberInput.val('');
                    self.inputMasks.currentMask = false;
                } else if (ccType !== false && self.inputMasks.currentMask !== ccType) {
                    ccNumberMask = self.inputMasks.creditCard[ccType].number;
                    cvvMask = self.inputMasks.creditCard[ccType].cvv;
                    $ccNumberInput.mask(ccNumberMask, { autoclear: false });
                    $cvvInput.mask(cvvMask, { autoclear: false });
                    self.setCaretPosition($ccNumberInput[0], cNumberUnMasked.length);
                    self.inputMasks.currentMask = ccType;
                }
            });
        },

        cvvNumericHandler: function () {
            var self = this,
                $cvvInput = $('#cvv');

            $cvvInput.on('keyup', function (e) {
                $cvvInput.val(self.formatNumeric($cvvInput.val()));
            });
        },

        validFirstCcNumber: function(char) {
            var validFirstKeyCodes = [ '2', '3', '4', '5', '6' ];
            return validFirstKeyCodes.indexOf(char) > -1;
        },

        unmaskNumber: function (maskedNumber) {
            return (!/\d/.test(maskedNumber)) ? '' : maskedNumber.match(/\d+/g).join('');
        },

        sanitizePhoneNumber: function (phoneNumber) {
            if(phoneNumber.length > 0) {
                var phoneNumberArray = phoneNumber.split('');
                $.each(phoneNumberArray, function (index, value) {
                    if (parseInt(phoneNumberArray[0]) === 1) {
                        phoneNumberArray.shift();
                        phoneNumber = phoneNumberArray.join('');
                    } else {
                        return false;
                    }
                });
            }
            return phoneNumber;
        },

        cardTooltipHandler: function () {
            var self = this,
                $cardNumberField = $('#cc_number');
            $cardNumberField.on('keyup', function (e) {
                self.setCvvTooltipImage();
            });
        },

        setCaretPosition: function(elem, caretPos) {
            var range;
            if (elem.createTextRange) {
                range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            } else {
                elem.focus();
                if (elem.selectionStart !== undefined) {
                    elem.setSelectionRange(caretPos, caretPos);
                }
            }
        },

        setCvvTooltipImage: function () {
            var self = this,
                $cardNumberField = $('#cc_number'),
                ccType = self.getCcType($cardNumberField),
                $cvvTooltipContent = $(self.cvvTooltip.selectors.content),
                helperImage = 'security-code-default.png';
            if(ccType !== false) {
                helperImage = self.cvvTooltip.helpImages[ccType];
            }
            $cvvTooltipContent.css('background', 'url(' + self.baseSkinImagePath + 'icons/bettercheckout/' + helperImage + ') no-repeat');
            $cvvTooltipContent.css('background-size', 'cover');
        },

        bindCvvTooltip: function () {
            var self = this;
            var $cvvTooltipIcon = $(self.cvvTooltip.selectors.icon),
                $cvvTooltipContent = $(self.cvvTooltip.selectors.content);
            $cvvTooltipIcon.on('mouseover', function (e) {
                $cvvTooltipContent.fadeIn();
            });
            $cvvTooltipIcon.on('mouseout', function (e) {
                $cvvTooltipContent.fadeOut();
            });
            $cvvTooltipIcon.on('touchstart', function (e) {
                if($cvvTooltipContent.is(':hidden')) {
                    $cvvTooltipContent.fadeIn();
                } else {
                    $cvvTooltipContent.fadeOut();
                }
            });
            $cvvTooltipContent.on('touchstart', function() {
                $cvvTooltipContent.fadeOut();
            });
        }
    };
})(jQuery);

var bettercheckout = new jQuery.Bettercheckout();

jQuery(document).ready(function () {
    var $ = jQuery;
    Validation.defaultOptions.immediate = true;
    var validator = new Validation('bettercheckout-form');

    $('#shipping\\:firstname').focus();

    $.each($('.checkout-notice'), function(){
        $(this).on('click', function(){
            $(this).parent().find('input[type=checkbox]').trigger('click');
        });
    });

    bettercheckout.stickRecapBox();
    // Load any existing customer data
    // TODO: Use promises to chain getSavedFields() calls
    // bettercheckout.getSavedFields('shipping');
	// bettercheckout.getSavedFields('billing');
    $(window).resize(function () {
        if ($(window).width() <= 768) {
            $('.checkout-recap').removeClass('topStill');
            $('.checkout-recap').removeClass('bottomStill');
        }
    });
    bettercheckout.getSavedFields('shipping');
	bettercheckout.getSavedFields('billing');

    $('#bettercheckout-form input').blur(function(){
        bettercheckout.setSavedFields();
    });

    $('.checkout-footer-container').show();

    $('.checkout-gc-link').click(function () {
        $('.checkout-gc-fields').toggle();
    });

    $('.checkout-customer-note-link').click(function () {
        $('.checkout-customer-note-fields').toggle();
    });

    $('#is-shipping-as-billing').click(function () {
        $('.checkout-billing-info').toggleClass("checkout-billing-info-open");
    });

    $('.checkout-recap-totals-mobile-link').click(function () {
        if ($('.checkout-recap-totals-mobile').is(':visible')) {
            $('.checkout-recap-totals-mobile').hide();
            $('.edit-cart-link').hide();
            $('.checkout-recap-totals-mobile-link span').text("View Cart Items");
        } else {
            $('.checkout-recap-totals-mobile').show();
            $('.edit-cart-link').show();
            $('.checkout-recap-totals-mobile-link span').text("Hide Cart Items");
        }
    });

    $('#estimate_method').addClass('default');
    $('#cc_expiration').addClass('default');
    $('#cc_expiration_yr').addClass('default');

    bettercheckout.showShippingAddressFields();

    if (
        (
        $('#shipping\\:region_id').length && $('#shipping\\:street1').length && $('#shipping\\:country_id').length) ||
        $('#shipping\\:country_id').val() == 'PR' ||
        $('#shipping\\:country_id').val() == 'VI'
    ) {
        setTimeout(function() {
            bettercheckout.renderShippingMethods.call(
                bettercheckout,
                $('#shipping\\:region_id').val(),
                $('#shipping\\:street1').val(),
                null,
                $('#shipping\\:country_id').val()
            );
        }, 3000);
    }

    $(document).trigger("enhance");

    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBsvTeU94wHZEYnKc1gRq6UpklcbPJbqpw&libraries=places&callback=initAutocomplete", function () {});
});


/*** Google Map API Misc functions ***/

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
var placeSearch, autocomplete;

var mappedFields = {
    'street_number': 'shipping:street1',
    'route': 'shipping:street2',
    'locality': 'shipping:city',
    'administrative_area_level_2': 'shipping:region_id',
    'administrative_area_level_1': 'shipping:region_id',
    'country': 'shipping:country_id',
    'postal_code': 'shipping:postcode'
};

var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    if (jQuery('#autocomplete').length) {

        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {
                types: ['geocode']
            });

        jQuery('#autocomplete').attr('placeholder', '');

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }
}

function fillInAddress() {
    bettercheckout.showShippingAddressFields();

    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        document.getElementById(mappedFields[component]).value = '';
        document.getElementById(mappedFields[component]).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    if (typeof place.address_components != "undefined") {
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];

            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                if (addressType == 'route') {
                    document.getElementById(mappedFields['street_number']).value = document.getElementById(mappedFields['street_number']).value + ' ' + val;
                } else {
                    document.getElementById(mappedFields[addressType]).value = val;
                }
                var elId = '#' + mappedFields[addressType].replace(":", "\\:");

                if (addressType == 'country') {
                    jQuery(elId).val(jQuery(elId + ' option:contains('+val+')').val()).trigger('change');
                }

                if (addressType == 'administrative_area_level_1') {
                    jQuery('#shipping\\:region_id_default').val(val);
                }
            }
        }
    }
    jQuery('shipping\\:street1').trigger('change');
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}
