
var hcPromo = {};
function openPopup(popupId) {
    jQuery.fancybox({
        'padding': [40, 20, 40, 20],
        'href': '#' + popupId
    });
}
jQuery(document).ready(function($) {
    hcPromo.hcPromos = [];

    if ($('.hcp-overlay').length) {
        $('.hcp-overlay').on('click', function(e){
            e.stopPropagation;
            $(this).stop(true).clearQueue();
            $(this).fadeOut(400);
        });
    }

    hcPromo.getTriggerEls = function(string) {
        var $triggerEls = $('[class^="' + string + '"], [class*=" ' + string + '"], [id^="' + string + '"]');
        return $triggerEls;
    };

    hcPromo.getTriggerId = function(triggerPrefix, trigger) {
        var triggerId = "";

        if (typeof trigger.id !== 'undefined') {
            if (trigger.id.indexOf(triggerPrefix) > -1) {
                triggerId = trigger.id;
            }
        }

        if (typeof trigger.className !== 'undefined') {
            var classes = trigger.className.split(/\s+/);

            $.each(classes, function (i) {
                if (classes[i].indexOf(triggerPrefix) > -1) {
                    triggerId = classes[i];
                    return false;
                }
            });
        }

        return triggerId;
    };

    hcPromo.applyOfferStyle = function () {
        var associatedEls = $('.hcp-element');
        $.each(hcPromo.hcPromos, function(i, offer) {
            if (offer.name != 'ofrManualEntry') {
                if (offer.isApplied) {
                    if (!(associatedEls.hasClass(offer.name + '-selected'))) {
                        associatedEls.addClass(offer.name + '-selected');
                    }
                    // SHOW A-SPOT OVERLAY ON HOMEPAGE
                    if ($('.cms-index-index').length) {
                        if ($('.hcp-overlay').length) {
                            $('.hcp-overlay').fadeIn(400, function () {
                                $(this).delay(3000).queue(function () {
                                    $(this).clearQueue().fadeOut(400);
                                });
                            });
                        }
                    }
                } else {
                    if (associatedEls.hasClass(offer.name + '-selected')) {
                        associatedEls.removeClass(offer.name + '-selected');
                    }
                    if ($('.hcp-toggle').length) {
                        $('.hcp-toggle').off().on('click', function () {
                            offer.apply();
                        });
                    }
                }
            }
        });
    };

    hcPromo.applyOffer = function(offerObj) {
        if (offerObj.isApplied) {
            hcPromo.applyOfferStyle();
        } else {
            var oLocation = window.location.href;

            if (oLocation.indexOf('checkout/cart') >= 0) {
                if ($('.bettercheckout .totals .ajax_loading').length) {
                    $('.bettercheckout .totals .ajax_loading').fadeIn();
                }
            }

            new Ajax.Request('/bettercheckout/applypromo/', {
                method: 'post',
                parameters: {"promocode": offerObj.code},
                onComplete: function (response) {
                    if (response.responseJSON.status) {
                        var promoNameAndCode = offerObj.name + '_' + offerObj.code;

                        if (response.responseJSON.expiration_date) {
                            var cookieExpiration = new Date(response.responseJSON.expiration_date);
                        }
                        setCookie('hcPromo', promoNameAndCode, cookieExpiration);

                        hcPromo.hcPromos.forEach(function (offer) {
                            offer.isApplied = (offer.code == offerObj.code);
                        });

                        // SHOW OFFER POPUP
                        if (promoNameAndCode !== 'undefined' && promoNameAndCode !== '' && $('#' + promoNameAndCode).length) {
                            openPopup(promoNameAndCode);
                        }

                        hcPromo.applyOfferStyle();
                    } else {
                        if ($('.bettercheckout .totals .ajax_loading').length) {
                            $('.bettercheckout .totals .ajax_loading').fadeOut();
                        }
                        $(".promocode-response-message").hide().removeClass('success').addClass("error").html(response.responseJSON.message).fadeIn();
                        return;
                    }

                    setTimeout(function () {
                        if (oLocation.indexOf('checkout/cart') >= 0) {
                            location.reload();
                        } else {
                            $.fancybox.close(true)
                        }
                    }, 3000);
                }
            });
        }
    };

    hcPromo.collectOffer = function(offerObj) {
        if (hcPromo.hcPromos.length) {
            var offerHasBeenCollected = false;
            $.each(hcPromo.hcPromos, function(i, offer) {
                offerHasBeenCollected = (offer.name == offerObj.name && offer.code == offer.code);
                if (offerHasBeenCollected) {
                    return false;
                }
            });

            if (!offerHasBeenCollected) {
                hcPromo.hcPromos.push(offerObj)
            }
        } else {
            hcPromo.hcPromos.push(offerObj);
        }
    };

    hcPromo.Offer = function(name, code, isapplied) {
        this.name = name;
        this.code = code;
        this.isApplied = isapplied;
        this.apply = function(){
            hcPromo.applyOffer(this);
        };
    };

    hcPromo.initHCPromos = function() {
        var checkOfferCookie = getCookie("hcPromo");
        var triggerEls = hcPromo.getTriggerEls('hcp_');
        var urlHasTriggerParam = new RegExp("[?&]hcp_").test(location.search);
        var activeOffer = null;

        if (checkOfferCookie) {
            var cookieName = checkOfferCookie.split('_')[0];
            var cookieCode = checkOfferCookie.split('_').slice(1).join('_');
            var cookieOffer = new hcPromo.Offer(cookieName, cookieCode, true);

            hcPromo.collectOffer(cookieOffer);

            activeOffer = cookieOffer;
        }

        if (urlHasTriggerParam) {
            var regex = new RegExp("hcp_[^&?%\\s]+");
            var param = location.href.match(regex)[0];
            var paramName = param.split('_')[1];
            var paramCode = param.split('_').slice(2).join('_');
            var paramOffer = new hcPromo.Offer(paramName, paramCode);

            hcPromo.collectOffer(paramOffer);

            activeOffer = paramOffer;
        }

        if (triggerEls.length) {
            $.each(triggerEls, function (i, trigger) {
                var triggerId = hcPromo.getTriggerId('hcp_', trigger);
                var targetId = triggerId.split('hcp_')[1];

                if (typeof targetId !== 'undefined' && targetId != '') {
                    var triggerOfrName = targetId.split('_')[0];
                    var triggerOfrCode = targetId.split('_').slice(1).join('_');
                    var triggerName = triggerOfrName;
                    var triggerCode = triggerOfrCode;

                    trigger.offer = new hcPromo.Offer(triggerName, triggerCode, false);

                    trigger.on('click', function () {
                        trigger.offer.apply();
                    });

                    hcPromo.collectOffer(trigger.offer);
                }
            });
        }

        if (activeOffer != null) {
            activeOffer.apply();
        }
    };

    hcPromo.initPopupTriggers = function() {
        var triggerEls = hcPromo.getTriggerEls('popopen_');
        var urlHasTriggerParam = new RegExp('[?&]popopen_').test(location.search);

        if (triggerEls.length) {
            $.each(triggerEls, function (i, trigger) {
                var triggerId = hcPromo.getTriggerId('popopen_', trigger);
                var popupId = triggerId.split('popopen_')[1];

                if (popupId != 'undefined' && popupId != '' && $('#' + popupId).length) {
                    trigger.on('click', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        openPopup(popupId);
                    });
                }
            });
        }

        if (urlHasTriggerParam) {
            var popupId = location.href.match(/[?&]popopen_(.[^&?]*)/)[1];

            if (popupId != 'undefined' && popupId != '' && $('#' + popupId).length) {
                openPopup(popupId);
            }
        }
    };

    hcPromo.manualEntry = function(val) {
        var manualOffer = new hcPromo.Offer('ofrManualEntry', val, false);
        hcPromo.collectOffer(manualOffer);
        manualOffer.apply();
    };

    hcPromo.initPopupTriggers();
    hcPromo.initHCPromos();

}); // END DOC READY
