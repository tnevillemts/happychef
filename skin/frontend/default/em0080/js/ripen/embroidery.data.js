var embroideryData1 = {
    "panels": {
        "locations": {
            "level": 1
        },
        "types": {
            "level": 2
        },
        "flags": {
            "level": 3
        },
        "ribbons": {
            "level": 3
        },
        "text": {
            "level": 3
        },
        "textColors": {
            "level": 4
        },
        "textStyles": {
            "level": 4
        },
        "preview": {
            "level": 0
        }
    },
    "productId": "66",
    "quoteItemId": "",
    "productOriginalData": {
        "locations": {
            "2": {
                "name": "Right Sleeve",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "5": {
                "name": "Right Cuff",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "6": {
                "name": "Right Collar",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "7": {
                "name": "Right Chest",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "15": {
                "name": "Left Sleeve",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "17": {
                "name": "Left Cuff",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "18": {
                "name": "Left Collar",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "19": {
                "name": "Left Chest",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            }
        },
        "embroideryGroup": {
            "name": "Chef Coat",
            "image": "image1.jpg"
        },
    },
    "productData": {
        "locations": {
            "2": {
                "name": "Right Sleeve",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "5": {
                "name": "Right Cuff",
                    "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "6": {
                "name": "Right Collar",
                    "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "7": {
                "name": "Right Chest",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "15": {
                "name": "Left Sleeve",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "17": {
                "name": "Left Cuff",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "18": {
                "name": "Left Collar",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            },
            "19": {
                "name": "Left Chest",
                "types": {
                    "1": {
                        "limit": null,
                        "name": "ribbons",
                        "selectedOption": []
                    },
                    "2": {
                        "limit": null,
                        "name": "flags",
                        "selectedOption": []
                    },
                    "4": {
                        "limit": "3",
                        "name": "text",
                        "selectedOption": []
                    }
                },
                "instructions": ""
            }
        },
        "embroideryGroup": {
            "name": "Chef Coat",
            "image": "image1.jpg"
        }
    },
    "previewData": [],
    "locations": [
        {
            "id": "2",
            "name": "Right Sleeve",
            "sortorder": null
        },
        {
            "id": "5",
            "name": "Right Cuff",
            "sortorder": null
        },
        {
            "id": "6",
            "name": "Right Collar",
            "sortorder": null
        },
        {
            "id": "7",
            "name": "Right Chest",
            "sortorder": null
        },
        {
            "id": "15",
            "name": "Left Sleeve",
            "sortorder": null
        },
        {
            "id": "17",
            "name": "Left Cuff",
            "sortorder": null
        },
        {
            "id": "18",
            "name": "Left Collar",
            "sortorder": null
        },
        {
            "id": "19",
            "name": "Left Chest",
            "sortorder": null
        }
    ],
    "types": [
        {
            "id": "1",
            "name": "ribbons",
            "price": "5.99",
            "size": "3 1/4",
            "sortorder": "10",
            "options": [
        {
            "id": "13",
            "name": "Stars &amp; Stripes",
            "image": "",
            "sortorder": "10"
        },
        {
            "id": "14",
            "name": "Red White &amp; Blue",
            "image": "",
            "sortorder": "20"
        },
        {
            "id": "15",
            "name": "Blue White &amp; Red",
            "image": "",
            "sortorder": "30"
        },
        {
            "id": "16",
            "name": "Green White &amp; Red",
            "image": "",
            "sortorder": "30"
        },
        {
            "id": "17",
            "name": "Red White &amp; Green",
            "image": "",
            "sortorder": "40"
        },
        {
            "id": "18",
            "name": "Black",
            "image": "",
            "sortorder": "50"
        },
        {
            "id": "19",
            "name": "Red",
            "image": "",
            "sortorder": "60"
        },
        {
            "id": "20",
            "name": "Royal Blue",
            "image": "",
            "sortorder": "70"
        },
        {
            "id": "21",
            "name": "Navy Blue",
            "image": "",
            "sortorder": "80"
        },
        {
            "id": "22",
            "name": "Gold",
            "image": "",
            "sortorder": "90"
        },
        {
            "id": "23",
            "name": "Green",
            "image": "",
            "sortorder": "100"
        }
            ]
        },
        {
            "id": "2",
            "name": "flags",
            "price": "5.99",
            "size": "3 1/4",
            "sortorder": "20",
            "options": [
        {
            "id": "4",
            "name": "USA",
            "image": "",
            "sortorder": "10"
        },
        {
            "id": "5",
            "name": "Canada",
            "image": "",
            "sortorder": "20"
        },
        {
            "id": "6",
            "name": "France",
            "image": "",
            "sortorder": "30"
        },
        {
            "id": "7",
            "name": "Germany",
            "image": "",
            "sortorder": "40"
        },
        {
            "id": "8",
            "name": "Switzerland",
            "image": "",
            "sortorder": "50"
        },
        {
            "id": "9",
            "name": "Mexico",
            "image": "",
            "sortorder": "60"
        },
        {
            "id": "10",
            "name": "Italy",
            "image": "",
            "sortorder": "70"
        },
        {
            "id": "11",
            "name": "Puerto Rico",
            "image": "",
            "sortorder": "80"
        },
        {
            "id": "12",
            "name": "United Kingdom",
            "image": "",
            "sortorder": "90"
        }
            ]
        },
        {
            "id": "3",
            "name": "art",
            "price": "5.99",
            "size": "3 1/4",
            "sortorder": "30",
            "options": []
        },
        {
            "id": "4",
            "name": "text",
            "price": "4.99",
            "size": "",
            "sortorder": "40",
            "options": []
        }
    ],
    "textColors": [
        {
            "id": "1",
            "name": "Black",
            "image": "",
            "sortorder": "10"
        },
        {
            "id": "2",
            "name": "Yellow",
            "image": "",
            "sortorder": "20"
        },
        {
            "id": "3",
            "name": "Silver Gray",
            "image": "",
            "sortorder": "30"
        },
        {
            "id": "4",
            "name": "White",
            "image": "",
            "sortorder": "40"
        },
        {
            "id": "5",
            "name": "Royal Blue",
            "image": "",
            "sortorder": "50"
        },
        {
            "id": "6",
            "name": "Red",
            "image": "",
            "sortorder": "60"
        },
        {
            "id": "7",
            "name": "Purple",
            "image": "",
            "sortorder": "70"
        },
        {
            "id": "8",
            "name": "Orange",
            "image": "",
            "sortorder": "80"
        },
        {
            "id": "9",
            "name": "Navy Blue",
            "image": "",
            "sortorder": "90"
        },
        {
            "id": "10",
            "name": "Kelly Green",
            "image": "",
            "sortorder": "100"
        },
        {
            "id": "11",
            "name": "Gold",
            "image": "",
            "sortorder": "110"
        },
        {
            "id": "12",
            "name": "Dark Green",
            "image": "",
            "sortorder": "120"
        },
        {
            "id": "13",
            "name": "Burgundy",
            "image": "",
            "sortorder": "130"
        },
        {
            "id": "14",
            "name": "Brown",
            "image": "",
            "sortorder": "140"
        }
    ],
    "textStyles": [
        {
            "id": "1",
            "name": "Classic Block",
            "image": "",
            "sortorder": "10"
        },
        {
            "id": "2",
            "name": "Select Script",
            "image": "",
            "sortorder": "20"
        },
        {
            "id": "3",
            "name": "Goudy Bold",
            "image": "",
            "sortorder": "30"
        },
        {
            "id": "4",
            "name": "Guard Script",
            "image": "",
            "sortorder": "40"
        }
    ],
    "ribbons": [],
    "flags": [],
    "textLines": [],
    "embroideryPrice": 0,
    "isOpen": false,
    "currentPanel": "locations",
    "panelTitle": "Embroidery",
    "isInstructionsVisible": false,
    "isDataSaved": [
        "locations",
        "types",
        "text"
    ]
}





/*

var embroideryData = {
    66: {
        "locations": {
            10: {
                "name": "Left Chest",
                "types": {
                    121: {
                        "name":"ribbons",
                        "selectedOption": [{"id":1, "name":"Red White Green"}]
                    },
                    122: {
                        "name":"flags",
                        "selectedOption": [{"id":2, "name":"Red White Blue"}]
                    },
                    123: {
                        "name":"art",
                        "selectedOption": []
                    },
                    124: {
                        "name":"custom",
                        "selectedOption": [
                            {"artworkName": "my precious", "media": "/media/999.jpg"}
                        ]
                    },
                    125: {
                        "name":"text",
                        "limit":3,
                        "selectedOption": [
                            {"textLine": "Hello1", "textColor": 3, "textColorValue":"Black", "textStyle": 1, "textStyleValue": "Gouda", "sortorder": 1},
                            {"textLine": "Hello2", "textColor": 2, "textStyle": 3, "sortorder": 2},
                            {"textLine": "Hello3", "textColor": 2, "textStyle": 2, "sortorder": 3},
                        ]
                    }
                },
                "instructions": "yo!"
            },

            12: {
                "types": {
                    121: {
                        "selectedOption": [3]
                    },
                    122: {
                        "selectedOption": []
                    }
                },
                "instructions": ""
            }
        },
        "embroideryGroup": {
            "name": "Chef Jacket",
            "image": "hey.jpg"
        }
    }
};

var embroideryLocations = [
    {"id":10, "name": "Left Chest", "sort":10},
    {"id":11, "name": "Right Chest", "sort":20},
    {"id":12, "name": "Center", "sort":30},
]

var embroideryTypes = [
    {
        "id": 121,
        "name": "ribbons",
        "price": 5.99,
        "size": "3 1/4",
        "options": [
            {"id": 1, "name": "Red White Green", "sort": 10, "image": "/media/blah.jpg"},
            {"id": 2, "name": "Red White Blue", "sort": 20, "image": "/media/blah.jpg"},
            {"id": 3, "name": "Red White Yellow", "sort": 30, "image": "/media/blah.jpg"},
        ]
    },
    {
        "id": 122,
        "name": "flags",
        "price": 5.99,
        "size": "3 1/4",
        "options": [
            {"id": 1, "name": "Canada", "sort": 10, "image": "/media/blah.jpg"},
            {"id": 2, "name": "Mexico", "sort": 20, "image": "/media/blah.jpg"},
            {"id": 3, "name": "USA", "sort": 30, "image": "/media/blah.jpg"},
        ]
    },
    {
        "id": 123,
        "name": "art",
        "price": 5.99,
        "size": "3 1/4",
        "options": [
            {"id": 1, "name": "Chef", "sort": 10, "image": "/media/blah.jpg"},
            {"id": 2, "name": "Fork", "sort": 20, "image": "/media/blah.jpg"},
            {"id": 3, "name": "Spoon", "sort": 30, "image": "/media/blah.jpg"}
        ]
    },
    {
        "id": 124,
        "name": "custom",
        "price": 9.99,
        "size": "3 1/4",
        "options": []
    },
    {
        "id": 125,
        "name": "text",
        "price": 4.99,
        "options": []
    }
];


var embroideryTextStyles = [
    {"id":1, "name": "Block", "sort": 10, "image": "/media/blah.jpg"},
    {"id":2, "name": "Script", "sort": 20, "image": "/media/blah.jpg"},
    {"id":3, "name": "Gouda", "sort": 30, "image": "/media/blah.jpg"},
];

var embroideryTextColors = [
    {"id":1, "name": "Red", "sort": 10, "image": "/media/blah.jpg"},
    {"id":2, "name": "Orange", "sort": 20, "image": "/media/blah.jpg"},
    {"id":3, "name": "Purple", "sort": 30, "image": "/media/blah.jpg"},
];

    */