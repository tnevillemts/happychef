RegionUpdater.prototype._checkRegionRequired = RegionUpdater.prototype._checkRegionRequired.wrap (function(parentMethod){


     var label, wildCard;
     var elements = [this.regionTextEl, this.regionSelectEl];
     var that = this;

     if($('region') != undefined && (this.countryEl.value == "PR" || this.countryEl.value == "VI")) {
        $('region').hide();
     }

     if (typeof this.config == 'undefined') {
     return;
     }
     var regionRequired = this.config.regions_required.indexOf(this.countryEl.value) >= 0;

     elements.each(function(currentElement) {
     Validation.reset(currentElement);
     label = $$('label[for="' + currentElement.id + '"]')[0];
     if (label) {
     wildCard = label.down('em') || label.down('span.required');
     if (!that.config.show_all_regions) {
     if (regionRequired) {
     label.up().show();
     } else {
     label.up().hide();
     }
     }
     }

     if (label && wildCard) {
     if (!regionRequired) {
     wildCard.hide();
     if (label.hasClassName('required')) {
     label.removeClassName('required');
     }
     } else if (regionRequired) {
     wildCard.show();
     if (!label.hasClassName('required')) {
     label.addClassName('required')
     }
     }
     }

     if (!regionRequired) {
     if (currentElement.hasClassName('required-entry')) {
     currentElement.removeClassName('required-entry');
     }
     if ('select' == currentElement.tagName.toLowerCase() &&
     currentElement.hasClassName('validate-select')) {
     currentElement.removeClassName('validate-select');
     }
     } else {
     if (!currentElement.hasClassName('required-entry')) {
     currentElement.addClassName('required-entry');
     }
     if ('select' == currentElement.tagName.toLowerCase() &&
     !currentElement.hasClassName('validate-select')) {
     currentElement.addClassName('validate-select');
     }
     }
     });
});

