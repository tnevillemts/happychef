(function($) {
    $(function() {
        var carousel = $('.jcarousel').jcarousel();
        // $('.jcarousel').jcarousel().jcarouselSwipe();

        var jcarousel = $('.jcarousel');

        jcarousel.jcarouselSwipe({perSwipe: 1});

        $('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination();

        // var carousel.jcarousel();

        carousel.touchwipe({
                wipeLeft: function() {
                    carousel.jcarousel('next');
                },
                wipeRight: function() {
                    carousel.jcarousel('prev');
                }
            });
    });
})(jQuery);
