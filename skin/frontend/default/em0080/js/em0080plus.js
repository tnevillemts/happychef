
(function($) {
jQuery(document).ready(function() {
	var $ = jQuery;
	domLoaded = true;
	
	setTimeout(function(){
        if (window.innerWidth > 767 || $('.two-col-var').length) {
            var maxHeightCat = getMaxHeightCat();
            $('.category-products .products-grid > .item').height(maxHeightCat);
		}
	}, 1000);
});

$(window).resize(function() {
    setTimeout(function(){
        if (window.innerWidth > 767 || $('.two-col-var').length) {
            var maxHeightCat = getMaxHeightCat();
            $('.category-products .products-grid > .item').height(maxHeightCat);
        }
    }, 1000);
});

function getMaxHeightCat(){
	var $ = jQuery;
	var maxHeightCat = 0;
	$('.category-products .products-grid > .item').each(function(){
		if ($(this).height() > maxHeightCat) {
			maxHeightCat = $(this).height();
		}
	});
	return maxHeightCat;
}

})(jQuery);
