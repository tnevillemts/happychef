(function($) {

$.fn.selectUl = function(){    
    $('.toolbar-title select').css('display','none');
    $('.toolbar-switch .toolbar-dropdown').hover(
        function(){
            if ($(this).siblings().length > 1){
                $(this).parent().find('.toolbar-dropdown > ul').addClass('over');
            } else {
                $(this).addClass('over');
                // $('.toolbar-dropdown', this).css({width: $(this).width()+50});
            }

            $(this).parent().find('.toolbar-dropdown > ul').fadeIn(100).slideDown(100);
        },
        function(){
            var that = this;
            
            $(this).parent().find('.toolbar-dropdown > ul').fadeOut(100).slideUp(100, function(){
                if ($(this).siblings().length > 1){
                    $(that).parent().find('.toolbar-dropdown > ul').removeClass('over');
                } else {
                    $(that).removeClass('over');
                }
            });
        }
    );
}
$.fn.insertTitle = function(){
    $('<span class="current"/>').text($(this).find('option:selected').text())
    .insertBefore($(this));
}
$.fn.insertUl = function(){
    var numOptions = $(this).children().length;

    $('<div class="toolbar-dropdown"><span class="current"/><ul style="display:none" /></div>')
        .insertAfter($(this).toggleClass('.toolbar-switch').parent());

    var divSpan = $(this).toggleClass('.toolbar-switch').parent().parent().find('span');
    divSpan.append($(this).parent().children('select').find('option:selected').text());

    var divUl = $(this).toggleClass('.toolbar-switch').parent().parent().find('ul');
    for (var i = 0; i < numOptions; i++) {
        var text = '<li><a href ="'+$(this).find('option').eq(i).val()+'">'+$(this).find('option').eq(i).text()+'</a></li>';
       //$('<li />').text(text).appendTo(divUl);
       divUl.append(text);
    }

}
$.fn.viewPC = function(){

    var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|hpwos/i.test(navigator.userAgent);
    var isPhone = /iPhone|iPod|Phone|Android/i.test(navigator.userAgent);

    if(isMobile || isPhone)
        return false;
    return true;
}

$.fn.selectUlCategorySearch = function(){
    $('.input_cat .catsearch-dropdown').hover(
        function(){
            if ($(this).siblings().length > 1){
                $(this).parent().find('.catsearch-dropdown > ul').addClass('over');
            } else {
                $(this).addClass('over');
                // $('.toolbar-dropdown', this).css({width: $(this).width()+50});
            }

            $(this).parent().children('.catsearch-dropdown').children('ul').fadeIn(100).slideDown(100);
        },
        function(){
            var that = this;
            $(this).parent().children('.catsearch-dropdown').children('ul').fadeOut(100).slideUp(100, function(){
                if ($(this).parent().children().length > 1){
                    $(that).parent().children('.catsearch-dropdown').children('ul').removeClass('over');
                } else {
                    $(that).removeClass('over');
                }
            });
        }
    );

    $('.input_cat select').css('display','none');
}

$.fn.hoverProductItem = function(){
    $(this).hover(function(){
        $(this).find('.actions-cart').css('display','block');
    }, function() {
        
        //Clean up
        $(this).find(".actions-cart").hide();
                
    });
} 


})(jQuery);