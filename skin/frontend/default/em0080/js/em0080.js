/**
 * EMThemes
 *
 * @license commercial software
 * @copyright (c) 2012 Codespot Software JSC - EMThemes.com. (http://www.emthemes.com)
 */


(function ($) {

    if (typeof EM == 'undefined') EM = {};
    if (typeof EM.tools == 'undefined') EM.tools = {};
    var imgH = 0;

    var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|webOS|iPod|BlackBerry|hpwos/i.test(navigator.userAgent);
    var isPhone = /iPhone|iPod|Phone|Mobile|Android|webOS|iPod|BlackBerry/i.test(navigator.userAgent);

    var domLoaded = false,
        windowLoaded = false,
        last_adapt_i,
        last_adapt_width;


    //************************************************************/
    // SINGLE ROW SWATCHES (CATEGORY PAGE PRODUCT GRID)
    //************************************************************/


    $(window).on('load', function () {
        // make this only for current item's swatches - events should still propagate on other items' swatches
        $('img[id^="amconf-image-"]').click(function (e) {
            if ($(this).closest('li.item').hasClass('swatchExpand')) {
                e.stopPropagation();
            }
        });

        /** swatchColors defined in app/design/frontend/default/em0080/template/catalog/product/list.phtml **/
        if (typeof swatchColors !== "undefined") {

            $(".amconf-images-container").css({"text-align":"center"});

            $('.amconf-image-container').each(function() {
                // Extract the color ID from the element ID
                var colorID = $(this).prop("id").match(/-([0-9]+)-/);
                if (colorID) {
                    if ($.inArray(colorID[1], swatchColors) < 0) {
                        $(this).hide();
                    }
                }
            });

        } else {
            // Override default set in CSS
            $('.amconf-image-container').css({'display':'inline-block'});
        }

        setSwatchContainerSize();

    });

    function setSwatchContainerSize() {
        var swatchesPerRow = 8;
        var allItems = $('.catalog-category-view .products-grid li.item, .catalogsearch-result-index .products-grid li.item');
        allItems.each(function () {
            var swatchContainer = $(this).find('.amconf-images-container');

            if (swatchContainer.length) {
                var itemSwatches = swatchContainer.find('.amconf-image-container:visible');
                var swatchWidth = itemSwatches.first().outerWidth(true);
                var swatchHeight = itemSwatches.first().outerHeight(true);

                if (itemSwatches.length > swatchesPerRow) {
                    swatchContainer.css({
                        'width': swatchWidth * swatchesPerRow,
                        'display': 'block'
                    });
                }

                swatchContainer.find('.swatchOverflow').remove();
                if (itemSwatches.length > swatchesPerRow) {
                    var overflowSwatch = $('<div />', {
                            text: '+' + String(itemSwatches.length - swatchesPerRow + 1),
                            'class': 'swatchOverflow',
                            css: {
                                'line-height': (swatchHeight - 6) + 'px',
                                'width': (swatchWidth - 2) + 'px',
                                'height': (swatchHeight - 2) + 'px'
                            }
                        });

                    swatchContainer.append(overflowSwatch);
                }
            }
        });
    }

    $(window).on('resize', function() {
        setTimeout(setSwatchContainerSize, 75)
    });

    $(document).on('click', '.swatchOverflow', function (e) {
        var thisItem = $(this).closest('li.item');
        thisItem.find('.swatchOverflow').fadeOut().css('display:none');
        if (!thisItem.hasClass('swatchExpand')) {
            thisItem.find('.amconf-images-container').animate({height: thisItem.find('.amconf-images-container').get(0).scrollHeight}, 250);
        }

        e.stopPropagation();
    });

    $(document).on('click', function () {
        $('.indicator').text('+');
    });

    responsive();

    /*****************************************************************/
    /* SINGLE ROW SWATCHES */
    /*****************************************************************/


    /**
     * Auto positioning product items in products-grid
     *
     * @param (selector/element) productsGridEl products grid element
     * @param (object) options
     * - (integer) width: width of product item
     * - (integer) spacing: spacing between 2 product items
     */

    /**
     * Decorate Product Tab
     */
    EM.tools.decorateProductCollateralTabs = function () {
        $(document).ready(function () {
            $('.product-collateral').addClass('tab_content').each(function (i) {
                $(this).wrap('<div class="tabs_wrapper collateral_wrapper" />');
                var tabs_wrapper = $(this).parent();
                var tabs_control = $(document.createElement('ul')).addClass('tabs_control').insertBefore(this);

                $('.box-collateral', this).addClass('tab-item').each(function (j) {
                    var id = 'box_collateral_' + i + '_' + j;
                    $(this).addClass('content_' + id);
                    tabs_control.append('<li><h2><a href="#' + id + '">' + $('h2', this).html() + '</a></h2></li>');
                });

                initToggleTabs(tabs_wrapper);
            });

        });
    };


    /**
     * Fix iPhone/iPod auto zoom-in when text fields, select boxes are focus
     */
    function fixIPhoneAutoZoomWhenFocus() {
        var viewport = $('head meta[name=viewport]');
        if (viewport.length == 0) {
            $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0"/>');
            viewport = $('head meta[name=viewport]');
        }

        var old_content = viewport.attr('content');

        function zoomDisable() {
            viewport.attr('content', old_content + ', user-scalable=0');
        }

        function zoomEnable() {
            viewport.attr('content', old_content);
        }

        $("input[type=text], textarea, select").mouseover(zoomDisable).mousedown(zoomEnable);
    }


    /**
     * Adjust elements to make it responsive
     *
     * Adjusted elements:
     * - Image of product items in products-grid scale to 100% width
     */



    function responsive() {
        var position = $('.products-grid .item').css('position');
        if (position != 'absolute' && position != 'fixed' && position != 'relative')
            $('.products-grid .item').css('position', 'relative');

        var img = $('.products-grid .item .product-image img');
        img.each(function () {
            img.data({
                'width': $(this).width(),
                'height': $(this).height()
            })
        });

        $('.custom-logo').each(function () {
            $(this).css({
                'max-width': $(this).width(),
                'width': '100%'
            });
        });
    }


    /**
     * Function called when layout size changed by adapt.js
     */
    function whenAdapt(i, width) {

        $('body').removeClass('adapt-0 adapt-1 adapt-2 adapt-3 adapt-4 adapt-5 adapt-6')
            .addClass('adapt-' + i);

        if (typeof em_slider !== 'undefined') {
            setTimeout(function () {
                em_slider.reinit();
            }, 100);
        }

        //disable freezed top menu when in iphone
        window.freezedTopMenu = (i != 0 && FREEZED_TOP_MENU) ? 1 : 0;
        if (window.freezedTopMenu && $(window).scrollTop() > 145) {
            $('.em_nav, .nav-container').addClass('fixed-top');
        } else {
            $('.em_nav, .nav-container').removeClass('fixed-top');
        }

    }

    function initToggleTabs_Product($selector) {
        if (jQuery($selector).length > 0) {
            var timeout = new Array(jQuery($selector).length);
            var div = new Array(jQuery($selector).length);
            jQuery($selector).addClass('ui-tabs ui-widget ui-widget-content ui-corner-all');
            jQuery($selector).each(function (index, value) {
                timeout[index] = null;
                div[index] = jQuery(this);
                div[index].addClass('ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all');
                //When page loads...
                div[index].find(".tab-item").hide(); //Hide all content
                div[index].children('div').children('ul').find("li:first").addClass("ui-tabs-selected").show(); //Activate first tab
                div[index].children('div').children('ul').addClass('ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all');
                div[index].children('div').children('ul').find('li').addClass('ui-state-default ui-corner-top');
                div[index].find(".tab-item:first").show(); //Show first tab content
                //On Click Event
                div[index].children('div').children('ul').find("li").click(function () {
                    var currentTab = jQuery(this);
                    if (currentTab.hasClass('ui-tabs-selected'))
                        return false;
                    if (timeout[index])
                        clearTimeout(timeout[index]);
                    timeout[index] = setTimeout(function () {
                        timeout[index] = null;
                        // Hide old content tab
                        jQuery(div[index].children('div').children('ul').find('li.ui-tabs-selected a').attr('href')).toggle('slow');

                        div[index].children('div').children('ul').find("li").removeClass("ui-tabs-selected"); //Remove any "ui-tabs-selected" class
                        currentTab.addClass("ui-tabs-selected"); //Add "active" class to selected tab

                        var activeTab = currentTab.find("a").attr("href"); //Find the href attribute value to identify the active tab + content
                        jQuery(activeTab).toggle('slow'); //Fade in the active ID content
                        return false;
                    }, 10);
                    return false;
                });
            });

        }
    }


    function menuVertical() {
        if ($('.vnav > .menu-item-link > .menu-container > li.fix-top').length > 0) {
            $('.vnav > .menu-item-link > .menu-container > li.fix-top').parent().parent().mouseover(function () {
                var $container = $(this).children('.menu-container,ul.level0');
                var $containerHeight = $container.outerHeight();
                var $containerTop = $container.offset().top;
                var $winHeight = $(window).height();
                var $maxHeight = $containerHeight + $containerTop;
                //if($maxHeight >= $winHeight){
                $setTop = $(this).parent().offset().top - $(this).offset().top;
                if (($setTop + $containerHeight) < $(this).height()) {
                    $setTop = $(this).outerHeight() - $containerHeight;
                }
                /*}else{
                    $setTop = (-1);
                }*/
                var $grid = $(this).parents('.em_nav').first().parents().first();
                $container.css('top', $setTop);
                if ($maxHeight < $winHeight) {
                    $('.vnav ul.level0,.vnav > .menu-item-link > .menu-container').first().css('top', $setTop - 9 + 'px');
                }

            });
            $('.vnav .menu-item-link > .menu-container,.vnav ul.level0').parent().mouseout(function () {
                var $container = $(this).children('.menu-container,ul.level0');
                $container.removeAttr('style');
            });
        }
    }


    window.hoverTopAccount = function () {
        $(function ($) {
            $('.myaccount .myaccount-content').mouseover(function () {
                $(this).addClass('active');
            })
                .mouseout(function () {
                    $(this).removeClass('active');
                });

            $('.myaccount ').each(function () {
                if (isMobile == true) {
                    /** TODO: Used as highlight, My account link was being hidden from the site, put it back as a regular link, this did not work at the time. Leaving the condition to not risk breaking mobile or desktop. Perhaps this can be refactored.
                     $('.myaccount').find("span#link-myaccount a").attr('href','javascript:void(0);');
                     $(this).unbind('click');
                     var divWrapper = $(this);
                     $(this).find("span#link-myaccount a").click(function (e) {
					e.preventDefault();
                  divWrapper.find('.myaccount-content').slideToggle();
                   if ($('.dropdown-cart').hasClass('open')){
                       $('.dropdown-cart').removeClass('open');
                   }
				});

                     /*
                     // the following code should be uncommented when ajax loading for My Account links is used (Ripen MM)
                     $(this).on("click", function(){
                loadAccount();
                });
                     */

                } else {
                    var tm;

                    function show(el) {
                        clearTimeout(tm);
                        tm = setTimeout(function () {
                            el.slideDown();
                        }, 200);
                    }

                    function hide(el) {
                        clearTimeout(tm);
                        tm = setTimeout(function () {
                            el.slideUp();
                        }, 1000);
                    }

                    $(this)
                        .bind('mouseenter', show.curry($('.myaccount-content', this)))
                        .bind('mouseleave', hide.curry($('.myaccount-content', this)))
                        .find('.myaccount-content').slideUp();
                }
            });
        })
    };

    jQuery(document).ready(function () {

        var $ = jQuery;
        domLoaded = true;
        initToggleTabs_Product('#product-content-tabs');
        isMobile && fixIPhoneAutoZoomWhenFocus();
        alternativeProductImage();

        setupReviewLink();
        if (FREEZED_TOP_MENU) persistentMenu();

        window.hoverTopAccount();

        $('.cat-search').each(function () {
            $(this).insertUlCategorySearch();
            $(this).selectUlCategorySearch();
        });

        menuVertical();

        $('.products-grid li.item a:not(".mob-quick-shop")').on('touchstart', touchScroll);
        $('.hnav .menu-item-link').on('touchstart', touchScroll);
    });

    function touchScroll(e) {
        $(e.target).on('touchend', function (e) {
            var el = $(e.target).closest("a");
            var link = el.attr('href');
            if (typeof link != "undefined") {
                window.location = link;
                $(e.target).off('touchend');
            }
        });
        $(e.target).on('touchmove', function (e) {
            $(e.target).off('touchend');
        });
    }

    $(window).on('focus', function () {
        if (!($('#myaccount').length > 0) && !$('body').hasClass('checkout-onepage-index')) {
            new Ajax.Request('/customercontent/html/myAccountNavItem', {
                method: 'post',
                onComplete: function (response) {
                    var result = $.parseJSON(response.responseText);
                    if (result.status) {
                        $('#login-link').attr('onclick', null).off('click');
                        $('#account').hide();
                        $('#account').replaceWith(result.myAccountHtml);
                        window.hoverTopAccount();
                        _gaq.push(['_trackEvent', 'Multi Tabs', 'Other Login Session', 'Updated Sign In Nav']);
                    }
                }
            });
        }
    });


    $(window).bind('load', function () {
        windowLoaded = true;
        whenAdapt(last_adapt_i, last_adapt_width);
        initButtonCart();
    });

    $(window).bind('orientationchange', function () {

        // Add Mansory

        if ($('.category-products ul.products-grid').hasClass('products-mansory')) {

            EM.tools.decorateProductsMansory('.category-products .products-mansory', {
                width: PRODUCTSGRID_ITEM_WIDTH_MAN,
                spacing: PRODUCTSGRID_ITEM_SPACING
            });
        } else {
            EM.tools.decorateProductsGrid('.category-products .products-grid', {
                width: PRODUCTSGRID_ITEM_WIDTH,
                spacing: PRODUCTSGRID_ITEM_SPACING
            });
        }

    });

    function showAgreementPopup(e) {
        jQuery('#checkout-agreements label.a-click').parent().parent().children('.agreement-content').show()
            .css({
                'left': (parseInt(document.viewport.getWidth()) - jQuery('#checkout-agreements label.a-click').parent().parent().children('.agreement-content').width()) / 2 + 'px',
                'top': (parseInt(document.viewport.getHeight()) - jQuery('#checkout-agreements label.a-click').parent().parent().children('.agreement-content').height()) / 2 + 'px'
            });
    };

    function hideAgreementPopup(e) {
        jQuery('#checkout-agreements .agreement-content').hide();

    };

    /**
     *   persistentMenu
     **/
    function persistentMenu() {
        var $ = jQuery;

        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 145 && window.freezedTopMenu) {
                    $('.top-menu').addClass('fixed-menu-top');
                    $('.nav-container').addClass('fixed-top');
                    $('.hnav').parent().addClass('fixed-top');
                } else {
                    $('.top-menu').removeClass('fixed-menu-top');
                    $('.nav-container').removeClass('fixed-top');
                    $('.hnav').parent().removeClass('fixed-top');
                }
            });
        });
    }

    /**
     *   showReviewTab
     **/
    function showReviewTab() {
        var $ = jQuery;
        if ($('#ProductDetail_ProductReviews-Container').size()) {
            // scroll to customer review
            $('html, body').animate({scrollTop: $('#ProductDetail_ProductReviews-Container').offset().top - 20}, 500);
            return true;
        }
        return false;
    }

    /**
     *   setupReviewLink
     **/
    function setupReviewLink() {
        jQuery('.r-lnk').click(function (e) {
            if (showReviewTab())
                e.preventDefault();
        });
    }

    /**
     * Change the alternative product image when hover
     */
    function alternativeProductImage() {
        var $ = jQuery;
        var tm;

        function swap() {
            clearTimeout(tm);
            setTimeout(function () {
                el = $(this).find('img[data-alt-src]');
                var newImg = $(el).data('alt-src');
                var oldImg = $(el).attr('src');
                $(el).attr('src', newImg).data('alt-src', oldImg);
                $(el).fadeIn(300, "easeInCubic");

            }.bind(this), 400);
        }

        $('.item .product-image img[data-alt-src]').parents('.product-image').bind('mouseenter', swap).bind('mouseleave', swap);
    }

    function initButtonCart() {
        var $ = jQuery;
        $('ul.products-grid').find('.item').each(function () {
            var o = $(this).find('.product-image').find('img');
            var offset = $(o).offset();

            if (offset && offset !== 'undefined') {
                var div_top = offset.top - $(this).offset().top + o.height() / 2;
                var div_left = (($(o).outerWidth() - $(this).find('.actions-cart').outerWidth(true)) / 2) + offset.left - $(this).offset().left;
                var qs = 0;
                if (EM.QuickShop != null) {
                    qs = qs + EM.QuickShop.QS_BTN_HEIGHT - 15;
                }
                div_top = div_top + qs;
                $(this).find('.actions-cart').css({
                    'top': div_top + 'px',
                    'left': div_left + 'px'
                });
            }
        });
    }

    /**
     *    Hover product item
     **/
    function hoverProduct() {
        var $ = jQuery;
        $('.products-grid > .item').each(function () {
            $(this).hoverProductItem();
        });

        $('.amconf-images-container').hover(function () {
            $(this).hoverProductItem();
        });
    }

    /**
     *   After Layer Update
     **/

    window.afterLayerUpdate = function () {

        var $ = jQuery;
        setTimeout(function () {
            initButtonCart();
        }, 500);

        alternativeProductImage();
        qs({
            itemClass: '.products-grid li.item, .products-list li.item, li.item .cate_product, .product-upsell-slideshow li.item, .mini-products-list li.item, #crosssell-products-list li.item',
            aClass: 'a.product-image', //selector for each a tag in product items,give us href for one product
            imgClass: '.product-image img' //class for quickshop href
        });

    }

})(jQuery);
