jQuery(document).ready(function() {
	var $ = jQuery;
    toogleStore();

});

function initSlider(e,verticals) {
	var $ = jQuery;
    var wraps;
	if (verticals == null){
		verticals=false;
        wraps = null;
    }else{
        wraps = 'circular';
    }
	
	var widthcss = $( e + ' li.item').width();
	var leftcss = $( e + ' li.item').outerWidth(true)- $( e + ' li.item').outerWidth();
	$(e).addClass('jcarousel-skin-tango');
	$(e).parent().append('<div class="slide_css">');
	$(e).parent().find('.slide_css').html('<style type="text/css">'+e+' .jcarousel-item {width:' + widthcss + 'px;margin-left:'+ leftcss +'px;}</style>');

    if ($(e)) {
        $(e).jcarousel({
            buttonNextHTML: '<a class="next" href="javascript:void(0);" title="Next"></a>',
            buttonPrevHTML: '<a class="previous" href="javascript:void(0);" title="Previous"></a>',
            scroll: 1,
            wrap: null,
            animation: 'slow',
            vertical: false,
            initCallback: function (carousel) {
                var context = carousel.container.context;
                $(context).touchwipe({
                    wipeLeft: function () {
                        carousel.next();
                    },
                    wipeRight: function () {
                        carousel.prev();
                    },
                    preventDefaultEvents: false
                });
                $(window).bind('emadaptchange', function () {
                    $(window).trigger('resize.jcarousel');
                });
            }
        });
    }
}

function verticalSlider(e,verticals) {
	var $ = jQuery;
   	
	var heightcss = $( e + ' li.item').height();
	var topcss = $( e + ' li.item').outerHeight(true)- $( e + ' li.item').outerHeight();
	$(e).addClass('jcarousel-skin-tango');
	$(e).parent().append('<div class="slide_css">');
	$(e).parent().find('.slide_css').html('<style type="text/css">'+e+' .jcarousel-item {height:' + heightcss + 'px;margin-top:'+ topcss +'px;}</style>');

	$(e).jcarousel({
		buttonNextHTML:'<a class="next" href="javascript:void(0);" title="Next"></a>',
		buttonPrevHTML:'<a class="previous" href="javascript:void(0);" title="Previous"></a>',
		scroll: 1,
		wrap: null,
		animation:'slow',
		vertical:true,
		initCallback: function (carousel) {
			var context = carousel.container.context;
			$(context).touchwipe({
				wipeLeft: function() { 
					carousel.next();
				},
				wipeRight: function() { 
					carousel.prev();
				},
				preventDefaultEvents: false
			});
	            $(window).bind('emadaptchange', function() {
	                $(window).trigger('resize.jcarousel');
	            });
		}
	});
}


function toogleStore(){
    var $=jQuery;
    initSlider('#slider_storeview ul');
    $('.storediv').hide();
    $(".btn_storeview").click(function() {
		store_show();
	});

	$(".btn_storeclose").click(function() {
		store_hide();
	});

	function store_show(){
		var bg	=	$("#bg_fade_color");
		bg.css("opacity",0.5);
		bg.css("display","block");
		$(".storediv").show();
        var top =( $(window).height() - $(".storediv").height() ) / 2;
        var left = ( $(window).width() - $(".storediv").width() ) / 2;
        $(".storediv").css('top', top+'px');
        $(".storediv").css('left', left+'px');
	}

	function store_hide(){
		var bg	=	$("#bg_fade_color");
		$(".storediv").hide();
		bg.css("opacity",0);
		bg.css("display","none");
	}
}