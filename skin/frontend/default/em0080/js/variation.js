/**
 * EMThemes
 *
 * @license commercial software
 * @copyright (c) 2012 Codespot Software JSC - EMThemes.com. (http://www.emthemes.com)
 */
var count = 0;
var object;
var default_object;
var elemPIE;
var specPIE;

(function($) {

$(document).ready(function() {
	
	initObject();
	
	
	$('.color-picker').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val('#'+hex);
			$(el).css('backgroundColor', '#' + hex);
			$(el).ColorPickerHide();
			
		},
		onChange: $.debounce( 300, function(hsb, hex) {
			var el = this.data('colorpicker').el;
			$(el).val('#'+hex);
			
			$(el).css('backgroundColor', '#' + hex);
			
			
			if (window.PIE) {
					
					jQuery(elemPIE).each(function() {
						PIE.detach(this);
					});
					
				}
			//console.log($(el).attr('name'))
			changeVariation('@'+$(el).attr('name'),$(el).val())
			
		}),
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);            
	});
	
	$('#demotool_variation input.input-text').bind('keyup',function(e,hbs,hex){
		if($(this).val()=="")
		{
			$(this).css('background-color','');
			returnDefault('@'+$(this).attr('name'));
		}
		else if(e.keyCode == 13)
			changeVariation('@'+$(this).attr('name'),$(this).val());
	});
	
	$('#demotool_variation input.input-text').blur(function() {
		if($(this).val()=="")
		{
			$(this).css('background-color','');
			returnDefault('@'+$(this).attr('name'));
		}
		else
			changeVariation('@'+$(this).attr('name'),$(this).val());
	});
});


/*Khoi Tao object*/
initObject = function(){
	object = new Object();
	for(key in default_object)
	{
		object[key] = default_object[key];
	}
}

/*-----------------------------------------------------*/
changeVariation = function (key, value){
		var key = key || null;
		var value = value || null;	
		
		if(key!=null)
		{
			if(String(value).indexOf('.') != -1 || String(value).indexOf('/')!=-1 || String(value).indexOf(' ') != -1)
				object[key] = '"'+String(value.replace(/"/g,"'"))+'"';
			else
				object[key] = String(value);
		}
		less.modifyVars(object);	
		AttachPIE(elemPIE,specPIE)
		
}

/*return default value when input value is null*/
returnDefault = function (key) {
	object[key] = default_object[key];
	changeVariation();
}

/*reset Variation*/
resetVariation = function(){
	initObject();
	changeVariation();
}

})(jQuery);


