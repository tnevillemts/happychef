<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2012 Amasty (http://www.amasty.com)
* @package Amasty_Stockstatus
*/
$installer = $this;
$installer->startSetup();


$tableName = $this->getTable('sales_flat_quote_item');
$fieldsSql = 'SHOW COLUMNS FROM ' . $tableName;
$cols = $this->getConnection()->fetchCol($fieldsSql);

if (!in_array('stock_status', $cols))
{
    $this->run("ALTER TABLE `{$tableName}`  ADD `stock_status` TINYINT NULL");
}
if (!in_array('qty_available', $cols))
{
    $this->run("ALTER TABLE `{$tableName}`  ADD `qty_available` TINYINT NULL");
}
if (!in_array('ship_lead_time', $cols))
{
    $this->run("ALTER TABLE `{$tableName}`  ADD `ship_lead_time` TINYINT NULL");
}

$tableName = $this->getTable('sales_flat_order_item');
$fieldsSql = 'SHOW COLUMNS FROM ' . $tableName;
$cols = $this->getConnection()->fetchCol($fieldsSql);
if (!in_array('stock_status', $cols))
{
    $this->run("ALTER TABLE `{$tableName}`  ADD `stock_status` TINYINT NULL");
}
if (!in_array('qty_available', $cols))
{
    $this->run("ALTER TABLE `{$tableName}`  ADD `qty_available` TINYINT NULL");
}
if (!in_array('ship_lead_time', $cols))
{
    $this->run("ALTER TABLE `{$tableName}`  ADD `ship_lead_time` TINYINT NULL");
}

$installer->endSetup();