<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2008-2012 Amasty (http://www.amasty.com)
* @package Amasty_Stockstatus
*/
class Amasty_Stockstatus_Model_Rewrite_Sales_Quote_Item extends Mage_Sales_Model_Quote_Item
{
    public function getMessage($string = true)
    {
        if (('checkout' == Mage::app()->getRequest()->getModuleName() || 'amscheckout' == Mage::app()->getRequest()->getModuleName()) && Mage::getStoreConfig('amstockstatus/general/displayincart')) {

            $simpleItem = ($this->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) ?
                reset($this->getChildren()) : $this;
/*
	    if ($_SERVER["REMOTE_ADDR"] == "75.146.246.189") {
		echo $this->getProductType();
		echo get_class($this);
		echo $this->getId();
                var_dump($this->getChildren());
            }
*/	
	    if ($simpleItem){
            	$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $simpleItem->getSku());
	    }

            if (!$product) {
                $product = Mage::getModel('catalog/product')->load($this->getProduct()->getId());
            }
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if (!(Mage::getStoreConfig('amstockstatus/general/displayforoutonly') && $product->isSaleable()) || ($product->isInStock() && $stockItem->getData('qty') <= Mage::helper('amstockstatus')->getBackorderQnt())) {
                $status = Mage::helper('amstockstatus')->getCustomStockStatusText(Mage::getModel('catalog/product')->load($product->getId()));
                if ($status && (!Mage::registry('am_is_duplicate') || (Mage::registry('am_is_duplicate') && !array_key_exists($product->getId(), Mage::registry('am_is_duplicate'))))) {

                    $this->addMessage($status);
                    if (Mage::registry('am_is_duplicate'))
                        $massKey = Mage::registry('am_is_duplicate');
                    else
                        $massKey = array();
                    $massKey[$product->getId()] = $product->getId();
                    Mage::unregister('am_is_duplicate');
                    Mage::register('am_is_duplicate', $massKey);
                }
            }
        }
        return parent::getMessage($string);
    }
}
