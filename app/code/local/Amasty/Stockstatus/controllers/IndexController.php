<?php
/**
* @copyright Amasty.
*/ 
class Amasty_Stockstatus_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {

    }

    public function getStockStatusAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        if ($isAjax) {

            $product = Mage::getModel('catalog/product')->load($_POST["productId"]);

            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if ( (!Mage::getStoreConfig('amstockstatus/general/displayforoutonly') || !$product->isSaleable()) || ($product->isInStock() && $stockItem->getData('qty') <= Mage::helper('amstockstatus')->getBackorderQnt() ) )
            {
                if ($product->getData('hide_default_stock_status') || ($product->isInStock() && 0 == $stockItem->getData('qty')))
                {
                    $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);

                } elseif (Mage::helper('amstockstatus')->getCustomStockStatusText($product)) {

                    if (!$product->isInStock())
                    {
                        $stockStatus = Mage::helper('amstockstatus')->__('Out of Stock') . ' - ' . Mage::helper('amstockstatus')->getCustomStockStatusText($product);
                    } else
                    {
                        $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);
                    }
                }
            }

            $this->getResponse()->setBody($stockStatus);

        } else {
            die("Error 543.");
        }
    }

    public function getStockStatusInitAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        
        if ($isAjax && strpos(Mage::app()->getRequest()->getServer('HTTP_REFERER'), Mage::getBaseUrl() ) !== false) {

            $product = Mage::getModel('catalog/product')->load($_POST["productId"]);

            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if ( (!Mage::getStoreConfig('amstockstatus/general/displayforoutonly') || !$product->isSaleable()) || ($product->isInStock() && $stockItem->getData('qty') <= Mage::helper('amstockstatus')->getBackorderQnt() ) )
            {
                if ($product->getData('hide_default_stock_status') || ($product->isInStock() && 0 >= $stockItem->getData('qty')))
                {
                    $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);

                } elseif (Mage::helper('amstockstatus')->getCustomStockStatusText($product)) {

                    if (!$product->isInStock())
                    {
                        $stockStatus = Mage::helper('amstockstatus')->__('Out of Stock') . ' - ' . Mage::helper('amstockstatus')->getCustomStockStatusText($product);

                    } else if ($product->isInStock() && $_POST["requestedQty"] > $stockItem->getData('qty')) {
                        $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);

                    } else {

                        $stockStatus = "In Stock";
                    }
                }
            }

            // Ignore custom stock status set in the db for configurable products
            // Always show In Stock
            if($stockItem->getData('type_id') == 'configurable') {
                $stockStatus = "In Stock";
            }

            $this->getResponse()->setBody($stockStatus);

        } else {
            die("Invalid Request");
        }
    }


    public function getStockStatusQtyAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        if ($isAjax) {

            $product = Mage::getModel('catalog/product')->load($_POST["productId"]);
            $qtyIn     = $_POST["qty"];

            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);

            if ( (!Mage::getStoreConfig('amstockstatus/general/displayforoutonly') || !$product->isSaleable()) || ($product->isInStock() && $stockItem->getData('qty') <= Mage::helper('amstockstatus')->getBackorderQnt() ) )
            {
                if ($product->getData('hide_default_stock_status') || ($product->isInStock() && 0 == $stockItem->getData('qty')))
                {
                    $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);

                } elseif (Mage::helper('amstockstatus')->getCustomStockStatusText($product)) {

                    if (!$product->isInStock())
                    {
                        $stockStatus = Mage::helper('amstockstatus')->__('Out of Stock') . ' - ' . Mage::helper('amstockstatus')->getCustomStockStatusText($product);
                    } else
                    {
                        $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);
                    }

                    if($qtyIn > $stockItem->getData('qty')) {
                        $stockStatus = Mage::helper('amstockstatus')->__('Quantity not in Stock') . ' - ' . Mage::helper('amstockstatus')->getCustomStockStatusText($product);
                    }
                }
            }

            if($qtyIn > $stockItem->getData('qty') && $stockItem->getData('qty') > 0) {
                $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product) . " as we only have " . round($stockItem->getData('qty')) . " available. If you would like it earlier, update your quantity.";
            } else if ($qtyIn > $stockItem->getData('qty') && $stockItem->getData('qty') <= 0) {
                $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);
            } else {
                $stockStatus = "In Stock";
	        }

            $this->getResponse()->setBody($stockStatus);

        } else {
            die("Error 543.");
        }
    }

}
