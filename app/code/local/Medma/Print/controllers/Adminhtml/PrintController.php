<?php

class Medma_Print_Adminhtml_PrintController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('print/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function printinvoiceAction() {
		
	
			//$this->loadLayout();
			//$this->renderLayout();

				
				$invoiceId = $this->getRequest()->getParam('invoice_id');
				$_invoice =  Mage::getModel('sales/order_invoice')->load($invoiceId);

				$increment_id = $_invoice->getIncrementId();

				$html=$this->getLayout()->createBlock('print/adminhtml_print')->setTemplate('print/print.phtml')->toHtml();
				require_once('Medma/Print/mpdf/mpdf.php');
				$mpdf=new mPDF('c'); 

				$mpdf->SetDisplayMode('fullpage');

				// LOAD a stylesheet
			  $cssFile = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'adminhtml/default/default/print-pdf.css';
				$stylesheet = file_get_contents($cssFile);
				$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
				$mpdf->WriteHTML($html);
				$mpdf->Output('invoice_'.$increment_id.'.pdf', 'D');
				exit;
	}

	public function printcreditmemoAction() {
		
	
			//$this->loadLayout();
			//$this->renderLayout();

				
				$creditmemoId = $this->getRequest()->getParam('creditmemo_id');
				$_creditmemo =  Mage::getModel('sales/order_creditmemo')->load($creditmemoId);

				$increment_id = $_creditmemo->getIncrementId();

				$html=$this->getLayout()->createBlock('print/adminhtml_print')->setTemplate('print/printcreditmemo.phtml')->toHtml();
				//$html= "test".$increment_id;

				require_once('Medma/Print/mpdf/mpdf.php');
				$mpdf=new mPDF('c'); 

				$mpdf->SetDisplayMode('fullpage');

				// LOAD a stylesheet
			  $cssFile = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'adminhtml/default/default/print-pdf.css';
				$stylesheet = file_get_contents($cssFile);
				$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
				$mpdf->WriteHTML($html);
				$mpdf->Output('creditmemo_'.$increment_id.'.pdf', 'D');
				exit;
	}


	public function printshipmentAction() {
		
	
			//$this->loadLayout();
			//$this->renderLayout();

				
				$shipmentId = $this->getRequest()->getParam('shipment_id');
				$_shipment =  Mage::getModel('sales/order_shipment')->load($shipmentId);

				$increment_id = $_shipment->getIncrementId();

				$html=$this->getLayout()->createBlock('print/adminhtml_print')->setTemplate('print/printshipment.phtml')->toHtml();
				//$html= "test".$increment_id;

				require_once('Medma/Print/mpdf/mpdf.php');
				$mpdf=new mPDF('c'); 

				$mpdf->SetDisplayMode('fullpage');

				// LOAD a stylesheet
			  $cssFile = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'adminhtml/default/default/print-pdf.css';
				$stylesheet = file_get_contents($cssFile);
				$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
				$mpdf->WriteHTML($html);
				$mpdf->Output('shipment_'.$increment_id.'.pdf', 'D');
				exit;
	}

	
}
