<?php
class Medma_Print_Block_Print extends Mage_Core_Block_Template
{
	/**
     * Renderers with render type key
     * block    => the block name
     * template => the template file
     * renderer => the block object
     *
     * @var array
     */
    protected $_itemRenders = array();
		public function _prepareLayout()
	  {
				return parent::_prepareLayout();
	  }
    
		 public function getPrint()     
		 { 
		    if (!$this->hasData('print')) {
		        $this->setData('print', Mage::registry('print'));
		    }
		    return $this->getData('print');
		    
		}
		public function getPaymentHtml($payment)
		{
				$paymentBlock = Mage::helper('payment')->getInfoBlock($payment)->setIsSecureMode(true);
        $paymentBlock->getMethod()->setStore($storeId);
       	$paymentBlockHtml = $paymentBlock->toHtml();
				return $paymentBlockHtml;
		}

		/*total block for invoice*/
		public function getInvoiceTotalsHtml($invoice)
    {
        $html = '';
        $totals = $this->getChild('invoice_totals');
        if ($totals) {
            $totals->setInvoice($invoice);
            $html = $totals->toHtml();
        }
        return $html;
    }
		/**
     * Add renderer for item product type
     *
     * @param   string $type
     * @param   string $block
     * @param   string $template
     * @return  Mage_Checkout_Block_Cart_Abstract
     */
		public function addItemRender($type, $block, $template)
    {
        $this->_itemRenders[$type] = array(
            'block'     => $block,
            'template'  => $template,
            'renderer'  => null
        );

        return $this;
    }
		 /**
     * Retrieve item renderer block
     *
     * @param string $type
     * @return Mage_Core_Block_Abstract
     */
		 public function getItemRenderer($type)
    {
        if (!isset($this->_itemRenders[$type])) {
            $type = 'default';
        }

        if (is_null($this->_itemRenders[$type]['renderer'])) {
            $this->_itemRenders[$type]['renderer'] = $this->getLayout()
                ->createBlock($this->_itemRenders[$type]['block'])
                ->setTemplate($this->_itemRenders[$type]['template'])
                ->setRenderedBlock($this);
        }
        return $this->_itemRenders[$type]['renderer'];
    }	
		/**
     * Return product type for quote/order item
     *
     * @param Varien_Object $item
     * @return string
     */
		protected function _getItemType(Varien_Object $item)
    {
        if ($item->getOrderItem()) {
            $type = $item->getOrderItem()->getProductType();
        } elseif ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $type = $item->getQuoteItem()->getProductType();
        } else {
            $type = $item->getProductType();
        }
        return $type;
    }
		/**
     * Prepare item before output
     *
     * @param Mage_Core_Block_Abstract $renderer
     * @return Mage_Sales_Block_Items_Abstract
     */
		protected function _prepareItem(Mage_Core_Block_Abstract $renderer)
    {
        return $this;
    }
		/**
     * Get item row html
     *
     * @param   Varien_Object $item
     * @return  string
     */
		public function getItemHtml(Varien_Object $item)
    {//echo $item;exit;
        $type = $this->_getItemType($item);

        $block = $this->getItemRenderer($type)
            ->setItem($item);
        $this->_prepareItem($block);
        return $block->toHtml();
    }
}
