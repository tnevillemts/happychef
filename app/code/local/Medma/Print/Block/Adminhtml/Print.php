<?php
class Medma_Print_Block_Adminhtml_Print extends Mage_Adminhtml_Block_Template
{


    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('print/print.phtml');

    }



		/*fetch current payment method*/
		public function getPaymentHtml($payment)
		{
				$paymentBlock = Mage::helper('payment')->getInfoBlock($payment)->setIsSecureMode(true);
        $paymentBlock->getMethod()->setStore($storeId);
       	$paymentBlockHtml = $paymentBlock->toHtml();
				return $paymentBlockHtml;
		}

}
