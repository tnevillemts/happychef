<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('shoppinglist_reminder')} ADD `count_send` int(11) NULL default '0';
ALTER TABLE {$this->getTable('shoppinglist_reminder')} ADD `interval_number`   int(11) NULL default '0';
ALTER TABLE {$this->getTable('shoppinglist_reminder')} ADD `send_number` int(11) NULL default '0'; 
");

$installer->endSetup(); 