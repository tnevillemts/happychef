<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Shoppinglist_IndexController extends Mage_Core_Controller_Front_Action
{
  public function indexAction()
  {
    if (Mage::helper('customer')->isLoggedIn()) {
      $this->loadLayout();
      $this->_initLayoutMessages('customer/session');
      $this->renderLayout();
    } else
      $this->_redirectUrl(Mage::getBaseUrl() . 'customer/account');
  }

  public function loginAction()
  {
    $params = $this->getRequest()->getParams();
    Mage::app()->getResponse()->setRedirect($params['path']);
    Mage::getSingleton('customer/session')->authenticate($this);
  }

  protected function _getProductRequest()
  {
    $requestInfo = $this->getRequest()->getParams();
    if ($requestInfo instanceof Varien_Object) {
      $request = $requestInfo;
    } elseif (is_numeric($requestInfo)) {
      $request = new Varien_Object();
      $request->setQty($requestInfo);
    } else {
      $request = new Varien_Object($requestInfo);
    }
    if (!$request->hasQty()) {
      $request->setQty(1);
    }
    return $request;
  }

  /**
   ** Load form for adding product from detail page
   **/
  public function detailformAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }
  public function createnewgroupincartAction()
  {
    //die('createNewGroupInCartAction');
    $this->loadLayout();
    $this->renderLayout();
  }
  public function cartloginAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }
  public function additemAction()
  {
    $post = $this->getRequest()->getParams();
    $now = Mage::getModel('core/date')->gmtTimestamp(now());
    $groupId = '';
    if ((isset($post['product_id'])) && ($post['product_id'] != '')) $productId = $post['product_id'];
      if ((isset($post['qty'])) && ($post['qty'] != '')) $qty = (int)$post['qty'];

      if (((isset($post['create_group'])) && ($post['create_group'] != '')) &&
        ((isset($post['customer_id'])) && ($post['customer_id'] != '')) &&
        ((isset($post['group-name'])) && ($post['group-name'] != ''))
      ) {
        
        $groupId = Mage::helper('shoppinglist/group')->saveNewGroup($post['customer_id'], $post['group-name'], 1, $post['existGroupName']);
        //die('bbbb');
        //Zend_Debug::dump($groupId);die();
        if(!$groupId) {
          /*Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Saved Cart Name already exists!'));
          $this->loadLayout();
          $this->_initLayoutMessages('customer/session');
          $this->renderLayout(); */
          $result['message'] =  '<div style="margin-top:50px;" id="messages_shoppinglist">
							<ul class="messages">
								<li class="error-msg">
									<ul>
										<li>
											<span>'.Mage::helper('shoppinglist')->__('Saved Cart Name already exists.</br> CLICK "Submit" to save this name, or </br> ENTER a new name for this Cart' ).'</span>
										</li>
									</ul>
								</li>
							</ul>
						</div>';
            $result['error'] = true; 
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
          return;
        }
      } else {
        if ((isset($post['select-group'])) && ($post['select-group'] != '')) $groupId = $post['select-group'];
      }
    try {
      

      if ((isset($post['product_id'])) && ($post['product_id'] != ''))
        $product = Mage::getModel('catalog/product')->load($post['product_id']);
      $product_type_id = $product->getTypeId();


      if (($product_type_id == 'grouped') && (isset($post['super_group'])) && ($post['super_group'] != '')) {
        $subProducts = $post['super_group'];
        foreach ($subProducts as $pId => $qty) {
          if ($qty > 0) {
            $product = Mage::getModel('catalog/product')
              ->setStoreId(Mage::app()->getStore()->getId())
              ->load($pId);
            $request = $this->_getProductRequest();
            $customOptions = $product->getTypeInstance()->prepareForCart($request, $product);
            Mage::helper('shoppinglist')->assignProductToList($groupId, $pId, $qty, $customOptions);
          }
        }
      } else {
        $product = Mage::getModel('catalog/product')
          ->setStoreId(Mage::app()->getStore()->getId())
          ->load($productId);
        $request = $this->_getProductRequest();
        $customOptions = $product->getTypeInstance()->prepareForCart($request, $product);
        Mage::helper('shoppinglist')->assignProductToList($groupId, $productId, $qty, $customOptions);
      }

      Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('The product was added to your '.Mage::helper('shoppinglist')->getGeneralTitle().'!'));
    } catch (Exception $e) {
      Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Please, try again later'));
      Zend_Debug::dump($e->getMessage());
      return;
    }
    //$this->loadLayout();
    //$this->_initLayoutMessages('customer/session');
    //$this->renderLayout();
    $result['message'] =  '<div style="margin-top:50px;" id="messages_shoppinglist">
    		<ul class="messages">
    			<li class="success-msg">
    				<ul>
    					<li>
    						<span>'.Mage::helper('shoppinglist')->__('The product successfully saved to your </br>'.Mage::helper('shoppinglist')->getGeneralTitle().'.').'</span>
    					</li>
    				</ul>
    			</li>
    		</ul>
    	</div>';
     $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
  }

  public function addAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }

  public function addGroupAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }

  public function sendRemindersAction()
  {
    if (Mage::helper('shoppinglist')->isActive()) {
      $groups = Mage::getModel('shoppinglist/group')->getCollection();
      foreach ($groups as $_group) {
        $dateFrom = new Zend_Date($_group->getUpdatedAt(), 'yyyy-MM-dd HH:m:s');
        $dateTo = new Zend_Date(now(), 'yyyy-MM-dd HH:m:s');

        $dateTo->sub($dateFrom);
        $diff = round($dateTo->getTimestamp() / (60 * 60 * 24));

        /* Get time setting */
        $sendEmailAfter = $_group->getSendReminderAfter();
        $items = Mage::getModel('shoppinglist/items')->getItemsByGroup($_group->getListId());

        if (count($items) > 0) { // The system will be send an email notification to the group has an item
          if ($diff > $sendEmailAfter) { // Check
            try {
              /* Send email */
              Mage::helper('shoppinglist')->sendEmailReminder($_group->getListId(), $_group->getListName(), $_group->getCustomerId(), $items);
              /* After send email success, update time */
              $_group->setUpdatedAt(now());
              $_group->save();

            } catch (Exception $e) {
              echo $e->getMessage();
              return;
            }
          }
        }
      }
    }
  }

  public function savelaterAction()
  {
    $isArray = $this->getRequest()->getParams(); 
    //Zend_Debug::dump($isArray['existGroupName']);die('savelaterAction');
    
    $quote = Mage::getSingleton('checkout/session')->getQuote();
    $_customer = Mage::getSingleton('customer/session')->getCustomer();
    
    $prefixName = Mage::helper('shoppinglist')->saveLaterPrefixName();
    $now = Mage::getModel('core/date')->gmtTimestamp(now());
    $groupName = $isArray['group-name'];
    $cartItems = $quote->getAllVisibleItems();
    if (count($cartItems) > 0) {
      $groupId = Mage::helper('shoppinglist/group')->saveNewGroup($_customer->getId(), $groupName, 1,$isArray['existGroupName']);
      //Zend_Debug::dump($groupId);die('savelaterAction');
      if(!$groupId) {
         $result['message'] =  '<div style="margin-top:50px;" id="messages_shoppinglist">
							<ul class="messages">
								<li class="error-msg">
									<ul>
										<li>
											<span>'.Mage::helper('shoppinglist')->__('Saved Cart Name already exists.</br> CLICK "Submit" to save this name, or </br> ENTER a new name for this Cart' ).'</span>
										</li>
									</ul>
								</li>
							</ul>
						</div>';
            $result['error'] = true; 
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            //Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Cart Name already exists!'));
            //$this->_redirectUrl(Mage::getUrl('shoppinglist/index/createnewgroupincart',array('error'=>true)));
        return; 
      }
      try {
        Mage::helper('shoppinglist')->saveCartForLater($groupId, $cartItems);
        $result['message'] =  '<div style="margin-top:50px;" id="messages_shoppinglist">
							<ul class="messages">
								<li class="success-msg">
									<ul>
										<li>
											<span>'.Mage::helper('shoppinglist')->__('All items successfully saved to your </br>'.Mage::helper('shoppinglist')->getGeneralTitle().'.').'</span>
										</li>
									</ul>
								</li>
							</ul>
						</div>';
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        return;
      } catch (Exception $e) {
         $result['message'] =  '<div style="margin-top:50px;" id="messages_shoppinglist">
							<ul class="messages">
								<li class="error-msg">
									<ul>
										<li>
											<span>'.Mage::helper('shoppinglist')->__('Unable to save cart to your shopping list'.Mage::helper('shoppinglist')->getGeneralTitle().'!').'</span>
										</li>
									</ul>
								</li>
							</ul>
						</div>';
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        return;
      }
    }
  }

  public function saveConfigAction()
  {
    if ($post = $this->getRequest()->getPost()) {
      $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
      if (isset($post['receive_email_config']) && $post['receive_email_config']) {
        // set to 1
        $reminder = Mage::getModel('shoppinglist/reminder')
          ->setReminder(1)
          ->setInterval($post['reminder_option'])
          ->setCountSend($post['count_send'])
          ->setIntervalNumber($post['interval_number'])
          ->setCustomerId($customerId)
          ->setLasttimeSend(now());
      } else {
        $reminder = Mage::getModel('shoppinglist/reminder')
          ->setReminder(0)
          ->setInterval(null)
          ->setCustomerId($customerId)
          ->setLasttimeSend(now());
      }
      if ($id = $reminder->loadByCustomerId()) {
        $reminder->setId($id);
      }
      try {
        $reminder->save();

        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('Your reminder config was successfully saved.'));
        $this->_redirect('*/');
        return;
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to set your reminder Config. Please try again.'));
        $this->_redirect('*/');
        return;
      }
    }
    $this->_redirect('*/');
    return;

  }

  public function loginpostAction()
  {
    $session = $this->_getCustomerSession();
    $login['username'] = $this->getRequest()->getParam('username');
    $login['password'] = $this->getRequest()->getParam('password');
    try {
      $session->login($login['username'], $login['password']);
      if ($session->getCustomer()->getIsJustConfirmed()) {
        echo $this->_welcomeCustomer($session->getCustomer(), TRUE);
      }
    } catch (Mage_Core_Exception $e) {
      switch ($e->getCode()) {
        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
          $value = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
          echo $message = Mage::helper('customer')->__('Account Not Confirmed', $value);
          break;
        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
          echo $message = $this->__('Invalid Email Address or Password');
          break;
        default:
          echo $message = $e->getMessage();
      }
      $session->setUsername($login['username']);
    }
    if ($session->getCustomer()->getId()) {
      echo 'login_success';
    }
  }

  private function _getCustomerSession()
  {
    return Mage::getSingleton('customer/session');
  }

  public function testAction()
  {
    $model = Mage::getModel('shoppinglist/observer')->sendReminder();
  }
}
