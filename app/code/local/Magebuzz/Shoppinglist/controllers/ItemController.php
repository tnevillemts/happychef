<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Shoppinglist_ItemController extends Mage_Core_Controller_Front_Action
{
  public function indexAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }

  public function addAction()
  {
    Mage::getSingleton('customer/session')->authenticate($this);
    $this->loadLayout();
    $this->getLayout()->getBlock('head')->setTitle(Mage::helper('shoppinglist')->__('Add Item to '.Mage::helper('shoppinglist')->getGeneralTitle()));
    $this->renderLayout();
  }

  public function loginAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }

  public function newAction()
  {
    $post = $this->getRequest()->getPost();
    $now = Mage::getModel('core/date')->gmtTimestamp(now());
    $groupId = '';
    if ($post) {
      try {
        $productId = $post['product_id'];
        $qty = (int)$post['qty'];

        if ($post['create_group']) {
          $groupId = Mage::helper('shoppinglist/group')->saveNewGroup($post['customer_id'], $post['group-name'], 1, $now);
        } else {
          $groupId = $post['select-group'];
        }
        Mage::helper('shoppinglist')->assignProductToList($groupId, $productId, $qty, $now);
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('The product was added to your '.Mage::helper('shoppinglist')->getGeneralTitle().'!'));
        $this->_redirect('*/');
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Please, try again later'));
        $this->_redirect('*/');
        return;
      }
    }
  }
}