<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Shoppinglist_GroupController extends Mage_Core_Controller_Front_Action
{
  public function publicViewAction() {
    $this->loadLayout();
    $group_id = $this->getRequest()->getParam('id');
    if ($group_id) {
      $group = Mage::getModel('shoppinglist/group')->load($group_id);
      $owner = Mage::getModel('customer/customer')->load($group->getCustomerId());
      $this->getLayout()->getBlock('head')->setTitle($this->__('%s\'s shopping list', $owner->getName()));
    }
    $this->renderLayout();
  }

  public function viewAction()
  {
    if (Mage::helper('customer')->isLoggedIn()) {
      $group_id = $this->getRequest()->getParam('id');
      if ($group_id) {
        $group = Mage::getModel('shoppinglist/group')->load($group_id);
        if ($group->getCustomerId() != Mage::helper('customer')->getCustomer()->getId()) {
          $this->_redirect('*/index/index');
          return;
        }
        Mage::register('current_group', $group);
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('shoppinglist')->getTitleTopLink().' | Group - ' . $group->getListName());
        $this->renderLayout();
        return;
      }
    } else $this->_redirectUrl(Mage::getBaseUrl() . 'shoppinglist');
  }

  public function saveAction()
  {
    $post = $this->getRequest()->getParams();
    $model = Mage::getModel('shoppinglist/group');
    $customer = Mage::getSingleton('customer/session')->getCustomer();
    $now = Mage::getModel('core/date')->gmtTimestamp(now());
    try {
      if (isset($post['group_id']) && ($post['group_id'] != '')) {
        /* Update existed group */
        $group = $model->load($post['group_id']);
        if (isset($post['group-name']) && ($post['group-name'])) $group->setListName($post['group-name']);
        if (isset($post['email-reminder']) && ($post['email-reminder'])) $model->setSendReminderAfter($post['email-reminder']);
        $group->setUpdatedAt($now);
        $group->save();
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('The group has been updated.'));
      } else {
        /* Create new group */
        $model->setCustomerId($customer->getId());
        if (isset($post['group-name']) && ($post['group-name'])) $model->setListName($post['group-name']);
        if (isset($post['email-reminder']) && ($post['email-reminder'])) $model->setSendReminderAfter($post['email-reminder']);
        $model->setStatus(1);
        $model->setCreatedAt($now);
        $model->setUpdatedAt($now);
        $model->save();
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('The group has been created.'));
      }
      $this->_redirect('*/');
    } catch (Exception $e) {
      Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Please, try again later'));
//      Mage::getSingleton('customer/session')->addError($e->getMessage());
      $this->_redirect('*/');
      return;
    }
  }

  public function editAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }

  public function newAction()
  {
    $this->_forward('edit');
  }

  public function deleteAction()
  {
    if ($this->getRequest()->getParam('id') > 0) {
      try {
        $model = Mage::getModel('shoppinglist/group');

        $model->setId($this->getRequest()->getParam('id'))
          ->delete();
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('The group was successfully deleted'));
        $this->_redirect('*/');
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to delete your group. Please, try again later'));
        $this->_redirect('*/');
        return;
      }
    }
    $this->_redirect('*/');
  }

  public function updateAction()
  {
    $post = $this->getRequest()->getParams();
    if ($post) {
      try {
        $groupId = $post['group_id'];
        $group = Mage::getModel('shoppinglist/group')->load($groupId);
        $group->setListName($post['list_name'])
          ->save();
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Please try again later.'));
        $this->_redirect('*/*/view/', array('id' => $groupId));
        return;
      }
      try {
        foreach ($post['item'] as $itemId) {
          $itemProduct = Mage::getModel('shoppinglist/items')->load($itemId['itemId']);
          if ($itemId['qty'] <= 0) {
            $itemProduct->delete();
          } else {
            $itemProduct->setQty($itemId['qty']);
            if ($itemId['select-group'] > 0) {
              $itemProduct->setListId($itemId['select-group']);
              $id = Mage::getResourceModel('shoppinglist/items')->isExisted($itemProduct);
              $itemNewGroup = Mage::getSingleton('shoppinglist/items')->load($id['item_id']);
              $itemProduct->setQty($id['qty'] + $itemProduct->getQty());
              $itemNewGroup->setData($itemProduct->getData());
              $itemNewGroup->setItemId($id['item_id']);
              $itemProduct->delete();
              $itemNewGroup->save();
            }
            $itemProduct->save();
          }

        }
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('Your '.Mage::helper('shoppinglist')->getGeneralTitle().' was successfully updated'));
        $this->_redirect('*/*/view/', array('id' => $groupId));
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Please try again later.'));
        $this->_redirect('*/*/view/', array('id' => $groupId));
        return;
      }
    } else {
      $this->_redirect('*/index/index');
    }
  }

  public function removeItemAction()
  {
    $item_ids = explode(',', $this->getRequest()->getParam('id'));
    $post = $this->getRequest()->getPost();
    $groupId = $this->getRequest()->getParam('group_id');
    if ($item_ids) {
      try {
        foreach ($item_ids as $item_id) {
          if ($item_id != '' || $item_id != null) {
            $model = Mage::getModel('shoppinglist/items');
            $model->setId($item_id)
              ->delete();
          }
        }
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('Item was successfully deleted'));
        $this->_redirect('*/*/view/', array('id' => $groupId));
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to delete this item. Please, try again later'));
        $this->_redirect('*/*/view/', array('id' => $groupId));
        return;
      }
    }
  }

  public function settingEmailAction()
  {
    $post = $this->getRequest()->getPost();
    if ($post) {
      try {
        $model = Mage::getModel('shoppinglist/group')->load($post['group_id']);
        $model->setSendReminderAfter($post['email-reminder']);
        $model->setUpdatedAt(now());
        $model->save();
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('Your setting was successfully updated'));
        $this->_redirect('*/');
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Please, try again later'));
        $this->_redirect('*/');
        return;
      }
    }
  }

  public function cartAction()
  {
    $quote = Mage::getSingleton('checkout/cart');
    $item_ids = explode(',', $this->getRequest()->getParam('items'));
    $groupId = $this->getRequest()->getParam('groupId');
    $referer_url = Mage::helper('core/http')->getHttpReferer();
    try {
      
      if ($item_ids[0] == 'all') {
        $group = Mage::getModel('shoppinglist/group')->load($groupId);
        $items = $group->getItems();
        
        if (count($items)) {
          
          foreach ($items as $item) {
            $qty = $item->getQty();
            $product = Mage::getModel('catalog/product')
              ->load($item->getProductId())
              ->setQty($qty);
            $req = unserialize($item->getBuyRequest());
            $req['qty'] = $product->getQty();
            $quote->addProduct($product, $req);
          }
          $quote->save();
          Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('shoppinglist')->__('Product was successfully added to shopping cart'));
          $this->_redirectUrl(Mage::getUrl('checkout/cart'));
          return;
        }
      } else {
        if ($item_ids) {
          foreach ($item_ids as $item_id) {
            if ($item_id != '' || $item_id != null) {
              $item = Mage::getModel('shoppinglist/items')->load($item_id);
              $qty = $item->getQty();
              $product = Mage::getModel('catalog/product')
                ->load($item->getProductId())
                ->setQty($qty);
              //  max(0.01) ;
              $req = unserialize($item->getBuyRequest());
              $req['qty'] = $product->getQty();
              $quote->addProduct($product, $req);
            }
          }
          $quote->save();
          Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('shoppinglist')->__('Product was successfully added to shopping cart'));
          $this->_redirect('checkout/cart');
          return;
        }
      }
    } catch (Exception $e) {
      Mage::getSingleton('core/session')->addError(Mage::helper('shoppinglist')->__('There was problem when adding product to cart. Please try again later.'));
      Mage::getSingleton('core/session')->addError($e->getMessage());
      $this->_redirectUrl($referer_url);
      return;
    }
  }

  /*
  * update module version 1.3
  * Author : Xboy
  * function : sendmailAction
  * show popup send mail
  */
  public function sendmailAction()
  {
    $this->loadLayout();
    $this->renderLayout();
  }

  /* function : sendmail_postAction 
  *  note : save data and send mail
  */
  public function sendmail_postAction()
  {
    $postData = $this->getRequest()->getParams();
    if (count($postData) > 0) {
      if (Mage::getSingleton("customer/session")->isLoggedIn()) {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        // call function sendEmailToFriends in helper Data
        if ((isset($postData['group_id']))&&($postData['email_friends'])&&($postData['name_friends'])&&($postData['text_massage']))
          $sendMail = Mage::helper('shoppinglist')->sendEmailToFriends($customer->getId(), $postData['group_id'], $postData['email_friends'], $postData['name_friends'], $postData['text_massage']);
        if ($sendMail) {
          Mage::getSingleton('customer/session')->addSuccess(Mage::helper('shoppinglist')->__('Send mail was successfully'));
        } else {
          Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('There was problem when send email to friends. Please try again later.'));
        }
      }
    }
    $this->loadLayout();
    $this->_initLayoutMessages('customer/session');
    $this->renderLayout();
  }
  /* end update version 1.3 */

}