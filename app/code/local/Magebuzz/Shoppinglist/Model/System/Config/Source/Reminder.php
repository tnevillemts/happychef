<?php

class Magebuzz_Shoppinglist_Model_System_Config_Source_Reminder extends Varien_Object
{
  public function toOptionArray()
  {
    return array(
      array('value' => 'hours', 'label' => 'Hours'),
      array('value' => 'daily', 'label' => 'Days'),
      array('value' => 'weekly', 'label' => 'Weeks'),
      array('value' => 'monthly', 'label' => 'Months')
    );
  }
}