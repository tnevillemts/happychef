<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Shoppinglist_Model_Observer
{
  public function controller_action_predispatch_customer_account_login($observer)
  {
    $request = Mage::app()->getRequest();
    $module = $request->getModuleName();
    $controller = $request->getControllerName();
    if ($module == 'shoppinglist') {
      if (Mage::helper('customer')->isLoggedIn()) {
        return;
      } else {
        Mage::getSingleton('customer/session')->setBeforeAuthUrl($request->getRequestUri());
        header("Status: 301");
        header('Location: ' . Mage::helper('customer')->getLoginUrl()); // send to the login page
      }
    }
  }

  public function sendReminder()
  {

    $reminders = Mage::getModel('shoppinglist/reminder')->getCollection()
      ->addFieldToFilter('reminder', 1);

    if (count($reminders)) {
      foreach ($reminders as $reminder) {
        $dateFrom = new Zend_Date($reminder->getLasttimeSend(), 'yyyy-MM-dd HH:m:s');
        $dateTo = new Zend_Date(now(), 'yyyy-MM-dd HH:m:s');
        $dateTo->sub($dateFrom);
        $hoursLoad = $this->getInterVarByReminder($reminder);
        $diff = round($dateTo->getTimestamp() / $hoursLoad);
        if ($diff >= $reminder->getIntervalNumber()) {
          if ($reminder->getSendNumber() < $reminder->getCountSend() || $reminder->getCountSend() == 0 || is_null($reminder->getCountSend())) {
            Mage::helper('shoppinglist')->sendEmailReminder($reminder->getCustomerId());
            $reminder->setSendNumber($reminder->getSendNumber() + 1);
            $reminder->setLasttimeSend(now());
            $reminder->save();
          }
        }
        continue;
      }
    }
  }

  public function getInterVarByReminder($reminder)
  {
    switch ($reminder->getInterval()) {
      case 'daily':
        return 24 * 60 * 60;
        break;
      case 'weekly':
        return (24 * 7 * 60 * 60);
        break;
      case 'monthly':
        echo "i equals 2";
        break;
      case 'hours':
        return 60 * 60;
        break;
    }
  }

  public function addTabShoppinglistToCustomer($observer)
  {

    $block = $observer->getEvent()->getBlock();
    if ($this->_helperShoppinglist()->isActive()) {
      if ($block instanceof Mage_Adminhtml_Block_Customer_Edit_Tabs) {
        if ($this->_getRequest()->getActionName() == 'edit' || $this->_getRequest()->getParam('type')) {
          $block->addTab('shopping_list', array(
            'label' => Mage::helper('shoppinglist')->__('Shopping List'),
            'title' => Mage::helper('shoppinglist')->__('Shopping List'),
            'url'   => $block->getUrl('shoppinglist/adminhtml_customer/index', array('_current' => TRUE)),
            'class' => 'ajax',
          ));
        }
      }
    }
  }

  protected function _helperShoppinglist()
  {
    return Mage::helper('shoppinglist');
  }

  protected function _getRequest()
  {
    return Mage::app()->getRequest();
  }
}