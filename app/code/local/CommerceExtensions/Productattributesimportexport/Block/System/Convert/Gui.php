<?php

class CommerceExtensions_Productattributesimportexport_Block_System_Convert_Gui extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'system_convert_gui';
        $this->_blockGroup = 'productattributesimportexport';
        
        $this->_headerText = Mage::helper('productattributesimportexport')->__('Profiles');
        $this->_addButtonLabel = Mage::helper('productattributesimportexport')->__('Add New Profile');

        parent::__construct();
    }
}