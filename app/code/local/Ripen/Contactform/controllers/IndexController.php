<?php
class Ripen_Contactform_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$this->loadLayout();
        $this->getLayout()->getBlock('contactform')->setFormAction( Mage::getUrl('*/*/post') );
        $this->getLayout()->getBlock('head')->setTitle('Happy Chef Contact Form');
		$this->renderLayout();

    }

    public function postAction()
    {
        if ($this->getRequest()->isPost()) {
            $request = Mage::getModel('contactform/contactform');
            date_default_timezone_set('America/New_York');
            $data['time_added'] = now();

            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $data['hostname'] = $_SERVER["HTTP_HOST"];

            $parms = $this->getRequest()->getPost();
            $fields = [
                'first_name',
                'last_name',
                'email',
                'send_offers',
                'phone',
                'message',
                'contact_type',
                'confirmation_number'
            ];
            foreach ($fields as $field) {
                if (isset($parms[$field])) $data[$field] = $parms[$field];
            }

            // TODO: Figure out why field is named like this.
            if (isset($parms['email'])) $data['emailx'] = $parms['email'];

            // Validate
            if (!$errors = $request->validate($data)) {
                MAGE::getSingleton('core/session')->addError($errors);
            }

            // Add to database
            try {
                $request->setData($data)->save();

                $templateCode = "Contact Form Custom";
                $emailTemplate = Mage::getModel('core/email_template')->loadByCode($templateCode);

                $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email', 1));
                $emailTemplate->setSenderName($data['first_name'] . " " . $data['last_name']);
                $emailTemplate->setReplyTo($data['email']);

                $recipient = Mage::getStoreConfig('trans_email/ident_support/email', 1);
                $emailTemplate->send($recipient, "Happy Chef", $data);

                echo "Thank you! We will get back to you shortly!";
            } catch (Exception $e) {
                echo "Sorry, we've had some trouble sending your request. Please try again.";
            }
        }
    }
}
