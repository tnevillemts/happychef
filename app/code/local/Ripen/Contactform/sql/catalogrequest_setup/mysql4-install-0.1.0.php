<?php

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('contactform')} (
  `contactform_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contact_type` varchar(50) NOT NULL DEFAULT '',
  `confirmation_number` varchar(50) NOT NULL DEFAULT '',
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `time_added` datetime NOT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `hostname` varchar(100) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`contactform_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

    ");

$installer->endSetup(); 