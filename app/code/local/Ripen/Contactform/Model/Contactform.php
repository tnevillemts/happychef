<?php

class Ripen_Contactform_Model_Contactform extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('contactform/contactform');
    }

    public function validate(array $data)
    {
        $errors = array();
        if (!Zend_Validate::is($data['first_name'], 'NotEmpty')) {
            $errors[] = 'First name cannot be empty';
        }
        if (!Zend_Validate::is($data['last_name'], 'NotEmpty')) {
            $errors[] = 'Last name cannot be empty';
        }

        $v = new Zend_Validate_EmailAddress();
        if (!$v->isValid($data['email'])) {
            $errors[] = 'Email is not valid';
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }
    
}