<?php

class Ripen_Contactform_Model_Status extends Varien_Object
{
    const STATUS_PENDING	= 1;
    const STATUS_PROCESSED	= 2;

    static public function getOptionArray()
    {

        return array(
            self::STATUS_PENDING    => Mage::helper('contactform')->__('Pending'),
            self::STATUS_PROCESSED   => Mage::helper('contactform')->__('Processed')
        );

    }
}