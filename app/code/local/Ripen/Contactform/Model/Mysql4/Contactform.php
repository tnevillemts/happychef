<?php

class Ripen_Contactform_Model_Mysql4_Contactform extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the contactform_id refers to the key field in your database table.
        $this->_init('contactform/contactform', 'contactform_id');
    }
}