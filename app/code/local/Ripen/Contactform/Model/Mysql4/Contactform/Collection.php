<?php

class Ripen_Contactform_Model_Mysql4_Contactform_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('contactform/contactform');
    }
}