<?php
class Ripen_Contactform_Block_Contactform extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getContactform()
     { 
        if (!$this->hasData('contactform')) {
            $this->setData('contactform', Mage::registry('contactform'));
        }
        return $this->getData('contactform');
        
    }

    public function getFormActionUrl()
    {
        return $this->getUrl('contactform/index/post', array('_secure' => false));
    }

}