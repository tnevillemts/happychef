<?php
class Ripen_Contactform_Block_Adminhtml_Contactform extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_contactform';
        $this->_blockGroup = 'contactform';
        $this->_headerText = Mage::helper('contactform')->__('Contact Form Entries');
        $this->_addButtonLabel = Mage::helper('contactform')->__('Add Item');
        parent::__construct();
    }
}
