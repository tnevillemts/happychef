<?php

class Ripen_Contactform_Block_Adminhtml_Contactform_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('contactformGrid');
        $this->setDefaultSort('contactform_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('contactform/contactform')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('contactform_id', array(
            'header'    => Mage::helper('contactform')->__('ID'),
            'align'     => 'right',
            'width'     => '50px',
            'index'     => 'contactform_id',
        ));

        $this->addColumn('first_name', array(
            'header'    => Mage::helper('contactform')->__('First Name'),
            'align'     => 'left',
            'index'     => 'first_name',
        ));

        $this->addColumn('last_name', array(
            'header'    => Mage::helper('contactform')->__('Last Name'),
            'align'     => 'left',
            'index'     => 'last_name',
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('contactform')->__('Email'),
            'align'     => 'left',
            'index'     => 'email',
        ));

        $this->addColumn('contact_type', array(
            'header'    => Mage::helper('contactform')->__('Inquiry Type'),
            'align'     => 'left',
            'index'     => 'contact_type',
        ));

        $this->addColumn('message', array(
            'header'    => Mage::helper('contactform')->__('Message'),
            'align'     => 'left',
            'index'     => 'message',
        ));

        $this->addColumn('last_name', array(
            'header'    => Mage::helper('contactform')->__('Last Name'),
            'align'     => 'left',
            'index'     => 'last_name',
        ));


        $this->addColumn('time_added', array(
            'header'    => Mage::helper('contactform')->__('Created At'),
            'align'     => 'left',
            'index'     => 'time_added',
            'type'      => 'datetime',
            'width'     => '160px',
        ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('contactform')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('contactform')->__('View'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('contactform')->__('CSV'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('contactform_id');
        $this->getMassactionBlock()->setFormFieldName('contactform');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('contactform')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('contactform')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('contactform/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('contactform')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('contactform')->__('Status'),
                    'values' => $statuses
                )
             )
        ));
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
