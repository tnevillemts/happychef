<?php

class Ripen_Contactform_Block_Adminhtml_Contactform_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('contactform_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('contactform')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('contactform')->__('Item Information'),
          'title'     => Mage::helper('contactform')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('contactform/adminhtml_contactform_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}