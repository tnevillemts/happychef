<?php

class Ripen_Contactform_Block_Adminhtml_Contactform_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'contactform';
        $this->_controller = 'adminhtml_contactform';
        
        $this->_updateButton('save', 'label', Mage::helper('contactform')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('contactform')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('contactform_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'contactform_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'contactform_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('contactform_data') && Mage::registry('contactform_data')->getId() ) {
            return Mage::helper('contactform')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('contactform_data')->getId()));
        } else {
            return Mage::helper('contactform')->__('Add Item');
        }
    }
}