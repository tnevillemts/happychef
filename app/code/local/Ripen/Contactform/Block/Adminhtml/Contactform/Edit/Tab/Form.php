<?php

class Ripen_Contactform_Block_Adminhtml_Contactform_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('contactform_form', array('legend'=>Mage::helper('contactform')->__('Item information')));
     
      $fieldset->addField('first_name', 'text', array(
          'label'     => Mage::helper('contactform')->__('First Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'first_name',
      ));

      $fieldset->addField('last_name', 'text', array(
          'label'     => Mage::helper('contactform')->__('Last Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'last_name',
      ));

      $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('contactform')->__('Email'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'email',
      ));


      $fieldset->addField('phone', 'text', array(
          'label'     => Mage::helper('contactform')->__('Phone'),
          'required'  => true,
          'name'      => 'phone',
      ));


      $fieldset->addField('message', 'textarea', array(
          'label'     => Mage::helper('contactform')->__('Message'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'message',
      ));

      $fieldset->addField('confirmation_number', 'text', array(
          'label'     => Mage::helper('contactform')->__('Order Number'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'confirmation_number',
      ));

      $fieldset->addField('contact_type', 'text', array(
          'label'     => Mage::helper('contactform')->__('Subject'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'contact_type',
      ));

      $fieldset->addField('time_added', 'text', array(
          'label'     => Mage::helper('contactform')->__('Time Added'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'time_added',
      ));

      $fieldset->addField('ip', 'text', array(
          'label'     => Mage::helper('contactform')->__('IP'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'ip',
      ));

      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('contactform')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('contactform')->__('Pending'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('contactform')->__('Processed'),
              ),
          ),
      ));

     
      if ( Mage::getSingleton('adminhtml/session')->getContactformData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getContactformData());
          Mage::getSingleton('adminhtml/session')->setContactformData(null);
      } elseif ( Mage::registry('contactform_data') ) {
          $form->setValues(Mage::registry('contactform_data')->getData());
      }
      return parent::_prepareForm();
  }
}