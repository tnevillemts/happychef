<?php

/**
 * Config Extensions
 */
class Ripen_All_Block_Adminhtml_System_Config_Extensions extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _prepareLayout() {

        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('ripen_all/adminhtml/installed_extensions.phtml');
        }
        return $this;
    }

    /**
     * Unset some non-related element parameters
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element) {

        $this->addData(array('rows'	=> Mage::helper('ripen_all')->getExtensions()));
        $this->setScriptPath(Mage::getBaseDir('design'));
        return $this->fetchView($this->getTemplateFile());
    }

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {


        return $this->_toHtml();
    }
}