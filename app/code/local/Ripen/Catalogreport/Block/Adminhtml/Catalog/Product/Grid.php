<?php

class Ripen_Catalogreport_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('product_filter');
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection()
    {
        $store = $this->_getStore();
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('type_id')
            ->addAttributeToSelect('department')
            ->addAttributeToSelect('tailored')
            ->addAttributeToSelect('fabric')
            ->addAttributeToSelect('accessories')
            ->addAttributeToSelect('colorgroup')

            ->addAttributeToSelect('apron_style')
            ->addAttributeToSelect('coat_style')
            ->addAttributeToSelect('footwear_style')
            ->addAttributeToSelect('headwear_style')
            ->addAttributeToSelect('pant_style_2')
            ->addAttributeToSelect('knife_style')
            ->addAttributeToSelect('shirt_style')
            ->addAttributeToSelect('shorts_style')
            ->addAttributeToSelect('skirt_style')
            ->addAttributeToSelect('tie_style')
            ->addAttributeToSelect('vest_style')
            ->addAttributeToSelect('narrow_results_by')
            ->addAttributeToSelect('search_keywords')
        ;

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');
        }
        if ($store->getId()) {
            //$collection->setStoreId($store->getId());
            $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
            $collection->addStoreFilter($store);
            $collection->joinAttribute(
                'name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $adminStore
            );
            $collection->joinAttribute(
                'custom_name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'visibility',
                'catalog_product/visibility',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'price',
                'catalog_product/price',
                'entity_id',
                null,
                'left',
                $store->getId()
            );

            $collection->joinAttribute(
                'price',
                'catalog_product/price',
                'entity_id',
                null,
                'left',
                $store->getId()
            );
        }
        else {

            $collection->addAttributeToSelect('price');
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
            $collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
            $collection->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

        }

        $this->setCollection($collection);

        parent::_prepareCollection();
        $this->getCollection()->addWebsiteNamesToResult();
        return $this;
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField('websites',
                    'catalog/product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left');
            }
        }
        return parent::_addColumnFilterToCollection($column);
    }

    protected function _prepareOptionsArray($attributeCode) {

        $items = Mage::getModel('eav/entity_attribute_option')->getCollection()->setStoreFilter()->join('attribute','attribute.attribute_id=main_table.attribute_id', 'attribute_code');
        foreach ($items as $item) :
            if ($item->getAttributeCode() == $attributeCode) {
                $options[$item->getOptionId()] = $item->getValue();
            }
        endforeach;

        return $options;

    }


    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header'=> Mage::helper('catalog')->__('id'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'entity_id',
        ));
        $this->addColumn('name',
            array(
                'header'=> Mage::helper('catalog')->__('Name'),
                'index' => 'name',
        ));

        $store = $this->_getStore();
        if ($store->getId()) {
            $this->addColumn('custom_name',
                array(
                    'header'=> Mage::helper('catalog')->__('Name in %s', $store->getName()),
                    'index' => 'custom_name',
            ));
        }

        $this->addColumn('type',
            array(
                'header'=> Mage::helper('catalog')->__('Type'),
                'width' => '60px',
                'index' => 'type_id',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));


        $this->addColumn('sku',
            array(
                'header'=> Mage::helper('catalog')->__('SKU'),
                'width' => '80px',
                'index' => 'sku',
        ));

        $store = $this->_getStore();
        $this->addColumn('price',
            array(
                'header'=> Mage::helper('catalog')->__('Price'),
                'type'  => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
        ));

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $this->addColumn('qty',
                array(
                    'header'=> Mage::helper('catalog')->__('Qty'),
                    'width' => '100px',
                    'type'  => 'number',
                    'index' => 'qty',
            ));
        }

        /*
        $this->addColumn('status',
            array(
                'header'=> Mage::helper('catalog')->__('Status'),
                'width' => '70px',
                'index' => 'status',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));
        */
        
        $categories = array();
        foreach ($this->load_tree() as $category) {
            $categories[$category['value']] =  $category['label'];
        }

        $this->addColumn('subcategory',
            array(
                'header'=> Mage::helper('catalog')->__('Category/Position'),
                'width' => '100px',
                'index' => 'subcategory',
                'type'  => 'options',
                'options'   => $categories,
                'renderer'  => 'Ripen_Catalogreport_Block_Adminhtml_Catalog_Product_Renderer_Category',
                'filter_condition_callback' => array($this, '_categoryFilter'),
            )
        );
/*
        $styleOptions =
            $this->_prepareOptionsArray('apron_style') +
            $this->_prepareOptionsArray('coat_style') +
            $this->_prepareOptionsArray('footwear_style') +
            $this->_prepareOptionsArray('headwear_style') +
            $this->_prepareOptionsArray('pant_style_2') +
            $this->_prepareOptionsArray('knife_style') +
            $this->_prepareOptionsArray('shirt_style') +
            $this->_prepareOptionsArray('shorts_style') +
            $this->_prepareOptionsArray('skirt_style') +
            $this->_prepareOptionsArray('tie_style') +
            $this->_prepareOptionsArray('vest_style')
        ;
*/

        $this->addColumn('style',
            array(
                'header'=> Mage::helper('catalog')->__('Style'),
                'width' => '70px',
                'type'  => 'options',
                'renderer'  => 'Ripen_Catalogreport_Block_Adminhtml_Catalog_Product_Renderer_Style',
                'options' => $styleOptions,
                'filter_condition_callback' => array($this, '_styleFilter'),
            )
        );

        $this->addColumn('customizable',
            array(
                'header'=> Mage::helper('catalog')->__('Custom'),
                'width' => '50px',
                'index' => 'narrow_results_by',
                'type'  => 'options',
                'renderer'  => 'Ripen_Catalogreport_Block_Adminhtml_Catalog_Product_Renderer_Customizable',
                'options' => array(0=>"Yes", 1=>"No"),
                'filter_condition_callback' => array($this, '_customizeFilter'),
            )
        );

        $this->addColumn('isnew',
            array(
                'header'=> Mage::helper('catalog')->__('New'),
                'width' => '50px',
                'index' => 'narrow_results_by',
                'type'  => 'options',
                'renderer'  => 'Ripen_Catalogreport_Block_Adminhtml_Catalog_Product_Renderer_Isnew',
                'options' => array(0=>"Yes", 1=>"No"),
                'filter_condition_callback' => array($this, '_isnewFilter'),
            )
        );

        $this->addColumn('issale',
            array(
                'header'=> Mage::helper('catalog')->__('Sale'),
                'width' => '50px',
                'index' => 'narrow_results_by',
                'type'  => 'options',
                'renderer'  => 'Ripen_Catalogreport_Block_Adminhtml_Catalog_Product_Renderer_Issale',
                'options' => array(0=>"Yes", 1=>"No"),
                'filter_condition_callback' => array($this, '_issaleFilter'),
            )
        );

        $this->addColumn('colors',
            array(
                'header'=> Mage::helper('catalog')->__('Colors'),
                'width' => '70px',
                'index' => 'colorgroup',
                'type'  => 'options',
                'options' => $this->_prepareOptionsArray('colorgroup'),
            )
        );

        $this->addColumn('accessories',
            array(
                'header'=> Mage::helper('catalog')->__('Accessories'),
                'width' => '70px',
                'index' => 'accessories',
                'type'  => 'options',
                'options' => $this->_prepareOptionsArray('accessories'),
            )
        );

        $this->addColumn('fabric',
            array(
                'header'=> Mage::helper('catalog')->__('Fabric'),
                'width' => '70px',
                'index' => 'fabric',
                'type'  => 'options',
                'options' => $this->_prepareOptionsArray('fabric'),
            )
        );


        $this->addColumn('tailored',
            array(
                'header'=> Mage::helper('catalog')->__('Tailored For'),
                'width' => '70px',
                'index' => 'tailored',
                'type'  => 'options',
                'options' => $this->_prepareOptionsArray('tailored'),
            )
        );

        $this->addColumn('department',
            array(
                'header'=> Mage::helper('catalog')->__('Department'),
                'width' => '70px',
                'type'  => 'options',
                'index' => 'department',
                'options' => $this->_prepareOptionsArray('department'),

            )
        );

        $this->addColumn('search_keywords',
            array(
                'header'=> Mage::helper('catalog')->__('Keywords'),
                'width' => '80px',
                'index' => 'search_keywords',
            ));

        $this->addExportType('*/*/exportCsv',
            Mage::helper('catalogreport')->__('CSV'));

        return parent::_prepareColumns();
    }

    public function buildCategoriesMultiselectValues(Varien_Data_Tree_Node $node, $values, $level = 0)
    {
        $level++;

        $values[$node->getId()]['value'] =  $node->getId();
        $values[$node->getId()]['label'] = str_repeat("--", $level) . $node->getName();

        foreach ($node->getChildren() as $child)
        {
            $values = $this->buildCategoriesMultiselectValues($child, $values, $level);
        }

        return $values;
    }

    public function load_tree()
    {
        $store = Mage::app()->getFrontController()->getRequest()->getParam('store', 0);
        $parentId = $store ? Mage::app()->getStore($store)->getRootCategoryId() : 1;  // Current store root category

        $tree = Mage::getResourceSingleton('catalog/category_tree')->load();

        $root = $tree->getNodeById($parentId);

        if($root && $root->getId() == 1)
        {
            $root->setName(Mage::helper('catalog')->__('Root'));
        }

        $collection = Mage::getModel('catalog/category')->getCollection()
            ->setStoreId($store)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('is_active');

        $tree->addCollectionData($collection, true);

        return $this->buildCategoriesMultiselectValues($root, array());
    }

    protected function _customizeFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->getSelect()->where(
            "sales_flat_order_address.city like ?
            OR sales_flat_order_address.street like ?
            OR sales_flat_order_address.postcode like ?"
            , "%$value%");


        return $this;
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> Mage::helper('catalog')->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
             'confirm' => Mage::helper('catalog')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('catalog/product_status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('catalog')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('catalog')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('catalog/update_attributes')){
            $this->getMassactionBlock()->addItem('attributes', array(
                'label' => Mage::helper('catalog')->__('Update Attributes'),
                'url'   => $this->getUrl('*/catalog_product_action_attribute/edit', array('_current'=>true))
            ));
        }

        Mage::dispatchEvent('adminhtml_catalog_product_grid_prepare_massaction', array('block' => $this));
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/catalog_product/edit', array(
            'store'=>$this->getRequest()->getParam('store'),
            'id'=>$row->getId())
        );
    }
}
