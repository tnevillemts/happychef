<?php
class Ripen_Catalogreport_Block_Adminhtml_Catalog_Product_Renderer_Issale extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        $valueArr = explode(",", $value);
        return in_array(827, $valueArr) ? "Yes" : "";
    }
}