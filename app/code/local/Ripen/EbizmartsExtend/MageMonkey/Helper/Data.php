<?php

/**
 * Overriding three functions for PROD-864 
 *
 */

class Ripen_EbizmartsExtend_MageMonkey_Helper_Data extends Ebizmarts_MageMonkey_Helper_Data 
{
    public function getMergeVars($customer, $includeEmail = FALSE, $websiteId = NULL, $includeOptin = FALSE)
    {
        $merge_vars = array();
        $maps = $this->getMergeMaps($customer->getStoreId());

        if (!$maps) {
            return;
        }

        $request = Mage::app()->getRequest();

        //Add Customer data to Subscriber if is Newsletter_Subscriber is Customer
        if (!$customer->getDefaultShipping() && $customer->getEntityId()) {
            $customer->addData(Mage::getModel('customer/customer')->load($customer->getEntityId())
                ->setStoreId($customer->getStoreId())
                ->toArray());
        } elseif ($customer->getCustomerId()) {
            $customer->addData(Mage::getModel('customer/customer')->load($customer->getCustomerId())
                ->setStoreId($customer->getStoreId())
                ->toArray());
        }

        $this->_setMaps($maps,$customer,$merge_vars, $websiteId);

        //GUEST
        if (!$customer->getId() && !$request->getPost('firstname')) {
            $guestFirstName = $this->config('guest_name', $customer->getStoreId());

            if ($guestFirstName) {
                $merge_vars['FNAME'] = $guestFirstName;
            }
        }
        if (!$customer->getId() && !$request->getPost('lastname')) {
            $guestLastName = $this->config('guest_lastname', $customer->getStoreId());

            if ($guestLastName) {
                $merge_vars['LNAME'] = $guestLastName;
            }
        }
        //GUEST

        if ($includeEmail) {
            $merge_vars['EMAIL'] = $customer->getEmail();
        }

	// PROD-864
	if ($includeOptin) {
	    $optin = '';
	    $moduleName = Mage::app()->getRequest()->getModuleName();
	    if($moduleName == 'customer') {
		$optin = 'Account';
	    } else if ($moduleName == 'newsletter') {
		$optin = 'Email';
	    } else if ($moduleName == 'catalogrequest') {
		$optin = 'Catalog';
	    } else if ($moduleName == 'dynamicoffers') {
		$optin = 'Promo';
	    } else if ($moduleName == 'bettercheckout' || $moduleName == 'checkout') {
		$optin = 'Order';
	    } else {
		//$optin = 'None';
		$optin = $moduleName;
	    }
	    $merge_vars['OPTIN'] = $optin;
	}

        $groups = $customer->getListGroups();
        $groupings = array();

        if (is_array($groups) && count($groups)) {
            foreach ($groups as $groupId => $grupoptions) {
                if (is_array($grupoptions)) {
                    $grupOptionsEscaped = array();
                    foreach ($grupoptions as $gopt) {
                        $gopt = str_replace(",", "%C%", $gopt);
                        $grupOptionsEscaped[] = $gopt;
                    }
                    $groupings[] = array(
                        'id' => $groupId,
                        'groups' => str_replace('%C%', '\\,', implode(', ', $grupOptionsEscaped))
                    );
                } else {
                    $groupings[] = array(
                        'id' => $groupId,
                        'groups' => str_replace(',', '\\,', $grupoptions)
                    );
                }
            }
        }

        $merge_vars['GROUPINGS'] = $groupings;

        //magemonkey_mergevars_after
        $blank = new Varien_Object;
        Mage::dispatchEvent('magemonkey_mergevars_after',
            array('vars' => $merge_vars, 'customer' => $customer, 'newvars' => $blank));
        if ($blank->hasData()) {
            $merge_vars = array_merge($merge_vars, $blank->toArray());
        }
        //magemonkey_mergevars_after

        return $merge_vars;
    }

    private function _setMaps($maps,$customer,$merge_vars, $websiteId)
    {
        foreach ($maps as $map) {
            $request = Mage::app()->getRequest();

            $customAtt = $map['magento'];
            $chimpTag = $map['mailchimp'];

            if ($chimpTag && $customAtt) {

                $key = strtoupper($chimpTag);

                switch ($customAtt) {
                    case 'gender':
                        $val = (int)$customer->getData(strtolower($customAtt));
                        if ($val == 1) {
                            $merge_vars[$key] = 'Male';
                        } elseif ($val == 2) {
                            $merge_vars[$key] = 'Female';
                        }
                        break;
                    case 'dob':
                        $dob = (string)$customer->getData(strtolower($customAtt));
                        if ($dob) {
                            $merge_vars[$key] = (substr($dob, 5, 2) . '/' . substr($dob, 8, 2));
                        }
                        break;
                    case 'billing_address':
                    case 'shipping_address':
                        $this->_setAddress($customAtt,$merge_vars, $customer, $key);

                        break;
                    case 'date_of_purchase':

                        $last_order = Mage::getResourceModel('sales/order_collection')
                            ->addFieldToFilter('customer_email', $customer->getEmail())
                            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
                            ->setOrder('created_at', 'desc')
                            ->setPageSize(1)
                            ->getFirstItem();
                        if ($last_order->getId()) {
                            $merge_vars[$key] = date('m/d/Y', strtotime($last_order->getCreatedAt()));
                        }

                        break;
                    case 'ee_customer_balance':
    		        $merge_vars[$key] = '';

                        if ($this->isEnterprise() && $customer->getId()) {

                            $_customer = Mage::getModel('customer/customer')->load($customer->getId());
                            if ($_customer->getId()) {
                                if (Mage::app()->getStore()->isAdmin()) {
                                    $websiteId = is_null($websiteId) ? Mage::app()->getStore()->getWebsiteId() : $websiteId;
                                }

                                $balance = Mage::getModel('enterprise_customerbalance/balance')
                                    ->setWebsiteId($websiteId)
                                    ->setCustomerId($_customer->getId())
                                    ->loadByCustomer();

                                $merge_vars[$key] = $balance->getAmount();
                            }

                        }

                        break;
                    case 'group_id':
                        $group_id = (int)$customer->getData(strtolower($customAtt));
                        $customerGroup = Mage::helper('customer')->getGroups()->toOptionHash();
                        if ($group_id == 0) {
                            $merge_vars[$key] = 'NOT LOGGED IN';
                        } else {
                            $merge_vars[$key] = $customerGroup[$group_id];
                        }
                        break;
                    default:

                        if (($value = (string)$customer->getData(strtolower($customAtt)))
                            OR ($value = (string)$request->getPost(strtolower($customAtt)))
                        ) {
                            $merge_vars[$key] = $value;
                        }

                        break;
                }
            }
        }
    }

    public function mergeVars($object = NULL, $includeEmail = FALSE, $currentList = NULL, $includeOptin = FALSE)
    {
        //Initialize as GUEST customer
        $customer = new Varien_Object;

        $regCustomer = Mage::registry('current_customer');
        $guestCustomer = Mage::registry('mc_guest_customer');

        if (Mage::helper('customer')->isLoggedIn()) {
            $customer = Mage::helper('customer')->getCustomer();
        } elseif ($regCustomer) {
            $customer = $regCustomer;
        } elseif ($guestCustomer) {
            $customer = $guestCustomer;
        } else {
            if (is_null($object)) {
                $customer->setEmail($object->getSubscriberEmail())
                    ->setStoreId($object->getStoreId());
            } else {
                $customer = $object;
            }

        }

        if (is_object($object)) {
            if ($object->getListGroups()) {
                $customer->setListGroups($object->getListGroups());
            }

            if ($object->getMcListId()) {
                $customer->setMcListId($object->getMcListId());
            }
        }

        // PROD-864
        $mergeVars = Mage::helper('monkey')->getMergeVars($customer, $includeEmail, NULL, $includeOptin);
        // add groups
        $monkeyPost = Mage::getSingleton('core/session')->getMonkeyPost();
        $request = Mage::app()->getRequest();
        $post = $request->getPost();
        if ($monkeyPost) {
            $post = unserialize($monkeyPost);
        }
        //if post exists && is not admin backend subscription && not footer subscription
        $this->_checkGrouping($mergeVars,$post,$currentList, $object);


        return $mergeVars;
    }

    private function _checkGrouping($merge_vars,$post,$currentList, $object)
    {
        $request = Mage::app()->getRequest();
        $adminSubscription = $request->getActionName() == 'save' && $request->getControllerName() == 'customer' && $request->getModuleName() == (string)Mage::getConfig()->getNode('admin/routers/adminhtml/args/frontName');
        $footerSubscription = $request->getActionName() == 'new' && $request->getControllerName() == 'subscriber' && $request->getModuleName() == 'newsletter';
        $customerSubscription = $request->getActionName() == 'saveadditional';
        $customerCreateAccountSubscription = $request->getActionName() == 'createpost';
        if ($post && !$adminSubscription && !$customerSubscription && !$customerCreateAccountSubscription || Mage::getSingleton('core/session')->getIsOneStepCheckout()) {
            $defaultList = Mage::helper('monkey')->config('list');
            //if can change customer set the groups set by customer else set the groups on MailChimp config
            $canChangeGroups = Mage::getStoreConfig('monkey/general/changecustomergroup', $object->getStoreId());
            if ($currentList && ($currentList != $defaultList || $canChangeGroups && !$footerSubscription) && isset($post['list'][$currentList])) {
                $subscribeGroups = array(0 => array());
                foreach ($post['list'][$currentList] as $toGroups => $value) {
                    if (is_numeric($toGroups)) {
                        $subscribeGroups[0]['id'] = $toGroups;
                        $subscribeGroups[0]['groups'] = implode(', ', array_unique($post['list'][$currentList][$subscribeGroups[0]['id']]));
                    }
                }
                $groups = NULL;
            } elseif ($currentList == $defaultList) {
                $groups = Mage::getStoreConfig('monkey/general/cutomergroup', $object->getStoreId());
                $groups = explode(",", $groups);
                if (isset($groups[0]) && $groups[0]) {
                    $subscribeGroups = array();
                    $_prevGroup = null;
                    $checkboxes = array();
                    foreach ($groups as $group) {
                        $item = explode("_", $group);
                        if ($item[0]) {
                            $currentGroup = $item[0];
                            if ($currentGroup == $_prevGroup || $_prevGroup == null) {
                                $checkboxes[] = $item[1];
                                $_prevGroup = $currentGroup;
                            } else {
                                $subscribeGroups[] = array('id' => $_prevGroup, "groups" => str_replace('%C%', '\\,', implode(', ', $checkboxes)));
                                $checkboxes = array();
                                $_prevGroup = $currentGroup;
                                $checkboxes[] = $item[1];
                            }
                        }
                    }
                    if ($currentGroup) {
                        $subscribeGroups[] = array('id' => $currentGroup, "groups" => str_replace('%C%', '\\,', implode(', ', $checkboxes)));
                    }

                }
                if (isset($subscribeGroups[0]['id']) && $subscribeGroups[0]['id'] != -1) {
                    $mergeVars["GROUPINGS"] = $subscribeGroups;
                }

   		$force = Mage::getStoreConfig('monkey/general/checkout_subscribe', $object->getStoreId());
                $map = Mage::getStoreConfig('monkey/general/markfield', $object->getStoreId());
                if (isset($post['magemonkey_subscribe']) && $map != "") {
                    $listsChecked = explode(',', $post['magemonkey_subscribe']);
                    $hasClicked = in_array($currentList, $listsChecked);
                    if ($hasClicked && $force != 3) {
                        $mergeVars[$map] = "Yes";
                    } else {
                        $mergeVars[$map] = "No";
                    }
                } elseif (Mage::getSingleton('core/session')->getIsOneStepCheckout()) {
                    $post2 = $request->getPost();
                    if (isset($post['subscribe_newsletter']) || isset($post2['subscribe_newsletter'])) {
                        $mergeVars[$map] = "Yes";
                    } elseif (Mage::helper('monkey')->config('checkout_subscribe') > 2) {
                        $mergeVars[$map] = "No";
                    }
                } elseif ($request->getModuleName() == 'checkout') {
                    $mergeVars[$map] = "No";
                }
            } else {
                $map = Mage::getStoreConfig('monkey/general/markfield', $object->getStoreId());
                $mergeVars[$map] = "Yes";
            }
        }
    }

    public function subscribeToList($object, $db, $listId = NULL, $forceSubscribe = FALSE)
    {
        if (!$listId) {
            $listId = Mage::helper('monkey')->config('list');
        }
        $email = $object->getEmail();

        if ($object instanceof Mage_Customer_Model_Customer) {
            $subscriber = Mage::getModel('newsletter/subscriber')
                ->setImportMode(TRUE)
                ->setSubscriberEmail($email);
        } else {
//            $customer = Mage::getSingleton('customer/customer')->load($email);
//            if($customer->getId()){
//                $object = $customer;
//            }
            $subscriber = $object;
        }

        $defaultList = Mage::helper('monkey')->config('list');
        if ($listId == $defaultList && !Mage::getSingleton('core/session')->getIsHandleSubscriber() && !$forceSubscribe/*from admin*/) {
            $subscriber->subscribe($email);
        } else {

            $alreadyOnList = Mage::getSingleton('monkey/asyncsubscribers')->getCollection()
                ->addFieldToFilter('lists', $listId)
                ->addFieldToFilter('email', $email)
                ->addFieldToFilter('processed', 0);
            //if not in magemonkey_async_subscribers with processed 0 add list
            if (count($alreadyOnList) == 0) {
                $isConfirmNeed = FALSE;
                if (!Mage::helper('monkey')->isAdmin() &&
                    (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_CONFIRMATION_FLAG, $object->getStoreId()) == 1 && !$forceSubscribe && !Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_CONFIRMATION_EMAIL, $object->getStoreId()))
                ) {
                    $isConfirmNeed = TRUE;
                }

                $isOnMailChimp = Mage::helper('monkey')->subscribedToList($email, $listId);
                //if( TRUE === $subscriber->getIsStatusChanged() ){
                if ($isOnMailChimp == 1) {
                    return false;
                }

                if ($isConfirmNeed) {
                    $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_UNCONFIRMED);
                }

		// PROD-864
                $mergeVars = Mage::helper('monkey')->mergeVars($object, FALSE, $listId, TRUE);

                $this->_subscribe($listId, $email, $mergeVars, $isConfirmNeed, $db);
            }
        }
    }

    public function _subscribe($listId, $email, $mergeVars, $isConfirmNeed, $db)
    {
        if ($db) {
            if ($isConfirmNeed) {
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('monkey')->__('Confirmation request will be sent soon.'));
            }
            $subs = Mage::getModel('monkey/asyncsubscribers');
            $subs->setMapfields(serialize($mergeVars))
                ->setEmail($email)
                ->setLists($listId)
                ->setConfirm($isConfirmNeed)
                ->setProcessed(0)
                ->setCreatedAt(Mage::getModel('core/date')->gmtDate())
                ->save();
        } else {
            if ($isConfirmNeed) {
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('monkey')->__('Confirmation request has been sent.'));
            }
            Mage::getSingleton('monkey/api')->listSubscribe($listId, $email, $mergeVars, 'html', $isConfirmNeed, TRUE);
        }
    }
}

