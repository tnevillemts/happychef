<?php

//require_once 'Ripen/Coreextended/controllers/ProductController.php';
require_once Mage::getModuleDir('controllers', 'Ripen_Coreextended') . DS . 'ProductController.php';

class Ripen_AWExtendAdvancedReviews_ProductController extends Ripen_Coreextended_ProductController
{

    public function postAction()
    {
        if (Mage::helper('advancedreviews')->getAntiSpamEnabled()) {
            $bot = ($this->getRequest()->getPost('_antispam')) ? false : true;
        } else {
            $bot = false;
        }

        if (!$bot) {

            $email = '';
            if ($data = $this->getRequest()->getPost()) {
                //Save PROSCONS data in customer session
                if (isset($data['proscons_items'])) {
                    Mage::register('advancedreviews_proscons_new_items', $data['proscons_items']);
                }
                if (isset($data['user-pros'])) {
                    Mage::register('advancedreviews_proscons_user_pros', $data['user-pros']);
                }
                if (isset($data['user-cons'])) {
                    Mage::register('advancedreviews_proscons_user_cons', $data['user-cons']);
                }
                if (isset($data['recommend'])) {
                    Mage::register('advancedreviews_recommend_value', $data['recommend']);
                }

                if (isset($data['email'])) {
                    Mage::register('advancedreviews_guest_email', $data['email']);
                    $email = $data['email'];
                } else {
                    if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                        $customer = Mage::getSingleton('customer/session')->getCustomer();
                        $email = $customer->getEmail();
                        Mage::register('advancedreviews_guest_email', $email);
                    } else {
                        $reviewOrderId = Mage::getSingleton('core/session')->getData('ReviewOrderId');
                        if (!empty($reviewOrderId)) {
                            $reviewOrder = Mage::getModel('sales/order')->load($reviewOrderId);
                            $email = $reviewOrder->getCustomerEmail();
                            if (!empty($email)) {
                                Mage::register('advancedreviews_guest_email', $email);
                            }
                        }
                    }
                }
            }

            //Here allow to insert review
            //Mage::getSingleton('review/session')->setRedirectUrl(Mage::helper('advancedreviews')->getReviewsBackUrl());
            // The line above was commented out by Ripen (MM) to fix review form submission

            parent::postAction();
            /*
             * Logical functional finished in AW_AdvancedReviews_Model_Mysql4_Review
             * throw overrided _afterSave() of Mage_Review_Model_Mysql4_Review
             */

            $reviewId = $this->getReviewId();
            if (!empty($reviewId) && !empty($email)) {
                $review = Mage::getModel('review/review')->load($reviewId);
                $review->setData('customer_email', $email);
                $review->save();
            }
        } else {
            Mage::getSingleton('core/session')->addError($this->__('Antispam code is invalid. Please, check if JavaScript is enabled in your browser settings.'));
            $this->_redirectReferer();
        }
    }

    public function viewAction()
    {
        $review = $this->_loadReview((int)$this->getRequest()->getParam('id'));
        if (!$review) {
            $this->_forward('noroute');
            return;
        }

        $product = $this->_loadProduct($review->getEntityPkValue());
        if (!$product) {
            $this->_forward('noroute');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('review/session');
        $this->_initLayoutMessages('catalog/session');
        $this->getLayout()->getBlock('head')->setTitle($product->getMetaTitle() . ' - ' . $review->getTitle() . $this->__(' - review by ') . $review->getNickname());
        $this->renderLayout();
    }

    protected function _loadReview($reviewId)
    {
        if (!$reviewId) {
            return false;
        }

        $review = Mage::getModel('review/review')->load($reviewId);
        /* @var $review Mage_Review_Model_Review */
        if (!$review->getId() || !$review->getStatusId() == Mage_Review_Model_Review::STATUS_APPROVED || !in_array(Mage::app()->getStore()->getId(), (array)$review->getStores())) {
            return false;
        }

        Mage::register('current_review', $review);

        return $review;
    }

    protected function _loadProduct($productId)
    {
        if (!$productId) {
            return false;
        }

        $product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($productId);
        /* @var $product Mage_Catalog_Model_Product */
        if (!$product->getId() || !$product->isVisibleInCatalog() || !$product->isVisibleInSiteVisibility()) {
            return false;
        }

        Mage::register('current_product', $product);
        Mage::register('product', $product);

        return $product;
    }

    protected function _cropReviewData(array $reviewData)
    {
        $croppedValues = array();
        $allowedKeys = array_fill_keys(array('detail', 'title', 'nickname', 'is_from_customer'), true);

        foreach ($reviewData as $key => $value) {
            if (isset($allowedKeys[$key])) {
                $croppedValues[$key] = $value;
            }
        }

        return $croppedValues;
    }
}
