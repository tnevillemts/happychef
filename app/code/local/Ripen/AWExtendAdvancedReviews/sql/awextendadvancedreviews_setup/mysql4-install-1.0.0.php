<?php

$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$this->getTable('review')}` ADD `customer_email` VARCHAR(255) NOT NULL AFTER `status_id`;");
$installer->endSetup();
