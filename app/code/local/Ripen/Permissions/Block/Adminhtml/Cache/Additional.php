<?php

class Ripen_Permissions_Block_Adminhtml_Cache_Additional extends Mage_Adminhtml_Block_Cache_Additional
{
    public function getCleanImagesUrl()
    {
        return $this->getUrl('*/*/cleanImages');
    }

    public function getCleanMediaUrl()
    {
        return $this->getUrl('*/*/cleanMedia');
    }
}
