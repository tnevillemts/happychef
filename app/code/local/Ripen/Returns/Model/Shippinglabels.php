<?php

class Ripen_Returns_Model_Shippinglabels extends Mage_Core_Model_Abstract
{
    protected $logFile = '';

    protected function _construct()
    {
        $this->_init("shippinglabels/shippinglabels");
        $this->logFile = Mage::getStoreConfig('shippinglabels/general/log_file');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Mage::helper('shipping labels')->__('Shipping labels interface');
    }

    /**
     * @param array $orderShippingAddress
     * @param $addToAddress - reference to obj
     */
    protected function setStreetAddress($orderShippingAddress, &$addToAddress) {

        $streetAddressRegValid = '/^\d+\s/';
        $streetAddressSplitReg = '/$\R?^/m';

        // There are potentially two address lines and they will be seperated by a newline.
        $addressLines = preg_split ($streetAddressSplitReg, $orderShippingAddress->getData('street'));

        $street1 = $addressLines[0];
        $street2 = $addressLines[1];

        // Stick whatever matches into line1.
        if(! preg_match($streetAddressRegValid, $addressLines[0]) && preg_match($streetAddressRegValid, $addressLines[1])) {
            $street1 = $addressLines[1];
            $street2 = $addressLines[0];
        }

        $addToAddress->setAddressLine1($street1);
        $addToAddress->setAddressLine2($street2);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Order_Address $orderShippingAddress
     * @return void
     */
    protected function setShipperAddress(&$shipment, $order, $orderShippingAddress)
    {
        // Set shipper (this is the carrier)
        $shipperNumber = Mage::getStoreConfig('shippinglabels/general/shipper_number');

        $shipper = $shipment->getShipper();
        $shipper->setShipperNumber              ($shipperNumber);
        $shipper->setName                       ($order->getCustomerName());
        $shipper->setAttentionName              ($order->getCustomerName());

        $shipperAddress = $shipper->getAddress();
        $this->setStreetAddress                 ($orderShippingAddress, $shipperAddress);
        $shipperAddress->setPostalCode          ($orderShippingAddress->getData('postcode'));
        $shipperAddress->setCity                ($orderShippingAddress->getData('city'));
        $region = Mage::getModel('directory/region')->load($orderShippingAddress->getData('region_id'));
        $shipperAddress->setStateProvinceCode   ($region->getCode());
        $shipperAddress->setCountryCode         ($orderShippingAddress->getData('country_id'));
        $shipper->setAddress($shipperAddress);

        $shipper->setEMailAddress               ($order->getCustomerEmail());
        $shipper->setPhoneNumber                ($orderShippingAddress->getData('telephone'));

        $shipment->setShipper($shipper);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @return void
     */
    protected function setReturnToAddress(&$shipment)
    {
        $returnToAddress = array();
        $company        = Mage::getStoreConfig('shippinglabels/return_to/company');
        $attentionName  = Mage::getStoreConfig('shippinglabels/return_to/attention_name');
        $street         = Mage::getStoreConfig('shippinglabels/return_to/street');
        $city           = Mage::getStoreConfig('shippinglabels/return_to/city');
        $state          = Mage::getStoreConfig('shippinglabels/return_to/state');
        $postalCode     = Mage::getStoreConfig('shippinglabels/return_to/postal_code');
        $country        = Mage::getStoreConfig('shippinglabels/return_to/country');
        $emailAddress   = Mage::getStoreConfig('shippinglabels/return_to/email_address');
        $phoneNumber    = Mage::getStoreConfig('shippinglabels/return_to/phone_number');
        $returnToAddress['CompanyName']         = $company;
        $returnToAddress['AttentionName']       = $attentionName;
        $returnToAddress['AddressLine1']        = $street;
        $returnToAddress['City']                = $city;
        $returnToAddress['StateProvinceCode']   = $state;
        $returnToAddress['PostalCode']          = $postalCode;
        $returnToAddress['CountryCode']         = $country;
        $returnToAddress['EmailAddress']        = $emailAddress;
        $returnToAddress['PhoneNumber']         = $phoneNumber;

        $address = new \Ups\Entity\Address();
        $address->setAddressLine1       ($returnToAddress['AddressLine1']);
        $address->setPostalCode         ($returnToAddress['PostalCode']);
        $address->setCity               ($returnToAddress['City']);
        $address->setStateProvinceCode  ($returnToAddress['StateProvinceCode']);
        $address->setCountryCode        ($returnToAddress['CountryCode']);

        $shipTo = new \Ups\Entity\ShipTo();
        $shipTo->setAddress($address);
        $shipTo->setCompanyName         ($returnToAddress['CompanyName']);
        $shipTo->setAttentionName       ($returnToAddress['AttentionName']);
        $shipTo->setEMailAddress        ($returnToAddress['EmailAddress']);
        $shipTo->setPhoneNumber         ($returnToAddress['PhoneNumber']);

        $shipment->setShipTo($shipTo);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Order_Address $orderShippingAddress
     * @return void
     */
    protected function setFromAddress(&$shipment, $order, $orderShippingAddress)
    {
        // From address
        $address = new \Ups\Entity\Address();
        $this->setStreetAddress             ($orderShippingAddress, $address);
        $address->setPostalCode             ($orderShippingAddress->getData('postcode'));
        $address->setCity                   ($orderShippingAddress->getData('city'));
        $region = Mage::getModel('directory/region')->load($orderShippingAddress->getData('region_id'));
        $address->setStateProvinceCode      ($region->getCode());
        $address->setCountryCode            ($orderShippingAddress->getData('country_id'));

        $shipFrom = new \Ups\Entity\ShipFrom();
        $shipFrom->setAddress($address);
        $shipFrom->setName                  ($order->getCustomerName());
        $shipFrom->setAttentionName         ($shipFrom->getName());
        $shipFrom->setCompanyName           ($shipFrom->getName());
        $shipFrom->setEMailAddress          ($order->getCustomerEmail());
        $shipFrom->setPhoneNumber           ($orderShippingAddress->getData('telephone'));

        $shipment->setShipFrom($shipFrom);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @param Mage_Sales_Model_Order $order
     * @param Mage_Sales_Model_Order_Address $orderBillingAddress
     * @return void
     */
    protected function setSoldTo(&$shipment, $order, $orderBillingAddress)
    {
        // Sold to (billing address of order)
        $address = new \Ups\Entity\Address();
        $this->setStreetAddress             ($orderBillingAddress, $address);
        $address->setPostalCode             ($orderBillingAddress->getData('postcode'));
        $address->setCity                   ($orderBillingAddress->getData('city'));
        $address->setCountryCode            ($orderBillingAddress->getData('country_id'));
        $region = Mage::getModel('directory/region')->load($orderBillingAddress->getData('region_id'));
        $address->setStateProvinceCode      ($region->getCode());

        $soldTo = new \Ups\Entity\SoldTo;
        $soldTo->setAddress($address);

        $soldTo->setAttentionName           ($orderBillingAddress->getData('firstname') . ' ' . $orderBillingAddress->getData('lastname'));
        $soldTo->setCompanyName             ($orderBillingAddress->getData('company'));
        $soldTo->setEMailAddress            ($order->getCustomerEmail());
        $soldTo->setPhoneNumber             ($orderBillingAddress->getData('telephone'));

        $shipment->setSoldTo($soldTo);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @return void
     */
    protected function setService(&$shipment)
    {
        // Set service
        $service = new \Ups\Entity\Service;
        $service->setCode(\Ups\Entity\Service::S_GROUND);
        $service->setDescription($service->getName());
        $shipment->setService($service);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @param int $code
     * @return void
     */
    protected function markAsReturn(&$shipment, $code)
    {
        $returnService = new \Ups\Entity\ReturnService;
        $returnService->setCode($code);
        $shipment->setReturnService($returnService);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @param Mage_Sales_Model_Order $order
     * @param int $code
     * @return void
     */
    protected function setShipmentServiceOptions(&$shipment, $order, $code)
    {
        if($code == \Ups\Entity\ReturnService::ELECTRONIC_RETURN_LABEL_ERL) {
            $message = "Happy Chef Return Label for Order #" . $order->getIncrementId();

            $shipmentServiceOptions = new \Ups\Entity\ShipmentServiceOptions();
            $labelDelivery = new \Ups\Entity\LabelDelivery();

            $labelDelivery->setLabelLinkIndicator                           (true);
            $labelDelivery->setEMailAddress($order->getCustomerEmail());
            $labelDelivery->setFromEMailAddress                             (Mage::getStoreConfig('shippinglabels/return_to/email_address'));
            $labelDelivery->setUndeliverableEMailAddress                    ('info@happychef.com');
            $labelDelivery->setFromName                                     (Mage::getStoreConfig('shippinglabels/return_to/company'));
            $labelDelivery->setSubject                                      ($message);
            $labelDelivery->setSubjectCode                                  ('1');

            $shipmentServiceOptions->setLabelDelivery($labelDelivery);
            $shipment->setShipmentServiceOptions($shipmentServiceOptions);
        }
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @param string $orderId
     * @return void
     */
    protected function setDescription($shipment, $orderId)
    {
        // Set description
        $shipment->setDescription('Return(s) from order ' . $orderId);
    }

    /**
     * @param Ups\Entity\Package $package
     * @param string $returnMerchandiseAuthorizationNumber
     * @return void
     */
    protected function setReferenceNumber(&$package, $returnMerchandiseAuthorizationNumber)
    {
        // Set Reference Number
        $referenceNumber = new \Ups\Entity\ReferenceNumber;

        $referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_RETURN_AUTHORIZATION_NUMBER);
        $referenceNumber->setValue($returnMerchandiseAuthorizationNumber);

        $package->setReferenceNumber($referenceNumber);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @param Mage_Sales_Model_Order $order
     * @param string $returnMerchandiseAuthorizationNumber
     * @return void
     */
    protected function addPackage(&$shipment, $order, $returnMerchandiseAuthorizationNumber)
    {
        // Add Package
        $package = new \Ups\Entity\Package();
        $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);

        // As per Jim Walsh: We found out from a UPS bill that they will charge us based on the higher of: Reported
        // weight on the label | Actual weight.  Since we don’t know what weight a shipment might be, please default
        // all labels to 1 lb no matter what the order contains. UPS just charged Mark for 40 lbs (stated on label)
        // when the actual weight was about 7 lbs. How quick can you make this change?
        //
        /* previously
        $weight = $order->getWeight();
        if($weight == 0) {
            $weight = 1;
        }
        */
        $weight = 1;

        $package->getPackageWeight()->setWeight($weight);
        $unit = new \Ups\Entity\UnitOfMeasurement;
        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
        $package->getPackageWeight()->setUnitOfMeasurement($unit);

        // Set dimensions
        $dimensions = new \Ups\Entity\Dimensions();

        $height = Mage::getStoreConfig('shippinglabels/package/package_height');
        $width  = Mage::getStoreConfig('shippinglabels/package/package_width');
        $length = Mage::getStoreConfig('shippinglabels/package/package_length');
        $dimensions->setHeight($height);
        $dimensions->setWidth($width);
        $dimensions->setLength($length);

        $unit = new \Ups\Entity\UnitOfMeasurement;
        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);
        $dimensions->setUnitOfMeasurement($unit);
        $package->setDimensions($dimensions);

        $this->setReferenceNumber($package, $returnMerchandiseAuthorizationNumber);

        // Add descriptions because it is a package
        $package->setDescription(' Order ID - ' . $order->getIncrementId());

        // Add this package
        $shipment->addPackage($package);
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @return void
     */
    protected function setPaymentInformation(&$shipment)
    {
        $shipperNumber = Mage::getStoreConfig('shippinglabels/general/shipper_number');

        // Set payment information
        $shipment->setPaymentInformation(new \Ups\Entity\PaymentInformation('prepaid', (object)array('AccountNumber' => $shipperNumber)));
    }

    /**
     * @param Ups\Entity\Shipment $shipment
     * @return void
     */
    protected function setNegotiatedRates(&$shipment)
    {
        // Ask for negotiated rates (optional)
        $rateInformation = new \Ups\Entity\RateInformation;
        $rateInformation->setNegotiatedRatesIndicator(1);
        $shipment->setRateInformation($rateInformation);
    }

    /**
     * Gives us a consistent return merchandise authorization number.
     *
     * @return string
     */
    protected function getReturnMerchandiseAuthorizationNumber()
    {
        $rmaRandMin = Mage::getStoreConfig('shippinglabels/general/rma_rand_min');
        $rmaRandMax = Mage::getStoreConfig('shippinglabels/general/rma_rand_max');

        // Avoid collisions
        while(true) {
            $aBigRandomNumber = 'W-' . rand($rmaRandMin, $rmaRandMax);
            $returnsCollections = Mage::getModel('returns/orderreturns')->getCollection();
            $returnsCollections->addFieldToFilter('rma_number', $aBigRandomNumber);
            if(count($returnsCollections) == 0)
                break;
        }
        return strval($aBigRandomNumber);
    }

    /**
     * @param stdClass $resp
     * @return string
     */
    protected function getFile($resp)
    {
        $graphicImage = $resp->LabelResults->LabelImage->GraphicImage;
        return $graphicImage;
    }

    /**
     * @param Ups\Shipping $api
     * @param Ups\Entity\Shipment $shipment
     * @param string $orderId
     * @param string $returnMerchandiseAuthorizationNumber
     * @return string
     */
    protected function getNewLabel($api, $shipment, $orderId, $returnMerchandiseAuthorizationNumber)
    {
        // Confirm holds the digest you need to accept the result
        $confirm = $api->confirm(\Ups\Shipping::REQ_VALIDATE, $shipment);
        if($confirm) {
            $api->accept($confirm->ShipmentDigest);

            // Recover the image, the label will actually be here.
            $recoverResp = $api->recoverLabel($confirm->ShipmentIdentificationNumber);

            // Record
            $returnsData  = array('order_id'=> $orderId,
                                  'rma_number'=>(string) $returnMerchandiseAuthorizationNumber,
                                  'shipment_id_number'=>(string) $confirm->ShipmentIdentificationNumber,
                                  'status'=>'1');

            $modelOrderReturns = Mage::getModel('returns/orderreturns')->setData($returnsData);
            $modelOrderReturns->save()->getId();
        } else {
            throw new Exception("UPS label confirmation failure.");
        }

        return $this->getFile($recoverResp);
    }

    /**
     * @param Ups\Shipping $api
     * @param Ripen_Returns_Model_Resource_Orderreturns_Collection $prevReturnsCollections
     * @return string
     */
    protected function getLastLabel($api, $prevReturnsCollections)
    {
        try {
            // Get the latest one, should already be order by created_at desc.
            foreach($prevReturnsCollections as $prevReturn) {
                $shipmentIdNumber = $prevReturn->getShipmentIdNumber();
                $resp = $api->recoverLabel($shipmentIdNumber);
                break;
            }
        } catch (Exception $e) {
            throw new Exception("Cannot get original label: " . $e->getMessage());
        }
        return $this->getFile($resp);
    }

    /**
     * @param string $orderId
     * @param Ripen_Returns_Model_Resource_Orderreturns_Collection $prevReturnsCollections
     * @return bool
     */
    protected function findUnusedLabelForOrder($orderId, $prevReturnsCollections)
    {
        // Only check whether a past label has been used when using integration (production) endpoint.
        // Will throw exceptions when run against UPS test system, because there tracking information there is not persistent.
        $useIntegrationUrl = Mage::getStoreConfig('shippinglabels/general/use_integration_url');
        if ($useIntegrationUrl === '0') {

            $accessKey = Mage::getStoreConfig('shippinglabels/general/api_access_key');
            $userId    = Mage::getStoreConfig('shippinglabels/general/user_id');
            $password  = Mage::getStoreConfig('shippinglabels/general/password');
            $tracking  = new Ups\Tracking($accessKey, $userId, $password);

            foreach ($prevReturnsCollections as $prevReturn) {
                $shipmentIdNumber = $prevReturn->getShipmentIdNumber();

                try {
                    $shipment = $tracking->track($shipmentIdNumber);
                    if (! $shipment || ! $shipment->Package) continue;

                    // If the label has not been used you would not see these status codes: [ P = Pickup, I = In Transit (arrival scan), D = Delivered ].
                    // Instead the code would be: M = Manifest pickup, which means that UPS has not yet seen the package.
                    if ($shipment->Package->Activity->Status->StatusType->Code == 'M') {
                        return true;
                    }
                } catch (Exception $e) {
                    // If there is no tracking information available the system throws an exception, we don't want that to stop the process for that.
                    if (strpos($e->getMessage(), "No tracking information available") !== false) {
                        Mage::log("Exception [" . $e->getMessage() . "]", null, $this->logFile, true);
                    } else {
                        throw new Exception("Check for unused label failed for order $orderId: " . $e->getMessage());
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param string $orderId
     * @return string
     */
    public function getShippingLabel($orderId)
    {
        // Get shipment info
        $accessKey = Mage::getStoreConfig('shippinglabels/general/api_access_key');
        $userId    = Mage::getStoreConfig('shippinglabels/general/user_id');
        $password  = Mage::getStoreConfig('shippinglabels/general/password');
        $useIntegrationUrl = Mage::getStoreConfig('shippinglabels/general/use_integration_url');

        // bool $useIntegration (arg 4) Determines if we should use production or CIE URLs.
        $api = new Ups\Shipping($accessKey, $userId, $password, (strtolower($useIntegrationUrl) === '1')? true : false);

        $prevReturnsCollections = Mage::getModel('returns/orderreturns')->getCollection()
            ->addFieldToFilter('order_id', $orderId)
            ->setOrder('created_at', 'DESC');

        if (! $this->findUnusedLabelForOrder($orderId, $prevReturnsCollections)) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel("sales/order")->load($orderId);
            $orderShippingAddress = $order->getShippingAddress();
            $orderBillingAddress  = $order->getBillingAddress();
            if (!$order->getId() || !$orderShippingAddress->getId() || !$orderBillingAddress->getId()) {
                throw new Exception("Cannot use order to process request.");
            }

            $returnMerchandiseAuthorizationNumber = $this->getReturnMerchandiseAuthorizationNumber();
            $shipment = new Ups\Entity\Shipment;
            $this->setReturnToAddress($shipment);
            $this->setShipperAddress($shipment, $order, $orderShippingAddress);
            $this->setFromAddress($shipment, $order, $orderShippingAddress);
            $this->setSoldTo($shipment, $order, $orderBillingAddress);
            $this->setService($shipment);
            $this->markAsReturn($shipment, \Ups\Entity\ReturnService::ELECTRONIC_RETURN_LABEL_ERL);
            $this->setShipmentServiceOptions($shipment, $order, \Ups\Entity\ReturnService::ELECTRONIC_RETURN_LABEL_ERL);
            $this->setDescription($shipment, $orderId);
            $this->addPackage($shipment, $order, $returnMerchandiseAuthorizationNumber);
            $this->setPaymentInformation($shipment);

            return $this->getNewLabel($api, $shipment, $orderId, $returnMerchandiseAuthorizationNumber);
        } else {
            return $this->getLastLabel($api, $prevReturnsCollections);
        }
    }
}
