<?php

class Ripen_Returns_Model_Orderreturns extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('returns/orderreturns');
    }

    protected function _beforeSave()
    {
        if (!$this->getIncrementId()) {
            $incrementId = Mage::getSingleton('eav/config')
                ->getEntityType('orderreturns')
                ->fetchNewIncrementId($this->getStoreId());
            $this->setIncrementId($incrementId);

            if($this->isObjectNew()){
                $this->setCreatedAt(Mage::getModel('core/date')->timestamp());
            }
        }
    }
}
