<?php

class Ripen_Returns_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     * @throws Varien_Exception
     */
    public function isOrderReturnEligible($order)
    {
        $testUserEmail = Mage::getStoreConfig('shippinglabels/general/test_user_email');
        $userEmail = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
        $returnsActive = empty($testUserEmail) || $testUserEmail == $userEmail;

        return $returnsActive && $order && $order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE;
    }
}
