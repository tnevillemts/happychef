<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->run("
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS {$this->getTable('returns/orderreturns')};
");

$orderReturnsTable = $installer->getConnection()
    ->newTable($installer->getTable('returns/orderreturns'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'Order ID')
    ->addColumn('rma_number', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'RMA Number')
    ->addColumn('shipment_id_number', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'Shipment Identification Number')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
    ), 'Created At')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'Status')
;
$installer->getConnection()->createTable($orderReturnsTable);

$installer->addEntityType('orderreturns', array(
    'entity_model'          => 'returns/orderreturns',
    'table'                 => 'returns/orderreturns',
    'increment_model'       => 'eav/entity_increment_numeric',
    'increment_per_store'   => true
));

$installer->endSetup();
