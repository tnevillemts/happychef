<?php

class Ripen_Returns_ResultController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $orderId           = $this->getRequest()->getParam('order_id');
        $inputEmailAddress = $this->getRequest()->getParam('email');

        try {
            $order = Mage::getModel('sales/order')->load($orderId);
            $custEmail = $order->getCustomerEmail();

            if (trim($inputEmailAddress) != trim($custEmail)) {
                throw new Exception("Provided email does not match email on order.");
            }

            if (! Mage::helper('returns')->isOrderReturnEligible($order)) {
                throw new Exception("This order is not currently eligible to be returned.");
            }

            /** @var Ripen_Returns_Model_Shippinglabels $shippingLabels */
            $shippingLabels = Mage::getModel('returns/shippinglabels');
            if ($shippingLabels) {
                $graphicImage = $shippingLabels->getShippingLabel($orderId);
            }

            $this->loadLayout();
            $block = $this->getLayout()->getBlock('returnsresult');
            $block->assign('custEmail', $custEmail);
            $block->assign('graphicImage', $graphicImage);

            $this->getLayout()->getBlock('head')->setTitle('Order Return Label');
            $this->renderLayout();
        } catch (Exception $e) {
            Mage::getSingleton( 'customer/session' )->addError('Error: ' . $e->getMessage());
            $this->_redirectReferer('*/index');
        }
    }
}
