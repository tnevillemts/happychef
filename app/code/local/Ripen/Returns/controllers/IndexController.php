<?php

class Ripen_Returns_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $orderId           = $this->getRequest()->getParam('order_id');
        $inputEmailAddress = $this->getRequest()->getParam('email');

        try {
            $order = Mage::getModel('sales/order')->load($orderId);
            $custEmail = $order->getCustomerEmail();

            if (trim($inputEmailAddress) != trim($custEmail)) {
                throw new Exception("Provided email does not match email on order.");
            }

            if (! Mage::helper('returns')->isOrderReturnEligible($order)) {
                throw new Exception("This order is not currently eligible to be returned.");
            }
        } catch (Exception $e) {
            Mage::getSingleton( 'customer/session' )->addError('Error: ' . $e->getMessage());
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('returns')->setFormAction( Mage::getUrl('*/returnsresult/') );
        $this->renderLayout();
    }
}
