<?php

class Ripen_Returns_Block_Adminhtml_Order_View_Tab extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('returns/order/view/tab.phtml');
    }

    public function getTabLabel()
    {
        return $this->__('Returns');
    }

    public function getTabTitle()
    {
        return $this->__('Returns');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getOrder()
    {
        return Mage::registry('current_order');
    }
}
