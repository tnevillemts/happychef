<?php

class Ripen_Returns_Block_Returns extends Mage_Core_Block_Template
{
    public function isCustomerLocatedForPrepaidLabel($orderId)
    {
        // Get the region code for the shipment.
        $order = Mage::getModel('sales/order')->load($orderId);
        if ($order->getId()) {
            $shippingAddress = $order->getShippingAddress();
            if ($shippingAddress) {
                $regionCode = $shippingAddress->getRegionCode();
                $lowerFortyEightStates = "AL AZ AR CA CO CT DE FL GA ID IL IN 
                                           IA KS KY LA ME MD MA MI MN MS MO MT 
                                           NE NV NH NJ NM NY NC ND OH OK OR PA 
                                           RI SC SD TN TX UT VT VA WA WV WI WY";

                if (strpos($lowerFortyEightStates, $regionCode) !== false) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getFormActionUrl($orderId, $email)
    {
        return $this->getUrl('returns/result', array('_secure' => false, 'order_id' => $orderId, 'email' => $email));
    }
}
