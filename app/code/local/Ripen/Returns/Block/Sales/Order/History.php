<?php

class Ripen_Returns_Block_Sales_Order_History extends Mage_Sales_Block_Order_History
{
    public function getReturnUrl($order)
    {
        return $this->getUrl('returns', array('order_id' => $order->getId(), 'email' => $order->getCustomerEmail()));
    }
}
