<?php
class Ripen_SearchSuite_Block_NoResult extends Mage_Core_Block_Template
{
    /**
     * Catalog Product collection
     *
     * @var Mage_CatalogSearch_Model_Resource_Fulltext_Collection
     */
    protected $_productCollection;

    /**
     * Retrieve query model object
     *
     * @return Mage_CatalogSearch_Model_Query
     */
    protected function _getQuery()
    {
        return $this->helper('catalogsearch')->getQuery();
    }

    /**
     * Prepare layout
     *
     * @return Mage_CatalogSearch_Block_Result
     */
    protected function _prepareLayout()
    {
        // modify page title
        $title = $this->__("Search results for: '%s'", $this->helper('catalogsearch')->getEscapedQueryText());

        $searchCategory = Mage::getResourceModel('catalog/category_collection')
            ->addFieldToFilter('name', 'Search Results')
            ->getFirstItem();

        Mage::register('current_category', $searchCategory);

        $this->getLayout()->getBlock('head')->setTitle($title);

        return parent::_prepareLayout();
    }
}
