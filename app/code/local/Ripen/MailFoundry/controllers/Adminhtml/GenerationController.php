<?php
/**
 * Class Ripen_MailFoundry_Adminhtml_GenerationController
 *
 * @author Ripen eCommerce
 * @package Ripen_MailFoundry
 */

class Ripen_MailFoundry_Adminhtml_GenerationController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('newsletter/mailfoundry/generation');
        return $this;
    }

    public function indexAction()
    {

        $this->_initAction()
            ->renderLayout();

    }

    /**
     * Load Campaign Action
     * AJAX action to load campaign data
     */
    public function loadcampaignAction() {
        $helper = Mage::helper('mailfoundry');
        $campaignData = array();

        if(isset($_GET['campaign']) && ($_GET['campaign'] !== '')) {
            $campaign = $_GET['campaign'];
            // print_r('campaign');

            $campaignData += $helper->loadCampaign($campaign);
        }
        $this->jsonResponse($campaignData);

    }

    /**
     * Campaign Form Vals Action
     * Ajax action for getting campaign values.
     */
    public function campaignformvalsAction() {
        $helper = Mage::helper('mailfoundry');

        $defaultOptions = array('new' => 'New');
        $liveCampaignOptions = $helper->getCampaignOptions();
        $campaignOptions = array_merge($defaultOptions, $liveCampaignOptions);

        $templateOptions = $helper->getTemplateOptions();
        $firstTemplateKey = key($templateOptions);
        $ymlTemplateData = $helper->getYmlTemplateData($firstTemplateKey);
        $ymlInputs = $helper->convertYmlToInputs($ymlTemplateData);
        $campaignFormVals = array(
            'campaignOptions' => $campaignOptions,
            'templateOptions' => $templateOptions,
            'ymlTemplateData' => $ymlTemplateData,
            'ymlInputs' => $ymlInputs
        );

        $this->jsonResponse($campaignFormVals);
    }

    /**
     * Load YML Template Action
     * AJAX Action for loading YML template date
     */
    public function loadymltemplateAction() {
        $helper = Mage::helper('mailfoundry');
        $templateData = array();

        if(isset($_GET['ymlTemplate']) && ($_GET['ymlTemplate'] !== '')) {
            $ymlTemplate = $_GET['ymlTemplate'];
            $ymlData = $helper->getYmlTemplateData($ymlTemplate);
            $ymlInputs = $helper->convertYmlToInputs($ymlData);
            $templateData += array('ymlData' => $ymlData);
            $templateData +=array('ymlInputs' => $ymlInputs);
        }
        $this->jsonResponse($templateData);
    }

    /**
     * Save Campaign Action
     * AJAX Action to save the email campaign data
     */
    public function savecampaignAction() {
        $helper = Mage::helper('mailfoundry');

        $saveLog = $helper->saveCampaign();

        $this->jsonResponse($saveLog);
    }

    /**
     * Filelist Action
     * Retrieves a list of files in the given path
     */
    public function filelistAction() {
        $filesystemType = $_GET['type'];
        $requestPath = ($_GET['path']) ? $_GET['path'] : '' ;

        $helper = Mage::helper('mailfoundry');
        $fileSystem = $helper->getFileSystem($filesystemType);
        $listContents = $fileSystem->listContents($requestPath);
        $response = $helper->formattedFileList($listContents);

        $this->jsonResponse($response);
    }


    /**
     * Environment Action
     * Gets environment specific values
     */
    public function environmentAction() {
        $env = $_GET['env'];
        $helper = Mage::helper('mailfoundry');
        $environmentValues = $helper->getEnvironmentValues($env);
        $this->jsonResponse($environmentValues);
    }


    /**
     * JSON Response
     * A slight shortcut to return encoded json response, interfacing this helps decouple from Magento for portability
     *
     * @param $response
     */
    protected function jsonResponse($response) {
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));
    }

}