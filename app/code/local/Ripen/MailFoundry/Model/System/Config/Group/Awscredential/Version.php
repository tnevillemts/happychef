<?php
/**
 * Region configuration values for AWS
 *
 * @author Ripen eCommerce
 * @package Ripen_MailFoundry
 */

class Ripen_MailFoundry_Model_System_Config_Group_Awscredential_Version
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '2006-03-01',
                'label' => 'S3 (2006-03-01)',
            )
        );
    }
}
