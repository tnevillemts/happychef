<?php
/**
 * Region configuration values for AWS
 *
 * @author Ripen eCommerce
 * @package Ripen_MailFoundry
 */

class Ripen_MailFoundry_Model_System_Config_Group_Awscredential_Region
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'us-east-2',
                'label' => 'US East (Ohio)',
            ),
            array(
                'value' => 'us-east-1',
                'label' => 'US East (N. Virginia)',
            ),
            array(
                'value' => 'us-west-1',
                'label' => 'US West (N. California)',
            ),
            array(
                'value' => 'us-west-2',
                'label' => 'US West (Oregon)',
            ),
            array(
                'value' => 'ap-south-1',
                'label' => 'Asia Pacific (Mumbai)',
            ),
            array(
                'value' => 'ap-northeast-2',
                'label' => 'Asia Pacific (Seoul)',
            ),
            array(
                'value' => 'ap-southeast-1',
                'label' => 'Asia Pacific (Singapore)',
            ),
            array(
                'value' => 'ap-southeast-2',
                'label' => 'Asia Pacific (Sydney)',
            ),
            array(
                'value' => 'ap-northeast-1',
                'label' => 'Asia Pacific (Tokyo)',
            ),
            array(
                'value' => 'ca-central-1',
                'label' => 'Canada (Central)',
            ),
            array(
                'value' => 'cn-north-1',
                'label' => 'China (Beijing)',
            ),
            array(
                'value' => 'eu-central-1',
                'label' => 'EU (Frankfurt)',
            ),
            array(
                'value' => 'eu-west-1',
                'label' => 'EU (Ireland)',
            ),
            array(
                'value' => 'eu-west-2',
                'label' => 'EU (London)',
            ),
            array(
                'value' => 'eu-west-3',
                'label' => 'EU (Paris)',
            ),
            array(
                'value' => 'sa-east-1',
                'label' => 'South America (São Paulo)',
            )
        );
    }
}
