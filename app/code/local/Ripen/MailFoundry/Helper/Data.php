<?php
/**
 * MailFoundry Helper
 *
 * @author Ripen eCommerce
 * @package Ripen_MailFoundry
 */

use Symfony\Component\Yaml\Parser;
use League\Flysystem\Adapter\Local as LocalAdapter;
use League\Flysystem\Filesystem as Filesystem;
use Aws\S3\S3Client as CDNclient;
use League\Flysystem\AwsS3v3\AwsS3Adapter as CDNfiles;
use Symfony\Component\Yaml\Yaml;


class Ripen_MailFoundry_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Core Config Data paths
     * Top level keys represent the field group tag in Ripen_Foundry/etc/system.xml, and the actual key corresponds to the field tag.
     *
     * @const array CORE_CONFIG_PATHS
     */
    const CORE_CONFIG_PATHS = array(
        'local_filesystem' => array(
            'campaign_path' => 'mailfoundry/local_filesystem/campaign_path',
            'production_url' => 'mailfoundry/local_filesystem/production_url',
        ),
        'awscredential' => array(
            'key' => 'mailfoundry/awscredential/key',
            'secret' => 'mailfoundry/awscredential/secret',
            'region' => 'mailfoundry/awscredential/region',
            'version' => 'mailfoundry/awscredential/version',
            'bucket' => 'mailfoundry/awscredential/bucket',
            'base_url' => 'mailfoundry/awscredential/base_url',
        ),
        'twig' => array(
            'template_path' => 'mailfoundry/twig/template_path',
            'compilation_cache' => 'mailfoundry/twig/compilation_cache'
        )
    );

    /**
     * Twig Template Path
     *
     * @const string TEMPLATE_PATH
     */
    const TEMPLATE_PATH = 'email/templates';

    /**
     * Twig Template Cache
     *
     * @const string TEMPLATE_CACHE
     */
    const TEMPLATE_CACHE = 'email/templates/compilation_cache';

    /**
     * Upload Prefix
     *
     * @const string UPLOAD_PREFIX
     */
    const UPLOAD_PREFIX = 'mailfoundry_';

    /**
     * Limited types of files that can be uploaded to create an email campaign.
     * This should not be user configurable, due to files having to be specifically processed by extension.
     *
     * @const array VALID_UPLOAD_TYPES
     */
    const VALID_UPLOAD_TYPES = array(
        'zip' => 'application/zip'
    );

    /**
     * Date Format
     *
     * @const string DATE_FORMAT
     */
    const DATE_FORMAT = 'l, F jS Y - h:i:s A';

    /**
     * The ['client'] portion of the array structure is meant to match Aws\S3\S3Client params
     * The ['bucket'] is used separately from the rest, but still part of a required config.
     *
     * @var array $awsConfig
     */
    protected $awsConfig = array(
        'client' => array(
            'credentials' => array(
                'key' => '',
                'secret' => '',
            ),
            'region' => '',
            'version' => '',
        ),
        'bucket' =>  '',

    );

    /**
     * @var Twig_Environment $twig
     */
    protected $twig;

    /**
     * Local Upload Paths
     */
    protected $localUploadPaths = array();

    /**
     * Generate Preview Content
     *
     * @param $path
     * @param $type
     */
    protected function generatePreviewContent($path, $type = 'aws') {
        try {
            $fileSystem = $this->getFilesystem($type);
            $listContents = $fileSystem->listContents($path);

            $this->deleteFilesByExtension($fileSystem, $path, 'html');

            $ymlInfo = $this->getYmlInfoFromListContents($listContents);

            if(!$ymlInfo){
                throw new Exception('Missing YML - Unable to generate email content.');
            }
            $ymlContents = $fileSystem->read($ymlInfo['path']);

            $productionBaseTemplateVars = $this->getBaseTemplateVars($path, 'production');
            $testBaseTemplateVars = $this->getBaseTemplateVars($path, 'test');
            $campaignTemplateVars = Yaml::parse($ymlContents);

            $productionTemplateVars = array_merge($productionBaseTemplateVars, $campaignTemplateVars);
            $testTemplateVars = array_merge($testBaseTemplateVars, $campaignTemplateVars);

            $uniqueOutputSuffix = $this->newUniqueIdentifier();

            $productOutputFile = $path . DS . 'production-' . $uniqueOutputSuffix .'.html';
            $testOutputFile = $path . DS . 'test-' . $uniqueOutputSuffix . '.html';

            $twigTemplateName = $ymlInfo['filename'];
            $fileSystem->write($productOutputFile, $this->renderTwig($twigTemplateName, $productionTemplateVars));
            $fileSystem->write($testOutputFile, $this->renderTwig($twigTemplateName, $testTemplateVars));

        } catch(Exception $e) {
            echo $e->getMessage();
            die;
        }

    }

    /**
     * Get Yml Info From List Contents
     * The first YML info encountered should be returned, as there should be only one. (Highlander?)
     * The reason for this is that when different templates are selected, the filename is the only way to know which template to use.
     * For the time being, it is implicit that the first YML is correct.
     *
     * @param $listContents
     * @return bool
     */
    protected function getYmlInfoFromListContents($listContents) {
        foreach($listContents as $index => $fileinfo) {
            if($fileinfo['extension'] == 'yml') {
                return $fileinfo;
            }
        }
        return false;
    }

    /**
     * Get Base Template Vars
     *
     * @param $path
     * @param $env
     * @return array
     */
    protected function getBaseTemplateVars($path, $env = 'production') {

        $baseHrefConfigPath = ($env === 'production') ? self::CORE_CONFIG_PATHS['local_filesystem']['production_url'] : 'web/secure/base_url' ;
        $baseTemplateVars = array(
            'baseHref' => Mage::getStoreConfig($baseHrefConfigPath),
            'baseBucketPath' => Mage::getStoreConfig(self::CORE_CONFIG_PATHS['awscredential']['base_url']),
            'baseImagePath' => Mage::getStoreConfig(self::CORE_CONFIG_PATHS['awscredential']['base_url'])  . $path . DS
        );

        return $baseTemplateVars;

    }

    /**
     * Load Campaign
     *
     * @param $campaignPath
     * @return array
     */
    public function loadCampaign($campaignPath) {
        $aws = $this->getFilesystem('aws');
        $filelist = $aws->listContents($campaignPath);

        $ymlFilename = '';
        $ymlData = '';
        $filePath = '';

        foreach($filelist as $index => $file) {
            if($file['extension'] === 'yml') {
                $ymlFilename = $file['filename'];
                $filePath = $file['path'];
                $ymlData = $aws->read($file['path']);
                break;
            }
        }

        $ymlInputs = $this->convertYmlToInputs($ymlData);
        $campaignData = array(
            'filelist' => $filelist,
            'ymlFilename' => $ymlFilename,
            'ymlData' => $ymlData,
            'ymlPath' => $filePath,
            'ymlInputs' => $ymlInputs,
        );

        return $campaignData;
    }


    /**
     * Convert Yml To Inputs
     * Generates text inputs and field sets out of YAML data
     *
     * @param string $ymlData
     * @return string
     */
    public function convertYmlToInputs($ymlData) {
        $ymlDataArray = Yaml::parse(htmlspecialchars($ymlData));
        $ymlInputText = '';
        foreach($ymlDataArray as $entity => $field) {
            if(is_array($field)) {
                $ymlInputText .= '
                    <fieldset>
                    <legend>'.$this->ymlToFormRenderDisplayText($entity).'</legend>';
                foreach($field as $subentity => $subfield) {
                    $inputNameParts =  array($entity, $subentity);
                    $ymlInputText .= $this->ymlToFormRenderTextInput($inputNameParts, $subfield);
                }
                $ymlInputText .= '</fieldset>';
            } else {
                $inputNameParts = array($entity);
                $ymlInputText .= $this->ymlToFormRenderTextInput($inputNameParts, $field);
            }
        }
        return $ymlInputText;
    }

    /**
     * Yml to Form Render Text Input
     * Converts parts of Symfony\Yaml::parse() and converts them to text inputs
     *
     * @param $inputNameParts
     * @param $value
     * @return string
     */
    protected function ymlToFormRenderTextInput($inputNameParts, $value) {
        $inputName = '[' . implode('][', $inputNameParts) . ']';
        $labelText = end($inputNameParts);
        return '<div class="field-item"><label>'.$this->ymlToFormRenderDisplayText($labelText).'</label><input type="text" name="yml_inputs'.$inputName.'" value="'.$value.'"/></div>';
    }

    /**
     * Yml To Form Render Display Text
     * Takes the YAML variable name and makes it display friendly
     *
     * @param $labelText
     * @return string
     */
    protected function ymlToFormRenderDisplayText($labelText) {
        return ucwords(str_replace('_' , ' ', $labelText ));
    }


    /**
     * Save Campaign
     * Calls a series of other methods to fully save the campaign
     * Returns a log array of operations, some of which is from the @calls
     *
     * @calls self::saveCampaignUpload()
     * @calls self::saveFilesToProduction()
     * @calls self::generatePreviewContent()
     * @throws Exception
     *
     * @return array $saveLog
     */
    public function saveCampaign() {
        $dateTime = $this->getLocalDateTime();

        $campaign = (($_POST['new_campaign'] === '') && ($_POST['campaign'] !== 'new')) ? $_POST['campaign'] : $_POST['new_campaign'] ;
        $template = $_POST['template'];
        $ymlInputs = $_POST['yml_inputs'];

        $saveLog = array(
            'date' => $dateTime->format(self::DATE_FORMAT),
            'local' => array(),
            'zipLog' => array(),
            'production' => array(),
            'previewGeneration' => array()

        );

        $filesystem = $this->getFilesystem('local');
        if($filesystem->has($campaign)) {
            $filesystem->deleteDir($campaign);
        }
        $filesystem->createDir($campaign);

        $ymlInputsFileName = $campaign . DS . $template . '.yml';

        $yamlInputsDump = Yaml::dump($ymlInputs);
        $filesystem->write($ymlInputsFileName, $yamlInputsDump);

        $saveLog['zipLog'] = $this->saveCampaignUpload($filesystem, $campaign);

        $saveLog['production'] = $this->saveFilesToProduction($campaign);

        $saveLog['yamlInputsDump'] = $ymlInputs;

        $this->generatePreviewContent($campaign);

        $filesystem->deleteDir($campaign);

        return $saveLog;
    }


    /**
     * Save Campaign Upload
     * Associated Zip file will be unzipped and
     *
     * @param $filesystem
     * @param $campaign
     * @return array $zipLog
     * @throws Exception
     */
    protected function saveCampaignUpload($filesystem, $campaign ) {
        $zipLog = array(
            'hasZip' => false,
            'zipFilename' => ''
        );
        try {
            if($this->isValidUploadMimeType()) {
                $uploadedFilename = $_FILES['asset_file']['name'];

                $this->initLocalUploadPaths($campaign);
                if (move_uploaded_file($_FILES['asset_file']['tmp_name'], $this->localUploadPaths['filePath'])) {
                    $zip = new ZipArchive;
                    if ($zip->open($this->localUploadPaths['filePath'])) {
                        $zip->extractTo($this->localUploadPaths['path']);
                        $zip->close();
                    }
                    $filesystem->deleteDir($campaign . DS . '__MACOSX');
                    $filesystem->delete($campaign . DS . $this->localUploadPaths['uploadName']);
                    $zipLog['hasZip'] = true;
                    $zipLog['zipFilename'] = $uploadedFilename;
                }
            }
        } catch(Exception $e) {
            echo $e->getMessage();
        }
        return $zipLog;

    }

    /**
     * Save Files To Production
     * Copy files from local filesystem, cleanup any existing file on remote filesystem.
     * Any remote files existing in the same relative path that do not exist in local, will be cleaned up.
     *
     * @param $path
     * @return array $saveToProductionLog
     */
    protected function saveFilesToProduction($path) {
        $path = (isset($path)) ? $path : '' ;
        try {
            if($path === '') {
                throw new Exception('No path given for copy to production. Root can not be copied.');
            }
            $saveToProductionLog = array(
                'directory_exists' => false,
                'files_removed' => array(),
                'files_existed' => array(),
                'files_saved' => array()
            );
            $local = $this->getFilesystem('local');
            $aws = $this->getFilesystem('aws');

            $localContents = $local->listContents($path);
            $filteredContents = $this->filterFileListContents($localContents, 'file');

            if(!$aws->has($path)) {
                $aws->createDir($path);
            } else {
                $saveToProductionLog['directory_exists'] = true;
                $awsContents = $aws->listContents($path);
                if(count($_FILES) > 0) {
                    $cleanupPathList = $this->cleanupPathList($awsContents, $localContents);
                    $filesRemoved = $this->cleanupFilesystemFromList($aws, $cleanupPathList);
                    $saveToProductionLog['files_removed'] = $filesRemoved;
                }
            }

            foreach($filteredContents as $index => $fileInfo) {
                if($aws->has($fileInfo['path'])) {
                    $aws->delete($fileInfo['path']);
                    $saveToProductionLog['files_saved'][] = $fileInfo['path'];
                }
                $currentFileContents = $local->read($fileInfo['path']);
                $aws->write($fileInfo['path'], $currentFileContents);
                $saveToProductionLog['files_saved'][] = $fileInfo['path'];
            }
            return $saveToProductionLog;
        } catch(Exception $e) {
            echo $e->getMessage();
        }
        return array();
    }

    /**
     * New Unique Identifier
     *
     * @return string
     */
    protected function newUniqueIdentifier() {
        return uniqid(md5(time().rand()), false);
    }

    /**
     * Cleanup Filesystem From List
     * Takes what is returned from self::cleanupPathList() and looks to remove those files.
     *
     * @param $filesystem
     * @param $cleanupPathList
     * @return array $cleanupList
     */
    protected function cleanupFilesystemFromList($filesystem, $cleanupPathList) {
        $cleanUpLog = array();
        foreach($cleanupPathList as $index => $pathname) {
            if($filesystem->has($pathname)) {
                $filesystem->delete($pathname);
                $cleanUpLog[] = $pathname;
            }
        }
        return $cleanUpLog;
    }

    /**
     * Cleanup Path List
     * Compares two sets of Flysystem\Filesystem::listContents() results.
     * Returns a list of the difference.
     *
     * @param $filesToClean
     * @param $filesToCompare
     * @return array
     */
    protected function cleanupPathList($filesToClean, $filesToCompare) {
        $compareList = array();
        $cleanupList = array();

        foreach($filesToCompare as $index => $fileinfo) {
            $compareList[] = $fileinfo['path'];
        }

        foreach($filesToClean as $cindex => $cfileinfo) {
            if(!in_array($cfileinfo['path'], $compareList)) {
                $cleanupList[] = $cfileinfo['path'];
            }
        }

        return $cleanupList;
    }

    /**
     * Delete Files By Extension
     *
     * @param $fileSystem
     * @param $path
     * @param $extension
     */
    protected function deleteFilesByExtension($fileSystem, $path, $extension) {
        $listContents = $fileSystem->listContents($path);
        foreach($listContents as $fileKey => $fileInfo) {
            if($fileInfo['extension'] == $extension) {
                $fileSystem->delete($fileInfo['path']);
            }
        }

    }

    /**
     * Filter File List Contents
     * Returns a filtered list of Flysystem\Filesystem::listContents() of given extension types
     *
     * @param array $fileContents
     * @param string || array $filter
     * @return array
     */
    protected function filterFileListContents($fileContents, $filter) {
        $filter = (is_array($filter)) ? $filter : array($filter);
        $filteredListContents = array();
        foreach($fileContents as $index => $fileInfo) {
            if(in_array($fileInfo['type'], $filter)) {
                $filteredListContents[] = $fileInfo;
            }
        }
        return $filteredListContents;
    }

    /**
     * Get Campaign Options
     * A [key => value] list of available AWS campaigns
     *
     * @return array $campaignOptions
     */
    public function getCampaignOptions() {
        $aws = $this->getFilesystem('aws');
        $bucketContents = $aws->listContents();
        $campaignOptions = array();
        foreach($bucketContents as $index => $bucketItem) {
            if($bucketItem['type'] == 'dir') {
                $valueAndKey = $bucketItem['filename'];
                $campaignOptions[$valueAndKey] = $valueAndKey;
            }
        }
        return $campaignOptions;
    }

    /**
     * Get Template Options
     * A list of available local templates
     *
     * @param string $ext
     * @return array $templateOptions
     */
    public function getTemplateOptions($ext = 'twig') {
        $templatePath = $this->getTemplatePath();
        $localAdapter = new LocalAdapter($templatePath);
        $fileSystem = new Filesystem($localAdapter);

        $templatesListing = $fileSystem->listContents();
        $templateOptions = array();

        foreach($templatesListing as $index => $listingItem) {
            if($listingItem['extension'] === $ext) {
                $templateOptions[$listingItem['filename']] = $this->templateOptionDisplayName($listingItem['filename']);
            }
        }
        return $templateOptions;
    }

    /**
     * Template Options Display Name
     * Turns a filename with underscores into a display friendly value - no extensions
     * e.g. my_filename -> My Filename
     *
     * @param $basename
     * @return string
     */
    public function templateOptionDisplayName($basename) {
        return ucwords(str_replace('_', ' ', $basename));
    }

    /**
     * Get Yml Template Data
     * Reads the contents of a local YML template
     *
     * @param $firstTemplateKey
     * @return bool|false|string
     */
    public function getYmlTemplateData($firstTemplateKey) {
        $templatePath = $this->getTemplatePath();
        $localAdapter = new LocalAdapter($templatePath);
        $fileSystem = new Filesystem($localAdapter);
        $ymlData = $fileSystem->read($firstTemplateKey . '.yml');
        return $ymlData;
    }

    /**
     * Get Local Upload Paths
     *
     * @param string $campaignDirectoryName
     * @throws Exception
     */
    protected function initLocalUploadPaths($campaignDirectoryName) {
        $localCampaignDir = $this->getFilesystem('local');
        $this->localUploadPaths['path'] = $this->getLocalBaseDir() . DS. $campaignDirectoryName;
        $this->localUploadPaths['uploadName'] = $this->getUploadName();
        $this->localUploadPaths['filePath'] = $this->localUploadPaths['path'] . DS . $this->localUploadPaths['uploadName'];
        if(!$localCampaignDir->has($campaignDirectoryName)) {
            $localCampaignDir->createDir($campaignDirectoryName);
        }
    }

    /**
     * Formatted File List
     * Utility for formatting file list contents from Flysystem file abstraction API - Filesystem::listContents();
     *
     * @param array $listContents
     * @return array $listContents
     */
    public function formattedFileList($listContents) {
        $dateTime = $this->getLocalDateTime();

        foreach($listContents as $index => $value) {
            if (isset($value['timestamp'])) {
                $dateTime->setTimestamp($value['timestamp']);
                $listContents[$index]['date'] = $dateTime->format(self::DATE_FORMAT);
            } else {
                $listContents[$index]['date'] = 'Unavailable';
            }
            $listContents[$index]['formattedsize'] = (isset($value['size'])) ? $this->formatBytes($value['size']) : '0' ;
        }
        return $listContents;
    }

    /**
     * Format Bytes
     *
     * @param $size
     * @param int $precision
     * @return string
     */
    protected function formatBytes($size, $precision = 1) {
        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }

    /**
     * Get Upload Name
     *
     * @return string|bool
     * @throws Exception
     */
    public function getUploadName() {
        try {

            $extension = $this->uploadExtension();

            if(!in_array($extension, $this->validExtensions())) {
                throw new Exception('Invalid file extension. Only ' . implode(', ' ,$this->validExtensions()) . ' are allowed.');
            }

            $uploadName = uniqid(self::UPLOAD_PREFIX) . '.' . $extension;
            return $uploadName;

        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return false;
    }

    /**
     * Get File System
     * Some abstraction for calling different filesystem types within this helper
     *
     * @param string $filesystemType
     * @calls $this->$methodName()
     * @return $this->$methodName()
     */
    public function getFilesystem($filesystemType) {
        try {

            $methodNamePart = ucfirst(strtolower($filesystemType));
            $methodName = 'get' . $methodNamePart . 'Filesystem';
            if(!method_exists($this, $methodName) || !$filesystemType) {
                $className = get_class($this);
                throw new Exception('Filesystem ' . $filesystemType . ' is not currently supported.');
            }

            $filesystem = $this->$methodName();
            return $filesystem;

        } catch(Exception $e) {
            echo $e->getMessage();
            die;
        }
    }

    /**
     * Get AWS File System
     *
     * @return mixed League\Flysystem\Filesystem $fileSystem|bool
     */
    protected function getAwsFilesystem() {
        try {

            $this->initAwsConfig();
            $awsClientConfig = self::getAwsClientConfig();
            $bucketName = self::getAwsBucketName();

            $client = new CDNclient($awsClientConfig);
            $adapter = new CDNfiles($client, $bucketName);
            $fileSystem = new FileSystem($adapter, ['ACL' => 'public-read']);
            return $fileSystem;

        } catch(Exception $e) {
            echo $e->getMessage();
        }
        return false;
    }

    /**
     * Get Local File System Campaign Dir
     *
     * @return League\Flysystem\Filesystem $fileSystem
     */
    protected function getLocalFilesystem() {
        $campaignsBaseDir = $this->getLocalBaseDir();
        $localAdapter = new LocalAdapter($campaignsBaseDir);
        $fileSystem = new Filesystem($localAdapter);
        return $fileSystem;
    }

    /**
     * Get Local Base Dir
     * Returns absolute local campaign assets directory path
     *
     * @return string
     */
    protected function getLocalBaseDir() {
        $relative_path = Mage::getStoreConfig(self::CORE_CONFIG_PATHS['local_filesystem']['campaign_path']);
        $absolute_path =  Mage::getBaseDir() . DS . $relative_path;
        return $absolute_path;
    }

    /**
     * Load AWS configuration values from the database
     *
     * @return bool
     * @throws Exception
     */
    protected function initAwsConfig() {
        try {
            $coreConfigPaths = self::CORE_CONFIG_PATHS['awscredential'];
            $coreConfigValues = array();

            foreach($coreConfigPaths as $key => $path) {
                $coreConfigValues[$key] = Mage::getStoreConfig($path);
            }

            if (count($coreConfigValues) != count($coreConfigPaths)) {
                throw new Exception('Missing configuration values. Check configuration table.');
            }

            $this->awsConfig['client']['credentials']['key'] = $coreConfigValues['key'];
            $this->awsConfig['client']['credentials']['secret'] = $coreConfigValues['secret'];
            $this->awsConfig['client']['region'] = $coreConfigValues['region'];
            $this->awsConfig['client']['version'] = $coreConfigValues['version'];
            $this->awsConfig['bucket'] = $coreConfigValues['bucket'];

            return true;

        } catch(Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Initialize Twig Template Environment
     */
    protected function initTwigEnvironment() {
        $templatePath = $this->getTemplatePath();

        $loader = new Twig_Loader_Filesystem($templatePath);
        $this->twig = new Twig_Environment($loader, array(
            'cache' => false
        ));

    }

    /**
     * Render Twig
     * Combine twig template with values and returns the result for output
     *
     * @param $twigTemplateName
     * @param $templateVars
     * @return string
     * @throws Exception
     */
    protected function renderTwig($twigTemplateName, $templateVars) {
        $this->initTwigEnvironment();
        return $this->twig->render($twigTemplateName. '.twig', $templateVars);
    }

    /**
     * Get Environment Values
     *
     * @param string $env
     * @return array $environmentValues
     */
    public function getEnvironmentValues($env) {
        $baseHrefConfigPath = ($env === 'aws') ? self::CORE_CONFIG_PATHS['awscredential']['base_url'] : 'web/secure/base_url' ;
        $baseUrl = Mage::getStoreConfig($baseHrefConfigPath);

        $environmentValues = array(
            'baseUrl' => $baseUrl
        );

        return $environmentValues;
    }

    /**
     * Valid Extensions
     *
     * @return array
     */
    protected function validExtensions() {
        return array_keys(self::VALID_UPLOAD_TYPES);
    }

    /**
     * Is Valid Upload Mime Type
     *
     * @return bool
     */
    protected function isValidUploadMimeType() {
        if((count($_FILES) == 0)) {
            return false;
        }
        return (mime_content_type($_FILES['asset_file']['tmp_name']) === self::VALID_UPLOAD_TYPES[$this->uploadExtension()]);
    }

    /**
     * Upload Extension
     *
     * @return mixed
     */
    protected function uploadExtension() {
        return pathinfo($_FILES['asset_file']['name'], PATHINFO_EXTENSION);
    }
    /**
     * Get Template Path
     *
     * @return string $templatePath
     */
    protected function getTemplatePath() {
        $templatePath = Mage::getBaseDir() . DS. self::TEMPLATE_PATH;
        return $templatePath;
    }

    /**
     * Get Twig Cache Path
     *
     * @return string $templatePath
     */
    public function getTemplateCachePath() {
        $cachePath = Mage::getBaseDir() . DS . self::TEMPLATE_CACHE;
        return $cachePath;
    }

    /**
     * Get Aws Client Config
     *
     * @return array
     */
    protected function getAwsClientConfig() {
        return $this->awsConfig['client'];
    }

    /**
     * Get Aws Client Config
     *
     * @return string
     */
    protected function getAwsBucketName() {
        return $this->awsConfig['bucket'];
    }

    /**
     * Get Local Date Time
     *
     * @return DateTime()
     */
    protected function getLocalDateTime() {
        $dateTime = new DateTime();
        $timeZone = new DateTimeZone(Mage::getStoreConfig('general/locale/timezone'));
        $dateTime->setTimezone($timeZone);
        return $dateTime;
    }
}