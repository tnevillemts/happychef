
# Ripen MailFoundry (v1.0.0)
A email generation tool for Magento 1.x via the admin UI. Uses YAML combined with associated asset ZIP files, to generate both test and production email output on Amazon S3.
 
 ### Getting Started
 After installing this extension, some basic configuration will be required. The following assumes a defaults style admin Magento UI theme.
 1. Log into the Admin UI
 2. From the top menu, look under System >> Configuration
 3. On the left, look under Ripen >> Mail Foundry
 4. Fill out appropriate configuration values
 
 #### Required Configuartion Values
 Before use, some fields do not have default settings, and are listed below. After filling out these fields, also check that the default values match what is appropriate for your current environment.
 
 ##### Local Filesystem
 - Production URL
 
 ##### Amazon CDN Credentials
 - Key
 - Secret
 - Bucket
 
 ### Production Use
 In the top menu look under Newsletter >> Email Generation. The form to generate email is on the left. The remote filesystem browser title "Live" is on the right, these will be the files on Amazon S3/AWS.
 
 ##### Upload Email Assets Fields
 - **Campaign** - This will be the name/key that will be the unique identifier of the email generated. Only uppercase letters, numbers, and dashes are allowed. Validation will force this.
 - **Template** - Template selection, changing this will load in the appropriate blank YAML template in the _YAML Data_ textarea below.
 - **YAML Data** - Fill in your variables for the email generated. It is **NOT recommended** to cut and past in YAML in this field. If you happen to paste in YAML template contents that mismatches the selected template, email generation will be broken or inaccurate.
 - **Asset Zip File** - A ZIP (.zip) file of associated email assets such as images goes here. Only the extension of .ZIP is accepted, but not required.
 
 ### Creating a New Campaign
 1. Select "New" as the _Campaign_
 2. Fill out the "New Campaign Title" text input to the right
 3. Select a _Template_
 4. Fill out the _YAML Data_ with valid YAML
 5. Select a zip file for _Asset Zip File_
 6. Click _Save Email Campaign_
 
 ### Update an Existing Campaign
 1. Select a _Campaign_ that is now "New"
 2. Update the information you want to change
 3. Attach a zip file if you want to update the images
 
 #### How Updates Work
 Updating is simple.
 - To change the _YAML Data_ in the email and nothing else, simply change what is needed, and submit. The assets that already exist will remain.
 - To update the assets only, simply select a new Zip file conatining **ALL assets** for the _Asset Zip File_ field and submit. The existing YAML will be left as is
 - To update both, do both
 
 **NOTE:** When adding/updating assets, any images that do not exist in the zip file update, will be deleted, so it is required that any zip file updates, subsequently contain all desired assets. The images must be directly in the root of the zip, no sub-folders. This procedure helps keep the file system tidy.
 
 ## Adding Templates
 In Ripen MailFoundry (v1.0.0), there is no where in the admin UI to add a template. Adding a template requires two main components, one YAML (.yml) and one TWIG (.twig) which must match exactly in their base file names minus the extension. The base filename should follow the naming convention of, all lowercase letters, and underscores where spaces would be.
 
 ##### Creating a template option "My New Template"
 From the base path of your Magento installation, under the _email/templates_ directory, just add .yml and one .twig file with the same base file name of **my_new_template** - these files should match in context for proper compilation.
 
###### Example template setup
- Add:: _MAGENTO_BASE_DIR_/email/templates/my_new_template.yml
- Add:: _MAGENTO_BASE_DIR_/email/templates/my_new_template.twig
- Now the _Template_ field wikl have the option of "My New Template"
- .yml is the only acceptable YAML extension.

### Notes
- Only one .yml file should exist in each S3 folder, as the first .yml encountered will be passed to the parser
- In the unlikely event an extra or mismatching .yml is placed on S3, a direct admin login to S3 will be required to remove the offending file(s)
- No local delete operations were included to prevent unwanted file removal, which could easily cause production issues
 
 ### Libraries
 - Amazon AWS SDK for PHP - <https://aws.amazon.com/sdk-for-php/>
 - Symfony (for YAML parsing) - <https://symfony.com/>
 - Twig Template Engine - <https://twig.symfony.com/>
 - Flysystem Filesystem API - <http://flysystem.thephpleague.com/>
 - Knockout.js - <http://knockoutjs.com/>
