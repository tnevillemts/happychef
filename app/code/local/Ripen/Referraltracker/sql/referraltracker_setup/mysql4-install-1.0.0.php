<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'),'ripen_referral', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Referral Link'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'),'ripen_referral', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Referral Link'
    ));

/*

 $installer->run("
        CREATE TABLE IF NOT EXISTS {$this->getTable('ripen_referrals')} (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `referral_name` varchar(128) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
    ");

$installer->getConnection()
    ->addIndex(
        $installer->getTable('sales/quote_item'),
        'IDX_RIPEN_REFERRAL',
        'ripen_referral'
    );

$installer->getConnection()
    ->addIndex(
        $installer->getTable('sales/order_item'),
        'IDX_RIPEN_REFERRAL',
        'ripen_referral'
    );
*/

$installer->endSetup();
