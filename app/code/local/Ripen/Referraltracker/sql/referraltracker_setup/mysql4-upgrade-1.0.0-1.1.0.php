<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addIndex(
        $installer->getTable('sales/quote_item'),
        'IDX_RIPEN_REFERRAL',
        'ripen_referral'
    );

$installer->getConnection()
    ->addIndex(
        $installer->getTable('sales/order_item'),
        'IDX_RIPEN_REFERRAL',
        'ripen_referral'
    );

$installer->endSetup(); 