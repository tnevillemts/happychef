<?php
class Ripen_Referraltracker_Model_Observer {

    public function saveRipenReferral($observer)
    {
        // Save the referral data against the quote
        $quoteItem = $observer->getQuoteItem();
        $quoteItem->setRipenReferral(Mage::app()->getRequest()->getParam("ripen_referral"));
    }
}
