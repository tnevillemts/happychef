<?php

class Ripen_BetterCheckout_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CUSTOM_QUOTE_SUFFIX = 'Custom Quote';

    /**
     * Return data about available shipping methods for a specified address object.
     * @param Mage_Sales_Model_Quote_Address $_address
     * @return array
     */
    public function getAvailShippingMethods($_address)
    {
        $_groups = $_address->getGroupedAllShippingRates();

        $_isPostOfficeBox = (bool)preg_match('/((p(ost(al)?|\.)?\s*)?(o(ffice|\.)?\s+)?)?box\s+(#|no|num(ber)?)?\s*\d+/i', $_address->getStreet(-1));
        $_options = [];
        $_specialNote = "";
        $_hasCustomQuote = false;

        foreach ($_groups as $_rates) {
            foreach ($_rates as $_rate) {
                $_methodTitle = $_rate->getMethodTitle();
                $_isCustomQuote = (strpos($_methodTitle, self::CUSTOM_QUOTE_SUFFIX) !== false);

                // Handle normal shipping methods.
                if ($_isCustomQuote) {
                    $_hasCustomQuote = $_hasCustomQuote || true;
                    $_price = "Call for Quote";
                    $_methodTitle = trim(str_replace(self::CUSTOM_QUOTE_SUFFIX, '', $_methodTitle));
                // Handle special case where Happy Chef needs to provide a quote manually.
                } else {
                    $_price = Mage::helper('core')->currency($_rate->getPrice(), true, false);
                }

                if (! $_isPostOfficeBox) {
                    $_methodTitle = $this->remapShippingMethodTitle($_methodTitle);
                }

                $_options[] = [
                    'code' => $_rate->getCode(),
                    'title' => $_methodTitle,
                    'numericPrice' => $_rate->getPrice(),
                    'price' => $_price,
                    'selected' => $_address->getShippingMethod() == $_rate->getCode(),
                    'custom' => $_isCustomQuote
                ];
            }
        }

        // Treat custom quotes as if they were the most expensive and sort last after any known prices.
        usort($_options, function ($a, $b) {
            $aPrice = $a['custom'] ? PHP_INT_MAX : $a['numericPrice'];
            $bPrice = $b['custom'] ? PHP_INT_MAX : $b['numericPrice'];
            if ($aPrice === $bPrice) return 0;
            return ($aPrice < $bPrice) ? -1 : 1;
        });

        // If shipping address is a post office box, show a warning message.
        if ($_isPostOfficeBox) {
            $_specialNote = "Please note: We can offer only standard shipping to P.O. boxes.";
        }

        if ($_hasCustomQuote) {
            $_specialNote = "A shipping quote for your location is not available online at this time. Please complete your order and our customer service team will contact you with the shipping cost.";
        }

        return [
            'options' => $_options,
            'specialNote' => $_specialNote
        ];
    }

    public function remapShippingMethodTitle($title)
    {
        if (strtolower($title) == "3 day") {
            $title = "3 Business Day Air";
        } elseif (strtolower($title) == "2 day") {
            $title = "2 Business Day Air";
        } elseif (strtolower($title) == "overnight") {
            $title = "Next Business Day Air";
        }

        return $title;
    }

    public function remapCountryToRegion($country)
    {
        $region = Mage::getModel('directory/region')->getCollection()->addFieldToFilter('code', $country)->getLastItem();
        return $region ? $region->getRegionId() : false;
    }
}
