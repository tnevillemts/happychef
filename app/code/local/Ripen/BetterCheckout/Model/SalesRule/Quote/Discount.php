<?php

class Ripen_BetterCheckout_Model_SalesRule_Quote_Discount extends Mage_SalesRule_Model_Quote_Discount
{
    public function __construct()
    {
        parent::__construct();
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getDiscountAmount();

        if ($amount!=0) {
            $title = Mage::helper('sales')->__('Discount');
            $code = $address->getDiscountDescription();
            if (strlen($code)) {
                $title = Mage::helper('sales')->__('%s', $code);
            }
            $address->addTotal(array(
                'code'=>$this->getCode(),
                'title'=>$title,
                'value'=>-$amount
            ));
        }
        return $this;
    }

}
