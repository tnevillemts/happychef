<?php
/**
 * Created by PhpStorm.
 * User: zcox
 * Date: 6/14/18
 * Time: 11:12 AM
 */

class Ripen_BetterCheckout_Block_Cart extends Mage_Checkout_Block_Cart
{
    public function chooseTemplate()
    {
        $hasSavedCart = false;
        $_customer = Mage::getSingleton('customer/session')->getCustomer();
        if ($_customer->getId()) {
            $hasSavedCart = count(Mage::getModel('shoppinglist/group')->getGroupByCustomer($_customer->getId())) > 0 ? true : false;
        }

        $itemsCount = $this->getItemsCount() ? $this->getItemsCount() : $this->getQuote()->getItemsCount();
        if ($itemsCount || $hasSavedCart) {
            $this->setTemplate($this->getCartTemplate());
        } else {
            $this->setTemplate($this->getEmptyTemplate());
        }
    }

}