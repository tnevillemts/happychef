<?php

class Ripen_BetterCheckout_Block_Onepage_Shipping extends Mage_Checkout_Block_Onepage_Shipping
{
    protected function _construct()
    {
        parent::_construct();
    }

    public function getAddressesHtmlSelect($type)
    {
        if ($this->isCustomerLoggedIn()) {
            $options = array();
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $options[] = array(
                    'value' => $address->getId(),
                    'label' => $address->format('oneline')
                );
            }

            $addressId = $this->getAddress()->getCustomerAddressId();
            if (empty($addressId)) {
                if ($type=='billing') {
                    $address = $this->getCustomer()->getPrimaryBillingAddress();
                } else {
                    $address = $this->getCustomer()->getPrimaryShippingAddress();
                }
                if ($address) {
                    $addressId = $address->getId();
                }
            }


            $selectHtml = "<select name='{$type}_address_id' id='{$type}-address-select' style='width: 100%'>";
            foreach($options as $option){
                $selected = ($addressId == $option['value']) ? "selected" : "";
                $selectHtml .= "<option value='".$option['value']."' {$selected}>".$option['label']."</option>";
            }
            $selectHtml .="<option value=''>Add New Address...</option>";
            $selectHtml .="</select>";
            return $selectHtml;

        }
        return '';
    }

    public function getCountryHtmlSelect($type)
    {
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        $address = $quote->getShippingAddress();

        //$countryId = $this->getAddress()->getCountryId();
        $countryId = $address->getCountryId();

        if (is_null($countryId)) {
            $countryId = Mage::helper('core')->getDefaultCountry();
        }

        $options = $this->getCountryOptions();

        $selectHtml = "<select name='{$type}[country_id]' id='{$type}:country_id' style='width: 100%' onchange='if (window.bettercheckout) bettercheckout.renderRegions(this.value, \"".$type."\")'>";
        foreach($options as $option){
            if($option['value']){
                $selected = ($countryId == $option['value']) ? "selected" : "";
                $selectHtml .= "<option value='".$option['value']."' {$selected}>".$option['label']."</option>";
            }
        }
        $selectHtml .="</select>";
        return $selectHtml;

    }

}
