<?php
class Ripen_BetterCheckout_Block_Onepage_Checkout extends Mage_Checkout_Block_Onepage_Shipping
{
	protected function _construct()
	{
		parent::_construct();
	}

public function getAddressesHtmlSelect($type)
{
     if ($this->isCustomerLoggedIn()) {
         $options = array();

          $customerId = $this->getCustomer()->getId();
          $customer = Mage::getModel('customer/customer')->load($customerId);

          $defaultAddressId = null;
          if($type == 'billing') {
              $defaultAddress = $customer->getDefaultBillingAddress();
          } else {
              $defaultAddress = $customer->getDefaultShippingAddress();
          }

          foreach ($this->getCustomer()->getAddresses() as $address) {
              $prefix = '';
              if($address->getEntityId() == $defaultAddress->getEntityId()) {
                  $prefix = '(Default) ';
              }
              $options[] = array(
                  'value' => $address->getId(),
                  'label' => $prefix . $address->format('oneline')
              );
          }

          if ($type == 'billing') {
              if ($this->getCustomer()->getPrimaryBillingAddress()) {
                  $addressId = $this->getCustomer()->getPrimaryBillingAddress()->getId();
              } else {
                  $addressId = $this->getCustomer()->getPrimaryShippingAddress()->getId();
              }
          } else {
              $addressId = $this->getAddress()->getCustomerAddressId();
              if(empty($addressId)) {
                  $addressId = $this->getCustomer()->getPrimaryShippingAddress()->getId();
              }
          }

          $selectHtml = "<select name='{$type}_address_id' id='{$type}-address-select' style='width: 100%' onchange='bettercheckout.prefillAddress(\"{$type}\")'>";
          foreach($options as $option){
              $selected = ($addressId == $option['value']) ? "selected" : "";
              $selectHtml .= "<option value='".$option['value']."' {$selected}>".$option['label']."</option>";
          }
          $selectHtml .="<option value=''>Add New Address...</option>";
          $selectHtml .="</select>";
          return $selectHtml;
      }
      return '';
}

	public function getCountryHtmlSelect($type)
	{
		$cart = Mage::getSingleton('checkout/cart');
		$quote = $cart->getQuote();
		$address = $type == 'shipping' ? $quote->getShippingAddress() : $quote->getBillingAddress();

		$countryId = $address->getCountryId();

		if (is_null($countryId)) {
			$countryId = Mage::helper('core')->getDefaultCountry();
		}

		$options = $this->getCountryOptions();

		$selectHtml = "<select name='{$type}[country_id]' id='{$type}:country_id' style='width: 100%' class='validate-select disabled' onchange='if (window.bettercheckout) bettercheckout.renderRegions(this.value, \"".$type."\")'>";
		foreach($options as $option){
			if($option['value']){
				$selected = ($countryId == $option['value']) ? "selected" : "";
				$selectHtml .= "<option value='".$option['value']."' {$selected}>".$option['label']."</option>";
			}
		}
		$selectHtml .="</select>";
		return $selectHtml;

	}

	public function hasOnlyEmailGiftCard () {
	    $giftCardType = 'aw_giftcard';
		$quote = Mage::helper('checkout/cart')->getQuote();
		$allItems = $quote->getAllItems();
		foreach($allItems as $item) {
		    if($item->getProductType() != $giftCardType) {
		        return false;
            }
        }
        return true;
	}

	public function hasEmbroidery ()
	{
		$cart = Mage::getSingleton('checkout/cart');
		$quote = $cart->getQuote();

		foreach ($quote->getAllItems() as $item) {
			if (count(Mage::helper('catalog/product_configuration')->getCustomOptions($item))) {
				return true;
			}
		}
		return false;
	}

	public function getMethods()
	{
		$methods = $this->getData('methods');
		if ($methods === null) {
			$cart = Mage::getSingleton('checkout/cart');
			$quote = $cart->getQuote();
			$store = $quote ? $quote->getStoreId() : null;
			$methods = array();
			foreach ($this->helper('payment')->getStoreMethods($store, $quote) as $method) {
				if ($method->isApplicableToQuote(
					$quote,
					Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL
				)) {
					$this->_assignMethod($method);
					$methods[] = $method;
				}
			}
			$this->setData('methods', $methods);
		}
		return $methods;
	}

	public function getPaymentMethodFormHtml(Mage_Payment_Model_Method_Abstract $method)
	{
		return $this->getChildHtml('payment.method.' . $method->getCode());
	}


	/**
	 * Check and prepare payment method model
	 *
	 * Redeclare this method in child classes for declaring method info instance
	 *
	 * @param Mage_Payment_Model_Method_Abstract $method
	 * @return bool
	 */
	protected function _assignMethod($method)
	{
		$method->setInfoInstance($this->getQuote()->getPayment());
		return $this;
	}
}
