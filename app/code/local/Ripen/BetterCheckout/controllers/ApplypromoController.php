<?php
/**
 * Created on: 09.19.16.
 * @copyright Ripen E-Commerce
 * @author     Ripen Dev Team
 */

class Ripen_Bettercheckout_ApplypromoController extends Mage_Core_Controller_Front_Action
{
    /**
     * Applies promo or gift card code and renders totals
     * @return JSON object
     */
    public function indexAction()
    {
        $isCart = stristr($this->_getRefererUrl(), "/cart");
        $promocode = $this->getRequest()->getParam('promocode');
        $expirationDate = date("Y/m/d H:i:s");

        // Identify provided code type (promo code vs. gift card)
        $gcPattern1 = "/^(\w{3}-\w{3}-\w{3}-\w{3})$/i";
        $gcPattern2 = "/^(GC)\d{11}$/i";

        // Gift card case
        if (preg_match($gcPattern1, $promocode) || preg_match($gcPattern2, $promocode)) {

            $giftcardModel = Mage::getModel('aw_giftcard/giftcard')->loadByCode(trim($promocode));

            try {
                if ($giftcardModel->getId() && $giftcardModel->isValidForRedeem()) {

                    Mage::register('current_giftcard', $giftcardModel, true);
                    Mage::helper('aw_giftcard/totals')->addCardToQuote($giftcardModel);

                    $quote = Mage::helper('aw_giftcard/totals')->getQuote();
                    $quote->setTotalsCollectedFlag(false)->collectTotals()->save();

                    $status = true;
                    $message = "Gift card has been applied.";
                } else {
                    $quote = Mage::helper('aw_giftcard/totals')->getQuote();
                    $status = false;
                    $message = "Please check your gift card code and try again.";
                }
            }catch( Exception $e) {
               $quote = Mage::helper('aw_giftcard/totals')->getQuote();
               $status = false;
               $message = "Please check your gift card code and try again.";
            }

        } else {
            // If provided code didn't match the gift card pattern (above), we assume
            // that a promo code (aka coupon code) was used

            $salesRule = Mage::getModel('salesrule/coupon')->load($promocode, 'code');
            $salesRuleExists = count($salesRule->getData());
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();

            if ($salesRuleExists) {
                $date = date('Y-m-d h:i:s');
                $expirationDate = $salesRule->getExpirationDate();
                $couponIsExpired = $date > $expirationDate;
                if  (!$couponIsExpired || is_null($expirationDate)) {
                    Mage::getSingleton("checkout/session")->setData("coupon_code", $promocode);
                    $quote->setCouponCode($promocode)
                        ->setTotalsCollectedFlag(false)
                        ->collectTotals()
                        ->save();
                    $quoteCouponCode = $quote->getCouponCode(); //get applied coupon code from the quote
                    if($quoteCouponCode) {
                        $message = "The promo code has been applied.";
                        $status = true;
                    } else {
                        $message = "The promo code could not be applied.";
                        $status = false;
                    }
                } else {
                    $message = "This promo code has expired.";
                    $status = false;
                }
            } else {
                $status = false;
                if ($isCart) {
                    $message = "If you meet all criteria, discount will be applied at checkout.";
                } else {
                    $message = "The promo code is invalid.";
                }
            }
        }

        $block = Mage::app()->getLayout()->createBlock('checkout/cart_totals');

        $updatedRows = $block->renderTotals();
        $totalsBlock = $block->setTemplate('ripen/bettercheckout/cart/totals.phtml')->toHtml();

        // This is sort of a heck. For some reason the totalsBlock returns only tax and grand total.
        // So we have to inject missing extra lines (subtotal, discount, gift card, etc.)
        $totalsBlock = str_replace("<tfoot>", "<tfoot>".$updatedRows, $totalsBlock);

        $response = array('status'=>$status, 'message'=>$message, 'totals'=>$totalsBlock, 'grandtotal'=>$quote->getGrandTotal(), 'expiration_date'=>$expirationDate);
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(json_encode($response));
    }
}
