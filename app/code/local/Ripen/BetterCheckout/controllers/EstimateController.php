<?php
/**
 * Created on: 09.19.16.
 * @copyright Ripen E-Commerce
 * @author	 Ripen Dev Team
 */

class Ripen_Bettercheckout_EstimateController extends Mage_Core_Controller_Front_Action
{
    /**
     * Retrieve shipping cost for provided country and state
     * @return JSON object
     */
    public function indexAction()
    {
        // Retrieve parameters sent via ajax post
        $country  = (string) $this->getRequest()->getParam('country_id');
        $regionId = (string) $this->getRequest()->getParam('region_id');
        $region	  = (string) $this->getRequest()->getParam('region');

        // Remap PR and VI countries to corresponding US regions
        if (in_array($country, ['PR','VI'])) {
            $regionId = Mage::helper('bettercheckout')->remapCountryToRegion($country);
            $country = "US";
        }

        // Retrieve shopcart and set parameters
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        $address = $quote->getShippingAddress();

        $address->setCountryId($country)
            ->setRegionId($regionId)
            ->setRegion($region)
            ->setCollectShippingRates(true);

        $address->collectTotals();
        $cart->save();

        $newShippingRates = false;

        // Establish if the cart contains only email gift card(s).
        $cartItems = $quote->getItemsCollection()->getItems();
        $emailGiftCardAlone = array_reduce($cartItems, function ($carry, $item) {
            return $carry !== false && $item->getProduct()->getSku() == 'emailgc';
        });

        // Retrieve delivery methods
        $methodData = Mage::helper('bettercheckout')->getAvailShippingMethods($address);
        $optionHtml = "";
        $freeShippingSelected = false;
        foreach ($methodData['options'] as $option) {
            if ($emailGiftCardAlone && $option['numericPrice'] != 0) continue;

            if ($option['selected']) {
                $selected = ' selected';

                if ($option['numericPrice'] == 0 && ! $option['custom']) $freeShippingSelected = true;
            } else {
                $selected = '';
            }

            if ($option['custom']) {
                $optionHtml .= "<option class='red-option' value='{$option['code']}'{$selected}>{$option['title']} - {$option['price']}</option>";
            } else {
                $optionHtml .= "<option value='{$option['code']}'{$selected}>{$option['title']} - {$option['price']}</option>";
            }
        }

        // Build dropdown containing shipping cost and delivery methods.
        // Have to include inline style due to the usage of select2 library (makes dropdowns fancy)
        $newSelect =
            "<select id='estimate_method' name='estimate_method' style='width:100%'>" .
            "<option value=''>Select shipping method...</option>" .
            $optionHtml .
            "</select>";

        if ($methodData['specialNote']) {
            $newSelect .= "<div class='shipping-alert-message'>{$methodData['specialNote']}</div>";
        }

        // Set JSON for ajax response
        $response = array(
            'dropdown' => $newSelect,
            // TODO: Hook this back up, as currently it's not actually set, even though it's used in JS.
            'newShippingRates' => $newShippingRates,
            'freeShippingSelected' => $freeShippingSelected
        );

        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(json_encode($response));
    }

    /**
     * Retrieve regions for provided country
     * @return JSON object
     */
    public function getRegionsAction()
    {
        // Retrieve shopcart and shipping address
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        $address = $quote->getShippingAddress();

        $options = "";
        $country	= (string) $this->getRequest()->getParam('country_id');

        // Retrieve regions by country
        $regionModel = Mage::getModel('directory/region');
        /** @var $collection Mage_Directory_Model_Resource_Region_Collection */
        $regions = $regionModel->getResourceCollection()
            ->addCountryFilter($country)
            ->addFieldToFilter('enabled', 1)
            ->load();

        foreach ($regions as $region) {
            $selected = ($address->getRegionId() == $region["region_id"]) ? "selected" : "";
            $options.= "<option value='{$region["region_id"]}' {$selected}>{$region["name"]}</option>";
        }

        // Build dropdown with regions option.
        // Have to include inline style due to the usage of select2 library (makes dropdowns fancy)
        $select = "<select id='region_id' name='region_id' style='width:100%'>
                <option value=''>Select state or province...</option>
                {$options}</select>";

        $select = (count($regions)) ? $select : "";
        $response = array('dropdown'=>$select);
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(json_encode($response));
    }

    /**
     * Updates totals after selecting a specific delivery method
     * @return array
     */
    public function estimateUpdateAction($estimate_method = null)
    {
        $status = false;
        $message = "";

        // Retrieve delivery method and update shopcart instance
        $code = (string) $this->getRequest()->getParam('estimate_method');
        $code = $code ? $code : $estimate_method;

        if ($code) {
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
            $address = $quote->getShippingAddress();
            $address->setShippingMethod($code)->collectTotals()->save();
            $cart->save();
            $status = true;
        }

        // Build a block for totals.phtml template that renders totals box
        $block = Mage::app()->getLayout()->createBlock('checkout/cart_totals');
        $updatedRows = $block->renderTotals();
        $totalsBlock = $block->setTemplate('ripen/bettercheckout/cart/totals.phtml')->toHtml();
        $totalsBlock = str_replace("<tfoot>", "<tfoot>".$updatedRows, $totalsBlock);

        $response = array(
            'status' => $status,
            'message' => $message,
            'totals' => $totalsBlock,
            'grandtotal' => Mage::helper('core')->currency($quote->getGrandTotal(), true, false)
        );
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(json_encode($response));

        return $response;
    }

    /**
     * Updates totals after removing gift card
     * @return JSON object
     */
    function removeGiftCardAction()
    {
        if ($giftcardCode = $this->getRequest()->getParam('giftcard_code', null)) {
            $giftcardCode = base64_decode($giftcardCode);

            Mage::helper('aw_giftcard/totals')->removeCardFromQuote(trim($giftcardCode));
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
            $address = $quote->getShippingAddress();
            $address->collectTotals();
            $cart->save();

            $block = Mage::app()->getLayout()->createBlock('checkout/cart_totals');
            $updatedRows = $block->renderTotals();
            $totalsBlock = $block->setTemplate('ripen/bettercheckout/cart/totals.phtml')->toHtml();
            $totalsBlock = str_replace("<tfoot>", "<tfoot>".$updatedRows, $totalsBlock);

            $response = array('status'=>$status, 'message'=>$message, 'totals'=>$totalsBlock, 'grandtotal'=>Mage::helper('core')->currency($quote->getGrandTotal(), true, false));
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }
}
