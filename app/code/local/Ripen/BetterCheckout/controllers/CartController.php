<?php

/**
 * Created on: 09.19.16.
 * @copyright Ripen E-Commerce
 * @author     Ripen Dev Team
 */
class Ripen_Bettercheckout_CartController extends Mage_Core_Controller_Front_Action
{
    public function updateCartAjaxAction()
    {
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();

        $itemId = $this->getRequest()->getParam('itemId');
        $qty = $this->getRequest()->getParam('qty');
        $embroideryData = $this->getRequest()->getParam('embroideryData');
        $hasLogoBeforeUpdate = $hasLogoAfterUpdate = false;

        if ($qty || $embroideryData) {
            $newItemTotal = "";
            $embroideryDisclaimer = "";

            $items = $cart->getItems();
            foreach ($items as $item) {
                if ($item->getId() == $itemId) {

                    if($hasLogoBeforeUpdate !== true){
                        $hasLogoBeforeUpdate = $item->hasLogoEmbroideryWithoutSetup();
                    }

                    // New embroidery data.
                    if ($embroideryData) {
                        $embroideryData = str_replace("\\", "", $embroideryData);

                        $option = $item->getProduct()->getCustomOption('info_buyRequest');
                        $optionArray = unserialize($option->getValue());
                        $optionArray["product-embroidery-options"] = $embroideryData;
                        $option->setValue(serialize($optionArray));

                        if ($option = $item->getOptionByCode('embroidery_options')) {
                            $option->setValue($embroideryData)->save();
                        } else {
                            $item->addOption(array(
                                'product_id' => $item->getProduct()->getId(),
                                'code' => 'embroidery_options',
                                'value' => $embroideryData
                            ));
                        }

                    }

                    if ($qty) {
                        $item->setQty($qty)->save();
                    }

                    // Fix embroidery data.
                    Mage::helper('embroidery')->applyQtyEmbroideryDiscounts($quote, $item, $qty);

                    $cart->save();

                    Mage::getSingleton('checkout/session')->setCartWasUpdated(true);

                    $newItemTotal = Mage::helper('core')->currency($item->getPriceInclTax() * $item->getQty(), true, false);
                    $newItemUnitPrice = Mage::helper('core')->currency($item->getPriceInclTax(), true, false);

                    $option = $item->getOptionByCode('simple_product');
                    if ($option) {
                        $product = $option->getProduct();
                    } else {
                        $product = $item->getProduct();
                    }
                    $stockStatusBlock = Mage::app()->getLayout()->createBlock('core/template')
                        ->setData('orderItem', $item)
                        ->setData('itemProduct', $product)
                        ->setTemplate('ripen/bettercheckout/cart/item/stockstatus.phtml')
                        ->toHtml();

                    if($hasLogoAfterUpdate !== true){
                        $hasLogoAfterUpdate = $item->hasLogoEmbroideryWithoutSetup();
                    }

                    $embroideryDisclaimer = Mage::helper('embroidery')->getEmbroideryDisclaimer($item->hasEmbroiderySaved(), $item->hasLogoEmbroideryWithoutSetup());
                    break;
                }

            }

            // Apply embroidery discount prices
            $updatedLineItemPrices = Mage::getModel('embroidery/embroidery')->applyEmbroideryDiscount();

            // Handle setup fee
            Mage::getModel('embroidery/observer')->handleEmbroiderySetupFee();

            $coupon_code = trim(Mage::getSingleton("checkout/session")->getData("coupon_code"));
            if ($coupon_code != '') {
                $quote->setCouponCode($coupon_code)->save();
                $quote->setTotalsCollectedFlag(false)->collectTotals();
            }

            $quote->setTotalsCollectedFlag(false)->collectTotals();

            // Build a block for totals.phtml template that renders totals box
            $block = Mage::app()->getLayout()->createBlock('checkout/cart_totals');
            $block->renderTotals();
            $totalsBlock = $block->setTemplate('ripen/bettercheckout/cart/totals.phtml')->toHtml();

            $ruleNotAppliedMessage = '';
            $appliedRuleIds = $quote->getAppliedRuleIds();
            if (!empty($coupon_code) && empty($appliedRuleIds)) {
                $ruleNotAppliedMessage = 'No promo code currently applied.';
            }

            $response = array(
                'updatedLineItemPrices' => $updatedLineItemPrices,
                'embroideryDisclaimer' => $embroideryDisclaimer,
                'newItemTotal' => $newItemTotal,
                'newItemUnitPrice' => $newItemUnitPrice,
                'stockStatus' => $stockStatusBlock,
                'totals' => $totalsBlock,
                'grandtotal' => Mage::helper('core')->currency($quote->getGrandTotal(), true, false),
                'coupon_code' => $coupon_code,
                'ruleNotAppliedMessage' => $ruleNotAppliedMessage,
                'requireReload' => true
            );

            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    /**
     * Remove Embroidery Custom Logo Action
     *
     * Used to remove ONLY the specific custom logo related to a specific embroidery setup fee quote item.
     * Both the embroidery_options and info_buyRequest should be synced up and saved.
     * The remaining embroidery selections are left intact.
     * Finally. removing the EMBROIDERY-SETUP associated with the logo removed.
     *
     */
    public function removeEmbroideryCustomLogoAction ()
    {
        if(!$this->getRequest()->isPost()) {
            return false;
        }

        try {
            $setupQuoteItemId = $this->getRequest()->getPost('setupQuoteItemId');
            $embroideryHelper = Mage::helper('embroidery');

            $cart = Mage::getSingleton('checkout/cart');
            $setupQuoteItem = Mage::getModel('sales/quote_item')->load($setupQuoteItemId);
            $customLogoPath = $setupQuoteItem->getAdditionalData();
            $quote = $cart->getQuote();
            $quoteItems = $quote->getAllItems();

            foreach($quoteItems as $item) {
                $embroideryOptions = $item->getProduct()->getCustomOption('embroidery_options');
                $infoBuyRequest = $item->getProduct()->getCustomOption('info_buyRequest');

                if(!is_null($embroideryOptions) && stristr($embroideryOptions->getValue(), $customLogoPath)) {
                    $embroideryOptionsValue = json_decode($embroideryOptions->getValue(), true);
                    $infoBuyRequestValue = unserialize($infoBuyRequest->getValue());
                    $filteredEmbroideryOptionsJson = json_encode($embroideryHelper->removeSelectedOptionType($embroideryOptionsValue, 'logo'));

                    if (isset($infoBuyRequestValue['product-embroidery-options'])) {
                        $infoBuyRequestValue['product-embroidery-options'] = $filteredEmbroideryOptionsJson;
                    }

                    $embroideryOptions->setValue($filteredEmbroideryOptionsJson);
                    $infoBuyRequest->setValue(serialize($infoBuyRequestValue));
                    $embroideryOptions->save();
                    $infoBuyRequest->save();
                }
            }

            $cart->removeItem($setupQuoteItemId)->save();

        } catch (Exception $e) {
            Mage::logException($e);
        }

    }


}
