<?php
require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'OnepageController.php';

class Ripen_Bettercheckout_CheckoutController extends Mage_Checkout_OnepageController
{
    /**
     * The default shipping option title
     */
    const DEFAULT_SHIPPING_TITLE_KEY = 'Standard';

    public function applyGiftCardAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($promocode = $this->getRequest()->getParam('giftcard_code', null)) {
                // Identify provided code type (promo code vs. gift card)
                $gcPattern1 = "/^(\w{3}-\w{3}-\w{3}-\w{3})$/i";
                $gcPattern2 = "/^(GC)\d{11}$/i";

                // Gift card case
                if (preg_match($gcPattern1, $promocode) || preg_match($gcPattern2, $promocode)) {
                    $giftcardModel = Mage::getModel('aw_giftcard/giftcard')->loadByCode(trim($promocode));

                    if ($giftcardModel->getId() && $giftcardModel->isValidForRedeem()) {
                        Mage::register('current_giftcard', $giftcardModel, true);

                        try {
                            Mage::helper('aw_giftcard/totals')->addCardToQuote($giftcardModel);

                            $quote = Mage::helper('aw_giftcard/totals')->getQuote();
                            $quote->setTotalsCollectedFlag(false)->collectTotals()->save();
                            $quoteData = $quote->getData();

                            $_collection = Mage::getModel('aw_giftcard/quote_giftcard')->getCollection();
                            $_collection->setFilterByQuoteId($quote->getId())->setFilterByGiftcardId($giftcardModel->getId())
                            ;
                            $appliedGiftCard = $_collection->getFirstItem();

                            $status = true;
                            $message = "You have applied Gift Card #{$giftcardModel->getCode()} in the amount of ".Mage::helper('checkout')->formatPrice($appliedGiftCard->getGiftcardAmount());
                        } catch (Exception $e) {
                            $status = false;
                            $message = $e->getMessage();
                        }
                    } else {
                        $status = false;
                        $message = "Please check your gift card code and try again.";
                    }
                } else {
                    // If provided code didn't match the gift card pattern (above), we assume
                    // that a promo code (aka coupon code) was used
                    $quote = Mage::getSingleton('checkout/session')->getQuote();
                    $quote->setCouponCode($promocode)
                        ->setTotalsCollectedFlag(false)
                        ->collectTotals()
                        ->save();

                    $quoteData = $quote->getData();

                    $quoteCouponCode = $quote->getCouponCode(); //get applied coupon code from the quote

                    if ($quoteCouponCode) {
                        $status = true;
                        $message = "Promo code {$quote->getCouponCode()} has been applied.";
                    } else {
                        $status = false;
                        $message = "Please check your gift card or promo code and try again.";
                    }
                }

                $response = array('status'=>$status, 'message'=>$message, 'cartTotal'=>$quoteData["grand_total"]);
                $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                $this->getResponse()->setBody(json_encode($response));
            }
        }
    }

    public function checkGiftCardBalanceAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($giftcardCode = $this->getRequest()->getParam('giftcard_code', null)) {

                $giftcard = Mage::getModel('aw_giftcard/giftcard')->loadByCode(trim($giftcardCode));

                $message = $giftcard->getId() ? "<span>Balance:</span> ".Mage::helper('core')->currency($giftcard->getBalance(), true, false) : "Invalid Gift Card";
                $response = array('balance'=>$message);

                $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                $this->getResponse()->setBody(json_encode($response));
            }
        }
    }

    public function removeGiftCardAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($giftcardCode = $this->getRequest()->getParam('giftcard_code', null)) {
                $giftcardCode = base64_decode($giftcardCode);

                Mage::helper('aw_giftcard/totals')->removeCardFromQuote(trim($giftcardCode));
                $cart = Mage::getSingleton('checkout/cart');
                $quote = $cart->getQuote();
                $address = $quote->getShippingAddress();
                $address->collectTotals();
                $cart->save();

                $block = Mage::app()->getLayout()->createBlock('checkout/cart_totals');
                $updatedRows = $block->renderTotals();
                $totalsBlock = $block->setTemplate('ripen/bettercheckout/checkout/onepage/totals.phtml')->toHtml();
                $totalsBlock = str_replace("<tfoot>", "<tfoot>".$updatedRows, $totalsBlock);

                $response = array('totals'=>$totalsBlock, 'total'=>$quote->getGrandTotal());
                $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                $this->getResponse()->setBody(json_encode($response));
            }
        }
    }

    public function renderTotalsAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();

            $block = Mage::app()->getLayout()->createBlock('checkout/cart_totals');
            $updatedRows = $block->renderTotals();
            $totalsBlock = $block->setTemplate('ripen/bettercheckout/checkout/onepage/totals.phtml')->toHtml();
            $totalsBlock = str_replace("<tfoot>", "<tfoot>".$updatedRows, $totalsBlock);

            $response = array('totals'=>$totalsBlock, 'cartTotal'=>$quote->getGrandTotal());
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    public function applyShippingAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $status = false;
            $message = "";

            $code = (string) $this->getRequest()->getParam('estimate_method');
            if (!empty($code)) {
                $cart = Mage::getSingleton('checkout/cart');
                $quote = $cart->getQuote();
                $address = $quote->getShippingAddress();
                $address->setShippingMethod($code)->collectTotals()->save();
                $cart->save();
                $status = true;
            } else {
                $status = false;
                $message = "Invalid Shipping Method";
            }

            $response = array('status'=>$status, 'message'=>$message);
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    public function getRegionsJsonAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $options = array();
            $country = (string)$this->getRequest()->getParam('country_id');
            $addressType = (string)$this->getRequest()->getParam('address_type');

            // Retrieve regions by country
            $regionModel = Mage::getModel('directory/region');
            $regions = $regionModel->getResourceCollection()
                ->addCountryFilter($country)
                ->addFieldToFilter('enabled', 1)
                ->load();

            $i = 1;
            $options[0]['value'] = '';
            $options[0]['label'] = 'Select state or province...';
            foreach ($regions as $region) {
                $options[$i]['value'] = $region["region_id"];
                $options[$i]['label'] = $region["name"];
                $i++;
            }

            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
            $address = $addressType == 'shipping' ? $quote->getShippingAddress() : $quote->getBillingAddress();
            if (!count($regions)) {
                $address->setRegionId(null);
                $options[0]['value'] = '';
                $options[0]['label'] = 'Not Applicable';
            }
            $address->setCountryId($country)->setCollectShippingRates(true);
            $address->collectTotals();
            $cart->save();

            $response = array('regions' => $options);
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    public function getShippingMethodsJsonAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $regionId = (string)$this->getRequest()->getParam('region_id');
            $street = (string)$this->getRequest()->getParam('street');
            $country = (string)$this->getRequest()->getParam('country');

            // Remap PR and VI countries to corresponding US regions
            if (in_array($country, ['PR','VI'])) {
                $regionId = Mage::helper('bettercheckout')->remapCountryToRegion($country);
                $country = "US";
            }

            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
//            Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " quote [" . print_r($quote, true) . "]", null, 'globba.log');


            $address = $quote->getShippingAddress();
//            Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " addr [" . print_r($address, true) . "]", null, 'globba.log');


            $address->setCountryId($country)
                ->setRegionId($regionId)
                ->setStreet($street)
                ->setCollectShippingRates(true);
            $address->collectTotals();


            $coupon_code = trim(Mage::getSingleton("checkout/session")->getData("coupon_code"));
            if (!empty($coupon_code)) {
                $quote->setCouponCode($coupon_code)
                    ->setTotalsCollectedFlag(false)
                    ->collectTotals()
                    ->save();
            }

            $cart->save();

            $response = Mage::helper('bettercheckout')->getAvailShippingMethods($address);

            if (count($response['options']) == 0 && $response['specialNote'] == "" && $regionId == "") {
                $response[options][0]['title'] = 'Select state or province first...';
            }

            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    public function getDeliveryTimeAction()
    {
        $greatestLeadTime = "";
        $selectedDeliveryMethod = (string) $this->getRequest()->getParam('delivery_method');
        if ($selectedDeliveryMethod) {
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
            $itemsWithEmbroidery = 0;
            $itemsWithLogo = 0;
            $itemsOutOfStock = 0;

            foreach ($quote->getAllItems() as $item) {
                $returnItems = array();
                $isSimpleItem = 0;

                if ($item->getHasChildren()) {
                    foreach($item->getChildren() as $child) {
                        $returnItems[] = $child;
                    }
                } else {
                    $returnItems[] = $item;
                    $isSimpleItem = 1;
                }

                $simpleItem = $returnItems[0];

                if (!$isSimpleItem && $item->hasEmbroiderySaved()) {
                    $itemsWithEmbroidery++;
                    if ($item->hasLogoEmbroidery()) {
                        $itemsWithLogo++;
                    }
                }

                $_product = Mage::getModel('catalog/product')->load($simpleItem->getProductId());
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getData();

                if ($isSimpleItem && ($stock["qty"] < $item->getQty())){
                    $itemsOutOfStock++;
                    $deliveryTime[] = Mage::helper('amstockstatus')->getCustomStockStatusText($_product);
                    $stockStatusIds[] = Mage::helper('amstockstatus')->getCustomStockStatusId($_product);
                }

            }

            if ($itemsOutOfStock == 0) {
                if ($itemsWithEmbroidery) {
                    if ($itemsWithLogo > 0) {
                        $dTime = "Ships 2 weeks after completion of custom logo setup.";
                    } else {
                        $dTime = "Please allow up to 2 weeks for embroidery.";
                    }
                # "Standard" or "Free" methods get slower shipping.
                } elseif (strpos($selectedDeliveryMethod, "Standard") !== false || strpos($selectedDeliveryMethod, "Free") !== false) {
                    $dTime = "Ships within 1 to 2 business days";
                # Any other shipping method gets same-day shipment.
                } else {
                    $dTime = "Ships today if ordered by 2:00 PM EST (Excluding weekends and holidays)";
                }
            } elseif ($itemsOutOfStock == 1 && !$itemsWithEmbroidery) {
                $dTime = $deliveryTime[0];
            } elseif ($itemsOutOfStock >= 1 && $itemsWithEmbroidery) {

//                '560' => 'Sold Out',
//                '554' => 'Ships in approx. 7-10 days.',
//                '559' => 'Ships in approx. 6 weeks.',
//                '558' => 'Ships in approx. 5 weeks.',
//                '557' => 'Ships in approx. 4 weeks.',
//                '556' => 'Ships in approx. 3 weeks.',
//                '555' => 'Ships in approx. 2 weeks.',
//                '553' => 'In Stock',
//                '561' => 'Expected to ship within 5 business days.');

                foreach ($stockStatusIds as $stockStatusId) {
                    switch ($stockStatusId) {
                        case '554':
                            $itemLeadTime = 7;
                            break;
                        case '559':
                            $itemLeadTime = 6;
                            break;
                        case '558':
                            $itemLeadTime = 5;
                            break;
                        case '557':
                            $itemLeadTime = 4;
                            break;
                        case '556':
                            $itemLeadTime = 3;
                            break;
                        case '555':
                            $itemLeadTime = 2;
                            break;
                        case '561':
                            $itemLeadTime = 1;
                            break;
                    }

                    $greatestLeadTime = ($greatestLeadTime >= $itemLeadTime ? $greatestLeadTime : $itemLeadTime);
                }

            }
        } else {
            $dTime = "";
        }

        if ($greatestLeadTime != "") {
            switch ($greatestLeadTime) {
                case 7:
                    $dTime = 'Ships in about 3 - 4 weeks.';
                    break;
                case 6:
                    $dTime = 'Ships in about 8 weeks.';
                    break;
                case 5:
                    $dTime = 'Ships in about 7 weeks.';
                    break;
                case 4:
                    $dTime = 'Ships in about 6 weeks.';
                    break;
                case 3:
                    $dTime = 'Ships in about 5 weeks.';
                    break;
                case 2:
                    $dTime = 'Ships in about 4 weeks.';
                    break;
                case 1:
                    $dTime = 'Ships in about 3 weeks.';
                    break;
                default:
                    $dTime = $deliveryTime[0];
            }
        } else {
            $match = "Ships in approx.";
            $replace = "Ships in about";
            if (substr($dTime, 0, strlen($match)) === $match) {
                $dTime = str_ireplace($match, $replace, $dTime);
            }
        }

        $response = array('deliveryTime'=>$dTime);
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));
    }

    public function setBillingStateAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()){
            $regionId    = (string) $this->getRequest()->getParam('region_id');
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
            $address = $quote->getBillingAddress();
            $address->setRegionId($regionId);
            $address->collectTotals();
            $cart->save();
        }
    }

    public function signInAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $session = Mage::getSingleton('customer/session');
            $status = false;

            if ($this->getRequest()->isPost()) {

                $username = $this->getRequest()->getPost('username');
                $password = $this->getRequest()->getPost('password');

                if (!empty($username) && !empty($password)) {

                    try {

                        $session->login($username, $password);
                        if ($session->getCustomer()->getId()){
                            $status = true;
                        }

                    } catch (Mage_Core_Exception $e) {

                        switch ($e->getCode()) {
                            case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                                $value = $this->_getHelper('customer')->getEmailConfirmationUrl($username);
                                $message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                                break;
                            case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                                $message = $e->getMessage();
                                break;
                            default:
                                $message = $e->getMessage();
                        }

                    } catch (Exception $e) {
                        // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                    }
                } else {
                    $message = $this->__('Login and password are required.');
                }
            }

            $response = array('status'=>$status, 'message'=>$message);
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    public function signOutAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $status = false;
            $session = Mage::getSingleton('customer/session');
            $session->logout()->renewSession();

            $response = array('status'=>$status);
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    public function placeOrderAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {

                $quote = Mage::helper('checkout/cart')->getQuote();
                $data = array();
                parse_str($this->getRequest()->getPost('data'), $data);

                // Remove masking that is submitted
                $data = $this->sanitizeOrderData($data);

                // If payment method is Check/Money Order, there is no Billing Address
                if (isset($data['payment']['method']) && $data['payment']['method'] == "checkmo") {
					$data["is-shipping-as-billing"] = true;
					$customerBillingAddressId = null;
				} else {
					$customerBillingAddressId = $data['shipping_billing_id'];
				}


                // try to submit the order
                $result = array();
                try {
                    // Save shipping data - PROD-853, set customerShippingAddressId to empty.
		            $customerShippingAddressId = '';

                    //Remap VI and PR countries to corresponding US regions
                    if(in_array($data["shipping"]["country_id"], ["VI", "PR"])) {
                        $data["shipping"]["region_id"] = Mage::helper('bettercheckout')->remapCountryToRegion($data["shipping"]["country_id"]);
                        $data["shipping"]["country_id"] = "US";
                    }

                    $resultShippingAddress = $this->getOnepage()->saveShipping($data["shipping"], $customerShippingAddressId);
                    $resultShippingMethod = $this->getOnepage()->saveShippingMethod($data["estimate_method"]);

                    /*
                     * Few orders were received with incorrect shipping method. Selected shipping method didn't match
                     * any methods available for a selected region. This problem is related to ajax latency issue.
                     * In addition to front-end validation we added an extra check here. A notification email will be
                     * sent to dev@ if mismatch occurs again.
                     */

                    $shippingMethodMatch = false;

                    // Iterate through available shipping methods and check if selected method is in the list.
                    foreach ($quote->getShippingAddress()->getAllShippingRates() as $rate) {
                        if ($rate->getCode() == $quote->getShippingAddress()->getShippingMethod()) {
                            $shippingMethodMatch = true;
                            break;
                        }
                    }

                    // Send an email to dev@ if selected shipping method doesn't match
                    if (($quote->getShippingAddress()->getShippingMethod() && !$shippingMethodMatch) || !$quote->getShippingAddress()->getShippingDescription()){
                        $message = "Shipping method mismatch alert. Selected shipping method [{$quote->getShippingAddress()->getShippingMethod()}] doesn't exist for provided region [{$quote->getShippingAddress()->getRegionId()}]. Check quote [{$quote->getId()}] created at [{$quote->getCreatedAt()}] for data integrity.";
                        $templateId = "Dev";
                        $emailTemplate = Mage::getModel('core/email_template')->loadByCode($templateId);
                        $vars = array("message" => $message);
                        $emailTemplate->getProcessedTemplate($vars);

                        $emailTemplate->setSenderName('Ripen Tech Team');
                        $emailTemplate->setSenderEmail('dev@ri.pn');
                        try {
                            $emailTemplate->send('mmedvedev@ri.pn', 'Ripen Tech', $vars);
                            $emailTemplate->send('drichter@ri.pn', 'Ripen Tech', $vars);
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }

                        // If shipping description is missing but shipping method is set, something is wrong
                        // with shipping calculation. Show error message.
                        if ($quote->getShippingAddress()->getShippingMethod() && !$quote->getShippingAddress()->getShippingDescription() && count($resultShippingMethod) == 0) {

                            $vars = array("message" => "Quote [{$quote->getId()}] shipping method is set [{$quote->getShippingAddress()->getShippingMethod()}], but shipping description is missing. Something is wrong. Showing error message to customer. ");
                            $emailTemplate->getProcessedTemplate($vars);
                            $emailTemplate->send('mmedvedev@ri.pn', 'Ripen Tech', $vars);
                            $resultShippingMethod = array("message"=>"Invalid Shipping Method");
                        }
                    }

                    // Save billing data
                    $data["billing"]["telephone"] = $data["shipping"]["telephone"];
                    $data["billing"]["email"] = $data["shipping"]["email"];
                    $billingAddressData = $data["is-shipping-as-billing"] ? $data["shipping"] : $data["billing"];
                    $resultBillingAddress = $this->getOnepage()->saveBilling($billingAddressData, $customerBillingAddressId);

                    Mage::getSingleton('core/session')->setEmailIntegrityCheck($data["billing"]["email"]);

                    // Save payment data
                    $resultPayment = $this->getOnepage()->savePayment($data["payment"]);
                    if ($data["payment"]) {
                        $data["payment"]['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_CHECKOUT
                            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                            | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX
                            | Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;
                        $this->getOnepage()->getQuote()->getPayment()->importData($data["payment"]);
                    }

                    $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
                    if ($requiredAgreements) {
                        $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                        $diff = array_diff($requiredAgreements, $postedAgreements);
                        if ($diff) {
                            $result['success'] = false;
                            $result['error'] = true;
                            $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                            return;
                        }
                    }

                    $errors = array_merge($resultShippingAddress, $resultShippingMethod, $resultBillingAddress, $resultPayment);
                    $block = $this->getLayout()->createBlock('bettercheckout/onepage_checkout');

                    if (!count($errors) || (count($resultShippingMethod) > 0 && $block->hasOnlyEmailGiftCard())){
                        $this->getOnepage()->saveOrder();

                        $iSubscribed = false;

                        foreach($data["list"] as $k=>$v){
                            if ($data["list"][$k]['subscribed']) {
                                $iSubscribed = true;
                            }
                        }
                        if ($iSubscribed){
                            Mage::getModel('newsletter/subscriber')->subscribe($data["shipping"]["email"]);
                        }

                        $redirectUrl = '/checkout/onepage/success/';
                        $result['success'] = true;
                        $result['error']   = false;
                    } else {
                        $errorMessages = is_array($errors["message"]) ? implode(", ", $errors["message"]) : $errors["message"];
                        $result['success'] = false;
                        $result['error'] = true;
                        $result['error_messages'] = $errorMessages;
                    }

                } catch (Mage_Payment_Model_Info_Exception $e) {
                    Mage::logException($e);
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $e->getMessage();

                } catch (Mage_Core_Exception $e) {
                    Mage::logException($e);
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $e->getMessage();

                } catch (Exception $e) {
                    Mage::logException($e);
                    $result['success']  = false;
                    $result['error']    = true;
                    $result['error_messages'] = $this->__('There was an error processing your order. Please try again later or contact us at 800.347.0288 from 9am to 5:30pm EST, Monday through Friday.');
                }

                $this->getOnepage()->getQuote()->save();

                $response = array('success'=>$result['success'], 'error'=>$result['error'], 'message'=>$result['error_messages'], "redirectUrl"=>$redirectUrl);
                $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                $this->getResponse()->setBody(json_encode($response));
            }
        }
    }

    /**
     * Sanitize Filter masking from data so that raw values are left
     *
     * @param $orderData
     * @return array|bool
     */
    protected function sanitizeOrderData($orderData) {
        try {
            if(!is_array($orderData)) {
                throw new Exception('Order data being passed is not an array.');
            }

            if(isset($orderData['shipping']['postcode'])) {
                $orderData['shipping']['postcode'] = str_replace(' ', '', strtoupper($orderData['shipping']['postcode']));
            }

            if(isset($orderData['shipping']['telephone'])) {
                $orderData['shipping']['telephone'] = preg_replace('/[^0-9]/', '', $orderData['shipping']['telephone']);
            }

            if(isset($orderData['billing']['postcode'])) {
                $orderData['billing']['postcode'] = str_replace(' ', '', strtoupper($orderData['billing']['postcode']));
            }

            if(isset($orderData['payment']['cc_number'])) {
                $orderData['payment']['cc_number'] = preg_replace('/[^0-9]/', '', $orderData['payment']['cc_number']);
            }

            return $orderData;

        } catch (Exception $e) {
            Mage::logException($e);
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $this->__('There was an error with your order data. Please try again.');

            return false;

        }
    }

    public function createAccountAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $status = false;
            $session = Mage::getSingleton('customer/session');
            if ($this->getRequest()->isPost()) {
                $customer = $this->_getFromRegistry('current_customer');

                try {
                    $errors = $this->_getCustomerErrors($customer);

                    if (empty($errors)) {
                        $customer->cleanPasswordsValidationData();
                        $customer->save();
                    } else {
                        $this->_addSessionError($errors);
                    }

                } catch (Mage_Core_Exception $e) {
                    $session->setCustomerFormData($this->getRequest()->getPost());
                    if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                        $url = $this->_getUrl('customer/account/forgotpassword');
//                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                        $message = $this->__('An account with this email already exists.');
                    } else {
                        $message = $this->_escapeHtml($e->getMessage());
                    }

                } catch (Exception $e) {
                    $session->setCustomerFormData($this->getRequest()->getPost());
                    $session->addException($e, $this->__('Cannot save the customer.'));
                }

                $response = array('status' => $status, 'message' => $message);
                $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                $this->getResponse()->setBody(json_encode($response));
            }
        }
    }

    public function setCheckoutDataAction()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			if ($this->getRequest()->isPost()) {
				$session = Mage::getSingleton('customer/session');
				$session->setData("checkout_form", $this->getRequest()->getPost());
				$response = $session->getData("checkout_form");
				$this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
				$this->getResponse()->setBody(json_encode($response));
			}
		}
	}

	public function getCheckoutDataAction()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$session = Mage::getSingleton('customer/session');
			$response = $session->getData("checkout_form");
			$this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
			$this->getResponse()->setBody(json_encode($response));
		}
	}


    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }



    /*
     *  Initialize Checkout
     *  Get all of the necessary data at page load for checkout.
     */

    public function initializeCheckoutAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $addressData = array(
                'shipping' => array(),
                'billing' => array()
            );

            $options = array();
            $regionModel = Mage::getModel('directory/region');

            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();

            foreach($addressData as $addressKey => $addressValue) {
                // Retrieve regions by country
                $addressType = $addressKey;
                $address = ($addressType == 'shipping') ? $quote->getShippingAddress() : $quote->getBillingAddress();
                $defaultShippingMethodId = ($addressType == 'shipping') ? $address->getShippingMethod() : null ;
                $country = ($address->getCountry()) ? $address->getCountry() : 'US';

                $regions = $regionModel->getResourceCollection()
                    ->addCountryFilter($country)
                    ->addFieldToFilter('enabled', 1)
                    ->load();

                $i = 1;
                $options[0]['value'] = '';
                $options[0]['label'] = 'Select state or province...';
                foreach ($regions as $region) {
                    $options[$i]['value'] = $region["region_id"];
                    $options[$i]['label'] = $region["name"];
                    $i++;
                }


                if (!count($regions)) {
                    $address->setRegionId(null);
                    $options[0]['value'] = '';
                    $options[0]['label'] = 'Not Applicable';
                }
                $address->setCountryId($address->getCountryId())->setCollectShippingRates(true);
                $address->collectTotals();
                $cart->save();

                $addressData[$addressKey] = array(
                    'type' => $addressKey,
                    'regions' => $options,
                    'countryId' => $address->getCountryId(),
                    'regionId' => $address->getRegionId(),
                    'defaultShippingMethodId' => $defaultShippingMethodId
                );

            }
            $shippingAddress = $quote->getShippingAddress();
            $shippingRegionId = $shippingAddress->getRegionId();
            $shippingStreet = $shippingAddress->getStreetFull();
            $shippingCountryId = $shippingAddress->getCountryId() ? $shippingAddress->getCountryId() : "US";

            $shippingContext = $this->getShippingContext($shippingRegionId, $shippingStreet, $shippingCountryId);

            $initialCheckoutData = array(
                'addressData' => $addressData,
                'shippingData' => $shippingContext['shippingData'],
                'deliveryTime' => $shippingContext['deliveryTime'],
                'appliedShipping' => $shippingContext['appliedShipping'],
                'checkoutTotals' => $this->getCheckoutTotalsData(),
                'checkoutForm' => $this->getCheckoutForm()
            );
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($initialCheckoutData));
        }
    }

    /**
     * getShippingMethodsComponentsAction()
     * Would replace the AJAX call to getShippingMethodsJson on the front end.
     * Called on the frontend in betteropcheckout.js by bettercheckout.renderShippingMethods();
     *
     * @throws Exception
     */
    public function getShippingMethodsComponentsAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $regionId = (string)$this->getRequest()->getParam('region_id');
            $street = (string)$this->getRequest()->getParam('street');
            $country = (string)$this->getRequest()->getParam('country');

            $shippingContext = $this->getShippingContext($regionId, $street, $country);

            $checkoutTotals = $this->getCheckoutTotalsData();
            $checkoutShippingData = array(
                'shippingData' => $shippingContext['shippingData'],
                'deliveryTime' => $shippingContext['deliveryTime'],
                'appliedShipping' => $shippingContext['appliedShipping'],
                'checkoutTotals' => $checkoutTotals
            );

            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($checkoutShippingData));
        }

    }

    /**
     * Get Shipping Context
     *
     * Return shipping data with delivery message and proper default selection
     *
     * @param string $regionId
     * @param string $street
     * @param string $country
     * @return array $shippingContext
     */
    protected function getShippingContext($regionId, $street, $country)
    {
        $shippingData = $this->getShippingMethodsData($regionId, $street, $country);

        $defaultShippingIndex = 0;
        $selectedShipping = null;
        foreach ($shippingData['options'] as $i => $shippingOption) {
            if ($shippingOption['selected']) {
                $selectedShipping = $shippingOption;
                break;
            }

            if ($shippingOption['title'] == self::DEFAULT_SHIPPING_TITLE_KEY) {
                $defaultShippingIndex = $i;
            }
        }
        if (empty($selectedShipping)) {
            $selectedShipping = $shippingData['options'][$defaultShippingIndex];
            $shippingData['options'][$defaultShippingIndex]['selected'] = true;
        }

        $appliedShipping = $this->applyShipping($selectedShipping['code']);

        $shippingContext = array(
            'shippingData' => $shippingData,
            'deliveryTime' => $this->getDeliveryTime($selectedShipping['title']),
            'appliedShipping' => $appliedShipping,
        );

        return $shippingContext;
    }

    /**
     * shippingMethodSelectionAction
     * For use whenever a shipping method selection is made.
     * Called on the frontend in betteropcheckout.js by bettercheckout.shippingMethodSelection();
     *
     * @throws Exception
     */
    public function shippingMethodSelectionAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $shippingMethodCode = (string)$this->getRequest()->getParam('method_code');
            $shippingMethodLabel =(string)$this->getRequest()->getParam('method_label');

            $appliedShipping = $this->applyShipping($shippingMethodCode);
            $deliveryTime = $this->getDeliveryTime($shippingMethodLabel);
            $checkoutTotals = $this->getCheckoutTotalsData();

            $shippingMethodSelection = array(
                'appliedShipping' => $appliedShipping,
                'deliveryTime' => $deliveryTime,
                'checkoutTotals' => $checkoutTotals
            );
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($shippingMethodSelection));
        }

    }

    /**
     * Get Shipping Methods Data
     * Return array used for shipping methods JSON
     *
     * TODO: Rename method or refactor, as this has side effects (not a pure getter).
     *
     * @param string $regionId
     * @param string $street
     * @param string $country
     * @return array
     */
    protected function getShippingMethodsData($regionId, $street, $country)
    {
        // Remap PR and VI countries to corresponding US regions
        if (in_array($country, ['PR','VI'])) {
            $regionId = Mage::helper('bettercheckout')->remapCountryToRegion($country);
            $country = "US";
        }

        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();

        $address = $quote->getShippingAddress();
        $address->setCountryId($country)
            ->setRegionId($regionId)
            ->setStreet($street)
            ->setCollectShippingRates(true);
        $address->collectTotals();

        $coupon_code = trim(Mage::getSingleton("checkout/session")->getData("coupon_code"));
        if (!empty($coupon_code)) {
            $quote->setCouponCode($coupon_code)
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();
        }

        $cart->save();

        $response = Mage::helper('bettercheckout')->getAvailShippingMethods($address);

        if (count($response['options']) == 0 && $response['specialNote'] == "" && $regionId == "") {
            $response[options][0]['title'] = 'Select state or province first...';
        }

        return $response;
    }

    /**
     * Get Delivery Time
     * Get the message that tells the customer delivery time
     *
     * @param string $selectedDeliveryMethod
     * @return string $dTime
     */
    protected function getDeliveryTime($selectedDeliveryMethod) {
        $greatestLeadTime = "";
        if ($selectedDeliveryMethod) {
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
            $itemsWithEmbroidery = 0;
            $itemsWithLogo = 0;
            $itemsOutOfStock = 0;

            foreach ($quote->getAllItems() as $item) {
                $returnItems = array();
                $isSimpleItem = 0;

                if ($item->getHasChildren()) {
                    foreach($item->getChildren() as $child) {
                        $returnItems[] = $child;
                    }
                } else {
                    $returnItems[] = $item;
                    $isSimpleItem = 1;
                }

                $simpleItem = $returnItems[0];

                if (!$isSimpleItem && $item->hasEmbroiderySaved()) {
                    $itemsWithEmbroidery++;
                    if ($item->hasLogoEmbroidery()) {
                        $itemsWithLogo++;
                    }
                }

                $_product = Mage::getModel('catalog/product')->load($simpleItem->getProductId());
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getData();

                if ($isSimpleItem && ($stock["qty"] < $item->getQty())){
                    $itemsOutOfStock++;
                    $deliveryTime[] = Mage::helper('amstockstatus')->getCustomStockStatusText($_product);
                    $stockStatusIds[] = Mage::helper('amstockstatus')->getCustomStockStatusId($_product);
                }

            }

            if ($itemsOutOfStock == 0) {
                if ($itemsWithEmbroidery) {
                    if ($itemsWithLogo > 0) {
                        $dTime = "Ships 2 weeks after completion of custom logo setup.";
                    } else {
                        $dTime = "Please allow up to 2 weeks for embroidery.";
                    }
                    # "Standard" or "Free" methods get slower shipping.
                } elseif (strpos($selectedDeliveryMethod, "Standard") !== false || strpos($selectedDeliveryMethod, "Free") !== false) {
                    $dTime = "Ships within 1 to 2 business days";
                    # Any other shipping method gets same-day shipment.
                } else {
                    $dTime = "Ships today if ordered by 2:00 PM EST (Excluding weekends and holidays)";
                }
            } elseif ($itemsOutOfStock == 1 && !$itemsWithEmbroidery) {
                $dTime = $deliveryTime[0];
            } elseif ($itemsOutOfStock >= 1 && $itemsWithEmbroidery) {

                foreach ($stockStatusIds as $stockStatusId) {
                    switch ($stockStatusId) {
                        case '554':
                            $itemLeadTime = 7;
                            break;
                        case '559':
                            $itemLeadTime = 6;
                            break;
                        case '558':
                            $itemLeadTime = 5;
                            break;
                        case '557':
                            $itemLeadTime = 4;
                            break;
                        case '556':
                            $itemLeadTime = 3;
                            break;
                        case '555':
                            $itemLeadTime = 2;
                            break;
                        case '561':
                            $itemLeadTime = 1;
                            break;
                    }

                    $greatestLeadTime = ($greatestLeadTime >= $itemLeadTime ? $greatestLeadTime : $itemLeadTime);
                }

            }
        } else {
            $dTime = "";
        }

        if ($greatestLeadTime != "") {
            switch ($greatestLeadTime) {
                case 7:
                    $dTime = 'Ships in about 3 - 4 weeks.';
                    break;
                case 6:
                    $dTime = 'Ships in about 8 weeks.';
                    break;
                case 5:
                    $dTime = 'Ships in about 7 weeks.';
                    break;
                case 4:
                    $dTime = 'Ships in about 6 weeks.';
                    break;
                case 3:
                    $dTime = 'Ships in about 5 weeks.';
                    break;
                case 2:
                    $dTime = 'Ships in about 4 weeks.';
                    break;
                case 1:
                    $dTime = 'Ships in about 3 weeks.';
                    break;
                default:
                    $dTime = $deliveryTime[0];
            }
        } else {
            $match = "Ships in approx.";
            $replace = "Ships in about";
            if (substr($dTime, 0, strlen($match)) === $match) {
                $dTime = str_ireplace($match, $replace, $dTime);
            }
        }

        return $dTime;

    }

    /**
     * Get Applied Shipping
     * Check the status of the shipping to see if it can be applied.
     *
     * @param $code
     * @return array
     */
    protected function applyShipping($code)
    {
        $status = false;
        $message = "";

        if (! empty($code)) {
            $cart = Mage::getSingleton('checkout/cart');
            $quote = $cart->getQuote();
            $address = $quote->getShippingAddress();
            if ($address->getShippingMethod() != $code) {
                $address->setShippingMethod($code)->collectTotals()->save();
                $cart->save();
            }
            $status = true;
        } else {
            $message = "Invalid Shipping Method";
        }

        return array(
            'status' => $status,
            'message' => $message
        );
    }

    /**
     * Get Checkout Totals Data
     * Returns array used for JSON with block HTML and grand total.
     * Used for rendering the checkout totals block
     *
     * @return array
     */
    public function getCheckoutTotalsData() {
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();

        $block = Mage::app()->getLayout()->createBlock('checkout/cart_totals');
        $updatedRows = $block->renderTotals();
        $totalsBlock = $block->setTemplate('ripen/bettercheckout/checkout/onepage/totals.phtml')->toHtml();
        $totalsBlock = str_replace("<tfoot>", "<tfoot>".$updatedRows, $totalsBlock);

        return array(
            'totals' => $totalsBlock,
            'cartTotal' =>$quote->getGrandTotal()
        );

    }

    /**
     * Get Checkout Form Data
     *
     * @return mixed
     */
    protected function getCheckoutForm() {
        $session = Mage::getSingleton('customer/session');
        return $session->getData('checkout_form');
    }
}
