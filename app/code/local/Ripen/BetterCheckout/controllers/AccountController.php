<?php
require_once Mage::getModuleDir('controllers', 'Mage_Customer').DS.'AccountController.php';

class Ripen_BetterCheckout_AccountController extends Mage_Customer_AccountController
{

    /**
     * Create customer account action
     */
    public function createAfterCheckoutAction()
    {

        if (!$this->_validateFormKey()) {
            $status = false;
            $message = "Invalid form key.";
        }

        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $status = false;
            $message = "You are already logged in.";
        }

        if (!$this->getRequest()->isPost()) {
            $status = false;
            $message = "Password is required.";
        }

        if ($status === false) {
            $response = array('status' => $status, 'message' => $message, 'redirectUrl' => $url);
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }

        $customer = $this->_getCustomer();

        try {
            $errors = $this->_getCustomerErrors($customer);

            if (empty($errors)) {
                $customer->cleanPasswordsValidationData();
                $customer->save();
                $this->_dispatchRegisterSuccess($customer);
                $this->_successProcessRegistration($customer);

                $incrementId = $this->getRequest()->getPost('increment_id');
                $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                $order->setCustomerId($customer->getId());
                $order->save();

                $orderBillingAddress = $order->getBillingAddress();
                $orderShippingAddress = $order->getShippingAddress();
                if (!$orderShippingAddress)
                    $orderShippingAddress = $orderBillingAddress;

                $customerShippingAddress = Mage::getModel('customer/address');
                $customerShippingAddress->setData($orderShippingAddress->getData())
                    ->setCustomerId($customer->getId())
                    ->setIsDefaultBilling('0')
                    ->setIsDefaultShipping('1')
                    ->setSaveInAddressBook('1')
                ;

                $customerBillingAddress = Mage::getModel('customer/address');
                $customerBillingAddress->setData($orderBillingAddress->getData())
                    ->setCustomerId($customer->getId())
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('0')
                    ->setSaveInAddressBook('1')
                ;

                $customerBillingAddress->save();
                $customerShippingAddress->save();

                $customer->saveEmbroideryLogos($order);

                $status = true;
                $url = "/customer/account/";

            } else {
                $status = false;
                $message = implode(", ", $errors);
            }
        } catch (Mage_Core_Exception $e) {

            $status = false;

            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $url = $this->_getUrl('customer/account/forgotpassword');
//                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                $message = $this->__('An account with this email already exists.');
            } else {
                $message = $this->_escapeHtml($e->getMessage());
            }

        } catch (Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }

        $response = array('status' => $status, 'message' => $message, 'redirectUrl' => $url);
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));

        // $this->_redirectError($errUrl);
    }

    /**
     * Success Registration
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_AccountController
     */
    protected function _successProcessRegistration(Mage_Customer_Model_Customer $customer)
    {
        $session = $this->_getSession();
        if ($customer->isConfirmationRequired()) {
            $app = $this->_getApp();
            $store = $app->getStore();
            $customer->sendNewAccountEmail(
                'confirmation',
                $session->getBeforeAuthUrl(),
                $store->getId()
            );
        } else {
            $session->setCustomerAsLoggedIn($customer);
        }
    }


}
