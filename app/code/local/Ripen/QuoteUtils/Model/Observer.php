<?php
class Ripen_QuoteUtils_Model_Observer
{
    /**
     * Clear Saved Quote
     * Delete an existing quote when there is a currently active cart and the user has visited the checkout page as guest.
     *
     * @param Varien_Event_Observer $observer
     */
    public function clearSavedQuote(Varien_Event_Observer $observer) {
        $customer = $observer->getEvent()->getData('customer');
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $checkoutSteps = Mage::getSingleton('checkout/session')->getData('steps');

        $quoteCollection = Mage::getModel('sales/quote')->getCollection();
        $quoteCollection->addFieldToFilter('customer_id', $customer->getId());
        $quoteCollection->addOrder('updated_at');

        $savedQuote = $quoteCollection->getFirstItem();
        if (!is_null($checkoutSteps) && $savedQuote->getId() && $cart->getId()) {
            $savedQuote->delete();
        }
    }
}