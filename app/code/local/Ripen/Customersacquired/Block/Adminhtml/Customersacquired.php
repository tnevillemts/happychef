<?php
class Ripen_Customersacquired_Block_Adminhtml_Customersacquired extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  	public function __construct()
  	{
    		parent::__construct();
    		$this->_controller = 'adminhtml_customersacquired';
    		$this->_blockGroup = 'customersacquired';
    		$this->_headerText = Mage::helper('customersacquired')->__('Customers Acquired Report');    
    		$this->_removeButton('add');
  	}
}
