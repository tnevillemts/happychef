<?php

class Ripen_Customersacquired_Block_Adminhtml_Customersacquired_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('customersacquiredGrid');
      $this->setDefaultSort('created_at');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
      $this->setSubReportSize(false);
      $this->setPagerVisibility(false);
      $this->setDateFilterVisibility(true);
      $this->setExportVisibility(false);
  }

  private function dateReformat($string)
  {
        $pieces = explode("/", $string);
        return $pieces[2] . '-' . $pieces[0] . '-' . $pieces[1];
  }

  protected function _prepareCollection()
  {
      date_default_timezone_set('America/New_York');
      if(empty($_COOKIE['rep_period_date_from'])) {
          $fromDate  = date('Y-m-d H:i:s', strtotime($toDateS . ' - 1 month'));
      } else {
          $fromDateString = $this->dateReformat($_COOKIE['rep_period_date_from']) . ' 00:00:00';
          $fromDate  = date('Y-m-d H:i:s', strtotime($fromDateString));
      }

      if(empty($_COOKIE['rep_period_date_to'])) {
          $toDate = date("Y-m-d H:i:s");
      } else {
          $toDateString = $this->dateReformat($_COOKIE['rep_period_date_to']) . ' 00:00:00';
          $toDate  = date('Y-m-d H:i:s', strtotime($toDateString));
      }

      $customerCount     = 0;
      $totalRevenue      = 0;
      $lastCustomerEmail = '';

      $reportCollection = new Varien_Data_Collection();
      $varObj = new Varien_Object();

      $orders = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToSort('customer_email', 'ASC')
                ->addAttributeToSort('created_at', 'ASC')
                ->addAttributeToFilter('created_at', array('from' => $fromDate, 'to' => $toDate))
                ->addAttributeToFilter('status', array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE));

      if($orders->count() > 0) {
              foreach($orders as $order) {
                      $customerEmail = $order->getCustomerEmail();

                      if($customerEmail == $lastCustomerEmail) {
                              continue;
                      } else {
                              $oldOrders = Mage::getModel('sales/order')->getCollection()
                                      ->addAttributeToSort('customer_email', 'ASC')
                                      ->addAttributeToSort('created_at', 'ASC')
                                      ->addAttributeToFilter('created_at', array('to' => $fromDate))
                                      ->addAttributeToFilter('customer_email', $customerEmail)
                                      ->addAttributeToFilter('status', array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE));

                              if($oldOrders->count() > 0) {
                                      continue;
                              }

                              //echo("Customer [" . $customerEmail . "] Grand Total [" . $order->getGrandTotal() . "]\n");
                              $totalRevenue += $order->getGrandTotal();
                              $customerCount++;
                      }

                      $lastCustomerEmail = $customerEmail;
              }
      } else {
	      Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " No Data ", null, 'customersacquired.log');
      }

      $varObj->setFrom($fromDate);
      $varObj->setTo($toDate);
      $varObj->setCount($customerCount);
      $varObj->setRevenue($totalRevenue);
      $reportCollection->addItem($varObj);

      $this->setCollection($reportCollection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $store = Mage::app()->getStore();

      $this->addColumn('from', array(
          'header' => Mage::helper('adminhtml')->__('From'),
          'align' => 'left',
	  'width' => '25%',
	  'filter'    => false,
          'type' => 'datetime',
          //'format' => 'Y-m-d H:i:s',
          'index' => 'from',
      ));

      $this->addColumn('to', array(
          'header' => Mage::helper('adminhtml')->__('To'),
          'align' => 'left',
	  'width' => '25%',
	  'filter'    => false,
          'type' => 'datetime',
          //'format' => 'Y-m-d H:i:s',
          'index' => 'to',
      ));

      $this->addColumn('count', array(
          'header'    => Mage::helper('customersacquired')->__('New Customer Count'),
          'align'     => 'right',
          'width'     => '25%',
	  'filter'    => false,
          'sortable'  => false,
          'index'     => 'count',
      ));

      $this->addColumn('revenue', array(
          'header'    => Mage::helper('customersacquired')->__('Revenue For New Customers'),
          'align'     =>'left',
          'width'     => '25%',
	  'filter'    => false,
          'sortable'  => false,
          'index'     => 'revenue',
	  'type'      => 'price',
	  'currency_code' => $store->getBaseCurrency()->getCode()
      ));
      
      $this->addExportType('*/*/exportCsv', Mage::helper('customersacquired')->__('CSV'));
      $this->addExportType('*/*/exportXml', Mage::helper('customersacquired')->__('XML'));
	  
      return parent::_prepareColumns();
  }   

  public function getRowUrl($row)
  {
      return "#";
  }

}
