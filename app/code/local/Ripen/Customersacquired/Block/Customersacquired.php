<?php
class Ripen_Customersacquired_Block_Customersacquired extends Mage_Core_Block_Template {

	public function _prepareLayout() {
    		return parent::_prepareLayout();
	}

	public function getCustomersacquired() {
    		if (!$this->hasData('customersacquired')) {
        		$this->setData('customersacquired', Mage::registry('customersacquired'));
    	}
    	return $this->getData('customersacquired');
	} 
}
