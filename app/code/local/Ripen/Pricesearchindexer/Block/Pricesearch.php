<?php
class Ripen_Pricesearchindexer_Block_Pricesearch extends Mage_Catalog_Block_Product_List
{
    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getProductImageByCategory($_product) {
        $imgSource = $this->helper('catalog/image')->init($_product, 'small_image')->resize(252, 283)->setQuality(100);
        return $imgSource;
    }
}
