<?php

//class Ripen_Pricesearchindexer_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Catalog_Product_Grid
class Ripen_Pricesearchindexer_Block_Adminhtml_Catalog_Product_Grid extends Ripen_Coreextended_Adminhtml_Block_Catalog_Product_Grid
{
    protected function _prepareMassaction() {
        //parent::_prepareMassaction();
	call_user_func(array(get_parent_class(get_parent_class($this)), '__prepareMassaction'));

        // Append new mass action option
        $this->getMassactionBlock()->addItem(
            'pricesearchindexer',
            array(  'label' => $this->__('Reindex'),
                    'url'   => $this->getUrl('pricesearchindexer/adminhtml_index/massReindex')
            )
        );
    }
}
