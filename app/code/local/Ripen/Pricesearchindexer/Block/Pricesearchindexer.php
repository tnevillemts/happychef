<?php
class Ripen_Pricesearchindexer_Block_Pricesearchindexer extends Mage_Catalog_Block_Product_List
{
    public function _getCollection() {
        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
        $collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
            ;

        return $collection;
    }

    public function _prepareLayout() {
        $this->collection = $this->_getCollection();
        return parent::_prepareLayout();
    }
}

