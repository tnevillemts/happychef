<?php
/**
 * @author Ripen
 * @copyright Ripen
 * @package Ripen_Pricesearchindexer
 */
class Ripen_Pricesearchindexer_Model_Indexer_Pricesearchindexer extends Mage_Index_Model_Indexer_Abstract
{
    const EVENT_MATCH_RESULT_KEY = 'pricesearchindexer_match_result';

    /**
     * @var array
     */
    protected $_matchedEntities = array(
        Mage_Catalog_Model_Product::ENTITY => array(
            Mage_Index_Model_Event::TYPE_SAVE,
            Mage_Index_Model_Event::TYPE_MASS_ACTION,
            Mage_Index_Model_Event::TYPE_DELETE
        )
    );

    /**
     * Retrieve Indexer name
     *
     * @return string
     */
    public function getName() {
        return Mage::helper('pricesearchindexer')->__('Price Search Indexes');
    }
    /**
     * Retrieve Indexer description
     *
     * @return string
     */
    public function getDescription() {
        return Mage::helper('pricesearchindexer')->__('Regenerate mag_catalog_product_index_price.tier_price data');
    }
    /**
     * Register data required by process in event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event) {
    }

    public function matchEvent(Mage_Index_Model_Event $event) {
        return false;
    }
    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event) {
    }

    public function reindexAll() {
        return $this->doReindexAll();
    }

    protected function doReindexAll($productIds = NULL) {
        try {
            $config  = Mage::getConfig()->getResourceConnectionConfig("default_setup");
            $dbinfo  = array(“host” => $config->host, “user” => $config->username, “pass” => $config->password, “dbname” => $config->dbname);
            $dbname  = $dbinfo[“dbname”];

            $resource        = Mage::getSingleton('core/resource');
            $readConnection  = $resource->getConnection('core_read');
            $writeConnection = $resource->getConnection('core_write');

            $shouldUpdate           = TRUE;
            $shouldLog              = TRUE;
            $highVal                = 1000000;
            $productIndexTableName  = "mag_catalog_product_index_price";

            if($shouldLog) {
                Mage::log("Start File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " ", null, 'Ripen_Pricesearchindexer.log');
                if(!readConnection && !writeConnection) {
                    Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " Database connections established.", null, 'Ripen_Pricesearchindexer.log');
                } else {
                    Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " Database connections NOT established.", null, 'Ripen_Pricesearchindexer.log');
                }
            }

            $collectionProducts = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('sku')
                ->addAttributeToSelect('visibility')
                ->addAttributeToSelect('price')
                ->addAttributeToFilter('status', array('eq' => 1))
                ->addAttributeToSort('type_id', 'ASC');

            $customerGroupIds = array();
            $sql = "SELECT customer_group_id FROM " . $dbname . "." . "mag_customer_group";
            $result = $readConnection->query($sql);
            if(! $result) {
                if($shouldLog) {
                    Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " sql failed [" . $sql . "]", null, 'Ripen_Pricesearchindexer.log');
                }
            } else {
                while($row = $result->fetch()) {
                    if($shouldLog) {
                    Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " retrieved group id [" . $row['customer_group_id'] . "]", null, 'Ripen_Pricesearchindexer.log');
                    }
                    $customerGroupIds [] = $row['customer_group_id'];
                }
            }

            if($shouldLog) {
                $outstr = sprintf("%32s %5s %16s %16s %16s %16s \t  %3s  %16s\t", "Sku", "getId", "Type", "ProductPrice", "min_price", "lowestTierPrice", "visibility", "tier_price\n");
                Mage::log($outstr, null, 'Ripen_Pricesearchindexer.log');
            }

            foreach($collectionProducts as $product) {
                $lowestTierPrice = $highVal;
                $attribute       = $product->getResource()->getAttribute('tier_price');

                if($attribute) {
                    $newTierPrice = 0;

                    // Find the lowest existing tier price for the product.
                    $attribute->getBackend()->afterLoad($product);
                    $tierPrices = $product->getTierPrice();
                    foreach($tierPrices as $tierPrice)  {
                        if($lowestTierPrice > $tierPrice['price']) {
                            $lowestTierPrice = $tierPrice['price'];
                        }
                    }
                    $tierPrice = 0;

                    // Make it zero if nothing is found.
                    if($lowestTierPrice == $highVal) {
                        $lowestTierPrice = 0;
                    }

                    $sql = "SELECT entity_id, min_price, tier_price FROM " . $dbname . "." . $productIndexTableName . " WHERE entity_id = " . $product->getId();
                    $result = $readConnection->query($sql);
                    if(! $result) {
                        if($shouldLog) {
                            Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " sql failed [" . $sql . "]", null, 'Ripen_Pricesearchindexer.log');
                        }
                    } else {
                        // Find the lowest tier price in the index records.
                        $lowestMinPrice = $highVal;
                        while($row = $result->fetch()) {
                            if($lowestMinPrice > $row['min_price']) {
                            $lowestMinPrice = $row['min_price'];
                            $tierPrice = $row['tier_price'];
                            }
                        }

                            // Make it zero if nothing is found.
                        if($lowestMinPrice == $highVal) {
                            $lowestMinPrice = 0;
                        }
                        if($tierPrice == NULL) {
                            $tierPrice = 'NULL';
                        }

                        // Basic price of product.
                        $price = $product->getPrice();
                        $pricesForProduct = array();
                        if($price != 0) {
                            $pricesForProduct [] = $product->getPrice();
                        }

                        // Lowest tier price defined for product.
                        if($lowestTierPrice != 0 && $lowestTierPrice != $highVal) {
                            $pricesForProduct [] = $lowestTierPrice;
                        }

                        // Lowest price in product index.
                        if($lowestMinPrice != 0 && $lowestMinPrice != $highVal) {
                            $pricesForProduct [] = $lowestMinPrice;
                        }

                        // The minimal of all of these.
                        $noPricesFoundMess = "";
                        if(count($pricesForProduct) > 0) {
                            $newTierPrice = min($pricesForProduct);
                        } else {
                            $noPricesFoundMess = ", no prices found.";
                        }

                        // Determine visibility.
                        $visibility = $product->getVisibility();
                        if($visibility == Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) {
                            $vis = "VISIBILITY_BOTH";
                        } else if ($visibility == Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG) {
                            $vis = "VISIBILITY_IN_CATALOG";
                        } else if ($visibility == Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH) {
                            $vis = "VISIBILITY_IN_SEARCH";
                        } else if ($visibility == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE) {
                            $vis = "VISIBILITY_NOT_VISIBLE";
                        }

                        if(/*$lowestTierPrice != 0 &&*/ $newTierPrice != $tierPrice && $visibility == Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) {
                            $updated = '';
                            if($shouldUpdate == TRUE) {
                                $sql = "UPDATE " . $dbname . "." . $productIndexTableName . " SET tier_price = '" . $newTierPrice . "' WHERE entity_id = " . $product->getId();
                                $result = $writeConnection->query($sql);
                                if(! $result) {
                                    $updated = 'False';
                                    if($shouldLog) {
                                        Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " sql failed [" . $sql . "]", null, 'Ripen_Pricesearchindexer.log');
                                    }
                                } else {
                                    $updated = 'True';
                                }
                            } else {
                                $updated = 'No';
                            }
                            if($shouldLog) {
                                $outstr = sprintf("%32s %5s %16s %16s %16s %16s %22s (%16s  ->  %16s) %1s - Needed",
                                    $product->getSku(), $product->getId(), $product->getTypeID(), $product->getPrice(), $lowestMinPrice, $lowestTierPrice, $vis, $tierPrice, $newTierPrice, $updated);
                            }
                        } else {
                            // Here we could consider inserting missing entries into "mag_catalog_product_index_price".

                            $updated = 'No';
                            if($shouldLog) {
                                $outstr = sprintf("%32s %5s %16s %16s %16s %16s %22s (%16s  ->  %16s) %1s - NOT needed" . $noPricesFoundMess,
                                    $product->getSku(), $product->getId(), $product->getTypeID(), $product->getPrice(), $lowestMinPrice, $lowestTierPrice, $vis, $tierPrice, $newTierPrice, $updated);
                            }
                        }
                        if($shouldLog) {
                            Mage::log($outstr, null, 'Ripen_Pricesearchindexer.log');
                        }
                    }
                } else {
                    if($shouldLog) {
                        Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " No attribute (tier_price)", null, 'Ripen_Pricesearchindexer.log');
                    }
                }
            }
        } catch (Exception $e){
            Mage::log("Error Pricesearchindexer: ". $e->getMessage());
            return false;
        }
        if($shouldLog) {
            Mage::log("Done File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " ", null, 'Ripen_Pricesearchindexer.log');
        }
        return true;
    }
}
