<?php
class Ripen_Customercontent_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Get Layout
     * @return Mage_Core_Model_Abstract
     */
    public function getLayout()
    {
        return Mage::getSingleton('core/layout');
    }

    /**
     * Get My Account Html
     *
     * @param $customer
     * @return mixed
     */
    public function getMyAccountHtml($customer) {
        $myAccountWelcome = 'Welcome' . (($customer->getName()) ? ' ' . $customer->getName() : '') . '!';
        return $this->getLayout()
            ->createBlock('core/template')
            ->assign('myAccountWelcome', $myAccountWelcome)
            ->setTemplate('page/html/myaccount.phtml')
            ->toHtml();
    }

    /**
     * Get Btn Mobile Display Drawer Html
     *
     * @return mixed
     */
    public function getBtnMobileDisplayDrawerHtml() {
        return $this->getLayout()
            ->createBlock('core/template')
            ->setTemplate('page/html/btn_mobile_display_drawer.phtml')
            ->toHtml();
    }
}