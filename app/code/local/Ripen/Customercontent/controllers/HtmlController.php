<?php
class Ripen_Customercontent_HtmlController extends Mage_Core_Controller_Front_Action
{
    /**
     * My Account Html Action
     * Get the customer session
     * return the nav item HTML in JSON
     * When a customer is logged in
     *
     * @return string $response (JSON)
     */
    public function myAccountNavItemAction() {
        $response = array(
            'status' => false,
            'myAccountHtml' => ''
        );

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $customerContent = Mage::helper('customercontent');

            $myAccountHtml = $customerContent->getMyAccountHtml($customer);

            $response['status'] = true;
            $response['myAccountHtml'] = $myAccountHtml;
        }

        $this->getResponse()
            ->clearHeaders()
            ->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));
    }
}