<?php
class Ripen_Ripenreports_Model_Colorsales extends Mage_Reports_Model_Mysql4_Order_Collection
{
    function __construct() {
        parent::__construct();
        $this->setResourceModel('sales/order_item_collection');
        $this->_init('sales/order_item','item_id');
    }

    public function setDateRange($from, $to) {
        $this->_reset();

        $this->getSelect()->join( array('items'=> Mage::getSingleton('core/resource')->getTableName('sales/order_item')),
            'main_table.entity_id=items.order_id', array('product_id', 'name'));

        $this->getSelect()->join( array('products'=> Mage::getSingleton('core/resource')->getTableName('catalog/product')),
            'items.product_id=products.entity_id', array('sku'));

        $this->addAttributeToFilter('parent_item_id', array('null' => true))
            ->addAttributeToFilter('state', array('neq' => Mage_Sales_Model_Order::STATE_CANCELED))
            ->addAttributeToFilter('main_table.created_at',  array('from'=>$from,'to'=>$to))
        ;

        $this->getSelect()
            ->columns('SUM(price) as productTotal, SUM(qty_ordered) AS productCount, "'.$from.'" as reportFrom, "'.$to.'" as reportTo')
            ->group('product_id');


        // uncomment next line to get the query log:
        // echo $this->getSelect()->__toString();
        // die("yo");
        // Mage::log('SQL: '.$this->getSelect()->__toString());

        return $this;
    }

    public function setStoreIds($storeIds)
    {
        return $this;
    }

}
