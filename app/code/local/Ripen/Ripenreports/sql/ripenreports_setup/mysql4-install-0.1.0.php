<?php
$installer = $this;

$installer->startSetup();
$installer->getConnection()
    ->addIndex(
        $installer->getTable('sales/order_item'),
        'IDX_PRODUCT_ID',
        'product_id'
    );
$installer->endSetup(); 