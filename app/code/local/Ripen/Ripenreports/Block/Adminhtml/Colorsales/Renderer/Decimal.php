<?php
class Ripen_Ripenreports_Block_Adminhtml_Colorsales_Renderer_Decimal extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return number_format($row->getData($this->getColumn()->getIndex()), 2);
    }
}