<?php
class Ripen_Ripenreports_Block_Adminhtml_Colorsales_Renderer_Colors extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $product = Mage::getModel('catalog/product')->load($row->getProductId());
        if($product->isConfigurable()){

            $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
            $attributeOptions = array();
            $statOutput = "";
            $statsArray = array();

            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);

            foreach($childProducts as $child) {
                $attr = $child->getResource()->getAttribute('color')->getFrontend()->getValue($child);
                $idsForColor[$attr][] = $child->getId();
            }

            foreach ($productAttributeOptions as $productAttribute) {

                foreach ($productAttribute['values'] as $attribute) {

                    $colorTotalSales = 0;
                    $colorTotalQty = 0;

                    if($productAttribute['label'] == "Color"){

                        $childrenWithSameColor = $idsForColor[$attribute['store_label']];

                        $collection = Mage::getModel('sales/order_item')->getCollection();
                        $collection->getSelect()->where("item_id IN (
                            SELECT parent_item_id
                            FROM `mag_sales_flat_order_item` AS `main_table`
                            INNER JOIN `mag_sales_flat_order` AS `orders` ON main_table.order_id=orders.entity_id
                            WHERE (`product_id` IN(".implode(",", $childrenWithSameColor)."))
                            AND (`orders`.`state` != '".Mage_Sales_Model_Order::STATE_CANCELED."')

                            AND (`main_table`.`created_at` >= '{$row->getData("reportFrom")}' AND `main_table`.`created_at` <= '{$row->getData("reportTo")}')
                        ) AND product_id = {$row->getProductId()}")
                            ->columns('SUM(price) as productTotal, SUM(qty_ordered) AS productCount');

                        $resultSet = $collection->getFirstItem();
                        $qty = number_format($resultSet->getData('productCount'), 0);
                        $sales = number_format($resultSet->getData('productTotal'),2);
                        $statsArray[$attribute['store_label']]["qty"] = $qty;
                        $statsArray[$attribute['store_label']]["sales"] = $sales;


                        /*
                                                $collection = Mage::getModel('sales/order_item')->getCollection();
                                                $collection->getSelect()->join( array('orders'=> Mage::getSingleton('core/resource')->getTableName('sales/order')),
                                                    'main_table.order_id=orders.entity_id', array());

                                                $collection->addAttributeToFilter('product_id', array('in' => $childrenWithSameColor))
                                                    ->addAttributeToFilter('orders.state', 'complete')
                                                    ->addAttributeToFilter('main_table.created_at',  array('from'=>'2016-05-01 08:00:00','to'=>'2016-05-06 07:59:59'))
                                                ;

                                                foreach($collection as $orderItem){
                                                    $parentItem = Mage::getModel('sales/order_item')->load($orderItem->getParentItemId());
                                                    if($parentItem->getProductId() == $row->getProductId()){
                                                        $colorTotalSales += $parentItem->getPrice();
                                                        $colorTotalQty += $parentItem->getQtyOrdered();
                                                    }
                                                }

                                                $qty = $colorTotalQty;
                                                $sales = $colorTotalSales;
                        */

                        //$stats .= $attribute['store_label'].": {$qty} (\${$sales})<br>";
                    }
                }
            }

            uasort ( $statsArray,  array(get_class($this),'compare'));

            foreach($statsArray as $color => $statData){
                $statOutput.= $color.": {$statData["qty"]} (\${$statData["sales"]})<br>";
            }
            
            return $statOutput;
        } else {
            return "n/a";
        }


        return;


        $collection = $this->setResourceModel('sales/order_item_collection');


        $product = Mage::getModel('catalog/product')->load($row->getEntityId());
        $cats = $product->getCategoryIds();
        $allCats = '';
        foreach($cats as $key => $cat)
        {
            $_category = Mage::getModel('catalog/category')->load($cat);

            $productPositions = $_category->getProductsPosition();

            $allCats.= $_category->getName()." (".$productPositions[$row->getEntityId()].")";
            if($key < count($cats)-1)
                $allCats.= ', ';
        }
        return $allCats;
    }

    function compare($a, $b){
        return ($a['qty'] < $b['qty']);
    }

}