<?php
class Ripen_Ripenreports_Block_Adminhtml_Colorsales_Grid extends Mage_Adminhtml_Block_Report_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('colorsalesGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setSubReportSize(false);
        $this->setFilter("report_period", "year");
        $this->setTemplate('ripen/ripenreports/grid.phtml');
    }

    protected function _prepareCollection() {

        parent::_prepareCollection();
        $this->getCollection()->initReport('ripenreports/colorsales');

        return $this;
    }

    protected function _prepareColumns() {

        $this->addColumn('sku', array(
            'header' => Mage::helper('ripenreports')->__('Model'),
            'align' => 'left',
            'index' => 'sku',
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('ripenreports')->__('Product'),
            'align' => 'left',
            'index' => 'name'
        ));

        $this->addColumn('productTotal', array(
            'header'    =>Mage::helper('ripenreports')->__('Total Sales'),
            'align'     =>'right',
            'index'     =>'productTotal',
            'total'     =>'sum',
            'type'      =>'number',
            'renderer'  =>'Ripen_Ripenreports_Block_Adminhtml_Colorsales_Renderer_Decimal'

        ));

        $this->addColumn('productCount', array(
            'header'    =>Mage::helper('ripenreports')->__('Total Quantity'),
            'align'     =>'right',
            'index'     =>'productCount',
            'total'     =>'sum',
            'type'      =>'number'
        ));

        $this->addColumn('colors', array(
            'header'    =>Mage::helper('ripenreports')->__('Colors'),
            'align'     =>'right',
            'index'     =>'colors',
            'from'     => $this->getFilter('report_from'),
            'to'     => $this->getFilter('report_to'),
            'renderer'  => 'Ripen_Ripenreports_Block_Adminhtml_Colorsales_Renderer_Colors'
        ));


        $this->addExportType('*/*/exportCsv', Mage::helper('ripenreports')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('ripenreports')->__('XML'));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return false;
    }

    public function getCategories()
    {
        $arr = array("0"=> "Select...");

        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addIsActiveFilter();

        foreach($categories as $cat) {
            $arr[$cat->getId()] = $cat->getName();
        }
        return $arr;
    }

    public function getReport($from, $to) {

        if ($from == '') {
            $from = $this->getFilter('report_from');
        }

        if ($to == '') {
            $to = $this->getFilter('report_to');
        }

        $report = $this->getCollection()->getReport($from, $to);

        if($this->getFilter('category_id')){
            $report->getSelect()->where("items.product_id IN (SELECT product_id FROM mag_catalog_category_product WHERE category_id = {$this->getFilter('category_id')})");
        }

        if($this->getFilter('model')){
            $report->getSelect()->where("items.sku LIKE '%{$this->getFilter('model')}%'");
        }

        $totals = new Varien_Object();
        $fields = array();
        $fields['productCount']    = 0;
        $fields['productTotal']    = 0;

        foreach ($report as $item) {
            foreach($fields as $field=>$value){
                $fields[$field]+=$item->getData($field);
            }
        }

        $totals->setData($fields);
        $this->setTotals($totals);
        $this->addGrandTotals($totals);

        return $report;
    }
}