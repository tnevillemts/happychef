<?php
class Ripen_Ripenreports_Block_Adminhtml_Colorsales extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_colorsales';
        $this->_blockGroup = 'ripenreports';
        $this->_headerText = Mage::helper('ripenreports')->__('Color Sales Report');
        parent::__construct();
        $this->_removeButton('add');
    }
}
