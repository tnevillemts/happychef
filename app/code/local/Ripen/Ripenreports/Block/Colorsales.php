<?php
class Ripen_Ripenreports_Block_Colorsales extends Mage_Core_Block_Template
{

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getColorsales() {
        if (!$this->hasData('colorsales')) {
            $this->setData('colorsales', Mage::registry('colorsales'));
        }
        return $this->getData('colorsales');
    }
}