<?php

class Ripen_Stockstatus_Helper_Data extends Amasty_Stockstatus_Helper_Data
{
    public function getCustomStockStatusText(Mage_Catalog_Model_Product $product, $qty=0)
    {
        if(!$product)
            return false;

        $product = Mage::getModel('catalog/product')->load($product->getId());
        $status = $product->getAttributeText('custom_stock_status');
        $stockItem   = null;

        if (false !== strpos($status, '{qty}'))
        {
            if (!$stockItem)
            {
                $stockItem   = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            }
            $status = str_replace('{qty}', intval($stockItem->getData('qty')  + $qty), $status);
        }

        // search for atttribute entries
        preg_match_all('@\{(.+?)\}@', $status, $matches);
        if (isset($matches[1]) && !empty($matches[1]))
        {
            foreach ($matches[1] as $match)
            {
                if ($value = $product->getData($match))
                {
                    if (preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $value))
                    {
                        $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
                        $value = Mage::getSingleton('core/locale')->date($value, null, null, false)->toString($format);
                    }
                    $status = str_replace('{' . $match . '}', $value, $status);
                }
                else{
                    $status = str_replace('{' . $match . '}', "", $status);
                }
            }
        }
        return $status;
    }

    public function getCustomStockStatusId(Mage_Catalog_Model_Product $product)
    {
        $statusId = $product->getData('custom_stock_status');
        return $statusId;
    }

}