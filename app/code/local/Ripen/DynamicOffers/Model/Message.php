<?php
/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2016 Ripen eCommerce
 * @package Ripen_Dynamicoffers
 */

class Ripen_DynamicOffers_Model_Message
{
    public function toOptionArray() {

        $options = array();
        $blocks = Mage::getResourceModel('cms/block_collection')->load();
        foreach ($blocks as $block) {
            array_push($options, array('value' => $block->getIdentifier(), 'label' => $block->getTitle()));
        }

        return $options;
    }

}