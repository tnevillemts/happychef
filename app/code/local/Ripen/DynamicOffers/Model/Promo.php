<?php
/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2016 Ripen eCommerce
 * @package Ripen_Dynamicoffers
 */

class Ripen_DynamicOffers_Model_Promo
{
    public function toOptionArray() {

        $options = array();
        $rules = Mage::getResourceModel('salesrule/rule_collection')->load();
        foreach ($rules as $rule) {
            array_push($options, array('value' => $rule->getId(), 'label' => $rule->getName()));
        }

        return $options;
    }

}