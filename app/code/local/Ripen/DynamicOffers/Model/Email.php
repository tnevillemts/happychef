<?php
/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2016 Ripen eCommerce
 * @package Ripen_Dynamicoffers
 */

class Ripen_DynamicOffers_Model_Email
{
    public function toOptionArray() {

        $options = array();
        $templates = Mage::getResourceModel('core/email_template_collection')->load();
        foreach ($templates as $template) {
            array_push($options, array('value' => $template->getId(), 'label' => $template->getTemplateCode()));
        }

        return $options;
    }

}