<?php

/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2016 Ripen eCommerce
 * @package Ripen_Dynamicoffers
 */
class Ripen_DynamicOffers_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('dynamicoffers')->setFormAction(Mage::getUrl('*/*/post'));
        $this->renderLayout();
    }

    /**
     * @desc validates and processes submitted offer form,  then creates a json response
     */
    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        $postObject = new Varien_Object();
        $postObject->setData($post);

        $captcha = $post['g-recaptcha-response'];
        if (!$captcha) {
            $jsonResponse = Mage::helper('core')->jsonEncode(array("error" => 1, "message" => "Your captcha response could not be validated. Please try again."));
        }

        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LfRNj0UAAAAAD-rg5VaWAe-n5QVTjOf6vNtUxPQ&response=" . $captcha;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $response = json_decode($response, true);
	// For development.
        //$response['success'] = true;
        //$response['error'] = 0;

        if ( $response['success'] == false ) {

            $jsonResponse = Mage::helper('core')->jsonEncode(array("error" => 1, "message" => "Your captcha response could not be validated. Please try again..."));

        } else {

            try {

                if (Zend_Validate::is($postObject->getEmail(), 'EmailAddress')) {

                    $subscribeFlag =  $postObject->getSubscribe();

                    $postData = $postObject->getPostData();
                    $decyptedData = json_decode(Mage::helper('core')->decrypt($postData));

                    $promoId = $decyptedData->promo_id;
                    $transactionalEmailId = $decyptedData->transactional_email_id;

                    if($subscribeFlag == 1){

                        // subscribe email, and send subscription confirmation email
                        Mage::getModel('newsletter/subscriber')->setImportMode(false)->subscribe($postObject->getEmail());

                        # get newly generated subscriber
                        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($postObject->getEmail());

                        # change status to "subscribed" and save
                        $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);

                        $thankyouBlockCode = $decyptedData->thankyou_block_code_alt;

                    } else {

                        // subscribe email, but don't send subscription confirmation email
                        Mage::getModel('newsletter/subscriber')->setImportMode(true)->subscribe($postObject->getEmail());

                        # get newly generated subscriber
                        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($postObject->getEmail());

                        # change status to "unconfirmed" and save
                        $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_UNCONFIRMED);

                        $thankyouBlockCode = $decyptedData->thankyou_block_code;
                    }


                    $subscriber->save();

                    // get rule by id
                    $rule = Mage::getModel('salesrule/rule')->load($promoId);

                    // get static coupon code or generate a new coupon code
                    $couponCode = $rule->getCouponCode() ? $rule->getCouponCode() : $this->getAutoCouponCode($rule->getId());

                    // apply coupon to the shopping cart
                    Mage::getSingleton('checkout/cart')
                        ->getQuote()
                        ->getShippingAddress()
                        ->setCollectShippingRates(true);

                    Mage::getSingleton('checkout/cart')
                        ->getQuote()
                        ->setCouponCode(strlen($couponCode) ? $couponCode : '')
                        ->collectTotals()
                        ->save();

                    // save coupon code in session so it's applied later (by observer) when item is added to the cart
                    Mage::getSingleton("checkout/session")->setData("coupon_code", $couponCode);
                    Mage::getSingleton('checkout/cart')->getQuote()->setCouponCode($couponCode)->save();

                    // send transactional email (with a thank you note and promo code)
                    $storeId = Mage::app()->getStore()->getStoreId();
                    $vars = array('coupon_code' => $couponCode);
                    $sender = array('name' => Mage::getStoreConfig('trans_email/ident_general/name', $storeId), 'email' => Mage::getStoreConfig('trans_email/ident_general/email', $storeId));

                    try {
                        Mage::getModel('core/email_template')->sendTransactional($transactionalEmailId, $sender, $postObject->getEmail(), "", $vars, $storeId);

                        // render thank you message
                        $block = Mage::getModel('cms/block')
                            ->setStoreId(Mage::app()->getStore()->getId())
                            ->load($thankyouBlockCode);

                        $array = array();
                        $array['promo_id'] = $promoId;
                        $array['coupon_code'] = $couponCode;

                        $filter = Mage::getModel('cms/template_filter');
                        $filter->setVariables($array);
                        $thanksContent = $filter->filter($block->getContent());

                        $jsonResponse = Mage::helper('core')->jsonEncode(array("error" => 0, "content" => $thanksContent));

                    } catch (Exception $e) {
                        $jsonResponse = Mage::helper('core')->jsonEncode(array("error" => 1, "message" => $e->getMessage()));

                    }

                }

            } catch (Exception $e) {

                $jsonResponse = Mage::helper('core')->jsonEncode(array("error" => 1, "url" => "Sorry, we've had some trouble saving your request. Please try again. ERROR: " . $e->getMessage()));

            }

        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonResponse);

    }

    /**
     * @desc generates a new coupon code for a specific promo rule
     * @param string $ruleId - rule identifier
     * @return string - coupon code
     */
    public function getAutoCouponCode($ruleId)
    {

        // Get the rule in question
        $rule = Mage::getModel('salesrule/rule')->load($ruleId);

        // Define a coupon code generator
        // Look at Mage_SalesRule_Model_Coupon_Massgenerator for options
        $generator = Mage::getModel('salesrule/coupon_massgenerator');

        // Set parameters for a coupon code format
        $parameters['format'] = "alphanum";
        $parameters['dash_every_x_characters'] = 0;
        $parameters['length'] = "8";
        $parameters['prefix'] = "";
        $parameters['suffix'] = "";

        if (!empty($parameters['format'])) {
            switch (strtolower($parameters['format'])) {
                case 'alphanumeric':
                case 'alphanum':
                    $generator->setFormat(Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHANUMERIC);
                    break;
                case 'alphabetical':
                case 'alpha':
                    $generator->setFormat(Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHABETICAL);
                    break;
                case 'numeric':
                case 'num':
                    $generator->setFormat(Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_NUMERIC);
                    break;
            }
        }

        $generator->setDash(!empty($parameters['dash_every_x_characters']) ? (int)$parameters['dash_every_x_characters'] : 0);
        $generator->setLength(!empty($parameters['length']) ? (int)$parameters['length'] : 6);
        $generator->setPrefix(!empty($parameters['prefix']) ? $parameters['prefix'] : '');
        $generator->setSuffix(!empty($parameters['suffix']) ? $parameters['suffix'] : '');

        // Set the generator, and coupon type so it's able to generate
        $rule->setCouponCodeGenerator($generator);
        $rule->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO);

        // Generate a new coupon code
        $coupon = $rule->acquireCoupon();
        $coupon->setType(Mage_SalesRule_Helper_Coupon::COUPON_TYPE_SPECIFIC_AUTOGENERATED)->save();
        $code = $coupon->getCode();

        return $code;
    }
}
