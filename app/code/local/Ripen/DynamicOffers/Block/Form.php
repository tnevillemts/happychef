<?php
/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2016 Ripen eCommerce
 * @package Ripen_Dynamicoffers
 */

class Ripen_DynamicOffers_Block_Form extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface {

    protected function _toHtml() {
        $html = '';
        $promoId = $this->getData('promo_id');
        if (empty($promoId)) {
            return $html;
        }

        $block = Mage::getSingleton('core/layout')
            ->createBlock('core/template')
            ->setTemplate('ripen/dynamicoffers/form.phtml');

        $block->setData('promo_id', $promoId);
        $block->setData('transactional_email_id', $this->getData('transactional_email_id'));
        $block->setData('thankyou_block_code', $this->getData('thankyou_block_code'));
        $block->setData('thankyou_block_code_alt', $this->getData('thankyou_block_code_alt'));

        $html = $block->toHtml();

        return $html;
    }
}
