<?php
/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2016 Ripen eCommerce
 * @package Ripen_Dynamicoffers
 */

class Ripen_DynamicOffers_Block_Dynamicoffers extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    /**
     * @desc retrieves offer content from a static CMS block
     * @return string - html content for a static block
     */
    public function getDynamicOffer(){

        $block = $this->getLayout()->createBlock('cms/block')->setBlockId('dynamic_offer_'.$this->getRequest()->getParam('offer'));
        return $block->toHtml();

    }

    /**
     * @desc checks if provided offer identifier is valid
     * @param string $offerCode - the unique identifier for a static block
     * @return bool - true or false
     */
    public function checkOfferIsValid($offerCode){

        $collection = Mage::getModel('cms/block')->getCollection()
            ->addFieldToFilter('identifier', array('eq' => 'dynamic_offer_'.$offerCode));

        return $collection->getSize() ? true : false;
    }

    /**
     * @desc retrieves the path to the controller responsible for processing submitted form
     * @return string - route url
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('dynamicoffers/index/post', array('_secure' => false));
    }

}