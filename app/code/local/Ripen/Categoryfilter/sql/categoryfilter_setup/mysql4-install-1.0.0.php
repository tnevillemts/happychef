<?php

$installer = $this;

$installer->startSetup();

$installer->run("
        CREATE TABLE IF NOT EXISTS {$this->getTable('catalog_category_filters')} (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `category_id` int(11) NOT NULL,
          `attribute_id` int(11) NOT NULL,
          `sort_order` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
    ");

$installer->endSetup(); 