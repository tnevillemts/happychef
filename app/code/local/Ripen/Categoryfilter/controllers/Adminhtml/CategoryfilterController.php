<?php

class Ripen_Categoryfilter_Adminhtml_CategoryfilterController extends Mage_Adminhtml_Controller_action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('colormapping/items');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getParams();
        $colorgroup_id = $postData['colorgroup_id'];

        $colorgroupCollection = Mage::getModel('colormapping/colormapping')->getCollection()->addFieldToFilter('colorgroup_id', $colorgroup_id);
        foreach ($colorgroupCollection as $item) {
            $item->delete();
        }

        foreach($postData as $param => $value) {

            if (strpos($param, "colorId_") !== false && $value == 1){
                $colorId = end(explode("_", $param));
                $sortKey = "colorSort_".$colorId;
                $sortOrder = intval($postData[$sortKey]);

                $data = array('colorgroup_id'=>$postData["colorgroup_id"],'color_id'=>$colorId,'sort_order'=>$sortOrder);
                $colorMappingModel = Mage::getModel('colormapping/colormapping')->setData($data);
                try {
                    $colorMappingModel->save();
                } catch (Exception $e){
                    echo $e->getMessage();
                }

            }
        }

        $message = $this->__('Saved Successfully');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);

        $this->_redirect("*/*/edit/id/$colorgroup_id");
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('colormapping');
    }

}
