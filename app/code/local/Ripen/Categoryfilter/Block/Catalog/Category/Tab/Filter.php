<?php
/**
 * Ripen override
 */

class Ripen_Categoryfilter_Block_Catalog_Category_Tab_Filter extends Mage_Adminhtml_Block_Catalog_Form
{

    protected $_category;

    public function __construct()
    {
        parent::__construct();
    }

    public function getCategory()
    {
        if (!$this->_category) {
            $this->_category = Mage::registry('category');
        }
        return $this->_category;
    }

    public function _prepareLayout()
    {
        parent::_prepareLayout();

        $form = new Varien_Data_Form();
        $form->setDataObject($this->getCategory());
        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('catalog')->__('Category Filters')));

        $categoryFilters = Mage::getModel('categoryfilter/categoryfilter')
            ->getCollection()
            ->addFilter('category_id', $this->getRequest()->getParam('id'));

        $savedAttributes = $categoryFilters->getColumnValues('attribute_id');

        $collection = Mage::getResourceModel('catalog/product_attribute_collection')
            ->addFieldToFilter('is_filterable', array("gt"=>0))
            ->getItems();

        $values = $checked = array();
        foreach ($collection as $attribute){
            $values[] = array('value'=>$attribute->getId(),'label'=>$attribute->getFrontendLabel());
            if (in_array($attribute->getId(), $savedAttributes)){
                $checked[] = $attribute->getId();
            }
        }

        $fieldset->addField('checkboxes', 'checkboxes', array(
            'label'     => 'Filters',
            'name'      => 'categoryfilters[]',
            'values' => $values,
            'checked' => false,
            'onclick' => "",
            'onchange' => "",
            'value'  => $checked,
            'disabled' => false,
            'after_element_html' => '',
            'tabindex' => 1
        ));

        //$this->_setFieldset($this->getCategory()->getDesignAttributes(), $fieldset);
        //$form->addValues($this->getCategory()->getData());

        $form->setFieldNameSuffix('categoryfilters');
        $this->setForm($form);

    }


}

