<?php
class Ripen_Categoryfilter_Model_Observer {

    static protected $_singletonFlag = false;


    public function categorySave(Varien_Event_Observer $observer)
    {
        $params = $observer->getRequest()->getParams();
        $categoryId = $params["id"];
        $selectedFilters = $params["categoryfilters"];

        //remove
        Mage::getModel('categoryfilter/categoryfilter')
            ->getCollection()
            ->addFilter('category_id', $categoryId)
            ->walk('delete');

        //re-add
        foreach($selectedFilters["categoryfilters"] as $selectedFilter){

            $categoryFilter = Mage::getModel('categoryfilter/categoryfilter')
                ->setCategoryId($categoryId)
                ->setAttributeId($selectedFilter);
            try {
                $categoryFilter->save();
            } catch (Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

    }


}
