<?php

class Ripen_Categoryfilter_Model_Catalog_Category extends Mage_Catalog_Model_Category
{
    public function isCategoryFilterEnabled($filterId)
    {
        // If none of the individual filters were enabled, we assume that all filters should show

        $categoryFilters = Mage::getModel('categoryfilter/categoryfilter')
            ->getCollection()
            ->addFilter('category_id', $this->getId());

        if ($categoryFilters->count() == 0){
            return true;
        }

        $categoryFilters = Mage::getModel('categoryfilter/categoryfilter')
            ->getCollection()
            ->addFilter('category_id', $this->getId())
            ->addFilter('attribute_id', $filterId)
        ;
        return $categoryFilters->count() ? true : false;

    }
}
