<?php

class Ripen_Categoryfilter_Model_Resource_Categoryfilter_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('categoryfilter/categoryfilter');
    }
}