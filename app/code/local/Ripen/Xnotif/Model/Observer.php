<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Ripen_Xnotif
 */
require_once 'Amasty/Xnotif/Model/Observer.php';
class Ripen_Xnotif_Model_Observer extends Amasty_Xnotif_Model_Observer
{
    /**
     * @param $product
     * @return bool
     */
    protected function getIsInStock($product)
    {
        $isInStock = false;
        $minQuantity = $this->getSettingMinQty();
        if ($product->isConfigurable() && $product->isInStock()) {
            $allProducts = $product->getTypeInstance(true)->getUsedProducts(null, $product);
            foreach ($allProducts as $simpleProduct) {
                $quantity = $this->getQty($simpleProduct);
                $isInStock = ($simpleProduct->isSalable() || $simpleProduct->isInStock())
                    && $quantity >= $minQuantity;

                if ($isInStock) {
                    break;
                }
            }
        } else {
            $isInStock = $this->getQty($product) > 0;
            if (!in_array($product->getTypeId(), array('bundle', 'grouped'))) {
                $quantity = $this->getQty($product);
                $isInStock = $isInStock && (int)$quantity >= (int)$minQuantity;
            }
        }

        return $isInStock;
    }
}
