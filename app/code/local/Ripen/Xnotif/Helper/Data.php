<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Xnotif
 */
class Ripen_Xnotif_Helper_Data extends Amasty_Xnotif_Helper_Data
{
    const SALT = 'AmastyUnsubscribeSalt';

    public function getStockAlertBlock($block)
    {
        $product = $this->getProduct();
        if (method_exists(Mage::helper('productalert'), 'isStockAlertAllowed')) {
            $isStockAlertAllowed = Mage::helper('productalert')->isStockAlertAllowed();
        } else {
            $isStockAlertAllowed = Mage::getStoreConfigFlag('catalog/productalert/allow_stock');
        }

        $today  = Mage::app()->getLocale()->date()->setTime('00:00:00')->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $newFrom = $product->getShowPreorderStart();
        $newTo = $product->getShowPreorderEnd();

        $isNewProduct = ((!is_null($newFrom) && $newFrom <= $today) && (is_null($newTo) || $today <= $newTo)) ?  true : false;

        /* checking product and module settings*/
        if ($product->getData('amxnotif_hide_alert') == '1'
            || !$this->enableForCustomerGroup('stock')
            || (!$isStockAlertAllowed
            || !$product
            || !($product->getStockItem()->getQty() <= $product->getStockItem()->getMinQty())
            || $isNewProduct)
        ) {
            $block->setTemplate('');
            return $block;
        }

        if (!Mage::helper('customer')->isLoggedIn()) {
            $block->setTemplate('ripen/amxnotif/product/view_email.phtml');
        } else {
            $block->setTemplate('ripen/amxnotif/product/view.phtml');
        }

        return $block;
    }

    public function getPriceAlertBlock($block)
    {
        $product = $this->getProduct();

        if (!$this->enableForCustomerGroup('price')
            || (!Mage::helper('productalert')->isPriceAlertAllowed() || false === $product->getCanShowPrice())
        ) {
            $block->setTemplate('');
            return $block;
        }

        if (!Mage::helper('customer')->isLoggedIn()) {
            $block->setTemplate('ripen/amxnotif/product/price/view_email.phtml');
        }

        return $block;
    }


    public function getStockAlert($product, $parentProductId = null)
    {
        $tempCurrentProduct = Mage::registry('current_product');
        Mage::unregister('current_product');
        Mage::register('current_product', $product);

        $alertBlock = Mage::app()->getLayout()->createBlock(
            'productalert/product_view',
            'productalert.stock.' . $product->getId()
        );
        if ($alertBlock) {
            $alertBlock->setProduct($product);
            $alertBlock->setParentProductId($parentProductId);
            $alertBlock->setTemplate('ripen/amxnotif/product/view.phtml');
            $alertBlock->prepareStockAlertData();
            $alertBlock->setHtmlClass('alert-stock link-stock-alert');
            $alertBlock = $this->getStockAlertBlock($alertBlock);
            $html = $alertBlock->toHtml();
        }

        Mage::unregister('current_product');
        Mage::register('current_product', $tempCurrentProduct);

        return $html;
    }

    public function getStockAlertBlockCategory()
    {
        $block = Mage::app()->getLayout()->createBlock('amxnotif/category_subscribe');
        $block = $this->configureTemplate($block);

        return $block;
    }

    public function configureTemplate($block)
    {
        $template = 'ripen/amxnotif/product/view.phtml';
        if (!Mage::helper('customer')->isLoggedIn()) {
            if (Mage::getStoreConfig('amxnotif/stock/with_popup')) {
                $block->setData('popup', true);
            }
            $template = 'ripen/amxnotif/product/view_email.phtml';
        }
        $block->setTemplate($template);

        return $block;
    }

    public function getCustomStockStatusText(Mage_Catalog_Model_Product $product, $qty=0)
    {
        $stockItem   = null;
        $status = $product->getAttributeText('custom_stock_status');

        if (false !== strpos($status, '{qty}'))
        {
            if (!$stockItem)
            {
                $stockItem   = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            }
            $status = str_replace('{qty}', intval($stockItem->getData('qty')  + $qty), $status);
        }

        // search for atttribute entries
        preg_match_all('@\{(.+?)\}@', $status, $matches);
        if (isset($matches[1]) && !empty($matches[1]))
        {
            foreach ($matches[1] as $match)
            {
                if ($value = $product->getData($match))
                {
                    if (preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $value))
                    {
                        $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
                        $value = Mage::getSingleton('core/locale')->date($value, null, null, false)->toString($format);
                    }
                    $status = str_replace('{' . $match . '}', $value, $status);
                }
                else{
                    $status = str_replace('{' . $match . '}', "", $status);
                }
            }
        }
        return $status;
    }

}
