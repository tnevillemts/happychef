<?php

class Ripen_Xnotif_Block_Product_View_Type_Configurable extends Amasty_Stockstatus_Block_Rewrite_Product_View_Type_Configurable
{
    protected $_options;

    protected function _afterToHtml($html)
    {
        $html = Amasty_Stockstatus_Block_Rewrite_Product_View_Type_Configurable_Pure::_afterToHtml($html);
        if ('product.info.options.configurable' == $this->getNameInLayout())
        {
            if (Mage::getStoreConfig('amstockstatus/general/change_custom_configurable_status')) {
                $changeConfigurableStatus = 'true';
            } else {
                $changeConfigurableStatus = 'false';
            }
            $html =  '
            <script type="text/javascript"> 
            var changeConfigurableStatus = '.$changeConfigurableStatus.';
            var amStAutoSelectAttribute = ' . intval(Mage::getStoreConfig('amstockstatus/general/auto_select_attribute')) . '
            </script>' . $html;

            $aStockStatus = array();
            $allProducts = $this->getProduct()->getTypeInstance(true)
                ->getUsedProducts(null, $this->getProduct());
            foreach ($allProducts as $product)
            {
                $key = array();
                for ($i = 0; $i < count($this->_options); $i++)
                {
                    foreach ($this->_options[$i] as $iOptionId => $productIds)
                    {
                        if (in_array($product->getId(), $productIds))
                        {
                            $key[] = $iOptionId;
                        }
                    }
                }

                $stockStatus = '';
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                if ( (!Mage::getStoreConfig('amstockstatus/general/displayforoutonly') || 0 == $stockItem->getData('qty')) || ($product->isInStock() && $stockItem->getData('qty') <= Mage::helper('amstockstatus')->getBackorderQnt() ) )
                {
                    if ($product->getData('hide_default_stock_status') || ($product->isInStock() && 0 == $stockItem->getData('qty')))
                    {
                        $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);

                    } elseif (Mage::helper('amstockstatus')->getCustomStockStatusText($product)) {

                        if (0 == $stockItem->getData('qty'))
                        {
                            $stockStatus = Mage::helper('amstockstatus')->__('Out of Stock') . ' - ' . Mage::helper('amstockstatus')->getCustomStockStatusText($product);
                        } else
                        {
                            $stockStatus = Mage::helper('amstockstatus')->getCustomStockStatusText($product);
                        }
                    }
                }
                if ($key)
                {
                    $productId = $product->getId();
                    $product = Mage::getModel('catalog/product')->load($productId);
                    $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                    $backorders = Mage::helper('amstockstatus')->getBackorderQnt();

                    $today  = Mage::app()->getLocale()->date()->setTime('00:00:00')->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

                    $newFrom = $product->getShowPreorderStart();
                    $newTo = $product->getShowPreorderEnd();

                    $isNewProduct = ((!is_null($newFrom) && $newFrom <= $today) && (is_null($newTo) || $today <= $newTo)) ?  true : false;

                    $aStockStatus[implode(',', $key)] = array(
                        'product_id'     => $product->getId(),
                        'type_id'        => $stockItem->getData('type_id'),
                        'sku'            => $product->getSku(),
                        'custom_status'  => $stockStatus,
                        'custom_status_icon' =>  Mage::helper('amstockstatus')->getStatusIconImage($product),
                        'custom_status_icon_only' => Mage::getStoreConfig('amstockstatus/general/icononly'),
                        'backorder_qty'  => $backorders,
                        'stock_qty'      => (int)$stockItem->getData('qty'),
                        'is_new_product' => $isNewProduct,
                        'stockalert'	 => Mage::helper('amxnotif')->getStockAlert($product),
                        'ship_lead_timedays_amazon' => (int) $product->getData('shipleadtimedaysamazon'),
                    );
                }
            }
            $html .= '<script type="text/javascript">var stStatus = new StockStatus(' . Zend_Json::encode($aStockStatus) . ');</script>';
        }
        $html = $this->helper('amstockstatus')->processViewStockStatus($this->getProduct(), $html);
        return $html;
    }

}
