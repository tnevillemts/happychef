<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Xnotif
 */
require_once 'Amasty/Xnotif/controllers/EmailController.php';
class Ripen_Xnotif_EmailController extends Amasty_Xnotif_EmailController
{
    public function stockAction()
    {
        /* @var $session Mage_Catalog_Model_Session */
        $productId  = (int) $this->getRequest()->getParam('product_id');
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $userEmail = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
        } else {
            $userEmail = $this->getRequest()->getParam('guest_email');
        }

        $parentId  = (int) $this->getRequest()->getParam('parent_id');
        $product = Mage::getModel('catalog/product')->load($productId);

        try {          
            $model = Mage::getModel('productalert/stock')
                ->setProductId($product->getId())
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
           
            if ($parentId) {
                 $model->setParentId($parentId);
            }
            $collection = Mage::getModel('productalert/stock')
                    ->getCollection()
                    ->addWebsiteFilter(Mage::app()->getWebsite()->getId())
                    ->addFieldToFilter('product_id', $productId)
                    ->addStatusFilter(0)
                    ->setCustomerOrder();

            if ($userEmail) {
                $userEmail = filter_var($userEmail, FILTER_SANITIZE_EMAIL);
                if (!Zend_Validate::is($userEmail, 'EmailAddress')) {
                    Mage::throwException($this->__('Please enter a valid email address.'));
                }
                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                $customer->loadByEmail($userEmail);
            
                if (!$customer->getId()) {
                    $model->setEmail($userEmail);
                    $collection->addFieldToFilter('email', $userEmail);
                } else {
                    $model->setCustomerId($customer->getId());
                    $collection->addFieldToFilter('customer_id', $customer->getId());
                }
            } else {
                $model->setCustomerId(Mage::getSingleton('customer/session')->getId());
                $collection->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId());
            }
            
            if ($collection->getSize() > 0) {
                $message = "Thanks! You are already signed up to receive an email when this is back in stock.";
            } else {
                $model->setStoreId(Mage::app()->getStore()->getId());
                $model->save();
                $message = "Thanks! You'll receive an email when this is back in stock.";
            }
        }
        catch (Exception $e) {
            $message = "Unable to update the alert subscription.";
        }

        $response = array('message'=>$message);
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));
    }
    
    public function priceAction()
    {
        $session = Mage::getSingleton('catalog/session');
        /* @var $session Mage_Catalog_Model_Session */
        $backUrl    = $this->getRequest()->getParam(Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED);
        $productId  = (int) $this->getRequest()->getParam('product_id');
        $userEmail  = $this->getRequest()->getParam('guest_email_price');
        $parentId  = (int) $this->getRequest()->getParam('parent_id');
         
        if (!$backUrl) {
            $this->_redirect('/');
            return ;
        }

        if (!$product = Mage::getModel('catalog/product')->load($productId)) {
            /* @var $product Mage_Catalog_Model_Product */
            $session->addError($this->__('Not enough parameters.'));
            $this->_redirectUrl($backUrl);
            return ;
        }

        try {          
            $model  = Mage::getModel('productalert/price')
                ->setCustomerId(Mage::getSingleton('customer/session')->getId())
                ->setProductId($product->getId())
                ->setPrice($product->getFinalPrice())
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
           
            if ($parentId) {
                 $model->setParentId($parentId);
            }
            $collection = Mage::getModel('productalert/price')
                    ->getCollection()
                    ->addWebsiteFilter(Mage::app()->getWebsite()->getId())
		            ->addFieldToFilter('product_id', $productId)
		            ->addFieldToFilter('status', 0)
                    ->setCustomerOrder();

            if ($userEmail) {
                $userEmail = filter_var($userEmail, FILTER_SANITIZE_EMAIL);
                if (!Zend_Validate::is($userEmail, 'EmailAddress')) {
                    Mage::throwException($this->__('Please enter a valid email address.'));
                }
                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                $customer->loadByEmail($userEmail);

                if (!$customer->getId()) {
                    $model->setEmail($userEmail);
                    $collection->addFieldToFilter('email', $userEmail);
                } else {
                    $model->setCustomerId($customer->getId());
                    $collection->addFieldToFilter('customer_id', $customer->getId());
                }
            } else {
                $model ->setCustomerId(Mage::getSingleton('customer/session')->getId());
                $collection->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId());
            }

            if ($collection->getSize() > 0) {
                $session->addSuccess($this->__('Thank you! You are already subscribed to this product.'));
            } else {
                $model->setStoreId(Mage::app()->getStore()->getId());
                $model->save();
                $session->addSuccess($this->__('Alert subscription has been saved.'));
            }
        }
        catch (Exception $e) {
            $session->addException($e, $this->__('Unable to update the alert subscription.'));
        }
        $this->_redirectReferer();
    }
}