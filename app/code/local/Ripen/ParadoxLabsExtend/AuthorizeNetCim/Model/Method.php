<?php

/////////////////////////////////////////////////////////////////////////////////////////////
//      
//       Ripen Paradox Labs overrides.
//      
/////////////////////////////////////////////////////////////////////////////////////////////
class Ripen_ParadoxLabsExtend_AuthorizeNetCim_Model_Method extends ParadoxLabs_AuthorizeNetCim_Model_Method
{
    // They (happychef) want some long messages displayed.
    protected $_text1 = "Please review your billing information.";
    protected $_text2 = "There was an error processing your order. Please try again later or contact us at 800.347.0288 from 9am to 5:30pm EST, Monday through Friday.";
    protected $_text3 = "Your credit card was declined. Please review your billing information or try another form of payment.";

    /**
     * @param array $result
     * @param Varien_Object $payment
     * @param string $tokens
     * @param string $status
     * @return boolean
     *
     * We record the result of every authorize() call in the database.mag_gateway_log .
     */
    public function record($result, $payment, $tokens, $status) {

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        try {

            $session    = Mage::getSingleton('checkout/session');
            $quoteId    = $session->getQuoteId();
            $order      = $payment->getOrder();
            $resultCode = $result['resultCode'];
            $code       = $result['code'];
            $avscode    = $result['avsCode'];
            $order      = $payment->getOrder();
            $orderno    = $order->getIncrementId();

            $logText = [];
            $logText['result-text']             = $result['text'];
            $logText['tokens-customer-payment'] = $tokens;
            $logText['order-email']             = $order->getCustomerEmail();
            $logText['cc-type']                 = $payment->getCcType();
            $logText['cc-last4']                = $payment->getCcLast4();
            $logText['billing-address-id']      = $order->getBillingAddressId();
            $logText['grand-total']             = $order->formatPrice($order->getGrandTotal());

            $row = array('quote_id'   =>$session->getQuoteId(),
                         'order_id'   =>$order->getIncrementId(),
                         'resultCode' =>$result['resultCode'],
                         'status'     =>$status,
                         'code'       =>$code,
                         'avscode'    =>$avscode,
                         'text'       =>json_encode($logText),
                         'created_at' => now());

            $writeConnection->insert("mag_gateway_log", $row);

        } catch(Exception $e) {
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " line: " . __LINE__ . " insert failed, error message [" . $e->getMessage() . "] ", null, 'gateway.log');
            return false;
        }

        return true;
    }

    /**
     * @param Varien_Object $payment
     * @param float $amount
     *
     * authorize() - Authorize a transaction
     */
    public function authorize(Varien_Object $payment, $amount) {

        $this->gateway()->reset();

        if($this->gateway()->devLog()) {
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " -> " . sprintf('authorize(%s %s, %s)', get_class($payment), $payment->getId(), $amount) . " ", null, 'gateway.log');
        }

        $orderno = $payment->getOrder()->getIncrementId();

        $this->_loadOrCreateCard($payment);

        // We have removed the rest of the original authorize() function here since we will
        // not be authorizing the amounts.  But we still have to throw AVS caused exceptions.
        if($this->gateway()->devLog()) {
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . "Card stored, skipping authorization of amount." . $exceptionText, null, 'gateway.log');
        }
        $this->_log('Card stored--skipping authorization');

        $result = array("avsCode" => "", "resultCode" => "", "code" => "", "text" => "");

        // This extension will first issue a <createCustomerPaymentProfileRequest>, if this fails (which
        // will be because the customer payment profile already exists) then this extension will issue
        // an <updateCustomerPaymentProfileRequest>.  We need to use the last returned avsCode values we
        // get back for auto-navigation to checkout form areas due to AVS errors.

        $callSeq = $this->gateway()->getCallSeq();
        if($callSeq == 'C') {

            // This extension has called <createCustomerPaymentProfile>
            $result['resultCode'] = $this->gateway()->getCResultCode();
            $result['avsCode']    = $this->gateway()->getCAvsCode();
            $result['code']       = $this->gateway()->getCCode();
            $result['text']       = $this->gateway()->getCText();
        } else if($callSeq == 'CU') {

            // This extension has called <createCustomerPaymentProfile> and <updateCustomerPaymentProfile>
            $result['resultCode'] = $this->gateway()->getUResultCode();
            $result['avsCode']    = $this->gateway()->getUAvsCode();
            $result['code']       = $this->gateway()->getUCode();
            $result['text']       = $this->gateway()->getUText();
        } else {

            // Now call <validateCustomerPaymentProfile>
            $vResp = $this->gateway()->validateCustomerPaymentProfile();
            $callSeq = $this->gateway()->getCallSeq();
            if($this->gateway()->devLog()) {
                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " vResp-messages-resultCode [" . $vResp['messages']['resultCode'] . "]" . $exceptionText, null, 'gateway.log');
            }
            if(isset($vResp['messages']['resultCode'])) {
                $result['resultCode'] = $vResp['messages']['resultCode'];
            }
            if(isset($vResp['directResponse'])) {
                $cPieces = explode(",", $vResp['directResponse']);
                $result['avsCode'] = $cPieces[5];
            }
            if(isset($vResp['messages']['code'])) {
                $result['code'] = $vResp['messages']['code'];
            }
            if(isset($vResp['messages']['text'])) {
                $result['text'] = $vResp['messages']['text'];
            }
        }

        // Handle AVS codes.
        /////////////////////////////////////////////////////////////////////////////////////////////
        //
        // To induce an AVS error (developer drichter) - Just for development!
        // Only uncomment one resultCode at a time to mimic(force) AVS errors.
        //
        // N -  Both street address and zip are invalid, billing address section.
        //$result['avsCode']     = 'N';
        // A -  Only zip is invalid, billing address section.
        //$result['avsCode']     = 'A';
        // B - Street address or zipcode are missing, theoretically can't happen because of validations on billing address section.
        //$result['avsCode']     = 'B';
        // P - Or anything else is considered a payment error, will send us to payment section.
        //$result['avsCode']     = 'P';
        // R - Retry, system unavailable. Same as error code.
        //$result['avsCode']     = 'R';
        // U - Address info is unavailable for this transaction.
        //$result['avsCode']     = 'U';
        // Y - This should be an allowed error.
        //$result['avsCode']     = 'Y';
        //
        // Uncomment all three of these when inducing errors.
        //$result['resultCode']  = 'Error';
        //$result['code']        = 'E00027';
        //$result['text'] = 'Induced err, avscode: ' . $result['avsCode'];
        // End induce error.
        //
        // Induce success
        //$result['code']        = 'I00001';
        //$result['resultCode']  = 'Ok';
        // End induce success.
        //
        /////////////////////////////////////////////////////////////////////////////////////////////

        $session = Mage::getSingleton('checkout/session');

        // Display the results that we got.
        if($this->gateway()->devLog()) {
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " Call sequence [" . $callSeq . "]", null, 'gateway.log');
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " resultCode [" . $result['resultCode'] . "]", null, 'gateway.log');
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " avsCode [" . $result['avsCode'] . "]", null, 'gateway.log');
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " code [" . $result['code'] . "]", null, 'gateway.log');
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " text [" . $result['text'] . "]", null, 'gateway.log');
            Mage::log("File: " . __FILE__ . " Quote [{$session->getQuote()->getId()}] - AVS code [{$result['avsCode']} - Message [{$result['text']}]", null, 'gateway-temp.log');
        }

        // This is the logic for letting AVS code of 'A' slide for Canadians but catching the for none Canadians.  No using this, for now.
        // $billingCountry = $session->getQuote()->getShippingAddress()->getCountry();
        // if(($billingCountry != 'CA' && ! in_array($result['avsCode'], ['A', 'B', 'N'])) || ($billingCountry == 'CA' && ! in_array($result['avsCode'], ['B', 'N']))) {

        // This is the logic for not catching AVS code of 'A' at all.
        if(!in_array($result['avsCode'], array('B', 'N'))) {

            // If it's not one of the AVS codes that we care about mark it successful.
            $payment->setStatus(self::STATUS_APPROVED);

            // Store tokens in payment.
            if($callSeq == 'V') {
                // Store the token arguments to validate call.  This happens when using stored card, because this call does not hand back these values new.
                $customerProfileId = $this->gateway()->getParameter('customerProfileId');
                $paymentProfileId  = $this->gateway()->getParameter('customerPaymentProfileId');
            } else {
                // Store tokens in payment that came back from create and/or update call. This happens when using new card.
                $customerProfileId = $this->gateway()->getCustomerProfileId();
                $paymentProfileId  = $this->gateway()->getPaymentProfileId();
            }

            $approval = '0';
            $ShippingProfileID = '0';
            $tokens = $customerProfileId . "-" . $paymentProfileId . "-" . $approval . "-" . $ShippingProfileID;
            $payment->setData('po_number', $tokens);
            $payment->setData('cc_avs_status', $result['avsCode']);

            $this->record($result, $payment, $tokens, 'Ok');

        } else {
            
            // Otherwise handle the AVS code as an error. Use messages defines at top of file depending on the AVS codes we got.
            $result['resultCode'] = "Error";
            if($result['avsCode'] == 'A' || $result['avsCode'] == 'N') {
                $result['text'] = $this->_text1;
            } else if($result['avsCode'] == 'B') {
                $result['text'] = $this->_text3;
            } else {
                $result['text'] = $this->_text2;
            }

            if(strpos(strtolower($result['text']), 'a duplicate transaction has been submitted') !== false) {
                $result['text'] = "Please review your billing information before submitting again.";
            }
            if(strpos(strtolower($result['text']), 'credit card type is not allowed for this payment method') !== false) {
                $result['text'] = "Only the following types of payment are accepted: Visa, Mastercard, American Express, and Discover.";
            }
            if(strpos(strtolower($result['text']), ' avs mismatch') !== false) {
                $result['text'] = "Please review your billing information.";
            }

            $this->record($result, $payment, 'na', 'Decline');

            // Throw exception.
            $exceptionText = '';
            try {
                if(strpos($result['text'], 'This transaction has been declined') !== false) {
                    $exceptionText = Mage::helper('checkout/cart')->__($result['text']);
                    if($exceptionText == '' || $exceptionText == null) {
                        $exceptionText = "Gateway error code " . $result['code'] . ": " . $result['text'];
                    }
                    if($this->gateway()->devLog()) {
                        Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " " . $exceptionText, null, 'gateway.log');
                    }
                } else {
                    $exceptionText = "Gateway error code " . $result['code'] . ": " . $result['text'];
                }
            } catch(Exception $e) {
                Mage::throwException("Another exception caught here: " . $e->getMessage() . ".");
            }

            if($this->gateway()->devLog()) {
                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " Throwing exception." . $exceptionText, null, 'gateway.log');
            }

            Mage::throwException($exceptionText);
        }
        return $this;
    }
}

