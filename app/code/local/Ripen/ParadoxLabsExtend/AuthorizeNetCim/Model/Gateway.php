<?php

/////////////////////////////////////////////////////////////////////////////////////////////
//
//  	Ripen Paradox Labs overrides.
//	
// 	We override functions in their entirety here.
//
/////////////////////////////////////////////////////////////////////////////////////////////
class Ripen_ParadoxLabsExtend_AuthorizeNetCim_Model_Gateway extends ParadoxLabs_AuthorizeNetCim_Model_Gateway
{
    // drichter - Store Ids collected in transactions.
    protected $_merchantCustomerId = 'M';
    protected $_customerProfileId = 'C';
    protected $_paymentId = 'P';

    protected $_cAvsCode = '';
    protected $_cCode = '';
    protected $_cText = '';
    protected $_cResultCode = '';

    protected $_uAvsCode = '';
    protected $_uCode = '';
    protected $_uText = '';
    protected $_uResultCode = '';

    protected $_callSeq = '';

    // Controls if the requests and responses are recorded in the database and log files. (For debugging only).
    protected $_devLog = FALSE;
    protected $_email = '';

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  	Get the stored Ids, codes, results from transactions.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function getMerchantCustomerId()
    {
        return $this->_merchantCustomerId;
    }

    public function getCustomerProfileId()
    {
        return $this->_customerProfileId;
    }

    public function getPaymentProfileId()
    {
        return $this->_paymentProfileId;
    }

    public function getCAvsCode()
    {
        return $this->_cAvsCode;
    }

    public function getUAvsCode()
    {
        return $this->_uAvsCode;
    }

    public function getCResultCode()
    {
        return $this->_cResultCode;
    }

    public function getUResultCode()
    {
        return $this->_uResultCode;
    }

    public function getCCode()
    {
        return $this->_cCode;
    }

    public function getUCode()
    {
        return $this->_uCode;
    }

    public function getCText()
    {
        return $this->_cText;
    }

    public function getUText()
    {
        return $this->_uText;
    }

    public function devLog()
    {
        return $this->_devLog;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // 	_callSeq  denotes what APIs were used by extension and in what order.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function getCallSeq()
    {
        return $this->_callSeq;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //      reset()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function reset()
    {
        $this->_merchantCustomerId = 'M';
        $this->_customerProfileId = 'C';
        $this->_paymentId = 'P';

        $this->_cAvsCode = '';
        $this->_cCode = '';
        $this->_cText = '';
        $this->_cResultCode = '';

        $this->_uAvsCode = '';
        $this->_uCode = '';
        $this->_uText = '';
        $this->_uResultCode = '';

        $this->_callSeq = '';
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //      xmlToArray()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function xmlToArray($pxml)
    {
        return $this->_xmlToArray($pxml);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //      recordRequest()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function recordRequest($url, $req, $resp, $customerEmail, $userAgent)
    {

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $ret = FALSE;

        try {

            $sql = "INSERT INTO mag_gateway_request (url, req, resp, customer_email, user_agent, created_at) " . " VALUES ('" . $url . "', '" . $req . "', '" . $resp . "', '" . $customerEmail . "', '" . $userAgent . "', now())";

            $result = $writeConnection->query($sql);

            if (!$result) {
                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " line: " . __LINE__ . " sql failed [" . $sql . "]", null, 'gateway.log');
            } else {
                $ret = TRUE;
            }

        } catch (Exception $e) {
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " line: " . __LINE__ . " sql failed [" . $sql . "] error message [" . $e->getMessage() . "] ", null, 'gateway.log');
        }

        return $ret;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  	Send the given request to Authorize.Net and process the results.
    //	Override so that we can log requests and responses.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    protected function _runTransaction($request, $params)
    {
        $auth = array(
            '@attributes' => array(
                'xmlns' => 'AnetApi/xml/v1/schema/AnetApiSchema.xsd',
            ),
            'merchantAuthentication' => array(
                'name' => $this->getParameter('loginId'),
                'transactionKey' => $this->getParameter('transactionKey'),
            )
        );

        $xml = $this->_arrayToXml($request, $auth + $params);

        $this->_lastRequest = $xml;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->_endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);

        if (!in_array($request, array('createTransactionRequest', 'createCustomerProfileTransactionRequest'))) {
            curl_setopt($curl, CURLOPT_TIMEOUT, 15);
        }

        curl_setopt($curl, CURLOPT_CAINFO, Mage::getModuleDir('', 'ParadoxLabs_AuthorizeNetCim') . '/resources/authorizenet-cert.pem');

        if ($this->_verifySsl === true) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        } else {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }

        // drichter - log request.
        if ($this->_devLog) {
            Mage::log('In file [' . __FILE__ . '] function [' . __FUNCTION__ . '] REQUEST: [' . $this->_sanitizeLog($xml) . '] ', null, 'gateway.log');
        }

        $this->_lastResponse = curl_exec($curl);

        // drichter - log response.
        if ($this->_devLog) {
            Mage::log('In file [' . __FILE__ . '] function [' . __FUNCTION__ . '] RESPONSE: [' . $this->_sanitizeLog($this->_lastResponse) . '] ', null, 'gateway.log');
        }

        // drichter - comment out this line to disable recording of all xml requests and responses.
        if ($this->_devLog) {
            $userAgent = Mage::helper('core/http')->getHttpUserAgent();
            $email = $this->getParameter('email');
            $this->recordRequest($this->_endpoint, str_replace("'", "-", $this->_sanitizeLog($xml)), str_replace("'", "-", $this->_sanitizeLog($this->_lastResponse)), $email, $userAgent);
        }

        $this->_log .= 'REQUEST: ' . $this->_sanitizeLog($xml) . "\n";
        $this->_log .= 'RESPONSE: ' . $this->_sanitizeLog($this->_lastResponse) . "\n";

        if ($this->_lastResponse && !curl_errno($curl)) {

            $this->_lastResponse = $this->_xmlToArray($this->_lastResponse);

            if ($this->_testMode === true) {
                Mage::helper('tokenbase')->log($this->_code . '-debug', $this->_log, true);
            }

            /**
             * Check for basic errors.
             */
            if ($this->_lastResponse['messages']['resultCode'] != 'Ok') {

                $errorCode = $this->_getArrayValue($this->_lastResponse, 'messages/message/code');
                $errorText = $this->_getArrayValue($this->_lastResponse, 'messages/message/text');

                Mage::log("File: " . __FILE__ . " - Error code [{$errorCode}] - Message [{$errorText}]", null, 'gateway-temp.log');

                $remap = 0;
                if (strpos(strtolower($errorText), 'a duplicate transaction has been submitted') !== false) {
                    $errorText = "Please review your billing information before submitting again.";
                    $remap++;
                }
                if (strpos(strtolower($errorText), 'credit card type is not allowed for this payment method') !== false) {
                    $errorText = "Only the following types of payment are accepted: Visa, Mastercard, American Express, and Discover.";
                    $remap++;
                }
                if (strpos(strtolower($errorText), ' avs mismatch') !== false) {
                    $errorText = "Please review your billing information.";
                    $remap++;
                }

                if($remap){
                    Mage::log("File: " . __FILE__ . " - Error code [{$errorCode}] - Remapped Message [{$errorText}]", null, 'gateway-temp.log');
                }

                $errorText2 = $this->_getArrayValue($this->_lastResponse, 'transactionResponse/errors/error/errorText');

                /**
                 * Log and spit out generic error. Skip certain warnings we can handle.
                 */
                $okayErrorCodes = array('E00039', 'E00040');
                $okayErrorTexts = array('The referenced transaction does not meet the criteria for issuing a credit.', 'The transaction cannot be found.');

                if (!empty($errorCode) && !in_array($errorCode, $okayErrorCodes) && !in_array($errorText, $okayErrorTexts) && !in_array($errorText2, $okayErrorTexts)) {
                    Mage::helper('tokenbase')->log($this->_code, sprintf("API error: %s: %s\n%s", $errorCode, $errorText, $this->_log));
                    throw Mage::exception('Mage_Payment_Model_Info', Mage::helper('tokenbase')->__(sprintf('%s', $errorText)));
                }
            }

            curl_close($curl);
        } else {
            Mage::helper('tokenbase')->log($this->_code, sprintf('CURL Connection error: ' . curl_error($curl) . ' (' . curl_errno($curl) . ')' . "\n" . 'REQUEST: ' . $this->_sanitizeLog($xml)));
            Mage::throwException(Mage::helper('tokenbase')->__(sprintf('Authorize.Net CIM Gateway Connection error: %s (%s)', curl_error($curl), curl_errno($curl))));
        }

        return $this->_lastResponse;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Override to Get profile Id.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function createCustomerProfile()
    {
        $params = array(
            'profile' => array(
                'merchantCustomerId' => intval($this->getParameter('merchantCustomerId')),
                'description' => $this->getParameter('description'),
                'email' => $this->getParameter('email'),
            ),
        );

        $result = $this->_runTransaction('createCustomerProfileRequest', $params);

        $text = $this->_getArrayValue($result, 'messages/message/text');

        if (isset($result['customerProfileId'])) {
            // drichter - Store Id in class variable.
            $this->_merchantCustomerId = $this->getParameter('merchantCustomerId');
            return $result['customerProfileId'];
        } elseif (strpos($text, 'duplicate') !== false) {
            return preg_replace('/[^0-9]/', '', $text);
        } else {
            $this->logLogs();
            Mage::throwException(Mage::helper('tokenbase')->__('Authorize.Net CIM Gateway: Unable to create customer profile. %s', $result['messages']['message']['text']));
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Override to Get profile Ids.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function createCustomerPaymentProfile()
    {
        $params = array(
            'customerProfileId' => $this->getParameter('customerProfileId'),
            'paymentProfile' => array(
                'billTo' => array(
                    'firstName' => $this->getParameter('billToFirstName'),
                    'lastName' => $this->getParameter('billToLastName'),
                    'company' => $this->getParameter('billToCompany'),
                    'address' => $this->getParameter('billToAddress'),
                    'city' => $this->getParameter('billToCity'),
                    'state' => $this->getParameter('billToState'),
                    'zip' => $this->getParameter('billToZip'),
                    'country' => $this->getParameter('billToCountry'),
                    'phoneNumber' => $this->getParameter('billToPhoneNumber'),
                    'faxNumber' => $this->getParameter('billToFaxNumber'),
                ),
                'payment' => array(),
            ),
            'validationMode' => $this->getParameter('validationMode'),
        );

        if ($this->hasParameter('customerType')) {
            $params['paymentProfile'] = array(
                    'customerType' => $this->getParameter('customerType')
                ) + $params['paymentProfile'];
        }

        if ($this->hasParameter('cardNumber')) {
            $params['paymentProfile']['payment'] = array(
                'creditCard' => array(
                    'cardNumber' => $this->getParameter('cardNumber'),
                    'expirationDate' => $this->getParameter('expirationDate'),
                ),
            );

            if ($this->hasParameter('cardCode')) {
                $params['paymentProfile']['payment']['creditCard']['cardCode'] = $this->getParameter('cardCode');
            }
        } elseif ($this->hasParameter('accountNumber')) {
            $params['paymentProfile']['payment'] = array(
                'bankAccount' => array(
                    'accountType' => $this->getParameter('accountType'),
                    'routingNumber' => $this->getParameter('routingNumber'),
                    'accountNumber' => $this->getParameter('accountNumber'),
                    'nameOnAccount' => $this->getParameter('nameOnAccount'),
                    'echeckType' => $this->getParameter('echeckType'),
                    'bankName' => $this->getParameter('bankName'),
                ),
            );
        }

        $this->_cAvsCode = '';
        $this->_cCode = '';
        $this->_cText = '';
        $this->_cResultCode = '';

        $this->_callSeq .= 'C';
        $cresult = $this->_runTransaction('createCustomerPaymentProfileRequest', $params);

        $paymentId = null;

        if (isset($cresult['customerPaymentProfileId'])) {
            $paymentId = $cresult['customerPaymentProfileId'];
        }

        $text = $this->_getArrayValue($cresult, 'messages/message/text');

        // drichter - Get avs code from createCustomerPaymentProfileResponse
        if (isset($cresult['messages']['resultCode'])) {
            $this->_cResultCode = $cresult['messages']['resultCode'];
        }

        if (isset($cresult['messages']['message']['code'])) {
            $this->_cCode = $cresult['messages']['message']['code'];
        }

        if (isset($cresult['messages']['message']['text'])) {
            $this->_cText = $cresult['messages']['message']['text'];
        }

        if (isset($cresult['validationDirectResponse'])) {
            $cPieces = explode(",", $cresult['validationDirectResponse']);
            $this->_cAvsCode = $cPieces[5];
        }

        if (strpos($text, 'duplicate') !== false) {
            /**
             * Handle duplicate card errors. Painful process.
             */

            if (empty($paymentId)) {
                $paymentId = preg_replace('/[^0-9]/', '', $text);
            }

            /**
             * If we still have no payment ID, try to match the duplicate manually.
             * Authorize.Net does not return the ID in this duplicate error message, contrary to documentation.
             */
            if (empty($paymentId)) {
                $paymentId = $this->findDuplicateCard();
            }

            if (!empty($paymentId)) {
                // Update the card record to ensure CVV and expiry are up to date.
                $this->setParameter('customerPaymentProfileId', $paymentId);
                $this->_callSeq .= 'U';
                $uresult = $this->updateCustomerPaymentProfile();

                // drichter - Get avs code from updateCustomerPaymentProfileResponse
                $this->_uAvsCode = '';
                $this->_uCode = '';
                $this->_uText = '';
                $this->_uResultCode = '';

                if (isset($uresult['messages']['resultCode'])) {
                    $this->_uResultCode = $uresult['messages']['resultCode'];
                }

                if (isset($uresult['messages']['message']['code'])) {
                    $this->_uCode = $uresult['messages']['message']['code'];
                }

                if (isset($uresult['messages']['message']['text'])) {
                    $this->_uText = $uresult['messages']['message']['text'];
                }

                if (isset($uresult['validationDirectResponse'])) {
                    $uPieces = explode(",", $uresult['validationDirectResponse']);
                    $this->_uAvsCode = $uPieces[5];
                }
            }
        }

        // drichter - Store Ids in class variables.
        $this->_merchantCustomerId = $this->getParameter('merchantCustomerId');
        $this->_customerProfileId = $this->getParameter('customerProfileId');
        $this->_paymentProfileId = $paymentId;

        return $paymentId;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Override to fix customerShippingAddressId issue.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function setCard(ParadoxLabs_TokenBase_Model_Card $card)
    {
        $this->setParameter('email', $card->getCustomerEmail());
        $this->setParameter('merchantCustomerId', $card->getCustomerId());
        $this->setParameter('customerProfileId', $card->getProfileId());
        $this->setParameter('customerPaymentProfileId', $card->getPaymentId());
        $this->setParameter('customerIp', $card->getCustomerIp());

        // drichter - Set the shipping address Id.
        $this->setParameter('customerShippingAddressId', $card->getShippingAddressId());

        return parent::setCard($card);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Override to fix customerShippingAddressId issue.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    public function validateCustomerPaymentProfile()
    {
        // drichter - Set the shipping address Id to '0' if it's empty.
        $customerShippingAddressId = $this->getParameter('customerShippingAddressId');
        if (empty($customerShippingAddressId)) {
            $customerShippingAddressId = '0';
        }
        $params = array(
            'customerProfileId' => $this->getParameter('customerProfileId'),
            'customerPaymentProfileId' => $this->getParameter('customerPaymentProfileId'),
            'customerShippingAddressId' => $customerShippingAddressId,
            //'validationMode'              => $this->getParameter('validationMode'),
            'validationMode' => 'testMode',
        );

        $this->_callSeq .= 'V';
        return $this->_runTransaction('validateCustomerPaymentProfileRequest', $params);
    }
}
