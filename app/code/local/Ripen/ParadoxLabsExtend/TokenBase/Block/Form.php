<?php
/**
 * Paradox Labs, Inc.
 * http://www.paradoxlabs.com
 * 717-431-3330
 * 
 * Need help? Open a ticket in our support system:
 *  http://support.paradoxlabs.com
 * 
 * Want to customize or need help with your store?
 *  Phone: 717-431-3330
 *  Email: sales@paradoxlabs.com
 *
 * @category	ParadoxLabs
 * @package		TokenBase
 * @author		Ryan Hoerr <magento@paradoxlabs.com>
 * @license		http://store.paradoxlabs.com/license.html
 */

abstract class Ripen_ParadoxLabsExtend_TokenBase_Block_Form extends ParadoxLabs_TokenBase_Block_Form
{
	protected $_cards		= null;
	
	/**
	 * Instantiate with default payment form (stored CC).
	 */
	protected function _construct()
	{
		parent::_construct();
		$this->setTemplate('paradoxlabs/tokenbase/form.phtml');
	}
}
