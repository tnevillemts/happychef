<?php
class Ripen_Colormapping_Model_Observer {

    /**
     * Rebuild configurable product colorgroup attributes based
     * on colors for associated simple products.
     */
    public function adjustColorgroupAttributes()
    {
        // Get the color attribute, and the colorgroup attribute and all it's options.
        $productModel = Mage::getModel('catalog/product');
        $colorGroupAttribute = $productModel->getResource()->getAttribute('colorgroup');
        $allColorGroupOptions = $colorGroupAttribute->getSource()->getAllOptions(true, true);
        $colorGroupAttributeOptionValues = array_column($allColorGroupOptions, 'value');

        // Rebuild color attributes only if color dependency is enabled (System > Configuration > Ripen > Color Mapping)
        if( Mage::getStoreConfig('ripen_colormapping/general/enable_color_dependency') ){
            $configurableProducts = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('type_id', 'configurable')
                ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));

            // Iterate through configurable products
            $productsToReindex = array();
            foreach($configurableProducts as $configurableProduct){

                // For each configurable product find all children
                $childProducts = $configurableProduct->getTypeInstance()->getUsedProducts();
                $colorgroupsArray = array();
                foreach($childProducts as $child) {
                    $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($child->getId());
                    $allowBackorder = $stockItem->getData('backorders');

                    // If child is enabled and set to allow backorders, build an array of valid colorgroups based on color mapping rules
                    if($child->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED && ($allowBackorder == 1 || ($allowBackorder == 0 && $stockItem->getQty() > 0 ))){
                        $val = Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getId(), 'color', 1);

                        $colorgroupCollection = Mage::getModel('colormapping/colormapping')->getCollection()
                            ->addFieldToSelect('colorgroup_id')
                            ->addFieldToFilter('color_id', $val);

                        $childColorgroups = $colorgroupCollection->getColumnValues('colorgroup_id');
                        $colorgroupsArray = array_merge($colorgroupsArray, $childColorgroups);

                        $oldChildColorGroups = Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getId(), 'colorgroup', 1);
                        $oldChildColorGroupsArray = explode(',', $oldChildColorGroups);

                        // Eliminate from the childColorGroup array those elements that will not be found in the color/pattern attribute for the simple product.
                        $childColorgroupsWithoutExcluded = array_intersect($childColorgroups, $colorGroupAttributeOptionValues);

                        if(array_diff($oldChildColorGroupsArray, $childColorgroupsWithoutExcluded) !== array_diff($childColorgroupsWithoutExcluded, $oldChildColorGroupsArray)) {
                            Mage::log("\tUPDATE Product sku [" . $child->getSku() . "] color [" . $child->getColor() . "] former childColorGroups [" . $oldChildColorGroups . "] new childColorgroups [" . implode(',', $childColorgroupsWithoutExcluded) . "]", null, 'colorgroups.log');
                            $child->setColorgroup($childColorgroupsWithoutExcluded);
                            $child->save();
                        } else {
                            //Mage::log("\tProduct sku [" . $child->getSku() . "] color [" . $child->getColor() . "] former childColorGroups [" . $oldChildColorGroups . "] new childColorgroups [" . implode(',', $childColorgroupsWithoutExcluded) . "]", null, 'colorgroups.log');
                        }
                    }
                }

                $newColorGroups = array_values(array_unique($colorgroupsArray));

                $oldColorGroups = explode(",", Mage::getResourceModel('catalog/product')->getAttributeRawValue($configurableProduct->getId(), 'colorgroup', 1));

                // Update colorgroup for configurable product if current values are different from those calculated above.
                if(count($newColorGroups) && (array_diff($newColorGroups, $oldColorGroups) || array_diff($oldColorGroups, $newColorGroups))){

                    $configurableProduct->setTierPrice($configurableProduct->getTierPrice());
                    $configurableProduct->setGroupPrice($configurableProduct->getGroupPrice());
                    $configurableProduct->setColorgroup($newColorGroups);
                    $configurableProduct->save();

                    //$p = Mage::getModel('catalog/product')->load($configurableProduct->getId());
                    //$p->setTierPrice($configurableProduct->getTierPrice());
                    //$p->setGroupPrice($configurableProduct->getGroupPrice());
                    //$p->setData('colorgroup', $newColorGroups);
                    //$p->save();

                    // add product to the reindex queue
                    $productsToReindex[] = $configurableProduct->getId();

                }
            }

            if(count($productsToReindex)){
                $productsToReindex = array_unique($productsToReindex);
                // create a file and write serialized array into it
                $filename = Mage::getBaseDir('base')."/var/to_reindex/".date("Y_m_d_H_i_s");
                $serialized = serialize($productsToReindex);
                file_put_contents($filename, $serialized);
                Mage::log("Products added to reindex queue [" . $filename . "] [".$serialized."]", null, "colorgroups.log");
            } else {
                Mage::log("No products with changed colorgroups found", null, "colorgroups.log");
            }
        }
    }
}
