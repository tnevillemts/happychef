<?php

class Ripen_Colormapping_Model_Resource_Colormapping_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('colormapping/colormapping');
    }
}