<?php

$installer = $this;

$installer->startSetup();

$installer->run("
        CREATE TABLE IF NOT EXISTS {$this->getTable('colormapping_mapping')} (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `colorgroup_id` int(11) NOT NULL,
          `color_id` int(11) NOT NULL,
          `sort_order` int(11) DEFAULT NULL,
          KEY `color_mapping_colorgroup_id` (`colorgroup_id`),
          KEY `color_mapping_color_id` (`color_id`),
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
    ");

$installer->endSetup(); 