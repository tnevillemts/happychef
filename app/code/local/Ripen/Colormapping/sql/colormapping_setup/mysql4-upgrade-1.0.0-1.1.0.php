<?php

$installer = $this;

$installer->startSetup();

$installer->run("RENAME TABLE {$this->getTable('color_mapping')} TO {$this->getTable('colormapping_mapping')}");

$installer->run("ALTER TABLE {$this->getTable('colormapping_mapping')} ADD `id`  int(11) AUTO_INCREMENT PRIMARY KEY");

//$installer->run("ALTER TABLE {$this->getTable('colormapping_mapping')} ADD PRIMARY KEY (`id`)");

$installer->endSetup(); 