<?php

class Ripen_ApplyCoupon_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $coupon_code = $this->getRequest()->getParam('coupon_code');

        if ($coupon_code != '') {
            Mage::getSingleton("checkout/session")->setData("coupon_code", $coupon_code);
            Mage::getSingleton('checkout/cart')->getQuote()->setCouponCode($coupon_code)->save();
            Mage::getSingleton('core/session')->addSuccess($this->__('Coupon was automatically applied'));
        } else {
            Mage::getSingleton("checkout/session")->setData("coupon_code", "");
            $cart = Mage::getSingleton('checkout/cart');
            foreach (Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item) {
                $cart->removeItem($item->getId());
            }
            $cart->save();
        }

        if ($this->getRequest()->getParam('url')) {
            //using raw header instead of _redirect because _redirect appends a /
            header('HTTP/1.1 301 Moved Permanently');
            $url = $this->getRequest()->getParam('url');
            header('Location: /' . $url);
            die();
        } else {
            $this->_redirect("/");
        }
    }

    public function applyCouponAjaxAction()
    {
        $coupon_code = $this->getRequest()->getParam('coupon_code');
        $salesRule = Mage::getModel('salesrule/coupon')->load($coupon_code, 'code');
        $salesRuleExists = count($salesRule->getData());

        if ($salesRuleExists) {
            $date = date('Y-m-d h:i:s');
            $expirationDate = $salesRule->getExpirationDate();
            $couponIsExpired = $date > $expirationDate;
            if  (!$couponIsExpired || is_null($expirationDate)) {
                $cart = Mage::getSingleton('checkout/cart');
                $quote = $cart->getQuote();
                Mage::getSingleton("checkout/session")->setData("coupon_code", $coupon_code);
                $quote->setCouponCode($coupon_code);
                $quote->save();
                $msg = "The coupon has been applied.";
                $status = true;
            } else {
                $msg = "This coupon has expired.";
                $status = false;
            }
        } else {
            $msg = "The coupon code is invalid.";
            $status = false;
        }

        $response = array('status'=>$status, 'message'=>$msg, 'expiration_date'=>$expirationDate);
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(json_encode($response));

    }
}
