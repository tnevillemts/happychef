<?php
class Ripen_Embroidery_Block_Adminhtml_Artwork_Manager extends Mage_Adminhtml_Block_Template
{
    const AJAX_URLS = [
        'categories' => 'embroidery/adminhtml_embroideryartwork/categories',
        'saveCategoriesOrder' => 'embroidery/adminhtml_embroideryartwork/saveCategoriesOrder',
        'saveArtworkOptionsOrder' => 'embroidery/adminhtml_embroideryartwork/saveArtworkOptionsOrder',
        'deleteArtwork' =>  'embroidery/adminhtml_embroideryartwork/deleteArtwork'
    ];

    /**
     * Set the template for the block
     */
    public function _construct() {
        parent::_construct();
    }

    public function getViewModelConfig() {
        $config = [];
        $config['formKey'] = $this->getFormKey();
        $config['baseMediaUrl'] = Mage::getBaseUrl('media');
        $config['ajaxUrls'] = $this->getAjaxUrls();
        $config['categories'] = Mage::helper('embroidery')->getArtworkCategoriesData();
        $config['artworkOptions'] = Mage::helper('embroidery')->getArtworkCategoryOptionsData();
        return json_encode($config);
    }

    protected function getAjaxUrls() {
        $ajaxUrls = [];
        foreach(self::AJAX_URLS as $urlKey => $url) {
            $ajaxUrls[$urlKey] = Mage::helper("adminhtml")->getUrl($url);
        }
        return $ajaxUrls;
    }

    public function categoriesList($sortOrder = 'ASC') {
        $cat = Mage::getModel('embroidery/artworkcategory')
            ->getCollection()
            ->setOrder('sortorder', $sortOrder)
        ->load();
        return $cat;
    }

    public function testResource() {
        return Mage::getModel('embroidery/option')
            ->getCollection()
            ->getOptionsByTypeName('artwork');
    }
}