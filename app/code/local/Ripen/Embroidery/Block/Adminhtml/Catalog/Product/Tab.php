<?php
class Ripen_Embroidery_Block_Adminhtml_Catalog_Product_Tab extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('ripen/embroidery/catalog/product/tab.phtml');
    }

    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Embroidery Options (New)');
    }

    /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Click here to view your custom tab content');
    }

    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    public function getLocations(){

        $locationCollection = Mage::getModel('embroidery/location')
            ->getCollection()
            ->setOrder('sortorder', "ASC");

        return $locationCollection;

    }

    public function getTypes(){

        $typeCollection = Mage::getModel('embroidery/type')
            ->getCollection()
            ->setOrder('sortorder', "ASC");

        return $typeCollection;

    }


    public function getSelectedTypes($locationId){

        $productId = Mage::registry('product')->getId();
        $typesCollection = Mage::getModel('embroidery/embroidery')
            ->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('location_id', $locationId);

        $typesCollection->getSelect()
            ->join(
                array('types' => Mage::getSingleton('core/resource')->getTableName('embroidery/type')),
                'main_table.type_id = types.type_id',
                array('type_name' => 'name'))
            ->order('types.sortorder', 'ASC');

        return $typesCollection;

    }

    public function getProductEmbroideryRecord($locationId, $typeId){

        $productId = Mage::registry('product')->getId();
        $record = Mage::getModel('embroidery/embroidery')
            ->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('location_id', $locationId)
            ->addFieldToFilter('type_id', $typeId)
            ->getFirstItem();

        return $record;
    }

    public function getAssignedTemplateId(){
        $templateProductRecord = Mage::getModel('embroidery/templateproduct')
            ->getCollection()
            ->addFieldToFilter('product_id', Mage::registry('product')->getId())
            ->getFirstItem();

        return $templateProductRecord ? $templateProductRecord->getTemplateId() : false;

    }
}



