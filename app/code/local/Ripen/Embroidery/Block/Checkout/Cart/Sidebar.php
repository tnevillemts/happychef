<?php
class Ripen_Embroidery_Block_Checkout_Cart_Sidebar extends Mage_Checkout_Block_Cart_Sidebar
{
    public function __construct()
    {
        $this->addItemRender('embroidery_setup', 'checkout/cart_item_renderer', 'ripen/bettercheckout/cart/item/sidebar/logosetupfee.phtml');
        parent::__construct();
    }
}
