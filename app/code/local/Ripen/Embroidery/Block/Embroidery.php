<?php

/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2017 Ripen eCommerce
 * @package Ripen_Embroidery
 */

class Ripen_Embroidery_Block_Embroidery extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    public function getCustomer()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if ($customer->getId()):
            return $customer;
        endif;

        return false;
    }

}