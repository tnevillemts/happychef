<?php

/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2017 Ripen eCommerce
 * @package Ripen_Embroidery
 */

class Ripen_Embroidery_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @const array EMBROIDERY_SETUP_SKUS
     */
    const EMBROIDERY_SETUP_SKUS = array(
        'EMBROIDERY-SETUP',
        'EMBROIDERY-SETUP-FRE'
    );

    /**
     * @const array EMBROIDERY_TYPES
     */
    const EMBROIDERY_TYPES = array(
        'ribbons' => array('label' => 'Ribbons'),
        'flags' => array('label' => 'Flags'),
        'artwork' => array('label' => 'Artwork'),
        'text' => array('label' => 'Text'),
        'logo' => array('label' => 'Custom Logo Application Fee'),
        'acfLogos' => array('label' => 'ACF Logo')
    );

    /**
     * Search multidimensional array for specific key
     *
     * @param $array
     * @param $key
     * @return array
     */
    public function _search($array, $key)
    {
        $results = array();

        if (is_array($array))
        {
            if (isset($array[$key]))
                $results[] = $array;

            foreach ($array as $subArray)
                $results = array_merge($results, $this->_search($subArray, $key));
        }

        return $results;
    }

    /**
     * Check if embroidery has at least one option set
     * @param $embroideryData
     * @return bool
     */
    public function isEmbroiderySet($embroideryData)
    {
        $selectedOptions = $this->_search(json_decode($embroideryData, true), 'price');
        return count($selectedOptions) ? true : false;
    }

    public function getEmbroideryPrice($embroideryData)
    {
        $totalPrice = 0;
        $embroideryData = json_decode($embroideryData, true);

        $selectedOptions = $this->_search($embroideryData, 'price');
        foreach($selectedOptions as $selectedOption) {
            $totalPrice += $selectedOption["price"];
        }

        return $totalPrice;

    }

    public function uploadEmbroideryImage($image, $directory = NULL){
        try {

            $directory = $directory ? $directory : "embroidery_options";

            $uploader = new Varien_File_Uploader(
                array(
                    'name' => $image['name'],
                    'type' => $image['type'],
                    'tmp_name' => $image['tmp_name'],
                    'error' => $image['error'],
                    'size' => $image['size']
                )
            );
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);

            $path = Mage::getBaseDir('media') . DS . $directory . DS;
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }
            $uploader->save($path, $image['name']);

            return array("status"=>true, "fileName"=> $uploader->getUploadedFileName());

        } catch (Exception $e) {

            return $e->getMessage();
        }
    }

    public function getEmbroideryImagePath()
    {
        $path = Mage::getStoreConfig('embroidery/general/path_to_embroidery_uploads');
        return $path;
    }

    public function getEmbroideryImageUrl()
    {
        return Mage::getBaseUrl('media') . 'embroidery_uploads';
    }

    public function getEmbroideryDisclaimer($hasEmbroidery, $hasLogoEmbroideryWithoutSetup, $isShortVersion = false){

        $message = "";
        $messageShort = "";

        if($hasEmbroidery){
            if($hasLogoEmbroideryWithoutSetup){
                $message = "Please allow an additional 2 weeks for embroidery after custom logo setup is complete.";
                $messageShort = "Please allow an additional 2 weeks for embroidery after custom logo setup is complete.";
            } else {
                $message = "Please allow an additional 2 weeks for embroidery.";
                $messageShort = "Allow an additional 2 weeks.";
            }
        }

        return $isShortVersion ? $messageShort : $message;
    }

    public function getEmbroideryTierPrices($typeId, $stitchesCount){

        $priceCollection = Mage::getModel('embroidery/price')
            ->getCollection()
            ->addFieldToSelect(array('price_id', 'qty_from', 'qty_to', 'price'))
            ->addFieldToFilter('type_id', $typeId)
            ->addFieldToFilter('stitches_count_from', array("lteq"=>$stitchesCount))
            ->addFieldToFilter('stitches_count_to', array("gteq"=>$stitchesCount))
        ;
        $priceCollection->getSelect()->order('price', 'ASC');

        return $priceCollection;

    }

    /**
     * Decode Embroidery Data
     *
     * @param $embroideryData
     * @param bool $returnAssoc
     * @return mixed
     */
    public function decodeEmbroideryData($embroideryData, $returnAssoc = true) {
        return json_decode($embroideryData, $returnAssoc);
    }

    /**
     * Get Embroidery Data Types
     * Uses a reset on decoded embroidery JSON to return the only associative element up one level keyed as 'types'
     *
     * @param $embroideryData
     * @return array
     */
    public function getEmbroideryDataTypes($embroideryData) {
        $embroideryData = $this->decodeEmbroideryData($embroideryData);
        $embroideryData = reset($embroideryData);
        return $embroideryData['types'];
    }

    /**
     * Get Option Type Price Sub Total
     *
     * @param array $locations
     * @return float
     */
    public function getOptionTypePriceSubTotal($locations) {
        $locationTotal = 0.00;
        foreach ($locations as $key => $location) {
            $locationTotal += !isset($location['price']) ?: $location['price'];
        }
        return $locationTotal;
    }

    /**
     * Get Embroidery Options Subtotals
     *
     * @param $embroideryData
     * @return array
     */
    public function getEmbroideryOptionsSubtotals($embroideryData) {
        if (is_null($embroideryData)) {
            return array();
        }

        $embroiderySelectedPriceBreakdown = array();
        $optionTypes = $this->getEmbroideryDataTypes($embroideryData);

        foreach ($optionTypes as $key => $option) {
            $typeName = $option['name'];
            $selectedOption = $this->_search($option, 'price');
            $embroiderySelectedPriceBreakdown[$typeName] = array(
                'label' => self::EMBROIDERY_TYPES[$typeName]['label'],
                'price' => $this->getOptionTypePriceSubTotal($selectedOption)
            );
        }

        return $embroiderySelectedPriceBreakdown;
    }

    /**
     * Remove Selected Option Type
     * Filter out only custom logos from selected locations.
     *
     * @param array $embroideryOptions
     * @param string $optionTypeKey
     * @return array $embroideryOptionTypes
     */
    public function removeSelectedOptionType($embroideryOptions, $optionTypeKey) {
        $productId = key($embroideryOptions);
        $embroideryOptionTypes = $embroideryOptions[$productId]['types'];

        foreach($embroideryOptionTypes as $key => $option) {
            if($option['name'] == $optionTypeKey) {
                $option['locations'] = $this->clearSelectedOptionLocations($option['locations']);
                $embroideryOptionTypes[$key] = $option;
            }
        }

        $embroideryOptions[$productId]['types'] = $embroideryOptionTypes;
        return $embroideryOptions;
    }

    /**
     * Clear Selected Option Locations
     * Pass in a subset array of embroidery option type locations and clear them.
     * Path from embroidery data root of parsed JSON :: [productId] => [types] => [index] => [locations]
     *
     * @param array $selectedOptionLocations
     * @return array $selectedOptionLocations
     */
    public function clearSelectedOptionLocations($selectedOptionLocations) {
        foreach($selectedOptionLocations as $key => $location) {
            $selectedOptionLocations[$key]['selectedOption'] = array();
        }
        return $selectedOptionLocations;
    }

    /**
     * Get Price Without Embroidery
     * Quote items are stored with one total that includes the embroidery.
     * Take the cost of the embroidery and subtract it from Unit Price for breakdown display.
     *
     * @param float $unitPrice
     * @param string $embroideryData
     * @return float $priceWithoutEmbroidery
     */
    public function getPriceWithoutEmbroidery($unitPrice, $embroideryData) {
        $embroideryPrice = $this->getEmbroideryPrice($embroideryData);
        $priceWithoutEmbroidery = $unitPrice - $embroideryPrice;
        return $priceWithoutEmbroidery;
    }

    /**
     * Get Embroidery Setup Skus
     * Speaks for itself.
     *
     * @return array self::EMBROIDERY_SETUP_SKUS
     */
    public function getEmbroiderySetupSkus(){
        return self::EMBROIDERY_SETUP_SKUS;
    }
    public function applyQtyEmbroideryDiscounts($quote, $item, $qty) {

        // Fix embroidery data based upon changed qty.
        // This applies embroidery discounts correctly when updating the qty in the cart.
        $curTierPrices = Mage::getModel('embroidery/embroidery')->getTypesTierPrices();
        $curEmbroideryData = json_decode($item->getEmbroideryData(), true);

        $isPriceUpdate = false;
        if(is_array($curEmbroideryData)) {
            foreach ($curEmbroideryData[$item->getProductId()]['types'] as $typeKey => $typeValue) {
                foreach ($typeValue['locations'] as $locationKey => $locationValue) {
                    if(count($locationValue['selectedOption']) > 0) {
                        foreach ($locationValue['selectedOption'] as $selectedOption) {
                            foreach($curTierPrices as $tpKey => $tpValue) {
                                if($tpKey == $typeKey) {
                                    foreach($tpValue as $tpQty) {
                                        if($tpQty['qty_from'] <= $qty && $qty <= $tpQty['qty_to']) {
                                            if(floatval($curEmbroideryData[$item->getProductId()]['types'][$typeKey]['locations'][$locationKey]['selectedOption'][0]['price']) != floatval($tpQty['price'])) {
                                                if(intval($tpQty['stitches_count_from']) != 0 || intval($tpQty['stitches_count_to']) != 0) {
                                                    if ($tpQty['stitches_count_from'] <= $selectedOption['stitches'] && $selectedOption['stitches'] <= $tpQty['stitches_count_to']) {
                                                        $curEmbroideryData[$item->getProductId()]['types'][$typeKey]['locations'][$locationKey]['selectedOption'][0]['price'] = $tpQty['price'];
                                                        $item->setCustomPrice($tpQty['price']);
                                                        $item->setOriginalCustomPrice($tpQty['price']);
                                                        $item->save();
                                                        $isPriceUpdate = true;
                                                    }
                                                } else {
                                                    $curEmbroideryData[$item->getProductId()]['types'][$typeKey]['locations'][$locationKey]['selectedOption'][0]['price'] = $tpQty['price'];
                                                    $item->setCustomPrice($tpQty['price']);
                                                    $item->setOriginalCustomPrice($tpQty['price']);
                                                    $item->save();
                                                    $isPriceUpdate = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Replace the fixed embroidery if it has changed.
        if($isPriceUpdate) {
            if($curEmbroideryData) {
                $option = $item->getProduct()->getCustomOption('info_buyRequest');
                $optionArray = unserialize($option->getValue());
                $curEmbroideryDataStr = json_encode($curEmbroideryData);
                $curEmbroideryDataStr = str_replace("\\", "", $curEmbroideryDataStr);
                $optionArray["product-embroidery-options"] = json_encode($curEmbroideryData);
                $option->setValue(serialize($optionArray));
                $option->save();
            }
            $quote->setTotalsCollectedFlag(false)->collectTotals()->save();
        }
    }

    public function getArtworkCategoriesData($sortOrder = 'ASC') {
        $categoriesData = [];
        $categories = Mage::getModel('embroidery/artworkcategory')
            ->getCollection()
            ->setOrder('sortorder', $sortOrder);

        foreach($categories as $category) {
            $categoryId = $category->getCategoryId();
            $categoriesData[] = [
                'categoryId' => $categoryId,
                'name' => $category->getName(),
                'sortorder' => $category->getSortorder(),
                'featured' => $category->getFeatured(),
                'featuredText' => $category->getFeaturedText(),
                'enabled' => $category->getEnabled()
            ];
        }
        return $categoriesData;
    }

    public function getArtworkCategoryOptionsData($sortOrder = 'ASC') {
        $optionsData = [];
        $options = Mage::getModel('embroidery/option')
            ->getCollection()
            ->getOptionsByTypeName('artwork', $sortOrder);

        foreach($options as $option) {
            $artworkCategoryId = $option->getArtworkCategoryId();
            $optionsData[$artworkCategoryId][] = [
                'typeOptionId' => $option->getTypeOptionId(),
                'typeId' => $option->getTypeId(),
                'artworkCategoryId' => $artworkCategoryId,
                'name' => $option->getName(),
                'nameOld' => $option->getNameOld(),
                'image' => $option->getImage(),
                'price' => $option->getPrice(),
                'sortorder' => $option->getSortorder(),
                'featured' => $option->getFeatured(),
                'enabled' => $option->getEnabled()
            ];
        }
        return $optionsData;
    }
}
