<?php

/**
 * @author Ripen eCommerce
 * @copyright Copyright (c) 2017 Ripen eCommerce
 * @package Ripen_Embroidery
 */

class Ripen_Embroidery_IndexController extends Mage_Core_Controller_Front_Action
{

    function getEmbroideryDataAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $productId = $this->getRequest()->getParam('id');
            $product =  Mage::getModel('catalog/product')->load($productId);
            $quoteItemId = $this->getRequest()->getParam('quoteItemId');
            $isSavedItem = $this->getRequest()->getParam('isSavedItem');
            $productEmbroidery = "";

            // Get product embroidery


            if ($quoteItemId) { // if embroidery was saved in the cart, pull data from quote_options table

                if ($isSavedItem && $isSavedItem !== "false") {
                    $item = Mage::getModel('shoppinglist/items')->getItemsByGroup($quoteItemId)->getFirstItem();

//                    $collection = Mage::getModel('shoppinglist/group')->getItems()
//                        ->addFieldToFilter('list_id', $this->getId())
//                        ->setOrder('item_id')
//                        ->load();

                    if ($item->getBuyRequest()) {
//                        $embroiderySavedOptionsArray = unserialize($item->getCustomOption('info_buyRequest')->getValue());
                        $embroiderySavedOptionsArray = unserialize($item->getBuyRequest());
                        $embroiderySavedOptions = $embroiderySavedOptionsArray["product-embroidery-options"];
                    }

                } else {
                    $cart = Mage::helper('checkout/cart')->getCart();
                    $item = $cart->getQuote()->getItemById($quoteItemId);

                    if ($item->getProduct()->getCustomOption('info_buyRequest')) {
                        $embroiderySavedOptionsArray = unserialize($item->getProduct()->getCustomOption('info_buyRequest')->getValue());
                        $embroiderySavedOptions = $embroiderySavedOptionsArray["product-embroidery-options"];
                    }
                }
            }

            if ($embroiderySavedOptions) {
                $productEmbroidery = json_decode($embroiderySavedOptions, true);
            };

            if (!$productEmbroidery) { // if embroidery wasn't saved
                $productEmbroidery = $product->getProductEmbroidery();
            }

            // Get embroidery locations
            $locations = Array();
            $locationCollection = Mage::getModel('embroidery/location')
                ->getCollection()
                ->setOrder('sortorder', 'ASC');

            foreach($locationCollection as $location) {
                $locations[] = array(
                    "id"=>$location->getId(),
                    "name"=>$location->getName(),
                    "sortorder"=>$location->getSortorder()
                    );
            }

            // Get embroidery logos
            $logos = Array();

            $session = Mage::getSingleton('core/session');
            //$session->unsetData('embroideryLogos');

            $sessionLogos = $session->getData('embroideryLogos');
            $logos = array_merge($logos, $sessionLogos?:[]);

            if(Mage::getSingleton('customer/session')->isLoggedIn()){
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $customerLogos = $customer->getLogos();
                //$customerLogos = $this->getCustomerLogos($customer->getId());
                $logos = array_merge($logos, $customerLogos?:[]);
            }

            // Get embroidery colors
            $colors = Array();
            $colorsCollection = Mage::getModel('embroidery/color')
                ->getCollection()
                ->setOrder('sortorder', 'ASC');
            foreach($colorsCollection as $color) {
                $colors[] = array(
                    "id"=>$color->getColorId(),
                    "name"=>$color->getName(),
                    "image"=>Mage::getBaseUrl('media').$color->getImage(),
                    "sortorder"=>$color->getSortorder()
                );
            }

            // Get embroidery styles
            $styles = Array();
            $stylesCollection = Mage::getModel('embroidery/style')
                ->getCollection()
                ->setOrder('sortorder', 'ASC');
            foreach($stylesCollection as $style) {
                $styles[] = array(
                    "id"=>$style->getStyleId(),
                    "name"=>$style->getName(),
                    "image"=>Mage::getBaseUrl('media').$style->getImage(),
                    "sortorder"=>$style->getSortorder()
                );
            }

            // Get artwork categories
            $artworkCategories = Array();
            $categoriesCollection = Mage::getModel('embroidery/artworkcategory')
                ->getCollection()
                ->setOrder('sortorder', 'ASC');

            foreach($categoriesCollection as $category) {
                $artworkCategories[] = array(
                    "id"=>$category->getCategoryId(),
                    "name"=>$category->getName(),
                    "sortorder"=>$category->getSortorder()
                );
            }

            // Get embroidery types
            $types = Array();
            $typeSortColumn = ( Mage::app()->getStore()->getId() == 1 ) ? 'sortorder' : 'sortorder_alt' ;
            $typesCollection = Mage::getModel('embroidery/type')
                ->getCollection()
                ->setOrder($typeSortColumn, 'ASC');

            foreach($typesCollection as $type) {

                // Get embroidery options for specific type
                $options = Array();
                $optionsCollection = Mage::getModel('embroidery/option')
                    ->getCollection()
                    ->addFieldToFilter('type_id', $type->getTypeId())
                    ->setOrder('sortorder', 'ASC');

                foreach($optionsCollection as $option){
                    $options[] = array(
                        "id"=>$option->getTypeOptionId(),
                        "artwork_category_id"=>$option->getArtworkCategoryId(),
                        "name"=>$option->getName(),
                        "image"=>Mage::getBaseUrl('media').$option->getImage(),
                        "price"=>$option->getPrice(),
                        "sortorder"=>$option->getSortorder()
                    );
                }

                // Get embroidery tier prices for specific type
                $prices = Array();
                $priceCollection = Mage::getModel('embroidery/price')
                    ->getCollection()
                    ->addFieldToFilter('type_id', $type->getTypeId());

                foreach($priceCollection as $price){
                    $prices[] = array(
                        "price_id"=>$price->getPriceId(),
                        "stitches_count_from"=>$price->getStitchesCountFrom(),
                        "stitches_count_to"=>$price->getStitchesCountTo(),
                        "qty_from"=>$price->getQtyFrom(),
                        "qty_to"=>$price->getQtyTo(),
                        "price"=>$price->getPrice()
                    );
                }

                $types[] = array(
                    "id"=>$type->getTypeId(),
                    "name"=>$type->getName(),
                    "price"=>$type->getPrice(),
                    "size"=>$type->getSize(),
                    "sortorder"=>$type->getSortorder(),
                    "popularity"=>$type->getPopularity(),
                    "icon_image"=>Mage::getBaseUrl('media').$type->getIconImage(),
                    "icon_title"=>$type->getIconTitle(),
                    "type_image"=>Mage::getBaseUrl('media').$type->getTypeImage(),
                    "options"=>$options,
                    "tier_prices"=>$prices
                );
            }

            $cartEmbroideryQty = Mage::getModel('embroidery/embroidery')->getCartEmbroideryQty($containerId);
            $response = array(
                'productEmbroidery'=>$productEmbroidery,
                'locations'=>$locations,
                'artwork_categories'=>$artworkCategories,
                'types'=>$types,
                'logos'=>$logos,
                'colors'=>$colors,
                'styles'=>$styles,
                'cart_embroidery_qty' => $cartEmbroideryQty,
                'is_signed_in' => Mage::getSingleton('customer/session')->isLoggedIn(),
                'store_id' => Mage::app()->getStore()->getId()
            );

            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        }
    }

    public function getCustomerLogos($customerId){

        $logos = array();
        $logosCollection = Mage::getModel('embroidery/logo')
            ->getCollection()
            ->setOrder('created_at', 'DESC')
            ->addFieldToFilter('customer_id', array("eq"=>$customerId));

        $i = 0;
        foreach($logosCollection as $logo) {

            $minPrice = $maxPrice = 0;
            if($logo->getStitchesCount() > 0){
                // hard code typeId for logos: 5
                $logoPrices = Mage::helper('embroidery')->getEmbroideryTierPrices(5, $logo->getStitchesCount());
                $minPrice = $logoPrices->getFirstItem()->getPrice();
                $maxPrice = $logoPrices->getlastItem()->getPrice();

                $logoTierPrices = array();
                foreach($logoPrices as $logoPrice){
                    $logoTierPrices[] = array(
                        "price_id"=>$logoPrice->getPriceId(),
                        "qty_from"=>$logoPrice->getQtyFrom(),
                        "qty_to"=>$logoPrice->getQtyTo(),
                        "price"=>$logoPrice->getPrice()
                    );
                }
            }

            $i++;
            $logos[] = array(
                "id"=>$logo->getLogoId(),
                "name"=>$logo->getName(),
                "image"=>Mage::getBaseUrl('media').$logo->getLogo(),
                "digitized_image"=>Mage::getBaseUrl('media').$logo->getDigitizedLogo(),
                "sortorder"=> $i * 10,
                "stitches"=>$logo->getStitchesCount(),
                "status"=>$logo->getStatus(),
                "min_price"=>$minPrice,
                "max_price"=>$maxPrice,
                "tier_prices"=>$logoTierPrices
            );
        }

        return $logos;
    }

    public function getQtyForDiscountAction(){

        $response = array("qtyInCart"=>5);
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));
    }

    public function removeImageAction(){

        $filename = $this->getRequest()->getParam('filename');
        $filename = str_replace(Mage::helper('embroidery')->getEmbroideryImageUrl(),"",$filename);
        $filename = substr($filename, 0, strpos($filename, strrchr($filename, ".")));

        if($filename){
            $mask = Mage::helper('embroidery')->getEmbroideryImagePath() . $filename.".*";
            array_map('unlink', glob($mask));
        }

    }

    public function uploadImageAction(){

        $file = $_FILES['file'];
        $extension = pathinfo($file["name"], PATHINFO_EXTENSION);

        try {

            $uploader = new Varien_File_Uploader(
                array(
                    'name' => $file['name'],
                    'type' => $file['type'],
                    'tmp_name' => $file['tmp_name'],
                    'error' => $file['error'],
                    'size' => $file['size']
                )
            );
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'eps', 'pdf', 'ai'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);

            $baseFilename = md5($file['name'] . time());
            $originalFileName = $previewFileName = $baseFilename . "." . $extension;
            $date = date('Y-m-d');
            $shortPath = $date . DS;
            $saveToPath = Mage::helper('embroidery')->getEmbroideryImagePath() . DS . $shortPath;

            if (!is_dir($saveToPath)) {
                mkdir($saveToPath, 0775, true);
            }
            $uploader->save($saveToPath, $originalFileName);


            if (in_array($extension, ['pdf', 'eps', 'ai'])) {
                $original_file = $saveToPath . $originalFileName;
                $previewFileName = $baseFilename . ".jpg";
                $preview_file = $saveToPath . $previewFileName;
                exec("convert -density 300 -trim " . $original_file . " -quality 100 " . $preview_file);
            }

            $response = array("status" => true, "previewFileName" => Mage::helper('embroidery')->getEmbroideryImageUrl() . DS . $shortPath . $previewFileName, "originalFileName" => Mage::helper('embroidery')->getEmbroideryImageUrl() . DS . $shortPath . $originalFileName);

            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $customer->setLastLogoTimestamp(Mage::getSingleton('core/date')->gmtDate())->save();
            } else {
                Mage::getSingleton('core/session')->setRecordLogoUploadOnSignIn(true);
            }

        } catch (Exception $e) {

            $response = array("status" => false, "message" => $e->getMessage());

        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));

    }

    public function resetPasswordAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {
            $email = $this->getRequest()->getParam('email');

            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {

                try {
                    $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                    $response = array("status"=>true);

                } catch (Exception $e) {
                    $response = array("status"=>false, "message"=>$e->getMessage());
                }

            } else {
                $response = array("status"=>false);
            }



            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));

        }

    }

    public function createAccountAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {
            $email = $this->getRequest()->getParam('email');
            $password = $this->getRequest()->getParam('password');
            $firstName = $this->getRequest()->getParam('firstName');
            $lastName = $this->getRequest()->getParam('lastName');

            if(!$firstName || !$lastName || !$password || !$email){
                $response = array("status"=>false, "message" => "All fields required.");
            } else {
                $customer = Mage::getModel("customer/customer");
                $customer->setWebsiteId(Mage::app()->getWebsite()->getId())
                    ->setStore(Mage::app()->getStore())
                    ->setEmail($email)
                    ->setPassword($password)
                    ->setFirstname($firstName)
                    ->setLastname($lastName);

                try {
                    $customer->save();

                    if (Mage::getSingleton('core/session')->getRecordLogoUploadOnSignIn() === true){
                        $customer->setLastLogoTimestamp(Mage::getSingleton('core/date')->gmtDate())->save();
                        Mage::getSingleton('core/session')->unsRecordLogoUploadOnSignIn();
                    }

                    $session = Mage::getSingleton('customer/session');
                    $session->login($email,$password);

                    $customerContent = Mage::helper('customercontent');
                    $myAccountHtml = $customerContent->getMyAccountHtml($customer);
                    $btnMobileDisplayDrawerHtml = $customerContent->getBtnMobileDisplayDrawerHtml();

                    $response = array(
                        "status"=> true,
                        "message" => "Success",
                        "logos"=> [],
                        "myAccountHtml" => $myAccountHtml,
                        "btnMobileDisplayDrawerHtml" => $btnMobileDisplayDrawerHtml
                    );
                } catch (Exception $e){
                    $response = array("status"=>false, "message" => $e->getMessage());
                }
            }

        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));


    }

    public function signInAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {
            $email = $this->getRequest()->getParam('email');
            $password = $this->getRequest()->getParam('password');

            try {

                $session = Mage::getSingleton('customer/session');
                $session->login($email, $password);
                $customer = $session->getCustomer();

                $customerLogos = $customer->getLogos();

                if (Mage::getSingleton('core/session')->getRecordLogoUploadOnSignIn() === true){
                    $customer->setLastLogoTimestamp(Mage::getSingleton('core/date')->gmtDate())->save();
                    Mage::getSingleton('core/session')->unsRecordLogoUploadOnSignIn();
                }

                $customerContent = Mage::helper('customercontent');
                $myAccountHtml = $customerContent->getMyAccountHtml($customer);
                $btnMobileDisplayDrawerHtml = $customerContent->getBtnMobileDisplayDrawerHtml();

                $response = array(
                    "status"=> true,
                    "message" => "Success",
                    "logos"=> $customerLogos,
                    "myAccountHtml" => $myAccountHtml,
                    "btnMobileDisplayDrawerHtml" => $btnMobileDisplayDrawerHtml
                );

            } catch (Exception $e){
                $response = array("status"=>false, "message" => $e->getMessage());
            }
        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));

    }

    public function addLogoToSessionAction(){

        $session = Mage::getSingleton('core/session');
        $sessionLogos = $session->getData('embroideryLogos');

        $newLogo = array(
            "id"=>0,
            "name"=>$this->getRequest()->getParam('name'),
            "image"=>$this->getRequest()->getParam('filename'),
            "sortorder"=>0,
            "stitches"=>0,
            "status"=>0,
            "min_price"=>0,
            "max_price"=>0
        );

        if(!is_array($sessionLogos))
            $sessionLogos = array();

        array_unshift($sessionLogos, $newLogo);
        foreach($sessionLogos as $index=>$logo){
            $sessionLogos[$index]["sortorder"] = $index + 1;
        }

        $session->setData('embroideryLogos', $sessionLogos);

    }

    public function testGarbageCollectorAction()
    {
        Mage::getModel('embroidery/observer')->collectGarbageLogos();
    }


    public function logosAction()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()):
            $this->_redirect('customer/account/login');
            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }

}
