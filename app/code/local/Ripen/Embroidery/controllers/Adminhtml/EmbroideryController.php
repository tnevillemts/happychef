<?php

class Ripen_Embroidery_Adminhtml_EmbroideryController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/embroidery');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();

    }

    public function editAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getParams();
        $templateId = $postData['template_id'];

        $typesCollection = Mage::getModel('embroidery/type')
            ->getCollection();
        foreach ($typesCollection as $type) {

            $imageKey = "templateTypeImage_".$type->getTypeId();

            $file = $_FILES[$imageKey];
            if ($file['size']) {

                $directory = 'embroidery_templates';

                $fileResult = Mage::helper('embroidery')->uploadEmbroideryImage($file, $directory);

                if (!is_array($fileResult)) {
                    Mage::getSingleton('adminhtml/session')->addNotice($fileResult);
                } else {

                    //$filename = $_FILES[$imageKey]['name'];
                    $filename = $fileResult["fileName"];

                    $templateImage = Mage::getModel('embroidery/templateimage')
                        ->getCollection()
                        ->addFieldToFilter('template_id', $templateId)
                        ->addFieldToFilter('type_id', $type->getTypeId());

                    if (!$templateImage->getSize()) {

                        $templateImage = Mage::getModel('embroidery/templateimage');
                        $data = array('template_id' => $templateId, 'type_id' => $type->getTypeId(), 'image' => $directory . "/" . $filename);
                        $templateImage->setData($data)->save();

                    } else {
                        $image = $templateImage->getFirstItem();
                        $image->setImage($directory . "/" . $filename)->save();
                    }
                }

            }
        }

        $templateDataCollection = Mage::getModel('embroidery/templatedata')
            ->getCollection()
            ->addFieldToFilter('template_id', $templateId);

        foreach ($templateDataCollection as $item) {
            $item->delete();
        }

        foreach ($postData as $param => $value) {

            if (strpos($param, "embroidery_option") !== false && $value == 1) {

                $ids = explode("-", $param);

                $locationId = $ids[1];
                $typeId = $ids[2];

                $data = array('template_id' => $templateId, 'location_id' => $locationId, 'type_id' => $typeId);
                if ($typeId == 4) {
                    $data["limit"] = 3;
                }

                $newRecord = Mage::getModel('embroidery/templatedata')->setData($data);
                try {
                    $newRecord->save();
                } catch (Exception $e) {
                    echo $e->getMessage();
                }

            }
        }

        $message = $this->__('Saved Successfully');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);

        $this->_redirect("*/*/edit/id/$templateId");
    }

    public function mapEmbroideryAction()
    {
        // get order items
        $orderItems = Mage::getModel('sales/order_item')
            ->getCollection()
            ->addFieldToFilter('product_options', array("nlike" => '%"template_info";a:0:{}%'))
            ->addFieldToFilter('product_options', array("nlike" => '%product-embroidery-options%'))
            ->addFieldToFilter('product_type', 'configurable')
            //->addFieldToFilter('product_id', 66)
            //->addFieldToFilter('order_id', 467991)
            ->setOrder('item_id', 'ASC')
        ;

        $orderItems->getSelect()->limit(1000); // limit, offset

        echo $orderItems->getSelect()."<br><br>";

        try {

            foreach ($orderItems as $orderItem) {
                $productId = $orderItem->getProductId();
                $product =  Mage::getModel('catalog/product')->load($productId);
                $options = $orderItem->getProductOptions();

                $embBuyRequest = $options["info_buyRequest"];
                $embroideryLocations = $embBuyRequest["template_info"];
                $embOptions = $options["options"];

                $emArr = array();

                foreach ((array)$embOptions as $embOption) {
                    if ($embOption) {

                        if (strpos($embOption["label"], "Color") !== FALSE) {
                            $emArr["EMBTEXT"][$embroideryLocations[$embOption["option_id"]]]["color"][] = $embOption["value"];
                        }

                        if (strpos($embOption["label"], "Style") !== FALSE) {
                            $emArr["EMBTEXT"][$embroideryLocations[$embOption["option_id"]]]["style"][] = $embOption["value"];
                        }

                        if (strpos($embOption["label"], "Text Line") !== FALSE) {
                            $emArr["EMBTEXT"][$embroideryLocations[$embOption["option_id"]]]["text"][] = $embOption["value"];
                            $emArr["EMBTEXT"][$embroideryLocations[$embOption["option_id"]]]["price"][] =
                                substr($embOption["label"], strpos($embOption["label"], "$") + 1);
                        }

                        if (strpos($embOption["label"], "Ribbon") !== FALSE) {
                            $emArr["RIBBON"][$embroideryLocations[$embOption["option_id"]]]["ribbon"] =
                                substr($embOption["value"], 0, strpos($embOption["value"], " - $"));
                            $emArr["RIBBON"][$embroideryLocations[$embOption["option_id"]]]["price"] =
                                substr($embOption["value"], strpos($embOption["value"], "$") + 1);
                        }

                        if (strpos($embOption["label"], "Flag") !== FALSE) {
                            $emArr["FLAG"][$embroideryLocations[$embOption["option_id"]]]["flag"] =
                                substr($embOption["value"], 0, strpos($embOption["value"], " - $"));
                            $emArr["FLAG"][$embroideryLocations[$embOption["option_id"]]]["price"] =
                                substr($embOption["value"], strpos($embOption["value"], "$") + 1);
                        }
                    }
                }

                $productEmbroidery = $product->getProductEmbroidery();


                foreach ($emArr as $embType => $embLocations) {
                    foreach ($embLocations as $embLocation => $embLabels) {

                        $location = $this->getLocationByName($embLocation);
                        $type = $this->getTypeByName($embType);

                        $limit = $type->getName() == 'text' ? 3 : null;

                        //throw new Exception("Can't find matching type [{$embType}]");

                        if ($location && $type){

                            $productEmbroidery[$productId]["types"][$type->getTypeId()]["name"] = $type->getName();
                            $productEmbroidery[$productId]["types"][$type->getTypeId()]["locations"][$location->getLocationId()]["limit"] = $limit;
                            $productEmbroidery[$productId]["types"][$type->getTypeId()]["locations"][$location->getLocationId()]["name"] = $location->getName();
                            $productEmbroidery[$productId]["types"][$type->getTypeId()]["locations"][$location->getLocationId()]["selectedOption"] = [];
                            $productEmbroidery[$productId]["types"][$type->getTypeId()]["instructions"] = "";

                            if ($embType == "EMBTEXT") {

                                for($i = 0; $i<=2; $i++){
                                    if($embLabels["text"][$i]){

                                        $textLine = end(explode(" x ", $embLabels["text"][$i]));
                                        $textColor = end(explode(" x ", $embLabels["color"][$i]));
                                        $textStyle = end(explode(" x ", $embLabels["style"][$i]));
                                        $textPrice = $embLabels["price"][$i]/$orderItem->getQtyOrdered();

                                        $color = $this->getColorByName($textColor);
                                        $style = $this->getStyleByName($textStyle);

                                        $textData = array("textLine" => $textLine,
                                            "textColor" => $color ? $color->getColorId() : 0,
                                            "textStyle" => $style ? $style->getStyleId() : 0,
                                            "sortorder" => ($i+1),
                                            "price" => $textPrice,
                                            "textColorValue" => $color ? $color->getName() : $textColor,
                                            "textColorImage" => $color ? $color->getImage() : '',
                                            "textStyleValue" => $style ? $style->getName() : $textStyle,
                                            "textStyleImage" => $style ? $style->getImage() : '');

                                        array_push($productEmbroidery[$productId]
                                        ["types"]
                                        [$type->getTypeId()]
                                        ["locations"]
                                        [$location->getLocationId()]
                                        ["selectedOption"], $textData);
                                    }
                                }
                            }

                            if($embType == "FLAG") {

                                $flag = str_replace(" FLAG", "", end(explode(" x ", $embLabels["flag"])));
                                $flagPrice = $embLabels["price"]/$orderItem->getQtyOrdered();
                                $flagObject = $this->getTypeOptionByName($flag, $type->getTypeId());

                                $flagData = array(
                                    "id" => $flagObject ? $flagObject->getId() : 0,
                                    "name" => $flagObject ? $flagObject->getName() : $flag,
                                    "price" => $flagPrice,
                                    "image" => $flagObject ? Mage::getBaseUrl('media').$flagObject->getImage() : ''
                                );

                                array_push($productEmbroidery[$productId]
                                ["types"]
                                [$type->getTypeId()]
                                ["locations"]
                                [$location->getLocationId()]
                                ["selectedOption"], $flagData);
                            }

                            if($embType == "RIBBON") {

                                $ribbon = str_replace(" RIBBON", "", end(explode(" x ", $embLabels["ribbon"])));
                                $ribbonPrice = $embLabels["price"]/$orderItem->getQtyOrdered();

                                $ribbonObject = $this->getTypeOptionByName($ribbon, $type->getTypeId());

                                $ribbonData = array(
                                    "id" => $ribbonObject ? $ribbonObject->getId() : 0,
                                    "name" => $ribbonObject ? $ribbonObject->getName() : $ribbon,
                                    "price" => $ribbonPrice,
                                    "image" => $ribbonObject ? Mage::getBaseUrl('media').$ribbonObject->getImage() : ''
                                );

                                array_push($productEmbroidery[$productId]
                                ["types"]
                                [$type->getTypeId()]
                                ["locations"][$location->getLocationId()]
                                ["selectedOption"], $ribbonData);
                            }
                        } else {
                            echo "<p style='color: red; font-weight: bold'>Location [{$embLocation}] or type [{$embType}] is not found for order_item [{$orderItem->getId()}].</p>";
                        }
                    }
                }
                /*
                                $infoBuyRequest = $orderItem->getOptionByCode('info_buyRequest');
                                if (is_object($infoBuyRequest)) {
                                    $buyRequestArr = unserialize($infoBuyRequest->getValue());
                                    $buyRequestArr["product-embroidery-options"] = json_encode($productEmbroidery);
                                }
                */

                $options = $orderItem->getProductOptions();
                $options["info_buyRequest"]['product-embroidery-options'] = json_encode($productEmbroidery);
                unset($options['product-embroidery-options']);
                $orderItem->setProductOptions($options)->save();

                echo "OrderItemId: {$orderItem->getId()} == OrderId: {$orderItem->getOrderId()}==";
                echo $orderItem->getProductOptions();
                echo "<br/><br/>";
                echo json_encode($productEmbroidery);
                echo "<br>--------------<br>";


                /*
                $childItem = Mage::getModel("sales/order_item")
                    ->getCollection()
                    ->addFieldToFilter('parent_item_id', $orderItem->getId())
                    ->getFirstItem();

                if( $childItem ) {
                    $childOptions = $childItem->getProductOptions();
                    //$childOptions['product-embroidery-options'] = json_encode($productEmbroidery);
                    unset($childOptions['product-embroidery-options']);
                    $childItem->setProductOptions($childOptions)->save();
                }
                */
            }

        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }

    }

    protected function getLocationByName($locationName)
    {
        $locations = Mage::getModel('embroidery/location')
            ->getCollection()
            //->addFieldToFilter('name_old', $locationName)
            ->addFieldToFilter(array('name', 'name_old'), array(
                array('name','eq'=>$locationName),
                array('name_old', 'eq'=>$locationName) ));
        ;

        return $locations->getSize() ? $locations->getFirstItem() : false;
    }

    protected function getTypeByName($typeName)
    {

        $types = Mage::getModel('embroidery/type')
            ->getCollection()
            ->addFieldToFilter(array('name', 'name_old'), array(
                array('name','eq'=>$typeName),
                array('name_old', 'eq'=>$typeName) ));

        return $types->getSize() ? $types->getFirstItem() : false;

    }

    protected function getColorByName($name){
        $colors = Mage::getModel('embroidery/color')
            ->getCollection()
            ->addFieldToFilter(array('name', 'name_old'), array(
                array('name','eq'=>$name),
                array('name_old', 'eq'=>$name) ));

        return $colors->getSize() ? $colors->getFirstItem() : false;

    }

    protected function getStyleByName($name){
        $styles = Mage::getModel('embroidery/style')
            ->getCollection()
            ->addFieldToFilter(array('name', 'name_old'), array(
                array('name','eq'=>$name),
                array('name_old', 'eq'=>$name) ));

        return $styles->getSize() ? $styles->getFirstItem() : false;

    }

    protected function getTypeOptionByName($name, $typeId){
        $options = Mage::getModel('embroidery/option')
            ->getCollection()
            ->addFieldToFilter(array('name', 'name_old'), array(
                array('name','eq'=>$name),
                array('name_old', 'eq'=>$name) ))
            ->addFieldToFilter('type_id', $typeId);

        return $options->getSize() ? $options->getFirstItem() : false;

    }


    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/embroidery');
    }

}
