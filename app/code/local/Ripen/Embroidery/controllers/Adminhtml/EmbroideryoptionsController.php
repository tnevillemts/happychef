<?php

class Ripen_Embroidery_Adminhtml_EmbroideryoptionsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/embroideryoptions');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();

    }

    public function editAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function saveTextOptionsAction()
    {
        $postData = $this->getRequest()->getParams();
        $optionType = $postData["type"];

        $collection = Mage::getModel('embroidery/'.$optionType)
            ->getCollection()
            ->setOrder('sortorder', 'ASC');

        foreach($collection as $option) {

            $nameKey = "option_{$option->getId()}";
            $imageKey = "optionimage_{$option->getId()}";
            $sortKey = "optionsort_{$option->getId()}";

            if($postData[$nameKey])
                $option->setName($postData[$nameKey]);

            if($postData[$sortKey])
                $option->setSortorder($postData[$sortKey]);


            $file = $_FILES[$imageKey];
            if($file['size']) {

                $fileResult = Mage::helper('embroidery')->uploadEmbroideryImage($file);
                if (!is_array($fileResult)) {
                    Mage::getSingleton('adminhtml/session')->addNotice($fileResult);
                } else {
                    $option->setImage("embroidery_options/".$fileResult["fileName"]);
                }

            }

            $option->save();

        }

        if ($postData["option_new"] && $_FILES["optionimage_new"]['size']) {

            $newOption = Mage::getModel('embroidery/'.$optionType);
            $newOption->setName($postData['option_new']);
            $newOption->setSortorder($postData['optionsort_new']);

            $file = $_FILES['optionimage_new'];
            if($file['size']) {

                $fileResult = Mage::helper('embroidery')->uploadEmbroideryImage($file);
                if (!is_array($fileResult)) {
                    Mage::getSingleton('adminhtml/session')->addNotice($fileResult);
                } else {
                    $newOption->setImage("embroidery_options/".$fileResult["fileName"]);
                    $newOption->save();
                }
            }
        }

        $message = $this->__('Saved Successfully');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);
        $this->_redirect("*/*/index");
    }

    public function saveOptionsAction()
    {
        $postData = $this->getRequest()->getParams();
        $optionTypeId = $postData["type"];

        $collection = Mage::getModel('embroidery/option')
            ->getCollection()
            ->addFieldToFilter('type_id', $optionTypeId)
            ->setOrder('sortorder', 'ASC');

        foreach($collection as $option) {

            $nameKey = "option_{$option->getId()}";
            $imageKey = "optionimage_{$option->getId()}";
            $sortKey = "optionsort_{$option->getId()}";

            if($postData[$nameKey])
                $option->setName($postData[$nameKey]);

            if($postData[$sortKey])
                $option->setSortorder($postData[$sortKey]);


            $file = $_FILES[$imageKey];
            if($file['size']) {
                $fileResult = Mage::helper('embroidery')->uploadEmbroideryImage($file);
                if (!is_array($fileResult)) {
                    Mage::getSingleton('adminhtml/session')->addNotice($fileResult);
                } else {
                    $option->setImage("embroidery_options/".$fileResult["fileName"]);
                }
            }

            $option->save();

        }

        if ($postData["option_new"] && $_FILES["optionimage_new"]['size']) {

            $newOption = Mage::getModel('embroidery/option');
            $newOption->setName($postData['option_new']);
            $newOption->setSortorder($postData['optionsort_new']);
            $newOption->setTypeId($optionTypeId);

            $file = $_FILES['optionimage_new'];
            if($file['size']) {

                $fileResult = Mage::helper('embroidery')->uploadEmbroideryImage($file);
                if (!is_array($fileResult)) {
                    Mage::getSingleton('adminhtml/session')->addNotice($fileResult);
                } else {
                    $newOption->setImage("embroidery_options/".$fileResult["fileName"]);
                    $newOption->save();
                }
            }
        }

        $message = $this->__('Saved Successfully');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);
        $this->_redirect("*/*/index");
    }

    public function removeTextOptionAction(){

        $postData = $this->getRequest()->getParams();
        $id = $postData["id"];
        $optionType = $postData["type"];

        $optionModel = Mage::getModel('embroidery/'.$optionType)->load($id);
        $optionModel->delete()->save();

        $message = $this->__('Successfully Removed');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);
        $this->_redirect("*/*/index");
    }

    public function removeOptionAction(){

        $postData = $this->getRequest()->getParams();
        $id = $postData["id"];

        $optionModel = Mage::getModel('embroidery/option')->load($id);
        $optionModel->delete()->save();

        $message = $this->__('Successfully Removed');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);
        $this->_redirect("*/*/index");
    }


    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/embroidery_options');
    }

}
