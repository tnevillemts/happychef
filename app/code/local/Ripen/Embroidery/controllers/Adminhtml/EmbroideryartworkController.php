<?php
use League\Flysystem\Adapter\Local as LocalAdapter;
use League\Flysystem\Filesystem as Filesystem;

class Ripen_Embroidery_Adminhtml_EmbroideryartworkController extends Mage_Adminhtml_Controller_Action
{
    const ARTWORK_ASSETS_PATH = 'embroidery_artwork/';


    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/embroidery');
        return $this;
    }

    public function managerAction()
    {
        $this->_initAction()
            ->renderLayout();

    }

    public function saveCategoriesOrderAction()
    {
        $artworkCategoryModel = Mage::getSingleton('embroidery/artworkcategory');
        $categoriesUpdateData = json_decode($this->getRequest()->getParam('categories_update_data'), true);

        foreach ($categoriesUpdateData as $key => $category) {
            $categorySaveData = [
                'category_id' => $category['categoryId'],
                'name' => $category['name'],
                'featured' => $category['featured'],
                'featured_text' => $category['featuredText'],
                'enabled' => $category['enabled'],
                'sortorder' => $category['sortorder']
            ];
            $artworkCategoryModel->setData($categorySaveData);
            $artworkCategoryModel->save();
        }

        $response = ['categories' => Mage::helper('embroidery')->getArtworkCategoriesData()];
        $this->jsonResponse($response);
    }


    public function saveArtworkOptionsOrderAction()
    {
        $helper = Mage::helper('embroidery');
        $artworkTypeOptionModel = Mage::getModel('embroidery/option');
        $artworkOptionsUpdateData = json_decode($this->getRequest()->getParam('artwork_options_update_data'), true);

        foreach ($artworkOptionsUpdateData as $key => $artworkOption) {
            $typeOptionId = $artworkOption['typeOptionId'];
            $nameOld = $artworkOption['nameOld'];
            $artworkData = [
                'type_option_id' => $typeOptionId,
                'artwork_category_id' => $artworkOption['artworkCategoryId'],
                'name' => $artworkOption['name'],
                'name_old' => $nameOld,
                'sortorder' => $artworkOption['sortorder'],
                'featured' => $artworkOption['featured'],
                'enabled' => $artworkOption['enabled']
            ];
            if($imageValue = $this->checkImageToRename($typeOptionId, $nameOld)) {
                $artworkData['image'] = $imageValue;
            }
            $artworkTypeOptionModel->setData($artworkData);
            $artworkTypeOptionModel->save();
        }

        $response = [
            'categories' => $helper->getArtworkCategoriesData(),
            'artworkOptions' => $helper->getArtworkCategoryOptionsData()
        ];

        $this->jsonResponse($response);
    }

    public function addCategoryAction()
    {
        try {
            $helper = Mage::helper('embroidery');
            $categoryModel = Mage::getModel('embroidery/artworkcategory');
            $addCategoryValues = $this->getRequest()->getParams();
            $categoryModel->setData($addCategoryValues);
            $categoryModel->save();

            $this->jsonResponse([
                'alertMessage' => 'ADDED CATEGORY - ' . $addCategoryValues['name'],
                'categories' => $helper->getArtworkCategoriesData(),
                'artworkOptions' => $helper->getArtworkCategoryOptionsData()
            ]);
        } catch (Exception $e) {
            $this->jsonErrorResponse($e);
        }
    }

    public function deleteCategoryAction()
    {
        try {
            $helper = Mage::helper('embroidery');
            $categoryModel = Mage::getModel('embroidery/artworkcategory');
            $categoryId = $this->getRequest()->getParam('category_id');

            $deleteCategoryData = $categoryModel->load($categoryId)->getData();
            $categoryModel->delete();
            $this->jsonResponse([
                'alertMessage' => 'DELETED CATEGORY - ' . $deleteCategoryData['name'],
                'categories' => $helper->getArtworkCategoriesData(),
                'artworkOptions' => $helper->getArtworkCategoryOptionsData()
            ]);
        } catch (Exception $e) {
            $this->jsonErrorResponse($e);
        }
    }

    protected function getCategoryImagesIndex($categoryId)
    {
        $categoryImagesCollection = Mage::getModel('embroidery/option')
            ->getCollection()
            ->addFieldToFilter('artwork_category_id', $categoryId);
        $categoryImagesCollection->getSelect()->columns(['type_option_id', 'image']);
        $categoryImagesIndex = [];
        foreach ($categoryImagesCollection as $key => $categoryImage) {
            $typeOptionId = $categoryImage['type_option_id'];
            $categoryImagesIndex[$typeOptionId] = $categoryImage['image'];
        }

        return $categoryImagesIndex;
    }

    protected function checkImageToRename($typeOptionId, $nameOld) {
        $imageCopyValue = null;
        $currentCategoryId = $this->getRequest()->getParam('current_category_id');
        $imagesIndex = $this->getCategoryImagesIndex($currentCategoryId);
        $existingImageValue = $imagesIndex[$typeOptionId];
        $imageFileName = strtolower(pathinfo($existingImageValue , PATHINFO_FILENAME));
        $imageExtension = strtolower(pathinfo($existingImageValue, PATHINFO_EXTENSION));
        $nameOldCompare = strtolower($nameOld);
        if($nameOldCompare !== $imageFileName) {
            $localAdapter = new LocalAdapter($this->getArtworkMediaPath());
            $fileSystem = new Filesystem($localAdapter);
            $newImageValue = $nameOldCompare . '.' . $imageExtension;
            $imageCopyValue = self::ARTWORK_ASSETS_PATH . $newImageValue;
            if($fileSystem->has($imageCopyValue)) {
                $fileSystem->delete($imageCopyValue);
            }
            $fileSystem->copy($existingImageValue, $imageCopyValue);
            $fileSystem->delete($existingImageValue);
        }
        return $imageCopyValue;
    }

    public function addArtworkAction()
    {
        try {
            $helper = Mage::helper('embroidery');
            $artworkTypeOptionModel = Mage::getModel('embroidery/option');
            $artworkOptionsAddData = $this->getRequest()->getParams();
            $artworkAssetsPath = $this->getArtworkAssetsPath();

            $imageExtension = pathinfo($_FILES['artwork_upload_image_file']['name'], PATHINFO_EXTENSION);
            $imageName = strtolower($artworkOptionsAddData['name_old']) . '.' . $imageExtension;
            $recordImageValue = self::ARTWORK_ASSETS_PATH . $imageName;
            $localAdapter = new LocalAdapter($artworkAssetsPath);
            $fileSystem = new Filesystem($localAdapter);

            $stream = fopen($_FILES['artwork_upload_image_file']['tmp_name'], 'r+');
            $fileSystem->writeStream($imageName, $stream);
            $typeId = $artworkTypeOptionModel->getCollection()->getTypeIdByName('artwork');
            fclose($stream);

            $addArtworkData = [
                'type_id' => $typeId,
                'artwork_category_id' => $artworkOptionsAddData['artwork_category_id'],
                'name' => $artworkOptionsAddData['name'],
                'name_old' => $artworkOptionsAddData['name_old'],
                'image' => $recordImageValue,
                'featured' => $artworkOptionsAddData['featured'],
                'enabled' => $artworkOptionsAddData['enabled']
            ];

            $artworkTypeOptionModel->setData($addArtworkData);
            $artworkTypeOptionModel->save();

            $this->jsonResponse([
                'artworkAddedName' => $artworkOptionsAddData['name'],
                'artworkAddedNameOld' => $artworkOptionsAddData['name_old'],
                'addedToCategoryId' => $artworkOptionsAddData['artwork_category_id'],
                'categories' => $helper->getArtworkCategoriesData(),
                'artworkOptions' => $helper->getArtworkCategoryOptionsData()
            ]);

        } catch (Exception $e) {
            $this->jsonErrorResponse($e);
        }

    }

    public function updateArtworkImageAction()
    {
        try {
            $helper = Mage::helper('embroidery');
            $artworkModel = Mage::getModel('embroidery/option');
            $typeOptionId = $this->getRequest()->getParam('type_option_id');
            $artworkUpdateImageData = $artworkModel->load($typeOptionId)->getData();
            $artworkImageToUpdate = $artworkUpdateImageData['image'];
            $filesystemMediaDir = $this->getFileSystemMediaDir();
            if($filesystemMediaDir->has($artworkImageToUpdate)) {
                $filesystemMediaDir->delete($artworkImageToUpdate);
            }
            $stream = fopen($_FILES['artwork_upload_image_file']['tmp_name'], 'r+');
            $filesystemMediaDir->writeStream($artworkImageToUpdate, $stream);
            fclose($stream);

            $this->jsonResponse([
                'alertMessage' => 'UPDATE ARTWORK IMAGE - ' . $artworkUpdateImageData['name'] . ' with Sku: ' .$artworkUpdateImageData['name_old'],
                'updatedTypeOptionId' => $artworkUpdateImageData['type_option_id'],
                'cacheBustImageName' => $artworkUpdateImageData['image'] . '?cache-' . time(),
                'categories' => $helper->getArtworkCategoriesData(),
                'artworkOptions' => $helper->getArtworkCategoryOptionsData()
            ]);

        } catch (Exception $e) {
            $this->jsonErrorResponse($e);
        }
    }

    public function deleteArtworkAction()
    {
        try {
            $helper = Mage::helper('embroidery');
            $artworkModel = Mage::getModel('embroidery/option');
            $typeOptionId = $this->getRequest()->getParam('type_option_id');
            $artworkDataToDelete = $artworkModel->load($typeOptionId)->getData();
            $artworkImageToDelete = $artworkDataToDelete['image'];
            $fileSystemMediaDir = $this->getFileSystemMediaDir();

            if($fileSystemMediaDir->has($artworkImageToDelete)) {
                $fileSystemMediaDir->delete($artworkImageToDelete);
            }

            $artworkModel->setData($artworkDataToDelete);
            $artworkModel->delete();

            $this->jsonResponse([
                'alertMessage' => 'DELETED ARTWORK - ' .$artworkDataToDelete['name'] . ' with Sku: ' .$artworkDataToDelete['name_old'],
                'categories' => $helper->getArtworkCategoriesData(),
                'artworkOptions' => $helper->getArtworkCategoryOptionsData()
            ]);

        } catch (Exception $e) {
            $this->jsonErrorResponse($e);
        }

    }

    protected function getFileSystemMediaDir() {
        $localAdapter = new LocalAdapter($this->getArtworkMediaPath());
        $fileSystem = new Filesystem($localAdapter);
        return $fileSystem;
    }

    protected function getArtworkAssetsPath()
    {
        return $this->getArtworkMediaPath() . self::ARTWORK_ASSETS_PATH;
    }

    protected function getArtworkMediaPath()
    {
        return Mage::getBaseDir() . '/media/';
    }

    /**
     * JSON Response
     * A slight shortcut to return encoded json response, interfacing this helps decouple from Magento for portability
     *
     * @param $response
     */
    protected function jsonResponse($response) {
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));
    }

    protected function jsonErrorResponse($exceptionErrorMessage) {
        $this->getResponse()->clearHeaders()->setHeader('HTTP/1.0','500',true);
        $this->getResponse()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode(['exceptionError' => $exceptionErrorMessage]));
    }
}
