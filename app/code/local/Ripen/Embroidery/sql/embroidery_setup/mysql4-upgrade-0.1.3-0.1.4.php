<?php
$installer = $this;

$installer->startSetup();

$templateImagesTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/templateimage'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Template ID')
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Type ID')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'Image Path')
;

$installer->getConnection()->createTable($templateImagesTable);


$installer->endSetup();