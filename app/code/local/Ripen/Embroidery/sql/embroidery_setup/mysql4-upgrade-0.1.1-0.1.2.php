<?php
$installer = $this;

$installer->startSetup();

$templateProductTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/templateproduct'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Template ID')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Product ID')
;
$installer->getConnection()->createTable($templateProductTable);


$installer->endSetup();