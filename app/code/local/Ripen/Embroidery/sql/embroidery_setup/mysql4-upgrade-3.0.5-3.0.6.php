<?php
// First - rename all images based on values of name_old (which is the logo sku)
// Images get named to: LOGO-EXAMPLESKU -> logo-examplesku.png
use League\Flysystem\Adapter\Local as LocalAdapter;
use League\Flysystem\Filesystem as Filesystem;

$mediaPath = Mage::getBaseDir() . '/media/';
$artworkDirectoryName = 'embroidery_artwork';
$localAdapter = new LocalAdapter($mediaPath);
$fileSystem = new Filesystem($localAdapter);

$artworkTypeOptionModel =  Mage::getModel('embroidery/option');
$typeId = $artworkTypeOptionModel->getCollection()->getTypeIdByName('artwork');
$artworkOptions = $artworkTypeOptionModel
    ->getCollection()
    ->addFieldToFilter('type_id', $typeId);

foreach($artworkOptions as $key => $artworkOption) {
    $nameOld = trim($artworkOption->getData('name_old'));
    $imagePathExisting = $artworkOption->getData('image');
    $imageExtension = pathinfo($imagePathExisting, PATHINFO_EXTENSION);
    $imagePathRename = $artworkDirectoryName . '/' . strtolower($nameOld) . '.' . $imageExtension;
    // Rename images that do not match the name_old/sku pattern
    if(!$fileSystem->has($imagePathRename)) {
        $fileSystem->copy($imagePathExisting, $imagePathRename);
        $fileSystem->delete($imagePathExisting);
        $artworkOption->setData('image', $imagePathRename);
    }
    // For cleanup, set name_old/sku to trimmed value -- many of these that exist have trailing spaces
    $artworkOption->setData('name_old', $nameOld);
    $artworkOption->save();
}

// Second --- Add columns for enabled and featured support
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/artworkcategory'), 'enabled', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length' => 2,
        'unsigned' => true,
        'nullable' => false,
        'after' => 'sortorder',
        'comment' => 'Enabled flag',
        'default' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/artworkcategory'), 'featured_text', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'nullable' => false,
        'after' => 'sortorder',
        'comment' => 'Featured text to be displayed',
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/artworkcategory'), 'featured', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length' => 2,
        'unsigned' => true,
        'nullable' => false,
        'after' => 'sortorder',
        'comment' => 'Featured flag',
        'default' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/option'), 'enabled', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length' => 2,
        'unsigned' => true,
        'nullable' => false,
        'after' => 'sortorder',
        'comment' => 'Enabled flag',
        'default' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/option'), 'featured', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length' => 2,
        'unsigned' => true,
        'nullable' => false,
        'after' => 'sortorder',
        'comment' => 'Featured flag',
        'default' => 0
    ));

$installer->endSetup();
