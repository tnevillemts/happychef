<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/logo'),'digitized_logo', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length'    => 255,
        'after'     => 'logo',
        'comment'   => 'Digitized logo'
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/logo'),'last_modified', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        'nullable'  => false,
        "default" => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
        'after'     => 'created_at',
        'comment'   => 'Last Modified'
    ));

$installer->endSetup();