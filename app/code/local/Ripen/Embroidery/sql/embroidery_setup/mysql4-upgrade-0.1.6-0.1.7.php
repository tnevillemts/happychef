<?php
$installer = $this;

$installer->startSetup();

$priceTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/price'))
    ->addColumn('price_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Type Id')
    ->addColumn('stitches_count_from', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Stitches From')
    ->addColumn('stitches_count_to', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Stitches To')
    ->addColumn('qty_from', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Qty From')
    ->addColumn('qty_to', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Qty To')
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', array(
        'nullable'  => false,
        'default'   => 0
        //'length'    => '12,2'
    ), 'Price');
$installer->getConnection()->createTable($priceTable);

$installer->endSetup();