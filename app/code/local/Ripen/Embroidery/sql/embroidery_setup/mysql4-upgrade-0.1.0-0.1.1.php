<?php
$installer = $this;

$installer->startSetup();

$templateTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/template'))
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'name')
    ->addColumn('sortorder', Varien_Db_Ddl_Table::TYPE_INTEGER, 255, array(
        'nullable'  => false,
    ), 'Priority')
;
$installer->getConnection()->createTable($templateTable);


$templateEmbroideryTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/templatedata'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'template ID')
    ->addColumn('location_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Location ID')
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Type ID')
    ->addColumn('limit', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => true,
    ), 'Limit')
;
$installer->getConnection()->createTable($templateEmbroideryTable);



$installer->endSetup();