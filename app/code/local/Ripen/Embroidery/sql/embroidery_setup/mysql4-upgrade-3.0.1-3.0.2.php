<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/logo'), 'digitized_logos',
        array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'after'     => 'logo',
        'comment'   => 'Digitized logo'
    ));

$installer->endSetup();