<?php
$installer = $this;
$installer->startSetup();

$typeTable = $installer->getTable('embroidery/type');

// update sort order
$installer->getConnection()->update($typeTable,
    array('sortorder' => '10'),
    array('name = ?' => 'text')
);

$installer->getConnection()->update($typeTable,
    array('sortorder' => '20'),
    array('name = ?' => 'logo')
);

$installer->getConnection()->update($typeTable,
    array('sortorder' => '30'),
    array('name = ?' => 'artwork')
);

$installer->getConnection()->update($typeTable,
    array('sortorder' => '40'),
    array('name = ?' => 'patches')
);

$installer->getConnection()->update($typeTable,
    array('sortorder' => '50'),
    array('name = ?' => 'acfLogos')
);

$installer->getConnection()->update($typeTable,
    array('sortorder' => '60'),
    array('name = ?' => 'ribbons')
);

$installer->getConnection()->update($typeTable,
    array('sortorder' => '70'),
    array('name = ?' => 'flags')
);

$installer->endSetup();
