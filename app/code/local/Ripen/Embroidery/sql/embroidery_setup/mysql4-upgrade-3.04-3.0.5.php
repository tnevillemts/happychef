<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/type'), 'sortorder_alt', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length'    => 11,
        'nullable' => false,
        'after'     => 'sortorder',
        'comment' => 'Alternate sortorder column for secondary sites.'
    ));

$installer->endSetup();