<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/type'),'icon_image', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'sortorder',
        'comment'   => 'Icon Image'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/type'),'icon_title', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'icon_image',
        'comment'   => 'Icon Title'
    ));

$installer->endSetup();