<?php
$installer = $this;

$installer->startSetup();

$logoTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/logo'))
    ->addColumn('logo_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Customer Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, NULL, array(
        'nullable'  => false,
        'length'    => 255
    ), 'Artwork Name')
    ->addColumn('logo', Varien_Db_Ddl_Table::TYPE_TEXT, NULL, array(
        'nullable'  => false,
        'length'    => 255
    ), 'Path to File')
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_TEXT, NULL, array(
        'nullable'  => false,
        'length'    => 255
    ), 'Sku')
    ->addColumn('stitches_count', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Qty To')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Status 1 - digitized')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, NULL, array(
        'nullable'  => false,
        "default" => Varien_Db_Ddl_Table::TIMESTAMP_INIT
    ), 'Created At');
$installer->getConnection()->createTable($logoTable);

$installer->endSetup();