<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/location'),'name_old', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'name',
        'comment'   => 'Old Name'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/type'),'name_old', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'name',
        'comment'   => 'Old Name'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/style'),'name_old', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'name',
        'comment'   => 'Old Name'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/color'),'name_old', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'name',
        'comment'   => 'Old Name'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/option'),'name_old', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'name',
        'comment'   => 'Old Name'
    ));

$installer->endSetup();