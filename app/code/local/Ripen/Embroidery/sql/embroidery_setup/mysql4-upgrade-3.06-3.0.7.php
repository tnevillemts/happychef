<?php
$installer = $this;
$installer->startSetup();

$typeTable = $installer->getTable('embroidery/type');

// add embroidery type image column
$installer->getConnection()
    ->addColumn($typeTable,'type_image', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length'    => false,
        'after'     => 'icon_title',
        'comment'   => 'Embroidery Type Sample Image'
    ));

// update the newly added column with the appropriate value
$installer->getConnection()->update($typeTable,
    array('type_image' => 'embroidery_types/ribbon.png'),
    array('name = ?' => 'ribbons')
);

$installer->getConnection()->update($typeTable,
    array('type_image' => 'embroidery_types/flag.png'),
    array('name = ?' => 'flags')
);

$installer->getConnection()->update($typeTable,
    array('type_image' => 'embroidery_types/artwork.png'),
    array('name = ?' => 'artwork')
);

$installer->getConnection()->update($typeTable,
    array('type_image' => 'embroidery_types/text.png'),
    array('name = ?' => 'text')
);

$installer->getConnection()->update($typeTable,
    array('type_image' => 'embroidery_types/logo.png'),
    array('name = ?' => 'logo')
);

$installer->getConnection()->update($typeTable,
    array('type_image' => 'embroidery_types/acf.png'),
    array('name = ?' => 'acfLogos')
);

$installer->getConnection()->update($typeTable,
    array('type_image' => 'embroidery_types/patch.png'),
    array('name = ?' => 'patches')
);

$installer->endSetup();
