<?php
$installer = $this;

$installer->startSetup();

$locationTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/location'))
    ->addColumn('location_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'name')
    ->addColumn('sortorder', Varien_Db_Ddl_Table::TYPE_INTEGER, 255, array(
        'nullable'  => false,
    ), 'Priority')
    ->addColumn('code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'Code')
;
$installer->getConnection()->createTable($locationTable);


$typeTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/type'))
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'name')
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'Priority')
    ->addColumn('size', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'Code')
    ->addColumn('sortorder', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Code')
;
$installer->getConnection()->createTable($typeTable);


$colorTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/color'))
    ->addColumn('color_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'name')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'Code')
    ->addColumn('sortorder', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Code')
;
$installer->getConnection()->createTable($colorTable);

$styleTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/style'))
    ->addColumn('style_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'name')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'Code')
    ->addColumn('sortorder', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Code')
;
$installer->getConnection()->createTable($styleTable);

$typeOptionTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/option'))
    ->addColumn('type_option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'name')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'name')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, NULL, array(
        'nullable'  => false,
    ), 'Code')
    ->addColumn('sortorder', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Code')
;
$installer->getConnection()->createTable($typeOptionTable);


$productEmbroideryTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/embroidery'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Product ID')
    ->addColumn('location_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Location ID')
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
    ), 'Type ID')
    ->addColumn('limit', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => true,
    ), 'Limit')
;
$installer->getConnection()->createTable($productEmbroideryTable);



$installer->endSetup();