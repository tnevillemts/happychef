<?php
$installer = $this;

$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/option'),'price', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'length'    => '12,2',
        'nullable'  => false,
        'after'     => 'image',
        'comment'   => 'Price',
        'default'   => 0
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/type'),'popularity', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length'    => '2',
        'nullable'  => false,
        'after'     => 'sortorder',
        'comment'   => 'Popularity',
        'default'   => 0
    ));

$installer->endSetup();