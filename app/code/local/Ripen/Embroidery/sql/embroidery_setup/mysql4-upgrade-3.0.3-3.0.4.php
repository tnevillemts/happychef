<?php
$installer = $this;

$installer->startSetup();

$setup = Mage::getModel ( 'customer/entity_setup' , 'core_setup' );

$setup->addAttribute("customer", "last_logo_timestamp",  array(
    'type'     => 'datetime',
    'input' => 'text',
    'label' => 'Logo Timestamp',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 0,
    'default' => '',
    'visible_on_front' => 0,
    'source' =>   NULL,
    'comment' => 'Logo Timestamp'

));

$installer->endSetup();