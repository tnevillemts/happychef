<?php
$installer = $this;

$installer->startSetup();

$categoryTable = $installer->getConnection()
    ->newTable($installer->getTable('embroidery/artworkcategory'))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, NULL, array(
        'nullable'  => false,
        'length'    => 255
    ), 'Artwork Category Name')
    ->addColumn('sortorder', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'nullable'  => false,
        'default'   => 0
    ), 'Sort Order');
$installer->getConnection()->createTable($categoryTable);


$installer->getConnection()
    ->addColumn($installer->getTable('embroidery/option'), 'artwork_category_id',
        array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => true,
        'after'     => 'type_id',
        'comment'   => 'Artwork Category Id'
    ));

$installer->endSetup();