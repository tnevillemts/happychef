<?php

class Ripen_Embroidery_Model_Quote_Item extends Mage_Sales_Model_Quote_Item
{
    public function hasEmbroiderySaved(){
        if(!$this->getProduct()->getCustomOption('info_buyRequest')){
            return false;
        }

        $embroiderySavedOptionsArray = unserialize($this->getProduct()->getCustomOption('info_buyRequest')->getValue());
        $embroiderySavedOptions = $embroiderySavedOptionsArray["product-embroidery-options"];
        return  ($embroiderySavedOptions) ? true : false;
    }

    public function getEmbroideryData(){
        if($this->getProduct()->getCustomOption('info_buyRequest')) {
            $embroiderySavedOptionsArray = unserialize($this->getProduct()->getCustomOption('info_buyRequest')->getValue());
            $embroiderySavedOptions = $embroiderySavedOptionsArray["product-embroidery-options"];
            return  $embroiderySavedOptions;
        }

        return false;
    }

    public function hasLogoEmbroidery() {

        $embroideryData = json_decode($this->getEmbroideryData(), true);
        foreach($embroideryData[$this->getProductId()]["types"] as $type) {
            if ($type["name"] == "logo") {
                foreach ($type["locations"] as $location) {
                    if (count($location["selectedOption"])){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function hasLogoEmbroideryWithoutSetup() {

        $embroideryData = json_decode($this->getEmbroideryData(), true);
        $productId = $this->getProductId();

        if (empty($embroideryData[$productId]["types"]) || ! is_array($embroideryData[$productId]["types"])) return false;

        foreach($embroideryData[$productId]["types"] as $type) {
            if ($type["name"] == "logo") {
                foreach ($type["locations"] as $location) {
                    if (count($location["selectedOption"])){
                        foreach ($location["selectedOption"] as $option) {
                            if(!array_key_exists("id", $option) || !$option["stitches"]){
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

}
