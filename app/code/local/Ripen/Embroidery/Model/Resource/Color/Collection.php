<?php

class Ripen_Embroidery_Model_Resource_Color_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('embroidery/color');
    }
}
