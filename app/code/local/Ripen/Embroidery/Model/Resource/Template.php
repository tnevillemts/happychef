<?php

class Ripen_Embroidery_Model_Resource_Template extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('embroidery/template', 'template_id');
    }
}
