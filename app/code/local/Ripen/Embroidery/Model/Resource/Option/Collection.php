<?php

class Ripen_Embroidery_Model_Resource_Option_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * @var array
     */
    protected $typeIdByNameReference = array();

    /**
     * _construct()
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('embroidery/option');
    }

    /**
     * @param null $name
     * @param string $sortOrder
     * @return $this
     */
    public function getOptionsByTypeName($name = null, $sortOrder = 'ASC') {

        if(!is_null($name)) {
            $typeId = $this->getTypeIdByName($name);
            $this->addFilter('type_id', $typeId)
                ->setOrder('sortorder', $sortOrder);;
        }
        return $this;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getTypeIdByName($name) {
        $this->setTypeIdByNameReference();
        return $this->typeIdByNameReference[$name];
    }

    /**
     * @param string $sortOrder
     */
    public function setTypeIdByNameReference($sortOrder = 'ASC') {
        $types = Mage::getModel('embroidery/type')
            ->getCollection()
            ->setOrder('sortorder', $sortOrder);
        foreach($types as $type) {
            $this->typeIdByNameReference[$type->getName()] = $type->getTypeId();
        }
    }

}
