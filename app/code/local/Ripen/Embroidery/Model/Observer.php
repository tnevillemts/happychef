<?php
class Ripen_Embroidery_Model_Observer {

    static protected $_singletonFlag = false;

    const SETUP_SKU = 'EMBROIDERY-SETUP';
    const FREE_SETUP_SKU = 'EMBROIDERY-SETUP-FRE';
    const LIMIT_FREE_SETUP = 1;
    const FREE_SETUP_TOTAL = 150;
    const LOGO_LIFETIME_DAYS = 548;

    public function collectGarbageLogos()
    {
        $firstDate = $date = Mage::getStoreConfig('embroidery/general/last_date_embroidery_logos_garbage_collector');
        $lastDate = date('Y-m-d', strtotime('-'. self::LOGO_LIFETIME_DAYS .' days'));

        Mage::log("--------------------------------------", null, 'embroideryGarbageCollector.log');
        Mage::log("Garbage collection started...", null, 'embroideryGarbageCollector.log');

        while (strtotime($date) <= strtotime($lastDate)) {

            $path = Mage::helper('embroidery')->getEmbroideryImagePath() . DS . $date;
            $files = preg_grep('/^([^.])/', scandir($path));

            Mage::log("  Checking files in [{$path}]", null, 'embroideryGarbageCollector.log');

            foreach($files as $file){

                $orderItem = Mage::getModel('sales/order_item')
                    ->getCollection()
                    ->addFieldToFilter('product_options', array("like" => '%'.$file.'%'))
                    ->addFieldToFilter('product_type', 'configurable')
                    ->addFieldToFilter('created_at', array("gteq" => $firstDate))
                    ->getLastItem();
                ;

                if ($orderItem->getId()){
                    $logMessage = "Found order item [{$orderItem->getId()}] for order [{$orderItem->getOrderId()}]";
                } else {
                    unlink($path . DS . $file);
                    $logMessage = "No orders found. File removed";
                }

                Mage::log("    File [{$file}] - {$logMessage}", null, 'embroideryGarbageCollector.log');
            }

            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

        Mage::log("Garbage collection completed...", null, 'embroideryGarbageCollector.log');


    }

    public function applyEmbroideryDiscount(Varien_Event_Observer $observer)
    {
        Mage::getModel('embroidery/embroidery')->applyEmbroideryDiscount();
    }


    /**
     * Add options to quote item
     */
    public function catalogProductLoadAfter(Varien_Event_Observer $observer)
    {
        // set the additional options on the product
        $action = Mage::app()->getFrontController()->getAction();

        if ($action->getFullActionName() == 'checkout_cart_configure')
        {
            $embroideryData = $action->getRequest()->getParam('product-embroidery-options');
            $embroideryData = str_replace("\\", "",$embroideryData);
            if($embroideryData) {
                $observer->getProduct()->addCustomOption('embroidery_options', $embroideryData);
            }
        }

        if ($action->getFullActionName() == 'ajaxcart_cart_add')
        {
            $embroideryData = $action->getRequest()->getParam('product-embroidery-options');
            $embroideryData = str_replace("\\", "",$embroideryData);
            if($embroideryData) {
                $observer->getProduct()->addCustomOption('embroidery_options', $embroideryData);
            }
        }

        if ($action->getFullActionName() == 'checkout_cart_updateItemOptions')
        {
            $embroideryData = $action->getRequest()->getParam('product-embroidery-options');
            $embroideryData = str_replace("\\", "",$embroideryData);
            if($embroideryData){
                $observer->getProduct()->addCustomOption('embroidery_options', $embroideryData);
            }
        }
    }


    /**
     * Add options to order item
     */
    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {
        $quoteItem = $observer->getItem();
        if ($embroideryOptions = $quoteItem->getOptionByCode('embroidery_options')) {
            $orderItem = $observer->getOrderItem();
            $options = $orderItem->getProductOptions();
            $options['embroidery_options'] = $embroideryOptions->getValue();
            $orderItem->setProductOptions($options);
        }
    }

    /**
     * Add support for reorders
    */
    public function checkoutCartProductAddAfter(Varien_Event_Observer $observer)
    {
        $action = Mage::app()->getFrontController()->getAction();
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getEvent()->getProduct();

        if ($action->getFullActionName() == 'sales_order_reorder')
        {
            if ($option = $quoteItem->getProduct()->getCustomOption('info_buyRequest')) {
                $optionArray = unserialize($option->getValue());
                $optionArray["product-embroidery-options"] = "hello";
                $option->setValue(serialize($optionArray));
            }
        }

        return $this;
    }

    public function handleEmbroiderySetupFee()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        $newLogos = array();
        $logosWithSetupFee = array();
        $productsTotal = 0;

        foreach ($quote->getAllVisibleItems() as $item) {

            if (in_array($item["sku"], [self::SETUP_SKU, self::FREE_SETUP_SKU]) && !in_array($item["additional_data"], $logosWithSetupFee)){
                array_push($logosWithSetupFee, $item["additional_data"]);
            }

            $embroideryData = json_decode($item->getEmbroideryData(), true);
            $productId = $item->getProductId();
            if (! empty($embroideryData[$productId]["types"]) && is_array($embroideryData[$productId]["types"])) {
                foreach($embroideryData[$productId]["types"] as $type) {
                    if ($type["name"] == "logo") {
                        foreach ($type["locations"] as $location) {
                            if (count($location["selectedOption"])){
                                foreach ($location["selectedOption"] as $option) {
                                    if(!array_key_exists("id", $option) || !$option["stitches"]){
                                        array_push($newLogos, $option["image"]);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!in_array($item->getSku(), [self::SETUP_SKU, self::FREE_SETUP_SKU])){
                $productsTotal += ($item["custom_price"] * $item["qty"]);
            }

        }

        $logosWithoutSetupFee = array_diff($newLogos, $logosWithSetupFee);
        $setupFeesWithoutLogos = array_diff($logosWithSetupFee, $newLogos);
        $setupFeeProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', self::SETUP_SKU);
        $freeSetupFeeProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', self::FREE_SETUP_SKU);

        foreach($logosWithoutSetupFee as $logo){

            $quoteItem = Mage::getModel('sales/quote_item')
                ->setProduct($setupFeeProduct)
                ->setQty(1)
                ->setStoreId(Mage::app()->getStore()->getStoreId())
                ->setAdditionalData($logo);

            $option = Mage::getModel('sales/quote_item_option')
                ->setProductId($setupFeeProduct->getId())
                ->setCode('product_type')
                ->setProduct($setupFeeProduct)
                ->setValue('embroidery_setup');

            $quoteItem->addOption($option);
            $quote->addItem($quoteItem);
        }

        foreach($setupFeesWithoutLogos as $logo) {
            $quoteItems = $quote->getAllItems();
            foreach ($quoteItems as $quoteItem) {
                if ($quoteItem->getAdditionalData() == $logo){
                    $quote->removeItem($quoteItem->getId());
                }
            }
        }

        $freeSetupCount = 0;
        if ($productsTotal >= self::FREE_SETUP_TOTAL  && $newLogos){
            foreach ($quote->getAllItems() as $item) {
                if (in_array($item->getSku(),[self::SETUP_SKU, self::FREE_SETUP_SKU])){
                    if ($freeSetupCount < self::LIMIT_FREE_SETUP){

                        $item->setProduct($freeSetupFeeProduct);
                        $item->setSku(self::FREE_SETUP_SKU);
                        $item->setProductId($freeSetupFeeProduct->getId());
                        $embPrice = $freeSetupFeeProduct->getPrice();
                        $item->setCustomPrice($embPrice);
                        $item->setOriginalCustomPrice($embPrice);
                        $item->getProduct()->setIsSuperMode(true);
                        $item->save();

                        $freeSetupCount++;

                    } else {
                        $item->setProduct($setupFeeProduct);
                        $item->setSku(self::SETUP_SKU);
                        $item->setProductId($setupFeeProduct->getId());
                        $item->setCustomPrice($setupFeeProduct->getPrice());
                        $item->setOriginalCustomPrice($setupFeeProduct->getPrice());
                        $item->getProduct()->setIsSuperMode(true);
                        $item->save();
                    }
                }
            }

        } else if ($productsTotal < self::FREE_SETUP_TOTAL  && $newLogos){
            foreach ($quote->getAllItems() as $item) {
                if ($item->getSku() == self::FREE_SETUP_SKU){
                    $item->setProduct($setupFeeProduct);
                    $item->setSku(self::SETUP_SKU);
                    $item->setProductId($setupFeeProduct->getId());
                    $item->setCustomPrice($setupFeeProduct->getPrice());
                    $item->setOriginalCustomPrice($setupFeeProduct->getPrice());
                    $item->setProduct($setupFeeProduct);
                    $item->getProduct()->setIsSuperMode(true);
                    $item->save();
                }
            }
        }
        $quote->save();
        $quote->setTotalsCollectedFlag(false)->collectTotals()->save();

        return $this;
    }

    public function salesConvertOrderToQuote(Varien_Event_Observer $observer){

        $action = Mage::app()->getFrontController()->getAction();
        $item = $observer->getQuote();
        echo $item->getId()."--";

        var_dump($observer->getData());
        die("+++");

    }


    /**
     * This method will run when the product is saved from the Magento Admin
     * Use this function to update the product model, process the
     * data or anything you like
     *
     * @param Varien_Event_Observer $observer
     */
    public function saveProductTabData(Varien_Event_Observer $observer)
    {
        if (!self::$_singletonFlag) {
            self::$_singletonFlag = true;

            $product = $observer->getEvent()->getProduct();

            try {

                $data =  $this->_getRequest()->getPost('product');
                $regProduct = Mage::registry('product');
                if(!is_null($regProduct)) {

                    if( (int)$data["embroidery_template_id"] ){ // load from template

                        $productEmbroideryCollection = Mage::getModel('embroidery/embroidery')
                            ->getCollection()
                            ->addFieldToFilter('product_id', $product->getId());

                        // Remove existing records
                        foreach($productEmbroideryCollection as $row){
                            $row->delete();
                        }

                        $templateEmbroideryCollection = Mage::getModel('embroidery/templatedata')
                            ->getCollection()
                            ->addFieldToFilter('template_id', $data["embroidery_template_id"]);

                        foreach($templateEmbroideryCollection as $templateRow){
                            $newRecord = Mage::getModel('embroidery/embroidery');
                            $newRecord->setLocationId($templateRow->getLocationId());
                            $newRecord->setTypeId($templateRow->getTypeId());
                            $newRecord->setProductId($product->getId());
                            $newRecord->setLimit($templateRow->getLimit());
                            $newRecord->save();
                        }

                    } else {

                        $locations = Mage::getModel('embroidery/location')->getCollection();
                        $types = Mage::getModel('embroidery/type')->getCollection();

                        foreach ( $locations as $location ){
                            foreach ( $types as $type ){
                                $key = "embroidery_option-{$location->getLocationId()}-{$type->getTypeId()}";

                                $productEmbroideryCollection = Mage::getModel('embroidery/embroidery')
                                    ->getCollection()
                                    ->addFieldToFilter('product_id', $product->getId())
                                    ->addFieldToFilter('location_id', $location->getLocationId())
                                    ->addFieldToFilter('type_id', $type->getTypeId())
                                ;

                                //echo $productEmbroideryCollection->getSelect()."--";
                                $productEmbroidery = $productEmbroideryCollection->getFirstItem();
                                //echo $productEmbroideryCollection->count()."==";

                                if($productEmbroideryCollection->count()){
                                    $productEmbroidery->delete()->save();
                                }

                                if ( $data[$key] == 1){

                                    $newRecord = Mage::getModel('embroidery/embroidery');
                                    $newRecord->setLocationId($location->getLocationId());
                                    $newRecord->setTypeId($type->getTypeId());
                                    $newRecord->setProductId($product->getId());

                                    // Dirty workaround for setting number of text lines for text type
                                    if($type->getTypeId() == 4){
-                                        $textLimitKey = "embroidery_text_limit-{$location->getLocationId()}-{$type->getTypeId()}";
                                        if ($data[$textLimitKey]){
                                            $newRecord->setLimit($data[$textLimitKey]);
                                        }
                                    }
                                    $newRecord->save();
                                }

                            }
                        }
                    }

                    if( (int)$data["embroidery_diagram_id"] ){ // load from template

                        $templateProductCollection = Mage::getModel('embroidery/templateproduct')
                            ->getCollection()
                            ->addFieldToFilter('product_id', $product->getId());

                        // Remove existing records
                        foreach($templateProductCollection as $row){
                            $row->delete();
                        }

                        $newRecord = Mage::getModel('embroidery/templateproduct');
                        $newRecord->setTemplateId($data["embroidery_diagram_id"]);
                        $newRecord->setProductId($product->getId());
                        $newRecord->save();
                    }
                }

            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
    }

    /**
     * Retrieve the product model
     *
     * @return Mage_Catalog_Model_Product $product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Shortcut to getRequest
     *
     */
    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }


}