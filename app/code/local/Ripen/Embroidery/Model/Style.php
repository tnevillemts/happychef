<?php

class Ripen_Embroidery_Model_Style extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('embroidery/style');
    }
}
