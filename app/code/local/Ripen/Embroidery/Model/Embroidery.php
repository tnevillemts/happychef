<?php

class Ripen_Embroidery_Model_Embroidery extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('embroidery/embroidery');
    }

    public function getCartEmbroideryQty($quoteItemId = NULL){

        $arr = [];
        $cart = Mage::getModel('checkout/cart')->getQuote();
        foreach ($cart->getAllVisibleItems() as $item) {

            // for cart page or edit mode on PDP, exclude item the item in question
            if($item->getId() != $quoteItemId){

                $qty = $item->getQty();
                if($item->getProduct()->getCustomOption('info_buyRequest')){
                    $embroiderySavedOptionsArray = unserialize($item->getProduct()->getCustomOption('info_buyRequest')->getValue());
                    $embroiderySavedOptions = $embroiderySavedOptionsArray["product-embroidery-options"];
                    if ($embroiderySavedOptions) {
                        $productEmbroidery = json_decode($embroiderySavedOptions, true);
                        foreach($productEmbroidery[$item->getProduct()->getId()]["types"] as $typeId=>$type) {
                            foreach ($type["locations"] as $location) {
                                if($location["selectedOption"]){
                                    foreach($location["selectedOption"] as $option){
                                        $found = false;

                                        if (empty($arr[$typeId]) || ! is_array($arr[$typeId])) continue;

                                        foreach($arr[$typeId] as $key => $opt){
                                            if($opt["id"] == $option["id"]){
                                                $arr[$typeId][$key]["count"] = $arr[$typeId][$key]["count"] + $qty;
                                                $found = true;
                                            }
                                        }
                                        if(!$found) {
                                            $arr[$typeId][] = array("id"=>$option["id"], "count"=>$qty);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /*
        $arr = [];
        $arr["3"] = array(
            array("id"=>26, "count"=>7),
            array("id"=>27, "count"=>4)
        );
        $arr["4"] = array(
            array("id"=>23, "count"=>3),
        );
        */

        return $arr;

    }
    public function getTypesTierPrices(){

        $tierPrices = Array();
        $types = Mage::getModel('embroidery/type')->getCollection();
        foreach($types as $type){

            $priceCollection = Mage::getModel('embroidery/price')
                ->getCollection()
                ->addFieldToFilter('type_id', $type->getTypeId());

            foreach($priceCollection as $price){
                $tierPrices[$type->getTypeId()][] = array(
                    "price_id"=>$price->getPriceId(),
                    "stitches_count_from"=>$price->getStitchesCountFrom(),
                    "stitches_count_to"=>$price->getStitchesCountTo(),
                    "qty_from"=>$price->getQtyFrom(),
                    "qty_to"=>$price->getQtyTo(),
                    "price"=>$price->getPrice()
                );
            }
        }

        return $tierPrices;
    }

    public function applyEmbroideryDiscount(){

        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();

        $embroideryCountArr = $this->getCartEmbroideryQty();
        $tierPrices = $this->getTypesTierPrices();
        $updatedLineItemPrices = [];

        foreach ($cart->getItems() as $item) {

            $option = $item->getProduct()->getCustomOption('info_buyRequest');
            if ($option) {
                $optionArray = unserialize($option->getValue());
                $embroideryData = $optionArray["product-embroidery-options"];

                if ($embroideryData){

                    foreach($embroideryCountArr as $typeId => $options){

                        foreach($options as $key => $opt){
                            $qtyInCart = $opt["count"];

                            if (empty($tierPrices[$typeId]) || ! is_array($tierPrices[$typeId])) continue;

                            foreach($tierPrices[$typeId] as $tierPrice){
                                if($qtyInCart >= $tierPrice["qty_from"] && $qtyInCart <= $tierPrice["qty_to"]){

                                    $price = $tierPrice["price"];

                                    $productEmbData = json_decode($embroideryData, true);
                                    $locations = $productEmbData[$item->getProduct()->getId()]["types"][$typeId]["locations"];

                                    foreach($locations as $locationId => $location){
                                        foreach($location["selectedOption"] as $index => $selectedOption){
                                            if(
                                                ($selectedOption["id"] == $opt["id"]) &&
                                                ( $typeId != 5 || ($typeId == 5 && $selectedOption["stitches"] > 0 && $selectedOption["stitches"] >= $tierPrice["stitches_count_from"] && $selectedOption["stitches"] <= $tierPrice["stitches_count_to"]))
                                            ){
                                                $productEmbData[$item->getProduct()->getId()]["types"][$typeId]["locations"][$locationId]["selectedOption"][$index]["price"] = $price;
                                                break;
                                            }
                                        }
                                    }

                                    $newProductEmbData = json_encode($productEmbData,true);
                                    $optionArray["product-embroidery-options"] = $newProductEmbData;
                                    $option->setValue(serialize($optionArray))->save();
                                    if ($embOpt = $item->getOptionByCode('embroidery_options')) {
                                        $embOpt->setValue($newProductEmbData)->save();
                                    }

                                    $quote->setTotalsCollectedFlag(false)->collectTotals();

                                    $newItemTotal = Mage::helper('core')->currency($item->getPriceInclTax() * $item->getQty(), true, false);
                                    $newItemUnitPrice = Mage::helper('core')->currency($item->getPriceInclTax(), true, false);
                                    $updatedLineItemPrices[$item->getId()] = array("newItemTotal"=>$newItemTotal, "newItemUnitPrice"=>$newItemUnitPrice);

                                    break;
                                }
                            }
                        }
                    }
                }
	    }
        }

        return $updatedLineItemPrices;

    }


}
