<?php

class Ripen_Embroidery_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
    public function hasEmbroidery()
    {
        $productEmbroideryCollection = Mage::getModel('embroidery/embroidery')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getId());

        return $productEmbroideryCollection->getSize() ? true : false;
    }


    public function getProductEmbroidery(){

        $productId = $this->getId();

        $productEmbroideryCollection = Mage::getModel('embroidery/embroidery')
            ->getCollection()
            ->addFieldToFilter('product_id', $productId);

        $productEmbroideryCollection->getSelect()
            ->join(
                array('locations' => Mage::getSingleton('core/resource')->getTableName('embroidery/location')),
                'main_table.location_id = locations.location_id',
                array('location_name' => 'name'))
            ->join(
                array('types' => Mage::getSingleton('core/resource')->getTableName('embroidery/type')),
                'main_table.type_id = types.type_id',
                array('type_name' => 'name', 'sortorder' => 'sortorder'))
            ->order('types.sortorder', 'ASC')
            ->order('locations.sortorder', 'ASC')
        ;

        $productEmbroidery = Array();
        foreach ($productEmbroideryCollection as $record){
            $productEmbroidery[$productId]["types"][$record->getTypeId()]["name"] = $record->getTypeName();
            $productEmbroidery[$productId]["types"][$record->getTypeId()]["sortorder"] = $record->getSortorder();

            if($record->getLimit()) {
                $productEmbroidery[$productId]["types"][$record->getTypeId()]["locations"][$record->getLocationId()]["limit"] = $record->getLimit();

                // hard code the number of text lines to 2 for SAGE site (store id = 2)
                if(Mage::app()->getStore()->getId() == 2){
                    $productEmbroidery[$productId]["types"][$record->getTypeId()]["locations"][$record->getLocationId()]["limit"] = 2;
                }
            }

            $productEmbroidery[$productId]["types"][$record->getTypeId()]["locations"][$record->getLocationId()]["name"] = $record->getLocationName();
            $productEmbroidery[$productId]["types"][$record->getTypeId()]["locations"][$record->getLocationId()]["selectedOption"] = [];
            $productEmbroidery[$productId]["instructions"] = "";

            $templateRecord = Mage::getModel('embroidery/templateproduct')
                ->getCollection()
                ->addFieldToFilter('product_id', $productId);
            $templateRecord->getSelect()
                ->join(
                    array('templateimage' => Mage::getSingleton('core/resource')->getTableName('embroidery/templateimage')),
                    'main_table.template_id = templateimage.template_id',
                    array('template_image' => 'image'))
                ->where('templateimage.type_id = '.$record->getTypeId())
                ->limit(1);

            if($templateRecord->count()){
                foreach($templateRecord as $template){
                    $templateImage = $template->getTemplateImage();
                }
                $productEmbroidery[$productId]["types"][$record->getTypeId()]["templateImage"] = Mage::getBaseUrl('media').$templateImage;
            }
        }

        return $productEmbroidery;

    }
}
