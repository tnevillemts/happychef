<?php

class Ripen_Embroidery_Model_Catalog_Product_Type_Price extends Mage_Catalog_Model_Product_Type_Price
{

    protected function _applyOptionsPrice($product, $qty, $finalPrice)
    {

        if( $option = $product->getCustomOption('info_buyRequest')){

            $infoArr = unserialize($option->getValue());

            $embroideryData = $infoArr["product-embroidery-options"];

            // Check if at least one embroidery option was chosen
            $isEmbroiderySet = Mage::helper('embroidery')->isEmbroiderySet($embroideryData);

            if($isEmbroiderySet){
                $totalPrice = Mage::helper('embroidery')->getEmbroideryPrice($embroideryData);
                $finalPrice = $finalPrice + $totalPrice;
            } else {
                unset($infoArr["product-embroidery-options"]);
                $option->setValue(serialize($infoArr));
            }

        }

        return $finalPrice;
    }

}
