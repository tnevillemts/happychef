<?php

class Ripen_Embroidery_Model_Customer_Customer extends Mage_Customer_Model_Customer
{
    public function saveEmbroideryLogos($order){

        // save logos used for embroidery
        $session = Mage::getSingleton('core/session');
        $sessionLogos = $session->getData('embroideryLogos');

        foreach($sessionLogos as $key=>$sessionLogo){
            $orderItems = $order->getAllVisibleItems();
            foreach($orderItems as $orderItem) {

                $options = $orderItem->getProductOptions();

                $filename = substr($sessionLogo["image"], strrpos($sessionLogo["image"], '/') + 1);

                if ($sessionLogo["saved"] !== true && $sessionLogo["image"] && strpos($options["info_buyRequest"]["product-embroidery-options"], $filename)){

                    $data = array(
                        'customer_id' => $this->getId(),
                        'name' => $sessionLogo["name"],
                        'logo' => str_replace(Mage::getBaseUrl('media'), "", $sessionLogo["image"])
                    );

                    $logo = Mage::getModel('embroidery/logo');
                    try {
                        $logo->setData($data)->save();
                        $sessionLogo["saved"] = true;
                        unset($sessionLogos[$key]);
                    } catch (Exception $e) {
                        Mage::log($e->getMessage());
                    }
                }
            }
        }

        $session->setData('embroideryLogos', $sessionLogos);

        // Comment the line below if you want to keep the logos in the session after order is placed
        $session->unsetData('embroideryLogos');

    }

    public function getLogos(){

        $logos = array();
        $logosCollection = Mage::getModel('embroidery/logo')
            ->getCollection()
            ->setOrder('created_at', 'DESC')
            ->addFieldToFilter('customer_id', array("eq"=>$this->getId()));

        $i = 0;
        foreach($logosCollection as $logo) {

            $digitizedLogos = json_decode($logo->getDigitizedLogos(),true);

            if (! empty($digitizedLogos) && is_array($digitizedLogos)) {
                foreach($digitizedLogos as $digitizedLogo) {

                    if($digitizedLogo["is_category"] == 0 || count($digitizedLogos) == 1){
                        $minPrice = $maxPrice = 0;
                        if($digitizedLogo["stitches_count"] > 0){
                            // hard code typeId for logos: 5
                            $logoPrices = Mage::helper('embroidery')->getEmbroideryTierPrices(5, $digitizedLogo["stitches_count"]);
                            $minPrice = $logoPrices->getFirstItem()->getPrice();
                            $maxPrice = $logoPrices->getlastItem()->getPrice();

                            $logoTierPrices = array();
                            foreach($logoPrices as $logoPrice){
                                $logoTierPrices[] = array(
                                    "price_id"=>$logoPrice->getPriceId(),
                                    "qty_from"=>$logoPrice->getQtyFrom(),
                                    "qty_to"=>$logoPrice->getQtyTo(),
                                    "price"=>$logoPrice->getPrice()
                                );
                            }
                        }

                        $i++;
                        $logos[] = array(
                            "id"=>$logo->getLogoId(),
                            "name"=>$logo->getName(),
                            "image"=>Mage::getBaseUrl('media').$logo->getLogo(),
                            "digitized_image"=>Mage::getBaseUrl('media').$digitizedLogo["digitized_logo"],
                            "sortorder"=> $i * 10,
                            "stitches"=>$digitizedLogo["stitches_count"],
                            "status"=>$digitizedLogo["status"],
                            "min_price"=>$minPrice,
                            "max_price"=>$maxPrice,
                            "tier_prices"=>$logoTierPrices
                        );
                    }
                }
            }

            if(!count($digitizedLogos)){
                $i++;
                $logos[] = array(
                    "id"=>$logo->getLogoId(),
                    "name"=>$logo->getName(),
                    "image"=>Mage::getBaseUrl('media').$logo->getLogo(),
                    "digitized_image"=>"",
                    "sortorder"=> $i * 10,
                    "stitches"=>0,
                    "status"=>0,
                    "min_price"=>0,
                    "max_price"=>0,
                    "tier_prices"=>array()
                );
            }
        }

        return $logos;

    }

}
