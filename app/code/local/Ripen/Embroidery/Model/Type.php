<?php

class Ripen_Embroidery_Model_Type extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('embroidery/type');
    }
}
