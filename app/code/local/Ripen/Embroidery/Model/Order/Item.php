<?php

class Ripen_Embroidery_Model_Order_Item extends Mage_Sales_Model_Order_Item
{
    public function getFirstLogo(){

        $embroideryData = $this->getEmbroideryData();
        foreach($embroideryData as $locations){
            foreach ($locations["types"] as $type) {
                if($type["name"] == "logo"){
                    return $logo = $type["options"][0]["image"];
                }
            }
        }
        return false;
    }

    public function getEmbroideryData(){

        $options = $this->getProductOptions();
        $embroideryObject = json_decode($options["info_buyRequest"]["product-embroidery-options"], true);
        $embroidery = $embroideryObject[$this->getProductId()];
        $types = $embroidery["types"];

        $embroideryReverse = array();

        if (! empty($types) && is_array($types)) {
            foreach($types as $typeId=>$type){
                $locations = $type["locations"];

                foreach($locations as $locationId=>$location){
                    if(count($location["selectedOption"])){

                        $newType = array();
                        $newType["name"] = $type["name"];
                        $newType["id"] = $typeId;
                        $newType["options"] = array();

                        foreach($location["selectedOption"] as $optionId=>$option){

                            $newOption = array();
                            $newOption["id"] = $option["id"];
                            $newOption["name"] = $option["name"];
                            $newOption["price"] = $option["price"];
                            $newOption["textLine"] = $option["textLine"];
                            $newOption["textColorId"] = $option["textColor"];
                            $newOption["textColorValue"] = $option["textColorValue"];
                            $newOption["textStyleId"] = $option["textStyle"];
                            $newOption["textStyleValue"] = $option["textStyleValue"];
                            $newOption["textLineNumber"] = $option["sortorder"];
                            $newOption["image"] = $option["image"];
                            $newOption["imageOriginal"] = $option["imageOriginal"];
                            $newOption["artworkColorId"] = $option["artworkColorId"];
                            $newOption["artworkColorValue"] = $option["artworkColorValue"];
                            $newOption["artworkColorImage"] = $option["artworkColorImage"];

                            $newType["options"][] = $newOption;

                        }

                        $addNewLocation = true;
                        foreach($embroideryReverse as $idx=>$loc){
                            if($loc["id"] == $locationId){
                                $embroideryReverse[$idx]["types"][] = $newType;
                                $addNewLocation = false;
                                break;
                            }
                        }


                        if($addNewLocation){
                            $newLocation = array();
                            $newLocation["name"] = $location["name"];
                            $newLocation["id"] = $locationId;
                            $newLocation["types"] = array();
                            $newLocation["types"][] = $newType;
                            $embroideryReverse[] = $newLocation;
                        }
                    }
                }
            }
        }
        return $embroideryReverse;
    }
}
