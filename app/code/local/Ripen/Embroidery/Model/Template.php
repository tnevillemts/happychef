<?php

class Ripen_Embroidery_Model_Template extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('embroidery/template');
    }

    public function getSupportedTypes(){

        $templateData = Mage::getModel('embroidery/templatedata')
            ->getCollection()
            ->addFieldToFilter('template_id', $this->getTemplateId())
            ->addFieldToSelect('type_id');


        $templateData->getSelect()
            ->join(
                array('types' => Mage::getSingleton('core/resource')->getTableName('embroidery/type')),
                'main_table.type_id = types.type_id',
                array('type_name' => 'name'))
            ->group('type_id');

        return $templateData;

    }
}
