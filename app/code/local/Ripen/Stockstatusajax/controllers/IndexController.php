<?php
/**
 * Stock Status Ajax
 *
 * @package    Ripen_Stockstatusajax
 * @author     Ripen Dev Team
 */
class Ripen_Stockstatusajax_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        if ($isAjax) {
            $request = $this->getRequest();
            $module = $request->getModuleName();
            $controller = $request->getControllerName();
            $action = $request->getActionName();
            $productId = $this->getRequest()->getParam('productId');
            $product = Mage::getModel('catalog/product')->load($productId);
            $response['skuModel'] = $product->getSku();
            $response['pageType'] = ($module == 'checkout' && $controller == 'cart' && $action == 'index') ? 'Cart' : 'Order';
            $response['statusText'] = "In Stock";
            $response['availQty'] = 0;
            $response['pdpQty'] = (int)($this->getRequest()->getParam('qtyRequested'));
            $response['cartQty'] = 0;

            if ($product->getTypeId() != 'configurable') {
                $cartItems = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                $response['availQty'] = (int)($stockItem->getData('qty'));
                $response['statusText'] = Mage::helper('amstockstatus')->getCustomStockStatusText($product);

                foreach ($cartItems as $cartItem) {
                    $cartProductId = $cartItem->getProduct()->getId();
                    $cartItemOption = $cartItem->getOptionByCode('simple_product');

                    if ($cartItemOption != null) {
                        $cartProductId = $cartItemOption->getProduct()->getId();
                    }

                    if ($cartProductId == $productId) {
                        $response['cartQty'] += $cartItem->getQty();
                    }
                }

                $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
                $this->getResponse()->setBody(json_encode($response));
            }
        }
    }
}