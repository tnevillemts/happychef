<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 8/28/18
 * Time: 4:33 PM
 */
class Ripen_WebshopappsExtend_Premiumrate_Model_Mysql4_Carrier_Premiumrate_Collection extends Webshopapps_Premiumrate_Model_Mysql4_Carrier_Premiumrate_Collection
{
    public function setWebsiteFilter($websiteId)
    {
        if ($websiteId == 2) {
            $websiteId = 1;
        }
        $this->_select->where("website_id = ?", $websiteId);

        return $this;
    }
}