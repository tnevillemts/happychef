<?php
/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

/** @var Ripen_BetterCheckout_Helper_Data $checkoutHelper */
$checkoutHelper = Mage::helper('bettercheckout');
$customQuoteSuffix = $checkoutHelper::CUSTOM_QUOTE_SUFFIX;

$this->getConnection()->update(
    $this->getTable('shipping_premiumrate'),
    ['delivery_type' => new Zend_Db_Expr("CONCAT(delivery_type, ' $customQuoteSuffix')")],
    'price = 0'
);

$this->endSetup();
