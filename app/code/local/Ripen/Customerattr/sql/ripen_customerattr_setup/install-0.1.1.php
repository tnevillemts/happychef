<?php
$installer = $this;

Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " X ", null, '1251.log');

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute("customer", "corp_customer_id",  array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Corp Customer Id",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    'user_defined' => 0,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => "Corp Customer Id"
    ));

$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "corp_customer_id");

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'corp_customer_id',
    '999'  //sort_order
);

$used_in_forms=array();

$used_in_forms[]="adminhtml_customer";
//$used_in_forms[]="checkout_register";
//$used_in_forms[]="customer_account_create";
//$used_in_forms[]="customer_account_edit";
//$used_in_forms[]="adminhtml_checkout";
        $attribute->setData("used_in_forms", $used_in_forms)
                ->setData("is_used_for_customer_segment", true)
                ->setData("is_system", 0)
                ->setData("is_user_defined", 1)
                ->setData("is_visible", 1)
                ->setData("sort_order", 100)
                ;
        $attribute->save();

$installer->endSetup();
