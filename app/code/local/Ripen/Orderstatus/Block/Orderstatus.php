<?php
class Ripen_Orderstatus_Block_Orderstatus extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getContactform()
     { 
        if (!$this->hasData('orderstatus')) {
            $this->setData('orderstatus', Mage::registry('orderstatus'));
        }
        return $this->getData('orderstatus');
        
    }

    public function getFormActionUrl()
    {
        return $this->getUrl('orderstatus/result/', array('_secure' => false));
    }

}