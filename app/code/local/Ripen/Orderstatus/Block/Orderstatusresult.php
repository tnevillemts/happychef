<?php
class Ripen_Orderstatus_Block_Orderstatusresult extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    public function getOrder()
    {
        return Mage::registry('current_order');
    }
}
