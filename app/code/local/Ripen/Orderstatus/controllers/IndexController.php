<?php
class Ripen_Orderstatus_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
		$this->loadLayout();
        $this->getLayout()->getBlock('orderstatus')->setFormAction( Mage::getUrl('*/result/') );
        $this->getLayout()->getBlock('head')->setTitle('Happy Chef Order Status');
		$this->renderLayout();

    }

}
