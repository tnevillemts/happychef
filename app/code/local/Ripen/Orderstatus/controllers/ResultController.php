<?php
class Ripen_Orderstatus_ResultController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {

        $parms = $this->getRequest()->getParams();

        if (!$parms['email'] || !$parms['confirmation_number']) {
            $this->_redirect('*/index');
            return false;
        }

        $data['email'] = $parms['email'];
        $data['confirmation_number'] = $parms['confirmation_number'];

        try {

            $collection = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToFilter('increment_id', $data['confirmation_number'])
                ->addAttributeToFilter('customer_email', $data['email']);

            $_orderId = $collection->getFirstItem()->getId();
            $_order = Mage::getModel('sales/order')->load($_orderId);
            Mage::register('current_order', $_order);

        } catch(Exception $e) {
            return $e;
        }

        $this->loadLayout();
        $this->getLayout()->getBlock('orderstatusresult');
        $this->getLayout()->getBlock('head')->setTitle('Happy Chef Order Status');
        $this->renderLayout();
    }

}
