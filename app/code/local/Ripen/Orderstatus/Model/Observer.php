<?php

class Ripen_Orderstatus_Model_Observer
{
    public function afterOrder(Varien_Event_Observer $observer)
    {
        $this->storeCustomStockStatus($observer);
        $this->clearSession($observer);
    }

    public function storeCustomStockStatus(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getData('order');
        $items = $order->getAllItems();

        foreach ($items as $item) {

            $productType = $item->getProductType();
            // Default to 'In stock' saves a little logic. Configurable has no status.
            $statusUponOrdering = ($productType != 'configurable') ? 'In Stock': '';
            $product = $item->getProduct();
            $customStockStatus = $product->getAttributeText('custom_stock_status');
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            $preorderQuantity = $item->getData('qty_ordered') + $stockItem->getData('qty');

            if(($productType == 'simple') && (strtolower($customStockStatus) != 'in stock')){
                if ($item->getData('qty_ordered') > $preorderQuantity) {
                    $statusUponOrdering = $customStockStatus;
                }
            }

            $item->setStatusUponOrdering($statusUponOrdering);
            $item->save();
        }
    }

    private function clearSession(Varien_Event_Observer $observer)
    {
        Mage::getSingleton("checkout/session")->setData("coupon_code","");
    }

}