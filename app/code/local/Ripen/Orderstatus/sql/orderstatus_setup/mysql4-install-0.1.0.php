<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'status_upon_ordering', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'nullable' => true,
        'after'     => null,
        'comment' => 'Product attribute custom_stock_status text shown at the time of the order being placed.'
    ));

$installer->endSetup();