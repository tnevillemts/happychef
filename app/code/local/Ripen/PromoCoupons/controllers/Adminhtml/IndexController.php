<?php

class Ripen_PromoCoupons_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->_title('Manage Coupons');
        $this->loadLayout();
        $this->_setActiveMenu('promo/promocoupons');

        $this->_addContent($this->getLayout()->createBlock('promocoupons/adminhtml_coupon'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('promocoupons/adminhtml_coupon_grid')->toHtml()
        );
    }


    public function incrementAction()
    {
        $id = $this->getRequest()->getParam('id');
        $coupon = Mage::getModel('salesrule/coupon')->load($id);

        if ($coupon->getTimesUsed() < $coupon->getUsageLimit()){
            $coupon->setTimesUsed(($coupon->getTimesUsed()+1));
            $coupon->save();
        }

        $this->_redirect('*/*/index');

    }

    public function decrementAction()
    {
        $id = $this->getRequest()->getParam('id');
        $coupon = Mage::getModel('salesrule/coupon')->load($id);

        if ( $coupon->getTimesUsed() > 0 ) {
            $coupon->setTimesUsed(($coupon->getTimesUsed() - 1));
            $coupon->save();
        }

        $this->_redirect('*/*/index');

    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('promo/promocoupons');
    }

}
