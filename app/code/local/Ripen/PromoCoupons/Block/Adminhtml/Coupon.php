<?php

class Ripen_PromoCoupons_Block_Adminhtml_Coupon extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'promocoupons';
        $this->_controller = 'adminhtml_coupon';
        $this->_headerText = Mage::helper('promocoupons')->__('Coupon Lookup');

        parent::__construct();
        $this->_removeButton('add');
    }
}
