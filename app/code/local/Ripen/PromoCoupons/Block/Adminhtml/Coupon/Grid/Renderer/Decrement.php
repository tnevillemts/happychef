<?php

class Ripen_PromoCoupons_Block_Adminhtml_Coupon_Grid_Renderer_Decrement extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {
        if ($row->getData('times_used') > 0){
            return '<a href="'.$this->getUrl('*/*/decrement', array('id' => $row->getId())).'"
                onclick="return window.confirm(\'Are you sure you want to do this?\')">Decrement Used Count</a>';
        } else {
            return '-';
        }


    }
}
