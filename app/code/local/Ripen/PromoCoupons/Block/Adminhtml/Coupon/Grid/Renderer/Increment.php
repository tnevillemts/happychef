<?php

class Ripen_PromoCoupons_Block_Adminhtml_Coupon_Grid_Renderer_Increment extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {
        if ($row->getData('times_used') < $row->getData('usage_limit')){
            return '<a href="'.$this->getUrl('*/*/increment', array('id' => $row->getId())).'"
                onclick="return window.confirm(\'Are you sure you want to do this?\')">Increment Used Count</a>';
        } else {
            return '-';
        }


    }
}
