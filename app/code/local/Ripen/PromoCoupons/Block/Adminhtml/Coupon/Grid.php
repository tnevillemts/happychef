<?php

class Ripen_PromoCoupons_Block_Adminhtml_Coupon_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('promocoupons_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {

        $collection = Mage::getModel('salesrule/coupon')->getCollection();
        $collection->getSelect()
            ->joinLeft(
                array('scr' => $collection->getTable('salesrule/rule')),
                'main_table.rule_id = scr.rule_id',
                array('name')
            )
            // PROD-2123
            //->where('main_table.type = ?', 1)
            ->order('main_table.created_at DESC')
        ;

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('promocoupons');

        $this->addColumn('coupon', array(
            'header'       => $helper->__('Coupon Code'),
            'index'        => 'code',
        ));

        $this->addColumn('name', array(
            'header'       => $helper->__('Promo Name'),
            'index'        => 'name',
        ));


        $this->addColumn('created_on', array(
            'header' => $helper->__('Created On'),
            'type'   => 'datetime',
            'index'  => 'created_at'
        ));

        $this->addColumn('usage_limit', array(
            'header'       => $helper->__('Usage Limit'),
            'index'        => 'usage_limit'
        ));

        $this->addColumn('times_used', array(
            'header' => $helper->__('Times Used'),
            'index'  => 'times_used'
        ));

        $this->addColumn(
            'Increment',
            array(
                'header'    => $this->__('Increment'),
                'width'     => '150px',
                'type'      => 'action',
                'getter'    => 'getId',
                'filter'    => false,
                'sortable'  => false,
                'is_system' => true,
                'renderer'=> new Ripen_PromoCoupons_Block_Adminhtml_Coupon_Grid_Renderer_Increment()
            )
        );

        $this->addColumn(
            'Decrement',
            array(
                'header'    => $this->__('Decrement'),
                'width'     => '150px',
                'type'      => 'action',
                'getter'    => 'getId',
                'filter'    => false,
                'sortable'  => false,
                'is_system' => true,
                'renderer'=> new Ripen_PromoCoupons_Block_Adminhtml_Coupon_Grid_Renderer_Decrement()
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/promo_quote/edit/', array('id' => $row->getRuleId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}
