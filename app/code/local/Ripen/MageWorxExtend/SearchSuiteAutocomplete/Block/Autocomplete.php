<?php

include_once "MageWorx/SearchSuiteAutocomplete/Block/Autocomplete.php";

class Ripen_MageWorxExtend_SearchSuiteAutocomplete_Block_Autocomplete  extends MageWorx_SearchSuiteAutocomplete_Block_Autocomplete  {

    public function revisedForMageWorxToHtml() {

        if (Mage::getStoreConfig('advanced/modules_disable_output/' . $this->getModuleName())) {
            return '';
        }
        $html = $this->_loadCache();
        if ($html === false) {
            $translate = Mage::getSingleton('core/translate');

            if ($this->hasData('translate_inline')) {
                $translate->setTranslateInline($this->getData('translate_inline'));
            }

            $this->_beforeToHtml();
            $html = $this->_toHtml();
            $this->_saveCache($html);

            if ($this->hasData('translate_inline')) {
                $translate->setTranslateInline(true);
            }
        }
        $html = $this->_afterToHtml($html);

        if ($this->_frameOpenTag) {
            $html = '<' . $this->_frameOpenTag . '>' . $html . '<' . $this->_frameCloseTag . '>';
        }

        $transportObject = new Varien_Object;
        $transportObject->setHtml($html);
        $html = $transportObject->getHtml();

        return $html;
    }
}