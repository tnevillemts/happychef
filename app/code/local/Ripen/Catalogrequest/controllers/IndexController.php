<?php
class Ripen_Catalogrequest_IndexController extends Mage_Core_Controller_Front_Action
{
  
    public function indexAction()
    {
		$this->loadLayout();
        $this->getLayout()->getBlock('catalogrequest')->setFormAction( Mage::getUrl('*/*/post') );
        $this->getLayout()->getBlock('head')->setTitle('Happy Chef Catalog Request');
		$this->renderLayout();

    }
    public function postAction()
    {
        $parms = $this->getRequest()->getPost();

        $captcha = $parms['g-recaptcha-response'];
        if (!$captcha) {
            $jsonResponse = Mage::helper('core')->jsonEncode(array("error"=>1, "message"=>"Your captcha response could not be validated. Please try again."));
        }

        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LfRNj0UAAAAAD-rg5VaWAe-n5QVTjOf6vNtUxPQ&response=" . $captcha;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        
        $response = json_decode($response, true);
	// For development.
	// $response['success'] = true;
	// $response['error'] = 0;

        if ($response['success'] == false) {

            $jsonResponse = Mage::helper('core')->jsonEncode(array("error"=>1, "message"=>"Your captcha response could not be validated. Please try again."));

        } else {


             if($this->getRequest()->isPost()){
                  $request = Mage::getModel('catalogrequest/catalogrequest');
                  $data['time_added'] = now();
                  $data['ip'] = $_SERVER['REMOTE_ADDR'];
                  $data['first_name'] = ucwords($parms['first_name']);
                  $data['last_name'] = ucwords($parms['last_name']);
                  $data['company'] = $parms['company'];
                  $data['title'] = $parms['title'];
                  $data['address1'] = ucwords(str_replace(",", " ",$parms['address1']));
                  $data['address2'] = ucwords(str_replace(",", " ",$parms['address2']));
                  $data['city'] = ucwords($parms['city']);
                  $data['state'] = $parms['State'];
                  $data['zip'] = $parms['Zip'];
                  $data['country'] = $parms['Country'];
                  $data['email'] = $parms['cat-email'];
                  $data['send_offers'] = $parms['send_offers'];
                  $data['hostname'] = $_SERVER["HTTP_HOST"];
                  $data['phone'] = $parms['phone'];
                  $data['fax'] = $parms['fax'];

                

                  // Validate
                  if(!$errors = $request->validate($data)){
                      MAGE::getSingleton('core/session')->addError($errors);
                  }
      
                  // Add to database
                  try {
                      $request->setData($data)->save();

                      //Add to newsletter subscription list
                      if($data['send_offers']) {

                          $ownerId = Mage::getModel('customer/customer')
                              ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                              ->loadByEmail($data['email'])
                              ->getId();

                          if ($ownerId === null) {
                              $status = Mage::getModel('newsletter/subscriber')->subscribe($data['email']);
                          }

                      }

                      $jsonResponse = Mage::helper('core')->jsonEncode(array("error"=>0, "url"=> Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'thank-you'));

                  } catch(Exception $e){

                      $jsonResponse = Mage::helper('core')->jsonEncode(array("error"=>1, "url"=>"Sorry, we've had some trouble saving your request. Please try again."));

                  }
      
              }
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonResponse);

        //return $jsonResponse;


    }
}
