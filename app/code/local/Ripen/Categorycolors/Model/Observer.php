<?php
class Ripen_Categorycolors_Model_Observer {


    /**
     * Flag to stop observer executing more than once
     *
     * @var static bool
     */
    static protected $_singletonFlag = false;

    /**
     * This method will run when the product is saved from the Magento Admin
     * Use this function to update the product model, process the
     * data or anything you like
     *
     * @param Varien_Event_Observer $observer
     */
    public function saveProductTabData(Varien_Event_Observer $observer)
    {
        if (!self::$_singletonFlag) {
            self::$_singletonFlag = true;

            $product = $observer->getEvent()->getProduct();

            try {

                $data =  $this->_getRequest()->getPost('product');
		        $regProduct = Mage::registry('product');
                $resource = Mage::getSingleton('core/resource');

		        if(!is_null($regProduct)) {
			        $productCategories = $regProduct->getCategoryCollection();
			        foreach($productCategories as $category) {
                        $key = "categorycolor_" . $category->getId();
                        if ($key != 'current_image_color') {
                            $writeConnection = $resource->getConnection('core_write');
                            $table = $resource->getTableName('catalog/category_product');

                            $query = "UPDATE {$table} SET color_id = '{$data[$key]}'
				              WHERE category_id = '{$category->getId()}' AND product_id = '{$product->getId()}'";
                            $writeConnection->query($query);
                        }
			        }
		        }

                $readConnection = $resource->getConnection('core_read');
                $query = "SELECT attribute_id FROM mag_eav_attribute WHERE entity_type_id = 4 AND attribute_code = 'default_image_color'";
                $attributeId = $readConnection->fetchOne($query);
                if(!empty($attributeId)) {
                    $query = "SELECT value_id FROM mag_catalog_product_entity_varchar WHERE entity_type_id = 4 AND attribute_id = " . $attributeId . " AND entity_id = " . $product->getId() . " ";
                    $valueId = $readConnection->fetchOne($query);
                    if(!empty($valueId) && !empty($data['current_image_color'])) {
                        $query = "UPDATE mag_catalog_product_entity_varchar SET value = " . $data['current_image_color'] . " WHERE value_id = " . $valueId . " ";
                        $writeConnection->query($query);
                    }
                }
                
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
    }

    /**
     * Retrieve the product model
     *
     * @return Mage_Catalog_Model_Product $product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Shortcut to getRequest
     *
     */
    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }

}
