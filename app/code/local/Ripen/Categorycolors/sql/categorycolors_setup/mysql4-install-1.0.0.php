<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('catalog/category_product'),'color_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => true,
        'length'    => 11,
        'after'     => null, // column name to insert new column after
        'comment'   => 'ID for the color attribute'
    ));

$installer->endSetup();
