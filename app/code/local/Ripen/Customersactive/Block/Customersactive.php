<?php
class Ripen_Customersactive_Block_Customersactive extends Mage_Core_Block_Template {

	public function _prepareLayout() {
    		return parent::_prepareLayout();
	}

	public function getCustomersactive() {
    		if (!$this->hasData('customersactive')) {
        		$this->setData('customersactive', Mage::registry('customersactive'));
    	}
    	return $this->getData('customersactive');
	} 
}
