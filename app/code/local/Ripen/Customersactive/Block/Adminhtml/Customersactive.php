<?php
class Ripen_Customersactive_Block_Adminhtml_Customersactive extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_customersactive';
    $this->_blockGroup = 'customersactive';
    $this->_headerText = Mage::helper('customersactive')->__('Customers Active Report');    
    parent::__construct();
    $this->_removeButton('add');
  }
}
