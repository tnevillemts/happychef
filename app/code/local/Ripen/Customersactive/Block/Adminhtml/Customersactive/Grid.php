<?php

class Ripen_Customersactive_Block_Adminhtml_Customersactive_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('customersactiveGrid');
      $this->setDefaultSort('created_at');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
      $this->setSubReportSize(false);
      $this->setPagerVisibility(false);
      $this->setDateFilterVisibility(true);
      $this->setExportVisibility(false);
  }

  private function dateReformat($string) 
  {
	$pieces = explode("/", $string);
	return $pieces[2] . '-' . $pieces[0] . '-' . $pieces[1];
  }

  protected function _prepareCollection()
  {
      date_default_timezone_set('America/New_York');
      if(empty($_COOKIE['rep_period_date_from'])) {
      	  $fromDate  = date('Y-m-d H:i:s', strtotime($toDateS . ' - 1 month'));
      } else {
	  $fromDateString = $this->dateReformat($_COOKIE['rep_period_date_from']) . ' 00:00:00';
	  $fromDate  = date('Y-m-d H:i:s', strtotime($fromDateString));
      }

      if(empty($_COOKIE['rep_period_date_to'])) {
	  $toDate = date("Y-m-d H:i:s");
      } else {
	  $toDateString = $this->dateReformat($_COOKIE['rep_period_date_to']) . ' 00:00:00';
	  $toDate  = date('Y-m-d H:i:s', strtotime($toDateString));
      }

      $customerCount     = 0;
      $lastCustomerEmail = '';
      $uniqueCustomers   = 0;
      $customerMetrics   = array();
      $reportCollection  = new Varien_Data_Collection();

      $orders = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToSort('customer_email', 'ASC')
                ->addAttributeToSort('created_at', 'ASC')
                ->addAttributeToFilter('created_at', array('from' => $fromDate, 'to' => $toDate))
                ->addAttributeToFilter('status', array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE));

      if($orders->count() > 0) {
              foreach($orders as $order) {
                      $customerEmail = $order->getCustomerEmail();
                      if(! array_key_exists($customerEmail, $customerMetrics)) {
                              $customerMetrics[$customerEmail] = array();
                              $customerMetrics[$customerEmail]['cumgrandtotal'] = $order->getGrandTotal();
                              $customerMetrics[$customerEmail]['number']     = 1;
                              $uniqueCustomers++;
                      } else {
                              $customerMetrics[$customerEmail]['cumgrandtotal'] += $order->getGrandTotal();
                              $customerMetrics[$customerEmail]['number']     += 1;
                      }
                      $lastCustomerEmail = $customerEmail;
              }
      } else {
	      Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " No Data ", null, 'customersactive.log');
      }

      $bucketCustomers  = array(
             "Over $1,000"           => 0,
             "$500 to $999.99"       => 0,
             "$200 to $499.99"       => 0,
             "$100 to $199.99"       => 0,
             "$50 to $99.99"         => 0,
             "$49.99 and below"      => 0,
             "Total"                 => 0,
      );
      $bucketRevenue  = array(
             "Over $1,000"           => 0,
             "$500 to $999.99"       => 0,
             "$200 to $499.99"       => 0,
             "$100 to $199.99"       => 0,
             "$50 to $99.99"         => 0,
             "$49.99 and below"      => 0,
             "Total"                 => 0,
      );
      $bucketOrders  = array(
             "Over $1,000"           => 0,
             "$500 to $999.99"       => 0,
             "$200 to $499.99"       => 0,
             "$100 to $199.99"       => 0,
             "$50 to $99.99"         => 0,
             "$49.99 and below"      => 0,
             "Total"                 => 0,
      );

      foreach($customerMetrics as $customerMetric) {
              $bucketCustomers['Total'] += 1;
              $bucketOrders['Total']    += $customerMetric['number'];
              $bucketRevenue['Total']   += $customerMetric['cumgrandtotal'];

              if($customerMetric['cumgrandtotal'] < 49.99 && $customerMetric['cumgrandtotal'] > 0) {
		      $thisKey = '$49.99 and below';
                      $bucketCustomers[$thisKey]  += 1;
                      $bucketOrders[$thisKey]     += $customerMetric['number'];
                      $bucketRevenue[$thisKey]    += $customerMetric['cumgrandtotal'];
              } else if ($customerMetric['cumgrandtotal'] < 99.99 && $customerMetric['cumgrandtotal'] > 50) {
		      $thisKey = '$50 to $99.99';
                      $bucketCustomers[$thisKey]  += 1;
                      $bucketOrders[$thisKey]     += $customerMetric['number'];
                      $bucketRevenue[$thisKey]    += $customerMetric['cumgrandtotal'];
              } else if ($customerMetric['cumgrandtotal'] < 199.99 && $customerMetric['cumgrandtotal'] > 100) {
		      $thisKey = '$100 to $199.99';
                      $bucketCustomers[$thisKey]  += 1;
                      $bucketOrders[$thisKey]     += $customerMetric['number'];
                      $bucketRevenue[$thisKey]    += $customerMetric['cumgrandtotal'];
              } else if ($customerMetric['cumgrandtotal'] < 499.99 && $customerMetric['cumgrandtotal'] > 200) {
		      $thisKey = '$200 to $499.99';
                      $bucketCustomers[$thisKey]  += 1;
                      $bucketOrders[$thisKey]     += $customerMetric['number'];
                      $bucketRevenue[$thisKey]    += $customerMetric['cumgrandtotal'];
              } else if ($customerMetric['cumgrandtotal'] < 999.99 && $customerMetric['cumgrandtotal'] > 500) {
		      $thisKey = '$500 to $999.99';
                      $bucketCustomers[$thisKey]  += 1;
                      $bucketOrders[$thisKey]     += $customerMetric['number'];
                      $bucketRevenue[$thisKey]    += $customerMetric['cumgrandtotal'];
              } else if ($customerMetric['cumgrandtotal'] > 1000) {
		      $thisKey = 'Over $1,000';
                      $bucketCustomers[$thisKey]  += 1;
                      $bucketOrders[$thisKey]     += $customerMetric['number'];
                      $bucketRevenue[$thisKey]    += $customerMetric['cumgrandtotal'];
              }
      }

      foreach($bucketOrders as $key => $value) {
	      $varObj = new Varien_Object();
      	      $varObj->setValuebucket($key);
	      $varObj->setFrom($fromDate);
      	      $varObj->setTo($toDate);
              $varObj->setNumberoforders($bucketOrders[$key]);
              $varObj->setNumberofcustomers($bucketCustomers[$key]);
              $varObj->setTotalrevenue($bucketRevenue[$key]);
              $reportCollection->addItem($varObj);
      }

      $this->setCollection($reportCollection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $store = Mage::app()->getStore();

      $this->addColumn('valuebucket', array(
          'header'    => Mage::helper('customersactive')->__('Order Value Range'),
          'align'     => 'left',
          'width'     => '10%',
          'filter'    => false,
          'sortable'  => false,
          'index'     => 'valuebucket',
      ));


      $this->addColumn('from', array(
          'header' => Mage::helper('adminhtml')->__('From'),
          'align' => 'left',
	  'width' => '21%',
	  'filter'    => false,
          'type' => 'datetime',
          //'format' => 'Y-m-d H:i:s',
          'index' => 'from',
      ));

      $this->addColumn('to', array(
          'header' => Mage::helper('adminhtml')->__('To'),
          'align' => 'left',
	  'width' => '21%',
	  'filter'    => false,
          'type' => 'datetime',
          //'format' => 'Y-m-d H:i:s',
          'index' => 'to',
      ));

      $this->addColumn('numberofcustomers', array(
          'header'    => Mage::helper('customersactive')->__('Unique Customers'),
          'align'     => 'right',
          'width'     => '15%',
	  'filter'    => false,
          'sortable'  => false,
          'index'     => 'numberofcustomers',
      ));

      $this->addColumn('numberoforders', array(
          'header'    => Mage::helper('customersactive')->__('Total Order For Customers'),
          'align'     => 'right',
          'width'     => '16%',
	  'filter'    => false,
          'sortable'  => false,
          'index'     => 'numberoforders',
      ));

      $this->addColumn('totalrevenue', array(
          'header'    => Mage::helper('customersactive')->__('Total Revenue For Customers'),
          'align'     =>'left',
          'width'     => '16%',
	  'filter'    => false,
          'sortable'  => false,
          'index'     => 'totalrevenue',
	  'type'      => 'price',
	  'currency_code' => $store->getBaseCurrency()->getCode()
      ));
      
      $this->addExportType('*/*/exportCsv', Mage::helper('customersactive')->__('CSV'));
      $this->addExportType('*/*/exportXml', Mage::helper('customersactive')->__('XML'));
	  
      return parent::_prepareColumns();
  }   

  public function getRowUrl($row)
  {
      return "#";
  }

}
