<?php
/**
 * Ripen override
 */

/**
 * Adminhtml customer grid block
 *
 */
class Ripen_Coreextended_Adminhtml_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $collection->getSelect()->joinLeft(array('sfog' => 'mag_sales_flat_order_grid'), 'main_table.entity_id = sfog.entity_id', array('sfog.shipping_name', 'sfog.billing_name'));
        $collection->getSelect()->joinLeft(
            array(
                'sfo' => 'mag_sales_flat_order'),
                'sfo.entity_id=main_table.entity_id',
                array('sfo.customer_email', 'sfo.increment_id', 'sfo.store_id', 'sfo.created_at', 'sfo.status', 'sfo.base_grand_total', 'sfo.grand_total')
        );
        $collection->getSelect()->joinLeft(array('sfoa' => 'mag_sales_flat_order_address'), 'main_table.entity_id = sfoa.parent_id AND sfoa.address_type="shipping"', array('sfoa.region', 'sfoa.city'));
        $this->setCollection($collection);

        return call_user_func(array(get_parent_class(get_parent_class($this)), '_prepareCollection'));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('real_order_id', array(
            'header' => Mage::helper('sales')->__('Order #'),
            'width' => '70px',
            'type' => 'text',
            'index' => 'increment_id',
            'filter_index' => 'sfo.increment_id'
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => Mage::helper('sales')->__('Purchased From (Store)'),
                'width' => '110px',
                'index' => 'store_id',
                'type' => 'store',
                'store_view' => true,
                'display_deleted' => true,
                'filter_index' => 'sfo.store_id'
            ));
        }

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '90px',
            'filter_index' => 'sfo.created_at'
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'width' => '180px',
            'index' => 'billing_name',
            'filter_index' => 'sfog.billing_name'
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'width' => '180px',
            'index' => 'shipping_name',
            'filter_index' => 'sfog.shipping_name'
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('sales')->__('Customer Email'),
            'index' => 'customer_email',
            'filter_index' => 'sfo.customer_email',
            'width' => '50px',
        ));

        $this->addColumn('city', array(
            'header' => Mage::helper('sales')->__('Shipping City'),
            'index' => 'city',
            'filter_index' => 'sfoa.city',
            'width' => '50px',
        ));


        $this->addColumn('region', array(
            'header' => Mage::helper('sales')->__('Shipping State'),
            'index' => 'region',
            'filter_index' => 'sfoa.region',
            'width' => '50px',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'width' => '20px',
            'index' => 'base_grand_total',
            'filter_index' => 'sfo.base_grand_total',
            'type' => 'currency',
            'currency' => 'base_currency_code'
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'width' => '60px',
            'index' => 'grand_total',
            'filter_index' => 'sfo.grand_total',
            'type' => 'currency',
            'currency' => 'order_currency_code'
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'width' => '70px',
            'filter_index' => 'sfo.status',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action', array(
                'header' => Mage::helper('sales')->__('Action'),
                'width' => '50px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('sales')->__('View'),
                        'url' => array('base' => '*/sales_order/view'),
                        'field' => 'order_id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));
        }

        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

	    return call_user_func(array(get_parent_class(get_parent_class($this)), '_prepareColumns'));
    }
}

