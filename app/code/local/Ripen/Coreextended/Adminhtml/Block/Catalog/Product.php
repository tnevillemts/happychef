<?php
class Ripen_Coreextended_Adminhtml_Block_Catalog_Product extends Mage_Adminhtml_Block_Catalog_Product
{

	protected function _prepareLayout() {

		/** Add button for exporting visible products */

		$this->_addButton('export_visible', array(
			'label'   => Mage::helper('catalog')->__('Export Visible Products'),
			'onclick' => "setLocation('{$this->getUrl('*/*/exportvisible')}')",
			'class'   => 'go'
		));

		return parent::_prepareLayout();

	}

}
