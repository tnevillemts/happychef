<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_CatalogSearch
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog search helper
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Ripen_Coreextended_Helper_CatalogSearch_Data extends Mage_CatalogSearch_Helper_Data
{

    protected function _construct()
    {
        parent::_construct();
    }

    public function _getQuery( $catchAll = FALSE)
    {


        $q = !$catchAll ? $this->getQueryText() : $this->getQueryCatchAll();

        $this->_query = Mage::getModel('catalogsearch/query')->loadByQuery($q);

        if (!$this->_query->getId()) {
            $this->_query->setQueryText($q);
        }

        return $this->_query;
    }

    public function getQueryText()
    {
        if (!isset($this->_queryText)) {

            $queryText = $this->_getRequest()->getParam($this->getQueryParamName());

            $overrides = $this->_getOverrides();
            foreach ($overrides as $original => $override) {

                if (strpos(strtolower($queryText), $original) !== false) {
                    $queryText = str_replace($original, $override, $queryText);
                    break;
                }
            }

            $skus = $this->_getSkus();

            foreach($skus as $sku){

                if ( strlen($sku) && strpos(strtolower($queryText), $sku) !== false && strcspn($queryText, '0123456789') != strlen($queryText)) {

                    $queryText2 = $sku;

                    /*
                    $newQueryText = str_replace($queryText2, "[".$queryText2."]", $queryText);

                    $query = Mage::getModel('catalogsearch/query')->loadByQuery($newQueryText);
                    if (!$query->getId()) {
                        $query = Mage::helper('catalogsearch')->_getQuery();
                        $query->setStoreId(Mage::app()->getStore()->getId());
                        $query->setQueryText($newQueryText);
                    }

                    if($queryText != $newQueryText) {
                        $query->setPopularity($query->getPopularity() + 1);
                    }

                    $query->save();
                    */
                    $queryText = $queryText2;

                    break;
                }
            };

            $this->_queryText = $queryText;

            if ($this->_queryText === null) {
                $this->_queryText = '';
            } else {
                /* @var $stringHelper Mage_Core_Helper_String */
                $stringHelper = Mage::helper('core/string');
                $this->_queryText = is_array($this->_queryText) ? ''
                    : $stringHelper->cleanString(trim($this->_queryText));

                $maxQueryLength = $this->getMaxQueryLength();
                if ($maxQueryLength !== '' && $stringHelper->strlen($this->_queryText) > $maxQueryLength) {
                    $this->_queryText = $stringHelper->substr($this->_queryText, 0, $maxQueryLength);
                    $this->_isMaxLength = true;
                }
            }
        }
        return $this->_queryText;
    }

    public function getQueryCatchAll()
    {

            $queryText = $this->_getRequest()->getParam($this->getQueryParamName());

            $catchalls = $this->_getCatchalls();

            foreach ($catchalls as $index => $catchall) {
                if (strpos(strtolower($queryText), $catchall) !== false) {
                    $queryText = $catchall;
                    break;
                }
            }

            $this->_queryText = $queryText;

        return $this->_queryText;
    }

    protected function _getSkus() {

        $filename = Mage::getBaseDir() . '/var/cache/indexModels';

        /**
         * Generate a fresh indexModel file every day. This files just holds an array with product SKUs.
         */

        $timeElapsed = time() - filemtime($filename);
        $generateFrequency = 24; // in hours
        $hoursPassed = round(abs($timeElapsed) / 60 / 60, 2);

        if( file_exists($filename) && filesize($filename) && $hoursPassed < $generateFrequency) {

            $indexes = file_get_contents($filename);

        } else {

            $productsCollection = Mage::getModel('catalog/product')->getCollection();
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($productsCollection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($productsCollection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($productsCollection);
            $productsCollection->addAttributeToSelect("model");
            $productsCollection->addAttributeToSelect("sku");

            foreach ($productsCollection as $product) {
                if ($product->getModel())
                    $skus[] = strtolower($product->getModel());
            }

            $skus = array_unique($skus);

            usort($skus, function ($a, $b) {
                return strlen($b) - strlen($a);
            });

            $indexes = "";
            foreach ($skus as $sku) {
                $indexes .= "{$sku}\n";
            }

            file_put_contents($filename, $indexes);
        }

        $skus = explode("\n", $indexes);
        return $skus;
    }

    protected function _getOverrides() {

        $filename = Mage::getBaseDir() . '/var/cache/indexOverrides';
        if( file_exists($filename) && filesize($filename)) {

            $index = file_get_contents($filename);

        } else {

            $overrides = array(
                "size" => "size chart",
                "sizing" => "size chart",
                "custom" => "customization",
                "embroidery" => "customization",
                "logo" => "customization",
                "personalized" => "customization",
                "flag" => "customization",
                "ribbon" => "customization",
                "shipping" => "shipping & delivery",
                "clearance" => "sale",
                "clearence" => "sale",
                "return" => "returns & exchanges",
                "tracking" => "order status",
                "catalog" => "request a catalog",
                "gift certificate" => "gift cards",
                "address" => "contact us",
                "woman" => "women",
                "ladies" => "women",
                "skirts" => "women",
                "maternity" => "women",
                "child" => "kids",
                "youth" => "kids",
                "toddler" => "kids",
                "knives" => "knife",
                "wusthof" => "knife",
                "mundial" => "knife",
                "knife bag" => "knife roll",
                "jacket" => "coat",
                "tshirt" => "t-shirt",
                "headwear" => "hat",
                "dickies" => "pant",
                "smock" => "apron",
                "crocs" => "clog",
                "dansko" => "clog",
                "bowtie" => "bow tie",
                "footwear" => "shoe",
                "waitress" => "women",
                "assymetrical" => "symmetrical",
                "cook cool" => "cookcool",
                "executive" => "executive coat",
                "cutlery set" => "knife set",
                "uniform" => "??",
                "waitress dress" => "women"
            );

            asort($overrides);
            $index = serialize($overrides);
            file_put_contents($filename, $index);
        }

        $overrides = unserialize($index);
        return $overrides;
    }

    protected function _getCatchalls() {

        $filename = Mage::getBaseDir() . '/var/cache/indexCatchalls';
        if( file_exists($filename) && filesize($filename)) {

            $index = file_get_contents($filename);

        } else {

            $catchalls = array(
                10=>"coat",
                20=>"pant",
                30=>"hat",
                40=>"cap",
                50=>"shoe",
                60=>"shirt",
                70=>"knife",
                80=>"t-shirt",
                90=>"cobbler",
                100=>"shoes",
                110=>"shirt",
                120=>"towel",
                130=>"sushi",
                140=>"polo",
                150=>"smart",
                160=>"cookcool"
            );

            ksort($catchalls);

            $index = serialize($catchalls);
            file_put_contents($filename, $index);
        }

        $catchalls = unserialize($index);
        return $catchalls;
    }

}
