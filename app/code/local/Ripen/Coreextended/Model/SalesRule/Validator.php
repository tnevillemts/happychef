<?php

class Ripen_Coreextended_Model_SalesRule_Validator extends Mage_SalesRule_Model_Validator
{
    protected function _canProcessRule($rule, $address)
    {
        if ($rule->hasIsValidForAddress($address) && !$address->isObjectNew()) {
            return $rule->getIsValidForAddress($address);
        }

        /**
         * Check of the awafptc rules.
         */
        $allQuoteItems = $address->getQuote()->getAllItems();
        $awafptcRuleCollection = Mage::getModel('awafptc/rule')->getCollection();
        foreach ($allQuoteItems as $item) {
            foreach($awafptcRuleCollection as $awafptcRule) {
                $awafptcThisRule = $awafptcRule;
                if($awafptcRule->getData("product_id") == $item->getData("product_id")) {
                    $awafptcRuleStartDate = DateTime::createFromFormat('Y-m-d H:i:s', $awafptcRule->getData('start_date'));
                    $awafptcRuleEndDate = DateTime::createFromFormat('Y-m-d H:i:s', $awafptcRule->getData('end_date'));

                    if(!$awafptcRuleStartDate || !$awafptcRuleEndDate) {
                        Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . " Could not parse the awafptc rule date(s).", null, 'system.log');
                    } else {
                        $currentDateDate = new DateTime();
                        $currentDateDate->format('Y-m-d H:i:s');
                        if ($awafptcRuleStartDate < $currentDateDate && $awafptcRuleEndDate > $currentDateDate) {
                            if (($awafptcRule->getData('priority') != $rule->getData('sort_order')) && $awafptcRule->getData('stop_rules_processing') == '1') {
                                return false;
                            }
                        }
                    }
                }
            }
        }


        /**
         * check per coupon usage limit
         */
        if ($rule->getCouponType() != Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON) {
            $couponCode = $address->getQuote()->getCouponCode();
            if (strlen($couponCode)) {
                $coupon = Mage::getModel('salesrule/coupon');
                $coupon->load($couponCode, 'code');
                if ($coupon->getId()) {
                    // check entire usage limit
                    if ($coupon->getUsageLimit() && $coupon->getTimesUsed() >= $coupon->getUsageLimit()) {
                        $rule->setIsValidForAddress($address, false);
                        return false;
                    }
                    // check per customer usage limit
                    $customerId = $address->getQuote()->getCustomerId();
                    if ($customerId && $coupon->getUsagePerCustomer()) {
                        $couponUsage = new Varien_Object();
                        Mage::getResourceModel('salesrule/coupon_usage')->loadByCustomerCoupon($couponUsage, $customerId, $coupon->getId());
                        if ($couponUsage->getCouponId() && $couponUsage->getTimesUsed() >= $coupon->getUsagePerCustomer()) {
                            $rule->setIsValidForAddress($address, false);
                            return false;
                        }
                    }
                }
            }
        }

        /**
         * check per rule usage limit
         */
        $ruleId = $rule->getId();
        if ($ruleId && $rule->getUsesPerCustomer()) {
            $customerId     = $address->getQuote()->getCustomerId();
            $ruleCustomer   = Mage::getModel('salesrule/rule_customer');
            $ruleCustomer->loadByCustomerRule($customerId, $ruleId);
            if ($ruleCustomer->getId()) {
                if ($ruleCustomer->getTimesUsed() >= $rule->getUsesPerCustomer()) {
                    $rule->setIsValidForAddress($address, false);
                    return false;
                }
            }
        }
        $rule->afterLoad();
        /**
         * quote does not meet rule's conditions
         */
        if (!$rule->validate($address)) {
            $rule->setIsValidForAddress($address, false);
            return false;
        }
        /**
         * passed all validations, remember to be valid
         */
        $rule->setIsValidForAddress($address, true);
        return true;
    }

}
