<?php

class Ripen_Coreextended_Model_Catalog_Product extends Ripen_Embroidery_Model_Catalog_Product
{
    protected function _construct()
    {
        parent::_construct();
    }

    private function clearToIndex() { 
        try {
	    $databaseName = Mage::getConfig()->getResourceConnectionConfig('default_setup')->dbname;
	    $sql = "SELECT feed_mutex FROM " . $databaseName . "." . Mage::getConfig()->getTablePrefix() . "feed_control WHERE feed_name = 'IN_PRODUCT_SYNC'";
	    $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
	    $mutexFlag = $connection->fetchOne($sql);
	    if($mutexFlag != '1') {
		return TRUE;
	    } 
	} catch (Exception $e) {
		Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . "Exception: " . $e->getMessage() . "", null, 'indexProductOnSave.log');
        }
	Mage::log("File: " . __FILE__ . "Function: " . __FUNCTION__ . " Line: " . __LINE__ . "Info: FALSE ", null, 'indexProductOnSave.log');
	return FALSE;
    }

    public function afterCommitCallback()
    {
        //parent::afterCommitCallback();
	call_user_func(array(get_parent_class(get_parent_class(get_parent_class($this))), 'afterCommitCallback'));

	if($this->clearToIndex()) {
            /** @var \Mage_Index_Model_Indexer $indexer */
            $indexer = Mage::getSingleton('index/indexer');
            $indexer->processEntityAction($this, self::ENTITY, Mage_Index_Model_Event::TYPE_SAVE);
	}

        return $this;
    }

    function getShortDescription(){
        $processor = Mage::getModel('core/email_template_filter');
        $html = $processor->filter($this->getData('short_description'));
        return $html;
    }
}
