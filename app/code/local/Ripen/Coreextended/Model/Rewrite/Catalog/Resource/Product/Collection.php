<?php
/**
 * Magento
 *
 */
class Ripen_Coreextended_Model_Rewrite_Catalog_Resource_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    protected function _construct()
    {
        parent::_construct();
    }

    protected function _preparePriceExpressionParameters($select)
    {
	// prepare response object for event
        $response = new Varien_Object();
        $response->setAdditionalCalculations(array());
        $tableAliases = array_keys($select->getPart(Zend_Db_Select::FROM));
        if (in_array(self::INDEX_TABLE_ALIAS, $tableAliases)) {
            $table = self::INDEX_TABLE_ALIAS;
        } else {
            $table = reset($tableAliases);
        }

        // prepare event arguments
        $eventArgs = array(
            'select'          => $select,
            'table'           => $table,
            'store_id'        => $this->getStoreId(),
            'response_object' => $response
        );

        Mage::dispatchEvent('catalog_prepare_price_select', $eventArgs);

        $additional   = join('', $response->getAdditionalCalculations());

	// Override is for this one line.
        //$this->_priceExpression = $table . '.min_price';
        $this->_priceExpression = $table . '.tier_price';

        $this->_additionalPriceExpression = $additional;
        $this->_catalogPreparePriceSelect = clone $select;

        return $this;
    }
}
