<?php

class Ripen_Coreextended_Model_Sales_Quote_Address_Total_Shipping extends Mage_Sales_Model_Quote_Address_Total_Shipping
{
    protected function _construct()
    {
        parent::_construct();
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getShippingAmount();
        if ($amount != 0 || $address->getShippingDescription()) {
            $title = Mage::helper('sales')->__('Shipping & Handling');
            if ($address->getShippingDescription()) {
                $methodDescription = explode(" - ", $address->getShippingDescription());
                if (count($methodDescription) > 1)
                    $shippingDescription = $methodDescription[1];
                else
                    $shippingDescription = $address->getShippingDescription();

                $title .= ' (' . $shippingDescription . ' shipping)';
            }
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => $address->getShippingAmount()
            ));
        }
        return $this;
    }

}
