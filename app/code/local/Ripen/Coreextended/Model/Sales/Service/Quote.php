<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Quote submit service model
 */

class Ripen_Coreextended_Model_Sales_Service_Quote extends Mage_Sales_Model_Service_Quote
{

    protected function _construct()
    {
        parent::_construct();
    }

    public function submitOrder()
    {

        $this->_deleteNominalItems();
        $this->_validate();
        $quote = $this->_quote;
        $isVirtual = $quote->isVirtual();

        $transaction = Mage::getModel('core/resource_transaction');
        if ($quote->getCustomerId()) {
            $transaction->addObject($quote->getCustomer());
        }
        $transaction->addObject($quote);

        $quote->reserveOrderId();
        if ($isVirtual) {
            $order = $this->_convertor->addressToOrder($quote->getBillingAddress());
        } else {
            $order = $this->_convertor->addressToOrder($quote->getShippingAddress());
        }
        $order->setBillingAddress($this->_convertor->addressToOrderAddress($quote->getBillingAddress()));
        if ($quote->getBillingAddress()->getCustomerAddress()) {
            $order->getBillingAddress()->setCustomerAddress($quote->getBillingAddress()->getCustomerAddress());
        }
        if (!$isVirtual) {
            $order->setShippingAddress($this->_convertor->addressToOrderAddress($quote->getShippingAddress()));
            if ($quote->getShippingAddress()->getCustomerAddress()) {
                $order->getShippingAddress()->setCustomerAddress($quote->getShippingAddress()->getCustomerAddress());
            }
        }
        $order->setPayment($this->_convertor->paymentToOrderPayment($quote->getPayment()));

        foreach ($this->_orderData as $key => $value) {
            $order->setData($key, $value);
        }

        foreach ($quote->getAllItems() as $item) {
            $orderItem = $this->_convertor->itemToOrderItem($item);
            if ($item->getParentItem()) {
                $orderItem->setParentItem($order->getItemByQuoteItemId($item->getParentItem()->getId()));
            }
            $order->addItem($orderItem);
        }

        $order->setQuote($quote);

        $transaction->addObject($order);
        $transaction->addCommitCallback(array($order, 'place'));
        $transaction->addCommitCallback(array($order, 'save'));

        /**
         * We can use configuration data for declare new order status
         */
        Mage::dispatchEvent('checkout_type_onepage_save_order', array('order'=>$order, 'quote'=>$quote));
        Mage::dispatchEvent('sales_model_service_quote_submit_before', array('order'=>$order, 'quote'=>$quote));
        try {
            $transaction->save();
            $this->_inactivateQuote();
            Mage::dispatchEvent('sales_model_service_quote_submit_success', array('order'=>$order, 'quote'=>$quote));
        } catch (Exception $e) {

            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                // reset customer ID's on exception, because customer not saved
                $quote->getCustomer()->setId(null);
            }

            //reset order ID's on exception, because order not saved
            $order->setId(null);
            /** @var $item Mage_Sales_Model_Order_Item */
            foreach ($order->getItemsCollection() as $item) {
                $item->setOrderId(null);
                $item->setItemId(null);
            }

            Mage::dispatchEvent('sales_model_service_quote_submit_failure', array('order'=>$order, 'quote'=>$quote));
            throw $e;
        }
        Mage::dispatchEvent('sales_model_service_quote_submit_after', array('order'=>$order, 'quote'=>$quote));
        $this->_order = $order;

        $sendEmail = false;

        if (Mage::getSingleton('core/session')->getEmailIntegrityCheck() && Mage::getSingleton('core/session')->getEmailIntegrityCheck() != $order->getCustomerEmail()) {

            $message = "Email mismatch alert. Customer checked out as a guest and intended to place an order with [".Mage::getSingleton('core/session')->getEmailIntegrityCheck()."], however the completed order had a different email assigned to it [{$order->getCustomerEmail()}]. Check order #{$order->getEntityId()} for data integrity.";
            $sendEmail = true;

        } else if ((Mage::getSingleton('customer/session')->getCustomer() && Mage::getSingleton('customer/session')->getCustomer()->getEmail() && Mage::getSingleton('customer/session')->getCustomer()->getEmail() != $order->getCustomerEmail())){

            $message = "Email mismatch alert. Signed in customer intended to place an order with [".Mage::getSingleton('customer/session')->getCustomer()->getEmail()."], however the completed order had a different email assigned to it [{$order->getCustomerEmail()}]. Check order #{$order->getEntityId()} for data integrity.";
            $sendEmail = true;
        }

        if ( $sendEmail ){
            $templateId = "Dev";
            $emailTemplate = Mage::getModel('core/email_template')->loadByCode($templateId);
            $vars = array("message" => $message);
            $emailTemplate->getProcessedTemplate($vars);

            $emailTemplate->setSenderName('Ripen Tech Team');
            $emailTemplate->setSenderEmail('dev@ri.pn');
            try {
                $emailTemplate->send('mmedvedev@ri.pn', 'Ripen Tech', $vars);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        return $order;
    }

}
