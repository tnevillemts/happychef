<?php

class Ripen_Coreextended_Model_Sales_Order extends Mage_Sales_Model_Order
{
    protected function _construct()
    {
        parent::_construct();
    }

    public function spellCcType ($ccType){

        switch ($ccType){
            case "AE":
                $type = "American Express";
                break;

            case "VI":
                $type = "Visa";
                break;

            case "MC":
                $type = "Mastercard";
                break;

            case "DI":
                $type = "Discover";
                break;

            default:
                $type = "CC";
                break;
        }
        return $type;
    }


    public function queueNewOrderEmail($forceMode = false)
    {
        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            return $this;
        }

        $emailSentAttributeValue = $this->load($this->getId())->getData('email_sent');
        $this->setEmailSent((bool)$emailSentAttributeValue);
        if ($this->getEmailSent()) {
            //return $this;
        }

        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        $arr = array(
            'order'        => $this,
            'billing'      => $this->getBillingAddress(),
            'payment_html' => $paymentBlockHtml,
            'delivery_alert' => $this->getDeliveryAlertMessage(),
            'spelled_cc_type' => $this->spellCcType($this->getPayment()->getCcType()));

        if ($this->getShippingAddress() && $this->getShippingAddress()->getCountryId() == "CA") {
            $arr["canadian_note"] = 1;
        }

        if (strpos($this->getShippingDescription(), " - ") !== false ) {
            $arr["selected_shipping_method"] = end(explode(" - ", $this->getShippingDescription()));
        } else {
            $arr["selected_shipping_method"] = $this->getShippingDescription();
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams($arr);

        //$sTemplate = Mage::getModel('core/email_template')->load($templateId)->getProcessedTemplate($arr);
        //echo $sTemplate;
        //die("--");

        $mailer->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }

    public function getDeliveryAlertMessage() {

        $message = "";

        if (!$this->getShippingAddress())
            return $message;

        if (
            ($this->getShippingAddress() && $this->getShippingAddress()->getCountryId() == "CA" && $this->getShippingAmount() == 0) ||
            ($this->getShippingAddress() && in_array($this->getShippingAddress()->getCountryId(), array(21, 2, 52, 60)) && $this->getShippingAmount() == 0) ||
            ($this->getGrandTotal() >= 10000 && $this->getShippingAmount() == 0) ||
            ($this->getCustomerId() == 351757)
        ) {
            $message = "A shipping quote for your location was not available online at the time you placed your order. Our customer service team will contact you with the shipping cost. Thank you, Happy Chef.";
        }

        return $message;
    }

    public function getRandomItem() {

        $items = $this->getAllVisibleItems();
        $newItems = array();
        /**
         * Resort order items based on review_priority attribute and
         * eliminate duplicates for parent products (if the same item was purchased in
         * different colors, we want to collect only one review)
         */
        $alreadyAddedProduct = array();
        foreach($items as $item) {
            if(!in_array($item->getProductId(), $alreadyAddedProduct)){
		$item->setData('orderId', $this->getId());
                $newItem[$item->getId()] = $item;
                $sorted[$item->getId()] = $item->getProduct()->getReviewPriority();
                $alreadyAddedProduct[] = $item->getProductId();
            }
        }
        asort($sorted);
        foreach($sorted as $itemId => $priority) {
            $newItems[] = $newItem[$itemId];
        }

        // Limit to 3 items
        return array_slice($newItems, 0, 3);
    }
}
