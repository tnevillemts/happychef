<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'OnepageController.php';

class Ripen_Coreextended_OnepageController extends Mage_Checkout_OnepageController 
{
    public function indexAction()
    {
        parent::indexAction();
    }

    public function saveOrderAction()
    {

        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*');
            return;
        }

        if ($this->_expireAjax()) {
            return;
        }

        $dbResource = Mage::getSingleton('core/resource');
        $readConnection = $dbResource->getConnection('core_read');

        $avscode  = '';

        $result = array();
        try {
            $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
            if ($requiredAgreements) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                $diff = array_diff($requiredAgreements, $postedAgreements);
                if ($diff) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }

            $data = $this->getRequest()->getPost('payment', array());
            if ($data) {
                $data['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_CHECKOUT
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                    | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX
                    | Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;
                $this->getOnepage()->getQuote()->getPayment()->importData($data);
            }

            $this->getOnepage()->saveOrder();

            $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
            $result['success'] = true;
            $result['error']   = false;

            $quoteId = $this->getOnepage()->getQuote()->getId();

            if(! $readConnection) {
                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " No db connection ", null, 'gateway.log');
            } else {
                $sql = "SELECT avscode FROM mag_gateway_log WHERE quote_id = '" . $quoteId  . "' AND resultCode = 'Ok' ORDER BY created_at DESC LIMIT 1";
                $avscode = $readConnection->fetchOne($sql);
                if(! $avscode) {
                    Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " No query result, sql [" . $sql . "]", null, 'gateway.log');
                } else {
                    Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " query result, sql [" . $sql . "], avscode [" . $avscode . "]", null, 'gateway.log');
                }
            }

            $result['messages'] = 'Success-' . $avscode;

        } catch (Mage_Payment_Model_Info_Exception $e) {
            $message = $e->getMessage();
            if (!empty($message)) {
                $result['error_messages'] = $message;
            }
            $result['goto_section'] = 'payment';
            $result['update_section'] = array(
                'name' => 'payment-method',
                'html' => $this->_getPaymentMethodsHtml()
            );
        } catch (Mage_Core_Exception $e) {
            $quoteId = $this->getOnepage()->getQuote()->getId();
            if(! $readConnection) {
                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " No db connection ", null, 'gateway.log');
            } else {

                $sql = "SELECT avscode FROM mag_gateway_log WHERE quote_id = '" . $quoteId  . "' AND resultCode = 'Error' ORDER BY created_at DESC LIMIT 1";
                $avscode = $readConnection->fetchOne($sql);
                if(! $avscode) {
                    Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " No query result, sql [" . $sql . "]", null, 'gateway.log');
                }
            }

            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();

            //Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " avscode [" . $avscode . "]", null, 'gateway.log');
            if($avscode == 'A' || $avscode == 'B' || $avscode == 'N') {
                $result['success'] = false;
                $result['error'] = true;
                $result['goto_section'] = 'billing';
                $result['avscode'] = $avscode;
            } else {
                $gotoSection = $this->getOnepage()->getCheckout()->getGotoSection();
                if ($gotoSection) {
                    $result['goto_section'] = $gotoSection;
                    $this->getOnepage()->getCheckout()->setGotoSection(null);
                } else {
                    $result['goto_section'] = 'payment';
                    $this->getOnepage()->getCheckout()->setGotoSection(null);
		}
                $updateSection = $this->getOnepage()->getCheckout()->getUpdateSection();
                if ($updateSection) {
                    if (isset($this->_sectionUpdateFunctions[$updateSection])) {
                        $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                        $result['update_section'] = array(
                            'name' => $updateSection,
                            'html' => $this->$updateSectionFunction()
                        );
                    }
                    $this->getOnepage()->getCheckout()->setUpdateSection(null);
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please try again later or contact us at 800.347.0288 from 9am to 5:30pm EST, Monday through Friday.');
        }
        $this->getOnepage()->getQuote()->save();
        /**
         * when there is redirect to third party, we don't want to save order yet.
         * we will save the order in return action.
         */
        if (isset($redirectUrl)) {
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }


    /**
     * Save checkout billing address
     */
    public function saveBillingAction()
    {
        $data = $this->getRequest()->getPost('billing', array());
        Mage::getSingleton('core/session')->setEmailIntegrityCheck($data["email"]);

        parent::saveBillingAction();

    }

}

