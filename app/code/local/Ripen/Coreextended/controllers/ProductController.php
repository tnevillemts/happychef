<?php

require_once Mage::getModuleDir('controllers', 'Mage_Review').DS.'ProductController.php';

class Ripen_Coreextended_ProductController extends Mage_Review_ProductController
{
    var $currentReviewId = null;

    public function setReviewId($reviewId) {
	$this->currentReviewId = $reviewId;
    }

    public function getReviewId() {
	$reviewId = $this->currentReviewId;
	$this->currentReviewId = null;
	return $reviewId;
    }

    public function indexAction()
    {
        parent::indexAction();
    }

    public function postAction()
    {
        if (!$this->_validateFormKey()) {
            // returns to the product item page
            $this->_redirectReferer();
            return;
        }

        if ($data = Mage::getSingleton('review/session')->getFormData(true)) {
            $rating = array();
            if (isset($data['ratings']) && is_array($data['ratings'])) {
                $rating = $data['ratings'];
            }
        } else {
            $data   = $this->getRequest()->getPost();
            $rating = $this->getRequest()->getParam('ratings', array());
        }

        if (($product = $this->_initProduct()) && !empty($data)) {
            $session = Mage::getSingleton('core/session');
            /* @var $session Mage_Core_Model_Session */
            $review = Mage::getModel('review/review')->setData($this->_cropReviewData($data));
            /* @var $review Mage_Review_Model_Review */

            $validate = $review->validate();
            if ($validate === true) {
                try {
                    if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
                    } else {
                        $reviewOrderId = $data["order_id"];
                        if (!empty($reviewOrderId)) {
                            $reviewOrder = Mage::getModel('sales/order')->load($reviewOrderId);
                            $customerId = $reviewOrder ? $reviewOrder->getCustomerId() : null;
                        }
                    }

                    $review->setEntityId($review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE))
                        ->setEntityPkValue($product->getId())
                        ->setStatusId(Mage_Review_Model_Review::STATUS_PENDING)
                        ->setCustomerId($customerId)
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->setStores(array(Mage::app()->getStore()->getId()))
                        ->save();

                    foreach ($rating as $ratingId => $optionId) {
                        Mage::getModel('rating/rating')
                        ->setRatingId($ratingId)
                        ->setReviewId($review->getId())
                        ->setCustomerId($customerId)
                        ->addOptionVote($optionId, $product->getId());
                    }

                    $review->aggregate();
                    $session->addSuccess($this->__('Your review has been accepted for moderation.'));
	                $this->setReviewId($review->getId());
                }
                catch (Exception $e) {
                    $session->setFormData($data);
                    $session->addError($this->__('Unable to post the review.'));
                }
            }
            else {
                $session->setFormData($data);
                if (is_array($validate)) {
                    foreach ($validate as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                }
                else {
                    $session->addError($this->__('Unable to post the review.'));
                }
            }
        }

        if ($redirectUrl = Mage::getSingleton('review/session')->getRedirectUrl(true)) {
            $this->_redirectUrl($redirectUrl);
            return;
        }
        $this->_redirectReferer();
    }
}
