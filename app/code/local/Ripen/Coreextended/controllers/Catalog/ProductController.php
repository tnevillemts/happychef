<?php
/**
 * Export CSV of all visible products with attributes
 *
 * @package	HappyChef
 * @author	Kim Kraft, Ripen Ecommerce kkraft@ri.pn
 * @since	08-15-2016
 */

include_once("Mage/Adminhtml/controllers/Catalog/ProductController.php"); // In case autoload fails

class Ripen_Coreextended_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{

	/**
	 * Array of all active attributes for products
	 *
	 * @var array
	 */
	protected $master_attributes = [];


	/**
	 * Ripen_Coreextended_Catalog_ProductController constructor
	 *
	 * Call parent constructors
	 */
	protected function _construct()
	{
		parent::_construct();
	}


	/**
	 * csvSafe() method
	 *
	 * Convert data to string safe for use in csv file (no commas)
	 *
	 * @param $in
	 * @return string
	 */
	protected function csvSafe($in) {
		return rtrim(str_replace(","," - ",$in)," - ");
	}


	/**
	 * getMasterAttributes() method
	 *
	 * Populates the $master_attributes var with currently active attributes for products
	 */
	protected function getMasterAttributes() {
		$attributes = Mage::getResourceModel('catalog/product_attribute_collection')
			->addIsFilterableFilter()
			->getItems();
		foreach($attributes as $attribute) {
			if ($attribute->getEntityTypeId() == 4){
				$this->master_attributes[$attribute->getAttributeId()] = $attribute->getAttributeCode();
			}
		}
	}


	/**
	 * exportcolorreportAction() method
	 *
	 * Uncomment/Comment relevant lines for CSV/XLS output
	 *
	 * Generate CSV color report
	 */
	public function exportcolorreportAction() {
		$products = Mage::getModel("catalog/product")->getCollection()
			->addAttributeToSelect('*')
			->addAttributeToFilter('status', 1);

		$csvArray = [];
		$xmlArray = [];
		$xmlArray[] = array(
			"SKU",
			"Product ID",
			"Name",
			"Available Colors"
		);

		foreach ($products as $product) {
			$typeInstance = $product->getTypeInstance(true);
			if (method_exists($typeInstance, "getConfigurableAttributesAsArray")){
				$productAttributeOptions = $typeInstance->getConfigurableAttributesAsArray($product);
				$colors = array();
				foreach ($productAttributeOptions as $productAttribute) {
					foreach ($productAttribute['values'] as $attribute) {
						if (strtolower($productAttribute['label']) == 'color'){
							$colors[] = $this->csvSafe($attribute['store_label']);
						}
					}
				}
				$csvArray[] = array(
					$product->getSku(),
					$product->getId(),
					$this->csvSafe($product->getName()),
					implode(" | ", $colors)
				);
				$xmlArray[] = array(
					$product->getSku(),
					$product->getId(),
					$this->csvSafe($product->getName()),
					implode(" | ", $colors)
				);
			}
		}

		$csvArray = array(
			"SKU",
			"Product ID",
			"Name",
			"Available Colors"
		);

		reset($csvArray);
		$content = "";
		while (list(,$csvRow) = each($csvArray)) {
			$content .= implode(",",$csvRow) . "\r\n";
		}

		$csvContent = implode(",", $csvHeader) . "\r\n" . $content;

		//$this->exportCsv($csvContent, "color_report.csv");
		$this->exportExcel($xmlArray, "color_report.xls");

	}


	/**
	 * exportvisibleAction() method
	 *
	 * Responds to click on Export button on Products page
	 */
	public function exportvisibleAction() {

		$this->getMasterAttributes();

		$products = Mage::getModel("catalog/product")->getCollection();
		$products->addAttributeToSelect('*');
		$products->addAttributeToFilter('status', 1);
		$products->addAttributeToFilter('has_options', 1);

		$this->exportCSV( $this->buildCSV($products) );

	}


	/**
	 * processAttributes() method
	 *
	 * Get the value of each attributes given a product object
	 *
	 * @param $atts
	 * @param $prod
	 * @return string
	 */
	protected function processAttributes($atts, $prod) {
		$output = "";

		while (list(, $attCode) = each($atts)){
			$attr = $prod->getResource()->getAttribute($attCode)->getFrontEnd()->getValue($prod);
			$output .= $this->csvSafe($attr) . ",";
		}
		return rtrim($output, ",");
	}


	/**
	 * buildCSV() method
	 *
	 * Build the contents of the CSV file
	 *
	 * @param Mage_Catalog_Model_Resource_Product_Collection $products
	 * @return string
	 */
	protected function buildCSV(Mage_Catalog_Model_Resource_Product_Collection $products) {

		$csvHeader = array(
			"SKU",
			"Name",
			"Categories"
		);

		$attributesToCollect = [];

		foreach($this->master_attributes as $attributeID => $attributeLabel){
			$attributesToCollect[$attributeLabel] = $attributeID;
			$csvHeader[] = $attributeLabel;
		}

		foreach($products as $prod) {

			$productCategories = $productAttributes = [];

			$categories = $prod->getCategoryCollection()->addAttributeToSelect("name");

			foreach($categories as $cat){
				$productCategories[] = $this->csvSafe($cat->getName());
			}

			unset($categories);

			$csvArray = array(
				$prod->getSku(),
				(string)$this->csvSafe($prod->getName()),
				implode(" - ", $productCategories),
			);

			$content .= implode(",", $csvArray) . "," . $this->processAttributes($attributesToCollect, $prod) . "\r\n";

		}

		return implode(",", $csvHeader) . "\r\n" . $content;

	}

	
	/**
	 * exportCSV() method
	 *
	 * Export the CSV file
	 *
	 * @param $content
	 * @param $filename
	 */
	protected function exportCSV($content, $filename = "export_visible.csv") {
		$this->_prepareDownloadResponse(
			$filename,
			$content,
			'text/csv'
		);

	}

	protected function exportExcel($content, $filename = "export_visible.xml") {
		$xmlObj = new Varien_Convert_Parser_Xml_Excel();
		$xmlObj->setVar('single_sheet', $filename);
		$xmlObj->setData($content);
		$xmlObj->unparse();
		$this->_prepareDownloadResponse(
			$filename,
			$xmlObj->getData(),
			'application/vnd.ms-excel'
		);

	}
}

