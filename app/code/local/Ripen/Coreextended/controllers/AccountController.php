<?php

require_once Mage::getModuleDir('controllers', 'Mage_Customer').DS.'AccountController.php';

class Ripen_Coreextended_AccountController extends Mage_Customer_AccountController
{
    public function indexAction()
    {
        parent::indexAction();
    }

    public function pulldownAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        //if ($isAjax) {
            $layout = $this->getLayout();
            $update = $layout->getUpdate();
            $update->load('customer_account_pulldown');
            $layout->generateXml();
            $layout->generateBlocks();
            $output = $layout->getOutput();
            $this->getResponse()->setBody($output);
        //}
    }

    /**
     * Overwrite _loginPostRedirect method
     *
     */
    protected function _loginPostRedirect()
    {
        $session = $this->_getSession();

        /*
         * This outer IF condition has been modified. If customer started One Page checkout and then used Sign In form,
         * redirect customer either to "My Account" page (if sign in was successful) or to "Sign In" page
         * (if sign in failed). Ripen (MM)
         *
         */

        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl() || (strpos($session->getBeforeAuthUrl(), "checkout") !== false && $this->getRequest()->getParam('context') != "checkout")) {
            // Set default URL to redirect customer to
            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
            // Redirect customer to the last page visited after logging in
            if ($session->isLoggedIn()) {
                if (!Mage::getStoreConfigFlag(
                    Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
                )) {
                    $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
                    if ($referer) {
                        // Rebuild referer URL to handle the case when SID was changed
                        $referer = $this->_getModel('core/url')
                            ->getRebuiltUrl( $this->_getHelper('core')->urlDecodeAndEscape($referer));
                        if ($this->_isUrlInternal($referer)) {
                            $session->setBeforeAuthUrl($referer);
                        }
                    }
                } else if ($session->getAfterAuthUrl()) {
                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                }
            } else {
                $session->setBeforeAuthUrl( $this->_getHelper('customer')->getLoginUrl());
            }
        } else if ($session->getBeforeAuthUrl() ==  $this->_getHelper('customer')->getLogoutUrl()) {
            $session->setBeforeAuthUrl( $this->_getHelper('customer')->getDashboardUrl());
        } else {
            if (!$session->getAfterAuthUrl()) {
                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
            }
            if ($session->isLoggedIn()) {
                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
            }
        }
        $this->_redirectUrl($session->getBeforeAuthUrl(true));
    }

    /**
     * Create customer account action
     */
    public function createPostAjaxAction()
    {
        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();

        $customer = $this->_getCustomer();

        try {
            $errors = $this->_getCustomerErrors($customer);

            if (empty($errors)) {
                $customer->cleanPasswordsValidationData();
                $customer->setReferredBy($this->getRequest()->getParam('referred_by'));
                $customer->setOtherReferral(htmlspecialchars($this->getRequest()->getParam('other_referral')));
                $customer->save();
                $this->_dispatchRegisterSuccess($customer);
                $this->_successProcessRegistration($customer);
                $response['errMsg'] = 'noerror';
            } else {
                $response['errMsg'] = $errors;
            }
        } catch (Mage_Core_Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $response['errMsg'] = $this->__('An account with this email already exists.');
            } else {
                $response['errMsg'] = $this->_escapeHtml($e->getMessage());
            }
        } catch (Exception $e) {
            $response['errMsg'] = $e;
        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($response));
    }

}
