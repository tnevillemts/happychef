<?php

$installer = $this;

$installer->startSetup();

$installer->run("
        INSERT IGNORE INTO `{$this->getTable('permission_block')}` (`block_id`, `block_name`, `is_allowed`) VALUES
        (NULL, 'contactform/contactform', 1),
        (NULL, 'catalogrequest/catalogrequest', 1),
        (NULL, 'catalog/product_list', 1),
        (NULL, 'orderstatus/orderstatus', 1),
        (NULL, 'cms/widget_block', 1),
        (NULL, 'cms/block', 1);
    ");

$installer->endSetup(); 