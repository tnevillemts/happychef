<?php
class Ripen_Catalogreviewpriority_Block_Adminhtml_Catalog_Product_Renderer_Style extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $data =  $row->getData();

        $list = "";
        foreach($data as $attribute => $value) {
            if (strpos($attribute, "style") !== false && strlen($value)){
                $attributeOptionIds = explode(',', $value);
                foreach($attributeOptionIds as $attributeOptionId){
                    $attributeDetails = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $attribute);
                    $list .=  $attributeDetails->getSource()->getOptionText($attributeOptionId).", ";
                }
            }
        }

        return substr($list,0,strrpos( $list, ", " ));
    }
}