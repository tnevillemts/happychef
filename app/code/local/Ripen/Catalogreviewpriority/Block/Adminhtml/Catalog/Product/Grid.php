<?php

class Ripen_Catalogreviewpriority_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('product_filter');
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection()
    {
        $store = $this->_getStore();
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('review_priority')
        ;

        $collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $collection->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);


        $collection->getSelect()->joinLeft(
            'mag_review_entity_summary',
            'e.entity_id = mag_review_entity_summary.entity_pk_value and mag_review_entity_summary.store_id = 1',
            array('reviews_count')
        )
            ->order('reviews_count DESC')
        ;


        $this->setCollection($collection);

        parent::_prepareCollection();
        $this->getCollection()->addWebsiteNamesToResult();
        return $this;
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField('websites',
                    'catalog/product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left');
            }
        }
        return parent::_addColumnFilterToCollection($column);
    }

    protected function _prepareOptionsArray($attributeCode) {

        $items = Mage::getModel('eav/entity_attribute_option')->getCollection()->setStoreFilter()->join('attribute','attribute.attribute_id=main_table.attribute_id', 'attribute_code');
        foreach ($items as $item) :
            if ($item->getAttributeCode() == $attributeCode) {
                $options[$item->getOptionId()] = $item->getValue();
            }
        endforeach;

        return $options;

    }


    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header'=> Mage::helper('catalog')->__('id'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'entity_id',
        ));

        $this->addColumn('sku',
            array(
                'header'=> Mage::helper('catalog')->__('sku'),
                'width' => '80px',
                'index' => 'sku',
        ));

        $this->addColumn('name',
            array(
                'header'=> Mage::helper('catalog')->__('name'),
                'index' => 'name',
            ));

        $this->addColumn('reviews_count',
            array(
                'header'=> Mage::helper('catalog')->__('number_of_reviews'),
                'width' => '100px',
                'type'  => 'number',
                'index' => 'reviews_count',
        ));


        $this->addColumn('review_priority',
            array(
                'header'=> Mage::helper('catalog')->__('priority'),
                'type'  => 'number',
                'index' => 'review_priority',
            ));

        $this->addExportType('*/*/exportCsv',
            Mage::helper('catalogreviewpriority')->__('CSV'));

        return parent::_prepareColumns();
    }

    protected function _customizeFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->getSelect()->where(
            "sales_flat_order_address.city like ?
            OR sales_flat_order_address.street like ?
            OR sales_flat_order_address.postcode like ?"
            , "%$value%");


        return $this;
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> Mage::helper('catalog')->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
             'confirm' => Mage::helper('catalog')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('catalog/product_status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('catalog')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('catalog')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('catalog/update_attributes')){
            $this->getMassactionBlock()->addItem('attributes', array(
                'label' => Mage::helper('catalog')->__('Update Attributes'),
                'url'   => $this->getUrl('*/catalog_product_action_attribute/edit', array('_current'=>true))
            ));
        }

        Mage::dispatchEvent('adminhtml_catalog_product_grid_prepare_massaction', array('block' => $this));
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/catalog_product/edit', array(
            'store'=>$this->getRequest()->getParam('store'),
            'id'=>$row->getId())
        );
    }
}
