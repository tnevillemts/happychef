<?php

class Ripen_Catalogreviewpriority_Adminhtml_CatalogreviewpriorityController extends Mage_Adminhtml_Controller_Action
{

    protected function _construct()
    {
        // Define module dependent translate
        $this->setUsedModuleName('Mage_Catalog');
    }

    /**
     * Product list page
     */
    public function indexAction()
    {

        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $this->loadLayout();
        $this->renderLayout();

        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
        //die("---");

    }

    /**
     * Create serializer block for a grid
     *
     * @param string $inputName
     * @param Mage_Adminhtml_Block_Widget_Grid $gridBlock
     * @param array $productsArray
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Ajax_Serializer
     */
    protected function _createSerializerBlock($inputName, Mage_Adminhtml_Block_Widget_Grid $gridBlock, $productsArray)
    {
        return $this->getLayout()->createBlock('adminhtml/catalog_product_edit_tab_ajax_serializer')
            ->setGridBlock($gridBlock)
            ->setProducts($productsArray)
            ->setInputElementName($inputName)
            ;
    }

    /**
     * Output specified blocks as a text list
     */
    protected function _outputBlocks()
    {
        $blocks = func_get_args();
        $output = $this->getLayout()->createBlock('adminhtml/text_list');
        foreach ($blocks as $block) {
            $output->insert($block, '', true);
        }
        $this->getResponse()->setBody($output->toHtml());
    }

    /**
     * Product grid for AJAX request
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }


    public function exportCsvAction()
    {
        $fileName   = 'ripen-review-priority-catalog-report.csv';
        $content    = $this->getLayout()->createBlock('catalogreviewpriority/adminhtml_catalog_product_grid')
            ->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $this->_prepareDownloadResponse($fileName, $content, $contentType);
    }

    public function massExportAction()
    {
        $productIds = $this->getRequest()->getParam('product');
        if (!is_array($productIds)) {
            $this->_getSession()->addError($this->__('Please select product(s).'));
            $this->_redirect('*/*/index');
        }
        else {
            //write headers to the csv file
            $content = "id,name,type,sku,price,qty,category,style,customizable,new,sale,colors,accessories,fabric,gender,department\n";

            try {
                foreach ($productIds as $productId) {

                    $product = Mage::getSingleton('catalog/product')->load($productId);
                    $id = $productId;
                    $name = $product->getName();
                    $type = "";
                    $sku = $product->getSku();

                    $content .=     "\"{$id}\",
                                    \"{$name}\",
                                    \"{$type}\",
                                    \"{$sku}\",
                                    \"{$price}\",
                                    \"{$qty}\",
                                    \"{$category}\",
                                    \"{$style}\",
                                    \"{$customizable}\",
                                    \"{$new}\",
                                    \"{$sale}\",
                                    \"{$colors}\",
                                    \"{$accessories}\",
                                    \"{$fabric}\",
                                    \"{$gender}\",
                                    \"{$department}\"\n";

                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/index');
            }
            $this->_prepareDownloadResponse('ripen-review-priority-report.csv', $content, 'text/csv');
        }

    }

    /**
     * Show item update result from updateAction
     * in Wishlist and Cart controllers.
     *
     */
    public function showUpdateResultAction()
    {
        $session = Mage::getSingleton('adminhtml/session');
        if ($session->hasCompositeProductResult() && $session->getCompositeProductResult() instanceof Varien_Object){
            /* @var $helper Mage_Adminhtml_Helper_Catalog_Product_Composite */
            $helper = Mage::helper('adminhtml/catalog_product_composite');
            $helper->renderUpdateResult($this, $session->getCompositeProductResult());
            $session->unsCompositeProductResult();
        } else {
            $session->unsCompositeProductResult();
            return false;
        }
    }

}
