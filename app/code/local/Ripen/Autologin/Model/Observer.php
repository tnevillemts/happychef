<?php

class Ripen_Autologin_Model_Observer
{
    const SAGE_REFERER = 'sage';
    const SAGE_ERROR_SLUG = 'sage-error';

    /**
     * Get current page URL; no core method provides this in non-escaped form, accessible from an observer.
     *
     * @return string
     */
    protected function getCurrentUrl()
    {
        return Mage::getBaseUrl() . ltrim(Mage::app()->getRequest()->getOriginalPathInfo(), '/');
    }

    /**
     * @return void
     */
    protected function redirectToSageError()
    {
        $currentUrl = $this->getCurrentUrl();
        if (strpos($currentUrl, self::SAGE_ERROR_SLUG) === false) {
            $redirectUrl = Mage::getUrl(self::SAGE_ERROR_SLUG);
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect($redirectUrl)
                ->sendResponse();
            exit;
        }
    }

    /**
     * @param Mage_Customer_Model_Session $session
     * @param Mage_Customer_Model_Customer $corpCustomer
     * @return bool
     */
    protected function shouldLogout($session, $corpCustomer)
    {
        if (!$session->isLoggedIn()) return false;

        $sessionCustomer = $session->getCustomer();
        $sessionCustomerData = Mage::getModel('customer/customer')->load($sessionCustomer->getId())->getData();

        return $sessionCustomerData['entity_id'] != $corpCustomer->getId();
    }

    /**
     * On every page load, check if on Sage site. If we are, check if the URL attribute corp_customer_id
     * is set and if the referrer URL is sagedining.com (any subdomain or path). If both conditions
     * match, look up the customer with the matching vendor ID and log them in automatically.
     *
     * @throws Mage_Core_Model_Store_Exception
     */
    public function autoLogin()
    {
        if (Mage::app()->getStore()->getCode() != 'sage_website_01') return;

        try {
            /** @var Mage_Customer_Model_Session $session */
            $session = Mage::getSingleton('customer/session');
            $corpCustomerId = Mage::app()->getRequest()->getParam('corp_customer_id');
            $referrerUrl = Mage::helper('core/http')->getHttpReferer();

            // Override of referrer check used intended for internal Ripen & HC use only.
            $allowAnyReferrer = (bool) Mage::app()->getRequest()->getParam('bypassreferrer');

            if (!empty($corpCustomerId) && ($allowAnyReferrer || strpos($referrerUrl, self::SAGE_REFERER) !== false)) {
                /** @var Mage_Customer_Model_Customer $corpCustomer */
                $corpCustomer = Mage::getModel('customer/customer')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('corp_customer_id', $corpCustomerId)->load()->getFirstItem();

                if (is_object($corpCustomer) && $corpCustomer->getId() > 0 && $corpCustomer->isInStore(Mage::app()->getStore())) {
                    if ($this->shouldLogout($session, $corpCustomer)) {
                        $session->logout();
                    }

                    if (!$session->isLoggedIn()) {
                        $session->setCustomerAsLoggedIn($corpCustomer);
                        Mage::app()->getFrontController()->getResponse()
                            ->setRedirect($this->getCurrentUrl())
                            ->sendResponse();
                        exit;
                    }
                }
            }

            if (! $session->isLoggedIn()) {
                throw new Exception('Unauthorized');
            }
        } catch (Exception $e) {
            $this->redirectToSageError();
        }
    }
}
