<?php
/**
 * @author Ripen
 * @copyright Ripen
 * @package Ripen_Thumbindexer
 */
class Ripen_Thumbindexer_Model_Indexer_Thumbindexer extends Mage_Index_Model_Indexer_Abstract
{
    const EVENT_MATCH_RESULT_KEY = 'thumbindexer_match_result';

    protected $_matchedEntities = array(
        Mage_Catalog_Model_Product::ENTITY => array(
            Mage_Index_Model_Event::TYPE_SAVE,
            Mage_Index_Model_Event::TYPE_MASS_ACTION,
            Mage_Index_Model_Event::TYPE_DELETE
        )
    );

    /**
     * Retrieve Indexer name
     *
     * @return string
     */
    public function getName() {
        return Mage::helper('thumbindexer')->__('Product HTML Indexes');
    }

    /**
     * Retrieve Indexer description
     *
     * @return string
     */
    public function getDescription() {
        return Mage::helper('thumbindexer')->__('Regenerate HTML indexes for all products');
    }

    /**
     * Check if event can be matched by process
     *
     * @param Mage_Index_Model_Event $event
     * @return bool
     */
    public function matchEvent(Mage_Index_Model_Event $event)
    {
        return false;
    }

    /**
     * Rebuild all index data.
     *
     * Does not return anything to maintain consistency with parent class.
     *
     * @return void
     */
    public function reindexAll()
    {
        $this->reindex();
    }

    /**
     * Rebuild all index data. Returns count of products successfully reindexed.
     *
     * @param array $productIds
     * @return int
     */
    public function reindexSelected($productIds)
    {
        if (empty($productIds)) return 0;

        return $this->reindex($productIds);
    }

    /**
     * Request URL to trigger a given index operation.
     *
     * @return bool
     */
    protected function curlUrl($url)
    {
        $success = false;

        $ch = curl_init();
        if ($ch) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 400);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);

            if (strpos(Mage::getBaseUrl(), "https") !== false) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_PORT, 443);
            } else {
                curl_setopt($ch, CURLOPT_PORT, 80);
            }

            curl_exec($ch);
            $curlErrno = curl_errno($ch);
            $responseCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
            if ($curlErrno) {
                Mage::log("Curl error: [$url] error no [$curlErrno] Curl error: [" . curl_error($ch) . "]", null, 'reindex.log');
            } else if ($responseCode !== 200) {
                Mage::log("Curl error: [$url] HTTP response [$responseCode]", null, 'reindex.log');
            } else {
                Mage::log("Curl succeeded: $url", null, 'reindex.log');
                $success = true;
            }
            curl_close($ch);
        }

        return $success;
    }

    /**
     * Rebuild index data. Returns count of products successfully reindexed.
     *
     * @param array $productIds Array of products to reindex; if omitted all products included.
     * @return int
     */
    protected function reindex($productIds = null) {
        // Reindex prices first
        Mage::log("Reindex catalog_product_price magento index...", null, 'reindex.log');
        $process = Mage::getModel('index/indexer')->getProcessByCode('catalog_product_price');
        $process->reindexAll();
        Mage::log("Reindex completed [catalog_product_price]", null, 'reindex.log');

        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
        if ($productIds && is_array($productIds)) {
            $collection->addFieldToFilter('entity_id', array('in' => $productIds));
        } else {
            $collection->addFieldToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
            )));
            $collection->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        }

        $countReindexed = 0;
        try {
            foreach ($collection as $p) {
                // Create indexes for individual product pages
                // Example URL: https://happychef.com/index.php/catalog/product/view/id/23/doIndex/1
                $productUrl = $p->getProductUrl();
                if (strpos($productUrl, "html") !== false) {
                    $productUrl = $productUrl . "?doIndex=1";
                } else {
                    $productUrl = substr($productUrl, -1) != "/" ? $productUrl . "/doIndex/1" : $productUrl . "doIndex/1";
                }
                $productSuccess = $this->curlUrl($productUrl);

                // Create indexes for thumbnails
                // Example URL: https://happychef.com/thumbindexer/index/thumb/id/23/doIndex/1
                $thumbUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."index.php/thumbindexer/index/thumb/id/".$p->getId()."/doIndex/1";
                $thumbSuccess = $this->curlUrl($thumbUrl);

                if ($productSuccess && $thumbSuccess) {
                    ++$countReindexed;
                }
            }
        } catch (Exception $e) {
            Mage::log("Exception reindexing product: ". $e->getMessage(), null, 'reindex.log');
            throw $e;
        }

        return $countReindexed;
    }

    // Abstract methods from parent that have to be implemented but don't need to do anything.
    protected function _registerEvent(Mage_Index_Model_Event $event) {}
    protected function _processEvent(Mage_Index_Model_Event $event) {}
}
