<?php
/**
 * @author Ripen
 * @copyright Ripen
 * @package Ripen_Thumbindexer
 */
class Ripen_Thumbindexer_Model_Cron extends Ripen_Thumbindexer_Model_Indexer_Thumbindexer
{

    public function runCron()
    {
        $message = "Cron test 1";
        $templateId = "Dev";
        $emailTemplate = Mage::getModel('core/email_template')->loadByCode($templateId);
        $vars = array("message" => $message);
        $emailTemplate->getProcessedTemplate($vars);
        $emailTemplate->setSenderName('Ripen Tech Team');
        $emailTemplate->setSenderEmail('dev@ri.pn');
        try {
            $emailTemplate->send('mmedvedev@ri.pn', 'Ripen Tech', $vars);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
