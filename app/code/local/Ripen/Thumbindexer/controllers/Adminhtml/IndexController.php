<?php
class Ripen_Thumbindexer_Adminhtml_IndexController extends Mage_Adminhtml_Controller_action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function massReindexAction()
    {
        $productIds = $this->getRequest()->getParam('product');

        if (empty($productIds)) {
            Mage::getSingleton("adminhtml/session")->addError("No products selected.");
        } else {
            try {
                $reindexCount = Mage::getModel('thumbindexer/indexer_thumbindexer')->reindexSelected($productIds);
                if ($reindexCount > 0) {
                    Mage::getSingleton("adminhtml/session")->addSuccess("Successfully reindexed $reindexCount products.");
                }

                $failedCount = count($productIds) - $reindexCount;
                if ($failedCount > 0) {
                    Mage::getSingleton("adminhtml/session")->addError("Failed to reindex $failedCount products.");
                }
            } catch (Exception $e){
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
            }
        }

        session_write_close();
        $url = Mage::helper("adminhtml")->getUrl("adminhtml/catalog_product/index");
        $this->_redirectUrl($url);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/thumbindexer');
    }

}
