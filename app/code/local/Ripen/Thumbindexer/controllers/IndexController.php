<?php
class Ripen_Thumbindexer_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
		$this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle('Thumb Indexer');
	    $this->renderLayout();

    }

    public function thumbAction()
    {
        if ($this->getRequest()->getParam('id')) {
            $block = Mage::app()->getLayout()
                ->createBlock('thumbindexer/thumbnail')
                ->setData('area','frontend')
                ->setData("id", $this->getRequest()->getParam('id'))
                ->setTemplate('thumbindexer/thumbnail.phtml');
            echo $block->toHtml();
        } else {
            Mage::log("Product ID is not specified for index thumbnail.");
        }
    }

}
