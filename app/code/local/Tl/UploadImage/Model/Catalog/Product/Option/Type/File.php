<?php

class Tl_UploadImage_Model_Catalog_Product_Option_Type_File extends Mage_Catalog_Model_Product_Option_Type_File {

    protected function _validateUploadedFile()
    {
        // Get the uploaded file
        $option = $this->getOption();
        // Get its params
        $processingParams = $this->_getProcessingParams();

        $isPreloaded = Mage::registry('is_preloaded');
        $productIdNumber = Mage::registry('product_id_number');
        $imageName = Mage::registry('image_name');
        $fileName = Mage::registry('file_name');
        $customFileName = Mage::registry('custom_file_name');
        $originalTitle = Mage::registry('original_title');
        $storeId = Mage::app()->getStore()->getStoreId();
        $baseUrl = $this->getUrl('');
        $extension = pathinfo($imageName, PATHINFO_EXTENSION);
        $firstletter = $originalTitle[0];
        $count = mb_strlen($originalTitle);
        if($count != "1") {
            $secondletter = $originalTitle[1];
        }


        // If the user select a preloaded option, if not then upload the image
        if($isPreloaded == 'yes') {
            $fileTitle = "Uploaded Image";
            $finalExtension = "image/$extension";
            $fileHash = md5($fileTitle);
            if($count != "1") {
                $quotePath = '/media/custom_options/quote/'.$firstletter.'/'.$secondletter.'/'.$imageName;
                $orderPath = '/media/custom_options/order/'.$firstletter.'/'.$secondletter.'/'.$imageName;
                $fullPath = $baseUrl.'media/custom_options/quote/'.$firstletter.'/'.$secondletter.'/'.$imageName;
            } else {
                $quotePath = '/media/custom_options/quote/'.$firstletter.'/'.$imageName;
                $orderPath = '/media/custom_options/order/'.$firstletter.'/'.$imageName;
                $fullPath = $baseUrl.'media/custom_options/quote/'.$firstletter.'/'.$imageName;
            }
            $this->setUserValue(array(
                'type'          => $finalExtension,
                'title'         => $fileTitle,
                'quote_path'    => $quotePath,
                'order_path'    => $orderPath,
                'fullpath'      => $fullPath,
                'size'          => '',
                'width'         => '',
                'height'        => '',
                'secret_key'    => substr($fileHash, 0, 20),
            ));
            return $this;
        } else if(isset($customFileName)) {
            $customFileName = $customFileName.".".$extension;
            $fileTitle = $customFileName;
            /**
             * Upload init
             */
            // The upload is being done
            $upload   = new Zend_File_Transfer_Adapter_Http();
            $file = $processingParams->getFilesPrefix() . 'options_' . $option->getId() . '_file';
            try {
                $runValidation = $option->getIsRequire() || $upload->isUploaded($file);
                if (!$runValidation) {
                    $this->setUserValue(null);
                    return $this;
                }

                $fileInfo = $upload->getFileInfo($file);
                $fileInfo = $fileInfo[$file];
                $fileInfo['title'] = $fileInfo['name'];

            } catch (Exception $e) {
                // when file exceeds the upload_max_filesize, $_FILES is empty
                if (isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > $this->_getUploadMaxFilesize()) {
                    $this->setIsValid(false);
                    $value = $this->_bytesToMbytes($this->_getUploadMaxFilesize());
                    Mage::throwException(
                        Mage::helper('catalog')->__("The file you uploaded is larger than %s Megabytes allowed by server", $value)
                    );
                } else {
                    switch($this->getProcessMode())
                    {
                        case Mage_Catalog_Model_Product_Type_Abstract::PROCESS_MODE_FULL:
                            Mage::throwException(
                                Mage::helper('catalog')->__('Please specify the product\'s required option(s).')
                            );
                            break;
                        default:
                            $this->setUserValue(null);
                            break;
                    }
                    return $this;
                }
            }

            /**
             * Option Validations
             */

            // Image dimensions - Validation
            $_dimentions = array();
            if ($option->getImageSizeX() > 0) {
                $_dimentions['maxwidth'] = $option->getImageSizeX();
            }
            if ($option->getImageSizeY() > 0) {
                $_dimentions['maxheight'] = $option->getImageSizeY();
            }
            if (count($_dimentions) > 0) {
                $upload->addValidator('ImageSize', false, $_dimentions);
            }

            // File extension - Validation
            $_allowed = $this->_parseExtensionsString($option->getFileExtension());
            if ($_allowed !== null) {
                $upload->addValidator('Extension', false, $_allowed);
            } else {
                $_forbidden = $this->_parseExtensionsString($this->getConfigData('forbidden_extensions'));
                if ($_forbidden !== null) {
                    $upload->addValidator('ExcludeExtension', false, $_forbidden);
                }
            }

            // Maximum filesize - Validation
            $upload->addValidator('FilesSize', false, array('max' => $this->_getUploadMaxFilesize()));

            /**
             * Upload process
             */

            $this->_initFilesystem();

            if ($upload->isUploaded($file) && $upload->isValid($file)) {

                $extension = pathinfo(strtolower($fileInfo['name']), PATHINFO_EXTENSION);

                $fileName = Mage_Core_Model_File_Uploader::getCorrectFileName($fileInfo['name']);
                $dispersion = Mage_Core_Model_File_Uploader::getDispretionPath($fileName);

                $filePath = $dispersion;
                $fileHash = md5(file_get_contents($fileInfo['tmp_name']));
                $filePath .= DS . $fileHash . '.' . $extension;
                $fileFullPath = $this->getQuoteTargetDir() . $filePath;

                $upload->addFilter('Rename', array(
                    'target' => $fileFullPath,
                    'overwrite' => true
                ));

                $this->getProduct()->getTypeInstance(true)->addFileQueue(array(
                    'operation' => 'receive_uploaded_file',
                    'src_name'  => $file,
                    'dst_name'  => $fileFullPath,
                    'uploader'  => $upload,
                    'option'    => $this,
                ));

                $_width = 0;
                $_height = 0;
                if (is_readable($fileInfo['tmp_name'])) {
                    $_imageSize = getimagesize($fileInfo['tmp_name']);
                    if ($_imageSize) {
                        $_width = $_imageSize[0];
                        $_height = $_imageSize[1];
                    }
                }

                $this->setUserValue(array(
                    'type'          => $fileInfo['type'],
                    'title'         => $fileTitle,
                    'quote_path'    => $this->getQuoteTargetDir(true) . $filePath,
                    'order_path'    => $this->getOrderTargetDir(true) . $filePath,
                    'fullpath'      => $fileFullPath,
                    'size'          => $fileInfo['size'],
                    'width'         => $_width,
                    'height'        => $_height,
                    'secret_key'    => substr($fileHash, 0, 20),
                ));

            } elseif ($upload->getErrors()) {
                $errors = $this->_getValidatorErrors($upload->getErrors(), $fileInfo);

                if (count($errors) > 0) {
                    $this->setIsValid(false);
                    Mage::throwException( implode("\n", $errors) );
                }
            } else {
                $this->setIsValid(false);
                Mage::throwException(Mage::helper('catalog')->__('Please specify the product required option(s)'));
            }
            return $this;
        } else {
            /**
             * Upload init
             */
            // The upload is being done
            $upload   = new Zend_File_Transfer_Adapter_Http();
            $file = $processingParams->getFilesPrefix() . 'options_' . $option->getId() . '_file';
            try {
                $runValidation = $option->getIsRequire() || $upload->isUploaded($file);
                if (!$runValidation) {
                    $this->setUserValue(null);
                    return $this;
                }

                $fileInfo = $upload->getFileInfo($file);
                $fileInfo = $fileInfo[$file];
                $fileInfo['title'] = $fileInfo['name'];

            } catch (Exception $e) {
                // when file exceeds the upload_max_filesize, $_FILES is empty
                if (isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > $this->_getUploadMaxFilesize()) {
                    $this->setIsValid(false);
                    $value = $this->_bytesToMbytes($this->_getUploadMaxFilesize());
                    Mage::throwException(
                        Mage::helper('catalog')->__("The file you uploaded is larger than %s Megabytes allowed by server", $value)
                    );
                } else {
                    switch($this->getProcessMode())
                    {
                        case Mage_Catalog_Model_Product_Type_Abstract::PROCESS_MODE_FULL:
                            Mage::throwException(
                                Mage::helper('catalog')->__('Please specify the product\'s required option(s).')
                            );
                            break;
                        default:
                            $this->setUserValue(null);
                            break;
                    }
                    return $this;
                }
            }

            /**
             * Option Validations
             */

            // Image dimensions - Validation
            $_dimentions = array();
            if ($option->getImageSizeX() > 0) {
                $_dimentions['maxwidth'] = $option->getImageSizeX();
            }
            if ($option->getImageSizeY() > 0) {
                $_dimentions['maxheight'] = $option->getImageSizeY();
            }
            if (count($_dimentions) > 0) {
                $upload->addValidator('ImageSize', false, $_dimentions);
            }

            // File extension - Validation
            $_allowed = $this->_parseExtensionsString($option->getFileExtension());
            if ($_allowed !== null) {
                $upload->addValidator('Extension', false, $_allowed);
            } else {
                $_forbidden = $this->_parseExtensionsString($this->getConfigData('forbidden_extensions'));
                if ($_forbidden !== null) {
                    $upload->addValidator('ExcludeExtension', false, $_forbidden);
                }
            }

            // Maximum filesize - Validation
            $upload->addValidator('FilesSize', false, array('max' => $this->_getUploadMaxFilesize()));

            /**
             * Upload process
             */

            $this->_initFilesystem();

            if ($upload->isUploaded($file) && $upload->isValid($file)) {

                $extension = pathinfo(strtolower($fileInfo['name']), PATHINFO_EXTENSION);

                $fileName = Mage_Core_Model_File_Uploader::getCorrectFileName($fileInfo['name']);
                $dispersion = Mage_Core_Model_File_Uploader::getDispretionPath($fileName);

                $filePath = $dispersion;
                $fileHash = md5(file_get_contents($fileInfo['tmp_name']));
                $filePath .= DS . $fileHash . '.' . $extension;
                $fileFullPath = $this->getQuoteTargetDir() . $filePath;

                $upload->addFilter('Rename', array(
                    'target' => $fileFullPath,
                    'overwrite' => true
                ));

                $this->getProduct()->getTypeInstance(true)->addFileQueue(array(
                    'operation' => 'receive_uploaded_file',
                    'src_name'  => $file,
                    'dst_name'  => $fileFullPath,
                    'uploader'  => $upload,
                    'option'    => $this,
                ));

                $_width = 0;
                $_height = 0;
                if (is_readable($fileInfo['tmp_name'])) {
                    $_imageSize = getimagesize($fileInfo['tmp_name']);
                    if ($_imageSize) {
                        $_width = $_imageSize[0];
                        $_height = $_imageSize[1];
                    }
                }

                $this->setUserValue(array(
                    'type'          => $fileInfo['type'],
                    'title'         => $fileInfo['name'],
                    'quote_path'    => $this->getQuoteTargetDir(true) . $filePath,
                    'order_path'    => $this->getOrderTargetDir(true) . $filePath,
                    'fullpath'      => $fileFullPath,
                    'size'          => $fileInfo['size'],
                    'width'         => $_width,
                    'height'        => $_height,
                    'secret_key'    => substr($fileHash, 0, 20),
                ));

            } elseif ($upload->getErrors()) {
                $errors = $this->_getValidatorErrors($upload->getErrors(), $fileInfo);

                if (count($errors) > 0) {
                    $this->setIsValid(false);
                    Mage::throwException( implode("\n", $errors) );
                }
            } else {
                $this->setIsValid(false);
                Mage::throwException(Mage::helper('catalog')->__('Please specify the product required option(s)'));
            }
            return $this;
        }
    }
}