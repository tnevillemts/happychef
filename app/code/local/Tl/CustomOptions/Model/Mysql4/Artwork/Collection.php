<?php

class Tl_CustomOptions_Model_Mysql4_Artwork_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('customoptions/artwork');
    }
}
