<?php

class Tl_CustomOptions_Model_System_Config_Source_Assigned_Attributes {
    public function toOptionArray() {
        $helper = Mage::helper('customoptions');
        return array(
            array('value' => 0, 'label'=>$helper->__('None')),
            array('value' => 1, 'label'=>$helper->__('Price')),
            array('value' => 2, 'label'=>$helper->__('Name'))
        );
    }
}