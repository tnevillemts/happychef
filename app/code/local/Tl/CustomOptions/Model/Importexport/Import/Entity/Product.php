<?php

if (version_compare(Mage::getVersion(), '1.8.0', '<') || (version_compare(Mage::getVersion(), '1.13.0', '<') && version_compare(Mage::getVersion(), '1.9.0', '>='))) {
    class Tl_CustomOptions_Model_Importexport_Import_Entity_Product extends Tl_CustomOptions_Model_Importexport_Import_Entity_Product_M1700 {}
} else {
    class Tl_CustomOptions_Model_Importexport_Import_Entity_Product extends Tl_CustomOptions_Model_Importexport_Import_Entity_Product_M1800 {}
}