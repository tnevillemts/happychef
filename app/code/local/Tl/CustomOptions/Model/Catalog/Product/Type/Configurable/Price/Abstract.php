<?php

if ((string)Mage::getConfig()->getModuleConfig('DerModPro_BCP')->active == 'true'){
    class Tl_CustomOptions_Model_Catalog_Product_Type_Configurable_Price_Abstract extends DerModPro_BCP_Model_Catalog_Product_Type_Configurable_Price {}
} else {
    class Tl_CustomOptions_Model_Catalog_Product_Type_Configurable_Price_Abstract extends Mage_Catalog_Model_Product_Type_Configurable_Price {}
}