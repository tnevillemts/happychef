<?php

class Tl_CustomOptions_Block_Core_Html_Select extends Mage_Core_Block_Html_Select {

    public function addOption($value, $label, $params = array()) {
        $this->_options[] = array('value' => $value, 'label' => $label, 'params' => $params);
        return $this;
    }

    protected function _toHtml() {
        if (!$this->_beforeToHtml()) {
            return '';
        }

        $html = '<select width="300px;" name="' . $this->getName() . '" id="' . $this->getId() . '" class="'. $this->getClass() . ' selectpicker" title="' . $this->getTitle() . '" ' . $this->getExtraParams() . '>';
        $values = $this->getValue();

        if (!is_array($values)) {
            if (!is_null($values)) {
                $values = array($values);
            } else {
                $values = array();
            }
        }

        $isArrayOption = true;
        foreach ($this->getOptions() as $key => $option) {
            $params = array();
            if ($isArrayOption && is_array($option)) {
                $value  = $option['value'];
                $label = $option['label'];
                $params = isset($option['params']) ? $option['params'] : array();
            } else {
                $value = $key;
                $label = $option;
                $isArrayOption = false;
            }

            if (is_array($value)) {
                $html.= '<optgroup label="' . $label . '">';
                foreach ($value as $keyGroup => $optionGroup) {
                    if (!is_array($optionGroup)) {
                        $optionGroup = array(
                            'value' => $keyGroup,
                            'label' => $optionGroup
                        );
                    }
                    $html.= $this->_optionToHtml(
                            $optionGroup, in_array($optionGroup['value'], $values)
                    );
                }
                $html.= '</optgroup>';
            } else {
                $html.= $this->_optionToHtml(array(
                    'value' => $value,
                    'label' => $label,
                    'params' => $params
                        ), in_array($value, $values)
                );

                $optionArray[] = array(
                    'value' => $value,
                    'label' => $label,
                    'params' => $params
                );
            }
        }
        $html .= '</select>';
       
        return $html;
    }

    protected function _optionToHtml($option, $selected = false) {
        $selectedHtml = $selected ? ' selected="selected"' : '';
        $params = '';
        if (isset($option['params']) && $option['params'] != '') {
            foreach ($option['params'] as $key => $value) {
                $params .= $key . '="' . $value . '" ';
            }
        }
        
        $arrayImg = explode("|", $option['label']);
        
        if($arrayImg[1]!="" && $arrayImg[1]!=" " ){
            $pathimgurl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."customoptions/".$arrayImg[1];
            $yyy = $this->htmlEscape($option['value']);
        } else {
            $pathimgurl="";
            //$yyy = 0;
            $yyy = $this->htmlEscape($option['value']);
        }
        
        $html = '<option  data-image="'.$pathimgurl.'"' . $params . 'value="'. $yyy . '"' . $selectedHtml . '>'
         . $this->htmlEscape($arrayImg[0]) . '</option>';
        
        

        return $html;
    }

}
