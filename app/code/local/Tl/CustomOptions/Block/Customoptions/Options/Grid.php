<?php

class Tl_CustomOptions_Block_Customoptions_Options_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
      parent::__construct();

      $this->setId('customoptionsOptionsGrid');
      $this->setDefaultSort('title');
      $this->setDefaultDir(Varien_Data_Collection::SORT_ORDER_ASC);
      $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
      $collection = Mage::getModel('customoptions/group')->getCollection();
      $collection->addProductsCount()->setShellRequest();      
      $this->setCollection($collection);
      return parent::_prepareCollection();
    }

    protected function getStoreId() {
        return Mage::registry('store_id');
    }

    protected function _prepareColumns() {
        $helper = $this->_getHelper();
        $this->addColumn('title', array(
            'header' => $helper->__('Embroidery Template Title'),
            'index' => 'title',
            'align' => 'left',
        ));

        return parent::_prepareColumns();
    }

    protected function _afterLoadCollection() {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    protected function _getHelper()
    {
    	return Mage::helper('customoptions');
    }

    public function getRowUrl($row)
    {
      return $this->getUrl('*/*/edit', array('group_id' => $row->getGroupId(), 'store' => $this->getStoreId()));
    }
}
