<?php

class Tl_CustomOptions_Block_Customoptions_Options_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_objectId = 'group_id';
        $this->_blockGroup = 'tl';
        $this->_controller = 'customoptions_options';
        $helper = Mage::helper('customoptions');

        parent::__construct();

            $this->_addButton('duplicate_button', array(
			'label'     => Mage::helper('adminhtml')->__('Duplicate'),
			'onclick'   => 'setLocation(\'' . $this->getDuplicateUrl() . '\')',
			'class'     => 'add',
			), -100);

        $this->_updateButton('save', '', array(
            'label' => $helper->__('Save'),
            'onclick' => 'saveOptionsForm()',
            'class' => 'save',
            'sort_order' => 0
        ));
        
          $this->_addButton('saveandcontinue', array(
			'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'   => 'saveAndContinueEdit()',
			'class'     => 'save',
		), -100);
		
		  $this->_addButton('saveandcontinue', array(
			'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'   => 'saveAndContinueEdit()',
			'class'     => 'save',
		), -100);

        $this->_formScripts[] = "
            function saveOptionsForm() {
                applySelectedProducts('save')
            }
            function saveAndContinueEdit() {
                applySelectedProducts('saveandcontinue')
            }
        ";
        
        $this->_formScripts[] = " function saveAndContinueEdit(){
            editForm.submit($('edit_form').action+'back/edit/');
        }
    ";
    }

    public function getHeaderText() {
        if (Mage::registry('customoptions_data') && Mage::registry('customoptions_data')->getId()) {
            return Mage::helper('customoptions')->__("Embroidery Template '%s'", $this->htmlEscape(Mage::registry('customoptions_data')->getTitle()));
        } else {
            return Mage::helper('customoptions')->__('New Embroidery Template');
        }
    }
    
     public function getDuplicateUrl()
    {
        return $this->getUrl('*/*/duplicate', array('_current'=>true));
    }

}
