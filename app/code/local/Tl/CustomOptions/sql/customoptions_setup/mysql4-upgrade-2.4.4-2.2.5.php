<?php

$installer = $this;
$installer->startSetup();
$installer->run("UPDATE IGNORE `{$this->getTable('core_config_data')}` SET `path` = REPLACE(`path`,'tl_sales/','tl_catalog/') WHERE `path` LIKE 'tl_sales/customoptions/%'");
$installer->endSetup();