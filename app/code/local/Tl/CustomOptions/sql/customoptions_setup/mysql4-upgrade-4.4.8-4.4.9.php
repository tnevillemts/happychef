<?php

// echo 'Running This Upgrade: '.get_class($this)."\n <br /> \n";
 //die("Exit for now"); 
	     $installer = $this;	
	     $installer->startSetup();
	     $installer->run("

	-- DROP TABLE IF EXISTS {$installer->getTable('artwork')};
	CREATE TABLE {$installer->getTable('artwork')} (
	    `art_id` int(10) NOT NULL AUTO_INCREMENT,
		  `customer_id` int(10) NOT NULL,
		  `product_id` int(10) NOT NULL,
		  `save_name` varchar(255) NOT NULL,
		  `img_path` varchar(255) NOT NULL,
		  PRIMARY KEY (`art_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

      $installer->endSetup();
     
?>

