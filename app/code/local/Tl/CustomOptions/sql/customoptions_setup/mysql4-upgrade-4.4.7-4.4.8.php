<?php
// echo 'Running This Upgrade: '.get_class($this)."\n <br /> \n";
 //die("Exit for now"); 
	     $installer = $this;	
	     $installer->startSetup();
	     $installer->run("

	-- DROP TABLE IF EXISTS {$installer->getTable('optioninfo')};
	CREATE TABLE {$installer->getTable('optioninfo')} (
	  `custom_id` int(10) NOT NULL AUTO_INCREMENT,
	  `customer_id` int(10) NOT NULL,
	  `product_id` int(10) NOT NULL,
	  `temp_name` varchar(255) NOT NULL,
	  `radio_info` varchar(255) NOT NULL,
	  `option_data` varchar(255) NOT NULL,
	   PRIMARY KEY (`custom_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

      $installer->endSetup();
?>
