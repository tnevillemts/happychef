<?php

$installer = $this;
$installer->startSetup();

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product'), 'sku_policy')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product'),
        'sku_policy',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
    
    $installer->run("
        UPDATE `{$installer->getTable('catalog_product_entity')}` AS t1 SET t1.`sku_policy` = 3 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `product_id` = t1.`entity_id` AND `sku_policy` = 3) > 0;
        UPDATE `{$installer->getTable('catalog_product_option')}` SET `sku_policy` = 0 WHERE `sku_policy` = 3;
    ");
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('customoptions/group'), 'sku_policy')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('customoptions/group'),
        'sku_policy',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
}

$installer->endSetup();