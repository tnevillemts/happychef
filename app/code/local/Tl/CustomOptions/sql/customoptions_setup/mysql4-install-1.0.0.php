<?php

$installer = $this;

$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/group')};
CREATE TABLE IF NOT EXISTS {$installer->getTable('customoptions/group')} (
  `group_id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `is_active` tinyint(1) NOT NULL,
  `store_id` smallint(5) unsigned default NULL,
  `hash_options` longtext NOT NULL,
   PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/relation')};
CREATE TABLE IF NOT EXISTS {$installer->getTable('customoptions/relation')} (
  `id` int(10) unsigned NOT NULL auto_increment,
  `group_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `UNQ_TL_CUSTOM_RELATION` (`group_id`,`option_id`,`product_id`),
   CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_INDEX_PRODUCT_ENTITY` FOREIGN KEY (`product_id`) REFERENCES `{$installer->getTable('catalog/product')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_INDEX_GROUP_RELATION` FOREIGN KEY (`group_id`) REFERENCES `{$installer->getTable('customoptions/group')}` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/option_description')};
CREATE TABLE IF NOT EXISTS {$installer->getTable('customoptions/option_description')} (
  `option_description_id` int(10) unsigned NOT NULL auto_increment,
  `option_id` int(10) unsigned NOT NULL default '0',
  `store_id` smallint(5) unsigned NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`option_description_id`),
  KEY `TL_EMBROIDERY_OPTIONS_DESCRIPTION_OPTION` (`option_id`),
  KEY `TL_EMBROIDERY_OPTIONS_DESCRIPTION_STORE` (`store_id`),
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_DESCRIPTION_OPTION` FOREIGN KEY (`option_id`) REFERENCES `{$installer->getTable('catalog/product_option')}` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_DESCRIPTION_STORE` FOREIGN KEY (`store_id`) REFERENCES `{$installer->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->run("-- DROP TABLE IF EXISTS `{$installer->getTable('customoptions/group_store')}`;
CREATE TABLE IF NOT EXISTS `{$installer->getTable('customoptions/group_store')}` (
  `group_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,  
  `hash_options` longtext NOT NULL,
  PRIMARY KEY (`group_store_id`),
  UNIQUE KEY `UNQ_EMBROIDERY_OPTIONS_GROUP_STORE` (`group_id`,`store_id`),
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_GROUP_STORE` FOREIGN KEY (`group_id`) REFERENCES `{$installer->getTable('customoptions/group')}` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->run("-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/option_default')};
CREATE TABLE IF NOT EXISTS `{$installer->getTable('customoptions/option_default')}` (
  `option_default_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `default_text` text NOT NULL,
  PRIMARY KEY (`option_default_id`),
  UNIQUE KEY `option_id+store_id` (`option_id`,`store_id`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_DEFAULT_OPTION` FOREIGN KEY (`option_id`) REFERENCES `{$installer->getTable('catalog/product_option')}` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_DEFAULT_STORE` FOREIGN KEY (`store_id`) REFERENCES `{$installer->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `{$installer->getTable('customoptions/option_description')}` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';
DELETE FROM `{$installer->getTable('customoptions/option_description')}` WHERE `description` = '';");

$installer->run("
-- DROP TABLE IF EXISTS `{$installer->getTable('customoptions/option_type_tier_price')}`;
CREATE TABLE IF NOT EXISTS `{$installer->getTable('customoptions/option_type_tier_price')}` (
  `option_type_tier_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_type_price_id` int(10) unsigned NOT NULL DEFAULT '0',
  `qty` int(10) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `price_type` enum('fixed','percent') NOT NULL DEFAULT 'fixed',
  PRIMARY KEY (`option_type_tier_price_id`),
  UNIQUE KEY `option_type_price_id+qty` (`option_type_price_id`,`qty`),
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_INDEX_OPTION_TYPE_TIER_PRICE` FOREIGN KEY (`option_type_price_id`) REFERENCES `{$installer->getTable('catalog/product_option_type_price')}` (`option_type_price_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- DROP TABLE IF EXISTS `{$installer->getTable('customoptions/option_type_image')}`;
CREATE TABLE IF NOT EXISTS `{$installer->getTable('customoptions/option_type_image')}` (
  `option_type_image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `image_file` varchar (255) default '',
  `sort_order` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `source` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1-file,2-color,3-gallery',
  PRIMARY KEY (`option_type_image_id`),
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_INDEX_OPTION_TYPE_IMAGE` FOREIGN KEY (`option_type_id`) REFERENCES `{$installer->getTable('catalog/product_option_type_value')}` (`option_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- DROP TABLE IF EXISTS `{$installer->getTable('customoptions/option_view_mode')}`;
CREATE TABLE IF NOT EXISTS `{$installer->getTable('customoptions/option_view_mode')}` (
  `view_mode_id` int(10) unsigned NOT NULL auto_increment,
  `option_id` int(10) unsigned NOT NULL default '0',
  `store_id` smallint(5) unsigned NOT NULL default '0',
  `view_mode` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`view_mode_id`),
  UNIQUE KEY `option_id+store_id` (`option_id`,`store_id`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_VIEW_MODE_OPTION` FOREIGN KEY (`option_id`) REFERENCES `{$installer->getTable('catalog/product_option')}` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TL_EMBROIDERY_OPTIONS_VIEW_MODE_STORE` FOREIGN KEY (`store_id`) REFERENCES `{$installer->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customoptions_status')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'customoptions_status', 'tinyint(1) NOT NULL default 0'
    );
}


if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'customoptions_qty')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'), 'customoptions_qty', "varchar(10) NOT NULL default ''"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customoptions_is_onetime')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'customoptions_is_onetime', 'TINYINT (1) NOT NULL DEFAULT 0'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'image_path')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'image_path', "varchar (255) default ''"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'default')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'), 'default', "tinyint(1) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customer_groups')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'customer_groups', "varchar (255) default ''"
    );
}
if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'qnty_input')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'qnty_input', "tinyint(1) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'view_mode')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'view_mode', "tinyint(1) NOT NULL DEFAULT '1'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'in_group_id')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'in_group_id', "SMALLINT UNSIGNED NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'in_group_id')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'), 'in_group_id', "SMALLINT UNSIGNED NOT NULL DEFAULT '0'"
    );
}


if ($installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customoptions_status')) {
    $installer->getConnection()->dropColumn(
        $installer->getTable('catalog/product_option'), 'customoptions_status'
    );
}

$installer->run("ALTER TABLE `{$installer->getTable('customoptions/relation')}` ADD INDEX (`option_id`)");
$installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option')}` CHANGE `in_group_id` `in_group_id` INT UNSIGNED NOT NULL DEFAULT '0'");
$installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option_type_value')}` CHANGE `in_group_id` `in_group_id` INT UNSIGNED NOT NULL DEFAULT '0'");

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'is_dependent')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'is_dependent', "tinyint(1) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'dependent_ids')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'), 'dependent_ids', "varchar(255) NOT NULL DEFAULT ''"
    );
}

$installer->run("UPDATE `{$installer->getTable('catalog/product_option')}` AS cpo, `{$installer->getTable('customoptions/relation')}` AS cor
    SET cpo.`in_group_id`=(cor.`group_id` * 65535) + cpo.`in_group_id`
    WHERE cpo.`option_id`=cor.`option_id` AND cpo.`in_group_id`>0 AND cpo.`in_group_id` < 65536 AND cor.`group_id`>0 AND cor.`group_id` IS NOT NULL");

$installer->run("UPDATE `{$installer->getTable('catalog/product_option_type_value')}` AS cpotv, `{$installer->getTable('customoptions/relation')}` AS cor
    SET cpotv.`in_group_id`=(cor.`group_id` * 65535) + cpotv.`in_group_id`
    WHERE cpotv.`option_id`=cor.`option_id` AND cpotv.`in_group_id`>0 AND cpotv.`in_group_id` < 65536 AND cor.`group_id`>0 AND cor.`group_id` IS NOT NULL");


$installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option_type_value')}` CHANGE `dependent_ids` `dependent_ids` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''");
$installer->run("UPDATE IGNORE `{$this->getTable('core_config_data')}` SET `path` = REPLACE(`path`,'tl_sales/','tl_catalog/') WHERE `path` LIKE 'tl_sales/customoptions/%'");

if ($installer->getConnection()->tableColumnExists($installer->getTable('customoptions/group'), 'store_id')) {
    $installer->run("ALTER TABLE `{$installer->getTable('customoptions/group')}` DROP `store_id`;");
}


if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'div_class')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'), 'div_class', "varchar(64) NOT NULL default ''"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'weight')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'), 'weight', "DECIMAL( 12, 4 ) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product'), 'absolute_weight')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product'), 'absolute_price', "TINYINT (1) NOT NULL DEFAULT 0"
    );

    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product'), 'absolute_weight', "TINYINT (1) NOT NULL DEFAULT 0"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('customoptions/group'), 'absolute_weight')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('customoptions/group'), 'absolute_price', "TINYINT (1) NOT NULL DEFAULT 0"
    );

    $installer->getConnection()->addColumn(
        $installer->getTable('customoptions/group'), 'absolute_weight', "TINYINT (1) NOT NULL DEFAULT 0"
    );
}


if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'view_mode') && $installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'is_enabled')) {
    $installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option')}` CHANGE `is_enabled` `view_mode` TINYINT(1) NOT NULL DEFAULT '1';");
}


if ($installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'view_mode')) {
    $installer->run("INSERT IGNORE INTO `{$installer->getTable('customoptions/option_view_mode')}` (`option_id`, `store_id`, `view_mode`) SELECT  `option_id`, 0 AS `store_id`, `view_mode` FROM  `{$installer->getTable('catalog/product_option')}`;");
    $installer->getConnection()->dropColumn(
        $installer->getTable('catalog/product_option'),
        'view_mode'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'sku_policy')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'sku_policy',
        "TINYINT( 1 ) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_price'), 'special_price')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_price'),
        'special_price',
        "DECIMAL(12, 4) NULL DEFAULT NULL"
    );
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_price'),
        'special_comment',
        "VARCHAR(255) NOT NULL DEFAULT ''"
    );
}


if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'image_mode')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'image_mode',
        "TINYINT(1) NOT NULL DEFAULT '1'"
    );

    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'exclude_first_image',
        "TINYINT(1) NOT NULL DEFAULT '0'"
    );
}

// fill image table
if ($installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'image_path')) {
    $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    $helper = Mage::helper('customoptions');
    $select = $connection->select()->from($installer->getTable('catalog/product_option_type_value'), array('option_type_id', 'image_path'))->where("image_path <> '' AND image_path IS NOT NULL");
    $allOptionValueImages = $connection->fetchAll($select);

    $installer->run('LOCK TABLES '. $connection->quoteIdentifier($installer->getTable('customoptions/option_type_image'), true) .' WRITE;');
    foreach($allOptionValueImages as $opValImg) {
        $result = $helper->getCheckImgPath($opValImg['image_path']);
        if ($result) {
            list($imagePath, $fileName) = $result;
            $imageFile = $imagePath . $fileName;
            $connection->insert($installer->getTable('customoptions/option_type_image'), array('option_type_id'=>$opValImg['option_type_id'],'image_file'=>$imageFile));
        }
    }
    $installer->run('UNLOCK TABLES;');

    $installer->getConnection()->dropColumn($installer->getTable('catalog/product_option_type_value'), 'image_path');
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product'), 'sku_policy')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product'),
        'sku_policy',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );

    $installer->run("
        UPDATE `{$installer->getTable('catalog_product_entity')}` AS t1 SET t1.`sku_policy` = 3 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `product_id` = t1.`entity_id` AND `sku_policy` = 3) > 0;
        UPDATE `{$installer->getTable('catalog_product_option')}` SET `sku_policy` = 0 WHERE `sku_policy` = 3;
    ");
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('customoptions/group'), 'sku_policy')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('customoptions/group'),
        'sku_policy',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('customoptions/group'), 'update_inventory')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('customoptions/group'),
        'update_inventory',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
}

$installer->run("UPDATE IGNORE `{$this->getTable('core_config_data')}` SET `path` = REPLACE(`path`,'tl_catalog/customoptions/option_sku_price_linking_enabled','tl_catalog/customoptions/assigned_product_attributes') WHERE `path` LIKE 'tl_catalog/customoptions/%'");

// fix and clean up the debris of tables whith options
$installer->run("
    DELETE t1 FROM `{$installer->getTable('catalog_product_option')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_entity')}` WHERE `entity_id` = t1.`product_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('catalog_product_option_title')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `option_id` = t1.`option_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('catalog_product_option_price')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `option_id` = t1.`option_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('catalog_product_option_type_value')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `option_id` = t1.`option_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('catalog_product_option_type_title')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option_type_value')}` WHERE `option_type_id` = t1.`option_type_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('catalog_product_option_type_price')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option_type_value')}` WHERE `option_type_id` = t1.`option_type_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('customoptions/option_type_image')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option_type_value')}` WHERE `option_type_id` = t1.`option_type_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('custom_options_relation')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `option_id` = t1.`option_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('customoptions/option_view_mode')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `option_id` = t1.`option_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('customoptions/option_description')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `option_id` = t1.`option_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('customoptions/option_default')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option')}` WHERE `option_id` = t1.`option_id`) = 0;
    DELETE t1 FROM `{$installer->getTable('customoptions/option_type_tier_price')}` AS t1 WHERE (SELECT COUNT(*) FROM `{$installer->getTable('catalog_product_option_type_price')}` WHERE `option_type_price_id` = t1.`option_type_price_id`) = 0;
");

$installer->endSetup();
