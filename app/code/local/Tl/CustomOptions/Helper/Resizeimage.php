<?php

class Tl_CustomOptions_Helper_Resizeimage extends Mage_Core_Helper_Abstract {
	
    public function generateImage($basepath, $newpath, $width, $height) {
        //echo "ee{$basepath}".$width; exit;
        $imageObj = new Varien_Image($basepath);
        $imageObj->constrainOnly(TRUE);
        $imageObj->keepAspectRatio(FALSE);
        $imageObj->keepFrame(FALSE);
        $imageObj->keepTransparency(True);
        $imageObj->resize($width, $height);
        $imageObj->save($newpath);
        return $newpath;
    }
}
