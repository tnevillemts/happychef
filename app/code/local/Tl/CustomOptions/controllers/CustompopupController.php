<?php

class Tl_CustomOptions_CustompopupController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {



        $customerId = $_POST['customerId'];
        $productId = $_POST['prId'];
        $savingName = $_POST['sname'];
        $oid = $_POST['oid'];
        


        $customOptionModel = Mage::getModel('customoptions/artwork');


        try {

            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $path = Mage::getBaseDir('media') . DS . 'artwork' . DS;
            $imgName = $_FILES['image']['name'];
            $path_parts = pathinfo($imgName);
            $image_path = $path_parts['filename'] . '_' . time() . '.' . $path_parts['extension'];
            $uploader->save($path, $image_path);

            $customOptionModel->setProductId($productId);
            $customOptionModel->setCustomerId($customerId);
            $customOptionModel->setSaveName($savingName);
            $customOptionModel->setOptionId($oid);
            $customOptionModel->setImgPath($image_path);
            $customOptionModel->save();
        } catch (Exception $e) {
            $result = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }
        $customOptionModel->getCollection()->setOrder('art_id', 'DESC')->getFirstItem();
        
        $mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'artwork/';

        $srtF = $mediaUrl . $image_path;

//        echo '<li class="uploaded-image-x" style="width: 60px !important; float: left; list-style: none; position: relative; margin-right: 10px;"><img width="60" height="60" class="swatch image-to-select" id="" src="' . $srtF . '" title="" /></li>';

        
        $imageslide = '';
        $imageslide['imageslide']= $this->getLayout()->createBlock('core/template')->setTemplate('customoptions/artwork/imageslider.phtml')->toHtml();
//        $imageslide['activeId'] = 'active-'.$customOptionModel->getArtId();
         $imageslide['activeId'] = $customOptionModel->getArtId();
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($imageslide));
        
    }

    /* public function resizeImg($fileName, $width, $height = '')
      {
      $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
      $imageURL = $folderURL . $fileName;

      $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $fileName;
      $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "resized" . DS . $fileName;
      //if width empty then return original size image's URL
      if ($width != '') {
      //if image has already resized then just return URL
      if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
      $imageObj = new Varien_Image($basePath);
      $imageObj->constrainOnly(TRUE);
      $imageObj->keepAspectRatio(FALSE);
      $imageObj->keepFrame(FALSE);
      $imageObj->resize($width, $height);
      $imageObj->save($newPath);
      }
      $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "resized" . DS . $fileName;
      } else {
      $resizedURL = $imageURL;
      }
      return $resizedURL;
      } */
}
