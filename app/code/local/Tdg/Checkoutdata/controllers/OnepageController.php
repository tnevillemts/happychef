<?php
include_once("Mage/Checkout/controllers/OnepageController.php");
class Tdg_Checkoutdata_OnepageController extends Mage_Checkout_OnepageController
{    
    /**
     * Order success action
     */
    public function successAction()
    {
        $session = $this->getOnepage()->getCheckout();
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        $lastRecurringProfiles = $session->getLastRecurringProfileIds();
        if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
            $this->_redirect('checkout/cart');
            return;
        }

        $session->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();

	 $visitor = Mage::getModel('visitor/visitor');
	 $visitor->load(Mage::getSingleton('core/session')->getSessionId(),'session_id');

        $resource   = Mage::getSingleton('core/resource');
	 $externaldb_read = $resource->getConnection('externaldb_read');
	 $write = $resource->getConnection('core_write');

	 $sql = sprintf('SELECT max(log_id) FROM ' . Mage::getConfig()->getTablePrefix() . 'adtrack_log WHERE visitor_id = %d',$visitor->getVisitorId());

        $adtrack_log_id = $externaldb_read->fetchOne($sql);
        $sql = sprintf('UPDATE ' . Mage::getConfig()->getTablePrefix() . 'sales_flat_order SET log_id = %d, visitor_id = %d, catalog = "%s",catalog_keycode = "%s" WHERE entity_id = %d', $adtrack_log_id, $visitor->getVisitorId(),Mage::getSingleton('core/session')->getData('order_catalog'),Mage::getSingleton('core/session')->getData('order_catalog_keycode'),$this->getOnepage()->getCheckout()->getLastOrderId());
	 $write->query($sql);

	//echo 'Hello';
	//die;
    }

    /**
     * save checkout billing address
     */
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
//            $postData = $this->getRequest()->getPost('billing', array());
//            $data = $this->_filterPostData($postData);
            $data = $this->getRequest()->getPost('billing', array());
			$data1 = $this->getRequest()->getPost();
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
	     Mage::getSingleton('core/session')->setData('order_catalog',$data1['catalog']);
	     Mage::getSingleton('core/session')->setData('order_catalog_keycode',$data1['catalog_keycode']);
		 $result = $this->getOnepage()->saveBilling($data, $customerAddressId);
	     /*$requestUpsAddressData = $data;
	     if(array_key_exists('region_id',$requestUpsAddressData)){
			$reg = Mage::getModel('directory/region')->load($data['region_id']);
	     		$requestUpsAddressData['region_id'] = $reg->getCode();
	     }
	     $upsaddressdata = array('ups_address_data'=>serialize($requestUpsAddressData));
	     $ch = curl_init ('http://dev-www.happychefuniforms.com/ups/SoapXAVClient.php') ;
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	     curl_setopt($ch,CURLOPT_POST,count($upsaddressdata));
            curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($upsaddressdata));
            $res = curl_exec ($ch);
            curl_close ($ch);
            if($res == 'Success'){
            	$result = $this->getOnepage()->saveBilling($data, $customerAddressId);
            }
	     else{
		 $result = array();
		 $result['error'] = true;
		 $result['message'] = $this->__('Invalid Address');
		  $result['success'] = false;
		  //$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
		  //return;
            }*/

            if (!isset($result['error'])) {
                /* check quote for virtual */
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result['goto_section'] = 'shipping_method';
                    $result['update_section'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Shipping address save action
     */
    public function saveShippingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
			$result = $this->getOnepage()->saveShipping($data, $customerAddressId);
            /*$requestUpsAddressData = $data;
	     if(array_key_exists('region_id',$requestUpsAddressData)){
			$reg = Mage::getModel('directory/region')->load($data['region_id']);
	     		$requestUpsAddressData['region_id'] = $reg->getCode();
	     }
	     $upsaddressdata = array('ups_address_data'=>serialize($requestUpsAddressData));
	     $ch = curl_init ('http://dev-www.happychefuniforms.com/ups/SoapXAVClient.php') ;
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	     curl_setopt($ch,CURLOPT_POST,count($upsaddressdata));
            curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($upsaddressdata));
            $res = curl_exec ($ch);
            curl_close ($ch);
             if($res == 'Success'){
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);
	      }
              else{
		 $result = array();
		 $result['error'] = true;
		 $result['message'] = $this->__('Invalid Address');
		  $result['success'] = false;
		  //$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
		  //return;
            }*/

            if (!isset($result['error'])) {
                $result['goto_section'] = 'shipping_method';
                $result['update_section'] = array(
                    'name' => 'shipping-method',
                    'html' => $this->_getShippingMethodsHtml()
                );
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    } 
}
