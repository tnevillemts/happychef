<?php
include_once("Mage/Checkout/controllers/CartController.php");
class Tdg_Checkoutdata_CartController extends Mage_Checkout_CartController
{
    /**
     * Add product to shopping cart action
     */
    public function addAction()
    {        
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                //$cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );
            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        }
        catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError($message);
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        }
        catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            $this->_goBack();
        }
	 $realtedProductArray = Mage::getSingleton('core/session')->getData('related_product');
	 if(isset($realtedProductArray) && !empty($realtedProductArray))
	 {
		$key = NULL;
		foreach($realtedProductArray as $relatedKey=>$relatedProductId)
		{
			if($relatedProductId == $this->getRequest()->getParam('product'))
			{
				$key  =  (int)$relatedKey;
				break;	
			}
		}
		//$key = array_search($this->getRequest()->getParam('product'),$realtedProductArray); 
              //$this->_getSession()->addError(implode(',',$realtedProductArray));
		//$this->_getSession()->addError($key);
		
		if($key >= 0)
		{
			unset($realtedProductArray[$key]);
			Mage::getSingleton('core/session')->setData('related_product',array_filter($realtedProductArray));
		}
		//$this->_getSession()->addError(implode(',',$realtedProductArray));
		if(count($realtedProductArray) > 0)
		{
			$nextProductId = array_slice($realtedProductArray, 0, 1);
			$productObj = Mage::getModel('catalog/product')->load($nextProductId);
                        $this->getResponse()->setRedirect($productObj->getProductUrl());
		}
	 }
    }
}
