 <?php
      $installer = $this;
      $installer->startSetup();
      $installer->run("
      DROP TABLE IF EXISTS {$this->getTable('visitor_session')};
      CREATE TABLE {$this->getTable('visitor_session')} 
	(
        	`id` int(11) unsigned NOT NULL auto_increment,
        	`visitor_id` int(11) unsigned NOT NULL,
        	`session_id` char(64) NOT NULL,
        	PRIMARY KEY (`id`) 
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
      $installer->endSetup();