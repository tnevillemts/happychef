<?php

class Tdg_Visitor_Model_Visitor extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('visitor/visitor');
    }
}