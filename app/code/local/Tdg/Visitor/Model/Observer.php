<?php
class Tdg_Visitor_Model_Observer
{
	public function initByRequest($observer)
	{
		$resource   = Mage::getSingleton('core/resource');
		$externaldb_read = $resource->getConnection('externaldb_read');
		$externaldb_write = $resource->getConnection('externaldb_write');
        $visitor = Mage::getModel('visitor/visitor');
		$visitor->load(Mage::getSingleton('core/session')->getSessionId(),'session_id');
		if($visitor->getId()){
			$visitor_id = $visitor->getVisitorId();
			$customerSession = Mage::getSingleton('customer/session', array('name'=>'frontend'));
			if($customerSession->isLoggedIn()){
				$customer = Mage::getModel('customer/customer');
				$customer->load($customerSession->getCustomer()->getId());
				if(!$customer->getData('visitor_id')){
					$customer->setData('visitor_id',$visitor_id);
					$customer->save();
				}
				else{
					if($visitor_id != $customer->getData('visitor_id')){
						$write = Mage::getSingleton('core/resource')->getConnection('core_write');
						
// FIX

                        $sql_del = "DELETE FROM " . Mage::getConfig()->getTablePrefix() . "visitor_session WHERE visitor_id = :id";
                        $binds = array('id'    => $customer->getData('visitor_id'));
                        $write->query($sql_del, $binds);

                        $sql_up = "UPDATE " . Mage::getConfig()->getTablePrefix() . "visitor_session SET visitor_id = :vid WHERE visitor_id = :id";
                        $binds = array('vid'    => $customer->getData('visitor_id'), 'id' => $visitor_id);
                        $write->query($sql_up, $binds);

                        $sql_visitor_page_views = "UPDATE " . Mage::getConfig()->getTablePrefix() . "visitor_page_views SET visitor_id = :vid WHERE visitor_id = :id";
                        $binds = array('vid'    => $customer->getData('visitor_id'), 'id' => $visitor_id);
                        $externaldb_write->query($sql_visitor_page_views, $binds);

                        $sql_visitor_browser_session = "UPDATE " . Mage::getConfig()->getTablePrefix() . "visitor_browser_session SET visitor_id = :vid WHERE visitor_id = :id";
                        $binds = array('vid'    => $customer->getData('visitor_id'), 'id' => $visitor_id);
                        $externaldb_write->query($sql_visitor_browser_session, $binds);

                        $sql_adtrack_log = "UPDATE " . Mage::getConfig()->getTablePrefix() . "adtrack_log SET visitor_id = :vid WHERE visitor_id = :id";
                        $binds = array('vid'    => $customer->getData('visitor_id'), 'id' => $visitor_id);
                        $externaldb_write->query($sql_adtrack_log, $binds);

                        Mage::getSingleton('core/session')->setGuestVisitorId('');
					}

                    $visitor_id = $customer->getData('visitor_id');
				}
			}

		} else {

            if(!Mage::getSingleton('core/session')->getGuestVisitorId()) {
                $visitor_id = $this->generateUniqueSessionId();
                Mage::getSingleton('core/session')->setGuestVisitorId($visitor_id);
            } else {
                $visitor_id = Mage::getSingleton('core/session')->getGuestVisitorId();
            }

            $visitor->setData('session_id',Mage::getSingleton('core/session')->getSessionId());
			$visitor->setData('visitor_id',$visitor_id);
			$visitor->save();
		}
		if(Mage::app()->getRequest()->getControllerName() == 'category'){

            //$sql = sprintf("INSERT INTO " . Mage::getConfig()->getTablePrefix() . "visitor_page_views(url,visitor_id,category_id) VALUES('%s',%d,%d)", Mage::helper("core/url")->getCurrentUrl(),$visitor_id,Mage::app()->getRequest()->getParam('id'));
            //$externaldb_write->query($sql);

            $sql = "INSERT INTO " . Mage::getConfig()->getTablePrefix() . "visitor_page_views(url,visitor_id,category_id) VALUES(:url,:vid,:id)";
            $binds = array(
                'url'=>Mage::helper("core/url")->getCurrentUrl(),
                'vid'    => $visitor_id,
                'id' => Mage::app()->getRequest()->getParam('id')
            );
            $externaldb_write->query($sql, $binds);

		}
		if(Mage::app()->getRequest()->getControllerName() == 'product')
		{
            $product = Mage::getModel('catalog/product')->load(Mage::app()->getRequest()->getParam('id'));
            $sql = "INSERT INTO " . Mage::getConfig()->getTablePrefix() . "visitor_page_views(url,visitor_id,sku) VALUES(:url,:vid,:sku)";
            $binds = array(
                'url'=>Mage::helper("core/url")->getCurrentUrl(),
                'vid'    => $visitor_id,
                'sku' => $product->getSku()
            );
            $externaldb_write->query($sql, $binds);

		} else {

            $sql = "INSERT INTO " . Mage::getConfig()->getTablePrefix() . "visitor_page_views(url,visitor_id) VALUES(:url,:vid)";
            $binds = array(
                'url'=>Mage::getUrl('*/*/*'),
                'vid'    => $visitor_id
            );
            $externaldb_write->query($sql, $binds);


        }
		$visitorData = Mage::getSingleton('core/session')->getVisitorData();
		if($visitorData['first_visit_at'] && $visitorData['last_visit_at']){

            $sql = "INSERT INTO " . Mage::getConfig()->getTablePrefix() . "visitor_browser_session(visitor_id,browser_start_time,browser_finish_time) VALUES(:vid,:stime,:ftime)";
            $binds = array(
                'vid'    => $visitor_id,
                'stime' => $visitorData['first_visit_at'],
                'ftime' => $visitorData['last_visit_at']
            );
            $externaldb_write->query($sql, $binds);

		}
	}

	public function generateUniqueSessionId()
	{
		//$randomNumber = rand(99999999, 9999999999);
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
// FIX
		$sql = sprintf('SELECT FLOOR(999999999 + RAND() * 99999) AS random_number FROM ' . Mage::getConfig()->getTablePrefix() . 'visitor_session WHERE "random_number" NOT IN (SELECT visitor_id FROM ' . Mage::getConfig()->getTablePrefix() . 'visitor_session) LIMIT 1');
		$randomNumber = $read->fetchOne($sql);
		if(!$randomNumber)
		{
			$randomNumber = rand(99999999, 9999999999);
		}
		return $randomNumber;
	}
	
	public function trackCategory($observer)
	{
		$resource   = Mage::getSingleton('core/resource');
		$externaldb_write = $resource->getConnection('externaldb_write');
		$visitor = Mage::getModel('visitor/visitor');
		$visitor->load(Mage::getSingleton('core/session')->getSessionId(),'session_id');
		if($visitor->getId()){

            $sql = "INSERT INTO " . Mage::getConfig()->getTablePrefix() . "category_track(category_id,visitor_id) VALUES(:cid,:vid)";
            $binds = array(
                'cid'    => $observer->getEvent()->getCategory()->getId(),
                'vid'    => $visitor->getVisitorId()
            );
            $externaldb_write->query($sql, $binds);

		}
	}
	
	public function trackProduct($observer)
	{
		$resource   = Mage::getSingleton('core/resource');
		$externaldb_write = $resource->getConnection('externaldb_write');
		$visitor = Mage::getModel('visitor/visitor');
		$visitor->load(Mage::getSingleton('core/session')->getSessionId(),'session_id');
		if($visitor->getId()){

            $sql = "INSERT INTO " . Mage::getConfig()->getTablePrefix() . "product_track(sku,visitor_id) VALUES(:sku,:vid)";
            $binds = array(
                'sku'    => $observer->getEvent()->getProduct()->getSku(),
                'vid'    => $visitor->getVisitorId()
            );
            $externaldb_write->query($sql, $binds);

		}
	}
}
