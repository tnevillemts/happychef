<?php

class Tdg_Adtrack_Model_Mysql4_Adgroup extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the adtrack_id refers to the key field in your database table.
        $this->_init('adtrack/adgroup', 'adgroup_id');
    }
}