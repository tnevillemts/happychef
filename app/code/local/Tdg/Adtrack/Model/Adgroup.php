<?php

class Tdg_Adtrack_Model_Adgroup extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('adtrack/adgroup');
    }
}