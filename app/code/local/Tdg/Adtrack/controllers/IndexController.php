<?php
class Tdg_Adtrack_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
	$pathInfo = '';
	$format = '';

	// Is this the old or new URL format ?
	$pathInfo = $this->getRequest()->getPathInfo();
	if($pathInfo == '/adtrack/index/') {
		$format = 'new';
	}
	else if($pathInfo == '/adtrack/') {
		$format = 'old';
	}

	// Get the URL params.
	$params = $this->getRequest()->getParams();
	$externalRead = Mage::getSingleton('core/resource')->getConnection('adtrack_read');

	// Only use parameters that match in the database.
	$lAdID 		= '';
	$lSourceID 	= '';
	$lCampaignID 	= '';
	$lAdGroupID 	= '';
	$lRedirectID    = '';

	if(isset($params['AdID'])) {
		$sql_adid = "SELECT adid_id FROM " . Mage::getConfig()->getTablePrefix() . "adtrack_adid";
		if($format == 'old') {
			$sql_adid .= " WHERE old_adid_id = " . $params['AdID'];
		} else {
			$sql_adid .= " WHERE adid_id = " . $params['AdID'];
		}
		$lAdID = $externalRead->fetchOne($sql_adid);
	} 
	if(isset($params['SourceID'])) {
		$sql_source = "SELECT source_id FROM " . Mage::getConfig()->getTablePrefix() . "adtrack_source";
		if($format == 'old') {
			$sql_source .= " WHERE old_source_id = " . $params['SourceID'];
		} else {
			$sql_source .= " WHERE source_id = " . $params['SourceID'];
		}
		$lSourceID = $externalRead->fetchOne($sql_source);
	}
	if(isset($params['CampaignID'])) {
		$sql_campaign = "SELECT campaign_id FROM " . Mage::getConfig()->getTablePrefix() . "adtrack_campaign";
		if($format == 'old') {
			$sql_campaign .= " WHERE old_campaign_id = " . $params['CampaignID'];
		} else {
			$sql_campaign .= " WHERE campaign_id = " . $params['CampaignID'];
		}
		$lCampaignID = $externalRead->fetchOne($sql_campaign);
	}
	if(isset($params['AdGroupID'])) {
		$sql_adgroup = "SELECT adgroup_id FROM " . Mage::getConfig()->getTablePrefix() . "adtrack_adgroup";
		if($format == 'old') {
			$sql_adgroup .= " WHERE old_adgroup_id = " . $params['AdGroupID'];
		} else {
			$sql_adgroup .= " WHERE adgroup_id = " . $params['AdGroupID'];
		} 
		$lAdGroupID = $externalRead->fetchOne($sql_adgroup);
	}

	// If there is a redirect Id then capture that if it can be found in the database.
	if(isset($params['RedirectID'])) {
		if($format == 'new') {
			$sql_redirect = "SELECT redirect_id from " . Mage::getConfig()->getTablePrefix() . "adtrack_redirect WHERE redirect_id = " . $params['RedirectID'];
			$lRedirectID = $externalRead->fetchOne($sql_redirect);
		}
	}
        
	$adtrackLog = Mage::getModel('adtrack/log');
	//$keywords = $params['Keywords'];
	$visitor = Mage::getModel('visitor/visitor');
	$visitor->load(Mage::getSingleton('core/session')->getSessionId(),'session_id');
	$adtrackLog->setVisitorId($visitor->getVisitorId());

	// Log any Url parameters. Since we are only using URL parameters that match in the
	// database, anything that didn't match will be recorded as blank in the logs.
	if(!empty($lSourceID)) {
		$adtrackLog->setSourceId($lSourceID);
	}
	if(!empty($lCampaignID)) {
		$adtrackLog->setCampaignId($lCampaignID);
	}
	if(!empty($lAdGroupID)) {
		$adtrackLog->setAdgroupId($lAdGroupID);
	}
	if(!empty($lAdID)) {
		$adtrackLog->setAdidId($lAdID);
	}
        if(!empty($lRedirectID)) {
		$adtrackLog->setRedirectId($lRedirectID);
	}
	$adtrackLog->setKeyword($params['Keywords']);
        $adtrackLog->setLoggedTime(Mage::getModel('core/date')->timestamp(time()));

	$adtrackLog->save();

	// The presense of a redirectID in the params takes priority.
        if(!empty($lRedirectID)) {
		$redirect = Mage::getModel('adtrack/redirect')->load($lRedirectID);
	} else if(!empty($lAdID)) {
		$adid = Mage::getModel('adtrack/adid')->load($lAdID);
		if($adid->getId()) {
			if($adid->getRedirectId()) {
				$redirect = Mage::getModel('adtrack/redirect')->load($adid->getRedirectId());
			} else {
				$this->_redirect();
			}
		} else {
			$this->_redirect();
		}
	} else {
		$this->_redirect();
	}

	if($redirect != NULL && $redirect->getId()) {
		if($redirect->getCPage()) {
			$this->_redirect($redirect->getCPage());
		}
		else if($redirect->getSku() && $redirect->getColor()) {
			$adtrackLog->load($adtrackLog->getId());
			$adtrackLog->setSku($redirect->getSku());
			$adtrackLog->save();
			$product = Mage::getModel('catalog/product');
			$product->load($product->getIdBySku($redirect->getSku()));
			$url = Mage::getBaseUrl().$product->getData('url_path').'?color='.$redirect->getColor();
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		}
		else if($redirect->getSku()) {
			$adtrackLog->load($adtrackLog->getId());
			$adtrackLog->setSku($redirect->getSku());
			$adtrackLog->save();
			$product = Mage::getModel('catalog/product');
			$product->load($product->getIdBySku($redirect->getSku()));
			$this->_redirect($product->getData('url_path'));
		}
		else if($redirect->getCatFilterId() && $redirect->getCatId()) {
			$adtrackLog->load($adtrackLog->getId());
			$adtrackLog->setCategoryId($redirect->getCatId());
			$adtrackLog->save();
			$category = Mage::getModel('catalog/category')->load($redirect->getCatId());
			$url = Mage::getBaseUrl().$category->getData('url_path').'?'.$redirect->getCatFilterId();
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		}
		else if($redirect->getCatId()) {
			$adtrackLog->load($adtrackLog->getId());
			$adtrackLog->setCategoryId($redirect->getCatId());
			$adtrackLog->save();
			$category = Mage::getModel('catalog/category')->load($redirect->getCatId());
			$this->_redirect($category->getData('url_path'));
		}
		else if($redirect->getSearchQuery()) {
			$url = Mage::getBaseUrl().'catalogsearch/result/?q='.$redirect->getSearchQuery();
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		}
		else if($redirect->getExtPageId()) {
			$extpage = Mage::getModel('adtrack/extpage')->load($redirect->getExtPageId());
			Mage::app()->getFrontController()->getResponse()->setRedirect($extpage->getUrl());
		}
		else if($redirect->getPageId()) {
			$cms = Mage::getModel('cms/page')->load($redirect->getPageId());
			$this->_redirect($cms->getData('identifier'));
		}
		/*else if($redirect->getParamId()) {
			$this->_redirect();
		}*/
		else {
			$this->_redirect();
		}
	}
	else {
		$this->_redirect();
	}

   	$this->loadLayout();     
   	$this->renderLayout();
    }
}


