<?php
class Tdg_Adtrack_NewController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$params = $this->getRequest()->getParams();
//drichter
error_log(__FILE__);
		//print_r($params);
		//die;
		$externalRead = Mage::getSingleton('core/resource')->getConnection('adtrack_read');
		if(!empty($params)){
			$adtrackLog = Mage::getModel('adtrack/log');
			//$keywords = $params['Keywords'];
			$visitor = Mage::getModel('visitor/visitor');
			$visitor->load(Mage::getSingleton('core/session')->getSessionId(),'session_id');
			$adtrackLog->setVisitorId($visitor->getVisitorId());
			$adtrackLog->setSourceId($params['SourceID']);
			$adtrackLog->setCampaignId($params['CampaignID']);
			$adtrackLog->setAdgroupId($params['AdGroupID']);
			$adtrackLog->setAdidId($params['AdID']);
			$adtrackLog->setKeyword($params['Keywords']);
			/*$adtrackLog->setSku('');
			$adtrackLog->setCategoryId('');
			$adtrackLog->setSubId('');
			$adtrackLog->setEmailId('');*/
			$adtrackLog->save();

			if($params['AdID']){
				$adid = Mage::getModel('adtrack/adid')->load($params['AdID']);
				
				if($adid->getId()){
					if($adid->getRedirectId()){
						$redirect = Mage::getModel('adtrack/redirect')->load($adid->getRedirectId());
						if($redirect->getId()){
							if($redirect->getCPage()){
								$this->_redirect($redirect->getCPage());
							}
							else if($redirect->getSku() && $redirect->getColor()){
								$adtrackLog->load($adtrackLog->getId());
								$adtrackLog->setSku($redirect->getSku());
								$adtrackLog->save();
								$product = Mage::getModel('catalog/product');
								$product->load($product->getIdBySku($redirect->getSku()));
								$url = Mage::getBaseUrl().$product->getData('url_path').'?color='.$redirect->getColor();
								Mage::app()->getFrontController()->getResponse()->setRedirect($url);
							}
							else if($redirect->getSku()){
								$adtrackLog->load($adtrackLog->getId());
								$adtrackLog->setSku($redirect->getSku());
								$adtrackLog->save();
								$product = Mage::getModel('catalog/product');
								$product->load($product->getIdBySku($redirect->getSku()));
								$this->_redirect($product->getData('url_path'));
							}
							else if($redirect->getCatFilterId() && $redirect->getCatId()){
								$adtrackLog->load($adtrackLog->getId());
								$adtrackLog->setCategoryId($redirect->getCatId());
								$adtrackLog->save();
								$category = Mage::getModel('catalog/category')->load($redirect->getCatId());
								$url = Mage::getBaseUrl().$category->getData('url_path').'?'.$redirect->getCatFilterId();
								Mage::app()->getFrontController()->getResponse()->setRedirect($url);
							}
							else if($redirect->getCatId()){
								$adtrackLog->load($adtrackLog->getId());
								$adtrackLog->setCategoryId($redirect->getCatId());
								$adtrackLog->save();
								$category = Mage::getModel('catalog/category')->load($redirect->getCatId());
								$this->_redirect($category->getData('url_path'));
							}
							else if($redirect->getSearchQuery()){
								$url = Mage::getBaseUrl().'catalogsearch/result/?q='.$redirect->getSearchQuery();
								Mage::app()->getFrontController()->getResponse()->setRedirect($url);
							}
							else if($redirect->getExtPageId()){
								$extpage = Mage::getModel('adtrack/extpage')->load($redirect->getExtPageId());
								Mage::app()->getFrontController()->getResponse()->setRedirect($extpage->getUrl());

							}
							else if($redirect->getPageId()){
								$cms = Mage::getModel('cms/page')->load($redirect->getPageId());
								$this->_redirect($cms->getData('identifier'));
							}
							/*else if($redirect->getParamId()){
								$this->_redirect();
							}*/
							else{
								$this->_redirect();
							}
						}
						else{
								$this->_redirect();
						}
					}
					else{
						$this->_redirect();
					}
				}
				else{
					$this->_redirect();
				}
			}
			else{			
				$this->_redirect();
			}
		}
		$this->loadLayout();     
		$this->renderLayout();
    }
}
