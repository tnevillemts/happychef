<?php

class Tdg_Adtrack_Adminhtml_DirectreportsController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('adtrack/directreports')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Adtrack Direct Report'), Mage::helper('adminhtml')->__('Adtrack Direct Report'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}	
}