<?php

class Tdg_Adtrack_Block_Adminhtml_Redirect_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('redirectGrid');
      $this->setDefaultSort('redirect_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('adtrack/redirect')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('redirect_id', array(
          'header'    => Mage::helper('adtrack')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'redirect_id',
      ));

	$websitesList = array();

	foreach(Mage::app()->getWebsites() as $websites){
		$websitesList[$websites['website_id']] = $websites['name'];
	}

      $this->addColumn('store_id', array(
          'header'    => Mage::helper('adtrack')->__('Website'),
          'align'     =>'left',
          'index'     => 'store_id',
	   'type'      => 'options',
	   'options'   => $websitesList,
      ));

	/*$adidCollection = Mage::getModel('adtrack/adid')->getCollection();

	foreach($adidCollection as $adid){
		$adidList[$adid->getId()] = $adid->getName();
					
	}

	 $this->addColumn('adid_id', array(
          'header'    => Mage::helper('adtrack')->__('Ad Id'),
          'align'     =>'left',
          'index'     => 'adid_id',
	   'type'      => 'options',
	   'options'   => $adidList,
      ));*/

	 $this->addColumn('c_page', array(
          'header'    => Mage::helper('adtrack')->__('Custom Page'),
          'align'     =>'left',
          'index'     => 'c_page',
      ));

	 $this->addColumn('sku', array(
          'header'    => Mage::helper('adtrack')->__('Sku'),
          'align'     =>'left',
          'index'     => 'sku',
      ));

	$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','color');
	$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
	$attributeOptions = $attribute ->getSource()->getAllOptions();

	//print_r($attributeOptions);
	//$countOptions = count($attributeOptions);
	$colorList = array();

	foreach($attributeOptions as $attributeOption){
		if($attributeOption['value']!=''){
			$colorList[$attributeOption['label']] = 	$attributeOption['label'];
		}
	}

	 $this->addColumn('color', array(
          'header'    => Mage::helper('adtrack')->__('Color'),
          'align'     =>'left',
          'index'     => 'color',
	    'type'      => 'options',
	   'options'   => $colorList,
      ));

	$collection = Mage::getModel('catalog/category')->getCollection();
	$collection->addAttributeToSelect('name');
	$collection->addFieldToFilter('level',2);
	$collection->addAttributeToFilter('is_active',1);
	$collection->addAttributeToFilter('include_in_menu',1);
	$collection->addOrderField('name');
	
	$categoryList = array();

	foreach($collection as $category){
		$categoryList[$category->getId()] = $category->getName();
	}

	 $this->addColumn('cat_id', array(
          'header'    => Mage::helper('adtrack')->__('Cat Id'),
          'align'     =>'left',
          'index'     => 'cat_id',
	   'type'      => 'options',
	   'options'   => $categoryList,
      ));


	$this->addColumn('cat_filter_id', array(
          'header'    => Mage::helper('adtrack')->__('Cat Filter Id'),
          'align'     =>'left',
          'index'     => 'cat_filter_id',
      ));

	$this->addColumn('search_query', array(
          'header'    => Mage::helper('adtrack')->__('Search Query'),
          'align'     =>'left',
          'index'     => 'search_query',
      ));

       $collection = Mage::getModel('adtrack/extpage')->getCollection();
	
	$extpageList = array();

	foreach($collection as $extpage){
		$extpageList [$extpage->getId()] = $extpage->getUrl();
	}
	

	$this->addColumn('ext_page_id', array(
          'header'    => Mage::helper('adtrack')->__('Ext Page Id'),
          'align'     =>'left',
          'index'     => 'ext_page_id',
	   'type'      => 'options',
	   'options'   => $extpageList,
      ));

	$pagecollection = Mage::getModel('cms/page')->getCollection();

	$pageList = array();

	foreach($pagecollection as $page){
		$pageList[$page->getId()] = $page->getTitle();
	}

	$this->addColumn('page_id', array(
          'header'    => Mage::helper('adtrack')->__('Page Id'),
          'align'     =>'left',
          'index'     => 'page_id',
	    'type'      => 'options',
	   'options'   => $pageList,
      ));

	$collection = Mage::getModel('adtrack/paramid')->getCollection();
	
	$paramidList = array();

	foreach($collection as $paramid){
		$paramidList[$paramid->getId()] = $paramid->getUrlParam();
	}

	$this->addColumn('param_id', array(
          'header'    => Mage::helper('adtrack')->__('Param Id'),
          'align'     =>'left',
          'index'     => 'param_id',
	    'type'      => 'options',
	   'options'   => $paramidList,
      ));

	  $this->addColumn('update_time', array(
          'header'    => Mage::helper('adtrack')->__('Updated At'),
          'align'     =>'left',
          'index'     => 'update_time',
		  'type'      => 'datetime',
      ));
	  
	   $this->addColumn('created_time', array(
          'header'    => Mage::helper('adtrack')->__('Created At'),
          'align'     =>'left',
          'index'     => 'created_time',
		  'type'      => 'datetime',
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('adtrack')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('adtrack')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('adtrack')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('adtrack')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('adid_id');
        $this->getMassactionBlock()->setFormFieldName('adid');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('adtrack')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('adtrack')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('adtrack/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('adtrack')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('adtrack')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}