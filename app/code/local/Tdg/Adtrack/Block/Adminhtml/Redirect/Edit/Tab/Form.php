<?php

class Tdg_Adtrack_Block_Adminhtml_Redirect_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('redirect_form', array('legend'=>Mage::helper('adtrack')->__('Redirect Information')));
     
	$websitesList = array();

	foreach(Mage::app()->getWebsites() as $websites){
		$websitesList[] = array(
						'value' => $websites['website_id'],
						'label' => $websites['name'],
					   );
	}

	$fieldset->addField('store_id', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Website'),
          'name'      => 'store_id',
          'values'    => $websitesList,
      ));

	$adidList = array(array(
						'value' => '',
						'label' => '',
					   ),);

	/*$adidCollection = Mage::getModel('adtrack/adid')->getCollection();

	foreach($adidCollection as $adid){
		$adidList[] = array(
						'value' => $adid->getId(),
						'label' => $adid->getName(),
					   );
	}

	 $fieldset->addField('adid_id', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Ad Id'),
          'name'      => 'adid_id',
	   'values'    => $adidList,
      ));*/

      $fieldset->addField('c_page', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Custom Page'),
          'name'      => 'c_page',
      ));

	$fieldset->addField('sku', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Sku'),
          'name'      => 'sku',
      ));

	$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','color');
	$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
	$attributeOptions = $attribute ->getSource()->getAllOptions();

	//print_r($attributeOptions);
	//$countOptions = count($attributeOptions);
	$colorList = array(array(
						'value' => '',
						'label' => '',
					   ),);

	foreach($attributeOptions as $attributeOption){
		if($attributeOption['value']!=''){
			$colorList[] = 	array('value' => $attributeOption['label'], 'label' => $attributeOption['label']);
		}
	}

     $fieldset->addField('color', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Color'),
          'name'      => 'color',
	   'values'    => $colorList,
      ));

	$collection = Mage::getModel('catalog/category')->getCollection();
	$collection->addAttributeToSelect('name');
	$collection->addFieldToFilter('level',2);
	$collection->addAttributeToFilter('is_active',1);
	$collection->addAttributeToFilter('include_in_menu',1);
	$collection->addOrderField('name');

	$categoryList = array(array(
						'value' => '',
						'label' => '',
					   ),);
	foreach($collection as $category){
		$categoryList[] = array(
						'value' => $category->getId(),
						'label' =>$category->getName(),
					   );
	}


	  $fieldset->addField('cat_id', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Cat Id'),
          'name'      => 'cat_id',
	   'values'    => $categoryList,
      ));

	  $fieldset->addField('cat_filter_id', 'textarea', array(
          'label'     => Mage::helper('adtrack')->__('Cat Filter Id'),
          'name'      => 'cat_filter_id',
	   'note' =>  'Seperate each filter with comma (,)'
      ));

	$fieldset->addField('search_query', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Search Query'),
          'name'      => 'search_query',
      ));

	$extPageList = array(array(
						'value' => '',
						'label' => '',
					   ),);

	$collection = Mage::getModel('adtrack/extpage')->getCollection();

	foreach($collection as $extpage){
		$extPageList[] = array('value'=> $extpage->getId(), 'label'=>$extpage->getUrl());
	}

	$fieldset->addField('ext_page_id', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Ext Page Id'),
          'name'      => 'ext_page_id',
	   'values' => $extPageList,
      ));

	$pagecollection = Mage::getModel('cms/page')->getCollection();
	//$pagecollection->addAttributeToSelect('*');
	//$pagecollection->addFieldToFilter('is_active',1);

	$pageList = array(array(
						'value' => '',
						'label' => '',
					   ),);
	foreach($pagecollection as $page){
		$pageList[] = array(
						'value' => $page->getId(),
						'label' => $page->getTitle(),
					   );
	}

	$fieldset->addField('page_id', 'select', array(
          'label'     => Mage::helper('adtrack')->__('CMS Page Id'),
          'name'      => 'page_id',
	    'values'    => $pageList,
      ));

	$ParamList = array(array(
						'value' => '',
						'label' => '',
					   ),);
       $collection = Mage::getModel('adtrack/paramid')->getCollection();

	foreach($collection as $paramid){
		$ParamList [] = array('value'=> $paramid->getId(), 'label'=>$paramid->getUrlParam());
	}


	$fieldset->addField('param_id', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Param Id'),
          'name'      => 'param_id',
	   'values'    => $ParamList,
      ));

      if ( Mage::getSingleton('adminhtml/session')->getRedirectData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getRedirectData());
          Mage::getSingleton('adminhtml/session')->setRedirectData(null);
      } elseif ( Mage::registry('redirect_data') ) {
          $form->setValues(Mage::registry('redirect_data')->getData());
      }
      return parent::_prepareForm();
  }
}