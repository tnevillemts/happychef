<?php

class Tdg_Adtrack_Block_Adminhtml_Source_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('sourceGrid');
      $this->setDefaultSort('source_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('adtrack/source')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('source_id', array(
          'header'    => Mage::helper('adtrack')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'source_id',
	   'type'      => 'number',
      ));

      $this->addColumn('name', array(
          'header'    => Mage::helper('adtrack')->__('Name'),
          'align'     =>'left',
          'index'     => 'name',
      ));
	  
	   $this->addColumn('update_time', array(
          'header'    => Mage::helper('adtrack')->__('Updated At'),
          'align'     =>'left',
          'index'     => 'update_time',
		  'type'      => 'datetime',
      ));
	  
	   $this->addColumn('created_time', array(
          'header'    => Mage::helper('adtrack')->__('Created At'),
          'align'     =>'left',
          'index'     => 'created_time',
		  'type'      => 'datetime',
      ));

      $this->addColumn('status', array(
          'header'    => Mage::helper('adtrack')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));


	$this->addColumn('old_source_id', array(
          'header'    => Mage::helper('adtrack')->__('Old Source ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'old_source_id',
	   'type'      => 'number',
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('adtrack')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('adtrack')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('adtrack')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('adtrack')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('source_id');
        $this->getMassactionBlock()->setFormFieldName('source');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('adtrack')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('adtrack')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('adtrack/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('adtrack')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('adtrack')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}