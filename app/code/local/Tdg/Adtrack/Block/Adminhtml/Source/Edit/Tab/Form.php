<?php

class Tdg_Adtrack_Block_Adminhtml_Source_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('source_form', array('legend'=>Mage::helper('adtrack')->__('Source information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

	 $fieldset->addField('old_source_id', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Old Source Id'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'old_source_id',
      ));

		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('adtrack')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('adtrack')->__('Disabled'),
              ),
          ),
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getSourceData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getSourceData());
          Mage::getSingleton('adminhtml/session')->setSourceData(null);
      } elseif ( Mage::registry('source_data') ) {
          $form->setValues(Mage::registry('source_data')->getData());
      }
      return parent::_prepareForm();
  }
}