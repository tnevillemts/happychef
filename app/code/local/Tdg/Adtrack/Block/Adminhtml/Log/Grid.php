<?php

class Tdg_Adtrack_Block_Adminhtml_Log_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  	public function __construct()
  	{
      		parent::__construct();
      		$this->setId('logGrid');
      		$this->setDefaultSort('log_id');
      		$this->setDefaultDir('ASC');
      		$this->setSaveParametersInSession(true);
  	}

  	protected function _prepareCollection()
  	{
      		$collection = Mage::getModel('adtrack/log')->getCollection();
      		$this->setCollection($collection);
      		return parent::_prepareCollection();
  	}

  	protected function _prepareColumns()
  	{
      		$this->addColumn('log_id', array(
          		'header'    => Mage::helper('adtrack')->__('ID'),
          		'align'     =>'right',
          		'width'     => '50px',
          		'index'     => 'log_id',
      		));

      		$this->addColumn('visitor_id', array(
          		'header'    => Mage::helper('adtrack')->__('Visitor Id'),
          		'align'     =>'left',
          		'index'     => 'visitor_id',
      		));
	  
	   	$sources = Mage::getModel('adtrack/source')->getCollection();
	  	$sourcesOptionArray = array();
	  	foreach($sources as $source){
			$sourcesOptionArray[$source->getId()] = $source->getId() . '-' . $source->getName();
	  	}
	  
	   	$this->addColumn('source_id', array(
          		'header'    => Mage::helper('adtrack')->__('Source Id'),
          		'align'     =>'left',
          		'index'     => 'source_id',
		  	'type'      => 'options',
		  	'options'   => $sourcesOptionArray,
      		));
	  
	   	$campaigns = Mage::getModel('adtrack/campaign')->getCollection();
	  	$campaignsOptionArray = array();
	  	foreach($campaigns as $campaign){
			$campaignsOptionArray[$campaign->getId()] = $campaign->getId() . '-' . $campaign->getName();
	  	}
	  
	   	$this->addColumn('campaign_id', array(
          		'header'    => Mage::helper('adtrack')->__('Campaign Id'),
          		'align'     =>'left',
          		'index'     => 'campaign_id',
		  	'type'	  => 'options',
		  	'options'   => $campaignsOptionArray,
      		));
	  
	   	$adgroups = Mage::getModel('adtrack/adgroup')->getCollection();
	  	$adgroupsOptionArray = array();
	  	foreach($adgroups as $adgroup){
			$adgroupsOptionArray[$adgroup->getId()] = $adgroup->getId() . '-' . $adgroup->getName();
	  	}
	  
	   	$this->addColumn('adgroup_id', array(
          		'header'    => Mage::helper('adtrack')->__('Ad Group Id'),
          		'align'     =>'left',
          		'index'     => 'adgroup_id',
		  	'type'	  => 'options',
		  	'options'   => $adgroupsOptionArray,
      		));
	                 
                $adids = Mage::getModel('adtrack/adid')->getCollection();
	  	$adidsOptionArray = array();
	  	foreach($adids as $adid){
			$adidsOptionArray[$adid->getId()] = $adid->getId() . '-' . $adid->getName();
	  	}
	  
	   	$this->addColumn('adid_id', array(
          		'header'    => Mage::helper('adtrack')->__('Adid Id'),
          		'align'     =>'left',
          		'index'     => 'adid_id',
		  	'type'	  => 'options',
		  	'options'   => $adidsOptionArray,
      		));
                
                $redirects = Mage::getModel('adtrack/redirect')->getCollection();
	  	$redirectsOptionArray = array();
	  	foreach($redirects as $redirect){
			$redirectsOptionArray[$redirect->getId()] = $redirect->getId();
	  	}
	  
	   	$this->addColumn('redirect_id', array(
          		'header'    => Mage::helper('adtrack')->__('Redirect Id'),
          		'align'     =>'left',
          		'index'     => 'redirect_id',
		  	'type'	  => 'options',
		  	'options'   => $redirectsOptionArray,
      		));
	  
	   	$this->addColumn('keyword', array(
          		'header'    => Mage::helper('adtrack')->__('Keyword'),
          		'align'     =>'left',
          		'index'     => 'keyword',
      		));
	  
	   	$this->addColumn('sku', array(
          		'header'    => Mage::helper('adtrack')->__('Sku'),
          		'align'     =>'left',
          		'index'     => 'sku',
      		));
	  
	   	$this->addColumn('category_id', array(
          		'header'    => Mage::helper('adtrack')->__('Category Id'),
          		'align'     =>'left',
          		'index'     => 'category_id',
      		));
	  
	  	$this->addColumn('sub_id', array(
          		'header'    => Mage::helper('adtrack')->__('Sub Id'),
          		'align'     =>'left',
          		'index'     => 'sub_id',
      		));
	  
	  	$this->addColumn('email_id', array(
          		'header'    => Mage::helper('adtrack')->__('Email Id'),
          		'align'     =>'left',
          		'index'     => 'email_id',
      		));
	  
	   	$this->addColumn('logged_time', array(
          		'header'    => Mage::helper('adtrack')->__('Created At'),
          		'align'     =>'left',
          		'index'     => 'logged_time',
		  	'type'      => 'datetime',
      		));

		$this->addExportType('*/*/exportCsv', Mage::helper('adtrack')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('adtrack')->__('XML'));

    		return parent::_prepareColumns();
  	}

    	protected function _prepareMassaction()
    	{
        	$this->setMassactionIdField('log_id');
        	$this->getMassactionBlock()->setFormFieldName('log');

        	$this->getMassactionBlock()->addItem('delete', array(
             		'label'    => Mage::helper('adtrack')->__('Delete'),
             		'url'      => $this->getUrl('*/*/massDelete'),
             		'confirm'  => Mage::helper('adtrack')->__('Are you sure?')
        	));
        	return $this;
    	}

  	public function getRowUrl($row)
  	{
      		//return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	  	return '#';
  	}
}
