<?php

class Tdg_Adtrack_Block_Adminhtml_Adid_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('adid_form', array('legend'=>Mage::helper('adtrack')->__('Ad ID Information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

	$fieldset->addField('old_adid_id', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Old Adid Id'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'old_adid_id',
      ));


	$collection = Mage::getModel('adtrack/redirect')->getCollection();

	$redirectList = array(array('value' => '', 'label' => ''),);

	foreach($collection as $redirect){
		$redirectList[] = array('value' => $redirect->getId(), 'label' => $redirect->getId());
	}

	 $fieldset->addField('redirect_id', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Redirect Id'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'redirect_id',
	   'values'   => $redirectList,
      ));

      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('adtrack')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('adtrack')->__('Disabled'),
              ),
          ),
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getAdidData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getAdidData());
          Mage::getSingleton('adminhtml/session')->setAdidData(null);
      } elseif ( Mage::registry('adid_data') ) {
          $form->setValues(Mage::registry('adid_data')->getData());
      }
      return parent::_prepareForm();
  }
}