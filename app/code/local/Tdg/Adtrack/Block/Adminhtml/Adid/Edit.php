<?php

class Tdg_Adtrack_Block_Adminhtml_Adid_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'adtrack';
        $this->_controller = 'adminhtml_adid';
        
        $this->_updateButton('save', 'label', Mage::helper('adtrack')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('adtrack')->__('Delete'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('adid_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'adid_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'adid_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('adid_data') && Mage::registry('adid_data')->getId() ) {
            return Mage::helper('adtrack')->__("Edit Ad ID '%s'", $this->htmlEscape(Mage::registry('adid_data')->getName()));
        } else {
            return Mage::helper('adtrack')->__('Add Ad ID');
        }
    }
}