<?php

class Tdg_Adtrack_Block_Adminhtml_Adid_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('adid_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('adtrack')->__('Ad ID Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('adtrack')->__('Ad ID Information'),
          'title'     => Mage::helper('adtrack')->__('Ad ID Information'),
          'content'   => $this->getLayout()->createBlock('adtrack/adminhtml_adid_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}