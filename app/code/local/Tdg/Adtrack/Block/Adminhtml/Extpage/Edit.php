<?php

class Tdg_Adtrack_Block_Adminhtml_Extpage_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'adtrack';
        $this->_controller = 'adminhtml_extpage';
        
        $this->_updateButton('save', 'label', Mage::helper('adtrack')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('adtrack')->__('Delete'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('extpage_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'extpage_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'extpage_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('extpage_data') && Mage::registry('extpage_data')->getId() ) {
            return Mage::helper('adtrack')->__("Edit Ext Page '%s'", $this->htmlEscape(Mage::registry('extpage_data')->getId()));
        } else {
            return Mage::helper('adtrack')->__('Add Ext Page');
        }
    }
}