<?php

class Tdg_Adtrack_Block_Adminhtml_Extpage_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('extpage_form', array('legend'=>Mage::helper('adtrack')->__('Ext Page Information')));

      $fieldset->addField('url', 'text', array(
          'label'     => Mage::helper('adtrack')->__('URL'),
          'name'      => 'url',
      ));

      if ( Mage::getSingleton('adminhtml/session')->getExtpageData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getExtpageData());
          Mage::getSingleton('adminhtml/session')->setExtpageData(null);
      } elseif ( Mage::registry('extpage_data') ) {
          $form->setValues(Mage::registry('extpage_data')->getData());
      }
      return parent::_prepareForm();
  }
}