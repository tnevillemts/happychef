<?php
class Tdg_Adtrack_Block_Adminhtml_Source extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_source';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Source');
    $this->_addButtonLabel = Mage::helper('adtrack')->__('Add Source');
    parent::__construct();
  }
}