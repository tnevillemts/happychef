<?php
class Tdg_Adtrack_Block_Adminhtml_Campaigns extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
  	{
    		$this->_controller = 'adminhtml_campaigns';
    		$this->_blockGroup = 'campaigns';
    		$this->_headerText = Mage::helper('adtrack')->__('Campaigns');
    		parent::__construct();
    		//$this->removeButton('add');
  	}
}