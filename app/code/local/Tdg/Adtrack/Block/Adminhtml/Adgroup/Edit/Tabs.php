<?php

class Tdg_Adtrack_Block_Adminhtml_Adgroup_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('adgroup_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('adtrack')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('adtrack')->__('Item Information'),
          'title'     => Mage::helper('adtrack')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('adtrack/adminhtml_adgroup_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}