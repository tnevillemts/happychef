<?php

class Tdg_Adtrack_Block_Adminhtml_Adgroup_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  	public function __construct()
  	{
      		parent::__construct();
      		$this->setId('adgroupGrid');
      		$this->setDefaultSort('adgroup_id');
      		$this->setDefaultDir('ASC');
      		$this->setSaveParametersInSession(true);
  	}

  	protected function _prepareCollection()
  	{
      		$collection = Mage::getModel('adtrack/adgroup')->getCollection();
      		$this->setCollection($collection);
      		return parent::_prepareCollection();
  	}

  	protected function _prepareColumns()
  	{
      		$this->addColumn('adgroup_id', array(
          		'header'    => Mage::helper('adtrack')->__('ID'),
          		'align'     =>'right',
          		'width'     => '50px',
          		'index'     => 'adgroup_id',
	   		'type'      => 'number',
      	));

      	$this->addColumn('name', array(
       		'header'    => Mage::helper('adtrack')->__('Name'),
       		'align'     =>'left',
       		'index'     => 'name',
      	));

	$campaigns = Mage::getModel('adtrack/campaign')->getCollection();
	$campaignsOptionArray = array();
	foreach($campaigns as $campaign){
		$campaignsOptionArray[$campaign->getId()] = $campaign->getId() . '-' .$campaign->getName();
	}
					
	$this->addColumn('campaign_id', array(
          	'header'    => Mage::helper('adtrack')->__('Campaign'),
          	'align'     => 'left',
          	'width'     => '200px',
          	'index'     => 'campaign_id',
          	'type'      => 'options',
          	'options'   => $campaignsOptionArray,
      	));
	  
      	$this->addColumn('status', array(
          	'header'    => Mage::helper('adtrack')->__('Status'),
          	'align'     => 'left',
          	'width'     => '80px',
          	'index'     => 'status',
          	'type'      => 'options',
          	'options'   => array(
              	1 => 'Enabled',
              	2 => 'Disabled',
          	),
      	));

	$this->addColumn('old_adgroup_id', array(
          	'header'    => Mage::helper('adtrack')->__('Old Adgroup ID'),
          	'align'     =>'right',
          	'width'     => '50px',
          	'index'     => 'old_adgroup_id',
	   	'type'      => 'number',
      	));
	  
	$this->addColumn('update_time', array(
          	'header'    => Mage::helper('adtrack')->__('Updated At'),
          	'align'     =>'left',
          	'index'     => 'update_time',
		'type'      => 'datetime',
      	));
	  
	$this->addColumn('created_time', array(
          	'header'    => Mage::helper('adtrack')->__('Created At'),
          	'align'     =>'left',
          	'index'     => 'created_time',
		'type'      => 'datetime',
      	));
	  
        $this->addColumn('action',
            	array(
                	'header'    =>  Mage::helper('adtrack')->__('Action'),
                	'width'     => '100',
                	'type'      => 'action',
                	'getter'    => 'getId',
                	'actions'   => array(
                    		array(
                        		'caption'   => Mage::helper('adtrack')->__('Edit'),
                        		'url'       => array('base'=> '*/*/edit'),
                        		'field'     => 'id'
                    		)
                	),
                	'filter'    => false,
                	'sortable'  => false,
                	'index'     => 'stores',
                	'is_system' => true,
        	));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('adtrack')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('adtrack')->__('XML'));
	  
      		return parent::_prepareColumns();
  	}

    	protected function _prepareMassaction()
    	{
        	$this->setMassactionIdField('adgroup_id');
        	$this->getMassactionBlock()->setFormFieldName('adgroup');

        	$this->getMassactionBlock()->addItem('delete', array(
             		'label'    => Mage::helper('adtrack')->__('Delete'),
             		'url'      => $this->getUrl('*/*/massDelete'),
             		'confirm'  => Mage::helper('adtrack')->__('Are you sure?')
        	));

       		$statuses = Mage::getSingleton('adtrack/status')->getOptionArray();

        	array_unshift($statuses, array('label'=>'', 'value'=>''));
        	$this->getMassactionBlock()->addItem('status', array(
             		'label'=> Mage::helper('adtrack')->__('Change status'),
             		'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             		'additional' => array(
                    		'visibility' => array(
                        	 	'name' => 'status',
                         		'type' => 'select',
                         		'class' => 'required-entry',
                         		'label' => Mage::helper('adtrack')->__('Status'),
                         		'values' => $statuses
                     		)
             		)
        	));
        	return $this;
    	}

  	public function getRowUrl($row)
  	{
      		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}
