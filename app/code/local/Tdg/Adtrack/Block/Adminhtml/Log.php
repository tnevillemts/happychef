<?php
class Tdg_Adtrack_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_log';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Log');
    parent::__construct();
	$this->removeButton('add');
  }
}