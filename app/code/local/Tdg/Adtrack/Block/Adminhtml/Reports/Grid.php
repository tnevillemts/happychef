<?php

class Tdg_Adtrack_Block_Adminhtml_Reports_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('adtrack/reports/grid.phtml');
    }
  
    public function _getFilteredData()
    {
        $mainDb = Mage::getConfig()->getResourceConnectionConfig('default_setup')->dbname;
        $externalDb = (string)Mage::getConfig()->getNode('global/resources/adtrack_database/connection/dbname');

        $requestData = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        if($requestData) {
            $externalRead = Mage::getSingleton('core/resource')->getConnection('adtrack_read');
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $from = $requestData['from'] ;
            $to = $requestData['to'] ;
            $fromdate = new DateTime($requestData['from']);
            $requestData['from'] = $fromdate->format('Y-m-d');
            $todate = new DateTime($requestData['to']);
            $requestData['to'] = $todate->format('Y-m-d');
            $touchInterval  = $requestData['touch_interval'];
            $touchCriteria  = $requestData['touch_criteria'];
            
            $sql = sprintf("SELECT DISTINCT s.entity_id entity_id, s.log_id log_id, s.visitor_id visitor_id, s.created_at created_at
                   FROM  %s." . Mage::getConfig()->getTablePrefix() . "adtrack_log AS l LEFT JOIN " . $mainDb . "." . Mage::getConfig()->getTablePrefix() . "sales_flat_order AS s ON l.visitor_id = s.visitor_id
                   WHERE (DATE(l.logged_time) BETWEEN '%s' and '%s')
                   AND   s.entity_id IS NOT NULL
                   AND   DATEDIFF(s.created_at, l.logged_time) < '%d'",
                   $externalDb, $requestData['from'], $requestData['to'], $touchInterval);    

            $allOrders = $read->fetchAll($sql);
            $finalArray = array();
            
            if(count($allOrders) > 0) {
                $cntSales = 0;
                $totalSales = 0.0;
               
                /* Code starts for get the count of total log */
                $logCount = sprintf("SELECT COUNT(*) as log_count
                            FROM %s." . Mage::getConfig()->getTablePrefix() . "adtrack_log l
                            WHERE (DATE(l.logged_time) BETWEEN '%s' AND '%s')",
                            $externalDb, $requestData['from'], $requestData['to']);
                
                /* comment the below if filters dont consider count with filter basis */
                if($requestData['adtrack_source'] && $requestData['adtrack_aggregate'] != 'aggregate_source'){
                        $logCount = $logCount.sprintf(" AND l.source_id = %d ", $requestData['adtrack_source']);
                }
                if($requestData['adtrack_campaign'] && $requestData['adtrack_aggregate'] != 'aggregate_campaign'){
                        $logCount = $logCount.sprintf(" AND l.campaign_id = %d ", $requestData['adtrack_campaign']);
                }
                if($requestData['adtrack_group'] && $requestData['adtrack_aggregate'] != 'aggregate_group'){
                        $logCount = $logCount.sprintf(" AND l.adgroup_id = %d ", $requestData['adtrack_group']);
                }
                if($requestData['adtrack_adid'] && $requestData['adtrack_aggregate'] != 'aggregate_adid'){
                        $logCount = $logCount.sprintf(" AND l.adid_id = %d ", $requestData['adtrack_adid']);
                }
                if($requestData['adtrack_redirect_id'] && $requestData['adtrack_aggregate'] != 'aggregate_redirect'){
                        $logCount = $logCount.sprintf(" AND l.redirect_id = %d ", $requestData['adtrack_redirect_id']);
                }
                
                $totalLogCount = $read->fetchAll($logCount);                
                $FinalLogCount = $totalLogCount[0]['log_count']; 

                /* code ends for get the count of total log  */
				
                foreach($allOrders as $order) {                    
                    $sql = sprintf("SELECT l.*, s.entity_id ,s.visitor_id, s.created_at, s.base_grand_total sales, DATEDIFF(s.created_at, l.logged_time) dd
                           FROM  %s." . Mage::getConfig()->getTablePrefix() . "adtrack_log AS l LEFT JOIN " . $mainDb . "." . Mage::getConfig()->getTablePrefix() . "sales_flat_order AS s ON l.visitor_id = s.visitor_id
                           WHERE (DATE(l.logged_time) BETWEEN '%s' and '%s')
                           AND   s.entity_id IS NOT NULL
                           AND   DATEDIFF(s.created_at, l.logged_time) < '%d'
                           AND   s.entity_id = '%d'
                           AND   l.logged_time <= '%s'",
                           $externalDb, $requestData['from'], $requestData['to'], $touchInterval, $order['entity_id'], $order['created_at']); 
                    
                    if($requestData['adtrack_source'] && $requestData['adtrack_aggregate'] != 'aggregate_source'){
                        $sql = $sql.sprintf(" AND l.source_id = %d ", $requestData['adtrack_source']);
                    }
                    if($requestData['adtrack_campaign'] && $requestData['adtrack_aggregate'] != 'aggregate_campaign'){
                        $sql = $sql.sprintf(" AND l.campaign_id = %d ", $requestData['adtrack_campaign']);
                    }
                    if($requestData['adtrack_group'] && $requestData['adtrack_aggregate'] != 'aggregate_group'){
                        $sql = $sql.sprintf(" AND l.adgroup_id = %d ", $requestData['adtrack_group']);
                    }
                    if($requestData['adtrack_adid'] && $requestData['adtrack_aggregate'] != 'aggregate_adid'){
                        $sql = $sql.sprintf(" AND l.adid_id = %d ", $requestData['adtrack_adid']);
                    }
                    if($requestData['adtrack_redirect_id'] && $requestData['adtrack_aggregate'] != 'aggregate_redirect'){
                        $sql = $sql.sprintf(" AND l.redirect_id = %d ", $requestData['adtrack_redirect_id']);
                    }
                    
                    if($touchCriteria == touch_criteria_last){
                        $sql = $sql. " ORDER BY l.logged_time DESC LIMIT 1";
                    } else {
                        $sql = $sql. " ORDER BY l.logged_time LIMIT 1";
                    }
                    
                    $filteredLogs = $read->fetchAll($sql);
                    
                    if(count($filteredLogs) > 0) {
                        $cntSales++;
                        $totalSales += $filteredLogs[0]['sales'];           
                        
                        if($requestData['adtrack_aggregate']){
                            if($requestData['adtrack_aggregate'] == 'aggregate_source'){
                                $finalArray['label'] = 'Source';
                                $data = Mage::getModel('adtrack/source')->load($filteredLogs[0]['source_id']); 
                                $finalArray['aggregate'][$data->getName()]['orders']++;
                                $finalArray['aggregate'][$data->getName()]['sales'] = $finalArray['aggregate'][$data->getName()]['sales'] + $filteredLogs[0]['sales'];
                                $sql = $logCount.sprintf(" AND l.source_id = %d ", $filteredLogs[0]['source_id']);
                                $aggregateLogCount = $read->fetchAll($sql);
                                $finalArray['aggregate'][$data->getName()]['clicks'] = $aggregateLogCount[0]['log_count'];
                            }
                            if($requestData['adtrack_aggregate'] == 'aggregate_campaign'){
                                $finalArray['label'] = 'Campaign';
                                $data = Mage::getModel('adtrack/campaign')->load($filteredLogs[0]['campaign_id']); 
                                $finalArray['aggregate'][$data->getName()]['orders']++;
                                $finalArray['aggregate'][$data->getName()]['sales'] = $finalArray['aggregate'][$data->getName()]['sales'] + $filteredLogs[0]['sales'];
                                $sql = $logCount.sprintf(" AND l.campaign_id = %d ", $filteredLogs[0]['campaign_id']);
                                $aggregateLogCount = $read->fetchAll($sql);
                                $finalArray['aggregate'][$data->getName()]['clicks'] = $aggregateLogCount[0]['log_count'];
                            }
                            if($requestData['adtrack_aggregate'] == 'aggregate_group'){
                                $finalArray['label'] = 'AdGroup';
                                $data = Mage::getModel('adtrack/adgroup')->load($filteredLogs[0]['adgroup_id']); 
                                $finalArray['aggregate'][$data->getName()]['orders']++;
                                $finalArray['aggregate'][$data->getName()]['sales'] = $finalArray['aggregate'][$data->getName()]['sales'] + $filteredLogs[0]['sales'];
                                $sql = $logCount.sprintf(" AND l.adgroup_id = %d ", $filteredLogs[0]['adgroup_id']);
                                $aggregateLogCount = $read->fetchAll($sql);
                                $finalArray['aggregate'][$data->getName()]['clicks'] = $aggregateLogCount[0]['log_count'];
                            }
                            if($requestData['adtrack_aggregate'] == 'aggregate_adid'){
                                $finalArray['label'] = 'AdId';
                                $data = Mage::getModel('adtrack/adid')->load($filteredLogs[0]['adid_id']); 
                                $finalArray['aggregate'][$data->getName()]['orders']++;
                                $finalArray['aggregate'][$data->getName()]['sales'] = $finalArray['aggregate'][$data->getName()]['sales'] + $filteredLogs[0]['sales'];                                
                                $sql = $logCount.sprintf(" AND l.adid_id = %d ", $filteredLogs[0]['adid_id']);
                                $aggregateLogCount = $read->fetchAll($sql);
                                $finalArray['aggregate'][$data->getName()]['clicks'] = $aggregateLogCount[0]['log_count'];
                            }
                            if($requestData['adtrack_aggregate'] == 'aggregate_redirect'){
                                $finalArray['label'] = 'Redirect Id';
                                $finalArray['aggregate'][$filteredLogs[0]['redirect_id']]['orders']++;
                                $finalArray['aggregate'][$filteredLogs[0]['redirect_id']]['sales'] = $finalArray['aggregate'][$filteredLogs[0]['redirect_id']]['sales'] + $filteredLogs[0]['sales'];                                                                
                                $sql = $logCount.sprintf(" AND l.redirect_id = %d ", $filteredLogs[0]['redirect_id']);
                                $aggregateLogCount = $read->fetchAll($sql);
                                $finalArray['aggregate'][$filteredLogs[0]['redirect_id']]['clicks'] = $aggregateLogCount[0]['log_count'];
                            }
                        }
                    }       
                }
            }          
            
            $finalArray['clicks'] = $FinalLogCount;
            $finalArray['orders'] = $cntSales;
            $finalArray['sales'] = $totalSales;
            
            return $finalArray;
        }
    }  
}
