<?php
class Tdg_Adtrack_Block_Adminhtml_Extpage extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_extpage';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Ext Page');
    $this->_addButtonLabel = Mage::helper('adtrack')->__('Add Ext Page');
    parent::__construct();
  }
}