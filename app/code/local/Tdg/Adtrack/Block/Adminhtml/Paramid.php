<?php
class Tdg_Adtrack_Block_Adminhtml_Paramid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_paramid';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Param Id');
    $this->_addButtonLabel = Mage::helper('adtrack')->__('Add Param Id');
    parent::__construct();
  }
}