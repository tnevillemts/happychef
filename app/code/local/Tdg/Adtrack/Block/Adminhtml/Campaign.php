<?php
class Tdg_Adtrack_Block_Adminhtml_Campaign extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_campaign';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Campaign');
    $this->_addButtonLabel = Mage::helper('adtrack')->__('Add Campaign');
    parent::__construct();
  }
}