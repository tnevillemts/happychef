<?php
class Tdg_Adtrack_Block_Adminhtml_Redirect extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_redirect';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Redirect');
    $this->_addButtonLabel = Mage::helper('adtrack')->__('Add Redirect');
    parent::__construct();
  }
}