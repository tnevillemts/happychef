<?php

class Tdg_Adtrack_Block_Adminhtml_Directreports_Filter_Form extends Mage_Adminhtml_Block_Widget_Form
{
	/**
     	* Add fields to base fieldset which are general to sales reports
     	*
     	* @return Mage_Sales_Block_Adminhtml_Report_Filter_Form
     	*/
    	protected function _prepareForm()
    	{                
      		$form = new Varien_Data_Form(array(
                                             	'id' => 'filter_form',
                                     		'action' => $this->getUrl('*/*/index'),
                                     		'method' => 'post'
                                            ));
      		$form->setUseContainer(true);
     		$this->setForm($form);

      		$fieldset = $form->addFieldset('adtrack_directreports_form', array('legend'=>Mage::helper('adtrack')->__('Filters')));
     
		$sources = Mage::getModel('adtrack/source')->getCollection();
		$sourcesOptionArray = array(array('value' => '','label' => 'ALL'));
		foreach($sources as $source){
			$sourcesOptionArray[] = array('value' => $source->getId(),'label' => $source->getId() . '-' . $source->getName());
	  	}

      		$fieldset->addField('adtrack_source', 'select', array(
          		'label'     => Mage::helper('adtrack')->__('Source'),
          		'name'      => 'adtrack_source',
	   		'values'    => $sourcesOptionArray
      		));

      		$campaigns = Mage::getModel('adtrack/campaign')->getCollection();
      		$campaignOptionArray = array(array('value' => '','label' => 'ALL'));
		foreach($campaigns as $campaign){
			$campaignOptionArray[] = array('value' => $campaign->getId(),'label' => $campaign->getId() . '-' . $campaign->getName());
		}
      		$fieldset->addField('adtrack_campaign', 'select', array(
          		'label'     => Mage::helper('adtrack')->__('Campaign'),
          		'name'      => 'adtrack_campaign',
	   		'values'    => $campaignOptionArray
     		));

       		$groups = Mage::getModel('adtrack/adgroup')->getCollection();
       		$groupOptionArray = array(array('value' => '','label' => 'ALL'));
		foreach($groups as $group){
			$groupOptionArray[] = array('value' => $group->getId(),'label' => $group->getId() . '-' . $group->getName());
		}
		$fieldset->addField('adtrack_group', 'select', array(
          		'label'     => Mage::helper('adtrack')->__('Group'),
          		'name'      => 'adtrack_group',
	   		'values'    => $groupOptionArray
      		));		

		$adids = Mage::getModel('adtrack/adid')->getCollection();
       		$adidOptionArray = array(array('value' => '','label' => 'ALL'));
		foreach($adids as $adid){
			$adidOptionArray[] = array('value' => $adid->getId(),'label' => $adid->getId() . '-' . $adid->getName());
		}

		$fieldset->addField('adtrack_adid', 'select', array(
          		'label'     => Mage::helper('adtrack')->__('Adid'),
          		'name'      => 'adtrack_adid',
	   		'values'    => $adidOptionArray
      		));
                
                $redirects = Mage::getModel('adtrack/redirect')->getCollection();
       		$redirectsOptionArray = array(array('value' => '','label' => 'ALL'));
		foreach($redirects as $redirect){
			$redirectsOptionArray[] = array('value' => $redirect->getId(),'label' => $redirect->getId());
		}
		
		$fieldset->addField('adtrack_redirect_id', 'select', array(
          		'label'     => Mage::helper('adtrack')->__('Redirect Id'),
          		'name'      => 'adtrack_redirect_id',
	   		'values'    => $redirectsOptionArray
      		));		
		
		$dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		$fieldset->addField('from', 'date', array(
            		'name'      => 'from',
            		'format'    => $dateFormatIso,
            		'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            		'label'     => Mage::helper('reports')->__('From'),
            		'title'     => Mage::helper('reports')->__('From'),
            		'class'     => 'validate-date',
                        'required'  => true,
                        'after_element_html' => 'Format is: Month/Date/Year'
        	));

        	$fieldset->addField('to', 'date', array(
            		'name'      => 'to',
            		'format'    => $dateFormatIso,
            		'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            		'label'     => Mage::helper('reports')->__('To'),
            		'title'     => Mage::helper('reports')->__('To'),
                        'class'     => 'validate-date',                        
            		'required'  => true
        	));
		
		$touch_criteria_options = array(                                
                'touch_criteria_first' => $this->__('First'),
                'touch_criteria_last' => $this->__('Last')
        );

		$fieldset->addField('touch_criteria', 'select', array(
			'label'     => Mage::helper('adtrack')->__('Touch Criteria'),
			'name'      => 'touch_criteria',
                        'value'  => 'touch_criteria_last',
			'values'    => $touch_criteria_options
		));
		
		$fieldset->addField('touch_interval', 'text', array(
                        'label'     => Mage::helper('adtrack')->__('Touch Interval'),
                          'class'     => 'validate-digits validate-digits-range digits-range-1-120',
                        'name'      => 'touch_interval',
                        'value'     => '60', 
                        'required'  => true,
                        'after_element_html' => 'Enter time interval between 1 to 120 days'
        ));
		
		$aggregate_options = array(                
                '' => $this->__('NONE'),
                'aggregate_source' => $this->__('Source'),
                'aggregate_campaign' => $this->__('Campaign'),
                'aggregate_group' => $this->__('Group'),
                'aggregate_adid' => $this->__('Adid'),
                'aggregate_redirect' => $this->__('Redirect')
            );

            $fieldset->addField('adtrack_aggregate', 'select', array(
                'label'     => Mage::helper('adtrack')->__('Aggregate By'),
                'name'      => 'adtrack_aggregate',
                'values'    => $aggregate_options
            ));

      		return parent::_prepareForm();
    	}
}

