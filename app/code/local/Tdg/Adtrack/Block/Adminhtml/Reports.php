<?php
class Tdg_Adtrack_Block_Adminhtml_Reports extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    	$this->_controller = 'adminhtml_reports';
    	$this->_blockGroup = 'adtrack';
    	$this->_headerText = Mage::helper('adtrack')->__("Adtrack &ndash; Assisted Report");
    	parent::__construct();
    	$this->setTemplate('adtrack/reports/grid/container.phtml');
    	$this->_removeButton('add');
        $this->addButton('help', array(
            'label'     => Mage::helper('reports')->__('Help'),
            'onclick'   => 'showHelp()'
        ));
        $this->addButton('reset', array(
            'label'     => Mage::helper('reports')->__('Reset'),
            'onclick'   => 'resetForm()'
        ));
    	$this->addButton('filter_form_submit', array(
            'label'     => Mage::helper('reports')->__('Show Report'),
            'onclick'   => 'filterFormSubmit()'
        ));
  }

  public function getFilterUrl()
  {
    	$this->getRequest()->setParam('filter', null);
	return $this->getUrl('*/*/index', array('_current' => true));
  }
}