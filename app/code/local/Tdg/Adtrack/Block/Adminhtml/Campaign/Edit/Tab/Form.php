<?php

class Tdg_Adtrack_Block_Adminhtml_Campaign_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('campaign_form', array('legend'=>Mage::helper('adtrack')->__('Campaign information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

	$fieldset->addField('old_campaign_id', 'text', array(
          'label'     => Mage::helper('adtrack')->__('Old Campaign Id'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'old_campaign_id',
      ));


      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('adtrack')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('adtrack')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('adtrack')->__('Disabled'),
              ),
          ),
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getCampaignData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCampaignData());
          Mage::getSingleton('adminhtml/session')->setCampaignData(null);
      } elseif ( Mage::registry('campaign_data') ) {
          $form->setValues(Mage::registry('campaign_data')->getData());
      }
      return parent::_prepareForm();
  }
}