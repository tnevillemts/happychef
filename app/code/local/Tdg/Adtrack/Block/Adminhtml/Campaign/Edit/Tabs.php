<?php

class Tdg_Adtrack_Block_Adminhtml_Campaign_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('campaign_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('adtrack')->__('Campaign Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('adtrack')->__('Campaign Information'),
          'title'     => Mage::helper('adtrack')->__('Campaign Information'),
          'content'   => $this->getLayout()->createBlock('adtrack/adminhtml_campaign_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}