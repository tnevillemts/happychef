<?php
class Tdg_Adtrack_Block_Adminhtml_Adgroup extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_adgroup';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Ad Group');
    $this->_addButtonLabel = Mage::helper('adtrack')->__('Add Ad Group');
    parent::__construct();
  }
}