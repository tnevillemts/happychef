<?php
class Tdg_Adtrack_Block_Adminhtml_Adid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_adid';
    $this->_blockGroup = 'adtrack';
    $this->_headerText = Mage::helper('adtrack')->__('Manage Ad ID');
    $this->_addButtonLabel = Mage::helper('adtrack')->__('Add Ad ID');
    parent::__construct();
  }
}