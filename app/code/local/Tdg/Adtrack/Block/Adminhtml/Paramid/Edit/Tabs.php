<?php

class Tdg_Adtrack_Block_Adminhtml_Paramid_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('paramid_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('adtrack')->__('Param Id Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('adtrack')->__('Param Id Information'),
          'title'     => Mage::helper('adtrack')->__('Param Id Information'),
          'content'   => $this->getLayout()->createBlock('adtrack/adminhtml_paramid_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}