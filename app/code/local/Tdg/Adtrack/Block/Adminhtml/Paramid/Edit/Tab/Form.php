<?php

class Tdg_Adtrack_Block_Adminhtml_Paramid_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('paramid_form', array('legend'=>Mage::helper('adtrack')->__('Param Id Information')));
     
      $fieldset->addField('url_param', 'text', array(
          'label'     => Mage::helper('adtrack')->__('URL Param'),
          'name'      => 'url_param',
      ));

      if ( Mage::getSingleton('adminhtml/session')->getParamidData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getParamidData());
          Mage::getSingleton('adminhtml/session')->setParamidData(null);
      } elseif ( Mage::registry('paramid_data') ) {
          $form->setValues(Mage::registry('paramid_data')->getData());
      }
      return parent::_prepareForm();
  }
}