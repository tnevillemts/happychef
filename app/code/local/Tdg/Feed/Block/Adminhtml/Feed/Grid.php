<?php

class Tdg_Feed_Block_Adminhtml_Feed_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('feedGrid');
      $this->setDefaultSort('feed_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('feed/feed')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('feed_id', array(
          'header'    => Mage::helper('feed')->__('ID'),
          'align'     =>'center',
          'width'     => '50px',
          'index'     => 'feed_id',
      ));

      $this->addColumn('feed_name', array(
          'header'    => Mage::helper('feed')->__('Name'),
          'align'     =>'left',
          'index'     => 'feed_name',
      ));
      
      $this->addColumn('feed_url', array(
          'header'    => Mage::helper('feed')->__('Url'),
          'align'     =>'left',
          'index'     => 'feed_url',
      ));
      
      $this->addColumn('feed_status', array(
          'header'    => Mage::helper('feed')->__('Status'),
          'align'     =>'left',
          'index'     => 'feed_status',
      ));
      
      $this->addColumn('feed_mutex', array(
          'header'    => Mage::helper('feed')->__('Mutex'),
          'align'     =>'left',
          'index'     => 'feed_mutex',
      ));
      
      $this->addColumn('feed_errors', array(
          'header'    => Mage::helper('feed')->__('Errors'),
          'align'     =>'left',
          'index'     => 'feed_errors',
      ));
      
      $this->addColumn('feed_info', array(
          'header'    => Mage::helper('feed')->__('Info'),
          'align'     =>'left',
          'index'     => 'feed_info',
      ));
      
      $this->addColumn('run_started', array(
            'header' => Mage::helper('adminhtml')->__('Started At'),
            'align' => 'left',
            'type' => 'date',
	    'format' => 'Y-MMM-d H:mm:s a',
            'index' => 'run_started',
        ));
      
      $this->addColumn('run_finished', array(
            'header' => Mage::helper('adminhtml')->__('Finished At'),
            'align' => 'left',
            'type' => 'date',
	    'format' => 'Y-MMM-d H:mm:s a',
            'index' => 'run_finished',
        ));

			
        $this->addExportType('*/*/exportCsv', Mage::helper('feed')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('feed')->__('XML'));
	  
      return parent::_prepareColumns();
  }   

  public function getRowUrl($row)
  {
      return "#";
  }

}
