<?php
class Tdg_Feed_Block_Adminhtml_Feed extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_feed';
    $this->_blockGroup = 'feed';
    $this->_headerText = Mage::helper('feed')->__('Feed Report');    
    parent::__construct();
    $this->_removeButton('add');
  }
}