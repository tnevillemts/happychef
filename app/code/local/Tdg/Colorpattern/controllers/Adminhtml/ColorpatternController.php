<?php
class Tdg_Colorpattern_Adminhtml_ColorpatternController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('colorpattern/items');
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() 
	{
		$this->_initAction()
			->renderLayout();
	}
	
	public function saveAction()
	{
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
	     $postData = $this->getRequest()->getParams();
	     $colorpattern_id = $postData['colorpattern_id'];
            $sql = sprintf(
                           "DELETE FROM mag_colorpattern_mapping WHERE colorpattern_id = %d",
				 $colorpattern_id
                           );
	     $write->query($sql);
	     if(array_key_exists('color',$postData))
    	     {
			$countOptions = count($postData['color']);
			for($i=0;$i<$countOptions;$i++)
			{
				if($postData['color'][$i]!=''){
				$sql = sprintf(
						  "INSERT INTO mag_colorpattern_mapping(colorpattern_id,color)
						   VALUES(%d,'%s')",
						   $colorpattern_id,
						   $postData['color'][$i]
						 );
				$write->query($sql);
			       }
			}
	     }
	     $message = $this->__('Saved Successfully');
	     Mage::getSingleton('adminhtml/session')->addSuccess($message); 
	     $this->_redirect("*/*/edit/id/$colorpattern_id");
	}
}
