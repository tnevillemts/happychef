<?php
$installer = $this;

$installer->startSetup();

$installer->run("

      /*sales_flat_order*/
	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order` ADD `old_orderid` INT( 11 ) NULL DEFAULT NULL AFTER `gift_message_id` ;
	  
	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order` ADD `confirmation` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `base_giftcert_amount_credited` ;
	  
	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order` ADD `log_id` INT( 11 ) NULL DEFAULT NULL AFTER `from_site` ;
	  
	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order` ADD `visitor_id` BIGINT( 20 ) NULL DEFAULT NULL AFTER `log_id` ;
	  
	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order` ADD `order_status_id` INT( 11 ) NULL DEFAULT NULL AFTER `visitor_id` ;
	  
	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order` ADD `catalog` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `order_status_id` ;
	  
	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order` ADD `catalog_keycode` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `catalog` ;
	  
");

$installer->endSetup();