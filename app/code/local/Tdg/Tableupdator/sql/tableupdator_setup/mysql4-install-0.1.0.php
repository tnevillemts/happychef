<?php
//$installer = $this;
//
//$installer->startSetup();
//
//$installer->run("
//
//      /*catalogsearch_query*/
//      ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalogsearch_query` ADD `RedirectID` INT( 25 ) NULL DEFAULT NULL AFTER `updated_at` ;
//	  
//	  /*catalog_eav_attribute*/
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalog_eav_attribute` ADD `display_in_category` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `is_used_for_promo_rules`;
//	  
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalog_eav_attribute` ADD `position_compare` INT( 11 ) NULL DEFAULT NULL AFTER `display_in_category`;
//	  
//	  
//  	  /*catalog_product_option*/
//            ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalog_product_option` ADD `personalization_id` INT( 11 ) NULL DEFAULT NULL AFTER `sort_order`;
//	  
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalog_product_option` ADD `sl_no` INT( 11 ) NULL DEFAULT NULL AFTER `personalization_id` ;
//	  
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalog_product_option` ADD `children` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `sl_no` ;
//	  
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalog_product_option` ADD `image` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `children` ;
//	  
//	  /*catalog_product_option_type_value*/
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "catalog_product_option_type_value` ADD `image` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `sort_order` ;
//	  
//	  /*customer_group*/
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "customer_group` ADD `allowed_shipping_methods` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `tax_class_id`;
//	  
//");

$installer->endSetup();