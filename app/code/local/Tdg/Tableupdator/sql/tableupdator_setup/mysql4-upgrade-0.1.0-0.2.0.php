<?php
//$installer = $this;
//
//$installer->startSetup();
//
//$installer->run("
//        
//      /*sales_flat_order_address*/
//          ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order_address` ADD `jobtitle` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `fax` ;
//	  
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order_address` ADD `bill_to_customer_id` INT( 11 ) NULL DEFAULT NULL AFTER `vat_request_success` ;
//	  
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order_address` ADD `ship_to_customer_id` INT( 11 ) NULL DEFAULT NULL AFTER `bill_to_customer_id`  ;
//	  
//      /*sales_flat_order_item*/
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_order_item` ADD `itemstatus` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `base_weee_tax_row_disposition` ;
//	  
//      /*sales_flat_quote_address*/
//	 ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_quote_address` ADD `jobtitle` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `fax` ;
//	  
//      /*sales_flat_shipment*/	  
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_shipment` ADD `old_shipment_id` INT( 11 ) NULL DEFAULT NULL AFTER `shipping_label`  ;
//	  
//      /*sales_flat_shipment_track*/
//	  ALTER TABLE `" . Mage::getConfig()->getTablePrefix() . "sales_flat_shipment_track` ADD `old_track_id` INT( 11 ) NULL DEFAULT NULL AFTER `updated_at` ;
//	  
//");

$installer->endSetup();