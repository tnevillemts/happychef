<?php


$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE  {$this->getTable('followupemail/rule')} ADD `order_from` DATETIME NULL AFTER `active_to`
");
$installer->run("
    ALTER TABLE  {$this->getTable('followupemail/rule')} ADD `order_to` DATETIME NULL AFTER `order_from`
");
$installer->endSetup();

