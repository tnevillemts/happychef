<?php


$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE  {$this->getTable('followupemail/rule')} ADD `grace_period` INT NULL AFTER `order_to`
");

$installer->endSetup();

