<?php

/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 * 
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Previousnext
 * @copyright  Copyright (c) 2010-2011 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */class AW_Previousnext_Model_Previousnext extends Mage_Core_Model_Abstract {
    const LOOP = 'previousnext/general/loopproducts';
    private $_prev = 0;
    private $_next = 0;

    protected function _construct() {
        $loop = Mage::getStoreConfig(self::LOOP);
        $array = $this->_getDataArray();
        $cnt = count($array);

        if ($cnt - 1) {
            $pos = array_search(Mage::app()->getRequest()->getParam('id'), $array);
            $this->_prev = ($pos == 0) ? ($loop ? $array[$cnt - 1] : 0) : $array[$pos - 1];
            $this->_next = ($pos == ($cnt - 1)) ? ($loop ? $array[0] : 0) : $array[$pos + 1];
        }
   }

    public function getPrevID() {
        return (int) $this->_prev;
    }

    public function getNextID() {
        return (int) $this->_next;
    }

    private function _getDataArray() {
        $results=array();
        $select = Mage::getSingleton('core/session')->getData('aw_prevnext_sql');
        if($select){
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $results = $readConnection->fetchCol((string) $select);
        }
        return $results;
    }

}

