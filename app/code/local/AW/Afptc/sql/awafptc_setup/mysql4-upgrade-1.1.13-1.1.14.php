<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.14
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


try {
    $this->startSetup();
    $this->run("
        CREATE TABLE IF NOT EXISTS {$this->getTable('awafptc/history')} (
          `entity_id` int(10) unsigned not null auto_increment,
          `rule_id` int(10) unsigned not null,
          `product_id` int(10) unsigned not null,          
          `order_id` int(10) unsigned not null,
          PRIMARY KEY (`entity_id`),                       
          KEY `AW_AFPTC_HISTORY_RID` (`rule_id`),
          KEY `AW_AFPTC_HISTORY_OID` (`order_id`)         
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        
    ");
    $this->endSetup();
} catch (Exception $e) {
    Mage::logException($e);
}