<?php

class HappyChef_Shoppinglist_Model_Rewrite_Mysql4_Shoppinglist_Items extends Magebuzz_Shoppinglist_Model_Mysql4_Items {

/*
* Insert item to shopping list
*/
    public function insertItemShoppingList($groupId, $productId, $qty, $customOptions)
    {
        $model = Mage::getModel('shoppinglist/items');
        /* Update time updated to the group has item*/
        $group = Mage::getModel('shoppinglist/group')->load($groupId);
        $storeId = Mage::app()->getStore()->getId();
//    $product = Mage::getModel('catalog/product')->load($productId);
        $data = array(
            'list_id'    => $groupId,
            'product_id' => $productId,
            'store_id'   => $storeId,
            'qty'        => $qty,
            'updated_at' => now()
        );
        $model->setData($data);

        if ($customOptions) {
            $model->setBuyRequest($customOptions);
        }

        $group->setUpdatedAt(now());
        try {
            $group->save();
            if ($id = $this->isExisted($model)) {
                $model->setId($id['item_id']);
                $model->setQty($id['qty'] + $model->getQty());
                $model->save();
            } else {
                $model->setCreatedAt(now());
                $model->save();
            }
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }
    }

}