<?php

class HappyChef_Shoppinglist_Model_Rewrite_Shoppinglist_Items extends Magebuzz_Shoppinglist_Model_Items {

    protected $_optionsHtml = null;
    protected $_additionalPrice = null;

    public function _construct()
    {
        parent::_construct();
        $this->_init('shoppinglist/items');
    }

    public function getItemsByGroup($groupId)
    {
        $items = $this->getCollection();
        $items->addFieldToFilter('list_id', $groupId);
        return $items;
    }

    protected function _parseOptions($item)
    {
        $optionsHtml = '<dl class="item-options">';
        $optionsPrice = $additionalPrice = $embroideryPrice = 0;
        $buyRequest = $item->getBuyRequest();
        if ($buyRequest)
            $buyRequest = unserialize($buyRequest);
        $typeInstance = $item->getProduct()->getTypeInstance(TRUE);
        switch ($item->getProduct()->getTypeId()) {
            case 'configurable':
                if ($buyRequest && isset($buyRequest['super_attribute'])) {
                    $attributes = $typeInstance
                        ->getConfigurableAttributes($item->getProduct());

                    foreach ($attributes as $attribute) {
                        $attributeId = $attribute->getProductAttribute()->getId();
                        if ($attributeId && isset($buyRequest['super_attribute'][$attributeId])) {
                            $aPrices = $attribute->getPrices();
                            if ($aPrices) {
                                foreach ($aPrices as $aOptionData) {
                                    if ($buyRequest['super_attribute'][$attributeId] == $aOptionData['value_index']) {
                                        $optionsHtml .= '<dt>' . $attribute->getProductAttribute()->getFrontendLabel() . '</dt><dd>' . $aOptionData['store_label'] . '</dd>';
                                        $optionsPrice += $aOptionData['pricing_value'];
                                    }
                                }
                            }
                        }
                    }

                    if (isset($buyRequest['unitEmbroideryPrice'])) {
                        $embroideryPrice = $buyRequest['unitEmbroideryPrice'];
                    }

                    $additionalPrice += $optionsPrice + $embroideryPrice;
                }
            case 'simple':

            case 'virtual':
                if (!empty($buyRequest['options'])) {
                    if (!$item->getProduct()->getOptions()) {
                        $collection = $item->getProduct()->getProductOptionsCollection();
                        foreach ($collection as $o) {
                            $o->setProduct($item->getProduct());
                            $item->getProduct()->addOption($o);
                        }
                    }

                    foreach ($buyRequest['options'] as $iOptionId => $mValue) {
                        $option = null;
                        if ($mValue) {
                            $option = $item->getProduct()->getOptionById($iOptionId);
                        }
                        if ($option) {
                            $group = $option->groupFactory($option->getType())
                                ->setOption($option);

                            if (in_array($option->getType(), array('date', 'date_time', 'time'))) {
                                $group->setUserValue($mValue);
                                $group->validateUserValue($mValue);
                                $group->setIsValid(TRUE);
                                $group->setRequest(new Varien_Object($buyRequest));
                                $mValue = $group->prepareForCart();
                            }
                            if ($option->getType() == 'file') {
                                $aOrigValue = $mValue;
                            }
                            // add to price
                            foreach ($option->getValues() as $_value) {
                                if ($mValue) {
                                    if (is_array($mValue) AND in_array($_value->getId(), $mValue)) {
                                        $additionalPrice += $_value->getPrice(TRUE);
                                    } elseif (!is_array($mValue) AND $mValue == $_value->getOptionTypeId()) {
                                        $additionalPrice += $_value->getPrice(TRUE);
                                    }
                                }
                            }

                            if (is_array($mValue)) {
                                $mValue = implode(',', $mValue);
                            }
                            $aOptionData = array(
                                'label'       => $option->getTitle(),
                                'print_value' => $group->getPrintableOptionValue($mValue),
                            );

                            if (in_array($option->getType(), array('date', 'date_time', 'time'))) {
                                if ($mValue) {
                                    if ($option->getPrice()) {
                                        $additionalPrice += $option->getPrice(TRUE);
                                    }
                                } else {
                                    $aOptionData['print_value'] = '';
                                }
                            } else {
                                if ($option->getPrice()) {
                                    $additionalPrice += $option->getPrice(TRUE);
                                }
                            }

                            if ($option->getType() == 'file') {
                                if ($aOrigValue['width'] > 0 && $aOrigValue['height'] > 0) {
                                    $sizes = $aOrigValue['width'] . ' x ' . $aOrigValue['height'] . ' ' . Mage::helper('catalog')->__('px.');
                                } else {
                                    $sizes = '';
                                }

                                $aOptionData['print_value'] = sprintf('%s %s',
                                    Mage::helper('core')->htmlEscape($aOrigValue['title']),
                                    $sizes
                                );
                            }

                            if ($aOptionData['print_value']) {
                                $optionsHtml .= '<dt>' . $aOptionData['label'] . '</dt><dd>' . $aOptionData['print_value'] . '</dd>';
                            }
                        }
                    }
                }
                break;
            case 'aw_giftcard':
                if ($buyRequest && isset($buyRequest['aw_gc_amount'])) {
                    $gcOptions = $this->_getGiftcardOptions($buyRequest);
                    foreach($gcOptions as $gcOption){
                        $optionsHtml .= '<dt>' . $gcOption['label'] . '</dt><dd>' . $gcOption['value'] . '</dd>';
                    }
                    $additionalPrice = $buyRequest['aw_gc_amount'];
                }
                break;
        }

        $optionsHtml .= '</dl>';
        $this->_additionalPrice = $additionalPrice;
        $this->_optionsHtml = $optionsHtml;
    }

    protected function _getGiftcardOptions($buyRequest)
    {
        $senderName     		= $buyRequest['aw_gc_sender_name'];
        $recipientFirstName     = $buyRequest['aw_gc_recipient_first_name'];
        $recipientLastName		= $buyRequest['aw_gc_recipient_last_name'];
        $senderEmail    		= $buyRequest['aw_gc_sender_email'];
        $recipientEmail 		= $buyRequest['aw_gc_recipient_email'];
        $message        		= $buyRequest['aw_gc_message'];
        $address1				= $buyRequest['aw_gc_recipient_address'];
        $address2				= $buyRequest['aw_gc_recipient_address2'];
        $state					= $buyRequest['aw_gc_recipient_state'];
        $city					= $buyRequest['aw_gc_recipient_city'];
        $zipcode				= $buyRequest['aw_gc_recipient_zipcode'];
        $gcDesign               = $buyRequest['giftcard_design'];
        $result = array();
        if ($senderName) {
            $senderOptionText = $senderName;
            if ($senderEmail) {
                $senderOptionText = "{$senderName} &lt;{$senderEmail}&gt;";
            }
            $result[] = array(
                'label' => Mage::helper('catalog')->__('Gift Card Sender'),
                'value' => $senderOptionText
            );
        }

        if ($recipientFirstName) {
            $recipientOptionText = $recipientFirstName;
            if ($recipientLastName) {
                $recipientOptionText = "{$recipientFirstName} "."{$recipientLastName}";
            }
            $result[] = array(
                'label' => Mage::helper('catalog')->__("Recipient"),
                'value' => $recipientOptionText
            );
        }
        if ($recipientEmail) {
            $recipientOptionText = $recipientEmail;
            $result[] = array(
                'label' => Mage::helper('catalog')->__("Recipient's Email"),
                'value' => $recipientOptionText
            );
        }
        if ($address1) {
            $recipientOptionText = $address1;
            if ($address2) {
                $recipientOptionText .= " {$address2}";
            }
            if ($city) {
                $recipientOptionText .= ", {$city}";
            }
            if ($state) {
                $recipientOptionText .= ", {$state}";
            }
            if ($zipcode) {
                $recipientOptionText .= " {$zipcode}";
            }
            $result[] = array(
                'label' => Mage::helper('catalog')->__("Address"),
                'value' => $recipientOptionText
            );
        }
        if (trim($message)) {
            $result[] = array(
                'label' => Mage::helper('catalog')->__('Gift Card Message'),
                'value' => $message
            );
        }

        if ($gcDesign) {
            $result[] = array(
                'label' => Mage::helper('catalog')->__('Gift Card Design'),
                'value' => basename($gcDesign)
            );
        }
        return $result;
    }

    public function getPrice()
    {
        if (is_null($this->_additionalPrice)) {
            try {
                $this->_parseOptions($this);
            } catch (Exception $e) {
            }
        }
        $price = $this->getProduct()->getFinalPrice();
        if ($this->_additionalPrice)
            $price += $this->_additionalPrice;

        return sprintf("%.2f", $price);
    }

    public function getOptionsHtml()
    {
        if (is_null($this->_optionsHtml)) {
            try {
                $this->_parseOptions($this);
            } catch (Exception $e) {
            }
        }
        return $this->_optionsHtml;
    }

}