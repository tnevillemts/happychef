<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Happychef_Shoppinglist_Model_Rewrite_Observer extends Magebuzz_Shoppinglist_Model_Observer
{
    public function clearStatusMsgs()
    {
        $action = Mage::app()->getFrontController()->getAction()->getFullActionName();
        if ($action != 'checkout_cart_index' && $action != 'awafptc_cart_getPopupHtml' && $action != 'customer_account_login') {
            Mage::getSingleton('customer/session')->unsCartSort();
            Mage::getSingleton('customer/session')->unsSaveCartSort();
        }
    }
}