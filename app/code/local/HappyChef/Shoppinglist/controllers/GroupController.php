<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/
require_once 'Magebuzz/Shoppinglist/controllers/GroupController.php';

class Happychef_Shoppinglist_GroupController extends Magebuzz_Shoppinglist_GroupController
{
    public function publicViewAction()
    {
        $this->loadLayout();
        $group_id = $this->getRequest()->getParam('id');
        if ($group_id) {
            $group = Mage::getModel('shoppinglist/group')->load($group_id);
            $owner = Mage::getModel('customer/customer')->load($group->getCustomerId());
            $this->getLayout()->getBlock('head')->setTitle($this->__('%s\'s shopping list', $owner->getName()));
        }
        $this->renderLayout();
    }

    public function viewAction()
    {
        if (Mage::helper('customer')->isLoggedIn()) {
            $group_id = $this->getRequest()->getParam('id');
            if ($group_id) {
                $group = Mage::getModel('shoppinglist/group')->load($group_id);
                if ($group->getCustomerId() != Mage::helper('customer')->getCustomer()->getId()) {
//                    $this->_redirect('*/index/index');
                    $this->_redirect(Mage::getBaseUrl() . 'checkout/cart');
                    return;
                }
                Mage::register('current_group', $group);
                $this->loadLayout();
                $this->_initLayoutMessages('customer/session');
                $this->getLayout()->getBlock('head')->setTitle(Mage::helper('shoppinglist')->getTitleTopLink() . ' | Group - ' . $group->getListName());
                $this->renderLayout();
                return;
            }
        } else $this->_redirectUrl(Mage::getBaseUrl() . 'checkout/cart');
    }

    public function saveAction()
    {
        $post = $this->getRequest()->getParams();
        $model = Mage::getModel('shoppinglist/group');
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $now = Mage::getModel('core/date')->gmtTimestamp(now());
        try {
            if (isset($post['group_id']) && ($post['group_id'] != '')) {
                /* Update existed group */
                $group = $model->load($post['group_id']);
                if (isset($post['group-name']) && ($post['group-name'])) $group->setListName($post['group-name']);
                if (isset($post['email-reminder']) && ($post['email-reminder'])) $model->setSendReminderAfter($post['email-reminder']);
                $group->setUpdatedAt($now);
                $group->save();
            } else {
                /* Create new group */
                $model->setCustomerId($customer->getId());
                if (isset($post['group-name']) && ($post['group-name'])) $model->setListName($post['group-name']);
                if (isset($post['email-reminder']) && ($post['email-reminder'])) $model->setSendReminderAfter($post['email-reminder']);
                $model->setStatus(1);
                $model->setCreatedAt($now);
                $model->setUpdatedAt($now);
                $model->save();
            }
            $this->_redirect('*/');
        } catch (Exception $e) {
            $this->_redirect('*/');
            return;
        }
    }

    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function deleteAction($groupId = null)
    {
        if (!is_null($groupId)) {
            $id = $groupId;
        } else {
            $id = $this->getRequest()->getParam('id');
        }

        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('shoppinglist/group');

                $model->setId($id)
                    ->delete();
                //$this->_redirect('*/');
            } catch (Exception $e) {
                //$this->_redirect('*/');
                return;
            }
        }
        //$this->_redirect('*/');
    }

    public function updateAction()
    {
        $post = $this->getRequest()->getParams();
        if ($post) {
            try {
                $groupId = $post['group_id'];
                $group = Mage::getModel('shoppinglist/group')->load($groupId);
                $group->setListName($post['list_name'])
                    ->save();
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError(Mage::helper('shoppinglist')->__('Unable to submit your request. Please try again later.'));
                //$this->_redirect('*/*/view/', array('id' => $groupId));
                return;
            }
            try {
                foreach ($post['item'] as $itemId) {
                    $itemProduct = Mage::getModel('shoppinglist/items')->load($itemId['itemId']);
                    if ($itemId['qty'] <= 0) {
                        $itemProduct->delete();
                    } else {
                        $itemProduct->setQty($itemId['qty']);
                        if ($itemId['select-group'] > 0) {
                            $itemProduct->setListId($itemId['select-group']);
                            $id = Mage::getResourceModel('shoppinglist/items')->isExisted($itemProduct);
                            $itemNewGroup = Mage::getSingleton('shoppinglist/items')->load($id['item_id']);
                            $itemProduct->setQty($id['qty'] + $itemProduct->getQty());
                            $itemNewGroup->setData($itemProduct->getData());
                            $itemNewGroup->setItemId($id['item_id']);
                            $itemProduct->delete();
                            $itemNewGroup->save();
                        }
                        $itemProduct->save();
                    }

                }
                //$this->_redirect('*/*/view/', array('id' => $groupId));
            } catch (Exception $e) {
                //$this->_redirect('*/*/view/', array('id' => $groupId));
                return;
            }
        } else {
            //$this->_redirect('*/index/index');
        }
    }

    public function removeItemAction($groupId = null, $rmvItemIds = null, $isRemove = null)
    {
        $params = $this->getRequest()->getParams();
        $sort = [];

        if (is_null($rmvItemIds)) {
            $rmvItemIds = explode(',', $params['id']);
        } else {
            $rmvItemIds = explode(',', $rmvItemIds);
        }

        if (is_null($groupId)) {
            $groupId = $params['group_id'];
        }

        if (is_null($isRemove)) {
            $isRemove = $params['is_remove'];
        }

        $groupItems = Mage::getModel('shoppinglist/items')->getCollection()
            ->addFieldToFilter('list_id', $groupId);

        $groupLogos = $this->getGroupLogos($groupItems);
        $embroiderySetupSkus = Mage::helper('embroidery')->getEmbroiderySetupSkus();

        if ($rmvItemIds) {
            try {
                foreach ($rmvItemIds as $rmvItemId) {
                    $isLogoSetup = false;
                    //get the logo, if any, for the saved item
                    foreach ($groupLogos as $groupItemId => $logo) {
                        if ($groupItemId == $rmvItemId) {
                            $itemLogo = $logo;
                        }
                    }

                    foreach ($groupLogos as $groupItemWithLogoId => $logo) {
                        if ($groupItemWithLogoId != $rmvItemId) {
                            if ($logo == $itemLogo) {
                                $groupItemsWithSameLogo[] = $groupItems->getItemById($groupItemWithLogoId);
                            }
                        }
                    }

                    // delete the associated embroidery setup item, if any
                    foreach ($groupItemsWithSameLogo as $groupItemWithSameLogo) {
                        $gpItemProdId = $groupItemWithSameLogo->getProductId();
                        $productSku = Mage::getModel('catalog/product')->load($gpItemProdId)->getSku();

                        if (in_array($productSku, $embroiderySetupSkus)) {
                            $shopcartPosition = -1;
                            $sort[$shopcartPosition]['prod_name'] = Mage::getModel('catalog/product')->load($gpItemProdId)->getName();
                            $sort[$shopcartPosition]['is_remove'] = $isRemove;
                            $groupItemWithSameLogo->delete();
                        }
                    }

                    $removedItem = $groupItems->getItemById($rmvItemId);
                    $shopcartPosition = $params['sort_order'];
                    $productId = $removedItem->getProductId();
                    $sort[$shopcartPosition]['prod_name'] = Mage::getModel('catalog/product')->load($productId)->getName();
                    $sort[$shopcartPosition]['is_remove'] = $isRemove;
                    $removedItem->delete();
                }

                $group = Mage::getModel('shoppinglist/group')->load($groupId);

                if (!$group->getItems()) {
                    $this->deleteAction($group->getId());
                }

                $sess = Mage::getSingleton('core/session');
                $sess->unsCartSort();
                $sess->unsSaveCartSort()->setSaveCartSort($sort);

            } catch (Exception $e) {
                return;
            }
        }
    }

    protected function getGroupLogos($groupItems)
    {
        $logos = [];

        foreach ($groupItems as $groupItem) {
            $req = unserialize($groupItem->getBuyRequest());

            foreach ($req['embroidery_data'] as $option) {
                foreach ($option['types'] as $type) {
                    if($type['name'] == 'logo') {
                        foreach ($type['options'] as $typeOption) {
                            $logos[$groupItem->getId()] = $typeOption['image'];
                        }
                    }
                }
            }
        }

        return $logos;
    }

    public function settingEmailAction()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $model = Mage::getModel('shoppinglist/group')->load($post['group_id']);
                $model->setSendReminderAfter($post['email-reminder']);
                $model->setUpdatedAt(now());
                $model->save();
                $this->_redirect('*/');
            } catch (Exception $e) {
                $this->_redirect('*/');
                return;
            }
        }
    }

    public function cartAction()
    {
        $quote = Mage::getSingleton('checkout/cart');
        $itemIds = explode(',', $this->getRequest()->getParam('items'));
        $groupId = $this->getRequest()->getParam('group_id');
        $referer_url = Mage::helper('core/http')->getHttpReferer();

        try {
            if ($itemIds[0] == 'all') {
                $group = Mage::getModel('shoppinglist/group')->load($groupId);
                $items = $group->getItems();

                if (count($items)) {

                    foreach ($items as $item) {
                        $qty = $item->getQty();
                        $product = Mage::getModel('catalog/product')
                            ->load($item->getProductId())
                            ->setQty($qty);
                        $req = unserialize($item->getBuyRequest());
                        $req['qty'] = $product->getQty();
                        $prodOpts = $product->getCustomOptions();
                        $prodBuyRequest = unserialize($prodOpts['info_buyRequest']->getValue());
                        $embroideryData = $prodBuyRequest['product-embroidery-options'];
                        $embroideryData = str_replace("\\", "",$embroideryData);
                        if($embroideryData){
                            $product->addCustomOption('embroidery_options', $embroideryData);
                        }

                        $quote->addProduct($product, $req);
                        $itemIds[] = $item->getId();
                    }

                    $this->removeItemAction($groupId, $itemIds, false);

                    $quote->save();

                    $this->_redirectUrl(Mage::getUrl('checkout/cart'));
                    return;
                }
            } else {
                if ($itemIds) {
                    foreach ($itemIds as $itemId) {
                        if ($itemId != '' || $itemId != null) {
                            $item = Mage::getModel('shoppinglist/items')->load($itemId);
                            $qty = $item->getQty();
                            $product = Mage::getModel('catalog/product')
                                ->load($item->getProductId())
                                ->setQty($qty);
                            //  max(0.01) ;
                            $req = unserialize($item->getBuyRequest());
                            $req['qty'] = $product->getQty();

                            $embroideryData = $req['product-embroidery-options'];
                            $embroideryData = str_replace("\\", "",$embroideryData);
                            if($embroideryData){
                                $product->addCustomOption('embroidery_options', $embroideryData);
                            }

                            $quote->addProduct($product, $req);
                            $itemIds[] = $item->getId();
                        }
                    }

                    $this->removeItemAction($groupId, $itemId, false);

//                    Mage::getSingleton('core/session')->unsCartSort()->setCartSort($sort);
                    Mage::getSingleton('core/session')->unsCartSort();

                    $quote->save();
                    $this->_redirectUrl(Mage::getUrl('checkout/cart'));
                    return;
                }
            }
        } catch (Exception $e) {
            $this->_redirectUrl($referer_url);
            return;
        }
    }

    /*
    * update module version 1.3
    * Author : Xboy
    * function : sendmailAction
    * show popup send mail
    */
    public function sendmailAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /* function : sendmail_postAction
    *  note : save data and send mail
    */
    public function sendmail_postAction()
    {
        $postData = $this->getRequest()->getParams();
        if (count($postData) > 0) {
            if (Mage::getSingleton("customer/session")->isLoggedIn()) {
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                // call function sendEmailToFriends in helper Data
                if ((isset($postData['group_id'])) && ($postData['email_friends']) && ($postData['name_friends']) && ($postData['text_massage']))
                    $sendMail = Mage::helper('shoppinglist')->sendEmailToFriends($customer->getId(), $postData['group_id'], $postData['email_friends'], $postData['name_friends'], $postData['text_massage']);
            }
        }
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }
    /* end update version 1.3 */

}