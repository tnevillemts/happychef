<?php
require_once 'Magebuzz/Shoppinglist/controllers/IndexController.php';

class HappyChef_Shoppinglist_IndexController extends Magebuzz_Shoppinglist_IndexController {

    public function additemAction()
    {
        $params = $this->getRequest()->getParams();
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $groupName = $custId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $cartItems = $quote->getAllVisibleItems();
        $prodIds = [];

        // NEED CART ITEM TO GET CUSTOM OPTIONS - THEN GET PRODUCT ID FROM ITEM
        if (!empty($cartItems)) {
            $saveItemOpts = null;
            $logoSetups = [];
            $cartLogos = [];
            $cartHasLogoSetup = false;

            $embroiderySetupSkus = Mage::helper('embroidery')->getEmbroiderySetupSkus();

            foreach ($cartItems as $i => $item) {
                $itemId = $item->getId();
                if ($itemId == $params['item_id']) {
                    $cartItemToSave = $item;
                    $prodIds[] = $cartItemToSave->getProduct()->getId();
                    $saveItemOpts = $this->getSummaryData($cartItemToSave);
                    if ($saveItemOpts) {
                        $saveItemLogos = $this->getLogo($saveItemOpts);
                    }
                } else {
                    $isLogoSetup = false;
                    if (in_array($item->getSku(), $embroiderySetupSkus)) {
                        $logoSetups[$itemId]['product_id'] = $item->getProductId();
                        $logoSetups[$itemId]['image'] = $item->getAdditionalData();
                        $cartHasLogoSetup = $isLogoSetup = true;
                    }

                    if(!$isLogoSetup) {
                        $cartOpts = $this->getSummaryData($item);
                        if (count($cartOpts['embroidery_data'])) {
                            $cartLogos[] = $this->getLogo($cartOpts);
                        }
                    }
                }
            }

            if ($cartHasLogoSetup && count($saveItemLogos)) {
                $logoSetupsToSave = $this->allocateLogoSetups($cartLogos, $saveItemLogos, $logoSetups);
            }
        } else {
            return;
        }

        if (count($logoSetupsToSave)) {
            foreach ($logoSetupsToSave as $logoSetupToSave) {
                $prodIds[] = $logoSetupToSave['product_id'];
            }
        }

        $groupId = Mage::helper('shoppinglist/group')->saveNewGroup($custId, $groupName, 1, 0);

        $sort = array();

        if (count($prodIds)) {
            foreach ($prodIds as $prodId) {
                try {
                    $isLogoSetup = false;
                    $product = Mage::getModel('catalog/product')->load($prodId);

                    if ((isset($params['qty'])) && ($params['qty'] != '')) {
                        $qty = (int)$params['qty'];
                    } else {
                        $stockItem = $product->getStockItem();
                        if ($stockItem) {
                            $qty = ($stockItem->getMinSaleQty() && $stockItem->getMinSaleQty() > 0 ? $stockItem->getMinSaleQty() * 1 : null);
                        } else {
                            $qty = 1;
                        }
                    }

                    $product_type_id = $product->getTypeId();

                    if (($product_type_id == 'grouped') && (isset($params['super_group'])) && ($params['super_group'] != '')) {
                        $subProducts = $params['super_group'];
                        foreach ($subProducts as $pId => $qty) {
                            if ($qty > 0) {
                                $customOptions = serialize($saveItemOpts);
                                Mage::helper('shoppinglist')->assignProductToList($groupId, $prodId, $qty, $customOptions);
                            }
                        }
                    } else {
                        if (in_array($product->getSku(), $embroiderySetupSkus)) {
                            $setupPrice = Mage::getModel('catalog/product')->loadByAttribute('sku', 'EMBROIDERY-SETUP')->getPrice();
                            $saveItemOpts['snapshot_row_total'] = $saveItemOpts['snapshot_price'] = $setupPrice;
                            $isLogoSetup = true;
                        }

                        $customOptions = serialize($saveItemOpts);
                        Mage::helper('shoppinglist')->assignProductToList($groupId, $prodId, $qty, $customOptions);
                    }

                    if ($isLogoSetup) {
                        $shopcartPosition = -1;
                    } else {
                        $shopcartPosition = $params['sort_order'];
                    }

                    $sort[$shopcartPosition]['product_name'] = $product->getName();
                    $sort[$shopcartPosition]['is_remove'] = false;

                } catch (Exception $e) {
                    Zend_Debug::dump($e->getMessage());
                    return;
                }
            }

            $sess = Mage::getSingleton('core/session');
            $sess->unsSaveCartSort();
            $sess->unsCartSort()->setCartSort($sort);

            $quote->removeItem($params['item_id']);
            $quote->save();

            $result['message'] =  '<div style="margin-top:50px;" id="messages_shoppinglist">
                <ul class="messages">
                    <li class="success-msg">
                        <ul>
                            <li>
                                <span>'.Mage::helper('shoppinglist')->__('The product successfully saved to your </br>'.Mage::helper('shoppinglist')->getGeneralTitle().'.').'</span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>';

            if ($isAjax) {
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            } else {
                $this->_redirect('checkout/cart/');
            }
        }
    }

    protected function getSummaryData($item) {
        $prod = $item->getProduct();
        $prodId = $prod->getId();
        $customOptions = $prod->getCustomOptions();
        if (!is_null($customOptions)) {
            if (!empty($customOptions['info_buyRequest'])) {
                $opts = unserialize($customOptions['info_buyRequest']->getValue());
            }
        }
        $opts['orig_item_id'] = $item->getId();
        $opts['orig_prod_id'] = $prodId;
        $opts['snapshot_price'] = $item->getPrice();
        $opts['snapshot_row_total'] = $item->getRowTotal();
        $opts['embroidery_data'] = $this->prepareSummaryData($opts['product-embroidery-options']);
        return $opts;
    }

    protected function getLogo($itemOptions)
    {
        $logos = [];
        foreach ($itemOptions['embroidery_data'] as $option) {
            foreach ($option['types'] as $type) {
                if($type['name'] == 'logo') {
                    foreach ($type['options'] as $typeOption) {
                        $logos[] = $typeOption['image'];
                    }
                }
            }
        }

        return $logos;
    }

    protected function allocateLogoSetups($cartLogos, $savedItemLogos, $logoSetups)
    {
        $logoSetupsToSave = false;

        foreach ($logoSetups as $key => $value) {
            $logo = $value['image'];
            //see if other cart items have the same logo applied
            if (count($cartLogos)) {
                foreach ($cartLogos as $cartLogo) {
                    if ($cartLogo['image'] = $logo) {
                        $cartItemHasLogo = true;
                    }
                }
            }

            if (count($savedItemLogos)) {
                foreach ($savedItemLogos as $savedItemLogo) {
                    if ($savedItemLogo == $logo) {
                        $saveItemHasLogo = true;
                    }
                }
            }

            if (!$cartItemHasLogo && $saveItemHasLogo) {
                $logoSetupsToSave[$key] = $value;
            }
        }

        return $logoSetupsToSave;
    }

    public function prepareSummaryData($jsonData)
    {
        $embroideryReverse = [];
        $productData = json_decode($jsonData, true);
        // Creates new object based on productData presented in reversed structure: Locations -> Types -> Options
        foreach ($productData as $productDatum) {
            echo $productDatum;
            foreach ($productDatum['types'] as $typeId => $type) {
                echo $typeId;
                echo $type;

                $locationObject = [];

                foreach ($type['locations'] as $locationId => $location) {
                    if (count($location['selectedOption']) > 0) {
                        $typeObject = [];
                        $typeObject['name'] = $type['name'];
                        $typeObject['id'] = $typeId;
                        $typeObject['sortorder'] = $type['sortorder'];

                        $typeObject['options'] = [];

                        foreach ($location['selectedOption'] as $index => $option) {
                            $optionObject = [];
                            $optionObject['id'] = $option['id'];
                            $optionObject['name'] = $option['name'];
                            $optionObject['price'] = $option['price'];
                            $optionObject['textLine'] = $option['textLine'];
                            $optionObject['textColorId'] = $option['textColor'];
                            $optionObject['textColorValue'] = $option['textColorValue'];
                            $optionObject['artworkColorId'] = $option['artworkColor'];
                            $optionObject['artworkColorValue'] = $option['artworkColorValue'];
                            $optionObject['artworkColorImage'] = $option['artworkColorImage'];
                            $optionObject['textStyleId'] = $option['textStyle'];
                            $optionObject['textStyleValue'] = $option['textStyleValue'];
                            $optionObject['textLineNumber'] = $option['sortorder'];
                            $optionObject['image'] = $option['image'];
                            $optionObject['originalImage'] = $option['originalFilename'];

                            $typeObject['options'][] = $optionObject;
                        }

                        $addNewLocation = true;
                        foreach ($embroideryReverse as $idx => $loc) {
                            if ($loc['id'] == $locationId) {
                                $loc['types'][] = $typeObject;
                                $addNewLocation = false;
                                return false;
                            }
                        } // $idx => $loc

                        if ($addNewLocation) {
                            $locationObject = [];
                            $locationObject['name'] = $location['name'];
                            $locationObject['id'] = $locationId;
                            $locationObject['types'] = [];
                            $locationObject['types'][] = $typeObject;
                            $embroideryReverse[] = $locationObject;
                        }
                    }
                } //$locationId => $location)
            } //$typeId => $type
        } //$productDatum

        return $embroideryReverse;
    }
}
