<?php

class HappyChef_Shoppinglist_Helper_Rewrite_Shoppinglist_Data extends Magebuzz_Shoppinglist_Helper_Data {

    public function getGeneralTitle() {
        if(Mage::getStoreConfig('shoppinglist/title_setting/general_title')) {
            return Mage::getStoreConfig('shoppinglist/title_setting/general_title');
        } else {
            return 'Saved Cart';
        }
    }

}