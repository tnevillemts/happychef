<?php
class HappyChef_Shoppinglist_Helper_Rewrite_Shoppinglist_Group extends Magebuzz_Shoppinglist_Helper_Group {

    public function saveNewGroup($customerID, $groupName, $status, $existGroupName)
    {
        $helper = Mage::helper('shoppinglist');
        $sendEmailAfter = $helper->getConfigEmailReminder();
        $model = Mage::getModel('shoppinglist/group');

        /* Happychef removes the ability to create repeated name lists */
        $collection = $model->getCollection()->addFieldToFilter('list_name',$groupName);

        if(count($collection)>0) {
            $existingLists = $collection->getItems();
            $existingListId = reset($existingLists)->getId();
            return $existingListId;
        }
        /* edit end */

        //$model->load('list_name',$groupName)->getData();
        $model->setCustomerId($customerID);
        $model->setListName($groupName);
        $model->setStatus($status);
        $model->setCreatedAt(now());
        $model->setUpdatedAt(now());
        $model->setSendReminderAfter($sendEmailAfter[0]);
        try {
            $model->setId(null)->save();
        } catch (Exception $ex) {
        }
        return $model->getId();
    }

}