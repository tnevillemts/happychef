<?php

class HappyChef_Giftcard_Helper_Data extends Mage_Core_Helper_Abstract {

    protected $_emailGiftcardSku;
    protected $_mailGiftcardSku;

    public function getMailGiftcard(){
        $mailGiftcard = Mage::getModel('catalog/product')->loadByAttribute('sku' , $this->getMailGiftcardSku());
        if(!$mailGiftcard && !$sent){
            Mage::getModel('core/session')->addError($this->__("Sorry, there aren't mail giftcards active at the moment."));
            $sent = true;
        }
        return $mailGiftcard;
    }

    public function getEmailGiftcard(){
        $emailGiftcard = Mage::getModel('catalog/product')->loadByAttribute('sku' , $this->getEmailGiftcardSku());
        if(!$emailGiftcard && !$sent){
            Mage::getModel('core/session')->addError($this->__("Sorry, there aren't email giftcards active at the moment."));
            $sent = true;
        }
        return $emailGiftcard;
    }

    public function getMailGiftcardId(){
        if($this->getMailGiftcard())
            return $this->getMailGiftcard()->getId();

        return false;
    }

    public function getEmailGiftcardId(){
        if($this->getEmailGiftcard())
            return $this->getEmailGiftcard()->getId();

        return false;
    }

    public function getMailGiftcardSku(){
        if(!$this->_mailGiftcardSku)
            $this->_mailGiftcardSku = Mage::getStoreConfig('aw_giftcard/happychef/mailgc_sku');
        return $this->_mailGiftcardSku;
    }

    public function getEmailGiftcardSku(){
        if(!$this->_emailGiftcardSku)
            $this->_emailGiftcardSku = Mage::getStoreConfig('aw_giftcard/happychef/emailgc_sku');
        return $this->_emailGiftcardSku;
    }
}