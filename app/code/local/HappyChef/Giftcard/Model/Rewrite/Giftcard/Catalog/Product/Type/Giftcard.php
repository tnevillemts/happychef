<?php

class HappyChef_Giftcard_Model_Rewrite_Giftcard_Catalog_Product_Type_Giftcard extends AW_Giftcard_Model_Catalog_Product_Type_Giftcard {

    protected function _prepareProduct(Varien_Object $buyRequest, $product, $processMode)
    {
        $result = array();
        if (method_exists(get_parent_class($this), 'prepareForCartAdvanced')) {
            $result = parent::_prepareProduct($buyRequest, $product, $processMode);
            if (is_string($result)) {
                return $result;
            }
        }

        try {
            $amount = $this->_validateAndGetAmount($buyRequest, $product, $processMode);
        } catch (Mage_Core_Exception $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            return Mage::helper('aw_giftcard')->__('An error has occurred while adding product to cart.');
        }

        $product->addCustomOption('aw_gc_amounts', $amount, $product);
        $product->addCustomOption('aw_gc_sender_name', $buyRequest->getAwGcSenderName(), $product);
        $product->addCustomOption('aw_gc_recipient_first_name', $buyRequest->getAwGcRecipientFirstName(), $product);
        $product->addCustomOption('aw_gc_recipient_last_name', $buyRequest->getAwGcRecipientLastName(),$product);
        $product->addCustomOption('aw_gc_recipient_address', $buyRequest->getAwGcRecipientAddress(), $product);
        $product->addCustomOption('aw_gc_recipient_address2', $buyRequest->getAwGcRecipientAddress2(),$product);
        $product->addCustomOption('aw_gc_recipient_city', $buyRequest->getAwGcRecipientCity(), $product);
        $product->addCustomOption('aw_gc_recipient_state', $buyRequest->getAwGcRecipientState(),$product);
        $product->addCustomOption('aw_gc_recipient_zipcode', $buyRequest->getAwGcRecipientZipcode(),$product);
        $product->addCustomOption('aw_gc_recipient_email', $buyRequest->getAwGcRecipientEmail(), $product);
        $product->addCustomOption('giftcard_design', $buyRequest->getGiftcardDesign(), $product);

        if (!$this->isTypePhysical($product)) {
            $product->addCustomOption('aw_gc_sender_email', $buyRequest->getAwGcSenderEmail(), $product);
            $product->addCustomOption('aw_gc_recipient_email', $buyRequest->getAwGcRecipientEmail(), $product);
        }

        $messageAllowed = (bool) $product->getAwGcAllowMessage();
        if ($product->getAwGcConfigAllowMessage()) {
            $messageAllowed = (bool) Mage::helper('aw_giftcard/config')->isAllowGiftMessage();
        }

        if ($messageAllowed) {
            $product->addCustomOption('aw_gc_message', trim($buyRequest->getAwGcMessage()), $product);
        }
        return $result;
    }

    public function processBuyRequest($product, $buyRequest)
    {
        $options = array(
            'aw_gc_amounts'             => $buyRequest->getAwGcAmount(),
            'aw_gc_custom_amount'       => $buyRequest->getAwGcCustomAmount(),
            'aw_gc_sender_name'         => $buyRequest->getAwGcSenderName(),
            'aw_gc_sender_email'        => $buyRequest->getAwGcSenderEmail(),
            'aw_gc_recipient_first_name'=> $buyRequest->getAwGcRecipientFirstName(),
            'aw_gc_recipient_last_name'	=> $buyRequest->getAwGcRecipientLastName(),
            'aw_gc_recipient_email' 	=> $buyRequest->getAwGcRecipientEmail(),
            'aw_gc_message'         	=> $buyRequest->getAwGcMessage(),
            'aw_gc_recipient_address'	=> $buyRequest->getAwGcRecipientAddress(),
            'aw_gc_recipient_address2'	=> $buyRequest->getAwGcRecipientAddress2(),
            'aw_gc_recipient_city'		=> $buyRequest->getAwGcRecipientCity(),
            'aw_gc_recipient_state'		=> $buyRequest->getAwGcRecipientState(),
            'aw_gc_recipient_zipcode'	=> $buyRequest->getAwGcRecipientZipcode(),
            'giftcard_design'           => $buyRequest->getGiftcardDesign()

        );
        return $options;
    }

    protected function _validateBuyRequest(Varien_Object $buyRequest, $product, $processMode)
    {
        $isStrictProcessMode = true;
        if (method_exists(get_parent_class($this), '_isStrictProcessMode')) {
            $isStrictProcessMode = $this->_isStrictProcessMode($processMode);
        }

        if (!$buyRequest->getAwGcRecipientFirstName() && $isStrictProcessMode) {
            Mage::throwException(
                Mage::helper('aw_giftcard')->__('Please specify recipient firstname.')
            );
            return false;
        }
        if (!$buyRequest->getAwGcRecipientLastName() && $isStrictProcessMode) {
            Mage::throwException(
                Mage::helper('aw_giftcard')->__('Please specify recipient lastname.')
            );
            return false;
        }
        if (!$buyRequest->getAwGcRecipientEmail() && $isStrictProcessMode) {
            Mage::throwException(
                Mage::helper('aw_giftcard')->__('Please specify recipient email.')
            );
            return false;
        }
        if ($this->isTypePhysical($product)) {
            if (!$buyRequest->getAwGcRecipientAddress() && $isStrictProcessMode) {
                Mage::throwException(
                    Mage::helper('aw_giftcard')->__('Please specify recipient address.')
                );
                return false;
            }
            if (!$buyRequest->getAwGcRecipientCity() && $isStrictProcessMode) {
                Mage::throwException(
                    Mage::helper('aw_giftcard')->__('Please specify recipient city.')
                );
                return false;
            }
            if (!$buyRequest->getAwGcRecipientState() && $isStrictProcessMode) {
                Mage::throwException(
                    Mage::helper('aw_giftcard')->__('Please specify recipient state.')
                );
                return false;
            }
            if (!$buyRequest->getAwGcRecipientZipcode() && $isStrictProcessMode) {
                Mage::throwException(
                    Mage::helper('aw_giftcard')->__('Please specify recipient zip.')
                );
                return false;
            }
        }

//        if (!$buyRequest->getAwGcSenderEmail() && $isStrictProcessMode) {
//            Mage::throwException(
//                Mage::helper('aw_giftcard')->__('Please specify sender email.')
//            );
//            return false;
//        }

        return true;
    }

    private function _validateAndGetAmount(Varien_Object $buyRequest, $product, $processMode)
    {
        $product = $this->getProduct($product);
        $isStrictProcessMode = true;
        if (method_exists(get_parent_class($this), '_isStrictProcessMode')) {
            $isStrictProcessMode = $this->_isStrictProcessMode($processMode);
        }

        $allowedAmounts = array();
        $amountOptions = $product->getData('aw_gc_amounts');

        if (null === $amountOptions) {
            $amountOptions = $this->getProduct($product)->getPriceModel()->getAmountOptions($product);
        }

        foreach ($amountOptions as $value) {
            $allowedAmounts[] = Mage::app()->getStore()->roundPrice($value['value']);
        }

        $allowOpen = $product->getAwGcAllowOpenAmount();
        $minAmount = $product->getAwGcOpenAmountMin();
        $maxAmount = $product->getAwGcOpenAmountMax();

        $selectedAmountOption = $buyRequest->getData('aw_gc_amount');
        $customAmount = $buyRequest->getData('aw_gc_custom_amount');

        $rate = Mage::app()->getStore()->getCurrentCurrencyRate();
        if ($rate != 1 && $customAmount) {
            $customAmount = Mage::app()->getLocale()->getNumber($customAmount);
            if (is_numeric($customAmount) && $customAmount) {
                $customAmount = Mage::app()->getStore()->roundPrice($customAmount/$rate);
            }
        }

        $amount = null;

        if (($selectedAmountOption == 'custom' || !$selectedAmountOption) && $allowOpen) {
            if ($customAmount <= 0 && $isStrictProcessMode) {
                Mage::throwException(
                    Mage::helper('aw_giftcard')->__('Please specify Gift Card amount.')
                );
            }

            if (!$minAmount || ($minAmount && $customAmount >= $minAmount)) {
                if (!$maxAmount || ($maxAmount && $customAmount <= $maxAmount)) {
                    $amount = $customAmount;
                }

                if ($maxAmount && $customAmount > $maxAmount && $isStrictProcessMode) {
                    $messageAmount = Mage::helper('core')->currency($maxAmount, true, false);
                    Mage::throwException(
                        Mage::helper('aw_giftcard')->__('Maximum allowed Gift Card amount is %s', $messageAmount)
                    );
                }
            }

            if ($minAmount && $customAmount < $minAmount && $isStrictProcessMode) {
                $messageAmount = Mage::helper('core')->currency($minAmount, true, false);
                Mage::throwException(
                    Mage::helper('aw_giftcard')->__('Minimum allowed Gift Card amount is %s', $messageAmount)
                );
            }
        }

        if (is_numeric($selectedAmountOption) && in_array($selectedAmountOption, $allowedAmounts)) {
            $amount = $selectedAmountOption;
        }

        if (is_null($amount) && count($allowedAmounts) == 1) {
            $amount = array_shift($allowedAmounts);
        }

        if (is_null($amount) && $this->getProduct($product)->getCustomOption('aw_gc_amounts')) {
            $amount = $this->getProduct($product)->getCustomOption('aw_gc_amounts')->getValue();
        }

        if (is_null($amount) && $isStrictProcessMode) {
            Mage::throwException(
                Mage::helper('aw_giftcard')->__('Please specify Gift Card amount.')
            );
        }
        $this->_validateBuyRequest($buyRequest, $product, $processMode);

        return $amount;
    }
}
