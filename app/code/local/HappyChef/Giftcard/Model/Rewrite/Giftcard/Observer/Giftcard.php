<?php
class HappyChef_Giftcard_Model_Rewrite_Giftcard_Observer_Giftcard extends AW_Giftcard_Model_Observer_Giftcard {

    public function convertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {
        $orderItem = $observer->getEvent()->getOrderItem();
        $quoteItem = $observer->getEvent()->getItem();
        $_product = $quoteItem->getProduct();

        if ($quoteItem->getProductType() != AW_Giftcard_Model_Catalog_Product_Type_Giftcard::TYPE_CODE) {
            return $this;
        }

        $giftCardOptions = array(
            'aw_gc_amounts',
            'aw_gc_custom_amount',
            'aw_gc_sender_name',
            'aw_gc_sender_email',
            'aw_gc_recipient_first_name',
            'aw_gc_recipient_last_name',
            'aw_gc_recipient_address',
            'aw_gc_recipient_address2',
            'aw_gc_recipient_city',
            'aw_gc_recipient_state',
            'aw_gc_recipient_zipcode',
            'aw_gc_recipient_email',
            'aw_gc_message',
            'giftcard_design'
        );

        $productOptions = $orderItem->getProductOptions();
        foreach ($giftCardOptions as $_optionKey) {
            if ($option = $quoteItem->getProduct()->getCustomOption($_optionKey)) {
                $productOptions[$_optionKey] = $option->getValue();
            }
        }

        $store = Mage::app()->getStore($orderItem->getStoreId());
        $website = $store->getWebsite();

        $expireAfter = Mage::helper('aw_giftcard/config')->getExpireValue($website);
        if (!$_product->getAwGcConfigExpire()) {
            $expireAfter = $_product->getAwGcExpire();
        }
        $productOptions['aw_gc_expire_at'] = $expireAfter;

        $emailTemplate = Mage::helper('aw_giftcard/config')->getEmailTemplate($store);
        if (!$_product->getAwGcConfigEmailTemplate()) {
            $emailTemplate = $_product->getAwGcEmailTemplate();
        }
        $productOptions['aw_gc_email_template'] = $emailTemplate;

        $giftCardTypeValue = $_product->getTypeInstance()->getGiftcardTypeValue($_product);
        $productOptions['aw_gc_type'] = $giftCardTypeValue;
        $productOptions['aw_gc_created_codes'] = array();

        $orderItem->setProductOptions($productOptions);

        return $this;
    }

}