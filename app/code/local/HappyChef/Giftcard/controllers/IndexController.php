<?php
require_once 'Mage/Catalog/controllers/ProductController.php';
class HappyChef_Giftcard_IndexController extends Mage_Catalog_ProductController
{

    /**
     * Product view action
     */
    public function indexAction()
    {

        // Get initial data from request
        $productId  = (int) $this->getRequest()->getParam('id');
        $specifyOptions = $this->getRequest()->getParam('options');

        // Prepare helper and params
        $viewHelper = Mage::helper('catalog/product_view');
        $gcHelper = Mage::helper('happychef_giftcard');

        $requestedGc = Mage::getModel('catalog/product')->load($productId);
        $emailGc = $gcHelper->getEmailGiftcard();
        $mailGc = $gcHelper->getMailGiftcard();

        if ($productId && $requestedGc && $requestedGc->getStatus() == 1) {
            $pId = $productId;
        } else if ($emailGc && $emailGc->getStatus() == 1) {
            $pId = $gcHelper->getEmailGiftcardId();
        } else if ($mailGc && $mailGc->getStatus() == 1) {
            $pId = $gcHelper->getMailGiftcardId();
        } else {
            $pId = false;
        }

        $params = new Varien_Object();
        $params->setSpecifyOptions($specifyOptions);

        try {
            $viewHelper->prepareAndRender($pId, $this, $params);
        } catch (Exception $e) {
            if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
                if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
                    $this->_redirect('');
                } elseif (!$this->getResponse()->isRedirect()) {
                    $this->_forward('noRoute');
                }
            } else {
                Mage::logException($e);
                $this->_forward('noRoute');
            }
        }

    }

}
