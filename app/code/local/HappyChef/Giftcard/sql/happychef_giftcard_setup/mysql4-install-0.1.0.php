<?php

$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->addAttribute('catalog_product', 'giftcard_design_1', array(
    'backend'       => 'happychef_giftcard/entity_attribute_backend_image',
    'label'         => 'Design 1',
    'group'         => 'Giftcard Design',
    'input'         => 'image',
    'class'         => 'validate-digit',
    'global'        => true,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'default'       => '',
    'visible_on_front' => false,
    'used_in_product_listing' => true,
    'apply_to'      => 'aw_giftcard',
));

$setup->addAttribute('catalog_product', 'giftcard_design_2', array(
    'backend'       => 'happychef_giftcard/entity_attribute_backend_image',
    'label'         => 'Design 2',
    'group'         => 'Giftcard Design',
    'input'         => 'image',
    'class'         => 'validate-digit',
    'global'        => true,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'default'       => '',
    'visible_on_front' => false,
    'used_in_product_listing' => true,
    'apply_to'      => 'aw_giftcard',
));

$setup->addAttribute('catalog_product', 'giftcard_design_3', array(
    'backend'       => 'happychef_giftcard/entity_attribute_backend_image',
    'label'         => 'Design 3',
    'group'         => 'Giftcard Design',
    'input'         => 'image',
    'class'         => 'validate-digit',
    'global'        => true,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'default'       => '',
    'visible_on_front' => false,
    'used_in_product_listing' => true,
    'apply_to'      => 'aw_giftcard',
));

$setup->addAttribute('catalog_product', 'giftcard_design_4', array(
    'backend'       => 'happychef_giftcard/entity_attribute_backend_image',
    'label'         => 'Design 4',
    'group'         => 'Giftcard Design',
    'input'         => 'image',
    'class'         => 'validate-digit',
    'global'        => true,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,
    'default'       => '',
    'visible_on_front' => false,
    'used_in_product_listing' => true,
    'apply_to'      => 'aw_giftcard',
));

$installer->endSetup();