<?php

class HappyChef_Giftcard_Block_Rewrite_Giftcard_Frontend_Checkout_Cart_Item_Renderer extends AW_Giftcard_Block_Frontend_Checkout_Cart_Item_Renderer {
    /**
     * Retrieve URL to item Product
     *
     * @return string
     */
    public function getProductUrl()
    {
        $_product = $this->getProduct();
        return $this->getUrl('happychef_giftcard/index/index',array('id'=>$_product->getId()));
    }

    protected function _getGiftcardOptions()
    {
        $senderName     		= $this->_getPreparedCustomOptionByCode('aw_gc_sender_name');
        $recipientFirstName     = $this->_getPreparedCustomOptionByCode('aw_gc_recipient_first_name');
        $recipientLastName		= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_last_name');
        $senderEmail    		= $this->_getPreparedCustomOptionByCode('aw_gc_sender_email');
        $recipientEmail 		= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_email');
        $message        		= $this->_getPreparedCustomOptionByCode('aw_gc_message');
        $address1				= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_address');
        $address2				= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_address2');
        $state					= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_state');
        $city					= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_city');
        $zipcode				= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_zipcode');
        $gcDesign               = $this->_getPreparedCustomOptionByCode('giftcard_design');
        $result = array();
        if ($senderName) {
            $senderOptionText = $senderName;
            if ($senderEmail) {
                $senderOptionText = "{$senderName} &lt;{$senderEmail}&gt;";
            }
            $result[] = array(
                'label' => $this->__('Gift Card Sender'),
                'value' => $senderOptionText
            );
        }

        if ($recipientFirstName) {
            $recipientOptionText = $recipientFirstName;
            if ($recipientLastName) {
                $recipientOptionText = "{$recipientFirstName} "."{$recipientLastName}";
            }
            $result[] = array(
                'label' => $this->__("Recipient"),
                'value' => $recipientOptionText
            );
        }
        if ($recipientEmail) {
            $recipientOptionText = $recipientEmail;
            $result[] = array(
                'label' => $this->__("Recipient's Email"),
                'value' => $recipientOptionText
            );
        }
        if ($address1) {
            $recipientOptionText = $address1;
            if ($address2) {
                $recipientOptionText .= " {$address2}";
            }
            if ($city) {
                $recipientOptionText .= ", {$city}";
            }
            if ($state) {
                $recipientOptionText .= ", {$state}";
            }
            if ($zipcode) {
                $recipientOptionText .= " {$zipcode}";
            }
            $result[] = array(
                'label' => $this->__("Address"),
                'value' => $recipientOptionText
            );
        }
        if (trim($message)) {
            $result[] = array(
                'label' => $this->__('Gift Card Message'),
                'value' => $message
            );
        }

        if ($gcDesign) {
            $result[] = array(
                'label' => $this->__('Gift Card Design'),
                'value' => basename($gcDesign)
            );
        }
        return $result;
    }

    /**
     * Get product thumbnail image
     *
     * @return Mage_Catalog_Model_Product_Image
     */
    public function getProductThumbnail()
    {
        if($this->_getPreparedCustomOptionByCode('giftcard_design')){
            return $this->helper('happychef_giftcard/image')->init($this->getProduct(), 'thumbnail', $this->_getPreparedCustomOptionByCode('giftcard_design'));
        }else{
            return parent::getProductThumbnail();
        }
    }
}