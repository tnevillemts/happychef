<?php

class HappyChef_Giftcard_Block_Rewrite_Giftcard_Adminhtml_Sales_Items_Column_Name_Giftcard extends AW_Giftcard_Block_Adminhtml_Sales_Items_Column_Name_Giftcard {

    protected function _getGiftcardOptions()
    {
        $senderName     		= $this->_getPreparedCustomOptionByCode('aw_gc_sender_name');
        $message        		= $this->_getPreparedCustomOptionByCode('aw_gc_message');
        $recipientFirstName 	= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_first_name');
        $recipientLastName		= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_last_name');
        $senderEmail    		= $this->_getPreparedCustomOptionByCode('aw_gc_sender_email');
        $recipientEmail 		= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_email');
        $address1				= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_address');
        $address2				= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_address2');
        $state					= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_state');
        $city					= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_city');
        $zipcode				= $this->_getPreparedCustomOptionByCode('aw_gc_recipient_zipcode');
        $type                   = $this->_getPreparedCustomOptionByCode('aw_gc_type');
        $giftCards              = $this->_getPreparedCustomOptionByCode('aw_gc_created_codes');
        $isEmailSent            = $this->_getPreparedCustomOptionByCode('email_sent');
        $gcDesign               = $this->_getPreparedCustomOptionByCode('giftcard_design');

        if ($type) {
            $result[] = array(
                'label' => $this->__('Gift Card Type'),
                'value' => Mage::getModel('aw_giftcard/source_product_attribute_giftcard_type')->getOptionText($type)
            );
        }

        if ($senderName) {
            $senderOptionText = $senderName;
            if ($senderEmail) {
                $senderOptionText = "{$senderName} &lt;{$senderEmail}&gt;";
            }
            $result[] = array(
                'label' => $this->__('Gift Card Sender'),
                'value' => $senderOptionText
            );
        }

        if ($recipientFirstName) {
            $recipientOptionText = $recipientFirstName;
            if ($recipientLastName) {
                $recipientOptionText = "{$recipientFirstName} "."{$recipientLastName}";
            }
            $result[] = array(
                'label' => $this->__("Recipient"),
                'value' => $recipientOptionText
            );
        }
        if ($recipientEmail) {
            $recipientOptionText = $recipientEmail;
            $result[] = array(
                'label' => $this->__("Recipient's Email"),
                'value' => $recipientOptionText
            );
        }
        if ($address1) {
            $recipientOptionText = $address1;
            if ($address2) {
                $recipientOptionText .= " {$address2}";
            }
            if ($city) {
                $recipientOptionText .= ", {$city}";
            }
            if ($state) {
                $recipientOptionText .= ", {$state}";
            }
            if ($zipcode) {
                $recipientOptionText .= " {$zipcode}";
            }
            $result[] = array(
                'label' => $this->__("Address"),
                'value' => $recipientOptionText
            );
        }
        if (trim($message)) {
            $result[] = array(
                'label' => $this->__('Gift Card Message'),
                'value' => $message
            );
        }

        if ($gcDesign) {
            $result[] = array(
                'label' => $this->__('Gift Card Design'),
                'value' => basename($gcDesign)
            );
        }

        $_codes = array($this->__('N/A'));
        if ($giftCards) {
            $_codes = $giftCards;
        }

        $result[] = array(
            'label' => $this->__('Gift Card Codes'),
            'value' => implode('<br />', $_codes)
        );
        return $result;
    }

}