<?php
class EM_Gridmode_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/gridmode?id=15 
    	 *  or
    	 * http://site.com/gridmode/id/15 	
    	 */
    	/* 
		$gridmode_id = $this->getRequest()->getParam('id');

  		if($gridmode_id != null && $gridmode_id != '')	{
			$gridmode = Mage::getModel('gridmode/gridmode')->load($gridmode_id)->getData();
		} else {
			$gridmode = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($gridmode == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$gridmodeTable = $resource->getTableName('gridmode');
			
			$select = $read->select()
			   ->from($gridmodeTable,array('gridmode_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$gridmode = $read->fetchRow($select);
		}
		Mage::register('gridmode', $gridmode);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
}