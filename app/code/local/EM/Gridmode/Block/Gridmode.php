<?php
class EM_Gridmode_Block_Gridmode extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getGridmode()     
     { 
        if (!$this->hasData('gridmode')) {
            $this->setData('gridmode', Mage::registry('gridmode'));
        }
        return $this->getData('gridmode');
        
    }
}