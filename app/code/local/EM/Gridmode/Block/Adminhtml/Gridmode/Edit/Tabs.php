<?php

class EM_Gridmode_Block_Adminhtml_Gridmode_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('gridmode_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('gridmode')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('gridmode')->__('Item Information'),
          'title'     => Mage::helper('gridmode')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('gridmode/adminhtml_gridmode_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}