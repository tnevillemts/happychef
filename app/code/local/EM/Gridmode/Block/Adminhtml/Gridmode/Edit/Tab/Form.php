<?php

class EM_Gridmode_Block_Adminhtml_Gridmode_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('gridmode_form', array('legend'=>Mage::helper('gridmode')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('gridmode')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('gridmode')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('gridmode')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('gridmode')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('gridmode')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('gridmode')->__('Content'),
          'title'     => Mage::helper('gridmode')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getGridmodeData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getGridmodeData());
          Mage::getSingleton('adminhtml/session')->setGridmodeData(null);
      } elseif ( Mage::registry('gridmode_data') ) {
          $form->setValues(Mage::registry('gridmode_data')->getData());
      }
      return parent::_prepareForm();
  }
}