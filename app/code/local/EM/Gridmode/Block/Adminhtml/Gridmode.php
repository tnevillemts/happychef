<?php
class EM_Gridmode_Block_Adminhtml_Gridmode extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_gridmode';
    $this->_blockGroup = 'gridmode';
    $this->_headerText = Mage::helper('gridmode')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('gridmode')->__('Add Item');
    parent::__construct();
  }
}