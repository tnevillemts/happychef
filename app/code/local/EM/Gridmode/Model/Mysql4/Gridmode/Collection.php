<?php

class EM_Gridmode_Model_Mysql4_Gridmode_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('gridmode/gridmode');
    }
}