<?php

class EM_Gridmode_Model_Mysql4_Gridmode extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the gridmode_id refers to the key field in your database table.
        $this->_init('gridmode/gridmode', 'gridmode_id');
    }
}