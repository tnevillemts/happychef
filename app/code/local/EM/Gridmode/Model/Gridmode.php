<?php

class EM_Gridmode_Model_Gridmode extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('gridmode/gridmode');
    }
}