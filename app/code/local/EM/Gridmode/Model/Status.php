<?php

class EM_Gridmode_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('gridmode')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('gridmode')->__('Disabled')
        );
    }
}