<?php
class EM_Staticblock_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/staticblock?id=15 
    	 *  or
    	 * http://site.com/staticblock/id/15 	
    	 */
    	/* 
		$staticblock_id = $this->getRequest()->getParam('id');

  		if($staticblock_id != null && $staticblock_id != '')	{
			$staticblock = Mage::getModel('staticblock/staticblock')->load($staticblock_id)->getData();
		} else {
			$staticblock = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($staticblock == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$staticblockTable = $resource->getTableName('staticblock');
			
			$select = $read->select()
			   ->from($staticblockTable,array('staticblock_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$staticblock = $read->fetchRow($select);
		}
		Mage::register('staticblock', $staticblock);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
}