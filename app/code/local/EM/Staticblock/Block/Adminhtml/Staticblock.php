<?php
class EM_Staticblock_Block_Adminhtml_Staticblock extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_staticblock';
    $this->_blockGroup = 'staticblock';
    $this->_headerText = Mage::helper('staticblock')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('staticblock')->__('Add Item');
    parent::__construct();
  }
}