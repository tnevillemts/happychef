<?php

class EM_Staticblock_Block_Adminhtml_Staticblock_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'staticblock';
        $this->_controller = 'adminhtml_staticblock';
        
        $this->_updateButton('save', 'label', Mage::helper('staticblock')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('staticblock')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('staticblock_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'staticblock_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'staticblock_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('staticblock_data') && Mage::registry('staticblock_data')->getId() ) {
            return Mage::helper('staticblock')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('staticblock_data')->getTitle()));
        } else {
            return Mage::helper('staticblock')->__('Add Item');
        }
    }
}