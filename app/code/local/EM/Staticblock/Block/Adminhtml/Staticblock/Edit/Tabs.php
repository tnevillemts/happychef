<?php

class EM_Staticblock_Block_Adminhtml_Staticblock_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('staticblock_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('staticblock')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('staticblock')->__('Item Information'),
          'title'     => Mage::helper('staticblock')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('staticblock/adminhtml_staticblock_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}