<?php

class EM_Staticblock_Block_Adminhtml_Staticblock_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('staticblock_form', array('legend'=>Mage::helper('staticblock')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('staticblock')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('staticblock')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('staticblock')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('staticblock')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('staticblock')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('staticblock')->__('Content'),
          'title'     => Mage::helper('staticblock')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getStaticblockData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getStaticblockData());
          Mage::getSingleton('adminhtml/session')->setStaticblockData(null);
      } elseif ( Mage::registry('staticblock_data') ) {
          $form->setValues(Mage::registry('staticblock_data')->getData());
      }
      return parent::_prepareForm();
  }
}