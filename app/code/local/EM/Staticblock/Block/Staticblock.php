<?php
class EM_Staticblock_Block_Staticblock extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getStaticblock()     
     { 
        if (!$this->hasData('staticblock')) {
            $this->setData('staticblock', Mage::registry('staticblock'));
        }
        return $this->getData('staticblock');
        
    }
}