<?php

class EM_Staticblock_Model_Mysql4_Staticblock extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the staticblock_id refers to the key field in your database table.
        $this->_init('staticblock/staticblock', 'staticblock_id');
    }
}