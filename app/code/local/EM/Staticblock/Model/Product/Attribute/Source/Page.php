<?php 
/** @category   Mage
 * @package    Mage_Catalog
 * @author      Nguyen Anh Dung <Dung.nguyen@codespot.vn>
 */
class EM_Staticblock_Model_Product_Attribute_Source_Page extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('cms/block_collection')
                ->load()
                ->toOptionArray();
            array_unshift($this->_options, array('value'=>'', 'id'=>Mage::helper('catalog')->__('Please select a static block ...')));
        }
        return $this->_options;
    }
}