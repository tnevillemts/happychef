<?php

$installer = $this;
$installer->startSetup();                       
if(!$installer->getAttributeId('catalog_product', 'display_block')) {                           
$installer->addAttribute('catalog_product', 'display_block', array(
			 'group'			 => 'Static Block',
             'label'             => 'Choose Block',
             'type'              => 'varchar',
             'input'             => 'select',
             'backend'           => 'eav/entity_attribute_backend_array',
             'frontend'          => '', 
             'source'            => 'staticblock/product_attribute_source_page',
             'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
             'visible'           => true,
             'required'          => false,
             'user_defined'      => true,
             'searchable'        => false,
             'filterable'        => false,
			 'is_configurable'   => false,
             'visible_on_front'  => false,
             'visible_in_advanced_search' => false,
             'unique'            => false
));
 }
$installer->endSetup();
