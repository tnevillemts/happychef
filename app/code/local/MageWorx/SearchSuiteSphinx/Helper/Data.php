<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuiteSphinx_Helper_Data extends MageWorx_SearchSuite_Helper_Data {

    protected $_instance = null;

    public function getSphinxHost() {
        $host = Mage::getStoreConfig('mageworx_searchsuite/sphinx/host');
        if (empty($host)) {
            $host = '127.0.0.1';
        }
        $port = Mage::getStoreConfig('mageworx_searchsuite/sphinx/port');
        if (empty($port)) {
            $port = 9312;
        }
        return array('host' => $host, 'port' => $port);
    }

    public function getSphinxTimeout() {
        return (int) Mage::getStoreConfig('mageworx_searchsuite/sphinx/timeout');
    }

    public function getInstance() {
        if (!$this->_instance) {
            include_once Mage::getBaseDir('lib') . DS . 'Sphinx' . DS . 'sphinxapi.php';
            $this->_instance = new SphinxClient();
            $host = $this->getSphinxHost();
            $this->_instance->SetServer($host['host'], $host['port']);
            $this->_instance->SetConnectTimeout($this->getSphinxTimeout());
            $this->_instance->SetArrayResult(true);
        }
        return $this->_instance;
    }

}
