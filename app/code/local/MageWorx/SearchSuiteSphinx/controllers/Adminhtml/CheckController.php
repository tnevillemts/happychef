<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuiteSphinx_Adminhtml_CheckController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $instance = Mage::helper('searchsuitesphinx')->getInstance();
        $status = $instance->Status();
        $msg = htmlentities($instance->GetLastError());
        $result = array('status' => (int) $status, 'msg' => $msg);
        $json = json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
        die($json);
    }

}
