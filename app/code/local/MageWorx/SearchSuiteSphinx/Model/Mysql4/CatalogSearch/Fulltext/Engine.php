<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuiteSphinx_Model_Mysql4_CatalogSearch_Fulltext_Engine extends MageWorx_SearchSuite_Model_Mysql4_CatalogSearch_Fulltext_Engine_Abstract {

    public function prepareResultForEngine($fulltextModel, $queryText, $query) {

        $helper = Mage::helper('searchsuitesphinx');
        $instance = $helper->getInstance();
        $multiplier = $helper->getPriorityMultiplier();
	$instance->setFieldWeights(array('data_index1' => intval(ceil(pow($multiplier, 4))), 'data_index2' => intval(ceil(pow($multiplier, 3))), 'data_index3' => intval(ceil(pow($multiplier, 2))), 'data_index4' => intval(ceil($multiplier)), 'data_index5' => intval(1)));
        $instance->SetRankingMode(SPH_RANK_PROXIMITY_BM25);
        $instance->SetSortMode(SPH_SORT_RELEVANCE);
        $instance->SetLimits(0, 1000, 1000);
        $instance->SetFilter('store_id', array($query->getStoreId(), 0));
        $instance->SetMatchMode(SPH_MATCH_EXTENDED);
        $words = $this->filterQueryWords($queryText, $query->getStoreId());
        $queryText = join(' ', $words);
        $result = $instance->Query($queryText, 'catalogsearch_index');
        //var_dump($result, $query->getStoreId());
        if (!$result) {
            return false;
        } else {
            if ($result['total'] > 0) {
                $data = array();
                foreach ($result['matches'] as $row) {
                    $data[] = '(' . $query->getId() . ',' . $row['attrs']['product_id'] . ',' . $row['weight'] . ')';
                }
                $sql = 'INSERT INTO `' . $this->getTable('catalogsearch/result') . '` VALUES ' . implode(',', $data) . ' ON DUPLICATE KEY UPDATE `relevance` = VALUES(`relevance`)';
                $adapter = $this->_getWriteAdapter();
                $adapter->query($sql);
            }
        }
        return true;
    }

    public function rebuildIndexForEngine($fulltextModel, $storeId = null, $productIds = null) {
        if (!is_null($productIds)) {
            $adapter = $this->_getWriteAdapter();
            $select = $this->_getReadAdapter()->select()
                    ->from($fulltextModel->getMainTable(), array('fulltext_id', 'product_id'))
                    ->where('product_id IN (?)', $productIds);
            $sql = 'INSERT INTO `' . $this->getTable('searchsuite/update_index') . '`(fulltext_id,product_id) (' . $select->__toString() . ')';
            $adapter->query($sql);
        }
        return true;
    }

    public function formatResult($matches = array()) {
        $rc = array();
    }

}
