<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */
/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteSphinx
 * @author     MageWorx Dev Team
 */
require_once Mage::getBaseDir('lib') . DS . 'Sphinx' . DS . 'sphinxapi.php';

class MageWorx_SearchSuiteSphinx_Model_System_Config_Source_Ranker {

    public function toOptionArray() {
        return array(
            array('value' => SPH_RANK_PROXIMITY_BM25, 'label' => 'SPH_RANK_PROXIMITY_BM25'),
            array('value' => SPH_RANK_BM25, 'label' => 'SPH_RANK_BM25'),
            array('value' => SPH_RANK_NONE, 'label' => 'SPH_RANK_NONE'),
            array('value' => SPH_RANK_WORDCOUNT, 'label' => 'SPH_RANK_WORDCOUNT'),
            array('value' => SPH_RANK_PROXIMITY, 'label' => 'SPH_RANK_PROXIMITY'),
            array('value' => SPH_RANK_MATCHANY, 'label' => 'SPH_RANK_MATCHANY'),
            array('value' => SPH_RANK_FIELDMASK, 'label' => 'SPH_RANK_FIELDMASK'),
            array('value' => SPH_RANK_SPH04, 'label' => 'SPH_RANK_SPH04'),
        );
    }

}
