<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuiteAutocomplete_Block_Adminhtml_System_Config_Form_Field_Customizer extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $helper = Mage::helper('searchsuiteautocomplete');
        $element->setType('hidden');
        $autocomplete = Mage::getSingleton('core/layout')->createBlock('searchsuiteautocomplete/autocomplete')->setNameInLayout('autocomplete');
        $skinUrl = Mage::getDesign()->getSkinBaseUrl(array('_area' => 'frontend', '_package' => 'default', '_theme' => 'default')) . 'css/mageworx/searchsuiteautocomplete.css';
        $suggestData = Mage::getResourceModel('catalogsearch/query_collection');
        $suggestData->getSelect()->limit(($helper->getSuggestResultsNumber() < 10 && $helper->getSuggestResultsNumber() > 0) ? $helper->getSuggestResultsNumber() : 3);
        $autocomplete->setSuggestData($suggestData);
        $attr = array('name', 'price');
        $fields = $helper->getProductResultFields();
        if (in_array('description', $fields)) {
            $attr[] = 'description';
        }
        if (in_array('short_description', $fields)) {
            $attr[] = 'short_description';
        }
        if (in_array('product_image', $fields)) {
            $attr[] = 'image';
        }
        $products = Mage::getResourceModel('catalog/product_collection');
        $products->addAttributeToSelect($attr);
        $products->setOrder('relevance', 'desc');
        $products->getSelect()->limit(($helper->getProductResultsNumber() > 0 && $helper->getProductResultsNumber() < 10) ? $helper->getProductResultsNumber() : 10);
        $autocomplete->setProducts($products);

        $cmsPage = Mage::getResourceModel('cms/page_collection');
        $cmsPage->addFieldToFilter('identifier', array('nin' => explode(',', Mage::helper('searchsuite')->getFilterCmsPages())));
        $cmsPage->getSelect()->limit(3);
        $autocomplete->setCmsPages($cmsPage);

        $categories = Mage::getResourceModel('catalog/category_collection');
        $categories->addFieldToFilter('path', array('neq' => '1'))
                ->addAttributeToFilter('parent_id', array('neq' => '0'))
                ->addIsActiveFilter();
        $categories->getSelect()->limit(5);
        foreach ($helper->getCategoryFields() as $field) {
            $categories->addAttributeToSelect($field);
        }
        $autocomplete->setCategories($categories);

        Mage::getDesign()->setArea('frontend');
        $html = $autocomplete->toHtml();
        Mage::getDesign()->setArea('adminhtml');

        return $html . Mage::getSingleton('core/layout')->createBlock('searchsuiteautocomplete/adminhtml_js')->toHtml()
                . $element->getElementHtml()
                . '<link rel="stylesheet" type="text/css" href="' . $skinUrl . '" />';
    }

}
