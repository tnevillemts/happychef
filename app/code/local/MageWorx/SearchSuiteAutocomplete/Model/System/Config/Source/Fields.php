<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuiteAutocomplete_Model_System_Config_Source_Fields {

    public function toOptionArray() {
        return array(
            array('value' => 'suggest', 'label' => Mage::helper('searchsuiteautocomplete')->__('Suggested')),
            array('value' => 'product', 'label' => Mage::helper('searchsuiteautocomplete')->__('Products')),
            array('value' => 'category', 'label' => Mage::helper('searchsuiteautocomplete')->__('Categories')),
            array('value' => 'cmspage', 'label' => Mage::helper('searchsuiteautocomplete')->__('CMS Pages')),
        );
    }

}
