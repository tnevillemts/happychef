<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuiteAutocomplete_Model_System_Config_Source_Product_Fields {

    public function toOptionArray() {
        return array(
            array('value' => 'product_name', 'label' => Mage::helper('searchsuiteautocomplete')->__('Product Name')),
            array('value' => 'sku', 'label' => Mage::helper('searchsuiteautocomplete')->__('SKU')),
            array('value' => 'product_image', 'label' => Mage::helper('searchsuiteautocomplete')->__('Product Image')),
            array('value' => 'reviews_rating', 'label' => Mage::helper('searchsuiteautocomplete')->__('Reviews Rating')),
            array('value' => 'short_description', 'label' => Mage::helper('searchsuiteautocomplete')->__('Short Description')),
            array('value' => 'description', 'label' => Mage::helper('searchsuiteautocomplete')->__('Description')),
            array('value' => 'price', 'label' => Mage::helper('searchsuiteautocomplete')->__('Price')),
            array('value' => 'add_to_cart_button', 'label' => Mage::helper('searchsuiteautocomplete')->__('Add to Cart Button')),
        );
    }

}
