<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuiteAutocomplete
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuiteAutocomplete_Model_Observer {

    public function controllerActionLayoutRenderBefore($observer) {
        $helper = Mage::helper('searchsuiteautocomplete');
        if ($helper->showPopup()) {
            $ssHelper = Mage::helper('searchsuite');
            $ssHelper->addFooterJs('mageworx/searchsuiteautocomplete.js');
            $animation = $helper->getAnimation();
            if ($animation == 'nprogress') {
                $block = Mage::app()->getLayout()->getBlock('head');
                if ($block) {
                    $ssHelper->addFooterJs('mageworx/NProgress/nprogress.js');
                    $block->addCss('css/mageworx/NProgress/nprogress.css');
                }
            }
        }
    }

    public function controllerActionLayoutRenderBeforeAdminhtmlSystemConfigEdit($observer) {
        if (Mage::app()->getRequest()->getParam('section') == 'mageworx_searchsuite') {
            $block = Mage::app()->getLayout()->getBlock('head');
            if ($block) {
                $block->addJs('mageworx/jquery/jquery.min.js');
                $block->addJs('mageworx/jquery/noconflict.js');
                $block->addItem('skin_js', 'mageworx/tinycolor-0.9.15.min.js');
                $block->addItem('skin_js', 'mageworx/pick-a-color-1.2.2.min.js');
                $block->addCss('mageworx/pick-a-color-1.2.0.min.css');
                $block->addCss('mageworx/customizer.css');
                $block->addJs('mageworx/adminhtml/customizer.js');
            }
        }
    }

}
