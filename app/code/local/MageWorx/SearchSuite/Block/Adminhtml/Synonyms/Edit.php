<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Block_Adminhtml_Synonyms_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_synonyms';
        $this->_blockGroup = 'searchsuite';
        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('searchsuite')->__('Save Synonyms'));
        $this->_updateButton('delete', 'label', Mage::helper('searchsuite')->__('Delete Synonyms'));
    }

    public function getHeaderText() {
        if (Mage::registry('current_catalog_search')->getId()) {
            return Mage::helper('searchsuite')->__("Edit Synonyms For '%s'", $this->htmlEscape(Mage::registry('current_catalog_search')->getQueryText()));
        } else {
            return Mage::helper('searchsuite')->__('New Synonyms');
        }
    }

}
