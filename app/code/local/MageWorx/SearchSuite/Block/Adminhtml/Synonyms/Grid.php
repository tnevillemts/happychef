<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Block_Adminhtml_Synonyms_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function _construct() {
        $this->setId('synonymsGrid');
        parent::_construct();
    }

    protected function _prepareColumns() {
        parent::_prepareColumns();

        $this->addColumn('query_text', array(
            'header' => Mage::helper('catalogsearch')->__('Query Text'),
            'sortable' => true,
            'index' => 'query_text',
            'width' => '200px'
        ));

        $this->addColumn('query_text', array(
            'header' => Mage::helper('catalogsearch')->__('Query Text'),
            'sortable' => true,
            'index' => 'query_text',
            'width' => '200px'
        ));
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => Mage::helper('catalog')->__('Store'),
                'index' => 'store_id',
                'type' => 'store',
                'store_view' => true,
                'sortable' => false,
                'width' => '200px'
            ));
        }
        $this->addColumn('synonyms', array(
            'header' => Mage::helper('searchsuite')->__('Synonyms'),
            'index' => 'synonyms',
        ));

        return $this;
    }

    protected function _prepareCollection() {
    	$collection = Mage::getResourceModel('searchsuite/synonym_collection')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getRowUrl($row) {
        return $this->getUrl('searchsuite/adminhtml_synonyms/edit', array(
                    'id' => $row->getId()
        ));
    }
    
    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('synonym_ids');
        $this->getMassactionBlock()->addItem('delete_synonym', array(
            'label' => Mage::helper('adminhtml')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('searchsuite')->__('Are you sure?')
        ));
    }

}
