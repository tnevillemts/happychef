<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Block_Adminhtml_Attributes_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function _construct() {
        $this->setId('searchAttributes');
        parent::_construct();
    }

    protected function _prepareColumns() {
        parent::_prepareColumns();

        $this->addColumn('attribute_code', array(
            'header' => Mage::helper('eav')->__('Attribute Code'),
            'sortable' => true,
            'index' => 'attribute_code'
        ));

        $this->addColumn('frontend_label', array(
            'header' => Mage::helper('eav')->__('Attribute Label'),
            'sortable' => true,
            'index' => 'frontend_label'
        ));

        $priority = Mage::getModel('searchsuite/search_priority')->toArray();
        foreach ($priority as $key => $item) {
            $this->addColumn('quick_search_priority_' . $key, array(
                'header' => $item . ' ' . Mage::helper('searchsuite')->__('Priority'),
                'sortable' => true,
                'index' => 'quick_search_priority',
                'type' => 'options',
                'renderer' => 'searchsuite/adminhtml_attributes_grid_renderer_priority',
                'options' => $priority,
                'filter' => false,
                'width' => '50px',
                'align' => 'center',
            ));
        }

        $this->addColumn('quick_search_priority_0', array(
            'header' => Mage::helper('searchsuite')->__('Do not use'),
            'sortable' => true,
            'index' => 'is_searchable',
            'type' => 'options',
            'renderer' => 'searchsuite/adminhtml_attributes_grid_renderer_priority',
            'options' => $priority,
            'filter' => false,
            'width' => '20px',
            'align' => 'center',
        ));

        $this->addColumn('is_attributes_search', array(
            'header' => Mage::helper('searchsuite')->__('Search by Attributes'),
            'sortable' => true,
            'index' => 'is_attributes_search',
            'type' => 'checkbox',
            'renderer' => 'searchsuite/adminhtml_attributes_grid_renderer_search',
            'align' => 'center',
            'width' => '20px',
            'filter' => false,
        ));

        return $this;
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('catalog/product_attribute_collection')
                ->addVisibleFilter();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

}
