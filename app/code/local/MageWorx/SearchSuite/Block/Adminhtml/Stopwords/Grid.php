<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Block_Adminhtml_Stopwords_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function _construct() {
        $this->setId('stopwordsGrid');
        parent::_construct();
    }

    protected function _prepareColumns() {
        parent::_prepareColumns();

        $this->addColumn('id', array(
            'header' => Mage::helper('catalog')->__('ID'),
            'width' => '50px',
            'index' => 'id',
        ));

        $this->addColumn('stopword', array(
            'header' => Mage::helper('searchsuite')->__('Stopword'),
            'sortable' => true,
            'index' => 'word',
        ));
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => Mage::helper('catalog')->__('Store'),
                'index' => 'store_id',
                'type' => 'store',
                'store_view' => true,
                'sortable' => false,
                'width' => '200px'
            ));
        }

        return $this;
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('searchsuite/stopword_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getRowUrl($row) {
        return $this->getUrl('searchsuite/adminhtml_stopwords/edit', array(
                    'id' => $row->getId()
        ));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('stopword_ids');
        $this->getMassactionBlock()->addItem('delete_stopword', array(
            'label' => Mage::helper('adminhtml')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('searchsuite')->__('Are you sure?')
        ));
    }

}
