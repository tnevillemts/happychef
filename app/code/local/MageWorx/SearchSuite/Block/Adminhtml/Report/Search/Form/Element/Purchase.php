<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Block_Adminhtml_Report_Search_Form_Element_Purchase extends Varien_Data_Form_Element_Abstract {

    /**
     * Retrieve search query model instance
     * @return Mage_CatalogSearch_Model_Query
     */
    public function getSearchQuery() {
        return Mage::registry('current_catalog_search');
    }

    /**
     * Get tracking collection for curent query
     * @return MageWorx_SearchSuite_Model_Mysql4_Tracking_Purchase_Collection
     */
    public function getCollection() {
        $collection = Mage::getModel('searchsuite/tracking_purchase')->getCollection()
                ->setQueryFilter($this->getSearchQuery())
                ->addOrderToSelect();
        return $collection;
    }

    public function getElementHtml() {
        $collection = $this->getCollection();
        $html = array();
        if ($collection->count() > 0) {
            foreach ($collection as $item) {
                if ($item->getIncrementId()) {
                    $url = Mage::helper('adminhtml')->getUrl("*/sales_order/view", array('order_id' => $item->getOrderId()));
                    $html[] = '<a href="' . $url . '">' . $item->getIncrementId() . '</a>';
                }
            }
        } else {
            $html[] = '<span>' . Mage::helper('searchsuite')->__('No orders yet') . '</span>';
        }

        return implode('<br/>' . PHP_EOL, $html);
    }

}
