<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */
/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
include_once ('Mage/CatalogSearch/controllers/ResultController.php');

class MageWorx_SearchSuite_ResultController extends Mage_CatalogSearch_ResultController {

    public function indexAction() {
        $helper = Mage::helper('searchsuite');
        $attr = null;
        $cat = null;

        if ($helper->isSearchByAttributes() && $helper->getSearchParameter()) {
            $attr = $helper->getSearchParameter();
        }
        if ($helper->isSearchByCategories() && $helper->getSearchCategory()) {
            $cat = $helper->getSearchCategory();
        }
        if (!$attr && !$cat) {
            //parent::indexAction();
            $this->_indexAction();

        } else {
            $query = Mage::helper('catalogsearch')->getQuery();
            $query->setStoreId(Mage::app()->getStore()->getId());
           
            if ($query->getQueryText() != '') {
                if (!Mage::helper('catalogsearch')->isMinQueryLength()) {
                    $key = '_singleton/catalogsearch/layer';
                    Mage::register($key, Mage::getModel('searchsuite/layer'), true);
                } else {
                    $query->setId(0)
                            ->setIsActive(1)
                            ->setIsProcessed(1);
                }
                $this->loadLayout();
                $this->_initLayoutMessages('catalog/session');
                $this->_initLayoutMessages('checkout/session');
                $this->renderLayout();
            } else {
                $this->_redirectReferer();
            }
        }
    }

    public function _indexAction()
    {

        $queryText = Mage::helper('catalogsearch')->getQueryText();

        if ($queryText && !empty($queryText)) {

            $query = Mage::helper('catalogsearch')->_getQuery();
            $query->setStoreId(Mage::app()->getStore()->getId());
            $query->prepare();
            $query->save();
        }

        $collection = Mage::getSingleton('catalogsearch/layer')->getProductCollection();
        if ($collection->getSize() == 0) {
            $query2 = Mage::helper('catalogsearch')->_getQuery(true);
            $query2->setStoreId(Mage::app()->getStore()->getId());
            $query = $query2;
        }

        if ($query && $query->getQueryText() != '') {

            if (Mage::helper('catalogsearch')->isMinQueryLength()) {

                $query->setId(0)
                    ->setIsActive(1)
                    ->setIsProcessed(1);
            } else {

                if ($query->getId()) {
                    $query->setPopularity($query->getPopularity()+1);
                } else {
                    $query->setPopularity(1);
                }

                if ($query->getRedirect()){
                    $query->save();
                    $this->getResponse()->setRedirect($query->getRedirect());
                    return;
                }
                else {
                    $query->prepare();
                }
            }

            $this->loadLayout();
            $this->_initLayoutMessages('catalog/session');
            $this->_initLayoutMessages('checkout/session');
            $this->renderLayout();

            if (!Mage::helper('catalogsearch')->isMinQueryLength()) {
                $query->save();
            }
        }
        else {
            $this->_redirectReferer();
        }

    }
}
