<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Adminhtml_StopwordsController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('MageWorx_SearchSuite');
    }

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('catalog/mageworx_searchsuite')
                ->_addBreadcrumb(Mage::helper('searchsuite')->__('Search Suite'), Mage::helper('searchsuite')->__('Search Suite'));
        return $this;
    }

    public function indexAction() {

        $this->_title(Mage::helper('searchsuite')->__('Search Suite'))->_title(Mage::helper('searchsuite')->__('Manage Stopwords'));
        $this->loadLayout()
                ->_setActiveMenu('catalog')
                ->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_title($this->__('Catalog'))->_title(Mage::helper('searchsuite')->__('Stopword'));
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('searchsuite/stopword');
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('searchsuite')->__('This stopword no longer exists.'));
                $this->_redirect('*/*');
                return;
            }
        }
        $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        Mage::register('current_stopword', $model);
        $this->_initAction();
        $this->_title($id ? $model->getWord() : Mage::helper('searchsuite')->__('New Stopword'));
        $this->getLayout()->getBlock('head')->setCanLoadRulesJs(true);
        $this->getLayout()->getBlock('stopwords_edit')->setData('action', $this->getUrl('*/*/save'));
        $this->_addBreadcrumb($id ? Mage::helper('catalog')->__('Edit Stopword') : Mage::helper('catalog')->__('New Stopword'), $id ? Mage::helper('catalog')->__('Edit Stopword') : Mage::helper('catalog')->__('New Stopword'));
        $this->renderLayout();
    }

    public function saveAction() {
        $hasError = false;
        $data = $this->getRequest()->getPost();
        $id = $this->getRequest()->getPost('id', null);
        if ($this->getRequest()->isPost() && $data) {
            $model = Mage::getModel('searchsuite/stopword');
            $stopword = $this->getRequest()->getPost('word', false);
            $storeId = $this->getRequest()->getPost('store_id', false);
            try {
                if ($queryText) {
                    $model->setStoreId($storeId);
                    $model->loadByWord($stopword);
                    if ($model->getId() && $model->getId() != $id) {
                        Mage::throwException(
                                Mage::helper('searchsuite')->__('Stopword with such word already exists.')
                        );
                    } else if (!$model->getId() && $id) {
                        $model->load($id);
                    }
                } else if ($id) {
                    $model->load($id);
                }
                $model->addData($data);
                $model->save();
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $hasError = true;
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('searchsuite')->__('An error occurred while saving the stopword.')
                );
                $hasError = true;
            }
        }

        if ($hasError) {
            $this->_getSession()->setPageData($data);
            $this->_redirect('*/*/edit', array('id' => $id));
        } else {
            $this->_getSession()->addSuccess(Mage::helper('searchsuite')->__('Stopword successfully saved'));
            $this->_redirect('*/*');
        }
    }

    public function deleteAction() {
        $hasError = false;
        $stopwordId = $this->getRequest()->getParam('id', null);
        if ($stopwordId) {
            $model = Mage::getModel('searchsuite/stopword')->load($stopwordId);
            if ($model->getId() == $stopwordId) {
                try {
                    $model->delete();
                } catch (Exception $ex) {
                    $this->_getSession()->addException($ex, Mage::helper('searchsuite')->__('An error occurred deleting stopword.'));
                    $hasError = true;
                }
            }
        }
        if ($hasError) {
            $this->_redirect('*/*/edit', array('id' => $stopwordId));
        } else {
            $this->_getSession()->addSuccess(Mage::helper('searchsuite')->__('Stopword successfully deleted'));
            $this->_redirect('*/*');
        }
    }

    public function massDeleteAction() {
        $ids = $this->getRequest()->getPost('stopword_ids', array());
        if (count($ids)) {
            $collection = Mage::getResourceModel('searchsuite/stopword_collection');
            $collection->addFieldToFilter('id', array('in' => $ids));
            try {
                $collection->delete($ids);
                $this->_getSession()->addSuccess(Mage::helper('searchsuite')->__('Stopword successfully deleted'));
            } catch (Exception $ex) {
                $this->_getSession()->addException($ex, Mage::helper('searchsuite')->__('An error occurred while deleting the stopwords.')
                );
            }
        }
        $this->_redirect('*/*');
    }

    public function importAction() {
        $this->_title($this->__('Catalog'))->_title(Mage::helper('searchsuite')->__('Stopword'));
        $this->_initAction();
        $this->_title(Mage::helper('searchsuite')->__('Import Stopwords'));
        $this->getLayout()->getBlock('head')->setCanLoadRulesJs(true);
        $this->getLayout()->getBlock('stopwords_import')->setData('action', $this->getUrl('*/*/upload'));
        $this->_addBreadcrumb(Mage::helper('catalog')->__('Import Stopwords'));
        $maxUploadSize = Mage::helper('importexport')->getMaxUploadSize();
        $this->_getSession()->addNotice(
                Mage::helper('importexport')->__('Total size of uploadable files must not exceed %s', $maxUploadSize)
        );
        $this->renderLayout();
    }

    public function uploadAction() {
        $info = $_FILES['file'];
        $hasError = false;
        $storeId = $this->getRequest()->getPost('store_id', null);
        if ($info && $info['error'] == 0 && $info['size'] > 0 && $storeId) {
            try {
                $h = fopen($info['tmp_name'], 'r');
                $content = fread($h, $info['size']);
                fclose($h);
                $words = explode("\n", $content);
                $model = Mage::getModel('searchsuite/stopword');
                $model->import($storeId, $words);
            } catch (Exception $ex) {
                $this->_getSession()->addException($ex, Mage::helper('searchsuite')->__('An error occurred while importing the stopwords.')
                );
            }
        } else {
            $this->_getSession()->addError(Mage::helper('searchsuite')->__('The file is empty or has the wrong format.'));
        }
        if ($hasError) {
            $this->_redirect('*/*/import');
        } else {
            $this->_getSession()->addSuccess(Mage::helper('searchsuite')->__('Stopwords successfully imported'));
            $this->_redirect('*/*/');
        }
    }

}
