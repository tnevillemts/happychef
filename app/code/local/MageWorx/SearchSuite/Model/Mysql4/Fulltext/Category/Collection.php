<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Mysql4_Fulltext_Category_Collection extends Mage_Catalog_Model_Resource_Category_Collection {

    public function addSearchFilter(Mage_CatalogSearch_Model_Query $query) {
        Mage::getSingleton('searchsuite/fulltext_category')->prepareResult($query);

        $this->getSelect()->joinInner(
                array('search_result' => $this->getTable('searchsuite/category_result')), $this->getConnection()->quoteInto(
                        'search_result.category_id=entity_id AND search_result.query_id = ?', $query->getId()
                ), array('relevance' => 'relevance')
        );
        return $this;
    }

}
