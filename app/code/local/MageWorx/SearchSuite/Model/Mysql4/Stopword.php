<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Mysql4_Stopword extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('searchsuite/stopwords', 'id');
    }

    public function loadByWord($object, $word) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where('word=?', $word)
                ->where('store_id=?', $object->getStoreId())
                ->limit(1);
        if ($data = $this->_getReadAdapter()->fetchRow($select)) {
            $object->setData($data);
            $this->_afterLoad($object);
        }

        return $this;
    }

    public function import($object, $storeId, array $words) {
        $data = array();
        foreach ($words as $word) {
            $word = trim($word);
            if (strlen($word)) {
                $data[] = array('store_id' => $storeId, 'word' => trim($word));
            }
        }
        $this->_getWriteAdapter()->insertOnDuplicate($this->getMainTable(), $data, array('word'));
        return $this;
    }

}
