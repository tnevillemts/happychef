<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Mysql4_Tracking_Region extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init('searchsuite/region_tracking', 'id');
    }

    protected function _checkUnique(Mage_Core_Model_Abstract $object) {
        $uses = $object->getNumUses();
        if (!$uses) {
            $object->setNumUses(1);
        }
        if ($object->getQueryId() && $object->getCountry()) {
            $select = $this->_getWriteAdapter()->select()
                            ->from($this->getMainTable())->where('query_id=?', $object->getQueryId())->where('country=?', $object->getCountry());
            $test = $this->_getWriteAdapter()->fetchRow($select);

            if ($test) {
                $object->setNumUses($uses++);
                $object->setId($test['id']);
                $object->setNumUses($test['num_uses'] + 1);
            }
        }
        return $this;
    }

}
