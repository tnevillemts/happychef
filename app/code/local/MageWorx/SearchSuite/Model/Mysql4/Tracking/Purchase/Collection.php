<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Mysql4_Tracking_Purchase_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('searchsuite/tracking_purchase');
    }

    public function setOrderFilter($order) {
        $orderId = 0;
        if (is_object($order)) {
            $orderId = $order->getId();
        } else {
            $orderId = $order;
        }

        $this->getSelect()->where('order_id = ?', $orderId);
        return $this;
    }

    public function setQueryFilter($query) {
        $queryId = 0;
        if (is_object($query)) {
            $queryId = $query->getId();
        } else {
            $queryId = $query;
        }

        $this->getSelect()->where('query_id = ?', $queryId);
        return $this;
    }

    public function addQueryToSelect() {
        $this->getSelect()
                ->joinLeft(array('search_query' => $this->getTable('catalogsearch/search_query')), 'main_table.query_id = search_query.query_id', 'query_text');
        return $this;
    }

    public function addOrderToSelect() {
        $this->getSelect()
                ->joinLeft(array('orders' => $this->getTable('sales/order_grid')), 'main_table.order_id = orders.entity_id', 'increment_id');
        return $this;
    }

}
