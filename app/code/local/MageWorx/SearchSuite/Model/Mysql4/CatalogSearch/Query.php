<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Mysql4_CatalogSearch_Query extends Mage_CatalogSearch_Model_Mysql4_Query {

    public function loadByQuery(Mage_Core_Model_Abstract $object, $value) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where('query_text=?', $value) // without synonym_for
                ->where('store_id=?', $object->getStoreId())
                ->order('synonym_for ASC')
                ->limit(1);
        if ($data = $this->_getReadAdapter()->fetchRow($select)) {
            $object->setData($data);
            $this->_afterLoad($object);
        }
        return $this;
    }

    public function clearSynonymsFor($terms) {
        $adapter = $this->_getWriteAdapter();
        $adapter->update($this->getMainTable(), array('synonym_for' => ''), array('query_text in (?)' => $terms));
        return $this;
    }

    public function getSynonymsByQueryText($storeId, $value) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where('synonym_for=?', $value)
                ->where('store_id=?', $storeId)
                ->order('synonym_for ASC');

        return $this->_getReadAdapter()->fetchAll($select);
    }

    public function getSynonyms($storeId, $queries) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where('synonym_for IN(?)', $queries)
                ->where('store_id=?', $storeId)
                ->order('synonym_for ASC');
        return $this->_getReadAdapter()->fetchAll($select);
    }

    public function getSynonymsForQueries($queries) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where('synonym_for IN(?)', $queries)
                ->order('synonym_for ASC');
        return $this->_getReadAdapter()->fetchAll($select);
    }

    public function getRelatedQueries($storeId, $query) {
        $equery = addslashes($query);
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where("query_text REGEXP '[[:<:]]{$equery}[[:>:]]'")
                ->where('query_text!=?', $query)
                ->where('store_id=?', $storeId)
                ->order('query_text ASC')
                ->limit(3);
        return $this->_getReadAdapter()->fetchAll($select);
    }

}
