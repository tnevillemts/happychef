<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Mysql4_CatalogSearch_Fulltext_Engine_Abstract extends Mage_CatalogSearch_Model_Mysql4_Fulltext_Engine {

    public function saveEntityIndexes($storeId, $entityIndexes, $entity = 'product') {
        $searchHelper = Mage::helper('searchsuite');
        $data = array();
        $storeId = (int) $storeId;
        foreach ($entityIndexes as $entityId => $index) {
            $data[] = array(
                'product_id' => (int) $entityId,
                'store_id' => $storeId,
                'data_index' => $index[0],
                'data_index1' => $searchHelper->prepareValue($index[1]),
                'data_index2' => $searchHelper->prepareValue($index[2]),
                'data_index3' => $searchHelper->prepareValue($index[3]),
                'data_index4' => $searchHelper->prepareValue($index[4]),
                'data_index5' => $searchHelper->prepareValue($index[5])
            );
        }
        if ($data) {
            $this->insertOnDuplicate_compatible($this->getMainTable(), $data, array('data_index', 'data_index1', 'data_index2', 'data_index3', 'data_index4', 'data_index5'));
        }
        return $this;
    }

    // for magento < 1.7.0.0 from CatalogSearch Fulltext Index Engine resource model
    public function getAllowedVisibility_compatible() {
        return Mage::getSingleton('catalog/product_visibility')->getVisibleInSearchIds();
    }

    // for magento < 1.7.0.0 from CatalogSearch Mysql resource helper model
    public function insertOnDuplicate_compatible($table, array $data, array $fields = array()) {
        return $this->_getWriteAdapter()->insertOnDuplicate($table, $data, $fields);
    }

    // search
    public function prepareResultForEngine($fulltextModel, $queryText, $query) {
        return false;
    }

    public function rebuildIndexForEngine($fulltextModel, $storeId = null, $productIds = null) {
        return false;
    }

    public function filterQueryWords($words, $storeId) {

        if (is_string($words)) {
            $words = Mage::helper('core/string')->splitWords(Mage::helper('searchsuite')->prepareValue($words), true, Mage::getStoreConfig(Mage_CatalogSearch_Model_Query::XML_PATH_MAX_QUERY_WORDS, $storeId));
        }
        if (count($words) > 0) {
            $stopwordCollection = Mage::getResourceModel('searchsuite/stopword_collection');
            $stopwordCollection->addWordFilter($words, $storeId);
            if ($stopwordCollection->count()) {
                foreach ($stopwordCollection as $item) {
                    if (isset($words[$item->getWord()])) {
                        unset($words[$item->getWord()]);
                    }
                }
            }
        }
        return $words;
    }

}
