<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Mysql4_Setup extends Mage_Eav_Model_Entity_Setup {

    public function rebuildIndex() {
        // for can reindex
        Mage::app()->reinitStores();
        Mage::app()->getStore(null)->resetConfig();
        // reindex
        Mage::getModel('catalogsearch/fulltext')->rebuildIndex();
    }

    public function updateAttributes() {
        $priority = array(
            'name' => 1,
            'sku' => 3,
            'manufacturer' => 3,
            'short_description' => 2,
            'description' => 4,
        );
        $search = array(
            'name',
            'sku',
            'description',
            'manufacturer'
        );
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $tablePrefix = (string) Mage::getConfig()->getTablePrefix();
        $productAttributeCollection = Mage::getResourceModel('catalog/product_attribute_collection');
        foreach ($productAttributeCollection as $attribute) {
            $update = array();
            if (isset($priority[$attribute->getAttributeCode()])) {
                $update['quick_search_priority'] = $priority[$attribute->getAttributeCode()];
            }
            if (in_array($attribute->getAttributeCode(), $search)) {
                $update['is_attributes_search'] = '1';
            }
            if (count($update)) {
                $connection->update($tablePrefix . 'catalog_eav_attribute', $update, 'attribute_id = ' . $attribute->getId());
            }
        }
    }

}
