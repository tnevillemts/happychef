<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Indexer_Blog extends MageWorx_SearchSuite_Model_Indexer_Abstract {

    const EVENT_MATCH_RESULT_KEY = 'searchsuite_blog_match_result';

    public function getName() {
        return Mage::helper('searchsuite')->__('Blog Search Index');
    }

    public function getDescription() {
        return Mage::helper('searchsuite')->__('Rebuild blog fulltext search index');
    }

    protected function _getResource() {
        return Mage::getResourceSingleton('searchsuite/indexer_blog');
    }

    protected function _getIndexer() {
        return Mage::getSingleton('searchsuite/fulltext_blog');
    }

    public function matchEvent(Mage_Index_Model_Event $event) {
        // not supported!
        return false;

        if (!$this->isVisible()) {
            return false;
        }
        $data = $event->getNewData();
        if (isset($data[self::EVENT_MATCH_RESULT_KEY])) {
            return $data[self::EVENT_MATCH_RESULT_KEY];
        }

        $entity = $event->getEntity();
        if ($entity == 'blog_search') {
            $result = $event->getDataObject()->hasDataChanges() || ($event->getType() == Mage_Index_Model_Event::TYPE_DELETE);
        } else {
            $result = parent::matchEvent($event);
        }

        $event->addNewData(self::EVENT_MATCH_RESULT_KEY, $result);
        return $result;
    }

    protected function _registerEvent(Mage_Index_Model_Event $event) {
        $event->addNewData(self::EVENT_MATCH_RESULT_KEY, true);
        $entity = $event->getEntity();

        if ($entity == 'blog_search') {
            $eventType = $event->getType();
            if ($eventType == Mage_Index_Model_Event::TYPE_SAVE) {
                $page = $event->getObject();
                $event->addNewData('blog_index_update_id', $page->getId());
            } else if ($eventType == Mage_Index_Model_Event::TYPE_DELETE) {
                $page = $event->getObject();
                $event->addNewData('blog_index_delete_id', $page->getId());
            }
        }
        return $this;
    }

    protected function _processEvent(Mage_Index_Model_Event $event) {
        $data = $event->getNewData();
        if ($data['blog_index_update_id']) {
            $this->reindex(null, $data['blog_index_update_id']);
        }
        if ($data['blog_index_delete_id']) {
            $this->remove(null, $data['blog_index_delete_id']);
        }
    }

}
