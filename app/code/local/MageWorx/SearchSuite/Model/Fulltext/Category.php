<?php

/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */

/**
 * Search Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SearchSuite
 * @author     MageWorx Dev Team
 */
class MageWorx_SearchSuite_Model_Fulltext_Category extends MageWorx_SearchSuite_Model_Fulltext_Abstract {

    protected function _construct() {
        $this->_init('searchsuite/fulltext_category');
    }

    public function rebuildIndex($storeId = null, $pageIds = null) {

        Mage::dispatchEvent('searchsuite_category_index_process_start', array(
            'store_id' => $storeId,
            'category_ids' => $pageIds
        ));
        $this->getResource()->rebuildIndex($storeId, $pageIds);
        Mage::dispatchEvent('searchsuite_category_index_process_complete', array());
        return $this;
    }

}
