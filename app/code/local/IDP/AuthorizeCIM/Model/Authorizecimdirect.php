<?php
/**
Copyright 2009-2014 Eric Levine
IDP
 */
class IDP_AuthorizeCIM_Model_Authorizecimdirect extends Mage_Payment_Model_Method_Cc
{   protected $_code  = 'authorizecimsoap';
    protected $_isGateway               = true;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = true;
    protected $_canRefund               = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid                 = true;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;
    protected $_canSaveCc               = false;
    const CC_URL_LIVE = 'https://api.authorize.net/xml/v1/request.api';
	const CC_URL_TEST = 'https://apitest.authorize.net/xml/v1/request.api';
    const STATUS_APPROVED = 'Approved';
	const STATUS_SUCCESS = 'Complete';
	const PAYMENT_ACTION_AUTH_CAPTURE = 'authorize_capture';
	const PAYMENT_ACTION_AUTH = 'authorize';
    const STATUS_COMPLETED    = 'Completed';
    const STATUS_DENIED       = 'Denied';
    const STATUS_FAILED       = 'Failed';
    const STATUS_REFUNDED     = 'Refunded';
    const STATUS_VOIDED       = 'Voided';
	public function getGatewayUrl() {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url_test');
		}
		else
		{
			return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url');
		}
	}
	public function getUsername($storeId=0) {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			//return 'xxxxxxxxx';   //hard code test ID username here
			return Mage::getStoreConfig('payment/authorizecimsoap/testusername',$storeId);
		}
		else
		{
			//return 'xxxxxxxxx';   //hard code live ID username here
			return Mage::getStoreConfig('payment/authorizecimsoap/username',$storeId);
		}
	}
	public function getPassword($storeId=0) {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			//return 'xxxxxxxxxxxxxxxxxxx';  //hard code test ID key/password herekey/password here
			return Mage::getStoreConfig('payment/authorizecimsoap/testpassword',$storeId);
		}
		else
		{
			//return 'xxxxxxxxxxxxxxxxxxx';  //hard code live ID key/password here
			return Mage::getStoreConfig('payment/authorizecimsoap/password',$storeId);
		}
	}
	public function getDebug1() {
		return true;
	}
	public function getDebug() {
			if((Mage::getStoreConfig('payment/authorizecimsoap/test')) & (file_exists(Mage::getBaseDir() . '/var/log'))) {
				return Mage::getStoreConfig('payment/authorizecimsoap/debug'); 
			} else {
				return false;
			}
	}
	public function getTest() {
		return Mage::getStoreConfig('payment/authorizecimsoap/test');
	}
	public function getLogPath() {
		return Mage::getBaseDir() . '/var/log/authorizecimsoap.log';
	}
	public function getLiveUrl() {
		return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url');
	}
	public function getTestUrl() {
		return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url_test');
	}
	public function getPaymentAction()
	{
		return Mage::getStoreConfig('payment/authorizecimsoap/payment_action');
	}
	public function getStrictCVV() {
		return true;
	}
	public function createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $custID, $billToWho, $cvv, $storeId=0) {
		$doc = new SimpleXMLElement('<?xml version ="1.0" encoding = "utf-8"?><createCustomerProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('profile');
		$profile->addChild('merchantCustomerId', $custID);
		$profile->addChild('email', $CustomerEmail);
		$PaymentProfile = $profile->addChild('paymentProfiles');
		$billTo = $PaymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        } else { $billTo->addChild('company',"xxxx"); }
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$Payment1 = $PaymentProfile->addChild('payment');
		$cc = $Payment1->addChild('creditCard');
		$cc->addChild('cardNumber', htmlentities($ccnum));
		$cc->addChild('expirationDate', $ExpirationDate);
		if($cvv) {$cc->addChild('cardCode', $cvv); }
		$CustProfileXML = $doc->asXML();
		return $CustProfileXML;
	}
	public function addPaymentProfileXML($CustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerPaymentProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$paymentProfile = $doc->addChild('paymentProfile');
		$billTo = $paymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        } else { $billTo->addChild('company',"xxxx"); }
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment = $paymentProfile->addChild('payment');
		$credit = $payment->addChild('creditCard');
		$credit->addChild('cardNumber', $ccnum);
		$credit->addChild('expirationDate', $ExpirationDate);
		if($cvv) {$credit->addChild('cardCode', $cvv); }
		$CustPaymentXML = $doc->asXML();
		return $CustPaymentXML;
	}
	public function updatePaymentProfileXML($CustProfile, $PayProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><updateCustomerPaymentProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$paymentProfile = $doc->addChild('paymentProfile');
		$billTo = $paymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        } else { $billTo->addChild('company',"xxxx"); }
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment = $paymentProfile->addChild('payment');
		$credit = $payment->addChild('creditCard');
		$credit->addChild('cardNumber', $ccnum);
		$credit->addChild('expirationDate', $ExpirationDate);
		if($cvv) {$credit->addChild('cardCode', $cvv); }
		$pay2 = $paymentProfile->addChild('customerPaymentProfileId', $PayProfile);
		$CustPaymentXML = $doc->asXML();
		return $CustPaymentXML;
	}
	public function getProfileXML($CustProfile, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><getCustomerProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$getCustProfileXML = $doc->asXML();
		
		return $getCustProfileXML;
	}
	public function createTransXML($amount, $tax, $CustomerProfileID, $PaymentProfileID, $callby, $invoiceno, $authtransID, $Approval, $storeId=0) {
		// Build Transaction Request
		$TxRq = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerProfileTransactionRequest/>');
		$TxRq->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $TxRq->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$transaction = $TxRq->addChild('transaction');
		if($this->getPaymentAction()=='authorize')
		{
			switch($callby) {
				case 'captureonly':
					$credit = $transaction->addChild('profileTransCaptureOnly');
					$credit->addChild('amount', $amount);
					break;
				case 'capture':
					$credit = $transaction->addChild('profileTransPriorAuthCapture');
					$credit->addChild('amount', $amount);
					break;
				case 'refund':
					$credit = $transaction->addChild('profileTransRefund');
					$credit->addChild('amount', $amount);
					break;
				case 'void':
					$credit = $transaction->addChild('profileTransVoid');
					break;
				default:
					$credit = $transaction->addChild('profileTransAuthOnly');
					$credit->addChild('amount', $amount);
					if($tax>0) {
						$credittax = $credit->addChild('tax');
						$credittax->addChild('amount', $tax);
					}
					break;
			}
		} else {
			switch($callby) {
				case 'refund':
					$credit = $transaction->addChild('profileTransRefund');
					$credit->addChild('amount', $amount);
					break;
				case 'void':
					$credit = $transaction->addChild('profileTransVoid');
					break;
				default:
					$credit = $transaction->addChild('profileTransAuthCapture');
					$credit->addChild('amount', $amount);
					if($tax>0) {
						$credittax = $credit->addChild('tax');
						$credittax->addChild('amount', $tax);
					}
					break;
			}
		}
		$credit->addChild('customerProfileId', $CustomerProfileID);
		$credit->addChild('customerPaymentProfileId', $PaymentProfileID);
		if($this->getPaymentAction()=='authorize')
		{
			switch($callby) {
				case 'captureonly':
					$credit->addChild('approvalCode', $Approval);
					break;
				case 'capture':
					$credit->addChild('transId', $authtransID);
					break;
				case 'refund':
					$credit->addChild('transId', $authtransID);
					break;
				case 'void':
					$credit->addChild('transId', $authtransID);
					break;
				default:
					$order = $credit->addChild('order');
					$order->addChild('invoiceNumber', $invoiceno);
					break;
			}
		} else {
			switch($callby) {
				case 'refund':
					$credit->addChild('transId', $authtransID);
					break;
				case 'void':
					$credit->addChild('transId', $authtransID);
					break;
				default:
					$order = $credit->addChild('order');
					$order->addChild('invoiceNumber', $invoiceno);
					break;
			}
		}
		$TxRqXML = $TxRq->asXML();
		return $TxRqXML;
	}
	public function processRequest($url, $xml)	{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    $response = curl_exec($ch);
	    curl_close ($ch);
	      		
	 if($this->getDebug()) {
	      	$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
	      	$logger->info("XML Sent: $xml");
	      	$logger->info("XML Received: $response");
	   }

	    return $response;
	}
		
	public function parseXML($start, $end, $xml)	 {
		//return preg_replace('|^.*?'.$start.'(.*?)'.$end.'.*?$|i', '$1', substr($xml, 335));
		return preg_replace('|^.*?'.$start.'(.*?)'.$end.'.*?$|i', '$1', $xml);
	}
	public function parseMultiXML($xml, $ccnum)	 {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("MultiParseXML...");
		}	
		$ccnum='XXXX'.substr($ccnum, -4, 4);		
		$pos = strpos($xml,"<?xml version=");
		$fixedXML=substr($xml,$pos);
		$fixedXML = preg_replace('/xmlns="(.+?)"/', '', $fixedXML);
		$dom=new DOMDocument;
		$dom->loadXML($fixedXML);
		//print_r($dom);
		$profileID=array();
		$CardNo=array();
		$ProfileIDs=$dom->getElementsByTagname('customerPaymentProfileId');
		$index=0;
		foreach ($ProfileIDs as $Profile) {
			$value=$Profile->nodeValue;
			$profileID[]=$value;
			$index++;
		}
		//print_r($profileID);
		$Cards=$dom->getElementsByTagname('cardNumber');
		foreach ($Cards as $Card) {
			$value=$Card->nodeValue;
			$CardNo[]=$value;
		}
		//print_r($CardNo);
		for ($iindex=0; $iindex<$index; $iindex++) {
			if($this->getDebug())
			{
				$logger->info(" Payment: " . $profileID[$iindex]);
				$logger->info(" Card Number: " . $CardNo[$iindex]);
			}	
			if($CardNo[$iindex]==$ccnum) {
				$returnvalue=$profileID[$iindex];
				break;
			}
				$returnvalue=$profileID[0];
		}
			return $returnvalue;
	}
	public function validate() {
    	$info = $this->getInfoInstance();
		$ccNumber = $info->getCcNumber();
		
		if($this->getDebug())
		{
	    	$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering validate()");
		}
		
		/* If the string contains "tkn" assume that the number came from the admin site. */
		if(strpos($ccNumber,'tkn') === FALSE) {	
		
	        //parent::validate();
	        $paymentInfo = $this->getInfoInstance();
	        if ($paymentInfo instanceof Mage_Sales_Model_Order_Payment) {
	            $currency_code = $paymentInfo->getOrder()->getBaseCurrencyCode();
	        } else {
	            $currency_code = $paymentInfo->getQuote()->getBaseCurrencyCode();
	        }
	        return $this;
		}
		return $this;
    }

protected function _call2(Varien_Object $payment,$callby='') {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("callby: ".$callby);
		}

		//print "<pre>"; print_r($payment); print "</pre>"; exit;
		$billToWho = array();
		$billToWho['firstname']=$payment->getFirst();
		$billToWho['lastname']=$payment->getLast();
		$billToWho['company']=$payment->getCompany();
		$billToWho['telephone']=$payment->getTelephone();
		$billToWho['fax']=$payment->getFax();
		$billToWho['address']=preg_replace('/[^A-Za-z0-9_]/', ' ', $payment->getStreet1()." ".$payment->getStreet2());	
		$billToWho['city']=$payment->getCity();
		$billToWho['region']=$payment->getState();
		$billToWho['postcode']=$payment->getZip();
		$billToWho['country_id']=$payment->getCountryId();
		//print "<pre>payment: "; print_r($payment); print "</pre>"; 
		//print "<pre>billtowho:"; print_r($billToWho); print "</pre>"; 
		$ExpirationDate = $payment->getCcExpYear() .'-'. str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT);
		//$invoiceno = $payment->getOrder()->getIncrementId();
		$custID=Mage::getSingleton('customer/session')->getCustomerId();
		$customer = Mage::getModel('customer/customer')->load($custID)->getData();
		$CustomerEmail = $customer["email"];
		$storeId = $customer["store_id"];
		//$billToWho = $payment->getOrder()->getBillingAddress();
		$cvv = $payment->getCcCid();
		$ccnum = $payment->getCcNumber();
		$ponum=$ccnum;
		$payment->setCcSsStartMonth('1');
		$token=$payment->getToken();
			$fullcarddata = true;
		if($token>'') {
			$fields = preg_split('/-/',$token);
			$CustomerProfileID = $fields[0];
			$PaymentProfileID = $fields[1];

		} else {
			$CustomerProfileID = 0;
			$PaymentProfileID = 0;
			//$fullcarddata = false;
		}
		//$cim=$payment->getCcSsStartMonth();
		$cim=1;
		
		if($this->getDebug()) { 
		$logger->info("Token: $token $CustomerProfileID $PaymentProfileID");
		$logger->info("CcNumber PoNumber: $ccnum, $ponum SaveCimCC: $cim"); }
		$url = $this->getGatewayUrl();

		$authtransID = '';
		if($this->getDebug()) { $logger->info("from database: $CustomerProfileID, $PaymentProfileID, $authtransID"); }
		/* If we have the Customer ID and Payment ID, we can just do the transaction */
		if(($CustomerProfileID>0) and ($PaymentProfileID>0)) {
				$updatePaymentProfileXML = $this->updatePaymentProfileXML($CustomerProfileID, $PaymentProfileID, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId);
//				$response = $this->processRequest($url, $updatePaymentProfileXML);
				 if($this->getDebug()) {		$logger->info("\n\n UPDATED PROFILE $PaymentProfileID \n\n"); }

//			$TxRqXML = $this->createTransXML($amountcalled, $tax, $CustomerProfileID, $PaymentProfileID, $callby, $invoiceno, $authtransID, $Approval, $storeId);
			$TxRqResponse = $this->processRequest($url, $updatePaymentProfileXML);

		}
		else {
			/* First try to create a Customer Profile */
			if($CustomerProfileID<1) {
				$CustProfileXML = $this->createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $custID, $billToWho, $cvv, $storeId);
				$response = $this->processRequest($url, $CustProfileXML);
				
				$responsebegin = strpos($response,"?>");
				if($this->getDebug()) {	$logger->info("responsebegin: $responsebegin"); }
				$response = substr($response,$responsebegin+2);
				if($this->getDebug()) {	$logger->info("response: $response"); }
				
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				/* Get Customer Profile ID */
				$CustomerProfileID = (int) $this->parseXML('<customerProfileId>','</customerProfileId>', $response);
			}
			$ExistingCustProfile=$CustomerProfileID;
			/* Get Payment Profile ID */
			if($PaymentProfileID == '0') {
				if($this->getDebug()) {	$logger->info("\n\n PAYMENT PROFILE NOT SET 2\n\n"); }
		// First try to create a Customer Profile
			$CustProfileXML = $this->createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $custID, $billToWho, $cvv, $storeId);
			$response = $this->processRequest($url, $CustProfileXML);
			
				$responsebegin = strpos($response,"?>");
				if($this->getDebug()) {	$logger->info("responsebegin: $responsebegin"); }
				$response = substr($response,$responsebegin+2);
				if($this->getDebug()) {	$logger->info("response: $response"); }

			
			$resultErrorCode = $this->parseXML('<code>','</code>',$response);
 if($this->getDebug()) {		$logger->info("error code: $resultErrorCode"); }
			// Get Customer and Payment Profile ID
			$CustomerProfileID = (int) $this->parseXML('<customerProfileId>','</customerProfileId>', $response);
			$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileIdList><numericString>','</numericString></customerPaymentProfileIdList>', $response);

 if($this->getDebug()) {		$logger->info("CustomerProfileID: $CustomerProfileID"); }			
 if($this->getDebug()) {		$logger->info("PaymentProfileID: $PaymentProfileID"); }
			
			$resultText = $this->parseXML('<text>','</text>',$response);
			$resultCode = $this->parseXML('<resultCode>','</resultCode>',$response);
			
 if($this->getDebug()) {		$logger->info("resultText: $resultText"); }			
 if($this->getDebug()) {		$logger->info("resultCode: $resultCode"); }
			
			if($resultErrorCode == 'E00039') {
					 if($this->getDebug()) { $logger->info("\n\n ALREADY HAVE A CUST PROFILE \n\n"); }
				$split = preg_split('/ /',$resultText);
				$ExistingCustProfile = $split[5];
				if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) 
				{ $ExistingCustProfile = $CustomerProfileID; } else { $CustomerProfileID = $ExistingCustProfile; }
				//NEVER GOT HERE
				if($this->getDebug()) { $logger->info("Account Values2-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }
				if($this->getDebug()) {		$logger->info("\n\n Problem with CustomerProfileID"); }
				}
				$addPaymentProfileXML = $this->addPaymentProfileXML($ExistingCustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId);
				
				//<customerPaymentProfileIdList><numericString>
				
				$response = $this->processRequest($url, $addPaymentProfileXML);
				
				$responsebegin = strpos($response,"?>");
				if($this->getDebug()) {	$logger->info("responsebegin: $responsebegin"); }
				$response = substr($response,$responsebegin+2);
				if($this->getDebug()) {	$logger->info("response: $response"); }

				$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileId>','</customerPaymentProfileId>', $response);
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				if(substr($resultErrorCode,0,1) == 'E') {
				if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A PAYMENT PROFILE WITH THE CARD $resultErrorCode \n\n"); }
					//Get Correct PaymentProfileID
				}
			}
			if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) 
				{ $ExistingCustProfile = $CustomerProfileID; }
			if(($CustomerProfileID==0) AND ($ExistingCustProfile>0)) 
				{ $CustomerProfileID = $ExistingCustProfile; }
			if($PaymentProfileID=='0') {
				if($this->getDebug()) {	$logger->info("\n\n PAYMENT PROFILE NOT SET 1\n\n"); }
				$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
				$responseGET = $this->processRequest($url, $getCustXML);
				$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, false);
			}
			if($this->getDebug()) {	$logger->info("\n\n fullcarddata1: $fullcarddata  \n\n"); 
									if($fullcarddata) { $logger->info("\n\n fullcarddata2: TRUE  \n\n"); }
									if(!$fullcarddata) { $logger->info("\n\n fullcarddata3: FALSE  \n\n"); }}

			if($PaymentProfileID == '0') {
				if($this->getDebug()) {	$logger->info("\n\n PAYMENT PROFILE NOT SET 2\n\n"); }
				if(!$fullcarddata) {
					$addPaymentProfileXML = $this->addPaymentProfileXML($ExistingCustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra, $storeId);
					$response = $this->processRequest($url, $addPaymentProfileXML);

				$responsebegin = strpos($response,"?>");
				if($this->getDebug()) {	$logger->info("responsebegin: $responsebegin"); }
				$response = substr($response,$responsebegin+2);
				if($this->getDebug()) {	$logger->info("response: $response"); }

					$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileId>','</customerPaymentProfileId>', $response);
					$resultErrorCode = $this->parseXML('<code>','</code>',$response);
					 if(substr($resultErrorCode,0,1) == 'E') {
					 if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A PAYMENT PROFILE WITH THE CARD \n\n"); }
						//Get Correct PaymentProfileID
						$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
						$responseGET = $this->processRequest($url, $getCustXML);
						$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, true);
			}}}
			if($fullcarddata) {
				$updatePaymentProfileXML = $this->updatePaymentProfileXML($ExistingCustProfile, $PaymentProfileID, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra, $storeId);
				$response = $this->processRequest($url, $updatePaymentProfileXML);
				$responsebegin = strpos($response,"?>");
				if($this->getDebug()) {	$logger->info("responsebegin: $responsebegin"); }
				$response = substr($response,$responsebegin+2);
				if($this->getDebug()) {	$logger->info("response: $response"); }

				 if($this->getDebug()) {		$logger->info("\n\n UPDATED PAYMENT PROFILE1 $PaymentProfileID \n\n"); }

				$resultErrorCode = $this->parseXML('<resultCode>','</resultCode>',$response);
				 if($resultErrorCode != 'Ok') { // Using an existing card already
				 if($this->getDebug()) {		$logger->info("\n\n UPDATING PAYMENT PROFILE1a $PaymentProfileID\n\n"); }
					//Get Correct PaymentProfileID
					$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
					$responseGET = $this->processRequest($url, $getCustXML);
					$PaymentProfileIDBack = $PaymentProfileID;
					$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, true);
					if($PaymentProfileID=='0') { $PaymentProfileID = $PaymentProfileIDBack; }
					if($forceupdate) {
						$updatePaymentProfileXML = $this->updatePaymentProfileXML($ExistingCustProfile, $PaymentProfileID, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra, $storeId);
						$response = $this->processRequest($url, $updatePaymentProfileXML);
				$responsebegin = strpos($response,"?>");
				if($this->getDebug()) {	$logger->info("responsebegin: $responsebegin"); }
				$response = substr($response,$responsebegin+2);
				if($this->getDebug()) {	$logger->info("response: $response"); }

					if($PaymentProfileID<10000) { $PaymentProfileID = $PaymentProfileIDBack; }
						 if($this->getDebug()) {		$logger->info("\n\n UPDATED PAYMENT PROFILE2 $PaymentProfileID \n\n"); }
					}}

				if($this->getDebug()) {	$logger->info("\n\n UPDATED PAYMENT PROFILE3 $PaymentProfileID \n\n"); }
				if($this->getDebug()) { $logger->info("Account Values4-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }
				}
		///////////////////////////////////////////////

		if(($PaymentProfileID == '0') AND ($this->getDebug())) {	$logger->info("\n\n PAYMENT PROFILE NOT SET 2"); }
		}
		$token="$CustomerProfileID-$PaymentProfileID";
		if($this->getDebug()) { $logger->info("token $CustomerProfileID - $PaymentProfileID"); }

		if (($CustomerProfileID>'0') AND ($PaymentProfileID>'0')) {
		/////////////////////////////////////////////////////////////////////////////////////////////////
		$orderTable=Mage::getConfig()->getTablePrefix().'sales_flat_order'; 
		$orderPaymentTable=Mage::getConfig()->getTablePrefix().'sales_flat_order_payment'; 
	
		$first=$billToWho['firstname'];
		$last=$billToWho['lastname'];
	
		$sql = "INSERT INTO $orderTable (state, status, store_id, customer_id, customer_email, customer_firstname, customer_lastname) VALUES ('data','data', $storeId, $custID, '$CustomerEmail', '$first', '$last')";

		if($this->getDebug()) {	$logger->info("part 1"); }
		if($this->getDebug()) {	$logger->info($sql); }
//print "<BR>1: ";
//print $sql;
		
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->query($sql);
	
//	$sql = "SELECT * FROM $orderPaymentTable p, $orderTable q WHERE q.customer_id=".$custID." AND q.entity_id=p.parent_id AND po_number LIKE '".$token."%' AND cc_type > '' ORDER BY p.entity_id desc Limit 1;";

	$sql = "SELECT * FROM $orderTable q WHERE q.customer_id=".$custID." ORDER BY q.entity_id desc Limit 1;";

	if($this->getDebug()) {	$logger->info("part 2"); }
	if($this->getDebug()) {	$logger->info($sql); }
//print "<BR>2: ";
//print $sql;
	
	$results = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($sql);
	foreach($results as $data2) {
		$parent_id = $data2['entity_id'];
	}

		$last4 = substr($payment->getCcNumber(), -4, 4);;
		$_ccType = $payment->getCcType();
		$_ccExpMonth = $payment->getCcExpMonth();
		$_ccExpYear = $payment->getCcExpYear();
	
	
		$sql = "INSERT INTO $orderPaymentTable (parent_id, cc_type, cc_last4, po_number, cc_exp_year, cc_exp_month, cc_ss_start_month, method) VALUES ('$parent_id', '$_ccType', '$last4', '$token', '$_ccExpYear', '$_ccExpMonth', '1', 'authorizecimsoap')";

		if($this->getDebug()) {	$logger->info("part 3"); }
		if($this->getDebug()) {	$logger->info($sql); }
//print "<BR>3: ";
//print $sql;
			
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->query($sql);
/////////////
		if($this->getDebug()) {	$logger->info("complete"); }
			//$paymentInfo->setPoNumber($token);
			//$paymentInfo->getOrder()->setTransactionId();
			//$paymentInfo->setCcSsStartMonth('1');  
}

		return true;
	}

	public function updatecc(Varien_Object $payment,$callby='') {

		$result = $this->_call2($payment,'updatecc');		
			//print "<PRE>paymenttest5:<BR>";
			//print_r($payment);
			//print "</PRE>";
	}
}
