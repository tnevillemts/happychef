<?php
/**
Copyright 2009-2013 Eric Levine
IDP
 */
class IDP_AuthorizeCIM_Model_authorizecimsoap extends Mage_Payment_Model_Method_Cc
{   
	protected $_code  		    = 'authorizecimsoap';
    	protected $_isGateway               = true;
    	protected $_canAuthorize            = true;
    	protected $_canCapture              = true;
    	protected $_canCapturePartial       = true;
    	protected $_canRefund               = true;
    	protected $_canRefundInvoicePartial = true;
    	protected $_canVoid                 = true;
    	protected $_canUseInternal          = true;
    	protected $_canUseCheckout          = true;
    	protected $_canUseForMultishipping  = true;
    	protected $_canSaveCc               = false;
    	const CC_URL_LIVE 		    = 'https://api.authorize.net/xml/v1/request.api';
	const CC_URL_TEST 		    = 'https://apitest.authorize.net/xml/v1/request.api';
    	const STATUS_APPROVED 		    = 'Approved';
	const STATUS_SUCCESS 		    = 'Complete';
	const PAYMENT_ACTION_AUTH_CAPTURE   = 'authorize_capture';
	const PAYMENT_ACTION_AUTH 	    = 'authorize';
    	const STATUS_COMPLETED    	    = 'Completed';
    	const STATUS_DENIED       	    = 'Denied';
    	const STATUS_FAILED       	    = 'Failed';
    	const STATUS_REFUNDED     	    = 'Refunded';
    	const STATUS_VOIDED       	    = 'Voided';

	public function getGatewayUrl() {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url_test');
		}
		else
		{
			return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url');
		}
	}

	public function getUsername($storeId=0) {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			//return 'xxxxxxxxx';   //hard code test ID username here
			return Mage::getStoreConfig('payment/authorizecimsoap/testusername',$storeId);
		}
		else
		{
			//if((isset(Mage::getStoreConfig('payment/authorizecimsoap/username',$storeId))) AND Mage::getStoreConfig('payment/authorizecimsoap/username',$storeId)>'') {
				return Mage::getStoreConfig('payment/authorizecimsoap/username',$storeId);
			//} else {
				//return Mage::getStoreConfig('payment/authorizenet/login',$storeId);
			//}
		}
	}

	public function getPassword($storeId=0) {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			//return 'xxxxxxxxxxxxxxxxxxx';  //hard code test ID key/password herekey/password here
			return Mage::getStoreConfig('payment/authorizecimsoap/testpassword',$storeId);
		}
		else
		{
			//if((isset(Mage::getStoreConfig('payment/authorizecimsoap/password',$storeId))) AND Mage::getStoreConfig('payment/authorizecimsoap/password',$storeId)>'') {
				return Mage::getStoreConfig('payment/authorizecimsoap/password',$storeId);
			//} else {
				//return Mage::getStoreConfig('payment/authorizenet/trans_key',$storeId);
			//}
		}
	}

	public function getDebug1() {
		 return true;
	}

	public function getDebug() {
			if((Mage::getStoreConfig('payment/authorizecimsoap/test')) & (file_exists(Mage::getBaseDir() . '/var/log'))) {
				return Mage::getStoreConfig('payment/authorizecimsoap/debug'); 
			} else {
				return false;
			}
	}

	public function getTest() {
		return Mage::getStoreConfig('payment/authorizecimsoap/test');
	}

	public function getLogPath() {
		return Mage::getBaseDir() . '/var/log/authorizecimsoap.log';
	}

	public function getLiveUrl() {
		return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url');
	}

	public function getTestUrl() {
		return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url_test');
	}

	public function getPaymentAction()
	{
		return Mage::getStoreConfig('payment/authorizecimsoap/payment_action');
	}

	public function getStrictCVV() {
		return true;
	}

	public function getReauth() {
		if(Mage::getStoreConfig('payment/authorizecimsoap/reauth')>0) {return true;} else {return false;}
	}
	
	public function createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $CustomerId, $billToWho, $cvv, $extra='', $storeId=0) {
		$doc = new SimpleXMLElement('<?xml version ="1.0" encoding = "utf-8"?><createCustomerProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('profile');
		$profile->addChild('merchantCustomerId', $CustomerId);
		$profile->addChild('email', $CustomerEmail);
		$PaymentProfile = $profile->addChild('paymentProfiles');
		$billTo = $PaymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        	} else { 
			$billTo->addChild('company',"xxxx"); 
		}
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment1 = $PaymentProfile->addChild('payment');
		$credit = $payment1->addChild('creditCard');
		
		if(substr($ccnum,0,4!="tkn-")) {
			$credit->addChild('cardNumber', $ccnum);
		} else {
			$credit->addChild('cardNumber', "XXXX".substr($ccnum, -4, 4));
		}
		$credit->addChild('expirationDate', $ExpirationDate);
		$testmode=$this->getTest();
		if(($cvv!=111) AND ($cvv!=1111)) {
			$credit->addChild('cardCode', $cvv); 
			// if($testmode) {
				// $doc->addChild('validationMode','testMode');
			// } else {
				// $doc->addChild('validationMode','liveMode');
			// }
		}
		//$exparam='<'."![CDATA[$extra]]".'>';
		//$extra = $doc->addChild('extraOptions',$exparam);
		$CustProfileXML = htmlspecialchars_decode($doc->asXML());
		return $CustProfileXML;
	}

	public function addPaymentProfileXML($CustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra='', $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerPaymentProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$PaymentProfile = $doc->addChild('paymentProfile');
		$billTo = $PaymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        	} else { 
			$billTo->addChild('company',"xxxx"); 
		}
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment1 = $PaymentProfile->addChild('payment');
		$credit = $payment1->addChild('creditCard');

		if(substr($ccnum,0,4!="tkn-")) {
			$credit->addChild('cardNumber', $ccnum);
		} else {
			$credit->addChild('cardNumber', "XXXX".substr($ccnum, -4, 4));
		}

		
		$credit->addChild('expirationDate', $ExpirationDate);
		$testmode=$this->getTest();
		if(($cvv!=111) AND ($cvv!=1111)) {
			$credit->addChild('cardCode', $cvv); 
			// if($testmode) {
				// $doc->addChild('validationMode','testMode');
			// } else {
				// $doc->addChild('validationMode','liveMode');
			// }
		}
		//$exparam='<'.'![CDATA['.$extra.']]'.'>';
		//$extra = $doc->addChild('extraOptions',$exparam);
		$CustPaymentXML = htmlspecialchars_decode($doc->asXML());
		return $CustPaymentXML;
	}

	public function updatePaymentProfileXML($CustProfile, $PayProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra='', $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><updateCustomerPaymentProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$PaymentProfile = $doc->addChild('paymentProfile');
		$billTo = $PaymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        	} else { 
			$billTo->addChild('company',"xxxx"); 
		}
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment1 = $PaymentProfile->addChild('payment');
		$credit = $payment1->addChild('creditCard');

		if(substr($ccnum,0,4!="tkn-")) {
			$credit->addChild('cardNumber', $ccnum);
		} else {
			$credit->addChild('cardNumber', "XXXX".substr($ccnum, -4, 4));
		}
		$credit->addChild('expirationDate', $ExpirationDate);
		$testmode=$this->getTest();
		if(($cvv!=111) AND ($cvv!=1111)) {
			$credit->addChild('cardCode', $cvv); 
			// if($testmode) {
				// $doc->addChild('validationMode','testMode');
			// } else {
				// $doc->addChild('validationMode','liveMode');
			// }
		}
		$PaymentProfile->addChild('customerPaymentProfileId', $PayProfile);
		//$exparam='<'.'![CDATA['.$extra.']]'.'>';
		//$extra = $doc->addChild('extraOptions',$exparam);
		$CustPaymentXML = htmlspecialchars_decode($doc->asXML());
		return $CustPaymentXML;
	}

	public function createCustomerShippingAddressXML($CustProfile, $shipToWho, $extra='', $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerShippingAddressRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$shipTo = $doc->addChild('address');
		$shipTo->addChild('firstName',$shipToWho['firstname']);
		$shipTo->addChild('lastName',$shipToWho['lastname']);
		if(!empty($shipToWho['company'])){
			$shipTo->addChild('company',htmlentities($shipToWho['company']));
        	} else { 
			$shipTo->addChild('company',"xxxx"); 
		}
		$shipTo->addChild('address',$shipToWho['address']);
		$shipTo->addChild('city',$shipToWho['city']);
		$shipTo->addChild('state',$shipToWho['region']);
		$shipTo->addChild('zip',$shipToWho['postcode']);
		$shipTo->addChild('country',$shipToWho['country_id']);
		$shipTo->addChild('phoneNumber',$shipToWho['telephone']);
		$shipTo->addChild('faxNumber',$shipToWho['fax']);
		//$exparam='<'.'![CDATA['.$extra.']]'.'>';
		//$extra = $doc->addChild('extraOptions',$exparam);
		$CustShipXML = htmlspecialchars_decode($doc->asXML());
		return $CustShipXML;
	}

	public function getProfileXML($CustProfile, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><getCustomerProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$getCustProfileXML = $doc->asXML();
		
		return $getCustProfileXML;
	}

	public function createTransXML($amount, $tax, $CustomerProfileID, $PaymentProfileID, $ShippingProfileID, $callby, $invoiceno, $authtransID, $Approval, $cvv=111, $extra='', $storeId=0) {
		// Build Transaction Request
		$TxRq = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerProfileTransactionRequest/>');
		$TxRq->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $TxRq->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$transaction = $TxRq->addChild('transaction');
		if($this->getPaymentAction()=='authorize')
		{
			switch($callby) {
				case 'captureonly':
					$credit = $transaction->addChild('profileTransCaptureOnly');
					$credit->addChild('amount', $amount);
					break;
				case 'capture':
					$credit = $transaction->addChild('profileTransPriorAuthCapture');
					$credit->addChild('amount', $amount);
					break;
				case 'refund':
					$credit = $transaction->addChild('profileTransRefund');
					$credit->addChild('amount', $amount);
					break;
				case 'void':
					$credit = $transaction->addChild('profileTransVoid');
					break;
				default:
					$credit = $transaction->addChild('profileTransAuthOnly');
					$credit->addChild('amount', $amount);
					if($tax>0) {
						$credittax = $credit->addChild('tax');
						$credittax->addChild('amount', $tax);
					}
					break;
			}
		} else {
			switch($callby) {
				case 'refund':
					$credit = $transaction->addChild('profileTransRefund');
					$credit->addChild('amount', $amount);
					break;
				case 'void':
					$credit = $transaction->addChild('profileTransVoid');
					break;
				default:
					$credit = $transaction->addChild('profileTransAuthCapture');
					$credit->addChild('amount', $amount);
					if($tax>0) {
						$credittax = $credit->addChild('tax');
						$credittax->addChild('amount', $tax);
					}
					break;
			}
		}
		$credit->addChild('customerProfileId', $CustomerProfileID);
		$credit->addChild('customerPaymentProfileId', $PaymentProfileID);
		
		if($this->getPaymentAction()=='authorize')
		{
			switch($callby) {
				case 'captureonly':
					$credit->addChild('approvalCode', $Approval);
					break;
				case 'capture':
					$credit->addChild('transId', $authtransID);
					break;
				case 'refund':
					$credit->addChild('transId', $authtransID);
					break;
				case 'void':
					$credit->addChild('transId', $authtransID);
					break;
				default:
					if(isset($ShippingProfileID) AND ($ShippingProfileID>0)) { 
					$credit->addChild('customerShippingAddressId', $ShippingProfileID); }
					$order = $credit->addChild('order');
					$order->addChild('invoiceNumber', $invoiceno);
					if(($cvv!=111) AND ($cvv!=1111)) {
						$credit->addChild('cardCode', $cvv); 
					}
					break;
			}
		} else {
			switch($callby) {
				case 'refund':
					$credit->addChild('transId', $authtransID);
					break;
				case 'void':
					$credit->addChild('transId', $authtransID);
					break;
				default:
					if(isset($ShippingProfileID) AND ($ShippingProfileID>0)) { 
					$credit->addChild('customerShippingAddressId', $ShippingProfileID); }
					$order = $credit->addChild('order');
					$order->addChild('invoiceNumber', $invoiceno);
					break;
			}
		}
		$exparam='<'.'![CDATA['.$extra.']]'.'>';
		$extra = $TxRq->addChild('extraOptions',$exparam);
		$TxRqXML = htmlspecialchars_decode($TxRq->asXML());
		return $TxRqXML;
	}

	public function processRequest($url, $xml, $customerEmail = null, $userAgent = null) {
        	$ch = curl_init();
        	curl_setopt($ch, CURLOPT_URL, $url);
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: text/xml'));
        	curl_setopt($ch, CURLOPT_HEADER, 1);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        	curl_setopt($ch, CURLOPT_POST, 1);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    	$response = curl_exec($ch);
	    	curl_close ($ch); 		
	 	if($this->getDebug()) {
	      		$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
	      		$logger->info("XML Sent: $xml");
	      		$logger->info("XML Received: $response");
	   	}
		// drichter - comment out this line to disable recording of all xml requests and responses.
		//$this->recordRequest($url, $xml, $response, $customerEmail, $userAgent);

	    	return $response;
	}	

	public function parseXML($start, $end, $xml) {
		$xmlbegin = 0;
		$xmlbegin = strpos($xml,"?>");
		if($xmlbegin > 0) { 
			$xml = substr($xml,$xmlbegin+2); 
		}
		return preg_replace('|^.*?'.$start.'(.*?)'.$end.'.*?$|i', '$1', $xml);
	}

	public function parseMultiXML($xml, $ccnum, $forcevalue) {
		$ccnum='XXXX'.substr($ccnum, -4, 4);
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("MultiParseXML for $ccnum");
		}
		$ccnumorig=$ccnum;		
		$pos = strpos($xml,"<?xml version=");
		$returnvalue=0;
		$fixedXML=substr($xml,$pos);
		$fixedXML = preg_replace('/xmlns="(.+?)"/', '', $fixedXML);
		$dom=new DOMDocument;
		$dom->loadXML($fixedXML);
		//print_r($dom);
		$profileID=array();
		$CardNo=array();
		$ProfileIDs=$dom->getElementsByTagname('customerPaymentProfileId');
		$index=0;
		foreach ($ProfileIDs as $Profile) {
			$value=$Profile->nodeValue;
			$profileID[]=$value;
			$index++;
		}
		{$ccnumflag=0;}
		//print_r($profileID);
		$Cards=$dom->getElementsByTagname('cardNumber');
		foreach ($Cards as $Card) {
			$value=$Card->nodeValue;
			$CardNo[]=$value;
			if($value==$ccnumorig) {$ccnumflag=$value;}
			if($this->getDebug()) { $logger->info("ccnumflag: $ccnumflag"); }		
		}
		//print_r($CardNo);
		for ($iindex=0; $iindex<$index; $iindex++) {
			if($this->getDebug())
			{
				$logger->info(" Payment: " . $profileID[$iindex]);
				$logger->info(" Card Number: " . $CardNo[$iindex]);
			}	
			if($CardNo[$iindex]==$ccnum) {
				$returnvalue=$profileID[$iindex];
				//break;
			}
		}
		if($this->getDebug()) { $logger->info("returnvalue5: $returnvalue"); }	
		if($returnvalue==0) {
			if($ccnumflag>0) { 
				$returnvalue=$ccnumorig; 
			} else { 
				if((isset($profile[0])) and ($profile[0]>0) and ($forcevalue)) { 
					$returnvalue=$profileID[0]; 
				}
			}
		}
			return $returnvalue;
	}

	public function parseMultiShippingXML($xml, $shipToWho, $returnfirst) {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("MultiParseShippingXML...");
		}
		$vvalue=0;	$customerShippingAddressId=0;

		$pos = strpos($xml,"<?xml version=");
		$fixedXML=substr($xml,$pos);
		$fixedXML = preg_replace('/xmlns="(.+?)"/', '', $fixedXML);
		$fixedXML=substr($xml,$pos);		
		$fixedXML = preg_replace('/xmlns="(.+?)"/', '', $fixedXML);
		$dom=new DOMDocument;
		if($this->getDebug()) { $logger->info("fixedXML: $fixedXML"); }			

		$dom=new DOMDocument;
		$dom->loadXML($fixedXML);
		$profileID=array();
		//$ProfileIDs=$dom->getElementsByTagname('shipToList')->getElementsByTagname('customerAddressId');
		$ProfileIDs=$dom->getElementsByTagname('customerAddressId');
		$Shippingindex=0;
		foreach ($ProfileIDs as $Profile) {
			$value=$Profile->nodeValue;
			$profileID[]=trim($value);
			$Shippingindex++;
		}
		$firstName=array();
		$firstnames=$dom->getElementsByTagname('firstName');
		$index=0;
		foreach ($firstnames as $Profile) {
			$value=$Profile->nodeValue;
			$firstName[]=trim($value);
			$index++;
		}
		$firstName=array_slice($firstName,$index-$Shippingindex);
		$lastName=array();
		$lastnames=$dom->getElementsByTagname('lastName');
		foreach ($lastnames as $Profile) {
			$value=$Profile->nodeValue;
			$lastName[]=trim($value);
		}
		$lastName=array_slice($lastName,$index-$Shippingindex);
		$company=array();
		$companys=$dom->getElementsByTagname('company');
		foreach ($companys as $Profile) {
			$value=$Profile->nodeValue;
			$company[]=trim($value);
		}
		$company=array_slice($company,$index-$Shippingindex);
		$address=array();
		$addresss=$dom->getElementsByTagname('address');
		foreach ($addresss as $Profile) {
			$value=$Profile->nodeValue;
			$address[]=trim($value);
		}
		$address=array_slice($address,$index-$Shippingindex);	
		$city=array();
		$citys=$dom->getElementsByTagname('city');
		foreach ($citys as $Profile) {
			$value=$Profile->nodeValue;
			$city[]=trim($value);
		}
		$city=array_slice($city,$index-$Shippingindex);	
		$state=array();
		$states=$dom->getElementsByTagname('state');
		foreach ($states as $Profile) {
			$value=$Profile->nodeValue;
			$state[]=trim($value);
		}
		$state=array_slice($state,$index-$Shippingindex);
		$zip=array();
		$zips=$dom->getElementsByTagname('zip');
		foreach ($zips as $Profile) {
			$value=$Profile->nodeValue;
			$zip[]=trim($value);
		}
		$zip=array_slice($zip,$index-$Shippingindex);
		$country=array();
		$countrys=$dom->getElementsByTagname('country');
		foreach ($countrys as $Profile) {
			$value=$Profile->nodeValue;
			$country[]=trim($value);
		}
		$country=array_slice($country,$index-$Shippingindex);
		$phoneNumber=array();
		$phoneNumbers=$dom->getElementsByTagname('phoneNumber');
		foreach ($phoneNumbers as $Profile) {
			$value=$Profile->nodeValue;
			$phoneNumber[]=trim($value);
		}
		$phoneNumber=array_slice($phoneNumber,$index-$Shippingindex);
		$faxNumber=array();
		$faxNumbers=$dom->getElementsByTagname('faxNumber');
		foreach ($faxNumbers as $Profile) {
			$value=$Profile->nodeValue;
			$faxNumber[]=trim($value);
		}
		$faxNumber=array_slice($faxNumber,$index-$Shippingindex);
		$i=0;
		$valuee=0;
		$cShippingAddressId=0;
		foreach ($ProfileIDs as $Profile) {
			$match=true;
			if($valuee==0) {$valuee=$Profile->nodeValue; }
			if(($match) and (strtoupper($shipToWho['firstname'])!=strtoupper($firstName[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['lastname'])!=strtoupper($lastName[$i])))
			{ $match=false;}
			if(strtoupper($shipToWho['company'])==""){$shipToWho['company']="XXXX"; }
			if(($match) and (strtoupper($shipToWho['company'])!=strtoupper($company[$i])))
			{ $match=false;}
			if(($match) and (strtoupper(preg_replace('/ /', $shipToWho['address'],'')!=strtoupper(preg_replace('/ /', $address[$i],'')))))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['city'])!=strtoupper($city[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['region'])!=strtoupper($state[$i]))) 
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['postcode'])!=strtoupper($zip[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['country_id'])!=strtoupper($country[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['telephone'])!=strtoupper($phoneNumber[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['fax'])!=strtoupper($faxNumber[$i])))
			{ $match=false;}
			if(($match) AND ($vvalue==0)) {$customerShippingAddressId=$valuee; $vvalue=$valuee;}
			if($vvalue==0) {$cShippingAddressId=$valuee; }			
			//PRINT "MATCH!"
			$i++;
		}
		if($vvalue==0) {
			$customerShippingAddressId=$cShippingAddressId; 
		}
		$i=0;
		foreach ($ProfileIDs as $Profile) {
			if($this->getDebug()) {
				$logger->info("i: $i");
				$logger->info((strtoupper($shipToWho['firstname']))."/".(strtoupper($firstName[$i]))); 
				$logger->info((strtoupper($shipToWho['lastname']))."/".strtoupper($lastName[$i])); 
				$logger->info((strtoupper($shipToWho['company']))."/".strtoupper($company[$i])); 
				$logger->info((strtoupper(preg_replace('/[^A-Za-z0-9_]/', ' ',$shipToWho['address']))."/".strtoupper(preg_replace('/[^A-Za-z0-9_]/', ' ',$address[$i])))); 
				$logger->info((strtoupper($shipToWho['city']))."/".strtoupper($city[$i])); 
				$logger->info((strtoupper($shipToWho['region']))."/".strtoupper($state[$i])); 
				$logger->info((strtoupper($shipToWho['postcode']))."/".strtoupper($zip[$i])); 
				$logger->info((strtoupper($shipToWho['country_id']))."/".strtoupper($country[$i])); 
				$logger->info((strtoupper($shipToWho['telephone']))."/".strtoupper($phoneNumber[$i])); 
				$logger->info((strtoupper($shipToWho['fax']))."/".strtoupper($faxNumber[$i])); 
				if($match) {$logger->info("MATCHED!");} 
				if (($match) and ($vvalue<1)) {$vvalue=$valuee;}
				$logger->info("VALUE: $valuee"); 
				$logger->info("VVALUE: $vvalue"); 
				if($this->getDebug()) {$logger->info($i);	};
			}
			$i++;
		}
		
		//if((!$match) and ($returnfirst) AND ($customerShippingAddressId==0)) {$customerShippingAddressId=$value; }
		if($valuee>1) {
			if(($match) and ($customerShippingAddressId < 1)) { 
				$customerShippingAddressId=$valuee; 
			}
			if($returnfirst) {
				$customerShippingAddressId=$valuee; 
			}
		} elseif(($returnfirst) and ($vvalue>0)) {
			$customerShippingAddressId=$vvalue; 
		}
		if($this->getDebug()) {
			$logger->info("customerShippingAddressIdsearch:".$customerShippingAddressId);	
		}
		
		return $customerShippingAddressId;
	}

	public function validate() {
		if($this->getDebug()) {
	    		$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering validate()");
		}
		return $this;
    	}

	public function authorize(Varien_Object $payment, $amount) {
		$orderno = NULL;
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering authorize()");
		}
		$this->setAmount($amount)
		->setPayment($payment);
		if(true) {
			$orderTable=Mage::getConfig()->getTablePrefix().'sales_flat_order'; 
			$orderTransTable=Mage::getConfig()->getTablePrefix().'sales_payment_transaction'; 
			$orderno = $payment->getOrder()->getIncrementId();
			$sql = "DELETE p.* FROM $orderTransTable p, $orderTable q WHERE q.increment_id ='".$orderno."' AND q.entity_id=p.order_id AND p.txn_type='authorization';";
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$write->query($sql);
		}
		$result = $this->_call($payment, 'authorize', $amount);
		if($this->getDebug()) { 
			$logger->info(var_export($result, TRUE)); 
		}
		if($result === false)
		{
			$e = $this->getError();
			if (isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		}
		else
		{
 			//////////////////////////////////////////////////////////
                        //
                        // Induce an AVS error (developer drichter) - Just for development. only uncomment one CcAvsStatus at a time.
                        //
                        // N -  Both street address and zip are invalid, billing address section.
                        //$result['Status']['CcAvsStatus']       = 'N';
                        // A -  Only zip is invalid, billing address section.
                        //$result['Status']['CcAvsStatus']       = 'A';
                        // B - Street address or zipcode are missing, theoretically can't happen because of validations on billing address section.
                        //$result['Status']['CcAvsStatus']       = 'B';
                        // P - Or anything else is considered a payment error, will send us to payment section.
                        //$result['Status']['CcAvsStatus']       = 'P';
                        // R - Retry, system unavailable. Same as error code.
                        // $result['Status']['CcAvsStatus']      = 'R';
                        // U - Address info is unavailable for this transaction.
                        //$result['Status']['CcAvsStatus']       = 'U';
                        // Y - This should be an allowed error.
                        //$result['Status']['CcAvsStatus']       = 'Y';
                        //
                        // Uncomment all three of these when testing errors.
                        //$result['Status']['statusCode']        = 'Error';
                        //$result['Status']['code']              = 'E00027';
                        //$result['Status']['statusDescription'] = 'Induced err, avscode: ' . $result['Status']['CcAvsStatus'];
			            //$result['Status']['statusDescription'] = 'A duplicate transaction has been submitted.';
                        //
                        // End Induce error.
                        //
                        // Induce success
                        //$result['Status']['code']              = 'I00001';
                        //$result['Status']['statusCode']        = 'Ok';
                        //
                        //////////////////////////////////////////////////////////

			$throwException = FALSE;
                        // Check if there is a gateway error.
                        if($result['Status']['statusCode'] == "Ok")
                        {
                                // Check if there is an error processing the credit card
                                $avscode = $result['Status']['CcAvsStatus'];
                                if($avscode == 'B') {
                                        $result['Status']['statusCode']        = 'Error';
                                        $result['Status']['code']              = 'E00027';
                                        $result['Status']['code'] = $result['Status']['code'] . '-' . $result['Status']['CcAvsStatus'];
                                        $result['Status']['statusDescription'] = "There was a problem with your credit card. It was declined by the processor because the billing address you entered is not the address associated with this credit card.  You may want to double check your information, try another card or call us at 800-347-0288 to complete your order.";
                                        $throwException = TRUE;
                                } else if($result['Status']['code'] == "I00001") {
                                        $payment->setStatus(self::STATUS_APPROVED);
                                        $payment->setTransactionId($result['Status']['transno']);
                                        $payment->setIsTransactionClosed(0);
                                        $payment->setTxnId($result['Status']['transno']);
                                        $payment->setParentTxnId($result['Status']['transno']);
                                        $payment->setCcTransId($result['Status']['transno']);
                                        $payment->setParentTxnId($result['Status']['transno']);
                                        $payment->setCcTransId($result['Status']['transno']);
                                        $payment->setCcAvsStatus($result['Status']['CcAvsStatus']);
                                        $payment->setCcCidStatus($result['Status']['CcCidStatus']);
                                }
                                $this->record($result, $orderno, 'OK');
                        } else {

                            $session = Mage::getSingleton('checkout/session');
                            Mage::log("File: " . __FILE__ . " - Quote [{$session->getQuote()->getId()}] - code [{$result['Status']['code']}] - AVS code [{$result['Status']['CcAvsStatus']} - Message [{$result['Status']['statusDescription']}]", null, 'gateway-temp.log');

                            $result['Status']['code'] = $result['Status']['code'] . '-' . $result['Status']['CcAvsStatus'];
                                if($result['Status']['CcAvsStatus'] == 'A' || $result['Status']['CcAvsStatus'] == 'N') {
                                        $result['Status']['statusDescription'] = "There was a problem with your credit card. It was declined by the processor because the billing address you entered is not the address associated with this credit card.  You may want to double check your information, try another card or call us at 800-347-0288 to complete your order.";
                                } else if ($result['Status']['CcAvsStatus'] == 'B') {
                                        $result['Status']['statusDescription'] = "The credit card you have entered has been declined. Please carefully check the credit card information you entered and resubmit your order.";
                                        $result['Status']['CcAvsStatus'] = 'P';
                                } else {
                                        $result['Status']['statusDescription'] = "There was a problem with your credit card. It was declined by the processor which could be due to a number of things. Some possible causes include insufficient funds, or perhaps the billing address you entered is not the address associated with this credit card. You may want to double check your information, try another card or call us at 800-347-0288 to complete your order.";
                                }
                                $this->record($result, $orderno, 'DECLINE');
                                $throwException = TRUE;
                        }

                        if($throwException) {
                                // Original code - removed.
                                //Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);

                                // New code.
                                try {
                                        if(strpos($result['Status']['statusDescription'], 'This transaction has been declined') !== false) {
                                                $exceptionText = Mage::helper('checkout/cart')->__($result['Status']['statusDescription']);
                                                if($exceptionText == '' || $exceptionText == null) {
                                                        $exceptionText = "Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription'];;
                                                }
                                                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " " . $exceptionText, null, 'gateway.log');
                                        } else {
                                                $exceptionText = "Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription'];
                                        }
                                } catch (Exception $e) {
                                        Mage::throwException("Another exception caught here: " . $e->getMessage() . ".");
                                }
                                Mage::throwException($exceptionText);
                                // End new code.
                        }
		}
		return $this;
	}

	public function record($result, $orderno, $status) {

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $ret = FALSE;

        try {
            $resultCode     = $result['Status']['statusCode'];
            $code           = $result['Status']['code'];
            $avscode        = $result['Status']['CcAvsStatus'];
            $etext          = $result['Status']['statusDescription'];
            $session        = Mage::getSingleton('checkout/session');
            $quoteId        = $session->getQuoteId();

            $sql = "INSERT INTO mag_gateway_log (quote_id, order_id, resultCode, status, code, avscode, text, created_at) " .
                " VALUES (" . $quoteId . ", " . $orderno . ", '" . $resultCode . "', '" . $status . "', '" . $code . "', '" . $avscode . "', '" . $etext . "', now())";

            $result = $writeConnection->query($sql);

            if(! $result) {
                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " line: " . __LINE__ . " sql failed [" . $sql . "]", null, 'gateway.log');
            } else {
                $ret = TRUE;
            }
        } catch(Exception $e) {
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " line: " . __LINE__ . " sql failed [" . $sql . "] error message [" . $e->getMessage() . "] ", null, 'gateway.log');
        }

        return $ret;
    }

	public function recordRequest($url, $req, $resp, $customerEmail, $userAgent) {

        $ret = FALSE;

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');


        try {

            $sql = "INSERT INTO mag_gateway_request (url, req, resp, customer_email, user_agent, created_at) " . " VALUES ('" . $url . "', '" . $req . "', '" . $resp . "', '" . $customerEmail . "', '" . $userAgent . "', now())";

            $result = $writeConnection->query($sql);

            if(! $result) {
                Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " line: " . __LINE__ . " sql failed [" . $sql . "]", null, 'gateway.log');
            } else {
                $ret = TRUE;
            }
        } catch(Exception $e) {
            Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " line: " . __LINE__ . " sql failed [" . $sql . "] error message [" . $e->getMessage() . "] ", null, 'gateway.log');
        }

        return $ret;
	}

	public function capture(Varien_Object $payment, $amount) {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering capture()");
		}
		
		$this->setAmount($amount)
			->setPayment($payment);

		$ttt=$this->getReauth();
		if($this->getDebug()) { 
			$logger->info("getReauth: $ttt"); 
		}
		if($this->getReauth()) {
		//if(false) {
			$orderTable=Mage::getConfig()->getTablePrefix().'sales_flat_order'; 
			if($this->getDebug()) { 
				$logger->info("Reauthorization Steps"); 
			}
			$orderInvoiceTable=Mage::getConfig()->getTablePrefix().'sales_flat_invoice'; 
			$orderno = $payment->getOrder()->getIncrementId();
			//$this->getInvoice()->getId();  invoice number
			$sql = "SELECT * FROM $orderInvoiceTable p, $orderTable q WHERE q.increment_id ='$orderno' AND q.entity_id=p.order_id AND p.increment_id>'' ORDER BY p.entity_id desc LIMIT 1;";
			$data2 = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchRow($sql);
			$lastinvoice = $data2['increment_id'];
			if($this->getDebug()) { 
				$logger->info("lastinvoice: $lastinvoice"); 
			}
			if($this->getDebug()) { 
				$logger->info("step4c"); 
			}
			if(!$lastinvoice) {
				$result = $this->_call($payment,'capture', $amount);
			} else { 
				$result = $this->_call($payment,'captureonly', $amount);
			}
			if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
			if($result['Status']['transno']=='0') {
				$result = $this->_call($payment,'captureonly', $amount);
			} 
		} else {
			if($this->getDebug()) { $logger->info("step4b"); }
			$result = $this->_call($payment,'capture', $amount);
		}

		if($this->getDebug()) { 
			$logger->info("step5"); 
		}
		if($this->getDebug()) { 
			$logger->info(var_export($result, TRUE)); 
		}
		if($result === false) {
			$e = $this->getError();
			if(isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		} else {
			// Check if there is a gateway error
			if($result['Status']['statusCode'] == "Ok") {
				// Check if there is an error processing the credit card
				if($result['Status']['code'] == "I00001") {
					//$payment->setIsTransactionClosed(1);
					//$payment->registerCaptureNotification();
					$payment->setIsTransactionClosed(0);
					$payment->setTransactionId($result['Status']['transno']);
					$payment->setLastTransId($result['Status']['transno'])
							->setCcApproval($result['Status']['code'])
							->setCcTransId($result['Status']['transno'])
							->setCcAvsStatus($result['Status']['CcAvsStatus'])
							->setCcCidStatus($result['Status']['CcCidStatus']);
				}
			} else {
				Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
			}
		}
		return $this;
	}

	public function refund(Varien_Object $payment, $amount) {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering refund()");
		}
		$this->setAmount($amount)
			->setPayment($payment);
			
		$result = $this->_call($payment,'refund', $amount);
		if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
		if($result === false)
		{
			$e = $this->getError();
			if(isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		} else {
			// Check if there is a gateway error
			if($result['Status']['statusCode'] == "Ok") {
				// Check if there is an error processing the credit card
				if($result['Status']['code'] == "I00001") {
					//$payment->registerRefundNotification();
					$payment->setCcApproval($result['Status']['code'])
						->setTransactionId($result['Status']['transno'])
						->setCcTransId($result['Status']['transno'])
						->setCcAvsStatus($result['Status']['CcAvsStatus'])
						->setCcCidStatus($result['Status']['CcCidStatus']);
				}
			} else {
				Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
			}
		}
		return $this;
	}

	public function void(Varien_Object $payment) {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering void()");
		}
		//$this->setAmount($amount)
		//	->setPayment($payment);

		$result = $this->_call($payment,'void', 99999);
		if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
		if($result === false) {
			$e = $this->getError();
			if (isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		} else {
			// Check if there is a gateway error
			if($result['Status']['statusCode'] == "Ok") {
				// Check if there is an error processing the credit card
				if($result['Status']['code'] == "I00001") {
					$cancelorder=true;
					if ($cancelorder) { $payment->getOrder()->setState('canceled', true, 'Canceled/Voided');
					} else { $payment->registerVoidNotification(); }
					$payment->setCcApproval($result['Status']['code'])
						->setTransactionId($result['Status']['transno'])
						->setCcTransId($result['Status']['transno'])
						->setCcAvsStatus($result['Status']['CcAvsStatus'])
						->setCcCidStatus($result['Status']['CcCidStatus']);
					$payment->setStatus(self::STATUS_VOIDED);
				}
			}
			else
			{
				Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
			}
		}
		return $this;
	}

	protected function getDelay($tries){
    		return (int) (pow(2, $tries) * 100000);
	}

	protected function _call(Varien_Object $payment,$callby='', $amountcalled) {
		if($this->getDebug()) {
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("paymentAction: ".$this->getPaymentAction());
			$storeId = $payment->getOrder()->getStoreId();
			$logger->info("Storeid: ".$storeId);
		}
		//print "<pre>"; print_r($payment); print "</pre>"; exit;
		$ExpirationDate = $payment->getCcExpYear() .'-'. str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT);
		$invoiceno = $payment->getOrder()->getIncrementId();

		$CustomerEmail = $payment->getOrder()->getCustomerEmail();
		if($this->getDebug()) { 
			$logger->info("CustomerEmail1: ".$CustomerEmail); 
		}
		$userAgent = Mage::helper('core/http')->getHttpUserAgent();

		if(!$CustomerEmail>"") { 
			$CustomerEmail = $payment->getOrder()->getBillingAddress()->getEmail();  
		}
		if($this->getDebug()) { 
			$logger->info("CustomerEmail2: ".$CustomerEmail); 
		}

		$CustomerId = $payment->getOrder()->getCustomerId();
		if($this->getDebug()) { 
			$logger->info("CustomerId1: ".$CustomerId); 
		}

		if(!$CustomerId>0) { 
			$CustomerId = Mage::getSingleton('customer/session')->getCustomerId();  
		}
		if($this->getDebug()) { 
			$logger->info("CustomerId2: ".$CustomerId); 
		}

		$storeId = $payment->getOrder()->getStoreId();

		$billToWho=array();
		$billToWhoA = $payment->getOrder()->getBillingAddress();
		$billToWho['firstname']=$billToWhoA->getFirstname();
		$billToWho['lastname']=$billToWhoA->getLastname();
		
		$billToWho['address']=preg_replace('/[^A-Za-z0-9_]/', ' ', $billToWhoA->getStreet(1)." ".$billToWhoA->getStreet(2));	
		$billToWho['company']=preg_replace('/[^A-Za-z0-9_]/', ' ', $billToWhoA->getCompany());
		$billToWho['telephone']=$billToWhoA->getTelephone();
		$billToWho['fax']=$billToWhoA->getFax();
		$billToWho['city']=$billToWhoA->getCity();
		$billToWho['region']=$billToWhoA->getRegion();
		$billToWho['postcode']=$billToWhoA->getPostcode();
		$billToWho['country_id']=$billToWhoA->getCountryId();

		$shipToWho=array();
		$shipToWhoA = $payment->getOrder()->getShippingAddress();
		if((isset($shipToWhoA['lastname'])) and ($shipToWhoA['lastname']>'')) {		
			$shipToWho['address']=preg_replace('/[^A-Za-z0-9_]/', ' ', $shipToWhoA->getStreet(1)." ".$shipToWhoA->getStreet(2));	
			$shipToWho['firstname']=$shipToWhoA->getFirstname();
			$shipToWho['lastname']=$shipToWhoA->getLastname();
			$shipToWho['company']=preg_replace('/[^A-Za-z0-9_]/', ' ', $shipToWhoA->getCompany());
			$shipToWho['telephone']=$shipToWhoA->getTelephone();
			$shipToWho['fax']=$shipToWhoA->getFax();
			$shipToWho['city']=$shipToWhoA->getCity();
			$shipToWho['region']=$shipToWhoA->getRegion();
			$shipToWho['postcode']=$shipToWhoA->getPostcode();
			$shipToWho['country_id']=$shipToWhoA->getCountryId();
		}
		//$tax = $payment->getOrder()->getTaxAmount();
		$tax = number_format((float)$payment->getOrder()->getTaxAmount(), 2, '.', '');
		$cvv = $payment->getCcCid();
		if(($this->getStrictCVV()) AND (!$cvv)) { 
			if($payment->getCcType()=='AE') {
				$cvv=1111; 
			} else {
				$cvv=111; 
			} 
		}
		$extra1 = Mage::helper('core/http')->getRemoteAddr(true);
		$extra2 = $_SERVER['REMOTE_ADDR'];
		//$extra="x_customer_ip=".$extra2."&x_email_customer=FALSE";
		$extra="x_customer_ip=".$extra2;
		$ccnum = $payment->getCcNumber();
		$ponum = $payment->getPoNumber();
		$last4 =  $payment->getCcLast4();
		$fullcarddata=false;
		if($ccnum=='') {
			$ccnum="tkn-$ponum-$last4";
		}

		$showfrontendcim=$payment->getCcSsStartMonth();
		if($this->getDebug()) { 
			$logger->info("CcNumber, PoNumber: $ccnum, $ponum SaveCimCC: $showfrontendcim\n"); 
		}
		if($amountcalled<1) { 
			$amountcalled = $this->getAmount(); 
		}
		$amountcalled=number_format((float)$amountcalled, 2, '.', '');
		$url = $this->getGatewayUrl();
		$CustomerProfileID = 0;
		$PaymentProfileID = 0;
		$Approval = 0;
		$ShippingProfileID = 0; 
		
		if($this->getDebug()) { 
			$logger->info("FullCardData001: $fullcarddata USING ccnum: $ccnum USING ponum: $ponum"); 
		}
		//$tkn=strpos($ccnum,'tkn')+10;
		
		$tag=substr($ccnum, 0, 3);
		//$tag=$last4;
		//$tag=substr($ccnum, -4, 4);
		if($this->getDebug()) { 
			$logger->info("Flag tag: $tag"); 
		}
		//tkn-cust-pay-approval-ship::::token from form
		//cust-pay-approval-ship::::::token from po_number
		if($tag=="tkn") {
			$fullcarddata=false;
			if($this->getDebug()) { 
				$logger->info("FullCardData002false: $fullcarddata"); 
			}
			if($ccnum!="") {
				$fields = preg_split('/-/',$ccnum);
				if(isset($fields[1]) and ((INT)$fields[1]>10000000)) { 
					$CustomerProfileID=(INT) $fields[1]; 
				}
				if(isset($fields[2]) and ((INT)$fields[2]>10000000)) { 
					$PaymentProfileID=(INT) $fields[2]; 
				}
				//if(isset($fields[2])) {
				// and ($callby=='authorize')) { 
				if(isset($fields[3])) { 
					$Approval = $fields[3];
				} else {
					$Approval = 0; 
				}
				if(isset($fields[4]) and ((INT)$fields[4]>10000000)) { 
					$ShippingProfileID=(INT) $fields[4]; 
				}
				if(($ponum!="") and ($ShippingProfileID == 0)) {
					$fields2 = preg_split('/-/',$ponum);
					if(isset($fields2[3]) and ((INT)$fields2[3]>10000000)) { $ShippingProfileID=(INT) $fields2[3]; }
					$ccnum='XXXX'.substr($ccnum, -4, 4);
				}
			}
		} else {
			$fullcarddata = true;
			if($this->getDebug()) { 
				$logger->info("FullCardData002true: $fullcarddata"); 
			}
			$fields2 = preg_split('/-/',$ponum);
			if(($ponum!="") and ($CustomerProfileID == 0)) {
				if(isset($fields2[0]) and ((INT)$fields2[0]>10000000)) { 
					$CustomerProfileID=(INT) $fields2[0]; 
				}
			}
		}	
		//$ccnum=substr($ccnum,-4);
		//$ccnum='XXXX'.substr($ccnum, -4, 4);

		if($this->getDebug()) { 
			$logger->info("USING TOKEN FullCardData: $fullcarddata USING ccnum: $ccnum USING ponum: $ponum"); 
		}
		if($this->getDebug()) { 
			$logger->info("Account Values0-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); 
		}
		$authtransID = $payment->getOrder()->getTransactionId();
		if($authtransID < 1) { 
			$authtransID=$payment->getParentTransactionId();	
		}
		if($authtransID < 1) { 
			$authtransID=$payment->getCcTransId();	
		}
		$authtrans2 = preg_split ('/-/',$authtransID);
		$authtransID = $authtrans2[0];
		if((INT) $PaymentProfileID == 0) { 
			$forceupdate=true; 
		} else { 
			$forceupdate=false; 
		}

		if($CustomerProfileID > 0) { 
			$ExistingCustProfile=$CustomerProfileID; } else { $ExistingCustProfile=0; 
		}
		//if($CustomerProfileID==0) {
		if($this->getDebug()) { 
			$logger->info("Account Values1-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); 
		}
		if($fullcarddata) {
			// First try to create a Customer Profile
			$CustProfileXML = $this->createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $CustomerId, $billToWho, $cvv, $extra, $storeId);
			$response = $this->processRequest($url, $CustProfileXML, $CustomerEmail, $userAgent);
			$resultErrorCode = $this->parseXML('<code>','</code>',$response);
			// Get Customer and Payment Profile ID
			$CustomerProfileID = (int) $this->parseXML('<customerProfileId>','</customerProfileId>', $response);
			$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileIdList><numericString>','</numericString></customerPaymentProfileIdList>', $response);

			$resultText = $this->parseXML('<text>','</text>',$response);
			$resultCode = $this->parseXML('<resultCode>','</resultCode>',$response);
			if($resultErrorCode == 'E00039') {
				if($this->getDebug()) {		
					$logger->info("\n\n ALREADY HAVE A CUST PROFILE \n\n"); 
				}
				$split = preg_split('/ /',$resultText);
				$ExistingCustProfile = $split[5];
				if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) {
					 $ExistingCustProfile = $CustomerProfileID; 
				} else { 
					$CustomerProfileID = $ExistingCustProfile; 
				}
				if($this->getDebug()) { 
					$logger->info("Account Values2-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); 
				}
				if($this->getDebug()) {		
					$logger->info("\n\n Problem with CustomerProfileID \n\n"); 
				}
			}
			$addPaymentProfileXML = $this->addPaymentProfileXML($ExistingCustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra, $storeId);
				
			//<customerPaymentProfileIdList><numericString>
				
			$response = $this->processRequest($url, $addPaymentProfileXML, $CustomerEmail, $userAgent);
			$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileId>','</customerPaymentProfileId>', $response);
			$resultErrorCode = $this->parseXML('<code>','</code>',$response);
			if(substr($resultErrorCode,0,1) == 'E') {
				if($this->getDebug()) {		
					$logger->info("\n\n ALREADY HAVE A PAYMENT PROFILE WITH THE CARD $resultErrorCode \n\n"); 
				}
				//Get Correct PaymentProfileID
			}
		}
		if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) { 
			$ExistingCustProfile = $CustomerProfileID; 
		}
		if(($CustomerProfileID==0) AND ($ExistingCustProfile>0)) { 
			$CustomerProfileID = $ExistingCustProfile; 
		}
		if($PaymentProfileID=='0') {
			if($this->getDebug()) {		
				$logger->info("\n\n PAYMENT PROFILE NOT SET 1\n\n"); 
			}
			$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
			$responseGET = $this->processRequest($url, $getCustXML, $CustomerEmail, $userAgent);
			$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, false);
		}
		if($this->getDebug()) {	
			$logger->info("\n\n fullcarddata1: $fullcarddata  \n\n"); 
			if($fullcarddata) { 
				$logger->info("\n\n fullcarddata2: TRUE  \n\n"); 
			}
			if(!$fullcarddata) { 
				$logger->info("\n\n fullcarddata3: FALSE  \n\n"); 
			}
		}

		if($PaymentProfileID == '0') {
			if($this->getDebug()) {	
				$logger->info("\n\n PAYMENT PROFILE NOT SET 2\n\n"); 
			}
			if(!$fullcarddata) {
				$addPaymentProfileXML = $this->addPaymentProfileXML($ExistingCustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra, $storeId);
				$response = $this->processRequest($url, $addPaymentProfileXML, $CustomerEmail, $userAgent);
				$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileId>','</customerPaymentProfileId>', $response);
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				if(substr($resultErrorCode,0,1) == 'E') {
					if($this->getDebug()) {		
						$logger->info("\n\n ALREADY HAVE A PAYMENT PROFILE WITH THE CARD \n\n"); 
					}
					//Get Correct PaymentProfileID
					$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
					$responseGET = $this->processRequest($url, $getCustXML, $CustomerEmail, $userAgent);
					$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, true);
				}
			}
		}
		if($fullcarddata) {
			$updatePaymentProfileXML = $this->updatePaymentProfileXML($ExistingCustProfile, $PaymentProfileID, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra, $storeId);
			$response = $this->processRequest($url, $updatePaymentProfileXML, $CustomerEmail, $userAgent);
			 if($this->getDebug()) {		
				$logger->info("\n\n UPDATED PAYMENT PROFILE1 $PaymentProfileID \n\n"); 
			}

			$resultErrorCode = $this->parseXML('<resultCode>','</resultCode>',$response);
			if($resultErrorCode != 'Ok') { 
				// Using an existing card already
				if($this->getDebug()) {		
					$logger->info("\n\n UPDATING PAYMENT PROFILE1a $PaymentProfileID\n\n"); 
				}
				//Get Correct PaymentProfileID
				$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
				$responseGET = $this->processRequest($url, $getCustXML, $CustomerEmail, $userAgent);
				$PaymentProfileIDBack = $PaymentProfileID;
				$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, true);
				if($PaymentProfileID=='0') { 
					$PaymentProfileID = $PaymentProfileIDBack; 
				}
				if($forceupdate) {
					$updatePaymentProfileXML = $this->updatePaymentProfileXML($ExistingCustProfile, $PaymentProfileID, $ccnum, $ExpirationDate, $billToWho, $cvv, $extra, $storeId);
					$response = $this->processRequest($url, $updatePaymentProfileXML, $CustomerEmail, $userAgent);
					if($PaymentProfileID<10000) { 
						$PaymentProfileID = $PaymentProfileIDBack; 
					}
					if($this->getDebug()) {		
						$logger->info("\n\n UPDATED PAYMENT PROFILE2 $PaymentProfileID \n\n"); 
					}
				}
			}

			if($this->getDebug()) {	$logger->info("\n\n UPDATED PAYMENT PROFILE3 $PaymentProfileID \n\n"); }
			if($this->getDebug()) { $logger->info("Account Values4-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }
		}
		//}
		if($this->getDebug()) {		
			$logger->info("\n\n Shipping address detected \n\n"); 
		}
		$stw=$shipToWho['lastname'];
		if($this->getDebug()) {		
			$logger->info("\n\n Shipping $stw \n\n"); 
		}
		if((isset($shipToWho['lastname'])) and ($shipToWho['lastname']>'')) {
			if($this->getDebug()) {		
				$logger->info("\n\n Shipping address detected last: ".$shipToWho['lastname']." first: ".$shipToWho['lastname']."\n\n"); 
			}
			//Get Correct ShippingProfileID
			//$ExistingCustProfile=$CustomerProfileID;
			$getXML = $this->getProfileXML($CustomerProfileID, $storeId);
			$responseGET = $this->processRequest($url, $getXML, $CustomerEmail, $userAgent);
			if($this->getDebug()) {		
				$logger->info("\n\n responseGET $responseGET \n\n"); 
			}
			if($ShippingProfileID==0) {
				$ShippingProfileID = (INT) $this->parseMultiShippingXML($responseGET, $shipToWho, FALSE);
			}
			if($ShippingProfileID==0) {
				$createCustomerShippingAddressXML=$this->createCustomerShippingAddressXML($CustomerProfileID, $shipToWho, $extra, $storeId);
				$response = $this->processRequest($url, $createCustomerShippingAddressXML, $CustomerEmail, $userAgent);
				if($this->getDebug()) {		
					$logger->info("\n\n Shipping Address Response: $response\n\n"); 
				}
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				
				if(substr($resultErrorCode,0,1) == 'E') { 
				// Using an existing card already					
				//if($resultErrorCode == 'E00039') { 
					// Using an existing card already
					if($this->getDebug()) {		
						$logger->info("\n\n ALREADY HAVE A SHIPPING PROFILE WITH THE CARD \n\n responseGET: $responseGET"); 
					}
					$ShippingProfileID = (INT) $this->parseMultiShippingXML($responseGET, $shipToWho, TRUE);
				}
			}
			// if($ShippingProfileID==0) {
			// if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A SHIPPING PROFILE WITH THE CARD \n\n responseGET: $responseGET"); }
			// $ShippingProfileID = (INT) $this->parseMultiShippingXML($responseGET, $shipToWho, TRUE);
			// }
		}

		$TxRqXML = $this->createTransXML($amountcalled, $tax, $CustomerProfileID, $PaymentProfileID, $ShippingProfileID, $callby, $invoiceno, $authtransID, $Approval, $cvv, $extra, $storeId);
		$TxRqResponse = $this->processRequest($url, $TxRqXML, $CustomerEmail, $userAgent);

 		//$dumped = var_export($TxRqResponse, true);
 		//Mage::log("File: " . __FILE__ . " Function: " . __FUNCTION__ . " call_ " . $dumped, null, 'gateway.log');

		$resultText = $this->parseXML('<text>','</text>',$TxRqResponse);
		$resultCode = $this->parseXML('<resultCode>','</resultCode>',$TxRqResponse);
		$resultErrorCode = $this->parseXML('<code>','</code>',$TxRqResponse);
		$transauthidar = $this->parseXML('<directResponse>','</directResponse>',$TxRqResponse);

		$delimiter=",";
	 	$fieldsAU = preg_split("/$delimiter/",$transauthidar);
		$responsecode  = $fieldsAU[0];
		if(!$responsecode=="1") { 
			$resultCode="No";  
		}
		if(isset($fieldsAU[4])) { 
			$approval = $fieldsAU[4];
		} else {
			$approval = 0; 
		}
		if(strlen($approval)<6) { 
			$approval=$Approval; 
		}
		if(isset($fieldsAU[6])) { 
			$transno = $fieldsAU[6];
		} else {
			$transno = 0; 
		}
		if((isset($fieldsAU[5])) AND ($fieldsAU[5]>'')) { 
			$avscode = $fieldsAU[5];
		} else {
			$avscode = "N/A";
		}
		if((isset($fieldsAU[38])) AND ($fieldsAU[38]>'')) { 
			$cvvcode = $fieldsAU[38];
		} else {
			$cvvcode = "P";
		}

		if($this->getDebug()) { 
			$logger->info("0. USING customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID \n");
			$logger->info("1. USING resultText: $resultText - resultCode: $resultCode - resultErrorCode: $resultErrorCode \n");
			$logger->info("2. transauthidar: $transauthidar \n");
			$logger->info("3. avscode: $avscode cvvcode: $cvvcode \n"); 
			$logger->info("4. TransID = $transno \n");
			$logger->info("5. Approval Code = $approval \n");
		}		

		$paymentInfo = $this->getInfoInstance();
		$paymentInfo->setCcAvsStatus($avscode);
		$paymentInfo->setCcCidStatus($cvvcode);	
		if(($CustomerProfileID>'0') AND ($PaymentProfileID>'0')) {
			$token="$CustomerProfileID-$PaymentProfileID-$approval-$ShippingProfileID";
			$paymentInfo->setCybersourceToken($token);
			$paymentInfo->setPoNumber($token);
			$paymentInfo->getOrder()->setTransactionId($transno);
			$orderPaymentTable=Mage::getConfig()->getTablePrefix().'sales_flat_order_payment'; 
			$cprofile=$CustomerProfileID."-".$PaymentProfileID;
			if(($paymentInfo->getCcSsStartMonth()=="on") or ($paymentInfo->getCcSsStartMonth()=="1")) {
				$paymentInfo->setCcSsStartMonth('1'); 
				$cprofile=$CustomerProfileID."-".$PaymentProfileID;
				$sql = "UPDATE $orderPaymentTable p SET p.cc_ss_start_month=1 WHERE p.po_number LIKE '$cprofile%' AND p.method='authorizecimsoap'";
			} else {
				$paymentInfo->setCcSsStartMonth('0'); 
				$sql = "UPDATE $orderPaymentTable p SET p.cc_ss_start_month=0 WHERE p.po_number LIKE '$cprofile%' AND p.method='authorizecimsoap'";
			}
			$tries    = 0;
			$maxTries = 5;
        		do {
            			$retry = false;
            			try {
					$results = Mage::getSingleton('core/resource')->getConnection('core_write')->query($sql);
            			} catch (Exception $e) {
                			if($tries < $maxTries && strpos($e->getMessage(), 'Serialization failure: 1213 Deadlock found when trying to get lock') !== false) {
                    				$retry = true;
                			} else {
                    				// we tried at least maxTries times, go ahead and throw exception
                    				throw new Zend_Db_Statement_Exception($e->getMessage());
                			}
                			usleep($this->getDelay());
                			$tries++;
            			}
        		} while($retry);
		}
		
		$result['Status']['transno'] = $transno;
		$result['Status']['approval'] = $approval;
		$result['Status']['CustomerProfileID'] = $CustomerProfileID;
		$result['Status']['PaymentProfileID'] = $PaymentProfileID;
		$result['Status']['ShippingProfileID'] = $ShippingProfileID;
		$result['Status']['statusCode'] = $resultCode;
		$result['Status']['code'] = $resultErrorCode;
		$result['Status']['statusDescription'] = $resultText;
		$result['Status']['CcAvsStatus'] = $avscode;
		$result['Status']['CcCidStatus'] = $cvvcode;

		return $result;
	}
}
