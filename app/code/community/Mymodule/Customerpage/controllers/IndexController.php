<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Adesh
 * @package     Mymodule_Customerpage
 * @author      Adesh
 * @Website     adeshsuryan.in
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
 
class Mymodule_Customerpage_IndexController extends Mage_Core_Controller_Front_Action {
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }      
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function editAction()
    {
	
		$name=$this->getRequest()->getParam('new_name');
		$id=$this->getRequest()->getParam('id_change');
		 $atrwork_collection = Mage::getModel('customoptions/artwork')->load($id);
		 $atrwork_collection->setSaveName($name);
		 $atrwork_collection->save();
		 Mage::getSingleton('core/session')->addSuccess("Your artwork has been renamed.");
		  $this->_redirect('*/*/');
		 
	
    }
    
    public function deleteAction()
    {
         $data = Mage::app()->getRequest()->getParams();
         echo $id=$this->getRequest()->getParam('delId');
		 $atrwork_collection = Mage::getModel('customoptions/artwork')->load($id);
		 /*echo "<pre>";
		 print_r($atrwork_collection->getData());
		 exit; */
		 //$atrwork_collection->printLogQuery(true); exit;
		 try {
			 $atrwork_collection->delete();
			 Mage::getSingleton('core/session')->addSuccess("Your artwork has been deleted successfully");
			 $this->_redirect('*/*/');

		} catch (Exception $e){
			echo $e->getMessage(); 
		}
		 
		
    }
    
    public function newartAction()
    {
		
		/*echo "<pre>";
	print_r($this->getRequest()->getParams());
		print_r($_FILES);
		exit;*/
		echo $customerId = $_POST['customerId'];
        $productId = $_POST['prId'];
       echo "save->".$savingName = $_POST['sname'];


        $customOptionModel = Mage::getModel('customoptions/artwork');


        try {

            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $path = Mage::getBaseDir('media') . DS . 'artwork' . DS;
            $imgName = $_FILES['image']['name'];
            $path_parts = pathinfo($imgName);
            $image_path = $path_parts['filename'] . '_' . time() . '.' . $path_parts['extension']; 
            $uploader->save($path, $image_path);

        //    $customOptionModel->setProductId($productId);
            $customOptionModel->setCustomerId($customerId);
            $customOptionModel->setSaveName($savingName);
            $customOptionModel->setImgPath($image_path);
            $customOptionModel->save();
        } catch (Exception $e) {
            $result = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }
		
				 Mage::getSingleton('core/session')->addSuccess("Your artwork has been saved.");
		  $this->_redirect('*/*/');
	}
    
    
    
    
    
    
}  
