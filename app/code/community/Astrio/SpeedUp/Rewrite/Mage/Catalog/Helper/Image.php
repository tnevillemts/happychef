<?php
/**
 * Astrio
 *
 * @category Astrio
 * @package Astrio_SpeedUp
 * @author Eldaniz Gasymov <e.gasymov@astrio.net>
 */
class Astrio_SpeedUp_Rewrite_Mage_Catalog_Helper_Image extends Amasty_Shopby_Helper_Image
{
    protected $_filterAttributes = null;

    public function getFilterAttributes()
    {
        if ($this->_filterAttributes === null) {
            $filterAttributes = array();
            $configurableCodes = trim(Mage::getStoreConfig('amshopby/general/configurable_images'));
            if ($configurableCodes) {
                $requestParams = $this->_getRequest()->getParams();
                if ($requestParams) {
                    $configurableCodes = explode(",", trim($configurableCodes));
                    $filterAttributeCodes = array_intersect($configurableCodes, array_keys($requestParams));
                    /**
                     * @var $eavConfig Mage_Eav_Model_Config
                     */
                    $eavConfig = Mage::getSingleton('eav/config');
                    foreach ($filterAttributeCodes as $attributeCode) {
                        $attribute = $eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
                        if ($attribute instanceof Mage_Eav_Model_Entity_Attribute_Abstract) {
                            if ($attribute->getData('is_configurable')) {
                                $filterAttributes[$attribute->getAttributeCode()] = $attribute;
                            }
                        }
                    }
                }
            }

            $this->_filterAttributes = $filterAttributes;
        }
        return $this->_filterAttributes;
    }

    public function setProduct($product)
    {
        if ($product instanceof Mage_Catalog_Model_Product && $product->isConfigurable()) {
            $filterAttributes = $this->getFilterAttributes();
            if ($filterAttributes && $product->isSaleable()) {
                /**
                 * @var $collection Mage_Catalog_Model_Resource_Product_Collection
                 */
                $requestParams = $this->_getRequest()->getParams();

                $collection = Mage::getResourceModel('catalog/product_collection');
                $collection->setStoreId($product->getStoreId());
                $collection->joinField(
                    'link_id',
                    'catalog/product_super_link',
                    'link_id',
                    'product_id=entity_id',
                    '{{table}}.parent_id=' . (int) $product->getId(),
                    'inner'
                );
                $collection->addAttributeToSelect(array('image', 'small_image', 'thumbnail'));

                $collection->setPageSize(1);

                foreach ($filterAttributes as $attributeCode => $attribute) {
                    $value = $requestParams[$attributeCode];
                    if (strpos($value, ",") !== false) {
                        $filterExpr = array('in', explode(",", $value));
                    } else {
                        $filterExpr = $value;
                    }
                    $collection->addAttributeToFilter($attributeCode, $filterExpr);
                }

                if (count($collection) > 0) {
                    $product = $collection->getFirstItem();
                }
            }
        }

        return Mage_Catalog_Helper_Image::setProduct($product);
    }
}
