<?php
/**
 * Astrio
 *
 * @category Astrio
 * @package Astrio_SpeedUp
 * @author Eldaniz Gasymov <e.gasymov@astrio.net>
 */
class Astrio_SpeedUp_Rewrite_Mage_Catalog_Model_Product_Type_Configurable
    extends Mage_Catalog_Model_Product_Type_Configurable
{
    /**
     * Check is product available for sale
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isSalable($product = null)
    {
        $salable = Mage_Catalog_Model_Product_Type_Abstract::isSalable($product);

        if ($salable !== false) {
            $salable = false;
            if (!is_null($product)) {
                $this->setStoreFilter($product->getStoreId(), $product);
            }
            if ($product && !Mage::app()->getStore()->isAdmin()) {
                /**
                 * @var $collection Mage_Catalog_Model_Resource_Product_Type_Configurable_Product_Collection
                 */
                $collection = $this->getUsedProductCollection($product)
                    ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
                $salable = $collection->getSize() > 0;
            } else {
                foreach ($this->getUsedProducts(null, $product) as $child) {
                    if ($child->isSalable()) {
                        $salable = true;
                        break;
                    }
                }
            }
        }
        return $salable;
    }

}