<?php
/**
 * Astrio
 *
 * @category Astrio
 * @package Astrio_SpeedUp
 * @author Eldaniz Gasymov <e.gasymov@astrio.net>
 */
class Astrio_SpeedUp_Rewrite_Mage_Eav_Model_Entity_Attribute_Source_Table
    extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    static $_staticOptions          = array();
    static $_staticOptionsDefault   = array();

    protected function _getOptions($attributeId, $storeId, $defaultValues = false)
    {
        if (!isset(self::$_staticOptions[$attributeId][$storeId])) {
            /**
             * @var $collection Mage_Eav_Model_Resource_Entity_Attribute_Option_Collection
             */
            $collection = Mage::getResourceModel('eav/entity_attribute_option_collection');
            $collection
                ->setAttributeFilter($attributeId)
                ->setStoreFilter($storeId)
            ;

            $collection->getSelect()
                ->reset(Zend_Db_Select::ORDER)
                ->order('main_table.sort_order ' . Zend_Db_Select::SQL_ASC)
                ->order('value ' . Zend_Db_Select::SQL_ASC)
            ;

            $stmt = $collection->getConnection()->query($collection->getSelect());

            $options = array();
            $optionsDefault = array();
            while($row = $stmt->fetch()) {
                $options[] = array(
                    'value'     => $row['option_id'],
                    'label'     => $row['value'],
                );
                $optionsDefault[] = array(
                    'value'     => $row['option_id'],
                    'label'     => $row['default_value'],
                );
            }

            self::$_staticOptions[$attributeId][$storeId] = $options;
            self::$_staticOptionsDefault[$attributeId][$storeId] = $optionsDefault;
        }

        return $defaultValues ? self::$_staticOptionsDefault[$attributeId][$storeId] : self::$_staticOptions[$attributeId][$storeId];
    }

    /**
     * Retrieve Full Option values array
     *
     * @param bool $withEmpty       Add empty option to array
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        $attributeId    = (int) $this->getAttribute()->getId();
        $storeId        = (int) $this->getAttribute()->getStoreId();

        if (!$withEmpty) {
            return $this->_getOptions($attributeId, $storeId, $defaultValues);
        }

        $options = $this->_getOptions($attributeId, $storeId, $defaultValues);
        array_unshift($options, array('label' => '', 'value' => ''));
        return $options;
    }

}