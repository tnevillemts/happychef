<?php
/**
 * @methods:
 * - get[Section]_[ConfigName]($defaultValue = '')
 */
class EM_Em0080settings_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $_variationVars = array();
	public function __call($name, $args) {
		if (method_exists($this, $name))
			call_user_func_array(array($this, $name), $args);
			
		elseif (preg_match('/^get([^_][a-zA-Z0-9_]+)$/', $name, $m)) {
			$segs = explode('_', $m[1]);
			foreach ($segs as $i => $seg)
				$segs[$i] = strtolower(preg_replace('/([^A-Z])([A-Z])/', '$1_$2', $seg));

			$value = Mage::getStoreConfig('em0080/'.implode('/', $segs));
			if (!$value) $value = @$args[0];
			return $value;
		}
		
		else 
			call_user_func_array(array($this, $name), $args);
	}
	
	public function getAllCssConfig() {
		$header_bgfile = Mage::getStoreConfig('em0080/typography/header_bgfile') ? 
			 (Mage::getBaseUrl('media') . 'background/' . Mage::getStoreConfig('em0080/typography/header_bgfile')) : '';
			
		$body_bgfile = Mage::getStoreConfig('em0080/typography/body_bgfile') ? 
			(Mage::getBaseUrl('media') . 'background/' . Mage::getStoreConfig('em0080/typography/body_bgfile')) : '';

		$footer_bgfile = Mage::getStoreConfig('em0080/typography/footer_bgfile') ? 
			(Mage::getBaseUrl('media') . 'background/' . Mage::getStoreConfig('em0080/typography/footer_bgfile')) : '';

		$typoArray = require_once(Mage::getSingleton('core/design_package')->getSkinBaseDir().DS.'css'.DS.'variation.php');	
		$configs = array();
		$quoteChars = array('general_font','h1_font','h2_font','h3_font','h4_font','h5_font','h6_font','header_bgimage','body_bgimage','footer_bgimage','image_url','img_pattern','header_bgfile','body_bgfile','footer_bgfile');
		$variationString = '';
		foreach($typoArray as $typo=> $value){
			$configValue = Mage::getStoreConfig('em0080/typography/'.$typo) ? Mage::getStoreConfig('em0080/typography/'.$typo) : $value;
			if(in_array($typo,$quoteChars)){
				$tmp = str_replace('"',"\'",$configValue);
				//$tmp = str_replace('" ','\" ',$configValue);
				$this->_variationVars[] = "'@{$typo}':'\"$tmp\"'";
				$configValue = str_replace('"',"'",$configValue);
				$configs["@{$typo}"] = "\"$configValue\"";
			}
			else{	
				$configs["@{$typo}"] = "{$configValue}";
				$this->_variationVars[] = "'@{$typo}':'$configValue'";
			}	
			
		}
		
		if($additionalCssFilesString = Mage::getStoreConfig('em0080/typography/additional_css_file')){
			$configs['custom_css_files'] = $additionalCssFilesString;
		}
		$configs['@header_bgfile'] = "\"{$header_bgfile}\"";
		$configs['@body_bgfile'] = "\"{$body_bgfile}\"";
		$configs['@footer_bgfile'] = "\"{$footer_bgfile}\"";
		$imageUrl = Mage::getDesign()->getSkinUrl('images');
		$imgPattern = $imageUrl.'/stripes';
		$configs['@image_url'] = "\"{$imageUrl}\"";
		$configs['@img_pattern'] = "\"{$imgPattern}\"";
		return $configs;
	}
	
	public function getImageBackgroundColor() {
		$color = Mage::getStoreConfig('em0080/general/image_bgcolor');
		if (!$color) $color = '#ffffff';
		$color = str_replace('#', '', $color);
		return array(
			hexdec(substr($color, 0, 2)),
			hexdec(substr($color, 2, 2)),
			hexdec(substr($color, 4, 2))
		);
	}
	
	public function getVarationVars(){
		return implode(',',$this->_variationVars);
	}
	
    public function getCategoriesCustom($parent,$curId){
				
		$result = '';
		if($parent->getLevel() == 1)
			$result = "<option value='0'>".$this->getCatNameCustom($parent)."</option>";
		else{
			$result = "<option value='".$parent->getId()."' ";
			
			if($curId){
				if($curId	==	$parent->getId()) $result .= " selected='selected'";
			}
			$result .= ">".$this->getCatNameCustom($parent)."</option>";			
		}
		
		try{
			$children = $parent->getChildrenCategories();
			
			if(count($children) > 0){
				foreach($children as $cat){
					$result .= $this->getCategoriesCustom($cat,$curId);
				}
			}
		}
		catch(Exception $e){
			return '';
		}
		return $result;
	}
	
	public function getCatNameCustom($category){
		$level = $category->getLevel();
		$html = '';
		for($i = 0;$i < $level;$i++){
			$html .= '&mdash;&ndash;';
		}
		if($level == 1)	return $html.' '.$this->__("All Categories");
		else return $html.' '.$category->getName();
	}
	
	public function insertStaticBlock($dataBlock) {
		// insert a block to db if not exists
		$block = Mage::getModel('cms/block')->getCollection()->addFieldToFilter('identifier', $dataBlock['identifier'])->getFirstItem();
		if (!$block->getId())
			$block->setData($dataBlock)->save();
		return $block;
	}
	
	public function insertPage($dataPage) {
		$page = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', $dataPage['identifier'])->getFirstItem();
		if (!$page->getId())
			$page->setData($dataPage)->save();
		return $page;
	}

    public function checkMobilePhp() {
		require_once(Mage::getBaseDir('lib') . DS . 'em/Mobile_Detect.php');
		$detect = new Mobile_Detect();
        $checkmobile = $detect->isMobile();
        $checktablet = $detect->isTablet();
        if($checkmobile){
            if($checktablet){
                return false;
            }else{
                return true;
            }
            
        }else{
            return false;
        }
	}
	
	public function isShowOfferPrice($productPrice){
		if(!Mage::registry('current_product'))
			return false;
		return Mage::registry('current_product')->getId() == $productPrice->getId();
	}
    
	public function getHomeUrl() {
        return array(
            "label" => $this->__('Home'),
            "title" => $this->__('Home Page'),
            "link" => Mage::getUrl('')
        );
    }
	
	public function importAdditionalCssFiles(){
		$cssHtml = '';
		if($additionalCssFilesString = Mage::getStoreConfig('em0080/typography/additional_css_file')){
			$additionalCssFiles = explode(',',$additionalCssFilesString);
			foreach($additionalCssFiles as $filename){
				$ext = end(explode('.',$filename));
				$cssHtml .= '<link ';
				if($ext == 'less'){
					$cssHtml .= 'rel="stylesheet/less" ';
				}
				$cssHtml .= 'type="text/css" href="'.Mage::getDesign()->getSkinUrl("css/$filename").'" media="all"/>';
			}
		}
		return $cssHtml;
	}	
	
	public function getActionReview(){
		$url = Mage::helper('core/url')->getCurrentUrl();
		$url_check = 'wishlist/index/configure';
		if(stripos($url,$url_check)):
			$id = Mage::registry('current_product')->getId();
			return Mage::getUrl('review/product/post/', array('id' => $id));
		else:
			$productId = Mage::app()->getRequest()->getParam('id', false);
        	return Mage::getUrl('review/product/post', array('id' => $productId));
		endif;
	}
}
