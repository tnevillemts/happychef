<?php
 
class EM_Em0080settings_Model_Config_Boxwide
{
	public function toOptionArray()
    {
		return array(
			array('label' => "Box", 'value' => "box"),
			array('label' => "Wide", 'value' => "wide")
		);
    }
 
}
