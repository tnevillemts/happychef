<?php

$installer = $this;

$installer->startSetup();

$block = Mage::getModel('cms/block');
$page = Mage::getModel('cms/page');
$stores = array(0);
####################################################################################################
# CREATE STATIC BLOCK
####################################################################################################
$dataBlock = array(
	'title' => 'EM0080 Shopping Information News Letter Footer - Area14',
	'identifier' => 'em0080_information_newsletter_footer',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="grid_8 category_link_ad alpha">{{block type="newsletter/subscribe" name="left.newsletter" template="newsletter/subscribe.phtml"}}</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Cookie restriction notice',
	'identifier' => 'cookie_restriction_notice_block_happychef',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<p>This website requires cookies to provide all of its features. For more information on what data is contained in the cookies, please see our <a href="{{store direct_url="privacy-policy-cookie-restriction-mode"}}">Privacy Policy page</a>. To accept cookies from this site, please click the Allow button below.</p>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Footer',
	'identifier' => 'happychef-footer',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="footer-wrapper-new">
<div class="top-footer">
<div class="same happychef">
<div class="title">Happy Chef</div>
<ul class="content">
<li class="first"><a href="{{store direct_url="about-us"}}">About Happy Chef </a></li>
<li><a href="{{store direct_url="privacy-policy-cookie-restriction-mode"}}">Privacy &amp; Security</a></li>
<li class="last"><a href="{{store direct_url="catalog/seo_sitemap/category"}}">Sitemap </a></li>
</ul>
</div>
<div class="same shopping">
<div class="title">Shopping</div>
<ul class="content">
<li class="first"><a href="{{store direct_url="topseller"}}">Top Sellers</a></li>
<li><a href="{{store direct_url="customization"}}">Customization </a></li>
<li><a href="{{store direct_url="sizechartfooter"}}">Size Charts </a></li>
<li><a href="{{store direct_url="sale"}}">Sale</a></li>
<li><a href="{{store direct_url="gift_card"}}">Gift Cards</a></li>
<li class="last"><a href="{{store direct_url="requestcatalog"}}">Request Catalog</a></li>
<li class="last"><a href="{{store direct_url="outlet-store-happychef"}}">Outlet Store</a></li>
</ul>
</div>
<div class="same customer">
<div class="title">Customer Service</div>
<ul class="content">
<li class="first"><a href="{{store direct_url="shipping-delivery"}}">Shipping &amp; Delivery </a></li>
<li><a href="{{store direct_url="returns-exchanges"}}">Returns &amp; Exchanges </a></li>
<li><a href="{{store direct_url="payment-pricing-promos"}}">Payment &amp; Promotions </a></li>
<li class="last"><a href="{{store direct_url="order-status-happychef "}}">Order Status </a></li>
<li class="last"><a href="{{store direct_url="customer/account/login/"}}">Account Information </a></li>
<li class="last"><a href="{{store direct_url="contact-us"}}">Contact Us</a></li>
</ul>
</div>
<div class="same social">
<div class="title">We are Social</div>
<div class="icon-social"><a href="https://www.facebook.com/happychef" target="_blank"><img src="{{media url="wysiwyg/facebook.jpg"}}" alt="" /></a> <a href="https://twitter.com/HappyChef" target="_blank"><img src="{{media url="wysiwyg/twitter.jpg"}}" alt="" /></a> <a href="https://plus.google.com/101415755986397710251/posts" target="_blank"><img src="{{media url="wysiwyg/google.jpg"}}" alt="" /></a> <a href="http://www.pinterest.com/happychef/" target="_blank"><img src="{{media url="wysiwyg/printer.jpg"}}" alt="" /></a> <a class="youtube" href="https://www.youtube.com/user/HappyChefUniforms" target="_blank"><img src="{{media url="wysiwyg/ytb.jpg"}}" alt="" /></a></div>
<div class="newsletter-new">{{block type="newsletter/subscribe" name="left.newsletter" template="newsletter/subscribe.phtml"}}</div>
</div>
</div>
<div class="bottom-footer">Customer Service (800) 347-0288 is available from 9am to 5pm EST, Monday through Friday. Use our <a href="{{store direct_url="contact-us"}}"> online contact form </a>to send us a message during all other times Copyright &copy;2002-2014 The Happy Chef&reg;, Inc. All rights reserved. Happy Chef, CookCool, and #SMART are trademarks of The Happy Chef, Inc.</div>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Homepage_3-column_a',
	'identifier' => 'Homepage_3-column_a',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="scf">
<div class="top-image"><a href="#"><img src="{{media url="wysiwyg/b1.jpg"}}" alt="" /></a></div>
<div class="block-title">Embroidery ACF</div>
<div class="block-content"><a href="#">See More Products</a></div>
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Homepage_2-column_a',
	'identifier' => 'Homepage_2-column_a',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div class="homepage-2column">
<div id="homepage-twocol-col1" class="homepage-2column-col1">
<div class="part-top">
<div class="image-left"><a href="#"><img src="{{media url="wysiwyg/cookcool.png"}}" alt="" /></a></div>
<div class="text-right">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit sit ame</div>
</div>
<div class="part-bottom"><a href="#">Call to action</a></div>
</div>
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef MegaMenu More Featuredcategory',
	'identifier' => 'happychef_megamenu_more_featuredcategory',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div class="title-more">FEATURED CATEGORY</div>
<div class="more-content">Explanatory copy promotes the remaining categories, and encourages user to choose one import ad minim veniam, quis nostrud exercitation. Phasellus laoreet lorem vel doctor tempus vehic ula ad minim veniam, exercitation..</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Logo Menu',
	'identifier' => 'happychef-logo-menu',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div><a class="logo-1" href="{{store direct_url="cook-cool.html"}}"><img src="{{media url="wysiwyg/logo-cook.png"}}" alt="" /></a> <a class="logo-2" href="{{store direct_url="smart.html"}}"><img src="{{media url="wysiwyg/logo-smart.png"}}" alt="" /></a></div>

EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'Header-Special-Message',
	'identifier' => 'header-special-message',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="free-shipping">NOW SHIPPING INTERNATIONALLY</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Phone',
	'identifier' => 'happychef-phone',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<p>+1.800-347-0288</p>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Category',
	'identifier' => 'happychef_header_banner_category',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div class="block-categories">
<div class="title"><span>Athletic</span><span>perfprmance</span></div>
<p>Coolcool Apparel Controls the Heat</p>
<div class="menu-block-cate">
<ul>
<li class="first"></li>
<li><a href="#">Coats</a></li>
<li><a href="#">Pants</a></li>
<li><a href="#">Hats</a></li>
<li><a href="#">Lear more</a></li>
<li class="last"></li>
</ul>
</div>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Sign Up Banner Area 16',
	'identifier' => 'happychef_signup_banner_area16',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<h3>Sign Up Catalog</h3>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Imagies Swatches',
	'identifier' => 'happychef_imagies_swatches',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div class="block-categories">
<div class="title"><span>Athletic</span><span>performance</span></div>
<p>Cookcool Apparel Controls the Heat</p>
<div class="menu-block-cate">
<ul>
<li class="first"></li>
<li class="one"><a href="#">Coats</a></li>
<li><a href="#">Pants</a></li>
<li><a href="#">Hats</a></li>
<li><a href="#">Learn more</a></li>
<li class="last"></li>
</ul>
</div>
</div>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'Happychef Imagies More Megamenu',
	'identifier' => 'happychef_imagies_more_megamenu',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="row-one">
<div class="item first"><a href="{{store direct_url="more-4/formal.html"}}"><img src="{{media url="wysiwyg/menu_normal.jpg"}}" alt="" />
<h3 class="title-more-content">FORMAL</h3></a>
</div>
<div class="item second"><a href="{{store direct_url="more-4/scrub.html"}}"><img src="{{media url="wysiwyg/menu_scrubs.jpg"}}" alt="" />
<h3 class="title-more-content">SCRUBS</h3></a>
</div>
<div class="item third"><a href="{{store direct_url="more-4/linevns.html"}}"><img src="{{media url="wysiwyg/menu_linevns.jpg"}}" alt="" />
<h3 class="title-more-content">LINENS &amp; SKIRTING</h3></a>
</div>
<div class="item fourth"><a href="{{store direct_url="more-4/shoe.html"}}"><img src="{{media url="wysiwyg/menu_shoes.jpg"}}" alt="" />
<h3 class="title-more-content">SHOES</h3></a>
</div>
</div>
<div class="row-two">
<div class="item fifth"><a href="{{store direct_url="more-4/kitchentool.html"}}"><img src="{{media url="wysiwyg/menu_kichentool.jpg"}}" alt="" />
<h3 class="title-more-content">KITCHEN TOOLS</h3></a>
</div>
<div class="item sixth"><a href="{{store direct_url="more-4/coordinating.html"}}"><img src="{{media url="wysiwyg/menu_coor.jpg"}}" alt="" />
<h3 class="title-more-content">COORDINATING</h3></a>
</div>
<div class="item seventh"><a href="{{store direct_url="more-4/uniform.html"}}"><img src="{{media url="wysiwyg/menu_uniform.jpg"}}" alt="" />
<h3 class="title-more-content">COMPLETE UNIFORMS</h3></a>
</div>
<div class="item last"><a href="{{store direct_url="more-4/chefgift.html"}}"><img src="{{media url="wysiwyg/menu_gift.jpg"}}" alt="" />
<h3 class="title-more-content">CHEF GIFTS</h3></a>
</div>
</div>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'Happychef Product Detail Email',
	'identifier' => 'happychef_product_detail_email',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<p>{{config path="trans_email/ident_general/email"}}</p>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'HappyChef Home Featured',
	'identifier' => 'happychef_home_featured',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="happychef_home_featured">{{block type="catalog/product_list" name="home.product.featured" alias="products_featured" category_id="61" template="catalog/product/listhome.phtml"}}</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Homepage_2-column_b',
	'identifier' => 'Homepage_2-column_b',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="homepage-2column">
<div class="smart" id="homepage-twocol-col2">
<div class="part-top">
<div class="image-left"><a href="#"><img src="{{media url="wysiwyg/smart.png"}}" alt="" /></a></div>
<div class="text-right">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit sit ame</div>
</div>
<div class="part-bottom"><a href="#">Call to action</a></div>
</div>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Homepage_3-column_b',
	'identifier' => 'Homepage_3-column_b',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
		<div class="special" id="homepage_3column_b">
	<div class="top-image"><a href="#"><img src="{{media url="wysiwyg/b2.jpg"}}" alt="" /></a></div>
	<div class="block-title">Specials</div>
	<div class="block-content"><a href="#">See More Products</a></div>
	</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Homepage_3-column_c',
	'identifier' => 'Homepage_3-column_c',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="colour" id="homepage_3column_c">
<div class="top-image"><a href="#"><img src="{{media url="wysiwyg/b3.jpg"}}" alt="" /></a></div>
<div class="block-title">Color Coordinations</div>
<div class="block-content"><a href="#">See More Products</a></div>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Category Header Test - Coats',
	'identifier' => 'CatHead_Coats',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div class="cathead-coat" style="height: 300px; background-color: blue; color: white;">
<h1>Dave's Test - This is a static block - used in the Category Design tab with the setting: Use Static Block and Products</h1>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Shopcart Logo',
	'identifier' => 'happychef-shopcart-logo',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div class="logo-cart">
<div class="logo1"><img src="{{media url="wysiwyg/logo2-cart.jpg"}}" alt="" /></div>
<div class="logo2"><img src="{{media url="wysiwyg/logo1-cart.jpg"}}" alt="" /></div>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Headwear',
	'identifier' => 'happychef_header_banner_headwear',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Hats-header.jpg'}});background-position:center;" >

<div class="align-ctr" style="margin-right:125px;padding-top:165px;"><!-- COPY WRAP -->

<div class="a-spot-head">CROWD PLEASERS</div>
<div class="a-spot-subhead">Hat Styles They'll Love</div>

<div class="a-spot-btn-wrap">
<ul>
<li><a class="a-spot-btn float-lt">Skull Caps</a></li>
<li><a class="a-spot-btn float-lt">Chef Hats</a></li>
<li><a class="a-spot-btn float-lt">Bandanas</a></li>
<li><a class="a-spot-btn float-lt">Caps</a></li>
</ul>
</div>
</div>

</div> <!-- END COPY -->

</div>
<!-- END A-SPOT -->
</div> <!-- END CUSTOM DIV -->
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Shirts',
	'identifier' => 'happychef_header_banner_shirts',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div id="shirts-banner" class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/shirt-header.jpg'}});" >

<div class="align-lt floatFix float-lt" style="padding-top:100px;width:550px;padding-left:30px;"> <!-- COPY WRAP -->

<div class="a-spot-head">SOMETHING<br />FOR EVERYONE</div>

<div class="a-spot-btn-wrap">
<a class="a-spot-btn float-lt">Kitchen Shirts</a>
<a class="a-spot-btn float-lt">T-Shirts</a>
<a class="a-spot-btn float-lt">Staff Shirts</a>
</div>


</div> <!-- END COPY WRAP -->

</div>
<!-- END A-SPOT -->
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Coats',
	'identifier' => 'happychef_header_banner_coats',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div id="coats-banner" class="a-spot-general floatFix" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/coats-banner.jpg'}});background-position:center;" > <!-- A-SPOT -->
<div class="copywrap floatFix"> <!-- COPY WRAP -->
<div class="a-spot-head">PICK A NEW<br />FAVORITE</div>
<div class="a-spot-subhead">The Right Coat, Right Here</div>

<div class="a-spot-btn-wrap float-lt"> <!-- BTN WRAP -->

<div style="float:left;">
<a class="a-spot-btn float-lt">Style</a>
<a class="a-spot-btn float-lt">Sleeve</a>
<a class="a-spot-btn float-lt">Gender</a>
<a class="a-spot-btn float-lt">Color</a>
</div>

</div> <!-- BTN WRAP -->
</div> <!-- /COPY WRAP -->
</div>
<!-- END A-SPOT -->
</div>
<div class="clear"></div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Formal',
	'identifier' => 'happychef_header_banner_formal',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Formal.jpg'}});background-position:center right;" ></div>
<!-- END A-SPOT -->
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Sales',
	'identifier' => 'happychef_header_banner_sales',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:245px;background-image:url({{media url='wysiwyg/a-spot-imgs/Sales.jpg'}});background-position:center;" >

<div class="align-ctr" style="padding-top:54px;"> <!-- COPY WRAP -->
<div class="a-spot-head">THAT'S MORE LIKE IT</div>
<div class="a-spot-subhead">Save Big With Our Latest Specials</div>

<div class="a-spot-btn-wrap">
<ul>
<li><a class="a-spot-btn float-lt">Aprons</a></li>
<li><a class="a-spot-btn float-lt">Coats</a></li>
<li><a class="a-spot-btn float-lt">Pants</a></li>
<li><a class="a-spot-btn float-lt">Hats</a></li>
<li><a class="a-spot-btn float-lt">Cutlery</a></li>
</ul>
</div>

</div> <!-- END COPY WRAP -->
</div>
<!-- END A-SPOT -->
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner New Products',
	'identifier' => 'happychef_header_banner_new_products',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:245px;background-image:url({{media url='wysiwyg/a-spot-imgs/New-Products.jpg'}});background-position:center;" >

<div class="align-ctr" style="padding-top:54px;">
<div class="a-spot-head">SOME FRESH IDEAS</div>
<div class="a-spot-subhead">Check Out Our Newest Styles</div>

<div class="a-spot-btn-wrap floatFix">
<ul>
<li><a class="a-spot-btn float-lt">Aprons</a></li>
<li><a class="a-spot-btn float-lt">Coats</a></li>
<li><a class="a-spot-btn float-lt">Pants</a></li>
<li><a class="a-spot-btn float-lt">Hats</a></li>
<ul>
</div>

</div>
</div>
<!-- END A-SPOT -->
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Coordinating',
	'identifier' =>'happychef_header_banner_coordinating',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Coordinating-header.jpg'}});background-position:center;" >

<div class="align-rt" style="padding-right:30px;padding-top:50px;"> <!-- COPY WRAP -->
<div class="a-spot-head">BETTER<br />TOGETHER</div>
<div class="a-spot-subhead">Coordinating Looks to<br />Match Your Team</div>

<div class="a-spot-btn-wrap float-rt">
<a class="a-spot-btn float-lt">Gender</a>
<a class="a-spot-btn float-lt">Color</a>
</div>
</div> <!-- END COPY WRAP -->

</div>
<!-- END A-SPOT -->
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Women',
	'identifier' => 'happychef_header_banner_women',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div id="womens-banner" class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Womens-header.jpg'}});" >

<div class="align-rt copywrap"> <!-- COPY WRAP -->
<div class="a-spot-head">FIND<br />YOUR FIT</div>
<div class="a-spot-subhead">Women's Professional Styles</div>

<div class="a-spot-btn-wrap float-rt">
<a class="a-spot-btn float-lt">Coats</a>
<a class="a-spot-btn float-lt">Pants</a>
<a class="a-spot-btn float-lt">Staff Shirts</a>
<a class="a-spot-btn float-lt">Formal</a>
</div>
</div> <!-- END COPY WRAP -->

</div>
<!-- END A-SPOT -->
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Aprons',
	'identifier' => 'happychef_header_banner_aprons',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Aprons-header.jpg'}});background-position:center;" >
<div class="align-rt floatFix gradient-lt float-rt" style="margin-top:75px;"> <!-- COPY WRAP -->
<div class="a-spot-head">BRINGING IT<br />TOGETHER</div>
<div class="a-spot-subhead">Aprons for Everyone on Staff</div>

<div class="a-spot-btn-wrap float-rt"> <!-- BTN WRAP -->
<a class="a-spot-btn float-lt">Bib</a>
<a class="a-spot-btn float-lt">Bistro</a>
<a class="a-spot-btn float-lt">Kitchen</a>
<a class="a-spot-btn float-lt">Waist</a>
</div>


</div> <!-- END COPY WRAP -->
</div>
<!-- END A-SPOT -->
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Staff Shirts',
	'identifier' =>'happychef_header_banner_staff_shirts',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div id="staff-shirts-banner" class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Staff-shirt-header.jpg'}});" >
<div style="padding-left:30px;padding-top:125px;">

<div class="a-spot-head">THE RIGHT IMPRESSION</div>
<div class="a-spot-subhead">Shirts That Show Off Your Staff</div>

<div class="a-spot-btn-wrap">
<a class="a-spot-btn float-lt">Style</a>
<a class="a-spot-btn float-lt">Gender</a>
<a class="a-spot-btn float-lt">Sleeve</a>
</div>


</div>

</div>
<!-- END A-SPOT -->
</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Pants',
	'identifier' => 'happychef_header_banner_pants',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/pants-header.jpg'}});background-position:center;" > <!-- A-SPOT -->

<div class="align-rt" style="padding-right:30px;padding-top:110px;"> <!-- COPY WRAP -->
<div class="a-spot-head">TWO STEPS FORWARD</div>
<div class="a-spot-subhead">Pants That Make Your Day Easier</div>

<div class="a-spot-btn-wrap float-rt">
<a class="a-spot-btn float-lt">Style</a>
<a class="a-spot-btn float-lt">Fabric</a>
<a class="a-spot-btn float-lt">Waist</a>
</div>
</div> <!-- END COPY WRAP -->

</div>
<!-- END A-SPOT -->
</div>


EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Happychef Header Banner Shoes',
	'identifier' => 'happychef_header_banner_shoes',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Shoes-header.jpg'}});background-position:center;" > <!-- A-SPOT -->
<div class="align-rt" style="padding-right:30px;padding-top:110px;"> <!-- COPY WRAP -->
<div class="a-spot-head">GET FEET HAPPY</div>
<div class="a-spot-subhead">With Our Lightweight Classic Clogs</div>

<div class="a-spot-btn-wrap float-rt">
<a class="a-spot-btn float-lt">SHOP NOW</a>
</div>
</div> <!-- END COPY WRAP -->

</div>
<!-- END A-SPOT -->
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'EM0080-Featurette-Product-Detail',
	'identifier' => 'em0080-featurette-productdetail',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<div class="block-feature-product">
Featurette static block
</div>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'HappyChef Header Banner Cutlery',
	'identifier' => 'happychef_header_banner_cutlery',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">

<!-- A-SPOT -->
<div class="a-spot-general" style="height: 320px; background-image: url('{{media url='wysiwyg/a-spot-imgs/Cutlery-header.jpg'}}'); background-position: center;">
<div><!-- COPY WRAP -->

<div class="float-lt" style="padding-left: 171px; padding-top: 42px;"> <!-- LEFT SIDE -->
<div class="align-rt" style="width:500px;">
<div class="a-spot-head" style="color:#333333;">PRIME CUTS</div>
<div class="a-spot-subhead" style="color:#333333;font-size:24px;">Professional-Grade Knives</div>
</div>
</div> <!-- END LEFT SIDE -->

<div class="float-lt"> <!-- RIGHT SIDE -->
<div style="padding-left:73px;padding-top:10px;margin-bottom:120px;font-size:12px;">Secure, ergonomic handles</div>
<div style="padding-left:30px;padding-top:13px;font-size:12px;">Forged full-tang construction</div>
</div>  <!-- END RIGHT SIDE -->

</div><!-- END COPY WRAP -->
</div>
<!-- END A-SPOT -->

</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'HappyChef Header Banner #SMART',
	'identifier' => 'happychef_header_banner_smart',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/SMART-header.jpg'}});background-position:center;" >
<div style="padding-left:30px;padding-top:160px;">

<div class="a-spot-head">SAFE & SOUND</div>
<div class="a-spot-subhead">Protect Your Devices with #SMART&reg;</div>

<div class="a-spot-btn-wrap">
<a class="a-spot-btn float-lt">Coats</a>
<a class="a-spot-btn float-lt">Aprons</a>
<a class="a-spot-btn float-lt">Pants</a>
</div>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'HappyChef Header Banner CookCool',
	'identifier' => 'happychef_header_banner_cookcool',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div id="cookcool-banner" class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/cookcool-header.jpg'}});" >
<div style="padding-left:30px;padding-top:100px;">

<div class="a-spot-head">ATHLETIC<br />PERFORMANCE</div>
<div class="a-spot-subhead">CookCool&reg; Apparel Controls the Heat</div>

<div class="a-spot-btn-wrap">
<a class="a-spot-btn float-lt">Coats</a>
<a class="a-spot-btn float-lt">Pants</a>
<a class="a-spot-btn float-lt">Hats</a>
</div>
</div>
</div>
<!-- END A-SPOT -->
</div>

EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'HappyChef Header Banner Kitchen Tools',
	'identifier' => 'happychef_header_banner_kitchen_tools',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
	<div class="custom">
	<!-- A-SPOT -->
	<div id="kitchen-tools-banner" class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Kitchen-tool-bg.jpg'}});background-position:center;position:relative;" >
	<img src="{{media url="wysiwyg/a-spot-imgs/Kitchen-tools.png"}}" alt="" />
	<div class="banner-copy-wrap align-ctr" style="z-index:1;padding-left:25px;padding-top:225px;"> <!-- COPY WRAP -->
	<div class="a-spot-subhead" style="color:#333333;">Get Set with These Essential Tools</div>
	<div class="a-spot-head" style="color:#333333;">READY FOR THE RUSH</div>
	</div> <!-- /COPY WRAP -->

	</div>
	<!-- END A-SPOT -->
	</div>

EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'HappyChef Header Banner Scrubs',
	'identifier' => 'happychef_header_banner_scrubs',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/Scrubs.jpg'}});background-position:center;" >

<div class="align-rt" style="padding-right:30px;padding-top:193px;"> <!-- COPY WRAP -->
<div class="a-spot-head">COMFORT ON CALL</div>
<div class="a-spot-subhead">Scrubs That Soften Your Shift</div>

</div> <!-- END COPY WRAP -->

</div>
<!-- END A-SPOT -->
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'HappyChef Header Banner Kitchen Shirts',
	'identifier' => 'happychef_header_banner_kitchen_shirts',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<div class="custom">
<!-- A-SPOT -->
<div id="kitchen-shirts-banner" class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/kitchen-shirts.jpg'}});" >

<div class="align-lt floatFix float-lt" style="padding-top:218px;padding-left:30px;"> <!-- COPY WRAP -->

<div class="a-spot-head">READY FOR EVERY SHIFT</div>

<div class="a-spot-subhead">Easy-Wearing Kitchen Shirt Styles</div>


</div> <!-- END COPY WRAP -->

</div>
<!-- END A-SPOT -->
</div>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'hc2-style',
	'identifier' => 'hc2-style',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
<style>

/* Content Page Styles 0806*/

/* Wrapper for all CMS content pages */
.customwrap {min-width:300px;}

/* COLUMN WIDTHS */
.customwrap {width: 960px;}
.custom .one-fourth-to-full-767
.custom .one_fourth,
.custom .one_third,
.custom  .one-third-to-one-half,
.custom .one-third-to-full-767,
.custom  .one-third-to-full,
.custom .one_half,
.custom .one-half-to-full-767,
.custom .two_thirds,
.custom .two-thirds-to-full-767,
.custom .three_fourths,
.custom .three-fourths-to-full-767
{
	display:inline;
	float: left;
	position: relative;
	margin-right: 10px;
}
.custom .one-third-to-full
{
	display:inline;
	float: left;
	position: relative;
	margin-left: 10px;
}
.custom .grid_24 {
	display:inline;
	float: left;
	position: relative;
}
.custom .one-fourth-to-full-767,
.custom .one_fourth {
	width:230px;
}
.custom .one-third-to-one-half img {max-width:310px;}
.custom  .one-third-to-one-half,
.custom  .one-third-to-full,
.custom .one-third-to-full-767,
.custom .one_third {
	width:310px;
}
.custom .one-half-to-full-767,
.custom .one_half {
	width:470px;
}
.custom .two-thirds-to-full-767,
.custom .two_thirds {
	width:630px;
}
.custom .three-fourths-to-full-767,
.custom .three_fourths {
	width:710px;
}
.custom #placementOptions  .omega,
.custom .omega {margin-right: 0;}
/* /COLUMN WIDTHS */

/***********************************************************/
/* Typography */
/***********************************************************/

/* FONT SIZES */
#cat-req-form-wrap h1,
#pg-cat-req-form-wrap h1,
.custom h1,
.custom .a-spot-head,
.custom .archer {font-family: 'Archer SSm A','Archer SSm B';}
.custom #chef-gifts-banner .a-spot-head {font-size:62px;}
#cat-req-form-wrap h1,
#pg-cat-req-form-wrap h1,
.custom h1 {font-size: 54px;font-weight:400;line-height:1.15;margin-bottom:10px;}
.a-spot-general .a-spot-head {font-size:50px;font-weight:400;}
.custom h2 {font-size: 32px;}
.custom h3 {font-size: 24px;font-weight: 400;margin-top: 30px;}
.custom #chef-gifts-banner .a-spot-subhead {font-size:28px;}
.custom .a-spot-subhead {font-size:22px;}
.custom h4 {font-size:18px;}
.custom h5 {margin:10px 0;}
.custom .intro,
.custom blockquote,
.custom .lg-copy {font-size:16px;}
.custom .med-copy {font-size:12px;}
.custom .sm-copy {font-size:10px;}
#cat-req-form-wrap .cat-input-rows-wrap h1 {font-size:50px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap h1 {font-size:50px;}

/* FONT WEIGHTS */
.custom a {cursor: pointer;}
.custom .bold {font-weight: bold;}
.custom h1 {font-weight:normal;}
.custom .a-spot-subhead,
.custom .narrow {font-weight:300;}

/* FONT COLORS */
#Catalog label {color:#666666;}

/* FONT STYLES */
.custom .ital {font-style:italic;}

/* LINE HEIGHTS */
.custom h4,
.custom .lg-line {line-height:24px;}
.custom .a-spot-subhead,
.custom .intro {line-height:26px;}
.custom #chef-gifts-banner .a-spot-head {line-height:62px;}
.a-spot-general .a-spot-head {line-height:60px;}
.custom .font-60 {font-size:60px;line-height:60px;}
.custom .font-48 {font-size:48px;line-height:48px;}
.custom .font-32 {font-size:32px;line-height:32px;}
.custom .font-24 {font-size:24px;line-height:30px;}
.custom .a-spot-btn,
.custom .font-14 {font-size:14px;line-height:16px;}

.custom blockquote {position: relative;}
.custom blockquote {font-weight: bold;}
.custom blockquote {font-style: normal;padding: 0 40px;line-height: 22px;margin-top: 10px;}
.custom blockquote:before {content: "\201C";font-size: 400%;position: absolute;left: 0;top: 10px;}
.custom blockquote:after {content: "\201D";font-size: 400%;position: absolute;right: 0;bottom: -23px;}
.custom .red {color:#ab0000;}
.custom .a-spot-btn,
.custom .a-spot-head,
.custom .a-spot-subhead,
.custom .white {color:#ffffff;}
.custom .lucida,
.custom blockquote:before,
.custom blockquote:after {font-family: 'Lucida Grande';}
.custom .a-spot-head,
.custom .a-spot-subhead,
.custom .shadow {text-shadow:1px 1px 5px #333333;}

/* TEXT TRANSFORMS */
.custom .all-ucase {text-transform: uppercase;}
.custom .ucase {text-transform: capitalize;}
.custom .lcase {text-transform: lowercase;}
.custom .half-width {width:50%;}
.custom a.link {text-decoration: underline;}

/* FLOATS */
#pg-cat-req-form-wrap .optin,
#pg-cat-req-form-wrap .opt-lbl,
.custom #coats-banner .copywrap,
#cat-req-form-wrap .cat-signup-header,
#pg-cat-req-form-wrap .cat-signup-header,
#cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-input-wrap,
.custom .float-lt,
.custom .resp-float-lt-767,
.custom .resp-float-lt-960,
.custom .resp-float-lt-480 {float: left;}
.custom .float-rt,
.custom .resp-float-rt-960,
.custom .resp-float-rt-767,
.custom .resp-float-rt-480 {float: right;}
/* Clear Floating Elements */
.custom .floatFix:after {clear: both;content: ' ';display: block;font-size: 0;line-height: 0;visibility: hidden;width: 0;height: 0;}
.custom .clear {clear:both;content:' ';display:block;font-size: 0;line-height: 0;visibility: hidden;width: 0;height: 0;}

/* TEXT ALIGNMENT */
#cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-input-wrap,
.custom .align-rt {text-align: right;}
#cat-req-form-wrap .cat-signup-header-text,
#pg-cat-req-form-wrap .cat-signup-header-text,
#cat-req-form-wrap .cat-signup-header-text-767,
#pg-cat-req-form-wrap .cat-signup-header-text-767,
.custom .align-ctr {text-align: center;}
.custom .sm-icon {width: 20px;height: 20px;margin: 5px;overflow: hidden;}
#pg-cat-req-form-wrap .cat-input-field,
.custom .no-border {border:none;}

/* BACKGROUNDS */
.custom .bkgd-lite {background-color:#f1f1f1;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select,
#pg-cat-req-form-wrap .cat-input-field,
.custom .bkgd-dark {background-color:#eeeeee;}
.custom #a-spot-general,
.custom #a-spot-custom {background-repeat:no-repeat;}
.custom .a-spot-btn {background-color:rgba(255,255,255,0.3);}
.custom #chef-gifts-banner,
.custom #womens-banner,
.custom #cookcool-banner,
.custom #kitchen-shirts-banner {background-position:center;}

/* BORDERS */
#contact-form,
#cat-req-form-wrap .cat-input-rows-wrap .cat-input-field,
#cat-req-form-wrap .cat-input-rows-wrap select,
#pg-cat-req-form-wrap .cat-input-rows-wrap select {border:none}
.custom .a-spot-btn {border:1px solid #ffffff}


/***********************************************************/
/* Structures */
/***********************************************************/

/* DISPLAY TYPES */
.a-spot-general a,
.custom .sm-icon,
.custom .inline-block,
#contact-form input,
#contact-form label,
#contact-form .rt-side {display:inline-block;}
.a-spot-btn-wrap ul {display:inline-table;}
.a-spot-btn-wrap ul li {display:inline;}
.custom img.ribbon,
.custom img.flag,
#pg-cat-req-form-wrap #Catalog,
#pg-cat-req-form-wrap .cat-signup-header img,
#cat-req-form-wrap .cat-signup-header img,
.custom #placementOptions img,
.custom .block,
#contact-form {display:block;}
#cat-req-form-wrap .cat-signup-header-img-767,
#cat-req-form-wrap .cat-signup-header-text-767,
.a-spot-general-mobile,
#a-spot-custom-mobile {display:none;}
.a-spot-custom-mobile div {overflow:hidden;}

/* MARGINS */
.custom img.ribbon,
.custom img.flag,
.custom .center {margin-left:auto;margin-right:auto;}
.a-spot-btn-wrap ul {margin:0;}
.custom .row-sm-vspace,
.custom .sm-vspace {margin-top:5px;margin-bottom:5px;}
.custom .row-vspace-10,
.custom .med-vspace {margin-top:10px;margin-bottom:10px;}
#cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-input-wrap,
.custom .row-vspace-15 {margin-top:15px;margin-bottom:15px;}
.custom .lg-vspace {margin-top:25px;margin-bottom:25px;}
.custom .a-spot-btn {margin: 0 1px;}

.custom .sm-vspace-top {margin-top: 5px;}
#cat-req-form-wrap h1,
#pg-cat-req-form-wrap h1,
.custom .med-vspace-top {margin-top: 10px;}
.custom #straightLine .rt-side,
.custom .a-spot-btn-wrap {margin-top:15px;}
#contact-form .submit-row,
.custom .vspace-top-20 {margin-top:20px;}
.custom .resp-lg-vspace-top-960,
.custom .lg-vspace-top {margin-top:25px;}
.custom .vspace-50-960 {margin-top:50px;}
#pg-cat-req-form-wrap .cat-signup-header img {margin-top:40px;}

#cat-req-form-wrap .cat-input-rows-wrap select,
#pg-cat-req-form-wrap .cat-input-rows-wrap select {margin-bottom:0;}
.custom .sm-vspace-bottom {margin-bottom: 5px;}
#cat-req-form-wrap input[type="image"],
#pg-cat-req-form-wrap input[type="image"],
.custom .med-vspace-bottom {margin-bottom: 10px}
.custom .lg-vspace-bottom {margin-bottom: 25px;}
.custom .vspace-50-960 {margin-bottom:50px;}

#cat-req-form-wrap .optin {margin-right:0;}
.custom .resp-sm-hspace-960,
.custom .sm-hspace {margin-left:5px;margin-right:5px;}
.custom .resp-med-hspace-960,
.custom .med-hspace {margin-left:10px;margin-right:10px;}
.custom .lg-hspace {margin-left:25px;margin-right:25px;}

#pg-cat-req-form-wrap .cat-signup-header img {margin-left:38px;}
#pg-cat-req-form-wrap #Catalog,
.customwrap {margin-left:auto;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .zip-lbl,
.custom .hspace-lt-5-6-960,
.custom .hspace-lt-5-6-480,
.custom .resp-sm-hspace-lt-960,
.custom .sm-hspace-lt {margin-left:5px;}
.custom .resp-med-hspace-lt-960,
.custom .med-hspace-lt {margin-left:10px;}
.custom .lg-hspace-lt {margin-left:25px;}
#cat-req-form-wrap .optin {margin-left:80px;}
#pg-cat-req-form-wrap input[type='image'],
#pg-cat-req-form-wrap .optin {margin-left:100px;}

#pg-cat-req-form-wrap #Catalog,
.customwrap {margin-right:auto;}
.custom .add-sm-hspace-rt-480 {margin-right:0;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .zip-lbl,
#pg-cat-req-form-wrap .cat-input-rows-wrap .std-lbl,
#contact-form label,
.custom .hspace-rt-5-6-480,
.custom .resp-sm-hspace-rt-480,
.custom .resp-sm-hspace-rt-960,
.custom .sm-hspace-rt {margin-right:5px;}
.custom .resp-med-hspace-rt-960,
.custom .med-hspace-rt {margin-right:10px;}
.custom .lg-hspace-rt {margin-right:25px;}


/* PADDING */
#pg-cat-req-form-wrap .cat-input-rows-wrap select,
.a-spot-btn-wrap ul {padding:0;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[type='text'] {padding-left:8px;padding-right:8px;}
.custom #coats-banner .copywrap,
.custom .resp-pad-lt-30-767,
.custom .pad-lt-30 {padding-left:30px;}
#cat-req-form-wrap .cat-signup-header,
.custom .pad-lt-50 {padding-left:50px;}
#cat-req-form-wrap input[type='image'] {padding-left:82px;}
.custom .pad-lt-100 {padding-left:100px;}
#contact-form input[type="text"] {padding-left:5px;}
.custom .a-spot-btn {padding-left:10px;}
#contact-form {padding-left:60px;}
#contact-form input[type="text"] {padding-right:5px;}
.custom .a-spot-btn {padding-right:10px;}
#pg-cat-req-form-wrap .cat-input-wrap {padding-right:0;}
.custom .pad-rt-20 {padding-right:20px;}
.custom #chef-gifts-banner .copywrap,
.custom #womens-banner .copywrap {padding-right:30px;}
#cat-req-form-wrap .cat-input-wrap,
.custom .pad-rt-50 {padding-right:50px;}
.custom #chef-gifts-banner .a-spot-subhead {padding-top:20px;}
.custom .pad-top-20,
#contact-form {padding-top:20px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select {padding:5px;}
.custom .lg-vpad-top {padding-top:30px;} 
.custom .lg-vpad {padding-top:30px;padding-bottom:30px;}
.custom #chef-gifts-banner .copywrap, 
.custom #womens-banner .copywrap {padding-top:40px;}
.custom #coats-banner .copywrap,
.custom .pad-top-100 {padding-top:100px;}
.custom .pad-top-160 {padding-top:160px;}
.custom .a-spot-btn {padding-top:5px;}
.custom .a-spot-btn {padding-bottom:5px;}

/* WIDTHS */
#cat-req-form-wrap .cat-input-rows-wrap select[name="Country"],
#cat-req-form-wrap .cat-input-rows-wrap .cat-input-field {width:190px;}
.custom img.ribbon {width:276px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select[name="Country"] {width:300px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field {width:284px;}
#cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"] {width:50px;}
#cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"] {width:60px;}
.custom img.flag {width:61px;}
.custom #straightLine .lt-side img {width:80px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"] {width:72px;}
#contact-form label {width:105px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"] {width:124px;}
.custom #straightLine .lr-side img {width:150px;}
#contact-form textarea,
#contact-form input[type="text"] {width:300px;}
#contact-form .rt-side {width:314px;}
#pg-cat-req-form-wrap #Catalog,
.custom .about-pg-banner,
#cat-req-form-wrap .cat-signup-header img {width:400px;}
#pg-cat-req-form-wrap .cat-signup-header {width:490px;}
#pg-cat-req-form-wrap .cat-signup-header img {width:450px;}
#pg-cat-req-form-wrap .cat-input-wrap {width:410px;}
.custom #straightLine .rt-side,
.custom #straightLine .lt-side {width:480px;}
.custom .a-spot-copy {width:550px;}

/* HEIGHTS */
#cat-req-form-wrap .cat-input-rows-wrap select,
#cat-req-form-wrap .cat-input-rows-wrap .cat-input-field {height:20px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select,
#pg-cat-req-form-wrap .cat-input-field,
#contact-form label,
#contact-form input[type="text"] {height:27px;}
.custom .hgt180 {height:180px;}

/* POSITIONING */
#kitchen-tools-banner img {position:absolute;}
#kitchen-tools-banner img {z-index:2;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select,
#cat-req-form-wrap .cat-input-rows-wrap select {position:relative;}

/* MISC */
.custom .required:before {content:"* ";color:#ab0000;} 
.custom a {text-decoration:none;}
.a-spot-btn-wrap ul {list-style:none;}
.custom .hgt180,
.a-spot-general {overflow:hidden;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select {top:-4px;}
#cat-req-form-wrap .cat-input-rows-wrap select {top:-2px;}


/* 960 BREAKPOINT */
@media screen and (max-width: 960px) {
.customwrap {width: 700px}
/* COLUMN WIDTHS 960 */	
.custom .one-fourth-to-full-767,
	.custom .one_fourth {
		width:167px;
	}
.custom .one-third-to-full-767,
	.custom .one_third {
		width:226px;
	}
.custom .one-third-to-one-half img {max-width:344px;}
.custom  .one-third-to-one-half,
.custom .one-half-to-full-767,
	.custom .one_half {
		width:344px;
	}
.custom .two-thirds-to-full-767,
	.custom .two_thirds {
		width:462px;
	}
	.custom .three-fourths-to-full-767,
	.custom .three_fourths {
		width:522px;
	}
/* /COLUMN WIDTHS 960 */
#cat-req-form-wrap h1 {font-size: 42px;font-weight:400;line-height:1.15;margin-bottom:10px;}
#pg-cat-req-form-wrap h1 {font-size: 46px;font-weight:400;line-height:1.15;margin-bottom:10px;}

/* DISPLAY 960 */
#a-spot-custom,
.custom .hide-960 {display:none;}
#a-spot-custom-mobile {display:block;}

/* BACKGROUND 960 */
.custom #kitchen-shirts-banner {background-position:-224px center;}
.custom #cookcool-banner {background-position:-26px center;}
.custom #staff-shirts-banner {background-position:-15px center;}
.custom #womens-banner {background-position:-75px center;}
.custom #shirts-banner {background-position:-247px center;}

/* FLOATS 960 */
.custom .resp-float-lt-960,
.custom .resp-float-rt-960 {float:none !important;}

/* CENTERING 960 */
.custom .center-960 {margin:auto;}
.custom #placementOptions  .one-third-to-full {width:100%;}

/* MARGINS 960 */
.custom #placementOptions  .one-third-to-full,
.custom .resp-med-hspace-960 {margin-left:0;margin-right:0;}
#pg-cat-req-form-wrap .cat-signup-header img,
.custom .resp-med-hspace-lt-960 {margin-left:0;}
.custom .hspace-lt-5-6-960 {margin-left:6px;}
#pg-cat-req-form-wrap input[type='image'],
#pg-cat-req-form-wrap .optin {margin-left:88px;}
#pg-cat-req-form-wrap #Catalog,
.custom .resp-med-hspace-rt-960 {margin-right:0;}
.custom .vspace-50-960,
.custom .resp-lg-vspace-top-960 {margin-top:0;}
#cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-input-wrap {margin-top:10px;}
.custom #straightLine .rt-side {margin-top:25px;}
.custom .vspace-50-960 {margin-bottom:0;}

/* WIDTHS 960 */
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"] {width:52px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"] {width:105px;}
.custom #straightLine .lt-side img {width:64px;}
.custom #straightLine .rt-side img {width:100px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select[name="Country"] {width:262px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field {width:246px;}
.custom #straightLine .rt-side {width:310px;}
.custom #straightLine .lt-side {width:345px;}
#pg-cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-signup-header img,
#pg-cat-req-form-wrap .cat-signup-header,
#pg-cat-req-form-wrap #Catalog,
#cat-req-form-wrap .cat-signup-header img {width:350px;}
.custom img.ribbon {width:100%;}

/* PADDING 960 */
#contact-form {padding-left: 0;}
#kitchen-tools-banner .banner-copy-wrap {padding-left:73px !important;padding-top:221px !important;}
#cat-req-form-wrap .cat-signup-header {padding-left:15px;}
#pg-cat-req-form-wrap .cat-input-wrap {padding-right:0;}
#cat-req-form-wrap .cat-input-wrap {padding-right:15px;}
#cat-req-form-wrap .cat-signup-header,
#pg-cat-req-form-wrap .cat-signup-header {padding-top:10px;}
.custom #womens-banner .copywrap {padding-top:105px;}

/* POSITIONING 960 */
#kitchen-tools-banner img {left:-98px;}

}
/* /960 BREAKPOINT */

/* 767 BREAKPOINT */
@media screen and (max-width: 767px) {
.align-center-767 {text-align: center;}
.custom .one-half-to-full-767,
.custom .one-fourth-to-full-767,
.custom .one-third-to-full-767,
.custom .two-thirds-to-full-767,
.custom  .one-third-to-full,
.custom .three-fourths-to-full-767,
.customwrap {width:460px;}
/* COLUMN WIDTHS 767 */	
	.custom .one_fourth {
		width:107px;
	}
	.custom .one_third {
		width:146px;
	}
.custom .one-third-to-one-half img {max-width:244px;}
.custom .one-third-to-one-half,
	.custom .one_half {
		width:224px;
	}
	.custom .two_thirds {
		width: 302px;
	}
	.custom .three_fourths {
		width:341px;
	}
.custom .one-fourth-to-full-767,
.custom .one-third-to-full-767,
       .custom .two-thirds-to-full-767,
	   .custom .three-fourths-to-full-767 {
		margin-left: auto;
		margin-right: auto;
	}
/* /COLUMN WIDTHS 767 */
/* FLOATS 767 */
#cat-req-form-wrap .cat-signup-header,
#pg-cat-req-form-wrap .cat-signup-header,
#cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-input-wrap,
.custom .resp-float-lt-767,
.custom .resp-float-rt-767 {float:none;}

/* POSITION 767 */


/* FONTS 767 */
.custom .font-48px-767 {font-size:48px;}
.custom #a-spot-custom-mobile h1 {font-size:50px;}

/* DISPLAY 767 */
#cat-req-form-wrap .cat-signup-header-text,
#cat-req-form-wrap .cat-signup-header-img,
#a-spot-custom,
.custom .hide-767 {display:none;}
#cat-req-form-wrap .cat-input-rows-wrap .std-lbl,
#pg-cat-req-form-wrap .cat-input-rows-wrap .std-lbl {display:inline-block;}
#cat-req-form-wrap .cat-signup-header-img-767,
#pg-cat-req-form-wrap .cat-signup-header-img-767,
#cat-req-form-wrap .cat-signup-header-text-767,
#pg-cat-req-form-wrap .cat-signup-header-text-767 {display:block;}

/* WIDTHS 767 */
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"] {width:72px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .std-lbl,
#cat-req-from-wrap .cat-input-rows-wrap .zip-lbl,
#cat-req-from-wrap .cat-input-rows-wrap .zip-lbl,
#cat-req-form-wrap .cat-input-rows-wrap .std-lbl {width:80px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"] {width:124px;}
#cat-req-form-wrap .opt-lbl {width:187px;}
.custom #placementOptions img {width:200px;}
#pg-cat-req-form-wrap .opt-lbl {width:244px;}
.custom #straightLine .rt-side {width:345px;}
#cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field,
#pg-cat-req-form-wrap .cat-signup-header img {width:284px;}
#pg-cat-req-form-wrap .cat-input-rows-wrap select[name="Country"],
#cat-req-form-wrap .cat-signup-header img {width:300px;}
#pg-cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap #Catalog {width:400px;}
.custom #straightLine .rt-side img {width:111px;}
#pg-cat-req-form-wrap .cat-signup-header {width:100%;}


/* CENTERING 767 */
#cat-req-form-wrap .cat-input-rows-wrap .zip-lbl,
#pg-cat-req-form-wrap .cat-input-rows-wrap .zip-lbl,
#cat-req-form-wrap .cat-input-rows-wrap .std-lbl,
#pg-cat-req-form-wrap .cat-input-rows-wrap .std-lbl {text-align:right;}
.custom .align-ctr-767 {text-align:center;}
#pg-cat-req-form-wrap .cat-signup-header img,
#cat-req-form-wrap .cat-signup-header img,
.custom .center-767 {margin:auto;}

/* BORDERS 767 */
#contact-form {border-right:0;}
#contact-form {border-bottom:1px solid #bbbbbb;}

/* MARGINS 767 */

#cat-req-form-wrap .cat-input-wrap {margin:auto;}
#cat-req-form-wrap .cat-input-rows-wrap .std-lbl,
#pg-cat-req-form-wrap .cat-input-rows-wrap .std-lbl {margin-right:10px;}
#cat-req-form-wrap .cat-signup-header-img-767 {margin-top:15px;}
#cat-req-form-wrap .optin {margin-left:60px;}
#pg-cat-req-form-wrap input[type="image"],
#pg-cat-req-form-wrap .optin {margin-left:100px;}

/* PADDING 767 */
#pg-cat-req-form-wrap .cat-signup-header {padding:0;}
#cat-req-form-wrap .cat-input-wrap,
#pg-cat-req-form-wrap .cat-input-wrap {padding-right:0;}
#cat-req-form-wrap .cat-signup-header {padding: 0 20px;}
.custom .resp-pad-lt-30-767 {padding-left:0;}
#cat-req-form-wrap input[type='image'] {padding-left:96px;}

/* MISC 767 */

}
/* /767 BREAKPOINT */

/* 480 BREAKPOINT */
@media screen and (max-width: 480px) {
/* COLUMN WIDTHS 480 */
.customwrap {width:100%;}
.align-center-480 {text-align: center;}
	/* GO TO FULL WIDTH */
.custom .one-fourth-to-full-767,
.custom .one-third-to-full-767,
	.custom .one_fourth,
.custom .one_third,
.custom .one-third-to-one-half,
.custom .one_half,
.custom .two_thirds,
.custom .two-thirds-to-full-767,
.custom .one-half-to-full-767,
.custom .three_fourths,
.custom .three-fourths-to-full-767,
.custom .full_width
	{
		width: 100%;
	}
	.custom .one_fourth,
	.custom .one_third,
	.custom .one_half,
	.custom .one-half-to-full-767,
	.custom .two_thirds,
	.custom .three_fourths
	{
		margin-left: auto;
		margin-right: auto;
	}
/* /COLUMN WIDTHS 480 */

	.custom h4 {font-size:24px;}
	.custom #a-spot-custom-mobile h1 {font-size:32px;}

     /* REMOVE FLOATS 480*/
     .custom .resp-float-lt-480 {float: none;}
     .custom .resp-float-rt-480 {float: none;}

   /* MARGINS 480 */
   #pg-cat-req-form-wrap .cat-input-wrap,
   #cat-req-form-wrap input[type="image"],
   #pg-cat-req-form-wrap input[type="image"] {margin-left:auto;}
   #pg-cat-req-form-wrap .cat-input-wrap,
   #cat-req-form-wrap input[type="image"],
   #pg-cat-req-form-wrap input[type="image"] {margin-right:auto;}
   #cat-req-form-wrap .cat-input-rows-wrap,
   #pg-cat-req-form-wrap .cat-input-rows-wrap,
   .custom .center480 {margin:auto;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap select[name="Country"],
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"],
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"],
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field,
   #cat-req-form-wrap .cat-input-rows-wrap .zip-lbl,
   #cat-req-form-wrap .cat-input-rows-wrap .row-vspace-15,
   #pg-cat-req-form-wrap .cat-input-rows-wrap .row-vspace-15 {margin-right:10px;}
   #contact-form label {margin-right: 0;}
   .custom #placementOptions  .one-third-to-full {margin-left:0}
   #pg-cat-req-form-wrap .optin {margin-left:41px;}
   .custom .resp-sm-hspace-rt-480 {margin-right:0;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap .zip-lbl,
   #pg-cat-req-form-wrap .cat-input-rows-wrap .std-lbl,
   .custom .add-sm-hspace-rt-480 {margin-right:5px;}
   .custom .hspace-lt-5-6-480 {margin-left:6px;}
   .custom .sm-hspace-rt-5-6-480 {margin-right:6px;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"] {margin-bottom:6px;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"] {margin-bottom:10px;}
   #cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"] {margin-bottom:15px;}
   
   /* PADDING 480 */
   #cat-req-form-wrap input[type="image"],
   #pg-cat-req-form-wrap input[type="image"],
   #pg-cat-req-form-wrap .cat-signup-header,
   #cat-req-form-wrap .cat-signup-header {padding:0;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"],

   /* WIDTHS 480 */
   .custom .one-third-to-one-half img {max-width:100%;}
   #cat-req-form-wrap .cat-input-rows-wrap select[name="Country"],
   #cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"],
   #cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"],
   #cat-req-form-wrap .cat-input-rows-wrap .cat-input-field {width:150px;}
   #contact-form textarea,
   #contact-form input[type="text"] {width:168px;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="Zip"],
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field {width:170px;}
   #pg-cat-req-form-wrap .opt-lbl,
   #contact-form .rt-side {width:182px;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap select[name="Country"],
   #pg-cat-req-form-wrap .cat-input-rows-wrap .cat-input-field[name="State"] {width:186px;}
   #cat-req-form-wrap .cat-signup-header img,
   #pg-cat-req-form-wrap .cat-signup-header img,
   #cat-req-form-wrap .cat-input-rows-wrap {width:260px;}
   #pg-cat-req-form-wrap #Catalog,
   #pg-cat-req-form-wrap .cat-input-wrap {width:293px;}
   .custom #straightLine .lt-side,
   .custom #straightLine .rt-side {width:300px;}
   .custom #straightLine .lt-side img {width:96px;}
   .custom #straightLine .rt-side img {width:96px;}
   #pg-cat-req-form-wrap .cat-input-rows-wrap .std-lbl,
   #cat-req-form-wrap .cat-input-rows-wrap .std-lbl {width:80px;}
   #cat-req-form-wrap .cat-input-rows-wrap select,
   #pg-cat-req-form-wrap .cat-input-rows-wrap select {width:156px;}

   /* CENTERING 480 */
   .custom .align-ctr-480,
   #contact-form .contact-form-body {text-align:center;}

   /* DISPLAY 480 */
   .custom .hide-480 {display:none}
   #cat-req-form-wrap input[type="image"],
   #pg-cat-req-form-wrap input[type="image"] {display:block;}
   
   /* MISC 480 */

} 
/* /480 BREAKPOINT */

.gradient-lt{
	background: rgba(0, 0, 0, 0.5);
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3);
	border-bottom-left-radius: 10px;
	border-top-left-radius: 10px;
	background: -webkit-linear-gradient(left,rgba(0,0,0,0.7),rgba(0,0,0,0.3));
	background: -moz-linear-gradient(right center , rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.7)) repeat scroll 0 0 rgba(0, 0, 0, 0);
	background: -o-linear-gradient(left,rgba(0,0,0,0.7),rgba(0,0,0,0.3));
	background: linear-gradient(to left, rgba(0.7,0,0,0), rgba(0.3,0,0,0));
	padding: 20px 30px 20px 20px !important;
}
.gradient-rt{
	background: rgba(0, 0, 0, 0.5);
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3);
	border-bottom-right-radius: 10px;
	border-top-right-radius: 10px;
	background: -webkit-linear-gradient(left,rgba(0,0,0,0.7),rgba(0,0,0,0.3));
	background: -moz-linear-gradient(right center , rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.7)) repeat scroll 0 0 rgba(0, 0, 0, 0);
	background: -o-linear-gradient(left,rgba(0,0,0,0.7),rgba(0,0,0,0.3));
	background: linear-gradient(to left, rgba(0.7,0,0,0), rgba(0.3,0,0,0));
	padding: 20px 20px 20px 30px !important;
}

</style>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'cust-pg-emb-table-style',
	'identifier' => 'cust-pg-emb-table-style',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<style>
/* LOGO EMBROIDERY RESPONSIVE TABLE */
#logo-table,
#logo-table table {
    border-collapse: collapse;
    border-spacing: 0;
    empty-cells: show;
    font-size: 100%;
	border:none;
}
#logo-table .data-table {
    width: 100%;
    text-align:center;
}
#logo-table .data-table td {text-align:center;vertical-align:middle;}
#logo-table .data-table .datatable-mobileheader,
#logo-table .data-table .datatable-mobileheader1,
#logo-table .data-table .datatable-mobileheader2,
#logo-table .data-table .datatable-mobileheader3 {
		display: none;
}
#logo-table .odd {background-color:#eeeeee;height:44px;}
#logo-table .even {background-color:#cccccc;height:45px;}
#logo-table .headrow {font-weight: bold;border-bottom:2px solid #aaaaaa;}
#logo-table .image-cell {width:216px;border:none;}
#logo-table table td {padding:0 10px;white-space:nowrap;border-right:1px solid #AAAAAA;}
#logo-table th {padding:0 10px;}
#logo-table table td:last-child {border-right:none;}
#logo-table table div {margin:auto;text-align:center;}
#logo-table table.data-table thead tr td div:first-child {width:110px;}
#logo-table table.data-table thead tr td:nth-child(n+2) div {width:55px;}

#logo-table .singleLine {line-height:45px;}

@media screen and (max-width: 960px) {
	#logo-table .odd {height:auto;}
	#logo-table .even {height:auto;}
	#logo-table table.data-table, 
	#logo-table .data-table thead, 
	#logo-table .data-table tbody, 
	#logo-table .data-table th, 
	#logo-table .data-table td, 
	#logo-table .data-table tr {
		display: block;
	}
	#logo-table .data-table thead tr {
		left: -9999px;
		position: absolute;
		top: -9999px;
	}
	#logo-table .data-table td {
		padding-left: 50% !important;
		position: relative;
	}
	#logo-table .data-table .datatable-mobileheader {
		display: block;
		left: 10px;
		top: 4px;
		padding-right: 10px;
		position: absolute;
		white-space: nowrap;
		width: 47%;
	}
	#logo-table .data-table .datatable-mobileheader1 {
		display: block;
		left: 10px;
		top: 10px;
		padding-right: 10px;
		position: absolute;
		white-space: nowrap;
		width: 47%;
	}
	#logo-table .data-table .datatable-mobileheader2 {
		display: block;
		left: 10px;
		top: 14px;
		padding-right: 10px;
		position: absolute;
		white-space: nowrap;
		width: 47%;
	}
	#logo-table .data-table .datatable-mobileheader3 {
		display: block;
		left: 10px;
		padding-right: 10px;
		position: absolute;
		white-space: nowrap;
		width: 47%;
	}
	#logo-table table td {border-right:none;height:45px;}
	#logo-table td.image-cell {display:none;}
	/*.resp-width {width:220px;}*/
	#logo-table .tbl-float1,
	#logo-table .tbl-float2 {float:left;width:50%;}
	
	#logo-table td:nth-child(2n) {background-color:#bbbbbb;}
	#logo-table td:nth-child(2n+1) {background-color:#eeeeee;}
	#logo-table .mobile-th1 {padding-bottom:10px;padding-top:10px;border-bottom:2px solid #aaaaaa;height:34px;}
	#logo-table .mobile-th2 {padding-top:20px;border-bottom:2px solid #aaaaaa;height:34px;}
	
}
@media screen and (max-width: 480px) {
	#logo-table .mobile-th1,
	#logo-table .mobile-th2 {font-size:85%;}
	#logo-table .data-table .datatable-mobileheader,
	#logo-table .data-table .datatable-mobileheader1 {left:0;}
	#logo-table .data-table .datatable-mobileheader {top:6px;}
	#logo-table .data-table .datatable-mobileheader2 {top:14px;}
	
	#logo-table td {padding:18px 0;font-size:95%;}
	.hide-480 {display:none;}
	#logo-table td.mobile-th1 {padding-bottom:10px;padding-top:10px;height:30px;border-bottom:2px solid #aaaaaa;}
	#logo-table td.mobile-th2 {padding-bottom:0;padding-top:20px;height:30px;border-bottom:2px solid #aaaaaa;}
}
/* END LOGO EMBROIDERY RESPONSIVE TABLE */
</style>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'HappyChef Header Banner Chef Gifts',
	'identifier' => 'happychef_header_banner_chef_gifts',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
	<div class="custom">
	<!-- A-SPOT -->
	<div id="chef-gifts-banner" class="a-spot-general" style="height:320px;background-image:url({{media url='wysiwyg/a-spot-imgs/chef-gifts-banner.jpg'}});" >
	<div class="align-rt copywrap"> <!-- COPY WRAP -->
	<div class="a-spot-head">JUST ADD<br />WRAPPING<br />PAPER</div>
	<div class="a-spot-subhead">Top Gifts for Chefs</div>
	</div> <!-- END COPY WRAP -->

	</div>
	<!-- END A-SPOT -->
	</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'Catalog Request Slideout',
	'identifier' =>'catalog-request-slideout',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}

<div id="cat-req-form-wrap" class="custom customwrap">
<div class="resp-float-rt-480 floatFix" style="background-color:#f4f2ed;border:5px solid #e0e0e0;"> <!-- WRAP -->

<div class="cat-signup-header"> <!-- LEFT SIDE -->
<div class="cat-signup-header-img-767"> <!-- TOP LEFT 767-->
<img src="{{media url="wysiwyg/catalog-form-img.png"}}">
</div> <!-- /BOTTOM LEFT -->
<div class="cat-signup-header-text"> <!-- TOP LEFT-->
<h1>GET YOUR FREE<br />2014 CATALOG</h1>
<h5>INCLUDES PRINT EDITION & DIGITAL DOWNLOAD</h5>
</div> 
<!-- /TOP LEFT -->
<div class="cat-signup-header-img"> <!-- BOTTOM LEFT -->
<img src="{{media url="wysiwyg/catalog-form-img.png"}}">
</div> <!-- /BOTTOM LEFT -->
<div class="cat-signup-header-text-767"> <!-- BOTTOM LEFT 767-->
<h1>GET YOUR FREE<br />2014 CATALOG</h1>
<h5>INCLUDES PRINT EDITION & DIGITAL DOWNLOAD</h5>
</div>
</div> <!-- /LEFT SIDE -->

<div class="cat-input-wrap"> <!-- RIGHT SIDE -->
<form id="Catalog" onsubmit="MM_validateForm('Email','','RisEmail','Name','','R','Address','','R','City','','R','State','','R','Zip','','R');return document.MM_returnValue" name="Catalog" method="post" action="/cs/catalog.cfm">
<div class="cat-input-rows-wrap">
<div class="row-vspace-15">
	<label class="required std-lbl" for="Name">Name</label>
    <input class="cat-input-field" id="Name" type="text" maxlength="50" value="" name="Name" />
</div>
<div class="row-vspace-15">
	<label class="std-lbl" for="Company">Company</label>
    <input class="cat-input-field" id="Company" type="text" maxlength="50" value="" name="Company" />
</div>
<div class="row-vspace-15">
	<label class="std-lbl" for="Title">Title</label>
    <input class="cat-input-field" id="Title" type="text" maxlength="25" value="" name="Title" />
</div>
<div class="row-vspace-15">
	<label class="required std-lbl" for="Address">Address 1</label>
    <input class="cat-input-field" id="Address" type="text" maxlength="50"value="" name="Address" />
</div>
<div class="row-vspace-15">
	<label class="std-lbl" for="Address2">Address 2</label>
    <input class="cat-input-field" id="Address2" type="text" maxlength="50" value="" name="Address2" />
</div>
<div class="row-vspace-15">
	<label class="required std-lbl" for="City">City</label>
    <input class="cat-input-field" id="City" type="text" maxlength="20" value="" name="City" />
</div>
<div class="row-vspace-15">
	<label id="stateLblPop" class="required std-lbl" for="State">State</label>
    <select id="State" class="cat-input-field" type="text" maxlength="2" value="" name="State">
    	<option value="AL" title="Alabama">AL</option>
        <option value="AK" title="Alaska">AK</option>
        <option value="AZ" title="Arizona">AZ</option>
        <option value="AR" title="Arkansas">AR</option>
        <option value="CA" title="California">CA</option>
        <option value="CO" title="Colorado">CO</option>
        <option value="CT" title="Connecticut">CT</option>
        <option value="DE" title="Delaware">DE</option>
        <option value="DC" title="District of Columbia">DC</option>
        <option value="FL" title="Florida">FL</option>
        <option value="GA" title="Georgia">GA</option>
        <option value="HI" title="Hawaii">HI</option>
        <option value="ID" title="Idaho">ID</option>
        <option value="IL" title="Illinois">IL</option>
        <option value="IN" title="Indiana">IN</option>
        <option value="IA" title="Iowa">IA</option>
        <option value="KS" title="Kansas">KS</option>
        <option value="KY" title="Kentucky">KY</option>
        <option value="LA" title="Louisiana">LA</option>
        <option value="ME" title="Maine">ME</option>
        <option value="MD" title="Maryland">MD</option>
        <option value="MA" title="Massachusetts">MA</option>
        <option value="MI" title="Michigan">MI</option>
        <option value="MN" title="Minnesota">MN</option>
        <option value="MS" title="Mississippi">MS</option>
        <option value="MO" title="Missouri">MO</option>
        <option value="MT" title="Montana">MT</option>
        <option value="NE" title="Nebraska">NE</option>
        <option value="NV" title="Nevada">NV</option>
        <option value="NH" title="New Hampshire">NH</option>
        <option value="NJ" title="New Jersey">NJ</option>
        <option value="NM" title="New Mexico">NM</option>
        <option value="NY" title="New York">NY</option>
        <option value="NC" title="North Carolina">NC</option>
        <option value="ND" title="North Dakota">ND</option>
        <option value="OH" title="Ohio">OH</option>
        <option value="OK" title="Oklahoma">OK</option>
        <option value="OR" title="Oregon">OR</option>
        <option value="PA" title="Pennsylvania">PA</option>
        <option value="RI" title="Rhode Island">RI</option>
        <option value="SC" title="South Carolina">SC</option>
        <option value="SD" title="South Dakota">SD</option>
        <option value="TN" title="Tennessee">TN</option>
        <option value="TX" title="Texas">TX</option>
        <option value="UT" title="Utah">UT</option>
        <option value="VT" title="Vermont">VT</option>
        <option value="VA" title="Virginia">VA</option>
        <option value="WA" title="Washington">WA</option>
        <option value="WV" title="West Virginia">WV</option>
        <option value="WI" title="Wisconsin">WI</option>
        <option value="WY" title="Wyoming">WY</option>
     </select>

	<label class="required zip-lbl" for="Zip">ZIP Code</label>
    <input class="cat-input-field" id="Zip" type="text" maxlength="10" value="" name="Zip" />
</div>
<div class="row-vspace-15">
	<label class="required std-lbl" for="Country">Country</label>
    <select id="Country" size="1" class="cat-input-field" name="Country" type="text">
    	<option value="USA">United States</option>
		<option value="CAN">Canada</option>
		<option value="PRI">Puerto Rico</option>
		<option value="VIR">Virgin Islands (U.S.)</option>
    </select>
     <script>
	var countrySelect = document.getElementById('Country');
	var stateSelect = document.getElementById('State');
	countrySelect.onchange = (function(){
		stateSelect.options.length = 0;
		switch(countrySelect.value){
			case 'CAN':
				jQuery('#stateLblPop').html('Province');
				stateSelect.options[0] = new Option('AB', 'AB');
				stateSelect.options[1] = new Option('BC', 'BC');
				stateSelect.options[2] = new Option('MB', 'MB');
				stateSelect.options[3] = new Option('NB', 'NB');
				stateSelect.options[4] = new Option('NL', 'NL');
				stateSelect.options[5] = new Option('NS', 'NS');
				stateSelect.options[6] = new Option('ON', 'ON');
				stateSelect.options[7] = new Option('PE', 'PE');
				stateSelect.options[8] = new Option('QC', 'QC');
				stateSelect.options[9] = new Option('SK', 'SK');
				stateSelect.options[10] = new Option('NT', 'NT');
				stateSelect.options[11] = new Option('NU', 'NU');
				stateSelect.options[12] = new Option('YT', 'YT');
				jQuery('#State option:first-child').attr({'title':'Alberta'});
				jQuery('#State option:nth-child(2)').attr({'title':'British Columbia'});
				jQuery('#State option:nth-child(3)').attr({'title':'Manitoba'});
				jQuery('#State option:nth-child(4)').attr({'title':'New Brunswick'});
				jQuery('#State option:nth-child(5)').attr({'title':'Newfoundland and Labrador'});
				jQuery('#State option:nth-child(6)').attr({'title':'Nova Scotia'});
				jQuery('#State option:nth-child(7)').attr({'title':'Ontario'});
				jQuery('#State option:nth-child(8)').attr({'title':'Prince Edward Island'});
				jQuery('#State option:nth-child(9)').attr({'title':'Quebec'});
				jQuery('#State option:nth-child(10)').attr({'title':'Saskatchewan'});
				jQuery('#State option:nth-child(11)').attr({'title':'Northwest Territories'});
				jQuery('#State option:nth-child(12)').attr({'title':'Nunavut'});
				jQuery('#State option:nth-child(13)').attr({'title':'Yukon'});
				break;
			case 'PRI':
				jQuery('#stateLblPop').html('State');
				stateSelect.options[0] = new Option('--', '--');
				break;
			case 'VIR':
				jQuery('#stateLblPop').html('State');
				stateSelect.options[0] = new Option('--', '--');
				break;
			case 'USA':
				jQuery('#stateLblPop').html('State');
				stateSelect.options[0] = new Option('AL', 'AL');
				stateSelect.options[1] = new Option('AK', 'AK');
				stateSelect.options[2] = new Option('AZ', 'AZ');
				stateSelect.options[3] = new Option('AR', 'AR');
				stateSelect.options[4] = new Option('CA', 'CA');
				stateSelect.options[5] = new Option('CO', 'CO');
				stateSelect.options[6] = new Option('CT', 'CT');
				stateSelect.options[7] = new Option('DE', 'DE');
				stateSelect.options[8] = new Option('DC', 'DC');
				stateSelect.options[9] = new Option('FL', 'FL');
				stateSelect.options[10] = new Option('GA', 'GA');
				stateSelect.options[11] = new Option('HI', 'HI');
				stateSelect.options[12] = new Option('ID', 'ID');
				stateSelect.options[13] = new Option('IL', 'IL');
				stateSelect.options[14] = new Option('IN', 'IN');
				stateSelect.options[15] = new Option('IA', 'IA');
				stateSelect.options[16] = new Option('KS', 'KS');
				stateSelect.options[17] = new Option('KY', 'KY');
				stateSelect.options[18] = new Option('LA', 'LA');
				stateSelect.options[19] = new Option('ME', 'ME');
				stateSelect.options[20] = new Option('MD', 'MD');
				stateSelect.options[21] = new Option('MA', 'MA');
				stateSelect.options[22] = new Option('MI', 'MI');
				stateSelect.options[23] = new Option('MN', 'MN');
				stateSelect.options[24] = new Option('MS', 'MS');
				stateSelect.options[25] = new Option('MO', 'MO');
				stateSelect.options[26] = new Option('MT', 'MT');
				stateSelect.options[27] = new Option('NE', 'NE');
				stateSelect.options[28] = new Option('NV', 'NV');
				stateSelect.options[29] = new Option('NH', 'NH');
				stateSelect.options[30] = new Option('NJ', 'NJ');
				stateSelect.options[31] = new Option('NM', 'NM');
				stateSelect.options[32] = new Option('NY', 'NY');
				stateSelect.options[33] = new Option('NC', 'NC');
				stateSelect.options[34] = new Option('ND', 'ND');
				stateSelect.options[35] = new Option('OH', 'OH');
				stateSelect.options[36] = new Option('OK', 'OK');
				stateSelect.options[37] = new Option('OR', 'OR');
				stateSelect.options[38] = new Option('PA', 'PA');
				stateSelect.options[39] = new Option('RI', 'RI');
				stateSelect.options[40] = new Option('SC', 'SC');
				stateSelect.options[41] = new Option('SD', 'SD');
				stateSelect.options[42] = new Option('TN', 'TN');
				stateSelect.options[43] = new Option('TX', 'TX');
				stateSelect.options[44] = new Option('UT', 'UT');
				stateSelect.options[45] = new Option('VT', 'VT');
				stateSelect.options[46] = new Option('VA', 'VA');
				stateSelect.options[47] = new Option('WA', 'WA');
				stateSelect.options[48] = new Option('WV', 'WV');
				stateSelect.options[49] = new Option('WI', 'WI');
				stateSelect.options[50] = new Option('WY', 'WY');
				jQuery('#State option:first-child').attr({'title':'Alabama'});
				jQuery('#State option:nth-child(2)').attr({'title':'Alaska'});
				jQuery('#State option:nth-child(3)').attr({'title':'Arizona'});
				jQuery('#State option:nth-child(4)').attr({'title':'Arkansas'});
				jQuery('#State option:nth-child(5)').attr({'title':'California'});
				jQuery('#State option:nth-child(6)').attr({'title':'Colorado'});
				jQuery('#State option:nth-child(7)').attr({'title':'Connecticut'});
				jQuery('#State option:nth-child(8)').attr({'title':'Delaware'});
				jQuery('#State option:nth-child(9)').attr({'title':'District of Columbia'});
				jQuery('#State option:nth-child(10)').attr({'title':'Florida'});
				jQuery('#State option:nth-child(11)').attr({'title':'Georgia'});
				jQuery('#State option:nth-child(12)').attr({'title':'Hawaii'});
				jQuery('#State option:nth-child(13)').attr({'title':'Idaho'});
				jQuery('#State option:nth-child(14)').attr({'title':'Illinois'});
				jQuery('#State option:nth-child(15)').attr({'title':'Indiana'});
				jQuery('#State option:nth-child(16)').attr({'title':'Iowa'});
				jQuery('#State option:nth-child(17)').attr({'title':'Kansas'});
				jQuery('#State option:nth-child(18)').attr({'title':'Kentucky'});
				jQuery('#State option:nth-child(19)').attr({'title':'Louisiana'});
				jQuery('#State option:nth-child(20)').attr({'title':'Maine'});
				jQuery('#State option:nth-child(21)').attr({'title':'Maryland'});
				jQuery('#State option:nth-child(22)').attr({'title':'Massachusetts'});
				jQuery('#State option:nth-child(23)').attr({'title':'Michigan'});
				jQuery('#State option:nth-child(24)').attr({'title':'Minnesota'});
				jQuery('#State option:nth-child(25)').attr({'title':'Mississippi'});
				jQuery('#State option:nth-child(26)').attr({'title':'Missouri'});
				jQuery('#State option:nth-child(27)').attr({'title':'Montana'});
				jQuery('#State option:nth-child(28)').attr({'title':'Nebraska'});
				jQuery('#State option:nth-child(29)').attr({'title':'Nevada'});
				jQuery('#State option:nth-child(30)').attr({'title':'New Hampshire'});
				jQuery('#State option:nth-child(31)').attr({'title':'New Jersey'});
				jQuery('#State option:nth-child(32)').attr({'title':'New Mexico'});
				jQuery('#State option:nth-child(33)').attr({'title':'New York'});
				jQuery('#State option:nth-child(34)').attr({'title':'North Carolina'});
				jQuery('#State option:nth-child(35)').attr({'title':'North Dakota'});
				jQuery('#State option:nth-child(36)').attr({'title':'Ohio'});
				jQuery('#State option:nth-child(37)').attr({'title':'Oklahoma'});
				jQuery('#State option:nth-child(38)').attr({'title':'Oregon'});
				jQuery('#State option:nth-child(39)').attr({'title':'Pennsylvania'});
				jQuery('#State option:nth-child(40)').attr({'title':'Rhode Island'});
				jQuery('#State option:nth-child(41)').attr({'title':'South Carolina'});
				jQuery('#State option:nth-child(42)').attr({'title':'South Dakota'});
				jQuery('#State option:nth-child(43)').attr({'title':'Tennessee'});
				jQuery('#State option:nth-child(44)').attr({'title':'Texas'});
				jQuery('#State option:nth-child(45)').attr({'title':'Utah'});
				jQuery('#State option:nth-child(46)').attr({'title':'Vermont'});
				jQuery('#State option:nth-child(47)').attr({'title':'Virginia'});
				jQuery('#State option:nth-child(48)').attr({'title':'Washington'});
				jQuery('#State option:nth-child(49)').attr({'title':'West Virginia'});
				jQuery('#State option:nth-child(50)').attr({'title':'Wisconsin'});
				jQuery('#State option:nth-child(51)').attr({'title':'Wyoming'});
				break;
		}
		
	});
	</script>
</div>
<div class="row-vspace-15">
	<label class="required std-lbl" for="cat-email">Email</label>
    <input id="cat-email" class="cat-input-field" type="text" />
</div>
</div> <!-- /RIGHT SIDE -->
<div class="row-vspace-15">
    <input class="optin" checked="" value="1" name="OptIn" type="checkbox" />
	<label class="opt-lbl" for="OptIn" style="display:inline-block;text-align:left;">Send me exclusive offers</label>
</div>
<div class="row-vspace-15">
	<div class="resp-float-lt-480">
    	<input type="image" width="100px" src="{{media url="wysiwyg/catalog-form-submit.png"}}" />
    </div>
    <div class="resp-float-rt-767 align-ctr-480 required sm-copy" style="color:#ab0000;">Required Field</div>
</div>

</form>
</div>
</div> <!-- /WRAP -->
</div>


EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'EM0080-Featurette-Product-Detail-Coats',
	'identifier' => 'EM0080-Featurette-Product-Detail-Coats',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="block-feature-product"> Featurette static block - COATS </div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'EM0080-Featurette-Product-Detail-CoatsTEST',
	'identifier' => 'EM0080-Featurette-Product-Detail-CoatsTEST',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="block-feature-product"> Featurette static block - COATS TEST</div>
EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'Happychef-Sizecharts',
	'identifier' => 'happychef_sizechart',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="size-chart">
<div class="title-size-chart">Unisex Coats & shirts</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th>4XL</th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Men's Neck Size</th><th> &nbsp </th><th>14-14<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>15-15<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>16-16<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>17-17<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>18-18<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>19-19<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>20-20<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Women's Size</th><th> &nbsp </th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20-22</th><th>24-26</th><th>28-30</th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Unisex Pants</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th>4XL</th><th>5XL</th><th> &nbsp </th></tr>
<tr><th class="title">Waist Size</th><th>24-26</th><th>28-30</th><th>32-34</th><th>36-38</th><th>40-42</th><th>44-46</th><th>54-57</th><th>58-63</th><th>64-68</th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Women's sizing</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Women's Size</th><th>2</th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20W-22W</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Kids sizing</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Kids Size</th><th>2t-4t</th><th>4-5</th><th>6-7</th><th>8-10</th><th>12</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Unisex performance pants</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Waist Size</th><th>24-26</th><th>28-30</th><th>32-34</th><th>36-38</th><th>40-42</th><th>44-46</th><th>48-50</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Unisex woven shirts</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th>4XL</th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Men's Neck Size</th><th> &nbsp </th><th>14-14<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>15-15<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>16-16<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>17-17<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>18-18<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>19-19<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>20-20<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Women's Size</th><th> &nbsp </th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20-22</th><th>24-26</th><th>28-30</th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>


<div class="size-chart">
<div class="title-size-chart">footwear size</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th rowspan="2"> Men's</th><th>US Size</th><th>6-6<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>7-7<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>8-8<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>9-9<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>10-10<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>11-11<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>12-12<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>13-13<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>14</th></tr>
<tr><th>European</th><th>39</th><th>40</th><th>40</th><th>42</th><th>43</th><th>44</th><th>45</th><th>46</th><th>47</th></tr>

<tr><th rowspan="2"> Women's</th><th>US Size</th><th>4-4<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>5-5<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>6-6<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>7-7<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>8-8<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>9-9<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>10-10<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>11-11<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th></th></tr>
<tr><th>European</th><th>35</th><th>36</th><th>37</th><th>38</th><th>39</th><th>40</th><th>41</th><th>42</th><th></th></tr>
</tbody>
</table>
</div>

EOB
);
$block->setData($dataBlock)->save();


$dataBlock = array(
	'title' => 'SizeChart Men',
	'identifier' => 'sizechartmen',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="size-chart">
<div class="title-size-chart">Unisex Coats &amp; shirts</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th>4XL</th><th>&amp;nbsp</th><th>&amp;nbsp</th></tr>
<tr><th class="title">Men's Neck Size</th><th>&amp;nbsp</th><th>14-14<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>15-15<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>16-16<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>17-17<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>18-18<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>19-19<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>20-20<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>&amp;nbsp</th><th>&amp;nbsp</th></tr>
<tr><th class="title">Women's Size</th><th>&amp;nbsp</th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20-22</th><th>24-26</th><th>28-30</th><th>&amp;nbsp</th><th>&amp;nbsp</th></tr>
</tbody>
</table>
</div>
EOB
);
$block->setData($dataBlock)->save();

$dataBlock = array(
	'title' => 'SizeChart Women',
	'identifier' =>'sizechart-women',
	'stores' => $stores,
	'is_active' => 1,
	'content'	=> <<<EOB
	<div class="size-chart">
<div class="title-size-chart">Women's sizing</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>&amp;nbsp</th><th>&amp;</th><th>&amp;</th><th>&amp;</th></tr>
<tr><th class="title">Women's Size</th><th>2</th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20W-22W</th><th>&amp;</th><th>&amp;nbsp</th><th>&amp;</th><th>&amp;</th></tr>
</tbody>
</table>
</div>
EOB
);
$block->setData($dataBlock)->save();
####################################################################################################
# INSERT PAGE
####################################################################################################
$name="happychef";
$dataPage = array(
	'title'				=> 'About Us',
	'identifier' 		=> 'about-us',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<div class="custom customwrap">

<div class="lg-vspace-bottom">
<img class="custom med-vspace-bottom about-pg-banner" src="{{media url="wysiwyg/a-spot-imgs/about.jpg"}}" alt="HappyChef Uniforms" />

<div class="align-ctr">
<h1>BETTER ALL THE TIME</h1>
</div>

<div class="one-fourth-to-full-767">
<blockquote style="font-weight: bold;font-style: normal;line-height: 22px">A one-stop resource for the latest innovations in culinary style</blockquote>
</div>

<div class="three-fourths-to-full-767 omega">
<p><span style="font-size: 18px;line-height: 32px;">Over three generations of dedicated service...</span><br>
What started as a New Jersey-based chef apparel company has transformed into a one-stop resource for the latest innovations in culinary styles. From coats and hats to kitchen tools and footwear, we have proudly earned our reputation by consistently designing high quality products and offering them at the absolute best prices.</p>
<p>What&rsquo;s more, all of our designs are based on your feedback. We&rsquo;re constantly on the lookout for new ways to fill your needs in an ever-changing culinary world. Because of this, we know you&rsquo;ll find products perfectly suited to your work. And we hope you&rsquo;ll enjoy using them as much as we enjoy making them.</p>
</div>


</div> <!-- END VSPACE -->
</div> <!-- END CONTENT -->
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();

$dataPage = array(
	'title'				=> 'Customer Service',
	'identifier' 		=> 'customer-service-new-'.$name,
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
<div class="page-head">
<h3>Customer Service</h3>
</div>
<ul class="disc" style="margin-bottom: 15px;">
<li><a href="#answer1">Shipping &amp; Delivery</a></li>
<li><a href="#answer2">Privacy &amp; Security</a></li>
<li><a href="#answer3">Returns &amp; Replacements</a></li>
<li><a href="#answer4">Ordering</a></li>
<li><a href="#answer5">Payment, Pricing &amp; Promotions</a></li>
<li><a href="#answer6">Viewing Orders</a></li>
<li><a href="#answer7">Updating Account Information</a></li>
</ul>
<dl><dt id="answer1">Shipping &amp; Delivery</dt><dd style="margin-bottom: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer2">Privacy &amp; Security</dt><dd style="margin-bottom: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer3">Returns &amp; Replacements</dt><dd style="margin-bottom: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer4">Ordering</dt><dd style="margin-bottom: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer5">Payment, Pricing &amp; Promotions</dt><dd style="margin-bottom: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer6">Viewing Orders</dt><dd style="margin-bottom: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer7">Updating Account Information</dt><dd style="margin-bottom: 10px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd></dl>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Customization',
	'identifier' 		=> 'customization',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="42"}}
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="43"}}


<div class="custom customwrap">

<!-- A-SPOT -->
<div id="a-spot-custom" style="height:502px;background-image:url({{media url="wysiwyg/custom-t1.jpg"}});background-position:center;" >

<div class="white pad-lt-30 pad-top-160 a-spot-copy">

<h1 class="archer font-48px">CUSTOMIZATION</h1>

<div class="row-med-vspace floatFix">
<a class="white link" href="#straightLine">STRAIGHT LINE</a><span class="lg-hspace-lt lg-hspace-rt hide-960">|</span>
<a class="white link" href="#logoEmbroidery">LOGO EMBROIDERY</a><span class="lg-hspace-lt lg-hspace-rt hide-960">|</span>
<a class="white link" href="#flagsAndRibbons">FLAGS AND RIBBONS</a>
</div>

<div class="sm-copy">Give your apparel a unique touch by adding your name, logo, or design to almost any Happy Chef product.
Customization specialists reproduce your image with high-precision machinery capable of rendering even the 
most detailed graphics accurately. From aprons and shirts to coats and hats, Happy Chef ensures your design 
makes the best impression possible.</div>
</div>
</div>
<!-- END A-SPOT -->

<!-- MOBILE A-SPOT -->
<div id="a-spot-custom-mobile">
<div class="hgt180">
<img  src="{{media url='wysiwyg/custom-t1.jpg'}}" style="width:100%;" />
</div>
<h1 class="align-ctr">CUSTOMIZATION</h1>

<div class="floatFix">
<div class="med-vspace align-ctr"><a class="link" href="#straightLine">STRAIGHT LINE</a></div>
<div class="med-vspace align-ctr"><a class="link" href="#logoEmbroidery">LOGO EMBROIDERY</a></div>
<div class="med-vspace align-ctr"><a class="link" href="#flagsAndRibbons">FLAGS AND RIBBONS</a></div>
</div>

<div class="sm-copy align-ctr">Give your apparel a unique touch by adding your name, logo, or design to almost any Happy Chef product.
Customization specialists reproduce your image with high-precision machinery capable of rendering even the 
most detailed graphics accurately. From aprons and shirts to coats and hats, Happy Chef ensures your design 
makes the best impression possible.</div>

</div>
<!-- END MOBILE A-SPOT -->


<!-- PLACEMENT OPTIONS -->
<div id="placementOptions" class="floatFix"> 
<h3>PLACEMENT OPTIONS</h3>


<div class="row-med-vspace floatFix">
<h4 class="align-ctr">Chef Coats</h4>
<div class="one_half"><img class="med-vspace center" src="{{media url="wysiwyg/customization-page-imgs/chef-coats-p1.jpg"}}"></div>
<div class="one_half omega"><img class="med-vspace center" src="{{media url="wysiwyg/customization-page-imgs/chef-coats-p2.jpg"}}"></div>
</div>

<div class="row-med-vspace floatFix">
<h4 class="align-ctr">T-Shirts</h4>
<div class="one_half"><img class="med-vspace center" src="{{media url="wysiwyg/customization-page-imgs/tshirts-p1.jpg"}}"></div>
<div class="one_half omega"><img class="med-vspace center" src="{{media url="wysiwyg/customization-page-imgs/tshirts-p2.jpg"}}"></div>
</div>


<div class="row-med-vspace floatFix">
<div>
<div class="one-third-to-one-half"><h4 class="align-ctr">Aprons</h4><img class="med-vspace center" src="{{media url="wysiwyg/customization-page-imgs/aprons-p1.jpg"}}"></div>
<div class="one-third-to-one-half omega"><h4 class="align-ctr">Shirts</h4><img class="med-vspace center" src="{{media url="wysiwyg/customization-page-imgs/shirts-p1.jpg"}}"></div>
</div>

<div>
<div class="one-third-to-full"><h4 class="align-ctr">Headwear</h4><img class="vspace-50-960 center" src="{{media url="wysiwyg/customization-page-imgs/headwear-p1.jpg"}}"></div>
</div>
</div>
</div> 
<!-- END PLACEMENT OPTIONS -->

<div class="clear"></div>
<hr style="color:#dddddd;margin:10px 0;" />

<!-- STRAIGHT LINE -->
<div id="straightLine"> 
<h3>STRAIGHT LINE</h3>
<p>Embroider up to three lines of text in a range of color, style, and placement combinations. 20 characters per line. <span class="bold">$4.99 per line</span></p>
<div class="resp-float-lt-767 center-767 lt-side floatFix"> <!-- LEFT SIDE -->

<div class="med-vspace hspace-rt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-blk.jpg"}}">
<div class="align-ctr sm-copy">BLACK</div>
</div>
<div class="med-vspace hspace-rt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-yellow.jpg"}}">
<div class="align-ctr sm-copy">YELLOW</div>
</div>
<div class="med-vspace resp-sm-hspace-rt-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-silvergray.jpg"}}">
<div class="align-ctr sm-copy">SILVER GRAY</div>
</div>
<div class="med-vspace hspace-rt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-white.jpg"}}">
<div class="align-ctr sm-copy">WHITE</div>
</div>
<div class="med-vspace float-lt add-sm-hspace-rt-480">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-royal.jpg"}}">
<div class="align-ctr sm-copy">ROYAL</div>
</div>


<div class="med-vspace resp-sm-hspace-rt-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-red.jpg"}}">
<div class="align-ctr sm-copy">RED</div>
</div>
<div class="med-vspace hspace-rt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-purple.jpg"}}">
<div class="align-ctr sm-copy">PURPLE</div>
</div>
<div class="med-vspace sm-hspace-rt float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-orange.jpg"}}">
<div class="align-ctr sm-copy">ORANGE</div>
</div>
<div class="med-vspace resp-sm-hspace-rt-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-navy.jpg"}}">
<div class="align-ctr sm-copy">NAVY</div>
</div>
<div class="med-vspace float-lt add-sm-hspace-rt-480">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-kellygreen.jpg"}}">
<div class="align-ctr sm-copy">KELLY GREEN</div>
</div>



<div class="med-vspace hspace-rt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-gold.jpg"}}">
<div class="align-ctr sm-copy">GOLD</div>
</div>
<div class="med-vspace resp-sm-hspace-rt-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-darkgreen.jpg"}}">
<div class="align-ctr sm-copy">DARK GREEN</div>
</div>
<div class="med-vspace hspace-rt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-burgundy.jpg"}}">
<div class="align-ctr sm-copy">BURGUNDY</div>
</div>
<div class="med-vspace hspace-rt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/swatch-brown.jpg"}}">
<div class="align-ctr sm-copy">BROWN</div>


</div>

</div> <!-- END LEFT SIDE -->

<div class="resp-float-rt-767 center-767 rt-side floatFix"> <!-- RIGHT SIDE -->
<div class="float-rt floatFix">
<div class="med-vspace float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/straight-line-classic-block.jpg"}}">
</div>
<div class="med-vspace hspace-lt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/straight-line-full-block.jpg"}}">
</div>
<div class="med-vspace hspace-lt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/straight-line-select-script.jpg"}}">
</div>

</div>
<div class="float-rt floatFix">
<div class="med-vspace float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/straight-line-goudy-bold.jpg"}}">
</div>
<div class="med-vspace hspace-lt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/straight-line-woodwork.jpg"}}">
</div>
<div class="med-vspace hspace-lt-5-6-480 float-lt">
<img src="{{media url="wysiwyg/customization-page-imgs/straight-line-guard-script.jpg"}}">
</div>
</div>
</div> <!-- END RIGHT SIDE -->
</div> 
<!-- END STRAIGHT LINE -->

<div class="clear"></div>
<hr style="color:#dddddd;margin:10px 0;" />

<!-- LOGO EMBROIDERY -->
<div id="logoEmbroidery" class="floatFix">  
<h3>LOGO EMBROIDERY</h3>
<p>Our team can digitize your logo for a one-time setup charge that includes up to nine colors. Or, if you have one, we can work from an existing file. <span class="bold">Email your logo (TIF, BMP, JPG, GIF, EPS, or PDF) to <a>logo@happychefuniforms.com</a></span></p>

<table id="logo-table" width="100%"> <!-- WRAPPING TABLE ACCOMMODATES LEFT SIDE IMAGE -->
<tbody>
<tr>
<td class="image-cell hide-960"><img src="{{media url="wysiwyg/customization-page-imgs/logo-embroidery-p1.jpg"}}" alt="" /></td> <!-- HOLDS IMAGE -->

<td style="padding:0 !important;"> <!-- PRICING TABLE WRAP -->

<div class="tbl-float1">
<table class="data-table">
<thead>
<tr class="headrow odd">
<td><div><strong>Number of<br />Stitches</strong></div></td>
<td><div><strong>Up to<br />7,000</strong></div></td>
<td><div><strong>7,001 -<br />12,000</strong></div></td>
<td><div><strong>12,001 -<br />17,000</strong></div></td>
<td><div><strong>17,001 -<br />23,000</strong></div></td>
<td><div><strong>23,001 -<br />29,000</strong></div></td>
<td><div><strong>29,001 -<br />35,000</strong></div></td>
<td><div><strong>35,001 -<br />41,000</strong></div></td>
<td><div><strong>41,000 -<br />50,000</strong></div></td>
</tr>
</thead>

<tbody>
<tr class="even">
<td class="mobile-th1"><div><span class="datatable-mobileheader1">Number of<br />Stitches</span>Unit price<br />Q<span class="hide-480">uanti</span>ty 1 - 10</div></td>
<td><span class="datatable-mobileheader2">Up to 7,000</span><span class="singleLine">$6.50</span></td>
<td><span class="datatable-mobileheader">7,001 -<br />12,000</span><span class="singleLine">$8.50</span></td>
<td><span class="datatable-mobileheader">12,001 -<br />17,000</span><span class="singleLine">$12.50</span></td>
<td><span class="datatable-mobileheader">17,001 -<br />23,000</span><span class="singleLine">$15.50</span></td>
<td><span class="datatable-mobileheader">23,001 -<br />29,000</span><span class="singleLine">$19.50</span></td>
<td><span class="datatable-mobileheader">29,001 -<br />35,000</span><span class="singleLine">$22.50</span></td>
<td><span class="datatable-mobileheader">35,001 -<br />41,000</span><span class="singleLine">$25.50</span></td>
<td class="last"><span class="datatable-mobileheader">41,000 -<br />50,000</span><span class="singleLine">$29.50</span></td>
</tr>
</tbody>
</table>
</div>

<div class="tbl-float2">
<table class="data-table">
<thead>
<tr class="odd">
<td><div>Unit price<br />Q<span class="hide-480">uanti</span>ty 11+</div></td>
<td class="singleLine"><div>$4.50</div></td>
<td class="singleLine"><div>$6.50</div></td>
<td class="singleLine"><div>$10.50</div></td>
<td class="singleLine"><div>$13.50</div></td>
<td class="singleLine"><div>$17.50</div></td>
<td class="singleLine"><div>$20.50</div></td>
<td class="singleLine"><div>$23.50</div></td>
<td class="singleLine"><div>$27.50</div></td>
</tr>
</thead>

<tbody>
<tr class="even">
<td class="mobile-th2"><div><span class="datatable-mobileheader1">Unit price<br />Q<span class="hide-480">uanti</span>ty 11+</span>Setup Charge</div></td>
<td class="singleLine"><span class="datatable-mobileheader3">$4.50</span>$95</td>
<td class="singleLine"><span class="datatable-mobileheader3">$6.50</span>$95</td>
<td class="singleLine"><span class="datatable-mobileheader3">$10.50</span>$125</td>
<td class="singleLine"><span class="datatable-mobileheader3">$13.50</span>$150</td>
<td class="singleLine"><span class="datatable-mobileheader3">$17.50</span>$150</td>
<td class="singleLine"><span class="datatable-mobileheader3">$20.50</span>$200</td>
<td class="singleLine"><span class="datatable-mobileheader3">$23.50</span>$200</td>
<td class="last singleLine"><span class="datatable-mobileheader3">$27.50</span>$200</td>
</tr>
</tbody>
</table>
</div>

</td> <!-- END PRICING TABLE WRAP -->
</tr>
</tbody>
</table> <!-- END WRAPPING TABLE -->
</div> 

<div class="row-vspace-10 floatFix">
<div class="one-half-to-full-767">
<h4>ACF Members Exclusive</h4>
<p>Officially licensed and updated each year, the embroidered logo of the American Culinary Federation can easily be added to your Happy Chef merchandise.</p>
</div>
<div class="one-half-to-full-767 omega">
<img class="float-rt" style="width:100%;" src="{{media url="wysiwyg/customization-page-imgs/ACF-p1.jpg"}}" />
</div>
</div>
<!-- END LOGO EMBROIDERY -->

<div class="clear"></div>
<hr style="color:#dddddd;margin:10px 0;" />

<!-- FLAGS AND RIBBONS -->
<div id="flagsAndRibbons">
<h3>FLAGS AND RIBBONS</h3>
<p>Show off your culinary heritage with colorful ribbons and flags. Patriotic designs let customers know their food was prepared with pride. Available on collars, cuffs, and some hats. Flags are &frac34;" x 1 &frac34;". Ribbons are &frac34;" high. <span class="bold">$4.99 per flag/ribbon</span></p>

<div class="floatFix"> <!-- TOP -->
<div class="one_third hide-960 lt-side"> <!-- TOP LEFT -->
<img style="display:block;margin-bottom:5px;" src="{{media url="wysiwyg/customization-page-imgs/flags-p1.jpg"}}">
</div> <!-- END TOP LEFT -->

<div class="one-third-to-one-half middle floatFix resp-lg-vspace-top-960"> <!-- TOP MIDDLE -->
<div> <!-- ROW 1 TOP MIDDLE -->
<div class="med-vspace float-lt" style="width:31%;margin-right:3.5%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-usa.jpg"}}">
<div class="align-ctr sm-copy">USA</div>
</div>
<div class="med-vspace float-lt" style="width:31%;margin-right:3.5%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-canada.jpg"}}">
<div class="align-ctr sm-copy">Canada</div>
</div>
<div class="med-vspace float-lt" style="width:31%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-france.jpg"}}">
<div class="align-ctr sm-copy">France</div>
</div>
</div> <!-- END ROW 1 TOP MIDDLE -->

<div class="one-third-to-one-half floatFix"> <!-- ROW 2 TOP MIDDLE -->
<div class="med-vspace float-lt" style="width:31%;margin-right:3.5%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-italy.jpg"}}">
<div class="align-ctr sm-copy">Italy</div>
</div>
<div class="med-vspace float-lt" style="width:31%;margin-right:3.5%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-england.jpg"}}">
<div class="align-ctr sm-copy">England</div>
</div>
<div class="med-vspace float-lt" style="width:31%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-germany.jpg"}}">
<div class="align-ctr sm-copy">Germany</div>
</div>
</div> <!-- END ROW 2 TOP MIDDLE -->
</div> <!-- END TOP MIDDLE -->

<div class="one-third-to-one-half omega rt-side floatFix resp-lg-vspace-top-960"> <!-- RIGHT -->
<div class="med-vspace float-lt" style="width:31%;margin-right:3.5%;"> <!-- ROW 1 RIGHT -->
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-switzerland.jpg"}}">
<div class="align-ctr sm-copy">Switzerland</div>
</div>
<div class="med-vspace float-lt" style="width:31%;margin-right:3.5%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-mexico.jpg"}}">
<div class="align-ctr sm-copy">Mexico</div>
</div>
<div class="med-vspace float-lt" style="width:31%;">
<img class="flag" src="{{media url="wysiwyg/customization-page-imgs/flag-puertorico.jpg"}}">
<div class="align-ctr sm-copy">Puerto Rico</div>
</div> 
</div> <!-- END RIGHT -->
</div> <!-- END TOP -->

<div class="floatFix"> <!-- BOTTOM -->
<div class="one_third hide-960 floatFix"> <!-- BOTTOM LEFT -->
<div>
<img style="display:block;" src="{{media url="wysiwyg/customization-page-imgs/ribbons-p1.jpg"}}">
</div>
</div> <!-- END BOTTOM LEFT -->

<div class="one-third-to-one-half floatFix resp-lg-vspace-top-960"> <!-- BOTTOM MIDDLE -->
<div class="med-vspace">
<img class="ribbon" src="{{media url="wysiwyg/customization-page-imgs/ribbon-stars-and-stripes.jpg"}}">
<div class="align-ctr sm-copy">Stars and Stripes</div>
</div>
<div class="med-vspace">
<img class="ribbon" src="{{media url="wysiwyg/customization-page-imgs/ribbon-usa-gold-lurex.jpg"}}">
<div class="align-ctr sm-copy">USA (Gold Lurex)</div>
</div>
</div> <!-- END BOTTOM MIDDLE -->


<div class="one-third-to-one-half omega floatFix resp-lg-vspace-top-960"> <!-- BOTTOM RIGHT -->
<div class="med-vspace">
<img class="ribbon" src="{{media url="wysiwyg/customization-page-imgs/ribbon-red-white-blue.jpg"}}">
<div class="align-ctr sm-copy">Red/White/Blue</div>
</div>
<div class="med-vspace">
<img class="ribbon" src="{{media url="wysiwyg/customization-page-imgs/ribbon-grn-white-red.jpg"}}">
<div class="align-ctr sm-copy">Green/White/Red</div>
</div>
</div> <!-- END BOTTOM RIGHT -->

</div> <!-- END BOTTOM -->
</div> 
<!-- END FLAGS AND RIBBONS -->

<div class="clear"></div>

<div class="lg-vspace">
<p>Please note: All customized items have been altered and cannot be returned. If you have any questions or would like a Happy Chef representative to help you place your order, please call us at 800.347.0288, 9AM-5:30PM EST Monday through Friday. Calling from outside the United States? Contact us at +1 973.492.2525. We are happy to take your call.</p>
</div>


</div> <!-- /content -->

EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'HappyChef Home page',
	'identifier' 		=> 'em0080_home',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<div style="display:none"> aa</div>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();

$dataPage = array(
	'title'				=> 'EM0080 Shopping Typography',
	'identifier' 		=> 'em0080_typography',
	'stores'			=> $stores,
	'content_heading'	=> 'Typography',
	'content'			=> <<<EOB
		<h2>General Elements</h2>
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<ul>
<li>Bullet List 1</li>
<ul>
<li>Bullet List 1.1</li>
<li>Bullet List 1.2</li>
<li>Bullet List 1.3</li>
<li>Bullet List 1.4</li>
</ul>
<li>Bullet List 2</li>
<li>Bullet List 3</li>
<li>Bullet List 4</li>
</ul>
<ol>
<li>Number List 1</li>
<ol>
<li>Number List 1.1</li>
<li>Number List 1.2</li>
<li>Number List 1.3</li>
<li>Number List 1.4</li>
</ol>
<li>Number List 2</li>
<li>Number List 3</li>
<li>Number List 4</li>
</ol><dl><dt>Definition title dt</dt><dd>Defination description dd</dd><dt>Definition title dt</dt><dd>Defination description dd</dd></dl>
<p><code>Code tag:&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</code></p>
<blockquote>
<p>block quote&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</blockquote>
<div class="box f-left">element with class <strong>.f-left</strong></div>
<div class="box f-right">element with class <strong>.f-right</strong></div>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<h2>Tables</h2>
<p>Table with class <strong>.data-table</strong></p>
<table class="data-table" style="width: 100%;" border="0">
<thead>
<tr><th>THEAD TH</th><th>THEAD TH</th><th>THEAD TH</th><th>THEAD TH</th><th>THEAD TH</th></tr>
</thead>
<tbody>
<tr>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
</tr>
<tr>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
</tr>
<tr>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
<td>TBODY TD</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2>Custom CSS Classes</h2>
<p class="normal">this is a paragraph with class <strong>.normal</strong></p>
<p class="primary">this is a paragraph with class <strong>.primary</strong></p>
<p class="secondary">this is a paragraph with class <strong>.secondary</strong></p>
<p class="secondary2">this is a paragraph with class <strong>.secondary2</strong></p>
<p class="small">tag <strong>small</strong> and class <strong>.small</strong></p>
<p class="underline">element with class <strong>.underline</strong></p>
<p><strong>ul.none</strong> and <strong>ol.none</strong>:</p>
<ul class="none">
<li>Bullet List 1</li>
<ul>
<li>Bullet List 1.1</li>
<li>Bullet List 1.2</li>
<li>Bullet List 1.3</li>
<li>Bullet List 1.4</li>
</ul>
<li>Bullet List 2</li>
<li>Bullet List 3</li>
<li>Bullet List 4</li>
</ul>
<p><strong>ul.hoz</strong> and <strong>ol.hoz</strong>:</p>
<ul class="hoz">
<li>Bullet List 1</li>
<li>Bullet List 2</li>
<li>Bullet List 3</li>
<li>Bullet List 4</li>
</ul>
<div class="box">
<p>paragraph with class <strong>.box</strong>:</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>
<p class="bottom">Paragraph with class <strong>.bottom</strong> always has margin-bottom = 0.</p>
<p>Add class <strong>.hide-lte2</strong> to hide element when screen's width less than 1280px.</p>
<p class="box hide-lte2">This block will disappear when resize window less than 1280px</p>
<p>Add class <strong>.hide-lte1</strong> to hide element when screen's width less than 980px.</p>
<p class="box hide-lte1">This block will disappear when resize window less than 980px</p>
<p>Add class <strong>.hide-lte0</strong> to hide element when screen's width less than 760px.</p>
<p class="box hide-lte0">This block will disappear when resize window less than 760px</p>
<h2>Icons</h2>
<table class="data-table" border="0">
<tbody>
<tr>
<td align="center" valign="top" width="20%">
<p>.icon.facebook</p>
<p><span class="icon facebook">span.icon.facebook</span></p>
</td>
<td align="center" valign="top" width="20%">
<p>.icon.twitter</p>
<p><span class="icon twitter">span.icon.twitter</span></p>
</td>
<td align="center" valign="top" width="20%">
<p>.icon.rss</p>
<p><span class="icon rss">span.icon.rss</span></p>
</td>
<td align="center" valign="top" width="20%">
<p>.icon.fr</p>
<p><span class="icon fr">span.icon.fr</span></p>
</td>
<td align="center" valign="top" width="20%">
<p>.icon.blogger</p>
<p><span class="icon blogger">span.icon.blogger</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table class="data-table" border="0">
<tbody>
<tr>
<td style="background: #f36f7c; color:#fff;" align="center" valign="top">
<p>.icon.cart</p>
<p><span class="icon cart">span.icon.cart</span></p>
</td>
<td align="center" valign="top">
<p>.icon.search</p>
<p><span class="icon search">span.icon.search</span></p>
</td>
<td align="center" valign="top">
<p>.icon.phone</p>
<p><span class="icon phone">span.icon.phone</span></p>
</td>
<td align="center" valign="top">
<p>.icon.email</p>
<p><span class="icon email">span.icon.email</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table class="data-table" border="0">
<tbody>
<tr>
<td align="center" valign="top">
<p>.icon.gift</p>
<p><span class="icon gift">span.icon.gift</span></p>
</td>
<td align="center" valign="top">
<p>.icon.refund</p>
<p><span class="icon refund">span.icon.refund</span></p>
</td>
<td align="center" valign="top">
<p>.icon.free_ship</p>
<p><span class="icon free_ship">span.icon.free_ship</span></p>
</td>
<td align="center" valign="top">
<p>.icon.star</p>
<p><span class="icon star">span.icon.star</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>image with class <strong>.fluid</strong>:</p>
<p><img class="fluid" src="{{media url="wysiwyg/em0080/cate_banner.png"}}" alt="" /></p>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'EM0080 Shopping Widgets',
	'identifier' 		=> 'em0080_widgets',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
<h2>Demo EM Slideshow Widget</h2>
<p>{{widget type="slideshow2/slideshow2" slideshow="1"}}</p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<h2>Demo EM Featured Products Widget</h2>
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;" valign="top">
<h3>Grid View</h3>
<p>{{widget type="dynamicproducts/dynamicproducts" order_by="name asc" new_category="3" featured_choose="em_featured" limit_count="10" item_width="250" item_height="370" item_spacing="30" thumbnail_width="250" thumbnail_height="250" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="false" show_price="true" show_reviews="false" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_featured_products/featured_grid.phtml"}}</p>
</td>
<td style="width: 50%;" valign="top">
<h3>Grid View with column count = 3</h3>
<p>{{widget type="dynamicproducts/dynamicproducts" order_by="name asc" new_category="3" featured_choose="em_featured" limit_count="10" column_count="3" item_width="175" item_spacing="21" thumbnail_width="173" thumbnail_height="173" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="false" show_price="true" show_reviews="false" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_featured_products/featured_grid.phtml"}}</p>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 48%; padding-right: 2%;" valign="top">
<h3>List View</h3>
<p>{{widget type="dynamicproducts/dynamicproducts" order_by="name asc" new_category="3" featured_choose="em_featured" limit_count="10" thumbnail_width="150" thumbnail_height="150" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="true" show_price="true" show_reviews="true" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_featured_products/featured_list.phtml"}}</p>
</td>
<td style="width: 50%;" valign="top">
<h3>List View with column count = 2</h3>
<p>{{widget type="dynamicproducts/dynamicproducts" order_by="name asc" new_category="3" featured_choose="em_featured" limit_count="10" column_count="2" thumbnail_width="150" thumbnail_height="150" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="true" show_price="true" show_reviews="true" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_featured_products/featured_list.phtml"}}</p>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<hr />
<p>&nbsp;</p>
<h2>Demo EM Bestseller Products Widget</h2>
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;" valign="top">
<h3>Grid View</h3>
<p>{{widget type="bestsellerproducts/list" new_category="3" order_by="name asc" limit_count="10" column_count="4" frontend_title="Bestseller Products" item_width="250" item_height="370" item_spacing="30" thumbnail_width="250" thumbnail_height="250" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="false" show_price="true" show_reviews="false" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_bestseller_products/bestseller_grid.phtml"}}</p>
</td>
<td style="width: 50%;" valign="top">
<h3>List View</h3>
<p>{{widget type="bestsellerproducts/list" new_category="3" order_by="name asc" limit_count="10" column_count="4" frontend_title="Bestseller Products" thumbnail_width="150" thumbnail_height="150" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="true" show_price="true" show_reviews="true" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_bestseller_products/bestseller_list.phtml"}}</p>
<div>&nbsp;</div>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<h2>&nbsp;Demo EM New Products Widget</h2>
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;" valign="top">
<h3>Grid View</h3>
<p>{{widget type="newproducts/list" limit_count="10" column_count="4" order_by="name asc" frontend_title="New Products" item_width="250" item_height="370" item_spacing="30" thumbnail_width="250" thumbnail_height="250" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="false" show_price="true" show_reviews="false" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_new_products/new_grid.phtml"}}</p>
</td>
<td style="width: 50%;" valign="top">
<h3>List View</h3>
<p>{{widget type="newproducts/list" limit_count="10" column_count="4" order_by="name asc" frontend_title="New Products" thumbnail_width="150" thumbnail_height="150" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="true" show_price="true" show_reviews="true" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_new_products/new_list.phtml"}}</p>
<div>&nbsp;</div>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<h2>&nbsp;Demo EM Sale Products Widget</h2>
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;" valign="top">
<h3>Grid View</h3>
<p>{{widget type="saleproducts/list" order_by="name asc" limit_count="10" column_count="4" frontend_title="Sale Products" item_width="250" item_height="400" item_spacing="30" thumbnail_width="250" thumbnail_height="250" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="false" show_price="true" show_reviews="true" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_sale_products/sale_grid.phtml"}}</p>
</td>
<td style="width: 50%;" valign="top">
<h3>List View</h3>
<p>{{widget type="saleproducts/list" order_by="name asc" limit_count="10" column_count="4" frontend_title="Sale Products" thumbnail_width="150" thumbnail_height="150" show_product_name="true" show_thumbnail="true" alt_img="thumbnail" show_description="true" show_price="true" show_reviews="true" show_addtocart="true" show_addto="true" show_label="true" choose_template="em_sale_products/sale_list.phtml"}}</p>
<div>&nbsp;</div>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<h2>&nbsp;Demo EM Flexible Widget</h2>
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;" valign="top">
<h3>Grid View</h3>
<p>{{widget type="flexiblewidget/list" column_count="2" limit_count="10" order_by="name asc" category="category/10" alt_img="thumbnail" frontend_title="Flexible widget" choose_template="flexiblewidget/grid.phtml"}}</p>
</td>
<td style="width: 50%;" valign="top">
<h3>List View</h3>
<p>{{widget type="flexiblewidget/list" column_count="4" limit_count="10" order_by="name asc" category="category/10" alt_img="thumbnail" frontend_title="Flexible widget" choose_template="flexiblewidget/list.phtml"}}</p>
<div>&nbsp;</div>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<h2>Demo EM Products Filter Widget</h2>
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 48%; padding-right: 4%;" valign="top">
<h3>Grid View</h3>
<p>{{widget type="productsfilterwidget/productsfilterwidget" cache_time="10" col_count="3" limit_count="10" product_width="170" product_height="300" sort_by="value" sort_direction="ASC" alt_img="thumbnail" toolbar="1" choose_template="em_productsfilterwidget/grid.phtml" special="1" conditions="YTo3OntzOjQ6InR5cGUiO3M6NDA6InNhbGVzcnVsZS9ydWxlX2NvbmRpdGlvbl9wcm9kdWN0X2NvbWJpbmUiO3M6OToiYXR0cmlidXRlIjtOO3M6ODoib3BlcmF0b3IiO047czo1OiJ2YWx1ZSI7czoxOiIxIjtzOjE4OiJpc192YWx1ZV9wcm9jZXNzZWQiO047czoxMDoiYWdncmVnYXRvciI7czozOiJhbGwiO3M6MTA6ImNvbmRpdGlvbnMiO2E6MTp7aTowO2E6NTp7czo0OiJ0eXBlIjtzOjMyOiJzYWxlc3J1bGUvcnVsZV9jb25kaXRpb25fcHJvZHVjdCI7czo5OiJhdHRyaWJ1dGUiO3M6Njoic3RhdHVzIjtzOjg6Im9wZXJhdG9yIjtzOjI6Ij09IjtzOjU6InZhbHVlIjtzOjE6IjEiO3M6MTg6ImlzX3ZhbHVlX3Byb2Nlc3NlZCI7YjowO319fQ,,-1373871983"}}</p>
</td>
<td style="width: 48%;" valign="top">
<h3>List View</h3>
<p>{{widget type="productsfilterwidget/productsfilterwidget" cache_time="10" col_count="3" limit_count="10" product_width="170" product_height="170" sort_by="value" sort_direction="ASC" alt_img="thumbnail" toolbar="2" choose_template="em_productsfilterwidget/listing.phtml" special="1" conditions="YTo3OntzOjQ6InR5cGUiO3M6NDA6InNhbGVzcnVsZS9ydWxlX2NvbmRpdGlvbl9wcm9kdWN0X2NvbWJpbmUiO3M6OToiYXR0cmlidXRlIjtOO3M6ODoib3BlcmF0b3IiO047czo1OiJ2YWx1ZSI7czoxOiIxIjtzOjE4OiJpc192YWx1ZV9wcm9jZXNzZWQiO047czoxMDoiYWdncmVnYXRvciI7czozOiJhbGwiO3M6MTA6ImNvbmRpdGlvbnMiO2E6MTp7aTowO2E6NTp7czo0OiJ0eXBlIjtzOjMyOiJzYWxlc3J1bGUvcnVsZV9jb25kaXRpb25fcHJvZHVjdCI7czo5OiJhdHRyaWJ1dGUiO3M6Njoic3RhdHVzIjtzOjg6Im9wZXJhdG9yIjtzOjI6Ij09IjtzOjU6InZhbHVlIjtzOjE6IjEiO3M6MTg6ImlzX3ZhbHVlX3Byb2Nlc3NlZCI7YjowO319fQ,,-1373872013"}}</p>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2>&nbsp;Demo EM Slider Widget</h2>
<div class="grid_12 alpha">
<h3>Vertical Sliding</h3>
<div>{{widget type="sliderwidget/slide" instance="47" direction="1" container=".products-grid" slider_height="250" circular="1" items_per_slide="1"}}</div>
</div>
<div class="grid_12 omega">
<h3>Horizontal Sliding</h3>
<div>{{widget type="sliderwidget/slide" instance="47" container=".products-grid" circular="1" items_per_slide="1"}}</div>
</div>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<h2>&nbsp;Demo EM Tabs Widget</h2>
<p>{{widget type="tabs/group" title_1="YTo0OntpOjA7czo1OiJUYWIgMSI7aToxO3M6MDoiIjtpOjM7czowOiIiO2k6MjtzOjA6IiI7fQ==" block_1="18" title_2="YTo0OntpOjA7czo1OiJUYWIgMiI7aToxO3M6MDoiIjtpOjM7czowOiIiO2k6MjtzOjA6IiI7fQ==" block_2="18" title_3="YTo0OntpOjA7czo1OiJUYWIgMyI7aToxO3M6MDoiIjtpOjM7czowOiIiO2k6MjtzOjA6IiI7fQ==" block_3="18" template="emtabs/group.phtml"}}</p>
<p>&nbsp;</p>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Enable Cookies',
	'identifier' 		=> 'enable-cookies-'.$name,
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<div class="std">
    <ul class="messages">
        <li class="notice-msg">
            <ul>
                <li>Please enable cookies in your web browser to continue.</li>
            </ul>
        </li>
    </ul>
    <div class="page-title">
        <h1><a name="top"></a>What are Cookies?</h1>
    </div>
    <p>Cookies are short pieces of data that are sent to your computer when you visit a website. On later visits, this data is then returned to that website. Cookies allow us to recognize you automatically whenever you visit our site so that we can personalize your experience and provide you with better service. We also use cookies (and similar browser data, such as Flash cookies) for fraud prevention and other purposes. If your web browser is set to refuse cookies from our website, you will not be able to complete a purchase or take advantage of certain features of our website, such as storing items in your Shopping Cart or receiving personalized recommendations. As a result, we strongly encourage you to configure your web browser to accept cookies from our website.</p>
    <h2 class="subtitle">Enabling Cookies</h2>
    <ul class="disc">
        <li><a href="#ie7">Internet Explorer 7.x</a></li>
        <li><a href="#ie6">Internet Explorer 6.x</a></li>
        <li><a href="#firefox">Mozilla/Firefox</a></li>
        <li><a href="#opera">Opera 7.x</a></li>
    </ul>
    <h3><a name="ie7"></a>Internet Explorer 7.x</h3>
    <ol>
        <li>
            <p>Start Internet Explorer</p>
        </li>
        <li>
            <p>Under the <strong>Tools</strong> menu, click <strong>Internet Options</strong></p>
            <p><img src="{{skin url="images/cookies/ie7-1.gif"}}" alt="" /></p>
        </li>
        <li>
            <p>Click the <strong>Privacy</strong> tab</p>
            <p><img src="{{skin url="images/cookies/ie7-2.gif"}}" alt="" /></p>
        </li>
        <li>
            <p>Click the <strong>Advanced</strong> button</p>
            <p><img src="{{skin url="images/cookies/ie7-3.gif"}}" alt="" /></p>
        </li>
        <li>
            <p>Put a check mark in the box for <strong>Override Automatic Cookie Handling</strong>, put another check mark in the <strong>Always accept session cookies </strong>box</p>
            <p><img src="{{skin url="images/cookies/ie7-4.gif"}}" alt="" /></p>
        </li>
        <li>
            <p>Click <strong>OK</strong></p>
            <p><img src="{{skin url="images/cookies/ie7-5.gif"}}" alt="" /></p>
        </li>
        <li>
            <p>Click <strong>OK</strong></p>
            <p><img src="{{skin url="images/cookies/ie7-6.gif"}}" alt="" /></p>
        </li>
        <li>
            <p>Restart Internet Explore</p>
        </li>
    </ol>
    <p class="a-top"><a href="#top">Back to Top</a></p>
    <h3><a name="ie6"></a>Internet Explorer 6.x</h3>
    <ol>
        <li>
            <p>Select <strong>Internet Options</strong> from the Tools menu</p>
            <p><img src="{{skin url="images/cookies/ie6-1.gif"}}" alt="" /></p>
        </li>
        <li>
            <p>Click on the <strong>Privacy</strong> tab</p>
        </li>
        <li>
            <p>Click the <strong>Default</strong> button (or manually slide the bar down to <strong>Medium</strong>) under <strong>Settings</strong>. Click <strong>OK</strong></p>
            <p><img src="{{skin url="images/cookies/ie6-2.gif"}}" alt="" /></p>
        </li>
    </ol>
    <p class="a-top"><a href="#top">Back to Top</a></p>
    <h3><a name="firefox"></a>Mozilla/Firefox</h3>
    <ol>
        <li>
            <p>Click on the <strong>Tools</strong>-menu in Mozilla</p>
        </li>
        <li>
            <p>Click on the <strong>Options...</strong> item in the menu - a new window open</p>
        </li>
        <li>
            <p>Click on the <strong>Privacy</strong> selection in the left part of the window. (See image below)</p>
            <p><img src="{{skin url="images/cookies/firefox.png"}}" alt="" /></p>
        </li>
        <li>
            <p>Expand the <strong>Cookies</strong> section</p>
        </li>
        <li>
            <p>Check the <strong>Enable cookies</strong> and <strong>Accept cookies normally</strong> checkboxes</p>
        </li>
        <li>
            <p>Save changes by clicking <strong>Ok</strong>.</p>
        </li>
    </ol>
    <p class="a-top"><a href="#top">Back to Top</a></p>
    <h3><a name="opera"></a>Opera 7.x</h3>
    <ol>
        <li>
            <p>Click on the <strong>Tools</strong> menu in Opera</p>
        </li>
        <li>
            <p>Click on the <strong>Preferences...</strong> item in the menu - a new window open</p>
        </li>
        <li>
            <p>Click on the <strong>Privacy</strong> selection near the bottom left of the window. (See image below)</p>
            <p><img src="{{skin url="images/cookies/opera.png"}}" alt="" /></p>
        </li>
        <li>
            <p>The <strong>Enable cookies</strong> checkbox must be checked, and <strong>Accept all cookies</strong> should be selected in the &quot;<strong>Normal cookies</strong>&quot; drop-down</p>
        </li>
        <li>
            <p>Save changes by clicking <strong>Ok</strong></p>
        </li>
    </ol>
    <p class="a-top"><a href="#top">Back to Top</a></p>
</div>

EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Gift Cards',
	'identifier' 		=> 'gift_card',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<div class="gift-card">
<div class="one"><img src="{{media url="wysiwyg/gift-cart-header.jpg"}}" alt="" /></div>
<div class="two">
<ul>
	<li class="first"><span>no fees</span></li>
	<li><span>no expiration</span></li>
	<li><span>from 25$ to $500</span></li>
	<li class="last"><span>redeem online or by phone</span></li>
</ul>
</div>
<div class="three">
<div class="left-card"><a href="{{store direct_url="thegift.html"}}">Email Delivery</a></div>
<div class="right-card"><a href="{{store direct_url="thegiftcard2.html"}}">MailDelivery</a></div>
</div>
</div>
{{block type="catalog/product_list" category_id="62" template="catalog/product/list_giftcard.phtml"}}

<div>
<div class="four">
	<div>Already have a gift card?</div>
	<a href="#">Check your balance</a>
</div>
<div class="five">
	<p>Happy Chef gift cards are not redeemable for cash and cannot be replaced if lost or stolen. Gift cards are valid toward both online and offline purchases. All orders are subject to review in cases of willingly abusive intent. Shipping to US only.</p>
</div>
</div>

EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'New_Events',
	'identifier' 		=> 'new-events',
	'stores'			=> $stores,
	'content_heading'	=> 'New and Events',
	'content'			=> <<<EOB
		<p>Insert your new product and events in this page</p>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Order Status',
	'identifier' 		=> 'order-status-'.$name,
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<div class="page-title">
<h1>Order Status</h1>
</div>
<div class="std">
<p>To track your order, please enter the e-mail address used to place the order and your confirmation number below. You&rsquo;ll find your confirmation number in your order confirmation e-mail, as well as in your account&rsquo;s receipt page and on your physical order summary page.</p>
<p>{{block type="core/template" template="cms/order-status-api.phtml"}}</p>
<p>For more information about your Happy Chef orders and your account status, please&nbsp;<span><a href="customer/account/login/" target="_blank">sign in</a></span>&nbsp;to your account. If you don&rsquo;t already have an account, you can&nbsp;<span><a href="customer/account/create/" target="_blank">create one now</a></span>.</p>
</div>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();

$dataPage = array(
	'title'				=> 'Outlet Store',
	'identifier' 		=> 'outlet-store-'.$name,
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<p>This is Outlet Store page</p>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Payment and Pricing and Promos',
	'identifier' 		=> 'payment-pricing-promos',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
<div class="page-title">
<h1>Payment, Pricing, and Promotions</h1>
</div>
<div class="std">
<p>We accept payment 24/7 for all online orders. At this time, we accept credit card payment via American Express, Discover, MasterCard, and Visa.</p>
<p>If you&rsquo;d like to arrange a special order or simply prefer ordering by phone, feel free to call us between 9:00 AM and 5:30 PM (Eastern Time), Monday through Friday at 800-347-0288 or 973-492-2525. We can also be reached via fax at 973-492-0303.</p>
<p>Happy Chef reserves the right to cancel or amend orders placed on mistyped, mispriced, or otherwise mistakenly displayed merchandise. Please note that prices and availability are subject to change, especially for items with limited availability.</p>
</div>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();

$dataPage = array(
	'title'				=> 'Privacy Policy',
	'identifier' 		=> 'privacy-policy-cookie-restriction-mode'.$name,
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<p style="color: #ff0000; font-weight: bold; font-size: 13px">
    Please replace this text with you Privacy Policy.
    Please add any additional cookies your website uses below (e.g., Google Analytics)
</p>
<p>
    This privacy policy sets out how {{config path="general/store_information/name"}} uses and protects any information
    that you give {{config path="general/store_information/name"}} when you use this website.
    {{config path="general/store_information/name"}} is committed to ensuring that your privacy is protected.
    Should we ask you to provide certain information by which you can be identified when using this website,
    then you can be assured that it will only be used in accordance with this privacy statement.
    {{config path="general/store_information/name"}} may change this policy from time to time by updating this page.
    You should check this page from time to time to ensure that you are happy with any changes.
</p>
<h2>What we collect</h2>
<p>We may collect the following information:</p>
<ul>
    <li>name</li>
    <li>contact information including email address</li>
    <li>demographic information such as postcode, preferences and interests</li>
    <li>other information relevant to customer surveys and/or offers</li>
</ul>
<p>
    For the exhaustive list of cookies we collect see the <a href="#list">List of cookies we collect</a> section.
</p>
<h2>What we do with the information we gather</h2>
<p>
    We require this information to understand your needs and provide you with a better service,
    and in particular for the following reasons:
</p>
<ul>
    <li>Internal record keeping.</li>
    <li>We may use the information to improve our products and services.</li>
    <li>
        We may periodically send promotional emails about new products, special offers or other information which we
        think you may find interesting using the email address which you have provided.
    </li>
    <li>
        From time to time, we may also use your information to contact you for market research purposes.
        We may contact you by email, phone, fax or mail. We may use the information to customise the website
        according to your interests.
    </li>
</ul>
<h2>Security</h2>
<p>
    We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure,
    we have put in place suitable physical, electronic and managerial procedures to safeguard and secure
    the information we collect online.
</p>
<h2>How we use cookies</h2>
<p>
    A cookie is a small file which asks permission to be placed on your computer's hard drive.
    Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit
    a particular site. Cookies allow web applications to respond to you as an individual. The web application
    can tailor its operations to your needs, likes and dislikes by gathering and remembering information about
    your preferences.
</p>
<p>
    We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page traffic
    and improve our website in order to tailor it to customer needs. We only use this information for statistical
    analysis purposes and then the data is removed from the system.
</p>
<p>
    Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful
    and which you do not. A cookie in no way gives us access to your computer or any information about you,
    other than the data you choose to share with us. You can choose to accept or decline cookies.
    Most web browsers automatically accept cookies, but you can usually modify your browser setting
    to decline cookies if you prefer. This may prevent you from taking full advantage of the website.
</p>
<h2>Links to other websites</h2>
<p>
    Our website may contain links to other websites of interest. However, once you have used these links
    to leave our site, you should note that we do not have any control over that other website.
    Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst
    visiting such sites and such sites are not governed by this privacy statement.
    You should exercise caution and look at the privacy statement applicable to the website in question.
</p>
<h2>Controlling your personal information</h2>
<p>You may choose to restrict the collection or use of your personal information in the following ways:</p>
<ul>
    <li>
        whenever you are asked to fill in a form on the website, look for the box that you can click to indicate
        that you do not want the information to be used by anybody for direct marketing purposes
    </li>
    <li>
        if you have previously agreed to us using your personal information for direct marketing purposes,
        you may change your mind at any time by writing to or emailing us at
        {{config path="trans_email/ident_general/email"}}
    </li>
</ul>
<p>
    We will not sell, distribute or lease your personal information to third parties unless we have your permission
    or are required by law to do so. We may use your personal information to send you promotional information
    about third parties which we think you may find interesting if you tell us that you wish this to happen.
</p>
<p>
    You may request details of personal information which we hold about you under the Data Protection Act 1998.
    A small fee will be payable. If you would like a copy of the information held on you please write to
    {{config path="general/store_information/address"}}.
</p>
<p>
    If you believe that any information we are holding on you is incorrect or incomplete,
    please write to or email us as soon as possible, at the above address.
    We will promptly correct any information found to be incorrect.
</p>
<h2><a name="list"></a>List of cookies we collect</h2>
<p>The table below lists the cookies we collect and what information they store.</p>
<table class="data-table">
    <thead>
        <tr>
            <th>COOKIE name</th>
            <th>COOKIE Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>CART</th>
            <td>The association with your shopping cart.</td>
        </tr>
        <tr>
            <th>CATEGORY_INFO</th>
            <td>Stores the category info on the page, that allows to display pages more quickly.</td>
        </tr>
        <tr>
            <th>COMPARE</th>
            <td>The items that you have in the Compare Products list.</td>
        </tr>
        <tr>
            <th>CURRENCY</th>
            <td>Your preferred currency</td>
        </tr>
        <tr>
            <th>CUSTOMER</th>
            <td>An encrypted version of your customer id with the store.</td>
        </tr>
        <tr>
            <th>CUSTOMER_AUTH</th>
            <td>An indicator if you are currently logged into the store.</td>
        </tr>
        <tr>
            <th>CUSTOMER_INFO</th>
            <td>An encrypted version of the customer group you belong to.</td>
        </tr>
        <tr>
            <th>CUSTOMER_SEGMENT_IDS</th>
            <td>Stores the Customer Segment ID</td>
        </tr>
        <tr>
            <th>EXTERNAL_NO_CACHE</th>
            <td>A flag, which indicates whether caching is disabled or not.</td>
        </tr>
        <tr>
            <th>FRONTEND</th>
            <td>You sesssion ID on the server.</td>
        </tr>
        <tr>
            <th>GUEST-VIEW</th>
            <td>Allows guests to edit their orders.</td>
        </tr>
        <tr>
            <th>LAST_CATEGORY</th>
            <td>The last category you visited.</td>
        </tr>
        <tr>
            <th>LAST_PRODUCT</th>
            <td>The most recent product you have viewed.</td>
        </tr>
        <tr>
            <th>NEWMESSAGE</th>
            <td>Indicates whether a new message has been received.</td>
        </tr>
        <tr>
            <th>NO_CACHE</th>
            <td>Indicates whether it is allowed to use cache.</td>
        </tr>
        <tr>
            <th>PERSISTENT_SHOPPING_CART</th>
            <td>A link to information about your cart and viewing history if you have asked the site.</td>
        </tr>
        <tr>
            <th>POLL</th>
            <td>The ID of any polls you have recently voted in.</td>
        </tr>
        <tr>
            <th>POLLN</th>
            <td>Information on what polls you have voted on.</td>
        </tr>
        <tr>
            <th>RECENTLYCOMPARED</th>
            <td>The items that you have recently compared.            </td>
        </tr>
        <tr>
            <th>STF</th>
            <td>Information on products you have emailed to friends.</td>
        </tr>
        <tr>
            <th>STORE</th>
            <td>The store view or language you have selected.</td>
        </tr>
        <tr>
            <th>USER_ALLOWED_SAVE_COOKIE</th>
            <td>Indicates whether a customer allowed to use cookies.</td>
        </tr>
        <tr>
            <th>VIEWED_PRODUCT_IDS</th>
            <td>The products that you have recently viewed.</td>
        </tr>
        <tr>
            <th>WISHLIST</th>
            <td>An encrypted list of products added to your Wishlist.</td>
        </tr>
        <tr>
            <th>WISHLIST_CNT</th>
            <td>The number of items in your Wishlist.</td>
        </tr>
    </tbody>
</table>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Privacy and Security',
	'identifier' 		=> 'privacy-security-'.$name,
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}


<div class="custom customwrap">
<div class="align-ctr">
<h1>PRIVACY AND SECURITY</h1>
</div>
<div class="std"><p class="intro">The Happy Chef, Inc. collects customer information to improve your overall shopping experience. We respect your privacy and are committed to maintaining and using this information responsibly. Any personal information that you may share with us is kept absolutely private. Neither your name nor anything about you is sold or shared with any other non-affiliated company or agency. By visiting happychefuniforms.com you are accepting the practices described in this privacy policy.</p>
<h3 class="lg-vspace-top">YOUR INFORMATION</h3>
<p>Any personal information you provide to us on our web site when you register or update an account, shop online or request information (i.e. name, address, e-mail address, telephone number and credit card information) is maintained in private files on our secure web server and our internal systems. The Happy Chef, Inc. shares information about our customers with some of our affiliated companies and trusted service providers that need access to your information to provide operational or other support services. The Happy Chef, Inc. requires these service providers to maintain your information in the strictest confidence. Also we will not sell, rent or share your e-mail address with others for them to send unsolicited email.</p>
<p>In addition, we may also share statistical, demographic and other non-personally identifiable information with Happy Chef business partners to help manage and optimize our internet business and communications. We use the services of a selected marketing company, with which Happy Chef has a relationship, to help us measure the effectiveness of our advertising and how visitors use our site. While these do allow our marketing company to track your visits, the data retrieved cannot be used for any reason without our authorization.</p>
<p>If you are visiting happychefuniforms.com from a location outside of the United States, you will be connected to servers located within the United States. All happychefuniforms.com orders will be accepted and fully processed in and shipped from the United States. All information you provide will be securely maintained in our web server and internal systems located within the United States.</p>
<h3>COOKIES</h3>
<p>We use cookies to recognize you and your preferences to make shopping easier &amp; provide you with a customized experience. Cookies are bits of information that we automatically store on your computer if your computer accepts cookies. Cookies do not contain any personally identifiable information such as your name, address or any financial information on your computer. The Happy Chef, Inc. places a unique user ID number on your cookie, which allows us to access information that will improve your shopping experience - such as holding items in your shopping bag, accessing your address book, or providing you with customized pages based on your past viewing or purchasing preferences. We also use cookies to track traffic to different parts of the web site in order to make improvements &amp; optimize your shopping experience.</p>
<h3>REGISTERING</h3>
<p>Personal information and your order history are only available once you provide the correct password you created. After providing your password, you can edit or delete your personal information at any time through the My Account page.</p>
<p>If you forget your password, you can request that it be e-mailed to your e-mail address. We will not give passwords out over the phone, nor will we e-mail them to a different e-mail address other than the one used to register.</p>
<p><strong>Ordering </strong></p>
<p>When placing an order, give us your name, address, phone number and e-mail address, along with credit card information. This will create an account so you may place orders more conveniently. We do not store any credit card information.</p>
<h3>SECURITY</h3>
<p>We need your name and address in order to mail you a catalog. We also require a phone or e-mail contact in case the address is wrong. To stop receiving catalogs, simply call 800-347-0288.</p>
<p><strong>E-mail newsletters </strong></p>
<p>We need your e-mail address. It will never be sold to anyone and will be shared only with the company who sends our e-mail to help us understand your use of happychefuniforms.com.</p>
<p><strong>Shipping </strong></p>
<p>In order to ship and track merchandise, your name and address are available to such companies like United Parcel Service. In no instances are these business affiliates allowed to use your personal information for any other purpose.</p>
<p><strong>Security </strong></p>
<p>The security of your information is very important to us. We use the 128-bit Secure Socket Layer (SSL) technology to protect the security of your online order information. SSL technology encrypts your order information to protect it from being decoded by anyone other than The Happy Chef, Inc.</p>
<p>Once you enter the checkout page your computer will begin communicating with our server in secure mode. You can tell that you are in secure mode by the following: the "http" in the web address will be replaced by "https" Depending on the browser, you will see either a padlock (Microsoft Internet Explorer, AOL, Netscape Communicator) or a solid key (Netscape Navigator) in the lower section for the browser window.</p>
<p>Only browsers that use the 128-bit Secure Socket Layer technology are able to order through The Happy Chef, Inc. web site.</p>
<p>While we implement these and other security measures on this site, please note that 100% security is not always possible.</p>
<h3>POLICY CHANGES AND QUESTIONS</h3>
<p>The Happy Chef, Inc. may update our privacy policy from time to time. Whenever we make a change, we will post the updated policy on our site and we encourage you to check it periodically. By using our website you consent to the current policy. If you have any questions or concerns regarding our privacy policy, please contact us toll-free at 800-347-0288. We can also be reached via fax at 800- 492-0303, or e-mail: info@happychefuniforms.com</p>
<p>Our mailing address is:<br> Happy Chef Customer Relations <br> 22 Park Place <br> Butler, NJ 07405</p></div>
</div>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Request a Catalog',
	'identifier' 		=> 'requestcatalog',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}

<style>
#pg-cat-req-form-wrap .row-med-vspace {margin-top:15px;margin-bottom:15px;}
#pg-cat-req-form-wrap .cat-shadow {
   -moz-box-shadow:    -2px 0 6px -2px #BBBBBB inset;
   -webkit-box-shadow: -2px 0 6px -2px #BBBBBB inset;
   box-shadow:         -2px 0 6px -2px #BBBBBB inset;
}
</style>

<div id="pg-cat-req-form-wrap" class="custom customwrap">
<div class="floatFix"> <!-- WRAP -->
<div class="cat-signup-header-text"> <!-- TOP LEFT-->
<h1>GET YOUR FREE 2014 CATALOG</h1>
<h4>INCLUDES PRINT EDITION & DIGITAL DOWNLOAD</h4>
</div> 
<!-- /TOP LEFT -->

<div class="cat-signup-header"> <!-- LEFT SIDE -->
<div class="cat-signup-header-img"> <!-- BOTTOM LEFT -->
<img src="{{media url='wysiwyg/CatSignUp_LargeImage.jpg'}}">
</div> <!-- /BOTTOM LEFT -->
</div> <!-- /LEFT SIDE -->

<div class="cat-input-wrap omega"> <!-- RIGHT SIDE -->
<form id="Catalog" onsubmit="MM_validateForm('Email','','RisEmail','Name','','R','Address','','R','City','','R','State','','R','Zip','','R');return document.MM_returnValue" name="Catalog" method="post" action="/cs/catalog.cfm">
<div class="cat-input-rows-wrap">
<div class="row-vspace-10">
	<label class="required std-lbl" for="Name">Name</label>
    <input class="cat-input-field" id="Name" type="text" maxlength="50" value="" name="Name" />
</div>
<div class="row-vspace-10">
	<label class="std-lbl" for="Company">Company</label>
    <input class="cat-input-field" id="Company" type="text" maxlength="50" value="" name="Company" />
</div>
<div class="row-vspace-10">
	<label class="std-lbl" for="Title">Title</label>
    <input class="cat-input-field" id="Title" type="text" maxlength="25" value="" name="Title" />
</div>
<div class="row-vspace-10">
	<label class="required std-lbl" for="Address">Address 1</label>
    <input class="cat-input-field" id="Address" type="text" maxlength="50"value="" name="Address" />
</div>
<div class="row-vspace-10">
	<label class="std-lbl" for="Address2">Address 2</label>
    <input class="cat-input-field" id="Address2" type="text" maxlength="50" value="" name="Address2" />
</div>
<div class="row-vspace-10">
	<label class="required std-lbl" for="City">City</label>
    <input class="cat-input-field" id="City" type="text" maxlength="20" value="" name="City" />
</div>
<div class="row-vspace-10">
	<label class="required std-lbl" for="State" id="stateLbl">State</label>
    <select id="State" class="cat-input-field" maxlength="2" value="" name="State">
    	<option value="AL" title="Alabama">AL</option>
        <option value="AK" title="Alaska">AK</option>
        <option value="AZ" title="Arizona">AZ</option>
        <option value="AR" title="Arkansas">AR</option>
        <option value="CA" title="California">CA</option>
        <option value="CO" title="Colorado">CO</option>
        <option value="CT" title="Connecticut">CT</option>
        <option value="DE" title="Delaware">DE</option>
        <option value="DC" title="District of Columbia">DC</option>
        <option value="FL" title="Florida">FL</option>
        <option value="GA" title="Georgia">GA</option>
        <option value="HI" title="Hawaii">HI</option>
        <option value="ID" title="Idaho">ID</option>
        <option value="IL" title="Illinois">IL</option>
        <option value="IN" title="Indiana">IN</option>
        <option value="IA" title="Iowa">IA</option>
        <option value="KS" title="Kansas">KS</option>
        <option value="KY" title="Kentucky">KY</option>
        <option value="LA" title="Louisiana">LA</option>
        <option value="ME" title="Maine">ME</option>
        <option value="MD" title="Maryland">MD</option>
        <option value="MA" title="Massachusetts">MA</option>
        <option value="MI" title="Michigan">MI</option>
        <option value="MN" title="Minnesota">MN</option>
        <option value="MS" title="Mississippi">MS</option>
        <option value="MO" title="Missouri">MO</option>
        <option value="MT" title="Montana">MT</option>
        <option value="NE" title="Nebraska">NE</option>
        <option value="NV" title="Nevada">NV</option>
        <option value="NH" title="New Hampshire">NH</option>
        <option value="NJ" title="New Jersey">NJ</option>
        <option value="NM" title="New Mexico">NM</option>
        <option value="NY" title="New York">NY</option>
        <option value="NC" title="North Carolina">NC</option>
        <option value="ND" title="North Dakota">ND</option>
        <option value="OH" title="Ohio">OH</option>
        <option value="OK" title="Oklahoma">OK</option>
        <option value="OR" title="Oregon">OR</option>
        <option value="PA" title="Pennsylvania">PA</option>
        <option value="RI" title="Rhode Island">RI</option>
        <option value="SC" title="South Carolina">SC</option>
        <option value="SD" title="South Dakota">SD</option>
        <option value="TN" title="Tennessee">TN</option>
        <option value="TX" title="Texas">TX</option>
        <option value="UT" title="Utah">UT</option>
        <option value="VT" title="Vermont">VT</option>
        <option value="VA" title="Virginia">VA</option>
        <option value="WA" title="Washington">WA</option>
        <option value="WV" title="West Virginia">WV</option>
        <option value="WI" title="Wisconsin">WI</option>
        <option value="WY" title="Wyoming">WY</option>
    </select>

	<label class="required zip-lbl" for="Zip">ZIP Code</label>
    <input class="cat-input-field" id="Zip" type="text" maxlength="10" value="" name="Zip" />
</div>
<div class="row-vspace-10">
	<label class="required std-lbl" for="Country">Country</label>
    <select class="cat-input-field" size="1" name="Country" id="Country">
    	<option value="USA">United States</option>
		<option value="CAN">Canada</option>
		<option value="PRI">Puerto Rico</option>
		<option value="VIR">Virgin Islands (U.S.)</option>
    </select>
    <script>
	var countrySelect = document.getElementById('Country');
	var stateSelect = document.getElementById('State');
	countrySelect.onchange = (function(){
		stateSelect.options.length = 0;
		switch(countrySelect.value){
			case 'CAN':
				jQuery('#stateLbl').html('Province');
				stateSelect.options[0] = new Option('AB', 'AB');
				stateSelect.options[1] = new Option('BC', 'BC');
				stateSelect.options[2] = new Option('MB', 'MB');
				stateSelect.options[3] = new Option('NB', 'NB');
				stateSelect.options[4] = new Option('NL', 'NL');
				stateSelect.options[5] = new Option('NS', 'NS');
				stateSelect.options[6] = new Option('ON', 'ON');
				stateSelect.options[7] = new Option('PE', 'PE');
				stateSelect.options[8] = new Option('QC', 'QC');
				stateSelect.options[9] = new Option('SK', 'SK');
				stateSelect.options[10] = new Option('NT', 'NT');
				stateSelect.options[11] = new Option('NU', 'NU');
				stateSelect.options[12] = new Option('YT', 'YT');
				jQuery('#State option:first-child').attr({'title':'Alberta'});
				jQuery('#State option:nth-child(2)').attr({'title':'British Columbia'});
				jQuery('#State option:nth-child(3)').attr({'title':'Manitoba'});
				jQuery('#State option:nth-child(4)').attr({'title':'New Brunswick'});
				jQuery('#State option:nth-child(5)').attr({'title':'Newfoundland and Labrador'});
				jQuery('#State option:nth-child(6)').attr({'title':'Nova Scotia'});
				jQuery('#State option:nth-child(7)').attr({'title':'Ontario'});
				jQuery('#State option:nth-child(8)').attr({'title':'Prince Edward Island'});
				jQuery('#State option:nth-child(9)').attr({'title':'Quebec'});
				jQuery('#State option:nth-child(10)').attr({'title':'Saskatchewan'});
				jQuery('#State option:nth-child(11)').attr({'title':'Northwest Territories'});
				jQuery('#State option:nth-child(12)').attr({'title':'Nunavut'});
				jQuery('#State option:nth-child(13)').attr({'title':'Yukon'});
				break;
			case 'PRI':
				jQuery('#stateLbl').html('State');
				stateSelect.options[0] = new Option('--', '--');
				break;
			case 'VIR':
				jQuery('#stateLbl').html('State');
				stateSelect.options[0] = new Option('--', '--');
				break;
			case 'USA':
				jQuery('#stateLbl').html('State');
				stateSelect.options[0] = new Option('AL', 'AL');
				stateSelect.options[1] = new Option('AK', 'AK');
				stateSelect.options[2] = new Option('AZ', 'AZ');
				stateSelect.options[3] = new Option('AR', 'AR');
				stateSelect.options[4] = new Option('CA', 'CA');
				stateSelect.options[5] = new Option('CO', 'CO');
				stateSelect.options[6] = new Option('CT', 'CT');
				stateSelect.options[7] = new Option('DE', 'DE');
				stateSelect.options[8] = new Option('DC', 'DC');
				stateSelect.options[9] = new Option('FL', 'FL');
				stateSelect.options[10] = new Option('GA', 'GA');
				stateSelect.options[11] = new Option('HI', 'HI');
				stateSelect.options[12] = new Option('ID', 'ID');
				stateSelect.options[13] = new Option('IL', 'IL');
				stateSelect.options[14] = new Option('IN', 'IN');
				stateSelect.options[15] = new Option('IA', 'IA');
				stateSelect.options[16] = new Option('KS', 'KS');
				stateSelect.options[17] = new Option('KY', 'KY');
				stateSelect.options[18] = new Option('LA', 'LA');
				stateSelect.options[19] = new Option('ME', 'ME');
				stateSelect.options[20] = new Option('MD', 'MD');
				stateSelect.options[21] = new Option('MA', 'MA');
				stateSelect.options[22] = new Option('MI', 'MI');
				stateSelect.options[23] = new Option('MN', 'MN');
				stateSelect.options[24] = new Option('MS', 'MS');
				stateSelect.options[25] = new Option('MO', 'MO');
				stateSelect.options[26] = new Option('MT', 'MT');
				stateSelect.options[27] = new Option('NE', 'NE');
				stateSelect.options[28] = new Option('NV', 'NV');
				stateSelect.options[29] = new Option('NH', 'NH');
				stateSelect.options[30] = new Option('NJ', 'NJ');
				stateSelect.options[31] = new Option('NM', 'NM');
				stateSelect.options[32] = new Option('NY', 'NY');
				stateSelect.options[33] = new Option('NC', 'NC');
				stateSelect.options[34] = new Option('ND', 'ND');
				stateSelect.options[35] = new Option('OH', 'OH');
				stateSelect.options[36] = new Option('OK', 'OK');
				stateSelect.options[37] = new Option('OR', 'OR');
				stateSelect.options[38] = new Option('PA', 'PA');
				stateSelect.options[39] = new Option('RI', 'RI');
				stateSelect.options[40] = new Option('SC', 'SC');
				stateSelect.options[41] = new Option('SD', 'SD');
				stateSelect.options[42] = new Option('TN', 'TN');
				stateSelect.options[43] = new Option('TX', 'TX');
				stateSelect.options[44] = new Option('UT', 'UT');
				stateSelect.options[45] = new Option('VT', 'VT');
				stateSelect.options[46] = new Option('VA', 'VA');
				stateSelect.options[47] = new Option('WA', 'WA');
				stateSelect.options[48] = new Option('WV', 'WV');
				stateSelect.options[49] = new Option('WI', 'WI');
				stateSelect.options[50] = new Option('WY', 'WY');
				jQuery('#State option:first-child').attr({'title':'Alabama'});
				jQuery('#State option:nth-child(2)').attr({'title':'Alaska'});
				jQuery('#State option:nth-child(3)').attr({'title':'Arizona'});
				jQuery('#State option:nth-child(4)').attr({'title':'Arkansas'});
				jQuery('#State option:nth-child(5)').attr({'title':'California'});
				jQuery('#State option:nth-child(6)').attr({'title':'Colorado'});
				jQuery('#State option:nth-child(7)').attr({'title':'Connecticut'});
				jQuery('#State option:nth-child(8)').attr({'title':'Delaware'});
				jQuery('#State option:nth-child(9)').attr({'title':'District of Columbia'});
				jQuery('#State option:nth-child(10)').attr({'title':'Florida'});
				jQuery('#State option:nth-child(11)').attr({'title':'Georgia'});
				jQuery('#State option:nth-child(12)').attr({'title':'Hawaii'});
				jQuery('#State option:nth-child(13)').attr({'title':'Idaho'});
				jQuery('#State option:nth-child(14)').attr({'title':'Illinois'});
				jQuery('#State option:nth-child(15)').attr({'title':'Indiana'});
				jQuery('#State option:nth-child(16)').attr({'title':'Iowa'});
				jQuery('#State option:nth-child(17)').attr({'title':'Kansas'});
				jQuery('#State option:nth-child(18)').attr({'title':'Kentucky'});
				jQuery('#State option:nth-child(19)').attr({'title':'Louisiana'});
				jQuery('#State option:nth-child(20)').attr({'title':'Maine'});
				jQuery('#State option:nth-child(21)').attr({'title':'Maryland'});
				jQuery('#State option:nth-child(22)').attr({'title':'Massachusetts'});
				jQuery('#State option:nth-child(23)').attr({'title':'Michigan'});
				jQuery('#State option:nth-child(24)').attr({'title':'Minnesota'});
				jQuery('#State option:nth-child(25)').attr({'title':'Mississippi'});
				jQuery('#State option:nth-child(26)').attr({'title':'Missouri'});
				jQuery('#State option:nth-child(27)').attr({'title':'Montana'});
				jQuery('#State option:nth-child(28)').attr({'title':'Nebraska'});
				jQuery('#State option:nth-child(29)').attr({'title':'Nevada'});
				jQuery('#State option:nth-child(30)').attr({'title':'New Hampshire'});
				jQuery('#State option:nth-child(31)').attr({'title':'New Jersey'});
				jQuery('#State option:nth-child(32)').attr({'title':'New Mexico'});
				jQuery('#State option:nth-child(33)').attr({'title':'New York'});
				jQuery('#State option:nth-child(34)').attr({'title':'North Carolina'});
				jQuery('#State option:nth-child(35)').attr({'title':'North Dakota'});
				jQuery('#State option:nth-child(36)').attr({'title':'Ohio'});
				jQuery('#State option:nth-child(37)').attr({'title':'Oklahoma'});
				jQuery('#State option:nth-child(38)').attr({'title':'Oregon'});
				jQuery('#State option:nth-child(39)').attr({'title':'Pennsylvania'});
				jQuery('#State option:nth-child(40)').attr({'title':'Rhode Island'});
				jQuery('#State option:nth-child(41)').attr({'title':'South Carolina'});
				jQuery('#State option:nth-child(42)').attr({'title':'South Dakota'});
				jQuery('#State option:nth-child(43)').attr({'title':'Tennessee'});
				jQuery('#State option:nth-child(44)').attr({'title':'Texas'});
				jQuery('#State option:nth-child(45)').attr({'title':'Utah'});
				jQuery('#State option:nth-child(46)').attr({'title':'Vermont'});
				jQuery('#State option:nth-child(47)').attr({'title':'Virginia'});
				jQuery('#State option:nth-child(48)').attr({'title':'Washington'});
				jQuery('#State option:nth-child(49)').attr({'title':'West Virginia'});
				jQuery('#State option:nth-child(50)').attr({'title':'Wisconsin'});
				jQuery('#State option:nth-child(51)').attr({'title':'Wyoming'});
				break;
		}
		
	});
	</script>
</div>
<div class="row-vspace-10">
	<label class="required std-lbl" for="cat-email">Email</label>
    <input id="cat-email" class="cat-input-field" type="text" />
</div>
</div> <!-- /RIGHT SIDE -->
<div class="row-vspace-10 floatFix">
    <input class="optin" checked="" value="1" name="OptIn" type="checkbox" />
	<label class="opt-lbl" for="OptIn" style="display:inline-block;text-align:left;">Send me exclusive offers</label>
</div>
<div class="row-vspace-10">
	<div class="resp-float-lt-480">
    	<input type="image" width="100px" src="{{media url='wysiwyg/catalog-form-submit.png'}}" />
    </div>
    <div class="resp-float-rt-767 align-ctr-480 required red sm-copy">Required Field</div>
</div>

</form>
</div>
</div> <!-- /WRAP -->
</div>


EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Returns and Exchanges',
	'identifier' 		=> 'returns-exchanges',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<p>{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}</p>
<div class="custom">
<div class="align-ctr">
<h1>RETURNS AND EXCHANGES</h1>
</div>
<div class="std">
<p>At Happy Chef, your satisfaction with our products is guaranteed. If you are unsatisfied with your purchase for any reason, simply fill out our product return form below and send your items back within 30 days for a full refund or exchange.</p>
<p>Shipping on replacement items is free. Customers are only responsible for the shipping costs of the items they are returning. Returns submitted after 30 days of receipt may be subject to a restocking fee. Products that have been worn, washed, or modified cannot be returned. Items that have been customized cannot be returned.</p>
<p><img class="sm-icon" title="Product Return Form" src="http://184.106.57.93:8080/HappyChef20/media/ups_ground_map.gif" alt="Product Return Form" /><a class="link">Product Return Form</a></p>
</div>
</div>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Sale',
	'identifier' 		=> 'sale',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
		<p>This is the sale page</p>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Shipping and Delivery',
	'identifier' 		=> 'shipping-delivery',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
<p>{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="73"}}</p>
<div class="col-main">
<div class="page-title">
<h1>Shipping and Delivery</h1>
</div>
<div class="std">
<p>At Happy Chef, most merchandise will be shipped via UPS within 24 hours of your order processing. If you&rsquo;ve ordered an item that is currently out of stock, it will be shipped as soon as it&rsquo;s available. For orders that include embroidered merchandise, please allow 7-10 additional business days. Note that, for orders processed in multiple shipments, shipping costs for the entire order will be reflected on the invoice accompanying the first delivery only.</p>
<p>Happy Chef reserves the right to refuse delivery to anyone for any reason, including but not limited to fraudulent orders, suspicious single-recipient order volume, and large quantity purchases of items with limited availability.</p>
<p>Choose your delivery location for an estimate of delivery costs and transit times.</p>
<p><img title="UPS Delivery Map" src="http://demohost.us/happychef/media/ups_ground_map.gif" alt="UPS Delivery Map" /></p>
</div>
</div>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Size Charts',
	'identifier' 		=> 'sizechartfooter',
	'stores'			=> $stores,
	'content_heading'	=> '',
	'content'			=> <<<EOB
<div class="size-chart">
<div class="title-size-chart">Unisex Coats & shirts</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th>4XL</th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Men's Neck Size</th><th> &nbsp </th><th>14-14<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>15-15<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>16-16<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>17-17<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>18-18<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>19-19<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>20-20<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Women's Size</th><th> &nbsp </th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20-22</th><th>24-26</th><th>28-30</th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Unisex Pants</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th>4XL</th><th>5XL</th><th> &nbsp </th></tr>
<tr><th class="title">Waist Size</th><th>24-26</th><th>28-30</th><th>32-34</th><th>36-38</th><th>40-42</th><th>44-46</th><th>54-57</th><th>58-63</th><th>64-68</th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Women's sizing</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Women's Size</th><th>2</th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20W-22W</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Kids sizing</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Kids Size</th><th>2t-4t</th><th>4-5</th><th>6-7</th><th>8-10</th><th>12</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Unisex performance pants</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Waist Size</th><th>24-26</th><th>28-30</th><th>32-34</th><th>36-38</th><th>40-42</th><th>44-46</th><th>48-50</th><th> &nbsp </th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">Unisex woven shirts</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th class="title">Size</th><th>XS</th><th>S</th><th>M</th><th>L</th><th>XL</th><th>2XL</th><th>3XL</th><th>4XL</th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Men's Neck Size</th><th> &nbsp </th><th>14-14<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>15-15<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>16-16<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>17-17<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>18-18<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>19-19<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>20-20<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th> &nbsp </th><th> &nbsp </th></tr>
<tr><th class="title">Women's Size</th><th> &nbsp </th><th>4-6</th><th>8-10</th><th>12-14</th><th>16-18</th><th>20-22</th><th>24-26</th><th>28-30</th><th> &nbsp </th><th> &nbsp </th></tr>
</tbody>
</table>
</div>


<div class="size-chart">
<div class="title-size-chart">footwear sizing</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th rowspan="2"> Men's</th><th>US Size</th><th>6-6<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>7-7<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>8-8<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>9-9<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>10-10<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>11-11<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>12-12<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>13-13<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>14</th></tr>
<tr><th>European</th><th>39</th><th>40</th><th>40</th><th>42</th><th>43</th><th>44</th><th>45</th><th>46</th><th>47</th></tr>

<tr><th rowspan="2"> Women's</th><th>US Size</th><th>4-4<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>5-5<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>6-6<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>7-7<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>8-8<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>9-9<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>10-10<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>11-11<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th></th></tr>
<tr><th>European</th><th>35</th><th>36</th><th>37</th><th>38</th><th>39</th><th>40</th><th>41</th><th>42</th><th></th></tr>
</tbody>
</table>
</div>

<div class="size-chart">
<div class="title-size-chart">footwear sizing troentorp clogs</div>
<table id="size-chart123" class="container_24">
<tbody>
<tr><th rowspan="2"> Men's</th><th>US Size</th><th>7-7<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>8-8<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>9-9<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>10-10<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>11-11<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>12-12<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>13-13<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>14-14<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>14</th></tr>
<tr><th>European</th><th>40</th><th>40</th><th>42</th><th>43</th><th>44</th><th>45</th><th>46</th><th>47</th></tr>

<tr><th rowspan="2"> Women's</th><th>US Size</th><th>5-5<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>6-6<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>7-7<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>8-8<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>9-9<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>10-10<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>11-11<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>12-12<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th>13-13<span class="fractions"><span class="number-top">1</span>/<span class="number-bottom">2</span></span></th><th></th></tr>
<tr><th>European</th><th>35</th><th>36</th><th>37</th><th>38</th><th>39</th><th>40</th><th>41</th><th>42</th>th>43</th><th></th></tr>
</tbody>
</table>
</div>

EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();


$dataPage = array(
	'title'				=> 'Top Seller',
	'identifier' 		=> 'topseller',
	'stores'			=> $stores,
	'content_heading'	=> 'Top Seller',
	'content'			=> <<<EOB
		<p>This is the page to display top seller product</p>
EOB
,
	'root_template'		=> 'one_column',
);
$page->setData($dataPage)->save();
####################################################################################################
# ADD THEMEFRAMEWORK LAYOUT
####################################################################################################

$model = Mage::getModel('themeframework/area');
	
$data = array(
	'package_theme'  => 'default/em0080',
	'layout'         => '1column',	
	'is_active'      => 1,
	'content_decode' => unserialize(<<<EOB
a:7:{i:0;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:1:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:30:"recent_views_fixed social_view";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:9:"em_area16";}}}}i:1;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:6:"header";}}i:1;s:5:"clear";}}i:2;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:55:"<div class="container-new-collection">{{content}}</div>";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:26:"slideshow_area omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area2";}}i:1;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area3";}}}}i:3;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:48:"<div class="container-em-main">{{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:9:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"breadcrumbs";}}i:1;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:19:"em_main omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:5:{i:0;s:15:"global_messages";i:1;s:8:"em_area4";i:2;s:7:"content";i:3;s:8:"em_area5";i:4;s:8:"em_area6";}}i:2;a:11:{s:6:"column";s:2:"12";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:15:"alpha column2a ";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"em_area13a1";}}i:3;a:11:{s:6:"column";s:2:"12";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:14:"omega column2b";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"em_area13a2";}}i:4;a:11:{s:6:"column";s:1:"8";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:14:"column3a alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"em_area13b1";}}i:5;a:11:{s:6:"column";s:1:"8";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:20:"column3b alpha omega";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"em_area13b2";}}i:6;a:11:{s:6:"column";s:1:"8";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:14:"column3c omega";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"em_area13b3";}}i:7;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:20:"omega alpha chefpick";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:23:"happychef_home_featured";}}i:8;s:5:"clear";}}i:4;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:47:"<div class="container-footer">{{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:16:"happychef-footer";}}i:1;s:5:"clear";}}i:5;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:14:"container_free";s:5:"items";a:1:{i:0;s:15:"before_body_end";}}i:6;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:0:{}}}
EOB
	)
);
$model->setData($data)->setStores(array(0))->save();

$data = array(
	'package_theme'  => 'default/em0080',
	'layout'         => '2columns-left',	
	'is_active'      => 1,
	'content_decode' => unserialize(<<<EOB
a:6:{i:0;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:1:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:30:"recent_views_fixed social_view";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:9:"em_area16";}}}}i:1;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:6:"header";}}i:1;s:5:"clear";}}i:2;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:55:"<div class="container-new-collection">{{content}}</div>";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:3:{i:0;a:11:{s:6:"column";s:1:"5";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:13:"category-left";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area1";}}i:1;a:11:{s:6:"column";s:2:"19";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:14:"slideshow_area";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area2";}}i:2;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area3";}}}}i:3;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:48:"<div class="container-em-main">{{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:5:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"breadcrumbs";}}i:1;a:11:{s:6:"column";s:1:"6";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:22:"sidebar col-left alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:4:{i:0;s:8:"em_area7";i:1;s:4:"left";i:2;s:8:"em_area8";i:3;s:8:"em_area9";}}i:2;a:11:{s:6:"column";s:2:"18";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:19:"em_main alpha omega";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:5:{i:0;s:15:"global_messages";i:1;s:8:"em_area4";i:2;s:7:"content";i:3;s:8:"em_area5";i:4;s:8:"em_area6";}}i:3;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:30:"bottom_slider_link omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:0:{}}i:4;s:5:"clear";}}i:4;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:48:"<div class="container-footer"> {{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:16:"happychef-footer";}}i:1;s:5:"clear";}}i:5;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:14:"container_free";s:5:"items";a:1:{i:0;s:15:"before_body_end";}}}
EOB
	)
);
$model->setData($data)->setStores(array(0))->save();

$data = array(
	'package_theme'  => 'default/em0080',
	'layout'         => '2columns-right',	
	'is_active'      => 1,
	'content_decode' => unserialize(<<<EOB
a:6:{i:0;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:1:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:30:"recent_views_fixed social_view";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:9:"em_area16";}}}}i:1;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:52:"<div class="container-header-top"> {{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:6:"header";}}i:1;s:5:"clear";}}i:2;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:56:"<div class="container-new-collection"> {{content}}</div>";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:3:{i:0;a:11:{s:6:"column";s:1:"5";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:13:"category-left";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area1";}}i:1;a:11:{s:6:"column";s:2:"19";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:14:"slideshow_area";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area2";}}i:2;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area3";}}}}i:3;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:48:"<div class="container-em-main">{{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:5:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"breadcrumbs";}}i:1;a:11:{s:6:"column";s:2:"19";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:7:"em_main";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:5:{i:0;s:15:"global_messages";i:1;s:8:"em_area4";i:2;s:7:"content";i:3;s:8:"em_area5";i:4;s:8:"em_area6";}}i:2;a:11:{s:6:"column";s:1:"5";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:17:"sidebar col-right";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:4:{i:0;s:9:"em_area10";i:1;s:5:"right";i:2;s:9:"em_area11";i:3;s:9:"em_area12";}}i:3;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:18:"bottom_slider_link";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:9:"em_area13";}}i:4;s:5:"clear";}}i:4;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:48:"<div class="container-footer"> {{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:16:"happychef-footer";}}i:1;s:5:"clear";}}i:5;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:14:"container_free";s:5:"items";a:1:{i:0;s:15:"before_body_end";}}}
EOB
	)
);
$model->setData($data)->setStores(array(0))->save();

$data = array(
	'package_theme'  => 'default/em0080',
	'layout'         => '3columns',	
	'is_active'      => 1,
	'content_decode' => unserialize(<<<EOB
a:6:{i:0;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:1:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:18:"recent_views_fixed";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:9:"em_area16";}}}}i:1;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:52:"<div class="container-header-top"> {{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:11:"omega alpha";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:6:"header";}}i:1;s:5:"clear";}}i:2;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:56:"<div class="container-new-collection"> {{content}}</div>";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:3:{i:0;a:11:{s:6:"column";s:1:"5";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:13:"category-left";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area1";}}i:1;a:11:{s:6:"column";s:2:"19";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:14:"slideshow_area";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area2";}}i:2;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:8:"em_area3";}}}}i:3;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:48:"<div class="container-em-main">{{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:6:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:11:"breadcrumbs";}}i:1;a:11:{s:6:"column";s:2:"14";s:4:"push";s:1:"5";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:7:"em_main";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:5:{i:0;s:15:"global_messages";i:1;s:8:"em_area4";i:2;s:7:"content";i:3;s:8:"em_area5";i:4;s:8:"em_area6";}}i:2;a:11:{s:6:"column";s:1:"5";s:4:"push";s:0:"";s:4:"pull";s:2:"14";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:16:"sidebar col-left";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:4:{i:0;s:8:"em_area7";i:1;s:4:"left";i:2;s:8:"em_area8";i:3;s:8:"em_area9";}}i:3;a:11:{s:6:"column";s:1:"5";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:17:"sidebar col-right";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:4:{i:0;s:9:"em_area10";i:1;s:5:"right";i:2;s:9:"em_area11";i:3;s:9:"em_area12";}}i:4;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:18:"bottom_slider_link";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:9:"em_area13";}}i:5;s:5:"clear";}}i:4;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:48:"<div class="container-footer"> {{content}}</div>";s:13:"display_empty";b:0;s:4:"type";s:12:"container_24";s:5:"items";a:2:{i:0;a:11:{s:6:"column";s:2:"24";s:4:"push";s:0:"";s:4:"pull";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";s:5:"first";b:0;s:4:"last";b:0;s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:13:"display_empty";b:0;s:5:"items";a:1:{i:0;s:16:"happychef-footer";}}i:1;s:5:"clear";}}i:5;a:6:{s:10:"custom_css";s:0:"";s:10:"inner_html";s:0:"";s:10:"outer_html";s:0:"";s:13:"display_empty";b:0;s:4:"type";s:14:"container_free";s:5:"items";a:1:{i:0;s:15:"before_body_end";}}}
EOB
	)
);
$model->setData($data)->setStores(array(0))->save();

####################################################################################################
# ADD MEGAMENU PRO
####################################################################################################

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('megamenupro')} (
  `megamenupro_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `type` smallint(6) NOT NULL default '0',
  `content` text NOT NULL default '', 
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`megamenupro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

####################################################################################################
# ADD EM Slideshow2
####################################################################################################

/**
 * Create table 'slideshow2/slider'
 */

if(!$installer->tableExists($installer->getTable('slideshow2/slider'))){
$table = $installer->getConnection()
    ->newTable($installer->getTable('slideshow2/slider'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Slideshow ID')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        ), 'Slideshow name')
	->addColumn('images', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'images')
	->addColumn('slider_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
        ), 'Slideshow type')
	->addColumn('slider_params', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'Slideshow params')
	->addColumn('delay', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        ), 'Slideshow delay')
	->addColumn('touch', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, array(
        ), 'Slideshow touch')
	->addColumn('stop_hover', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, array(
        ), 'Slideshow stop hover')
	->addColumn('shuffle_mode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, array(
        ), 'Slideshow shuffle mode')
	->addColumn('stop_slider', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, array(
        ), 'Slideshow stop slider')
	->addColumn('stop_after_loop', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, array(
        ), 'Slideshow stop after loop')
	->addColumn('stop_at_slide', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, array(
        ), 'Slideshow stop at slide')
	->addColumn('position', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'position')
	->addColumn('appearance', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'appearance')
	->addColumn('navigation', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'navigation')
	->addColumn('thumbnail', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'thumbnail')
	->addColumn('visibility', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'visibility')
	->addColumn('trouble', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'trouble')
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Slideshow Creation Time')
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Slideshow Modification Time')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '0',
        ), 'Is Slideshow Active')
    ->setComment('EM Slideshow2 Slider Table');
$installer->getConnection()->createTable($table);
}

$helper = Mage::helper('em0080settings');
$helperSample = Mage::helper('em0080settings/sample');
$helperSample->importSampleData();
$installer->endSetup();
