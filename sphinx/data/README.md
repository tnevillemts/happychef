This directory has been used in the past to store a Sphinx index.

On the new servers to be built on AWS these logs will be stored in the standard locations
outside the web root (see configuration file). Once live, this file can be deleted,
releasing the directory from being tracked by Git.
