#!/bin/bash
#############################################
#
#	gen_gbase_feed.sh
#
# 	Invoke feeder script.
#
#############################################

echo"-------------";
/usr/bin/php ./gen_gbase_feed.php --batch_mode 1 --verbose 1;
echo"-------------";

exit;
