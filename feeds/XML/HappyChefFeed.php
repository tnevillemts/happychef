<?php
	/////////////////////////////////////////////////////////////////////////////
	//
	// re-written by drichter.
	//
	/////////////////////////////////////////////////////////////////////////////

	echo("Start process.\n");

	set_time_limit(0);

	// Set the memory limit to infinite.
	ini_set('memory_limit', '-1');

	// Set 1 to enable download at HC server
	$local_download = 1;
	// Set 1 to enable download at client server
	$client_download = 1;

	// Email only after N threshold failures.
	$emailThreshold = 5;

	$cFileName = '/var/www/html/happychef-beta/feeds/XML/err_cnt.txt';
	
	if(! cURLcheckBasicFunctions()) {
		echo(__FILE__ . " Curl base functions not present.\n");	
	} else {
		echo("Curl base functions are present.\n");	
		$version = curl_version();
		echo("Curl version [");
		print_r($version);
		echo("]\n");
	}

	$passed = true;

	// Download files from Client server 
	if(downloadFile('https://ws-core.happychef.com/hcweb/weborders-out.xml','/var/www/html/XML/ftp1/weborders-out.xml')) {
		$file_order 		= "weborders-out.xml"; // Second file	
		$source_order 		= '/var/www/html/XML/ftp1/weborders-out.xml'; // Order source file
		$destination_order 	= '/ftp4xml/ordersync/xml'; // Destination path for order

		// Additional path on same HC server
		$additional_destination_order = '/var/www/html/XML/ftp2/'.$file_order; // Destination path for order

		// When request for file download at local HC end.
		if($local_download) {
			// Additional path get on HC server
			additionalFileTransfer($file_order, $source_order, $additional_destination_order);
		}
	
		// When request for file download at client end.
		if($client_download) {
			// Call function to get & put file of order
			fileTransfer($file_order, $source_order, $destination_order);
		}
	} else {
		$passed = false;
	}

	sleep(10);

	if(downloadFile('https://ws-core.happychef.com/hcweb/webproducts-out.xml','/var/www/html/XML/ftp1/webproducts-out.xml')) {
		$file_product 		= "webproducts-out.xml"; // Product file
		$source_prod 		= '/var/www/html/XML/ftp1/webproducts-out.xml'; // Product source file
		$destination_prod 	= '/ftp4xml/productsync/xml'; // Destination path for product
	
		// Additional path on same HC server
		$additional_destination_prod  = '/var/www/html/XML/ftp2/'.$file_product; // Destination path for product
	
		// When request for file download at local HC end.
		if($local_download) {
			// Additional path get on HC server
			additionalFileTransfer($file_product, $source_prod, $additional_destination_prod);
		}
	
		// When request for file download at client end.
		if($client_download) {
			// Call function to get & put file of product
			fileTransfer($file_product, $source_prod, $destination_prod);
		}
	} else {
		$passed = false;
	} 

	if(! $passed) {
		if(cFile($cFileName, $emailThreshold)) {
        		echo(__FILE__ . ": Threshold passed for products.\n");
			emailAlert();
		} else {
        		echo(__FILE__ . ": Threshold not passed for products.\n");
		}
	}  else {
		resetCFile($cFileName);
	}

	echo("Finish process.\n");

	/////////////////////////////////////////////////////////////////////////////
	//
	// 	Check base functions.
	//
	/////////////////////////////////////////////////////////////////////////////
	function cURLcheckBasicFunctions()
	{
  		if( !function_exists("curl_init") 	&&
      		    !function_exists("curl_setopt") 	&&
      		    !function_exists("curl_exec") 	&&
      		    !function_exists("curl_close")      &&
		    !function_exists("curl_version") ) {
			return false;
  		} else  {
			return true;
		}
	} 
	
	/////////////////////////////////////////////////////////////////////////////
	//
	//	Function: TO download xml file from Source Location
	//
	//	@param: $url - Source URL
	//	@param:	$path - Source file path
	//
	//	Error codes in libcurl ---->  http://curl.haxx.se/libcurl/c/libcurl-errors.html
	//
	/////////////////////////////////////////////////////////////////////////////
	function downloadFile($url, $path) {
		$retVal = false;
		echo ("Call -----> " . __FILE__ . " " . __FUNCTION__ . " (" . $url . ", " . $path . ")\n");
		$ch = curl_init($url);
		if($ch) {
			echo (__FILE__ . " " . __FUNCTION__ . ": Curl init succeeded.\n");

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
			$data = curl_exec($ch);

			if($data == FALSE) {
				echo ("Unable to generate XML file, no data.\n");
				$errorText 	= curl_error($ch);
				$errorNo 	= curl_errno($ch);
				if(!empty($errorText)) {
					echo(__FILE__ . " " . __FUNCTION__ . ": Curl error [" . $errorText . "], [" . $errorNo . "].\n");
				}
				return false;
			} else {
				if(! curl_error($ch)) {
					echo(__FILE__ . " " . __FUNCTION__ . ": Curl information for download of [" . $path . "].\n");
 					$info = curl_getinfo($ch);
					echo("Curl information [");
					print_r($info);
					echo("].\n");
				} 
			}
		
			if(! empty($data)) {
				echo(__FILE__ . " " . __FUNCTION__ . ": file_put_contents() success.\n");
				file_put_contents($path, $data);
			} else {
				echo(__FILE__ . " " . __FUNCTION__ . ": data is empty.\n");
				return false;
			}
		
			if(!filesize($path)) {
				echo (__FILE__ . " " . __FUNCTION__ . ": Unable to generate XML file, filesize [" . filesize($path) . "].\n");
				return false;
			} else {
				echo(__FILE__ . " " . __FUNCTION__ . ": File size > 0 for file [" . $path . "].\n");
			}
			curl_close($ch);
		} else {
			echo (__FILE__ . " " . __FUNCTION__ . ": Curl init failed.\n");
			return false;
		}
		echo ("End Call -----> " . __FILE__ . " " . __FUNCTION__ . "\n");
		return true;
	}
	
	/////////////////////////////////////////////////////////////////////////////
	//
	//	Function: TO transfer/download xml file at Client Location
	//
	//	@param: $fileNameMain - Name of file
	//	@param:	$sourceName - Source file path
	//	@param:	$destinationName - Destination file path
	//
	/////////////////////////////////////////////////////////////////////////////
    	function fileTransfer($fileNameMain, $sourceName, $destinationName) {
		$serverIp = "208.72.2.82";

		$con = ftp_connect($serverIp);

		if(! $con) {
 			echo(__FILE__ . " " . __FUNCTION__ . ": Could not established connection with " . $serverIp . ".\n");
		}
		if(! ftp_login($con,"ftp4xml","10k73BtRm81M")) {
 			echo(__FILE__ . " " . __FUNCTION__ . ": Invalid username or password at " . $serverIp . ".\n");	
		}
		if(! ftp_chdir($con,$destinationName)) {
 			echo(__FILE__ . " " . __FUNCTION__ . ": Could not find dir at " . $serverIp . ".\n");
		}
		// upload file to particular path
		$upload = ftp_put($con,$fileNameMain,$sourceName,FTP_BINARY);

		if($upload) {
			echo(__FILE__ . " " . __FUNCTION__ . ": Uploaded Successfully " . $fileNameMain . "\n"); 
		} else {
			echo(__FILE__ . " " . __FUNCTION__ . ": Upload Failed " . $fileNameMain . "\n"); 
		} 
		ftp_close($con);
	}
	
	/////////////////////////////////////////////////////////////////////////////
	//
	//	Function: TO transfer/download xml file at HC Location
	//
	//	@param: $fileNameMain - Name of file
	//	@param:	$sourceName - Source file path
	//	@param:	$destinationName - Destination file path
	//
	/////////////////////////////////////////////////////////////////////////////
	function additionalFileTransfer($fileNameMain, $sourcePath, $destinationPath) {				
		// upload file to particular path
		$upload_hc = file_put_contents($destinationPath, file_get_contents($sourcePath));
		if($upload_hc) {
			echo(__FILE__ . " " . __FUNCTION__ . ": Uploaded file Successfully at HC server - " . $fileNameMain . "\n"); 
		} else {
			echo(__FILE__ . " " . __FUNCTION__ . ": Upload file Failed at HC server - " . $fileNameMain . "\n"); 
		} 		
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	//
	//
	/////////////////////////////////////////////////////////////////////////////
	function cFile($cFileName, $threshold) {
        	$count = 0;
        	if(filesize($cFileName) != 0) {
                	$fp = fopen($cFileName, 'r');
                	$line = fgets($fp);
                	$count = intVal(substr($line, 0, strspn($line, "0123456789")));
                	$count++;
        	}
        	file_put_contents($cFileName, strVal($count));
        	fclose($fp);

        	if($count > $threshold) {
                	return true;
        	} else {
                	return false;
        	}
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	//
	//
	/////////////////////////////////////////////////////////////////////////////
	function resetCFile($cFileName) {
        	$count = 0;
        	file_put_contents($cFileName, strVal($count));
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	//
	//
	/////////////////////////////////////////////////////////////////////////////
	function emaiAlert() {
		$ok = @mail("drichter@ri.pn", $subject, $message, "From:" . $email);
                if(! $ok) {
                	echo(__FILE__ . " " . __FUNCTION__ . ": mail failed.\n");
		}	 
		/*
		$ok = @mail("dbarbella@ri.pn", $subject, $message, "From:" . $email);
                if(! $ok) {
                	echo(__FILE__ . " " . __FUNCTION__ . ": mail failed.\n");
		} 
		*/
	}

?>
