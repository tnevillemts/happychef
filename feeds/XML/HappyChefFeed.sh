#!/bin/bash
##########################################################################################
#
# HappyChefFeed.sh
#
#########################################################################################
SCRIPT_NAME='HappyChefFeed';
SCRIPT_PATH='/var/www/html/happychef-beta/feeds/XML/';
SCRIPT_FULL_NAME=$SCRIPT_PATH$SCRIPT_NAME;
EMAIL_BODY='emailbody.txt';
LOG=$SCRIPT_NAME'.log';
DIR='lockdir'
LOCKDIR=$SCRIPT_PATH$DIR;

# If it's too old then it's going to jam up the feed process so account for this. We'll give it 3 hours.
if test `find "$LOCKDIR" -mmin +180`
then
	ps aux | grep -i HappChefFeed | grep -v grep | awk {'print $2'} | xargs kill;
	rmdir $LOCKDIR;
fi

# Now start.
echo "------------------------------------------------------"      >> $LOG;
date 								   >> $LOG;
if mkdir -p $LOCKDIR
then
	echo "Directory locked (mutex)."			   >> $LOG;
	echo 'Process is NOT running, Starting: '$SCRIPT_FULL_NAME >> $LOG;
	echo 					   		   >> $LOG; 
	/usr/bin/php ./$SCRIPT_NAME'.php' 			   >> $LOG;
    	if rmdir $LOCKDIR
    	then
		echo "Directory unlock successful." 		   >> $LOG;
    	else
		echo "Directory unlock failed." 		   >> $LOG;
    	fi
else
	echo 'Process is already running: '$SCRIPT_FULL_NAME       > $EMAIL_BODY;
	echo 'Can not create directory '$LOCKDIR		   >> $EMAIL_BODY;
	echo 'Started at:' 				           >> $EMAIL_BODY;
	date 						           >> $EMAIL_BODY;
	echo 'Issue requires attention.' 		           >> $EMAIL_BODY;
	echo "-----------------------------------" 	           >> $EMAIL_BODY;
	ps -ef | grep $SCRIPT_NAME | grep -v grep 	           >> $EMAIL_BODY;
	echo "-----------------------------------" 	           >> $EMAIL_BODY;

	/bin/mail -s "Email from "$SCRIPT_NAME drichter@ri.pn < $EMAIL_BODY;
	/bin/mail -s "Email from "$SCRIPT_NAME mmedvedev@ri.pn < $EMAIL_BODY;
	#/bin/mail -s "Email from "$SCRIPT_NAME dbarbella@ri.pn < $EMAIL_BODY;
	cat $EMAIL_BODY 				 	   >> $LOG;
fi
echo "------------------------------------------------------"      >> $LOG;
exit;

