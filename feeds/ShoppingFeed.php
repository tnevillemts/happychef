<?php
define('__ROOT__', dirname(__FILE__));

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/sendShoppingFeed.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $outShoppingFeedObj = new sendShoppingFeed($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if mutually excluded.
    $outShoppingFeedObj->checkCanRun();

    $outShoppingFeedObj->lock();
    $outShoppingFeedSuccess = $outShoppingFeedObj->process();
    $outShoppingFeedObj->unlock();

    if (! $outShoppingFeedSuccess) {
        throw new Exception($outShoppingFeedObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    $logger->info("Success: " . __FILE__);
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::OUT_CATALOG_DATA_NAME);
try {
    process($logger);
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
