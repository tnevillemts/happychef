<?php
require_once(__ROOT__ . '/constants.php');
require_once(__ROOT__ . '/feederBase.php');

class syncOrders extends feederBase
{
    /**
     * @param string $magentoVersion
     * @param string $feedType
     */
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion  = $magentoVersion;
        $this->feedType        = $feedType;
        $this->feedName        = Constants::IN_ORDER_SYNC_NAME;
        $this->feedUrl         = Constants::IN_ORDER_SYNC;
        $this->logger          = new Logger ( Constants::LOGDIR . "/" . $this->feedName);
        parent::__construct();
    }

    /**
     * @param array $regionCollection
     * @param string $code
     * @return int|null
     */
    public function getRegionId($regionCollection, $code)
    {
        foreach($regionCollection as $region) {
            if($region->code == $code) {
                return $region->region_id;
            }
        }
        return null;
    }

    /**
     * @param SimpleXMLElement $webOrder
     * @param Mage_Sales_Model_Order $order
     * @return void
     */
    function callGiftcardObserver($webOrder, $order)
    {
        try {

            $orderItems = Mage::getModel('sales/order_item')->getCollection();
            $orderItems->addFieldToFilter('order_id', $order->getId());
            $orderItems->addAttributeToSelect('*');

            // Look at all the items in this order and if one is an email giftcard, then determine if it needs to be sent.
            /** @var Mage_Sales_Model_Order_Item $item */
            foreach($orderItems as $item) {
                if($item->getProductType() == AW_Giftcard_Model_Catalog_Product_Type_Giftcard::TYPE_CODE) {
                    $options = $item->getProductOptions();

                    // Find status of the item.
                    if(count($webOrder->lineitems)) {
                        foreach($webOrder->lineitems->children() as $witem) {
                            if((int)$witem->externallineitemid == $item->getId()) {
                                // As per Jim Walsh: If the email gift card item has status RW (Release to Warehouse) or SH (Shipped), then it's good to deliver.
                                $itemStatus = (string)$witem->itemstatus;
                                if($itemStatus == 'RW' || $itemStatus == 'SH') {
                                    $awGcRecipientEmail = $options['aw_gc_recipient_email'];

                                    // Log that we are sending the email gift card.
                                    $this->logger->info($this->feedName . " File [" . __FILE__ . "] Function [" . __FUNCTION__ . "] Send to: aw_gc_recipient_email [" . $awGcRecipientEmail . "]");

                                    // Send it.
                                    $event = new Varien_Event(array('order'=>$order));
                                    $observer = new Varien_Event_Observer();
                                    $observer->setData(array('event'=>$event));

                                    $this->waitOnIndexers();
                                    $awGiftOb = Mage::getModel("aw_giftcard/observer_giftcard");
                                    $awGiftOb->salesOrderSaveAfter($observer);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error($this->feedName . " File [" . __FILE__ . "] Function [" . __FUNCTION__ . "] Exception [" . $e->getMessage() . "]");
        }
    }

    /**
     * @return bool
     */
    public function process()
    {
        if (! $this->checkConnection() || ! $this->checkDatabase()) {
            $this->logger->fatal($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection.");
            return false;
        }

        $this->setFeedStart();
        $this->setStatus(Constants::FEEDSTATUS_R);

        $feedErrors     = [];
        $feedInfo       = [];
        $status         = Constants::FEEDSTATUS_F;
        $readSuccess    = false;
        $numSynced      = 0;
        $numNotSynced   = 0;
        $numReadTrys    = 10;
        $currentRow     = 0;

        try {
            // Try 10 times, every 30 seconds.
            for ($i  = 0; $i < $numReadTrys; $i++) {
                $xml = $this->getURL($this->feedUrl);
                libxml_use_internal_errors(true);
                foreach(libxml_get_errors() as $error) {
                    $this->logger->error($this->feedName.": libxml error [".$error."].");
                }
                $webOrders = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_PARSEHUGE);
                if ($webOrders === false) {
                    $xmlParseError = 'Error: simplexml_load_string failure: ';
                    foreach (libxml_get_errors() as $error) {
                        $xmlParseError .= 'error [' . $error->message . ']; ';
                    }
                    sleep(30);
                } else {
                    $readSuccess = true;
                    $numRead = count($webOrders);
                    break;
                }
            }

            if (! $readSuccess) {
                throw new Exception("Could not read order data: $xmlParseError");
            }

            foreach($webOrders as $webOrder) {
                // Process each order.
                $externalorderno = (string) $webOrder[0]->externalorderno;
                $this->logger->info(" ");
                $this->logger->info($this->feedName . " START of order with external id [" . $externalorderno . "].");

                $this->waitOnIndexers();
                /** @var Mage_Sales_Model_Order $order */
                $order = Mage::getModel('sales/order')->load($externalorderno, 'increment_id');

                if($order->getIncrementId() == $externalorderno) {
                    $this->logger->info($this->feedName . " Found order in magento [" . $order->getId() . "] with increment id [" . $externalorderno . "].");

                    $confirmation = $order->getConfirmation();

                    if(empty($confirmation)) {
                        $this->logger->info($this->feedName . " Confirmation number is not set... Order is from HC2... Proceed with order sync for [" . $externalorderno . "].");

                        // Set MOM id.
                        $internalorderno        = (string) $webOrder[0]->internalorderno;
                        $order->setmomorder($internalorderno);

                        $processbatchid         = (string) $webOrder[0]->processbatchid;
                        $order->setmombatchid($processbatchid);

                        $this->waitOnIndexers();
                        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

                        if ($customer) {
                            $internalcustid = (string) $webOrder[0]->billtocust[0]->internalcustid;
                            $customer->setMomcustid ($internalcustid);
                            $this->logger->info($this->feedName . " Updated customer record [" . $order->getCustomerId() . "]. MOM Customer Id set to [" . $internalcustid . "].");
                        }

                        $regionCollection = Mage::getModel('directory/region_api')->items('US');

                        // Update the order billing address information.
                        $billRegionId = $this->getRegionId($regionCollection, $webOrder[0]->billtocust[0]->state);

                        try {
                            $this->waitOnIndexers();
                            /** @var Mage_Sales_Model_Order_Address $billingAddress */
                            $billingAddress = Mage::getModel('sales/order_address')->load($order->getBillingAddress()->getId());

                            $billingAddress
                                ->setStoreId                    (Constants::STOREID)
                                ->setAddressType                (Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                                ->setFirstname                  ($webOrder[0]->billtocust[0]->firstname)
                                ->setLastname                   ($webOrder[0]->billtocust[0]->lastname)
                                ->setCompany                    ($webOrder[0]->billtocust[0]->company)
                                ->setStreet                     ($webOrder[0]->billtocust[0]->address1 . " " . $webOrder[0]->billtocust[0]->address2)
                                ->setCity                       ($webOrder[0]->billtocust[0]->city)
                                ->setCountry_id                 ($webOrder[0]->billtocust[0]->countrycode)
                                ->setRegion                     ($webOrder[0]->billtocust[0]->state)
                                ->setRegion_id                  ($billRegionId)
                                ->setPostcode                   ($webOrder[0]->billtocust[0]->postalcode)
                                ->setTelephone                  ($webOrder[0]->billtocust[0]->phone)
                                ->setFax                        ($webOrder[0]->billtocust[0]->fax)
                                ->setEmail                      ($webOrder[0]->billtocust[0]->email);

                            $this->waitOnIndexers();
                            if($billingAddress->save()) {
                                $this->logger->info($this->feedName.": Billing address [{$billingAddress->getId()}] saved for order [{$order->getId()}].");
                            } else {
                                $this->logger->error($this->feedName.": Billing address for order [{$order->getId()}] was NOT updated.");
                            }
                            $order->setBillingAddress($billingAddress);

                        } catch(Exception $e) {
                            $this->logger->error($this->feedName . ": Billing address record NOT found for order [{$order->getId()}].");
                        }

                        // Update the order shipping address information.
                        $shipRegionId = $this->getRegionId($regionCollection, $webOrder[0]->shiptocust[0]->state);

                        // Always check if shipping address record exists.
                        // Magento doesn't create shipping address record for non-shipable products (ex. virtual gift card)

                        if($order->getShippingAddress()) {
                            $this->waitOnIndexers();
                            /** @var Mage_Sales_Model_Order_Address $shippingAddress */
                            $shippingAddress = Mage::getModel('sales/order_address')->load($order->getShippingAddress()->getId());

                            $shippingAddress
                            ->setStoreId(Constants::STOREID)
                            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                            ->setCustomerId($order->getCustomerId())
                            ->setPrefix('')
                            ->setFirstname($webOrder[0]->shiptocust[0]->firstname)
                            ->setMiddlename('')
                            ->setFirstname($webOrder[0]->shiptocust[0]->lastname)
                            ->setSuffix('')
                            ->setCompany($webOrder[0]->shiptocust[0]->company)
                            ->setStreet($webOrder[0]->shiptocust[0]->address1 . " " . $webOrder[0]->shiptocust[0]->address2)
                            ->setCity($webOrder[0]->shiptocust[0]->city)
                            ->setCountry_id($webOrder[0]->shiptocust[0]->countrycode)
                            ->setRegion($webOrder[0]->shiptocust[0]->state)
                            ->setRegion_id($shipRegionId)
                            ->setPostcode($webOrder[0]->shiptocust[0]->postalcode)
                            ->setTelephone($webOrder[0]->shiptocust[0]->phone)
                            ->setFax($webOrder[0]->shiptocust[0]->fax)
                            ->setEmail($webOrder[0]->shiptocust[0]->email);

                            $this->waitOnIndexers();
                            if($shippingAddress->save()) {
                                $this->logger->info($this->feedName . ": Shipping address saved for order [{$order->getId()}].");
                            } else {
                                $this->logger->error($this->feedName . ": Shipping address for order [{$order->getId()}] was NOT updated.");
                            }

                            $order->setShippingAddress($shippingAddress);

                        } else {
                            $this->logger->info($this->feedName . ": Shipping address for order [{$order->getId()}] is not required.");
                        }

                        // Shipments - If there are any boxes / shipments.
                        if(count($webOrder->boxes)) {
                            // Need to map internal Ids in boxes -> external in items.
                            $items = array();
                            if(count($webOrder->lineitems)) {
                                foreach ($webOrder->lineitems->children() as $item) {
                                    $it = array();
                                    $it["externallineitemid"] = (int)$item->externallineitemid;
                                    $it["internallineitemid"] = (int)$item->internallineitemid;
                                    $it["externalsku"]        = (string)$item->externalsku;
                                    $it["internalsku"]        = (string)$item->internalsku;
                                    $it["itemstatus"]         = (string)$item->itemstatus;
                                    $it["qtyalreadyshipped"]  = 0;    // Quantity already shipped (in magento)
                                    $it["qtybeingshipped"]    = 0;    // Quantity being shipping (in feed)
                                    $it["qtyorderedmag"]      = 0;    // Quantity ordered (magento)
                                    $it["qtycanship"]         = 0;    // Quantity we can ship.
                                    $it["orderqty"]           = (int)$item->orderqty;
                                    $itemId                   = (string) $item->internallineitemid;
                                    $items[$itemId]           = $it;
                                }
                            }

                            // Find any existing shipments (as tracking numbers).
                            $existingTracknums = array();
                            $existingShipmentCollection = Mage::getResourceModel('sales/order_shipment_collection')
                                ->setOrderFilter($order)
                                ->load();

                            foreach($existingShipmentCollection as $existingShipment) {
                                foreach($existingShipment->getAllTracks() as $tracknum) {
                                    $existingTracknums[] = $tracknum->getNumber();
                                }

                                // Count quantities already shipped per item (as per magento).
                                foreach($existingShipment->getAllItems() as $shippedItem){
                                    foreach($items as &$webItem) {
                                        if($webItem['externallineitemid'] == $shippedItem->getOrderItemId()) {
                                            $webItem['qtyalreadyshipped'] += $shippedItem->getQtyShipped();
                                        }
                                    }
                                }
                            }

                            // We have to pre-count all the items (of this sku) currently being newly shipped (as per feed).
                            foreach($webOrder[0]->boxes->children() as $box) {
                                if(! in_array($box[0]->trackingno, $existingTracknums)) {
                                    foreach($box->items->children() as $boxItem) {
                                        $items[(string)$boxItem[0]->internallineitemid]['qtybeingshipped'] += $boxItem[0]->shipqty;
                                    }
                                }
                            }

                            // Get the qty ordered through magento for each item (sku) in the order, and the quantity that we can still ship.
                            $orderItems = $order->getAllVisibleItems();
                            foreach($items as &$webItem) {
                                foreach($orderItems as $orderItem) {
                                    if($webItem['externallineitemid'] == $orderItem->getId()) {
                                        $webItem['qtyorderedmag'] += intval($orderItem->getQtyOrdered());
                                        $webItem['qtycanship'] = $webItem['qtyorderedmag'] - $webItem['qtyalreadyshipped'];
                                    }
                                }
                            }

                            // Try to add each box as a shipment.
                            foreach ($webOrder[0]->boxes->children() as $box) {
                                $countExternalItems = 0;
                                // Shipments, denoted by tracking numbers are only to be added once.
                                if(! in_array($box[0]->trackingno, $existingTracknums)) {
                                    $convertOrder = new Mage_Sales_Model_Convert_Order();
                                    $shipment = $convertOrder->toShipment($order);
                                    $totalQty = 0;

                                    foreach($box->items->children() as $shipmentItem) {
                                        $sku = $items[(string)$shipmentItem[0]->internallineitemid]["internalsku"];
                                        if($items[(string)$shipmentItem[0]->internallineitemid]["externallineitemid"] != "" && $items[(string)$shipmentItem[0]->internallineitemid]["externallineitemid"] != 0){

                                            // Has external item id.
                                            $this->waitOnIndexers();
                                            /** @var Mage_Sales_Model_Order_Item $orderItem */
                                            $orderItem    = Mage::getModel('sales/order_item')->load($items[(string)$shipmentItem[0]->internallineitemid]["externallineitemid"]);
                                            $shipped_item = $convertOrder->itemToShipmentItem($orderItem);

                                            // Only ship quantities ordered through magento. Get the quantity that we can ship (as per magento) and the quantity to be shipped (as per feed).
                                            $qtyCanShip = $items[(string)$shipmentItem[0]->internallineitemid]["qtycanship"];
                                            $qtyToShip  = $shipmentItem[0]->shipqty;

                                            if($qtyCanShip < $qtyToShip) {
                                                // The quantity that we can ship (as per magento) is less than the quantity to be shipped (as per feed).
                                                $shipQty = $qtyCanShip;
                                                $qtyRem  = $qtyToShip - $qtyCanShip;
                                                if(strpos($sku, "EMB") === false) {
                                                    $shippingComment = "A shipment for this order contains an item not ordered online. Item sku: [" . $sku . "], quantity: [" . $qtyRem . "].";
                                                    $order->addStatusHistoryComment($shippingComment);
                                                    $shipment->addComment($shippingComment, false);
                                                }
                                            } else {
                                                // The quantity to ship is okay.
                                                $shipQty = $qtyToShip;
                                            }

                                            // Decrement the quantity that can be shipped by the amount we're shipping in this box.
                                            $items[(string)$shipmentItem[0]->internallineitemid]["qtycanship"] -= $shipQty;

                                            // Only create a shipment if there is a positive shipment quantity.
                                            if($shipQty > 0) {
                                                $shipped_item->setQty((string) $shipQty);
                                                $shipment->addItem($shipped_item);
                                                $this->logger->info($this->feedName.": Added shipment for order [{$order->getId()}].");
                                                $totalQty     += (int) $shipQty;
                                            }

                                            $countExternalItems++;

                                        } else {
                                            // No external item id.
                                            if(strpos($sku, "EMB") === false) {
                                                $order->addStatusHistoryComment("A shipment for this order contains an item not ordered online. Item Sku: [" . $sku . "] quantity: [" . $shipmentItem[0]->shipqty . "] Internal Item Id: [" . (string)$shipmentItem[0]->internallineitemid . "].");
                                            }
                                        }
                                    }

                                    $this->logger->info($this->feedName.": Total items qty prepared  for shipment [{$totalQty}].");

                                    $shipAt    = trim($box[0]->shipdate);
                                    $createdAt = date('Y-m-d H:i:s', strtotime($shipAt));
                                    $shipment->setCreatedAt($createdAt);
                                    $shipment->setTotalQty($totalQty);

                                    // Just the title.
                                    if(! empty($data['t_UPSID'])) {
                                        $title =  'Carrier-' . $box[0]->carrier . "-UPSID-" . $data['t_UPSID']  . '-Method-' . (string) $box[0]->shipmethod . '-Shipping';
                                    } else {
                                        $title = (string) $box[0]->carrier . "-Shipping";
                                    }

                                    // Tracking info.
                                    $tracking = array(
                                        'carrier_code'  => strtolower(trim($box[0]->carrier)),
                                        'title'         => $title,
                                        'number'        => (string) $box[0]->trackingno,
                                    );

                                    $this->waitOnIndexers();
                                    /** @var Mage_Sales_Model_Order_Shipment_Track $track */
                                    $track = Mage::getModel('sales/order_shipment_track')->addData($tracking);
                                    $shipment->addTrack($track);
                                    $this->logger->info($this->feedName.": Updated shipment tracking info for order [{$order->getId()}].");

                                    // Set the shipping method if required
                                    if( $order->getShippingAddress() && $countExternalItems > 0 ) {
                                        $order->getShippingAddress()->setShippingMethod($this->remapShippingMethod($box[0]->shipmethod));
                                        $order->setShippingMethod($box[0]->shipmethod);

                                        $this->waitOnIndexers();
                                        Mage::getModel('core/resource_transaction')
                                            ->addObject($shipment)
                                            ->addObject($shipment->getOrder())
                                            ->save();

                                        $order->addStatusToHistory('shipped', 'Order is shipped', false);

                                        // Send shipment email for each tracking number if tracking number is not NA
                                        if((string) $box[0]->trackingno != "NA") {
                                            $shipment->sendEmail();
                                            $this->logger->info($this->feedName.": Shipment email is sent to customer [{$order->getId()}].");
                                        } else {
                                            $this->logger->info($this->feedName.": Shipment email has not been sent to customer. Tracking number is not set. [{$order->getId()}].");
                                        }

                                    } else {
                                        $this->logger->error($this->feedName.": Shipping record does NOT exist for this order. Can't  update shipping for order [{$order->getId()}].");
                                    }
                                }
                            }
                        }

                        $summaryorderstatus = (string) $webOrder[0]->summaryorderstatus;

                        // Invoices - Create or update order invoice.
                        if($order->hasInvoices()) {
                            // Existing invoice.
                            $this->logger->info($this->feedName.": Existing Invoice(s). ");
                            $invIncrementIDs   = array();
                            $invoiceCollection = $order->getInvoiceCollection();

                            foreach($invoiceCollection as $invoice) {
                                $invIncrementIDs[] = $invoice->getIncrementId();

                                    // Set the invoice state and save it.
                                if($invoice && $invoice->getTotalQty()) {
                                    if($summaryorderstatus == '0') {
                                        $this->logger->info($this->feedName . " Increment Id [" . $externalorderno . "]: unprocessed.");
                                    } else if($summaryorderstatus == '1') {
                                        $invoice->setData('state', Mage_Sales_Model_Order_Invoice::STATE_OPEN);
                                    } else if($summaryorderstatus == '2') {
                                        $invoice->setData('state', Mage_Sales_Model_Order_Invoice::STATE_PAID);
                                    } else if($summaryorderstatus == '3') {
                                        $invoice->setData('state', Mage_Sales_Model_Order_Invoice::STATE_CANCELED);
                                    } else {
                                        $this->logger->error($this->feedName . " Id [" . $externalorderno . "]: Unknown Status [" . $summaryorderstatus . "].");
                                    }
                                }

                                $this->logger->info($this->feedName . ": Order id [" . $order->getId() . "], invoices [" . implode(",", $invIncrementIDs) . "].");
                            }
                        } else {
                            // New invoice.
                            if($order->canInvoice()) {
                                $this->logger->info($this->feedName.": Start Invoice. ");

                                $invoiceId = Mage::getModel('sales/order_invoice_api_v2')->create($order->getIncrementId(), $order->getAllItems(), "Invoice Created during order sync.", true);

                                // Load the Invoice object.
                                $this->waitOnIndexers();
                                $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceId);

                                // Set the invoice state and save it.
                                if($invoice && $invoice->getTotalQty()) {
                                    if($summaryorderstatus == '0') {
                                        $this->logger->info($this->feedName . " Increment Id [" . $externalorderno . "]: unprocessed.");
                                    } else if($summaryorderstatus == '1') {
                                        $invoice->setData('state', Mage_Sales_Model_Order_Invoice::STATE_OPEN);
                                    } else if($summaryorderstatus == '2') {
                                        $invoice->setData('state', Mage_Sales_Model_Order_Invoice::STATE_PAID);
                                    } else if($summaryorderstatus == '3') {
                                        $invoice->setData('state', Mage_Sales_Model_Order_Invoice::STATE_CANCELED);
                                    } else {
                                        $this->logger->error($this->feedName . " Id [" . $externalorderno . "]: Unknown Status [" . $summaryorderstatus . "].");
                                    }
                                }
                            }
                        }

                        if(isset($invoice)) {
                            $createdAt = date('Y-m-d H:i:s');

                            $invoice->setCreatedAt($createdAt);
                            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);

                            // be sure to use register() instead of save(); otherwise the 0 qty items will be displayed as well and the individual order lines will not be set to 'invoiced'
                            //$invoice->register();
                            // Cannot register existing invoice ERROR
                            $this->waitOnIndexers();
                            $invoice->save();

                            $psvs = array('1', '2', '3');
                            if(in_array($summaryorderstatus, $psvs, true)) {
                                $payment = $order->getPayment();
                                if($payment) {
                                    $details = $payment->getAdditionalInformation();
                                    $newkey = 'IN_ORDER_SYNC-' . date('Y-m-d H:i:s');
                                    $details[$newkey] = "feed note";
                                    $payment->setAdditionalInformation($details);
                                    $this->waitOnIndexers();
                                    $payment->save();
                                    $this->logger->info($this->feedName.": Done payment.");
                                }
                            }

                            // Update the qty invoiced in sales_flat_order_item table
                            foreach($order->getAllItems() as $item) {
                                $qtyOrdered = $item->getQtyOrdered();
                                $item->setQtyInvoiced($qtyOrdered);
                                $this->waitOnIndexers();
                                $item->save();
                            }

                            $this->logger->info($this->feedName.": Done Invoice.");
                        }

                        // Update the order state and status - this should be done last.
                        if($summaryorderstatus == '0') {
                            $order->setData('state', Mage_Sales_Model_Order::STATE_PROCESSING);
                            $order->setStatus('unprocessed');
                        } else if($summaryorderstatus == '1') {
                            $order->setData('state', Mage_Sales_Model_Order::STATE_PROCESSING);
                            $order->setStatus('processed');
                        } else if($summaryorderstatus == '2') {
                            $order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
                            $order->setStatus('complete');
                            $order->setHistoryEntityName("order");
                            $order->addStatusToHistory('complete', 'Order is complete', false);
                        } else if($summaryorderstatus == '3') {
                            $order->setData('state', Mage_Sales_Model_Order::STATE_CANCELED);
                            $order->setStatus('canceled');
                            $order->setHistoryEntityName("order");
                            $order->addStatusToHistory('canceled', 'Order is canceled', false);
                        } else {
                            $this->logger->error($this->feedName . " Increment Id [" . $externalorderno . "]: Unknown Status [" . $summaryorderstatus . "].");
                        }

                        $this->waitOnIndexers();
                        if($order->save()) {
                            $this->logger->info($this->feedName.": Order [{$order->getId()}] is updated.");
                        } else {
                            $this->logger->error($this->feedName.": Order [{$order->getId()}] was NOT updated.");
                        }

                        // See if we need to send an email giftcard.
                        if($order->hasInvoices()) {
                            $this->callGiftcardObserver($webOrder, $order);
                        }

                        // Do some clean up.
                        if(isset($order)) {
                            unset($order);
                        }
                        if(isset($billingAddress)) {
                            unset($billingAddress);
                        }
                        if(isset($shippingAddress)) {
                            unset($shippingAddress);
                        }
                        if(isset($invoice)) {
                            unset($invoice);
                        }
                        if(isset($shipment)) {
                            unset($shipment);
                        }
                        if(isset($payment)) {
                            unset($payment);
                        }

                        $numSynced++;

                    } else {
                        $this->logger->info($this->feedName . " FOUND Confirmation, when IncrementId [" . $externalorderno . "], This is an HC1 order.");
                        $numNotSynced++;
                    }

                } else {
                    $numNotSynced++;
                }

                $currentRow++;
            }

            $feedInfo = $this->addFeedInfo($feedInfo, "num_orders_read", $numRead);
            $feedInfo = $this->addFeedInfo($feedInfo, "num_orders_synced", $numSynced);
            $feedInfo = $this->addFeedInfo($feedInfo, "num_orders_not_synced", $numNotSynced);

            $status = Constants::FEEDSTATUS_S;
            return true;
        } catch (Exception $e) {
            $message = "{$this->feedName}: Exception thrown in " . $e->getFile() . ":" . $e->getLine() . " - message [" . $e->getMessage() . "]";
            $feedErrors[] = $message;
            $this->logger->fatal($message);
            return false;
        } finally {
            $this->setFeedComplete($status, $feedErrors, $feedInfo);
        }
    }
}
