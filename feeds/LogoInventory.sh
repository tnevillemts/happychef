#!/bin/bash
#############################################
#
#	LogoInventory.sh
#
# 	Invoke logo inventory script.
#
#############################################

cd $(dirname "${BASH_SOURCE[0]}")

/usr/bin/php publish/logoinventory.php nofile

exit;
