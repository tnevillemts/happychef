<?php
require_once(__ROOT__ . '/constants.php');
require_once(__ROOT__ . '/feederBase.php');

class syncProducts extends feederBase
{
    /**
     * @param string $magentoVersion
     * @param string $feedType
     */
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion = $magentoVersion;
        $this->feedType = $feedType;
        $this->feedName = Constants::IN_PRODUCT_SYNC_NAME;
        $this->feedUrl = Constants::IN_PRODUCT_SYNC;
        $this->logger = new Logger (Constants::LOGDIR . "/" . $this->feedName);
        parent::__construct();
    }

    /**
     * @param string $attrCode
     * @return int[]
     * @throws Varien_Exception
     */
    private function getLeadTimeOptions($attrCode)
    {
        $values = array();
        $this->waitOnIndexers();
        $valuesCollection = $this->getAttributeOptionValues($attrCode);
        foreach ($valuesCollection as $item) {
            if (strpos(strtolower($item->getValue()), '8 week') !== false) {
                $values[$item->getValue()] = '54';
            } else if (strpos(strtolower($item->getValue()), '7 week') !== false) {
                $values[$item->getValue()] = '47';
            } else if (strpos(strtolower($item->getValue()), '6 week') !== false) {
                $values[$item->getValue()] = '40';
            } else if (strpos(strtolower($item->getValue()), '5 week') !== false) {
                $values[$item->getValue()] = '33';
            } else if (strpos(strtolower($item->getValue()), '4 week') !== false) {
                $values[$item->getValue()] = '26';
            } else if (strpos(strtolower($item->getValue()), '3 week') !== false) {
                $values[$item->getValue()] = '19';
            } else if (strpos(strtolower($item->getValue()), '2 week') !== false) {
                $values[$item->getValue()] = '12';
            } else if (strpos(strtolower($item->getValue()), '10 day') !== false) {
                $values[$item->getValue()] = '7';
            } else if (strpos(strtolower($item->getValue()), '5 business days') !== false) {
                $values[$item->getValue()] = '2';
            } else if (strtolower($item->getValue()) == 'in stock') {
                $values[$item->getValue()] = '0';
            }
        }
        arsort($values);
        return $values;
    }

    /**
     * @param int[] $leadTimeOptions
     * @param int $val
     * @return string|null
     */
    private function getLeadTimeOptionValue($leadTimeOptions, $val)
    {
        foreach ($leadTimeOptions as $key => $value) {
            if ($value != '' && $value <= $val) {
                return $key;
            }
        }
        return null;
    }

    /**
     * @return int[]
     */
    private function getOrderedQtys()
    {
        $orderInv = array();

        // We need to be able to turn this off temporarily. Unprocessed orders will need to be taken into account in production but can't always be in development.
        if (Constants::SYNC_WITH_UNPROCESSED) {
            $this->waitOnIndexers();
            $orders = Mage::getModel('sales/order')->getCollection()
                ->addFieldToFilter('status', 'unprocessed');

            foreach ($orders as $order) {
                $ordered_items = $order->getAllItems();
                foreach ($ordered_items as $item) {
                    $sku = $item->getSku();
                    $qty = $item->getQtyOrdered();
                    if (!array_key_exists($sku, $orderInv)) {
                        $orderInv[$sku] = $qty;
                    } else {
                        $orderInv[$sku] += $qty;
                    }
                }
            }
        }
        return $orderInv;
    }

    /**
     * Adjust for quantities in open orders. These need to be subtracted also.
     *
     * @param string $sku
     * @param int $stockQty
     * @param int[] $ordersInv
     * @return int
     */
    private function adjustQty($sku, $stockQty, $ordersInv)
    {
        $adjusted = $stockQty - $ordersInv[$sku];
        $adjusted = ($adjusted >= 0) ? $adjusted : 0;
        return $adjusted;
    }

    /**
     * @return SimpleXMLElement|null
     */
    private function getPrevWebProducts()
    {
        $lastWebProductsFileName = $this->getLastRunData(Constants::LAST_DATA_TAG);
        $this->logger->info("lastWebProductsFileName [" . $lastWebProductsFileName . "]");

        if (empty($lastWebProductsFileName) || !file_exists($lastWebProductsFileName)) {
            $this->logger->info("Previous products NOT found.");
            return null;
        } else {
            $this->logger->info("Previous products found.");
            return $this->unserializeFromFile($lastWebProductsFileName);
        }
    }

    /**
     * @param SimpleXMLElement $curWebProducts
     * @param string $wpTopEl
     * @return SimpleXMLElement[][]
     */
    private function getWebProducts($curWebProducts, $wpTopEl)
    {
        $wpKey = 'internalsku';
        // Convert and sort the current web products.
        $aCurWebProducts = $this->xml2ArrayUnique($curWebProducts, $wpKey, $wpTopEl);
        $cWebProducts = $aCurWebProducts[$wpTopEl];
        $this->xsort($cWebProducts, $wpKey);
        for ($i = 0; $i < count($cWebProducts); ++$i) {
            $wp[$wpTopEl][] = $cWebProducts[$i];
        }
        return $wp;
    }

    /**
     * @param SimpleXMLElement $curWebProducts
     * @param SimpleXMLElement $prevWebProducts
     * @param string $wpTopEl
     * @return SimpleXMLElement[][]
     */
    private function getDeltaWebProducts($curWebProducts, $prevWebProducts, $wpTopEl)
    {
        $wpKey = 'internalsku';
        $isDelta = false;

        // Convert and sort the current web products.
        $aCurWebProducts = $this->xml2ArrayUnique($curWebProducts, $wpKey, $wpTopEl);
        $cWebProducts = $aCurWebProducts[$wpTopEl];
        $this->xsort($cWebProducts, $wpKey);

        // Convert and sort the previous web products.
        $aPrevWebProducts = $this->xml2ArrayUnique($prevWebProducts, $wpKey, $wpTopEl);
        $pWebProducts = $aPrevWebProducts[$wpTopEl];
        array_pop($pWebProducts);
        $this->xsort($pWebProducts, $wpKey);

        // Find intersection of current and previous web products.
        for ($i = 0, $j = 0; $i < count($cWebProducts);) {
            if (strcmp((string)$pWebProducts[$j]->$wpKey, (string)$cWebProducts[$i]->$wpKey) < 0) {
                $j++;
            } else if (strcmp((string)$pWebProducts[$j]->$wpKey, (string)$cWebProducts[$i]->$wpKey) > 0) {
                $i++;
            } else if (strcmp((string)$pWebProducts[$j]->$wpKey, (string)$cWebProducts[$i]->$wpKey) == 0) {
                $cA = json_decode(json_encode($cWebProducts[$i]), true);
                $pA = json_decode(json_encode($pWebProducts[$i]), true);
                foreach ($cA as $key => $value) {
                    if ($cA[$key] != $pA[$key]) {
                        $isDelta = true;
                    }
                }
                if ($isDelta) {
                    $delta[$wpTopEl][] = $cWebProducts[$i];
                }
                $isDelta = false;
                $i++;
            }
        }
        return $delta;
    }

    /**
     * @return bool
     */
    public function process()
    {
        $this->logger->info(" ");
        if (! $this->checkConnection() || ! $this->checkDatabase()) {
            $this->logger->error($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection");
            return false;
        }

        $this->setFeedStart();
        $this->setStatus(Constants::FEEDSTATUS_R);

        $feedErrors = [];
        $feedInfo = [];
        $status = Constants::FEEDSTATUS_F;
        $readSuccess = false;
        $numRead = 0;
        $numSynced = 0;
        $wpTopEl = 'webproduct';
        $numReadTrys = 10;

        try {
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $this->removeFeedInfoOnDateChange();

            $emailTextFileName = Constants::DATADIR . "/emailText";
            if (file_exists($emailTextFileName)) {
                if (unlink($emailTextFileName)) {
                    $this->logger->info($this->feedName . ": File " . $emailTextFileName . " unlinked.");
                }
            }

            // Try 10 times, every 30 seconds.
            for ($i = 0; $i < $numReadTrys; $i++) {
                $this->logger->info($this->feedName . ": Try to read from [" . $this->feedUrl . "]");

                $xml = $this->getURL($this->feedUrl);

                $this->logger->info($this->feedName . ": XML data successfully retrieved from curl");

                libxml_use_internal_errors(true);
                foreach (libxml_get_errors() as $error) {
                    $this->logger->error($this->feedName . ": libxml error [" . $error->message . "]");
                }

                $curWebProducts = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_PARSEHUGE | LIBXML_NOXMLDECL);
                if ($curWebProducts === false) {
                    $xmlParseError = 'Error: simplexml_load_string failure: ';
                    foreach (libxml_get_errors() as $error) {
                        $xmlParseError .= 'error [' . $error->message . ']; ';
                    }
                    sleep(30);
                } else {
                    $readSuccess = true;
                    $numRead = count($curWebProducts);
                    break;
                }
            }

            if (! $readSuccess) {
                throw new Exception("Could not read product data: $xmlParseError");
            }

            // Get the previous web products.
            $this->logger->info($this->feedName . ": Get previous Web Products");
            $prevWebProducts = $this->getPrevWebProducts();
            if ($prevWebProducts != null) {
                $numPrev = count($prevWebProducts);
            } else {
                $numPrev = 0;
            }

            $this->logger->info($this->feedName . ": Check if there are previous web products");
            if ($numPrev > 0) {
                $this->logger->info($this->feedName . ": Attempting to find delta...");

                // If there are previous web products then find the delta.
                $webProducts = $this->getDeltaWebProducts($curWebProducts, $prevWebProducts, $wpTopEl);
                $this->logger->info($this->feedName . ": Successfully built delta, count [" . count($webProducts) . "]");

            } else {
                // Otherwise use the full current list.
                $this->logger->info($this->feedName . ": Attempting to build full list...");
                $webProducts = $this->getWebProducts($curWebProducts, $wpTopEl);;
                $this->logger->info($this->feedName . ": Successfully built new list, count [" . count($webProducts) . "]");
            }

            $ordersInv = $this->getOrderedQtys();
            $leadTimeOptions = $this->getLeadTimeOptions('custom_stock_status');
            $usedSkus = array();
            $npSkus = array();
            $dupSkus = array();
            $numToSync = count($webProducts[$wpTopEl]);
            $currentRow = 0;

            // Iterate through products in the XML feed and update product inventory in the database.
            foreach ($webProducts[$wpTopEl] as $webProduct) {

                $sku = trim((string)$webProduct[0]->internalsku);
                $this->logger->info(" ");
                $this->logger->info($this->feedName . ": [" . $currentRow . "] Sku [" . $sku . "]");

                // Only sync a  sku once.
                if (!in_array($sku, $usedSkus)) {

                    $synced = false;

                    // Find all products with matching sku and old_sku.
                    // Old_slu attribute may hold the value of original product sku, that was changed
                    // For example simple product 1432-BLKPS might have a duplicate with another sku: 1432-SALE
                    // 1432-BLKPS is disabled, while 1432-SALE is visible. When we sync inventory, we want to update
                    // stock status for both products. Old_sku attribute for 1432-SALE is set to 1432-BLKPS.
                    $this->waitOnIndexers();
                    $productCollection = Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToSelect(array('id', 'sku', 'externalsku', 'custom_stock_status', 'availableamazon', 'shipleadtimedaysamazon', 'old_sku', 'visibility', 'color'))
                        ->addAttributeToFilter(
                            array(
                                array('attribute' => 'sku', 'eq' => $sku),
                            )
                        )->load();

                    $this->logger->info("Count of products for [" . $sku . "][" . $productCollection->count() . "]");

                    foreach ($productCollection as $product) {

                        if ($product && $sku != "emailgc" && $sku != "mailgc") {
                            $product->setExternalsku($webProduct[0]->externalsku);
                            $this->logger->info("Process product -> [" . $product->getSku() . "]");

                            $this->waitOnIndexers();
                            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());

                            if ($stockItem->getId()) {

                                $oldQty = $stockItem->getQty();

                                // Adjust available qty by taking into account orders that have been placed,
                                // but haven't been picked up by HC yet.
                                $qty = $this->adjustQty($product->getSku(), (int) $webProduct[0]->stockqtywww, $ordersInv);
                                $this->logger->info("product sku [" . $sku . "] stockItem oldQty [" . (int) $oldQty . "] -> new qty [" . $qty . "], stockqtywww [" . (int) $webProduct[0]->stockqtywww . "]");

                                $stockItem->setData('use_config_backorders', 0);

                                $itemDiscontinued = $webProduct[0]->itemdiscontinued;
                                $this->logger->info("itemDiscontinued [" . $itemDiscontinued . "]");

                                // If product is not discountinued...
                                if ($itemDiscontinued == 'false') {

                                    $this->logger->info("is_in_stock = 1");
                                    $stockItem->setData('is_in_stock', 1);

                                    // 0 = No Backorders, 1 = Allow Qty Below 0, 2 = Allow Qty Below 0 and Notify Customer.
                                    $stockItem->setData('backorders', 1);
                                } else if ($itemDiscontinued == 'true') {
                                    // if product has been discontinued, set "backorders" flag to false,
                                    // disable product and prepare product for reindexing
                                    if ($qty > 0) {
                                        $this->logger->info("is_in_stock = 1");
                                        $stockItem->setData('is_in_stock', 1);
                                    } else {
                                        $this->logger->info("is_in_stock = 0 -> FLAG");
                                        $stockItem->setData('is_in_stock', 0);

                                        // Disable discountinued product that got out of stock
                                        $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);

                                        // Send email notification to inform about products that got closed out.
                                        $body = "Happy " . date("l") . "!<br><br>
                                        Please note, the following closeout product just got out of stock:<br><br>
                                        {$product->getName()} ({$product->getSku()})<br><br>
                                        The product has been disabled and queued for reindex.
                                        However, double-check to make sure that configurable product doesn't use
                                        images related to this discontinued item.<br><br>
                                        Best,<br>
                                        Ripen Tech
                                        ";

                                        $emailTo = "merchandising@happychef.com";
                                        $subject = "Closeout product - out of stock";
                                        $mail = Mage::getModel('core/email')
                                            ->setToName('Ripen E-Commerce')
                                            ->setToEmail($emailTo)
                                            ->setBody($body)
                                            ->setSubject('Closeout product - out of stock')
                                            ->setFromEmail('info@ri.pn')
                                            ->setFromName('Ripen E-Commerce')
                                            ->setType('html');
                                        try {
                                            $mail->send();
                                            $this->logger->info("Email notification was sent to {$emailTo}. Subject: {$subject}");
                                        } catch (Exception $error) {
                                            $this->logger->error("Problem sending email notification to {$emailTo}. Error: {$error->getMessage()}");
                                        }

                                        // Build an array of product ids that got out of stock. For simple products, use IDs of parent products.
                                        // These products will be re-indexed by a cron.
                                        // (Ids are stored in small files. These files are picked up by a cron
                                        // that runs actual reindex.
                                        $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($product->getId());

                                        if (count($parentIds)) {
                                            foreach ($parentIds as $parentId) {
                                                $productsToReindex[] = $parentId;
                                            }
                                        } else {
                                            if ($product->getVisibility() != 1) {
                                                $productsToReindex[] = $product->getId();
                                            }
                                        }

                                        $this->logger->info("Adding to reindex queue " . implode(', ', $parentIds));
                                    }
                                    $stockItem->setData('backorders', 0);
                                } else {
                                    $this->logger->info("itemDiscontinued [" . $itemDiscontinued . "] unknown value.");
                                }

                                if($oldQty < 0) {
                                    $this->logger->info("product sku [" . $sku . "] stockItem oldQty [" . (int)$oldQty . "] -> Updating qty [" . $qty . "]");
                                }
                                $stockItem->setData('qty', $qty);

                                // Custom Stock Status
                                if ($itemDiscontinued == 'true' && $qty <= 0) {
                                    $this->logger->info($this->feedName . ": Set Custom Stock Status to SOLD OUT for product [" . $product->getSku() . "]");
                                    $product->setCustomStockStatus(560);
                                } else {
                                    $this->logger->info($this->feedName . ": Set Custom Stock Status for product [" . $product->getSku() . "] getLeadTimeOptionValue [" . $this->getLeadTimeOptionValue($leadTimeOptions, (int) $webProduct[0]->shipleadtimedayswww) . "]");
                                    $this->waitOnIndexers();
                                    $css = $this->getAttributeOptionId('custom_stock_status', $this->getLeadTimeOptionValue($leadTimeOptions, (int) $webProduct[0]->shipleadtimedayswww));
                                    $product->setData('custom_stock_status', $css);
                                }
                                $this->waitOnIndexers();
                                $stockItem->setProcessIndexEvents(false);
                                $stockItem->save();

                                if (isset($stockItem)) {
                                    unset($stockItem);
                                }

                            } else {
                                $this->logger->error($this->feedName . ": No stock found item for product [" . $product->getSku() . "]");
                            }

                            if ($webProduct[0]->availableamazon == 'true') {
                                $product->setAvailableamazon(1);
                            }
                            $product->setShipleadtimedaysamazon($webProduct[0]->shipleadtimedaysamazon);
                            $product->setTierPrice($product->getTierPrice());
                            $product->setGroupPrice($product->getGroupPrice());

                            $this->waitOnIndexers();
                            $product->save();

                            $numSynced++;
                            if (isset($product)) {
                                unset($product);
                            }
                            $synced = true;
                        } else {
                            $this->logger->info("Product [" . $webProduct[0]->externalsku . "] no object.");
                        }
                    }

                    if (!$synced) {
                        $npSkus [] = $sku;
                    }

                } else {
                    $dupSkus [] = $sku;
                }
                $usedSkus [] = (string)$webProduct[0]->internalsku;
                $currentRow++;
            }

            // While iterating through all products (loop above) and updating stock qty, we
            // created an array of products that should to be re-indexed, because they got out of stock.
            // Now let's save this array into a file. This file will be picked up by a cron later, for re-indexing.
            if (count($productsToReindex)) {
                $productsToReindex = array_unique($productsToReindex);
                // create a file and drop serialized array there
                $filename = Mage::getBaseDir('base') . "/var/to_reindex/" . date("Y_m_d_H_i_s");
                file_put_contents($filename, serialize($productsToReindex));
                $this->logger->info("Save products that got out of stock into file [" . $filename . "] for re-indexing");
            }

            // Serialize the products from the current invocation to be used by the next invocation.
            $tz = new DateTimeZone(Constants::TIME_ZONE_STR);
            $curDateTime = new DateTime('now', $tz);
            $webProductsFileName = Constants::DATADIR . '/webProducts_' . $curDateTime->format('Y-m-d_H:i:s') . '.data';
            if (!$this->serializeToFile($curWebProducts, $webProductsFileName)) {
                $this->logger->error($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: Data serialization failed.");
            }

            // Record what happened.
            if (!empty($webProductsFileName)) {
                $feedInfo = $this->addFeedInfo($feedInfo, Constants::LAST_DATA_TAG, $webProductsFileName);
            }

            $this->logger->info(" ");
            $this->logger->info($this->feedName . ": numRead [" . $numRead . "] numToSync [" . $numToSync . "] numSynced [" . $numSynced . "]");
            $feedInfo = $this->addFeedInfo($feedInfo, "num_products_read", $numRead);
            $feedInfo = $this->addFeedInfo($feedInfo, "num_products_to_sync", $numToSync);
            $feedInfo = $this->addFeedInfo($feedInfo, "num_products_synced", $numSynced);
            $feedInfo = $this->addFeedInfo($feedInfo, "num_dup_skus", count($dupSkus));

            if (count($npSkus) > 0) {
                $feedInfo = $this->addFeedInfo($feedInfo, "num_products_unprocessed", count($npSkus));
                $feedInfo = $this->addFeedInfo($feedInfo, "unprocessed_skus", $npSkus);

                // Add this to the email message.
                $emailFileHandle = fopen($emailTextFileName, 'w');
                $message = "Number products unprocessed: " . count($npSkus) . ", Unprocessed Products: " . json_encode($npSkus) . "\n";
                fwrite($emailFileHandle, $message);
                fclose($emailFileHandle);
            }

            if (count($dupSkus) > 0) {
                $feedInfo = $this->addFeedInfo($feedInfo, "duplicate_skus", $dupSkus);

                // Add this to the email message.
                $emailFileHandle = fopen($emailTextFileName, 'w');
                $message = "Number duplicate products: " . count($dupSkus) . ", Duplicate Products: " . json_encode($dupSkus) . "\n";
                fwrite($emailFileHandle, $message);
                fclose($emailFileHandle);
            }

            $status = Constants::FEEDSTATUS_S;
            return true;
        } catch (Exception $e) {
            $message = "{$this->feedName}: Exception thrown in " . $e->getFile() . ":" . $e->getLine() . " - message [" . $e->getMessage() . "]";
            $feedErrors[] = $message;
            $this->logger->fatal($message);
            return false;
        } finally {
            $this->setFeedComplete($status, $feedErrors, $feedInfo);
        }
    }
}
