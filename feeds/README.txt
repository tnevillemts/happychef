

Feeds documentation file.
---------------------------------------------------

Manifest:
---------------------------------------------------

// Constants required by feeder programs.
	constants.php   

// Call sequence. 
	OrderProductSyncFeed.sh   
		-> OrderProductSyncFeed.php 		// driver script 
			-> syncOrders.php   		// feeder class 
			-> syncProducts.php 		// feeder class
	SendCatalogRequestFeed.sh   
		-> SendCatalogRequestFeed.php 		// driver script	
			-> sendCatalogRequest.php 	// feeder class 
	SendProductsFeed.sh   
		-> SendProductsFeed.php 		// driver script 
			-> sendProducts.php     	// feeder class 

// Base class of feeder classes.
feederBase.php  

// other
sendOpenOrders.php      
sendUnprocessedOrders.php  

// Typical cron entries.
---------------------------------------------------
# (20 minute interval is fine) Order and Product in sync feeds from happychef into happychef.com.
10,30,50 * * * * cd /var/www/html/happychef-beta/feeds; ./OrderProductSyncFeed.sh >> /var/log/feeds/IN_ORDER_SYNC.log;

# Run product catalog feed to pregenerate static XML file for faster loading
0 */3 * * * cd /var/www/html/happychef-beta/feeds; ./ProductCatalog.sh > /dev/null

# Run logo sync feed
30 10 * * * cd /var/www/html/happychef-beta/feeds; ./LogoSyncFeed.sh > /dev/null

# Run shopping feed (former catalog feed for DRe) to pregenerate static XML file and FTP to godatafeed.com
30 4 * * * cd /var/www/html/happychef-beta/feeds; ./ShoppingFeed.sh > /dev/null
