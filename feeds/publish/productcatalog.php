<?php
define('__ROOT__', dirname(__FILE__).'/../');

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/sendProducts.php');
require_once(__ROOT__ . '/syncOrders.php');
require_once(__ROOT__ . '/syncProducts.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $outSendProductsObj = new sendProducts($magentoVersion, Constants::FEEDTYPE_SIMPLE);
    $inSyncOrdersObj = new syncOrders($magentoVersion, Constants::FEEDTYPE_SIMPLE);
    $inSyncProductsObj = new syncProducts($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Establish how long we will wait.
    $numWaitIntervals = 20;
    $waitSeconds = 30;

    // Check if mutually excluded.
    $outSendProductsObj->checkCanRun();

    for ($i = 0; $i < $numWaitIntervals; $i++) {
        try {
            $inSyncOrdersObj->checkCanRun();
            $inSyncProductsObj->checkCanRun();
        } catch (MutexException $e) {
            $outSendProductsObj->setStatus(Constants::FEEDSTATUS_B);
            sleep($waitSeconds);
            continue;
        }

        $outSendProductsObj->lock();
        $outSendProductsSuccess = $outSendProductsObj->process();
        $outSendProductsObj->unlock();

        if (! $outSendProductsSuccess) {
            throw new Exception($outSendProductsObj::PROCESS_FAILURE_MSG_PREFIX . 'Number of wait intervals [' . $i . ']');
        }

        // Output XML
        header('Content-Type: application/xml; charset=utf-8');
        echo $outSendProductsObj->xml;

        $logger->info("Success: " . __FILE__ . ', number of wait intervals [' . $i . ']');
        return;
    }

    // If we get here we've exceeded our wait.
    throw new MutexException("Exceeded $i intervals waiting for order & product feed mutexes to clear.");
}

/**
 * @var Logger $logger
 * @var bool $loadFile Whether to return cached file from disk (if present).
 * @return void
 * @throws Exception
 */
function loadFile($logger, $loadFile)
{
    Mage::init();

    $filename = Mage::getBaseDir() . '/feeds/publish/static/' . Constants::OUT_PRODUCT_DATA_NAME . ".xml";

    $connection = Mage::getModel('core/resource')->getConnection('core_read');
    $sql = "SELECT * FROM `mag_feed_control` WHERE feed_name = '" . Constants::OUT_PRODUCT_DATA_NAME . "'";
    $record = $connection->fetchRow($sql);

    $toTime = time();
    $fromTime = strtotime($record["run_finished"]);
    $hoursPassed = round(abs($toTime - $fromTime) / 60 / 60, 2);
    $generateFrequency = 6;

    /*
     * If it has been less than {generateFrequency} hours since last time file was generated,
     * don't regenerate file, use the existing one.
     */
    if (file_exists($filename) && filesize($filename) && $hoursPassed < $generateFrequency && $loadFile) {
        header('Content-Type: application/xml; charset=utf-8');
        echo file_get_contents($filename);
    } else {
        ob_start();
        process($logger);
        $output = ob_get_clean();
        if (! $output) {
            throw new Exception("ERROR creating static XML for {$filename}: No data to write.");
        }

        echo $output;

        $writeSuccess = file_put_contents($filename, $output);
        if ($writeSuccess === false) {
            throw new Exception("ERROR writing data into static XML for {$filename}");
        }
    }
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::OUT_PRODUCT_DATA_NAME);
try {
    if (!isset($_GET["nofile"]) && (!$argv[1] || $argv[1] != 'nofile')) {
        loadFile($logger, true);
    } else {
        loadFile($logger, false);
    }
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
