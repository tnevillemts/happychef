<?php
define('__ROOT__', dirname(__FILE__).'/../');

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/sendUnprocessedOrders.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $outSendUnprocessedOrdersObj = new sendUnprocessedOrders($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if mutually excluded.
    $outSendUnprocessedOrdersObj->checkCanRun();

    $outSendUnprocessedOrdersObj->lock();
    $outSendUnprocessedOrdersSuccess = $outSendUnprocessedOrdersObj->process();
    $outSendUnprocessedOrdersObj->unlock();

    if (! $outSendUnprocessedOrdersSuccess) {
        throw new Exception($outSendUnprocessedOrdersObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    // Output XML
    header('Content-Type: application/xml; charset=utf-8');
    echo $outSendUnprocessedOrdersObj->xml;

    $logger->info("Success: " . __FILE__);
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::OUT_UNPROCESSED_ORDERS_NAME);
try {
    process($logger);
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
