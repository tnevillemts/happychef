<?php
define('__ROOT__', dirname(__FILE__).'/../');

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/sendEGCOrders.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $outSendEGCOrdersObj = new sendEGCOrders($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if mutually excluded.
    $outSendEGCOrdersObj->checkCanRun();

    $outSendEGCOrdersObj->lock();
    $outSendEGCOrdersSuccess = $outSendEGCOrdersObj->process();
    $outSendEGCOrdersObj->unlock();

    if (! $outSendEGCOrdersSuccess) {
        throw new Exception($outSendEGCOrdersObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    // Output XML
    header('Content-Type: application/xml; charset=utf-8');
    echo $outSendEGCOrdersObj->xml;

    $logger->info("Success: " . __FILE__);
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::OUT_EGC_ORDERS_NAME);
try {
    process($logger);
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
