<?php
define('__ROOT__', dirname(__FILE__).'/../');

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/sendCatalogRequest.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $outSendCatalogRequestObj = new sendCatalogRequest($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if mutually excluded.
    $outSendCatalogRequestObj->checkCanRun();

    $outSendCatalogRequestObj->lock();
    $outSendCatalogRequestSuccess = $outSendCatalogRequestObj->process();
    $outSendCatalogRequestObj->unlock();

    if (! $outSendCatalogRequestSuccess) {
        throw new Exception($outSendCatalogRequestObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    // Output XML
    header('Content-Type: application/xml; charset=utf-8');
    echo $outSendCatalogRequestObj->xml;

    $logger->info("Success: " . __FILE__);
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::OUT_CATALOG_REQUEST_NAME);
try {
    process($logger);
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
