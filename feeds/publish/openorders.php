<?php
define('__ROOT__', dirname(__FILE__).'/../');

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/sendOpenOrders.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $outSendOpenOrdersObj = new sendOpenOrders($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if mutually excluded.
    $outSendOpenOrdersObj->checkCanRun();

    $outSendOpenOrdersObj->lock();
    $outSendOpenOrdersSuccess = $outSendOpenOrdersObj->process();
    $outSendOpenOrdersObj->unlock();

    if (! $outSendOpenOrdersSuccess) {
        throw new Exception($outSendOpenOrdersObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    // Output XML
    header('Content-Type: application/xml; charset=utf-8');
    echo $outSendOpenOrdersObj->xml;

    $logger->info("Success: " . __FILE__);
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::OUT_OPEN_ORDERS_NAME);
try {
    process($logger);
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
