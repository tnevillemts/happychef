<?php
define('__ROOT__', dirname(__FILE__).'/../');

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/sendLogos.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $outSendLogosObj = new sendLogos($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if mutually excluded.
    $outSendLogosObj->checkCanRun();

    $outSendLogosObj->lock();
    $outSendLogosSuccess = $outSendLogosObj->process();
    $outSendLogosObj->unlock();

    if (! $outSendLogosSuccess) {
        throw new Exception($outSendLogosObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    // Output XML
    header('Content-Type: application/xml; charset=utf-8');
    echo $outSendLogosObj->xml;

    $logger->info("Success: " . __FILE__);
}

/**
 * @var Logger $logger
 * @var bool $loadFile Whether to return cached file from disk (if present).
 * @return void
 * @throws Exception
 */
function loadFile($logger, $loadFile)
{
    Mage::init();

    $filename = Mage::getBaseDir() . '/feeds/publish/static/' . Constants::OUT_LOGO_DATA_NAME . ".xml";

    $connection = Mage::getModel('core/resource')->getConnection('core_read');
    $sql = "SELECT * FROM `mag_feed_control` WHERE feed_name = '" . Constants::OUT_LOGO_DATA_NAME . "'";
    $record = $connection->fetchRow($sql);

    $toTime = time();
    $fromTime = strtotime($record["run_finished"]);
    $hoursPassed = round(abs($toTime - $fromTime) / 60 / 60, 2);
    $generateFrequency = 6;

    /*
     * If it has been less than {generateFrequency} hours since last time file was generated,
     * don't regenerate file, use the existing one.
     */
    if (file_exists($filename) && filesize($filename) && $hoursPassed < $generateFrequency && $loadFile) {
        header('Content-Type: application/xml; charset=utf-8');
        echo file_get_contents($filename);
    } else {
        ob_start();
        process($logger);
        $output = ob_get_clean();
        if (! $output) {
            throw new Exception("ERROR creating static XML for {$filename}: No data to write.");
        }

        echo $output;

        $writeSuccess = file_put_contents($filename, $output);
        if ($writeSuccess === false) {
            throw new Exception("ERROR writing data into static XML for {$filename}");
        }
    }
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::OUT_LOGO_DATA_NAME);
try {
    if (!isset($_GET["nofile"]) && (!$argv[1] || $argv[1] != 'nofile')) {
        loadFile($logger, true);
    } else {
        loadFile($logger, false);
    }
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
