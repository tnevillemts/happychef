<?php

require_once(__ROOT__ . '/MutexException.php');

abstract class feederBase
{
    const PROCESS_FAILURE_MSG_PREFIX = 'Feed processing failure: ';
    const MUTEX_SET_MSG = 'Cannot run feed. Mutex is set.';

    protected $magentoVersion;
    protected $feedName;
    protected $feedType;
    protected $feedUrl;
    protected $parentFeedUrl;
    protected $feedControlTableName;
    protected $feedLogTableName;
    protected $showQueries;
    protected $logId;
    protected $writeConnection;
    protected $readConnection;

    /** @var Logger */
    protected $logger;

    /**
     * @return bool
     */
    abstract public function process();

    public function __construct()
    {
        $this->feedControlTableName = "mag_feed_control";
        $this->feedLogTableName = "mag_feed_log";
        $this->logId = null;
        $this->showQueries = false;

        $resource = Mage::getSingleton('core/resource');
        $this->writeConnection = $resource->getConnection('core_write');
        $this->readConnection = $resource->getConnection('core_write');
    }

    /**
     * @return DateTime
     */
    private function getCurrentDateTime()
    {
        $tz = new DateTimeZone(Constants::TIME_ZONE_STR);
        $curDateTime = new DateTime('now', $tz);
        return $curDateTime;
    }

    /**
     * @param string $url
     * @return bool
     */
    protected function getURL($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10 * 60);  // limit to 10 minutes (arbitrary but generous)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (strpos($url, "https") !== false) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_PORT, 443);
        } else {
            curl_setopt($ch, CURLOPT_PORT, 80);
        }

        $this->logger->info(__FILE__ . " Before curl_exec()");
        if (!$ret = curl_exec($ch)) {
            $err = curl_error($ch);
            $this->logger->error("Curl request failed: " . $err);
            $this->logger->error("Failed curl URL: " . $url);
        } else {
            $this->logger->info(__FILE__ . " curl_exec() success");
        }
        $this->logger->info(__FILE__ . " After curl_exec()");
        curl_close($ch);
        return $ret;
    }

    /**
     * @return bool
     */
    protected function checkControlTable()
    {
        try {
            $ret = $this->readConnection->fetchOne("SHOW TABLES LIKE '{$this->feedControlTableName}'");
            if ($ret !== false) {
                $this->logger->info("Found DB table " . $this->feedControlTableName);
            } else {
                $sql = " CREATE TABLE " . $this->feedControlTableName . "(
                         feed_id        INT NOT NULL AUTO_INCREMENT,
                         feed_name     	VARCHAR(100) NOT NULL,
                         feed_url       VARCHAR(300) NOT NULL,
                         feed_status    VARCHAR(40)  NOT NULL,
                         feed_mutex     VARCHAR(1)   ,
                         run_started    DATETIME,
                         run_finished   DATETIME,
                         feed_errors    TEXT,
                         feed_info      TEXT,
                         PRIMARY KEY ( feed_id ))  ";

                if ($this->writeConnection->query($sql)) {
                    $this->logger->info("Table " . $this->feedControlTableName . " create SUCCESS");
                } else {
                    $this->logger->error("Table " . $this->feedControlTableName . " create FAILURE");
                    return false;
                }
            }
        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function checkLogTable()
    {
        try {
            $ret = $this->readConnection->fetchOne("SHOW TABLES LIKE '{$this->feedLogTableName}'");
            if ($ret !== false) {
                $this->logger->info("Found DB table " . $this->feedLogTableName);
            } else {
                $sql = " CREATE TABLE " . $this->feedLogTableName . "(
                     id         	INT NOT NULL AUTO_INCREMENT,
                     feed_name     	VARCHAR(100) NOT NULL,
                     feed_status    VARCHAR(40)  NOT NULL,
                     feed_url       TEXT,
                     feed_info    	TEXT,
                     run_started    DATETIME,
                     run_finished   DATETIME,
                     PRIMARY KEY ( id )) ";

                if ($this->writeConnection->query($sql)) {
                    $this->logger->info("Table " . $this->feedLogTableName . " create SUCCESS");
                } else {
                    $this->logger->error("Table " . $this->feedLogTableName . " create FAILURE");
                    return false;
                }
            }
        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function checkControlRow()
    {
        try {
            if (!empty($this->feedName)) {

                $sql = " SELECT count(*) cnt FROM " . $this->feedControlTableName . " WHERE feed_name = '" . $this->feedName . "'";
                $cnt = $this->readConnection->fetchOne($sql);

                if ($cnt) {
                    return true;
                } else {
                    $sql = " INSERT INTO " . $this->feedControlTableName . " (feed_name, feed_url, feed_status, feed_mutex, run_started, run_finished, feed_errors, feed_info) " .
                        " VALUES('" . $this->feedName . "', 'na', 'NEW', 0, '1900-01-01 01:01:01', '1900-01-01 01:01:01', '', '') ";
                    $result = $this->writeConnection->query($sql);
                    if (!$result) {
                            $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Insert failed");
                            if($this->showQueries)
                                $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                            return false;
                    }
                }

            }
        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function checkConnection() {
        if (!$this->readConnection || !$this->writeConnection) {
            $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection.");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function checkDatabase()
    {
        if ($this->checkControlTable()) {
            if ($this->checkLogTable()) {
                if ($this->checkControlRow()) {
                    $this->logger->info("Checking control row: Success");
                } else {
                    $this->logger->error("Control row not ready");
                    return false;
                }
            } else {
                $this->logger->error("Log table not ready");
                return false;
            }
        } else {
            $this->logger->error("Control table not ready");
            return false;
        }
        return true;
    }

    /**
     * @param string $feedId
     * @return bool
     */
    public function checkChildren($feedId)
    {
        $sql = " SELECT * FROM " . $this->feedControlTableName . " WHERE parent_feed_id = '" . $feedId . "'";
        $result = $this->readConnection->fetchAll($sql);

        if (!count($result)) {
            return true;
        }

        if ($result[0]["feed_mutex"])
            return false;
        else {
            return $this->checkChildren($result[0]["feed_id"]);
        }
    }

    /**
     * @param string $feedName
     * @return void
     */
    public function checkCanRun($feedName = null)
    {
        $success = false;

        try {
            if ($feedName != null) {
                $fn = $feedName;
            } else {
                $fn = $this->feedName;
            }
            $sql = " SELECT * FROM " . $this->feedControlTableName . " WHERE feed_name = '" . $fn . "'";
            $result = $this->readConnection->fetchAll($sql);
            if (!$result) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Select failed");
                if ($this->showQueries) {
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                }
            } else {
                $feedMutex = $result[0]['feed_mutex'];
                if ($feedMutex == 0 && $this->checkChildren($result[0]["feed_id"])) {
                    $success = true;
                }
            }

        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
        }

        if (! $success) {
            throw new MutexException(self::MUTEX_SET_MSG);
        }
    }

    /**
     * @param string $status
     * @param string $pname
     * @return bool
     */
    public function setStatus($status, $pname = null)
    {
        try {
            if (empty($pname)) {
                $name = $this->feedName;
            } else {
                $name = $pname;
            }
            $sql = " UPDATE " . $this->feedControlTableName .
                " SET feed_status = '" . $status . "' " .
                " WHERE feed_name = '" . $name . "' ";

            $result = $this->writeConnection->query($sql);

            if (!$result) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Update failed");
                if ($this->showQueries)
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                return false;
            }
            if ($status == Constants::FEEDSTATUS_S) {
                $sql = " UPDATE " . $this->feedControlTableName . " SET feed_errors = '' " . " WHERE feed_name = '" . $name . "' ";
                $result = $this->writeConnection->query($sql);
                if (!$result) {
                    $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Update failed");
                    if($this->showQueries)
                            $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function setFeedStart()
    {
        try {
            $curDateTime = $this->getCurrentDateTime();
            $sql = " UPDATE " . $this->feedControlTableName .
                " SET run_started     = '" . $curDateTime->format('Y-m-d H:i:s') . "' " .
                " WHERE feed_name = '" . $this->feedName . "' ";

            $result = $this->writeConnection->query($sql);
            if (!$result) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Update failed");
                if ($this->showQueries)
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                return false;
            }

            if ($this->feedType == Constants::FEEDTYPE_SIMPLE) {
                $sql = " INSERT INTO " . $this->feedLogTableName . " (feed_name, feed_status, run_started) " .
                    " VALUES ('" . $this->feedName . "', '" . Constants::FEEDSTATUS_R . "', '" . $curDateTime->format('Y-m-d H:i:s') . "')";

                $result = $this->writeConnection->query($sql);
                if (!$result) {
                    $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Insert failed");
                    if($this->showQueries)
                            $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                    return false;
                }
                $this->logId = $this->writeConnection->lastInsertId();

            }

        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function lock()
    {
        try {
            $sql = " UPDATE " . $this->feedControlTableName .
                " SET feed_mutex  =  '1' " .
                " WHERE feed_name = '" . $this->feedName . "' ";

            $result = $this->writeConnection->query($sql);
            if (!$result) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Update failed");
                if ($this->showQueries)
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                return false;
            }
            $this->setStatus(Constants::FEEDSTATUS_P);

        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function unlock()
    {
        try {
            $sql = " UPDATE " . $this->feedControlTableName .
                " SET feed_mutex  =  0 " .
                " WHERE feed_name = '" . $this->feedName . "' ";

            $result = $this->writeConnection->query($sql);
            if (!$result) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Update failed");
                if ($this->showQueries)
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                return false;
            } else {
                $this->logger->info(__FILE__ . " " . __FUNCTION__ . " Success.");
            }

        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function removeFeedInfo()
    {
        try {
            $sql = " UPDATE " . $this->feedControlTableName .
                " SET feed_info  =  '' " .
                " WHERE feed_name = '" . $this->feedName . "' ";

            $result = $this->writeConnection->query($sql);
            if (!$result) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Updated failed");
                if ($this->showQueries)
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                return false;
            }


        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * Clears the feed_info column in the feed control table if the feed last finished at least a day ago.
     *
     * @return bool
     */
    protected function removeFeedInfoOnDateChange()
    {
        try {
            $sql = " SELECT run_finished FROM " . $this->feedControlTableName .
                " WHERE feed_name = '" . $this->feedName . "' ";
                $runFinished = $this->readConnection->fetchOne($sql);

                $tz = new DateTimeZone(Constants::TIME_ZONE_STR);
            $runFinishedDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $runFinished, $tz);
            $curDateTime = $this->getCurrentDateTime();
            if (is_object($runFinishedDateTime)) {
                // Calculate how many days ago the last run finished.
                $interval = $runFinishedDateTime->diff($curDateTime)->days;
                if ($interval > 0) {
                    $this->removeFeedInfo();
                }
            } else {
                $this->logger->error("ABC Error: runFinished non-object");
                return false;
            }

        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @param string $status
     * @param array|string $feedErrors
     * @param array|string $feedInfo
     * @return bool
     */
    protected function setFeedComplete($status, $feedErrors, $feedInfo)
    {
        // TODO: Check DB server connection and reconnect if necessary.
        //       Currently if feed fails due to connection failure to MOM, DB connection can time out due to
        //       inactivity while retrying MOM connection. That then leaves feed in a mutex-locked state because an
        //       "MySQL server has gone away" exception is thrown from here that causes the control script to think
        //       the script crashed (rather than working as expected, even if in a failure mode).

        try {
            if (is_array($feedErrors)) $feedErrors = implode("\n", $feedErrors);

            $curDateTime = $this->getCurrentDateTime();
            $sql = " UPDATE " . $this->feedControlTableName .
                " SET feed_status  = '" . $status . "', " .
                "     run_finished = '" . $curDateTime->format('Y-m-d H:i:s') . "', " .
                "     feed_errors  = '" . $feedErrors . "', " .
                "     feed_info    = '" . json_encode($feedInfo) . "' " .
                " WHERE feed_name = '" . $this->feedName . "' ";

            $result = $this->writeConnection->query($sql);
            if (!$result) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Update failed");
                if ($this->showQueries)
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                return false;
            }

            if (!empty($this->logId) && $this->feedType == Constants::FEEDTYPE_SIMPLE) {
                $sql = " UPDATE " . $this->feedLogTableName .
                    " SET feed_status  = '" . $status . "', " .
                    "     run_finished = '" . $curDateTime->format('Y-m-d H:i:s') . "', " .
                    "     feed_info    = '" . json_encode($feedInfo) . "' " .
                    " WHERE id = " . $this->logId;
                $result = $this->writeConnection->query($sql);
                if (!$result) {
                    $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Update failed");
                    if ($this->showQueries)
                        $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                    return false;
                }
                $this->logId = null;
            }

        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return false;
        }
        return true;
    }

    /**
     * @param array $info
     * @param string $label
     * @param mixed $data
     * @return array
     */
    protected function addFeedInfo($info, $label, $data)
    {
        $info[$label] = $data;
        return $info;
    }

    /**
     * @return string|null
     */
    public function getLastDataFileName()
    {
        $fileName = (string) $this->getLastRunData(Constants::LAST_DATA_TAG);

        if (! file_exists($fileName)) {
            $this->removeFeedInfo();
            return null;
        }

        return $fileName;
    }

    /**
     * @param string $source
     * @return string
     */
    public function remapShippingMethod($source)
    {
        $target = '';
        switch ($source) {
            case 'EXP':
                $target = '1DA';
                break;
            case 'PRI':
                $target = '2DA';
                break;
            case 'SEL':
                $target = '3DS';
                break;
            case 'STD':
                $target = 'GND';
                break;
            case null:
                $target = null;
                break;
            default:
                $this->logger->error($this->feedName . ": Unknown shipping method found [" . $source . "]");
                break;
        }
        return $target;
    }

    /**
     * @param string $label
     * @return mixed
     */
    public function getLastRunData($label)
    {
        try {

            $sql = " SELECT feed_info FROM " . $this->feedControlTableName . " WHERE feed_name = '" . $this->feedName . "'";
            $feedInfo = $this->readConnection->fetchOne($sql);
            if (!$feedInfo) {
                $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Select failed");
                if ($this->showQueries) {
                    $this->logger->error(__FUNCTION__ . " sql [" . $sql . "]");
                }
                return null;
            } else {
                $feedInfoArray = json_decode($feedInfo, true);

                if (array_key_exists($label, $feedInfoArray)) {
                    return is_array($feedInfoArray[$label]) ? implode(",", $feedInfoArray[$label]) : $feedInfoArray[$label];
                } else {
                    $this->logger->error(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: Cannot find key [ $label ] in data from last run.");
                    return null;
                }
            }

        } catch (Exception $e) {
            $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
            return null;
        }
    }

    /**
     * @param mixed $elements
     * @param string $fileName
     * @return bool
     */
    protected function serializeToFile($elements, $fileName)
    {
        $sData = '';
        $i = 0;
        foreach ($elements as $element) {
            $line = '';
            if ($i == 0) {
                foreach ($element as $key => $value) {
                    $line .= $key . ',';
                }
                $sData .= rtrim($line, ",") . "\n";
                $line = '';
            }
            foreach ($element as $key => $value) {
                $line .= $value . ',';
            }
            $sData .= rtrim($line, ",") . "\n";
            $i++;
        }

        if (file_exists(Constants::DATADIR)) {
            $fret = file_put_contents($fileName, $sData);
            if ($fret == false) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * @param string $fileName
     * @return SimpleXMLElement|null
     */
    protected function unserializeFromFile($fileName)
    {
        if (!empty($fileName) && file_exists($fileName)) {
            $fileHandle = fopen($fileName, "r");
            $currentRow = 0;
            $XML = new SimpleXMLElement("<news></news>");
            $XML->addChild("webproduct");
            while (($line = fgetcsv($fileHandle, Constants::BUFFERSIZE, ",")) !== false) {
                $numberOfColumns = count($line);
                if ($currentRow < 1) {
                    $headings = $line;
                } else {
                    for ($i = 0; $i < $numberOfColumns; $i++) {
                        $data[$headings[$i]] = $line[$i];
                    }
                    $XML->webproduct [] = new SimpleXMLElement("<news></news>");
                    foreach ($data as $key => $value) {
                        $XML->webproduct[$currentRow - 1]->addChild($key, $value);
                    }
                }
                $currentRow++;
            }
        } else {
            $this->logger->error(__FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " File not found [" . $fileName . "]");
            return null;
        }
        return $XML;
    }

    /**
     * @param SimpleXMLElement $xmlObject
     * @param string $key
     * @param string $el
     * @return SimpleXMLElement[][]
     */
    protected function xml2ArrayUnique($xmlObject, $key, $el)
    {
        $uniqueKeys = array();
        // Covert the top level to array.
        foreach ((array)$xmlObject as $index => $node) {
            $converted[$index] = (is_object($node)) ? $this->xml2arrayUnique($node, $key, $el) : $node;
        }
        // Now eliminate duplicate SimpleXMLElements.
        foreach ($converted[$el] as $xmlElement) {
            if (!in_array((string)$xmlElement->$key, $uniqueKeys)) {
                $uniqueKeys[] = (string)$xmlElement->$key;
                $uniqueElements[$el][] = $xmlElement;
            }
        }
        return $uniqueElements;
    }

    /**
     * @param SimpleXMLElement[] &$nodes
     * @param string $el
     * @param int $order SORT_ASC | SORT_DESC
     * @return void
     */
    protected function xsort(&$nodes, $el, $order = SORT_ASC)
    {
        $sortProxy = array();
        foreach ($nodes as $k => $node) {
            $sortProxy[$k] = (string)$node->$el;
        }
        array_multisort($sortProxy, $order, $nodes);
    }

    /**
     * @param string $string
     * @return string
     */
    public function prepForXml($string)
    {
        return htmlspecialchars(preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string));
    }

    /**
     * @return bool
     */
    public function waitOnIndexers() {
        $proceed      = true;
        $sleepSeconds = 5;
        do {
            try {
                if($proceed == false) {
                    sleep($sleepSeconds);
                    $proceed = true;
                }
                $sql = " SELECT indexer_code, status FROM " . Constants::DATABASENAME . ".mag_index_process";
                $result = $this->readConnection->fetchAll($sql);
                if (!$result) {
                    $this->logger->error("Info: " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Select failed");
                    if($this->showQueries)
                    $this->logger->error("Info: " . __FUNCTION__ . " sql [" . $sql . "]");
                    return false;
                } else {
                    foreach($result as $row) {
                        $indexerCode = $row['indexer_code'];
                        if($row['status'] == 'working') {
                            $this->logger->info("Info: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " working indexer_code [" . $indexerCode . "]");
                            $proceed = false;
                            break;
                        }
                    }
                }
            } catch (Exception $e) {
                $this->logger->error("Exception: " . __FILE__ . " line " . __LINE__ . ", function " . __FUNCTION__ . " [" . $e->getMessage() . "]");
                return false;
            }
        } while($proceed == false);
        return true;
    }

    /**
     * @param string $attrCode
     * @return Mage_Eav_Model_Resource_Entity_Attribute_Option_Collection
     * @throws Varien_Exception
     */
    protected function getAttributeOptionValues($attrCode)
    {
        $attribute = Mage::getModel('eav/entity_attribute');
        $attribute->loadByCode('catalog_product', $attrCode);
        $valuesCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setAttributeFilter($attribute->getId())
            ->setStoreFilter($attribute->getStoreId())
            ->load();
        return $valuesCollection;
    }

    /**
     * @param string $attrCode
     * @param string $val
     * @return int
     * @throws Varien_Exception
     */
    protected function getAttributeOptionId($attrCode, $val)
    {
        $values = array();
        $valuesCollection = $this->getAttributeOptionValues($attrCode);
        foreach ($valuesCollection as $item) {
            $values[$item->getValue()] = $item->getId();
        }
        return $values[$val];
    }
}
