#!/bin/bash
#############################################
#
#	LogoSyncFeed.sh
#
# 	Invoke feeder script.
#
#############################################

cd $(dirname "${BASH_SOURCE[0]}")

FEED_ROOT='/var/www/html/happychef-beta/feeds';

echo "-----------------------------------------------"
echo "Starting Sync Feed: New and Changed Logos..."

/usr/bin/php LogoSyncFeed.php
result=$?

echo "Check corresponding log file for details ($FEED_ROOT/logs)"
echo "-----------------------------------------------"

exit $result
