#!/bin/bash
#############################################
#
#	ShoppingFeed.sh
#
# 	Invoke shopping feed script.
#
#############################################

cd $(dirname "${BASH_SOURCE[0]}")

/usr/bin/php ShoppingFeed.php
exit $?
