<?php
require_once(__ROOT__ . '/constants.php');
require_once(__ROOT__ . '/feederBase.php');

class syncLogos extends feederBase
{
    /**
     * @param string $magentoVersion
     * @param string $feedType
     */
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion = $magentoVersion;
        $this->feedType = $feedType;
        $this->feedName = Constants::IN_LOGO_SYNC_NAME;
        $this->feedUrl = Constants::IN_LOGO_SYNC;
        $this->logger = new Logger (Constants::LOGDIR . "/" . $this->feedName);
        parent::__construct();
    }

    /**
     * @param SimpleXMLElement $curWebLogos
     * @param string $wlTopEl
     * @return SimpleXMLElement[][]
     */
    private function getWebLogos($curWebLogos, $wlTopEl)
    {
        $wpKey = 'externallogoid';
        $aCurWebLogos = $this->xml2ArrayUnique($curWebLogos, $wpKey, $wlTopEl);
        $cWebLogos = $aCurWebLogos[$wlTopEl];
        $this->xsort($cWebLogos, $wpKey);
        for ($i = 0; $i < count($cWebLogos); ++$i) {
            $wl[$wlTopEl][] = $cWebLogos[$i];
        }
        return $wl;
    }

    /**
     * @return bool
     */
    public function process()
    {
        if (! $this->checkConnection() || ! $this->checkDatabase()) {
            $this->logger->fatal($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection.");
            return false;
        }

        $this->setFeedStart();
        $this->setStatus(Constants::FEEDSTATUS_R);

        $feedErrors = [];
        $feedInfo = [];
        $status = Constants::FEEDSTATUS_F;
        $readSuccess = false;
        $wlTopEl = 'weblogo';
        $numReadTrys = 10;

        try {
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $this->removeFeedInfoOnDateChange();

            // Try 10 times, every 30 seconds.
            for ($i = 0; $i < $numReadTrys; $i++) {
                $this->logger->info($this->feedName . ": Try to read from [" . $this->feedUrl . "]");

                $xml = $this->getURL($this->feedUrl);

                $this->logger->info($this->feedName . ": XML data successfully retrieved from curl");

                libxml_use_internal_errors(true);
                foreach (libxml_get_errors() as $error) {
                    $this->logger->error($this->feedName . ": libxml error [" . $error->message . "]");
                }

                $curWebLogos = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_PARSEHUGE);
                if ($curWebLogos === false) {
                    $xmlParseError = 'Error: simplexml_load_string failure: ';
                    foreach (libxml_get_errors() as $error) {
                        $xmlParseError .= 'error [' . $error->message . ']; ';
                    }
                    sleep(30);
                } else {
                    $readSuccess = true;
                    $numRead = count($curWebLogos);
                    break;
                }
            }

            if (! $readSuccess) {
                throw new Exception("Could not read logo data: $xmlParseError");
            }

            $this->logger->info($this->feedName . ": Attempting to build full list...");
            $webLogos = $this->getWebLogos($curWebLogos, $wlTopEl);
            $this->logger->info($this->feedName . ": Successfully built new list");

            $currentRow = 0;
            $numLogosSynced = 0;

            // Iterate through logos in the XML feed and update logo inventory in the database.
            foreach ($webLogos[$wlTopEl] as $webLogo) {

                $logoId = trim((string)$webLogo[0]->externallogoid);
                $this->logger->info($this->feedName . ": Processing logo [ID: #{$logoId}]");

                $logo = Mage::getModel('embroidery/logo')->load($logoId);
                if ($logo) {

                    $savedDigitizedLogos = json_decode($logo->getDigitizedLogos(), true);

                    $customerId = trim((string)$webLogo[0]->externalcustid);

                    if ($logo->getCustomerId() == $customerId) {

                        $digitizedLogos = $webLogo[0]->digitizedlogos;
                        $newDigitizedLogosArray = array();

                        foreach ($digitizedLogos->digitizedlogo as $digitizedLogo) {

                            $sku = trim((string)$digitizedLogo->logosku);
                            $this->logger->info($this->feedName . ": [" . $currentRow . "] Checking logo [SKU: " . $sku . "]");
                            $filename = trim((string)$digitizedLogo->logoimagefilename);
                            $categoryId = (int)$digitizedLogo->appliestoversion;
                            $markedForRemoval = $digitizedLogo->toremove == 'False' ? false : true;

                            $stitchCount = trim((string)$digitizedLogo->logostitchcount);
                            $updatedAt = date('Y-m-d H:i:s', strtotime(trim((string)$digitizedLogo->logoupdatedt)));

                            $this->logger->info($this->feedName . ": Need to sync?");

                            $lastModified = date('Y-m-d H:i:s', strtotime($savedDigitizedLogos[$sku]["last_modified"]) + 300);

                            if (!array_key_exists($sku, $savedDigitizedLogos)){
                                $this->logger->info($this->feedName . ": {$sku} NOT found in savedDigitizedLogos - SYNC");
                            } else {
                                $this->logger->info($this->feedName . ": {$sku} found in savedDigitizedLogos - NO SYNC");
                            }

                            if ($lastModified < $updatedAt){
                                $this->logger->info($this->feedName . ": Date {$lastModified} < {$updatedAt} - SYNC");
                            } else {
                                $this->logger->info($this->feedName . ": Date {$lastModified} >= {$updatedAt} - NO SYNC");
                            }

                            if (!array_key_exists($sku, $savedDigitizedLogos) || $lastModified < $updatedAt) {

                                $this->logger->info($this->feedName . ": Updating logo [ID: #{$logoId}][SKU: {$sku}]");
                                $today = date('Y-m-d');

                                $path = Constants::PATH_TO_DIGITIZED_LOGOS;

                                $originDir = "embroidery_digitized/";
                                $originFile = $path . $originDir . $filename;

                                $destinationDir = "embroidery_uploads/{$today}/digitized/";

                                if (!is_dir($path . $destinationDir)) {
                                    mkdir($path . $destinationDir, 0775, true);
                                }

                                $name = substr($filename, 0, (strrpos($filename, ".") - 1));
                                $extension = substr(strrchr($filename, "."), 1);

                                $newFilename = md5(strtolower($name) . time()) . "." . (strtolower($extension));
                                $digitizedLogo = $destinationDir . $newFilename;
                                $destinationFile = $path . $digitizedLogo;

                                if (file_exists($originFile)) {

                                    $this->logger->info($this->feedName . ": Copying file [{$originFile}] to [{$destinationFile}]");

                                    copy($originFile, $destinationFile);

                                    if(!$markedForRemoval){
                                        $newDigitizedLogosArray[$sku] = array(
                                            "stitches_count" => $stitchCount,
                                            "status" => 1,
                                            "last_modified" => date('Y-m-d H:i:s'),
                                            "digitized_logo" => $digitizedLogo,
                                            "category_id" => $categoryId
                                        );
                                        $this->sendTransactionalEmail($digitizedLogo, $customerId);
                                    }

                                    // Temporary support old structure (oen logo has only one digitized version)
                                    if($categoryId == 0){
                                        $logo->setDigitizedLogo($digitizedLogo);
                                        $logo->setSku($sku);
                                        $logo->setStitchesCount($stitchCount);
                                        $logo->setStatus(1);
                                        $logo->setLastModified(date('Y-m-d H:i:s'));
                                    }


                                } else {
                                    $this->logger->error($this->feedName . ": File not found [{$originFile}]. Skipping.");
                                }

                            } else {
                                $this->logger->info($this->feedName . ": No updates [ID: #{$logoId}][SKU: {$sku}]. Skipping.");
                            }
                        }


                        if (count($newDigitizedLogosArray)) {
                            $logo->setDigitizedLogos(json_encode($newDigitizedLogosArray));
                            $logo->save();
                            $this->logger->info($this->feedName . ": Logo saved [ID: {$logo->getId()}][SKU: {$sku}]");
                            $numLogosSynced++;
                        }

                    } else {

                        $this->logger->error($this->feedName . ": Mismatched customer IDs [{$customerId}][{$logo->getCustomerId()}] for logo [ID: #{$logoId}]. Skipping.");
                    }

                } else {

                    $this->logger->error($this->feedName . ": Logo not found [ID: #{$logoId}]");

                    /*
                     * Try to find logo record by Order ID and Customer ID.
                     * Resolve this case programmatically only if one logo was used for the order
                     * If multiple logos are present in the order, must resolve manually via admin
                     */

                }

                $currentRow++;
            }

            $feedInfo = $this->addFeedInfo($feedInfo, "num_logos_read", $numRead);
            $feedInfo = $this->addFeedInfo($feedInfo, "num_logos_synced", $numLogosSynced);

            $status = Constants::FEEDSTATUS_S;
            return true;
        } catch (Exception $e) {
            $message = "{$this->feedName}: Exception thrown in " . $e->getFile() . ":" . $e->getLine() . " - message [" . $e->getMessage() . "]";
            $feedErrors[] = $message;
            $this->logger->fatal($message);
            return false;
        } finally {
            $this->setFeedComplete($status, $feedErrors, $feedInfo);
        }
    }

    /**
     * @param string $digitizedLogo
     * @param string $custId
     * @return void
     * @throws Mage_Core_Model_Store_Exception
     * @throws Varien_Exception
     */
    public function sendTransactionalEmail($digitizedLogo, $custId)
    {
        // Transactional Email Template's ID
        $templateId = 105;

        // Set sender information
        $senderName = Mage::getStoreConfig('trans_email/ident_support/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');
        $sender = array(
            'name' => $senderName,
            'email' => $senderEmail
        );

        // Set recipient information
        $customer = Mage::getModel('customer/customer')->load($custId);
        $recipientEmail = $customer->getEmail();
        $recipientName = $customer->getFirstname() . $customer->getLastname();

        // Get Store ID
        $storeId = Mage::app()->getStore()->getId();

        $logo_file = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $digitizedLogo;
        $site_url = Constants::SITEURL;

        $vars = array(
            'logo_file' => $logo_file,
            'site_url' => $site_url
        );

        $translate = Mage::getSingleton('core/translate');

        // Send Transactional Email
        Mage::getModel('core/email_template')
            ->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $vars, $storeId);

        $translate->setTranslateInline(true);
    }
}
