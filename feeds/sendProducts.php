<?php
require_once(__ROOT__ . '/constants.php');
require_once(Constants::ROOTDIR . Constants::SITEROOT . "/app/Mage.php");
require_once(__ROOT__ . '/feederBase.php');

class sendProducts extends feederBase
{
    protected $filename;
    protected $domTree;
    protected $currentProduct;

    ////////////////////////////////////////////////////////////////////////////////////////
    //
    // constructor
    //
    ////////////////////////////////////////////////////////////////////////////////////////
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion = $magentoVersion;
        $this->feedType = $feedType;
        $this->feedName = Constants::OUT_PRODUCT_DATA_NAME;
        $this->logger = new Logger (Constants::LOGDIR . "/" . $this->feedName);
        $this->filename = Constants::DATADIR . "/sendProducts-" . date("Y-m-d-H-i-s") . ".csv";

        parent::__construct();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getAttributeOptionValues()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    private function getOutgoingStatusId($customStockStatus)
    {
        $ret = '';
        if (strpos(strtolower($customStockStatus),'7 week') !== false) {
            $ret = '10';
        } else if (strpos(strtolower($customStockStatus),'sold out') !== false) {
            $ret = '8';
        } else if (strpos(strtolower($customStockStatus),'6 week') !== false) {
            $ret = '7';
        } else if (strpos(strtolower($customStockStatus),'6 week') !== false) {
            $ret = '5';
        } else if (strpos(strtolower($customStockStatus),'4 week') !== false) {
            $ret = '5';
        } else if (strpos(strtolower($customStockStatus),'3 week') !== false) {
            $ret = '4';
        } else if (strpos(strtolower($customStockStatus),'2 week') !== false) {
            $ret = '3';
        } else if (strpos(strtolower($customStockStatus),'10 day') !== false) {
            $ret = '2';
        } else if (strpos(strtolower($customStockStatus),'5 business days') !== false) {
            $ret = '9';
        } else if (strtolower($customStockStatus) == 'in stock') {
            $ret = '1';
        }
        return $ret;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // getVariationNode()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function getVariationNode($simpleProduct, $parentProduct = false, &$currentRow)
    {
        $curDate = date('Y-m-d');
        $pricesByAttributeValues = array();
        $valuesCollection = $this->getAttributeOptionValues("custom_stock_status");

        $this->logger->info(" ");
        $this->logger->info("Start product->");

        $this->logger->info($this->feedName . ": numSent [" . $currentRow . "] sku [" . $simpleProduct->getSku() . "]");

        $this->currentProduct->appendChild($this->domTree->createElement('title', htmlspecialchars($simpleProduct->getName())));
        $this->currentProduct->appendChild($this->domTree->createElement('model', $parentProduct ? htmlspecialchars($parentProduct->getModel()) : htmlspecialchars($simpleProduct->getModel())));

        $color = $simpleProduct->getAttributeText('color');
        if (is_array($color)) {
            $color = implode(',', $color);
        }
        $colorGroup = $simpleProduct->getAttributeText('colorgroup');
        if (is_array($colorGroup)) {
            $colorGroup = implode(',', $colorGroup);
        }
        $this->currentProduct->appendChild($this->domTree->createElement('color', $parentProduct ? htmlspecialchars($color) : htmlspecialchars($colorGroup)));
        $this->currentProduct->appendChild($this->domTree->createElement('sizeid', 0));
        $this->currentProduct->appendChild($this->domTree->createElement('lengthid', 0));
        $this->currentProduct->appendChild($this->domTree->createElement('sizename', htmlspecialchars($simpleProduct->getAttributeText('size'))));
        $this->currentProduct->appendChild($this->domTree->createElement('length', htmlspecialchars($simpleProduct->getAttributeText('length'))));
        //$this->currentProduct->appendChild($this->domTree->createElement('itemcodewww', htmlspecialchars($simpleProduct->getSku())));
        $this->currentProduct->appendChild($this->domTree->createElement('itemcodewww', $parentProduct ? "{$parentProduct->getId()}-{$simpleProduct->getId()}" : "0-{$simpleProduct->getId()}"));

        // Per Jim's request, use Old Sku if it's present.
        $oldsku = $simpleProduct->getOldSku() ? $simpleProduct->getOldSku() : $simpleProduct->getSku();
        $this->currentProduct->appendChild($this->domTree->createElement('itemcodemom', htmlspecialchars($oldsku)));

        $this->currentProduct->appendChild($this->domTree->createElement('corporate', 0));

        //$status = $parentProduct ? $parentProduct->getStatus() : $simpleProduct->getStatus();
        if ($parentProduct) {
            $status = max($parentProduct->getStatus(), $simpleProduct->getStatus());
        } else {
            $status = $simpleProduct->getStatus();
        }

        $productactive = ($status == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) ? '0' : '1';
        $this->currentProduct->appendChild($this->domTree->createElement('productactive', htmlspecialchars($productactive)));
        $this->currentProduct->appendChild($this->domTree->createElement('paactive', htmlspecialchars($productactive)));

        $spProduct = null;
        if ($parentProduct) {
            $spProduct = $parentProduct;
        } else {
            $spProduct = $simpleProduct;
        }

        $specialPrice = round($spProduct->getSpecialPrice(), 2);
        $sfd = date('Y-m-d', strtotime($spProduct->getSpecialFromDate()));
        if ($sfd == '1969-12-31') {
            $sfd = '1900-12-31';
            $sfds = 'Not set';
        } else {
            $sfds = $sfd;
        }

        $std = date('Y-m-d', strtotime($spProduct->getSpecialToDate()));
        if ($std == '1969-12-31') {
            $std = '2100-12-31';
            $stds = 'Not set';
        } else {
            $stds = $std;
        }

        $specialPriceDateRange = "Special price date range - (From: " . $sfds . " To:" . $stds . ")";

        unset($pricesByAttributeValues);
        unset($attributes);

        if ($parentProduct) {
            if (!isset($pricesByAttributeValues)) {
                $pricesByAttributeValues = array();
                $basePrice = $parentProduct->getFinalPrice();
                $attributes = $parentProduct->getTypeInstance(true)->getConfigurableAttributes($parentProduct);
                foreach ($attributes as $attribute) {
                    $prices = $attribute->getPrices();
                    foreach ((array)$prices as $price) {
                        if ($price['is_percent']) {
                            // If the price is specified in percents.
                            $pricesByAttributeValues[$price['value_index']] = (float)$price['pricing_value'] * $basePrice / 100;
                        } else {
                            // If the price is absolute value.
                            $pricesByAttributeValues[$price['value_index']] = (float)$price['pricing_value'];
                        }
                    }
                }
            }

            $totalPrice = $basePrice;
            $upcharges = 0;
            foreach ($attributes as $attribute) {
                $attributeCode = $simpleProduct->getData($attribute->getProductAttribute()->getAttributeCode());
                if (isset($pricesByAttributeValues[$attributeCode])) {
                    $upcharges += $pricesByAttributeValues[$attributeCode];
                    $totalPrice += $pricesByAttributeValues[$attributeCode];
                }
            }

            $max = $totalPrice;
            $min = $totalPrice;

            $priceRange = array($min, $max);
        } else {
            $priceRange = array($simpleProduct->getPrice(), $specialPrice);
        }

        if (!empty($specialPrice) && ($sfd <= $curDate) && (($spProduct->getSpecialToDate() && $std >= $curDate) || (!$spProduct->getSpecialToDate()))) {
            $this->currentProduct->appendChild($this->domTree->createElement('special', 1));
            $this->currentProduct->appendChild($this->domTree->createElement('unitprice', min($priceRange)));
        } else {
            $this->currentProduct->appendChild($this->domTree->createElement('special', 0));
            $this->currentProduct->appendChild($this->domTree->createElement('unitprice', max($priceRange)));
        }

        if ($parentProduct) {
            $this->currentProduct->appendChild($this->domTree->createElement('parentinfo', $parentProduct ? "Sku: {$spProduct->getSku()}, Upcharges: {$upcharges}" : ""));
            $this->currentProduct->appendChild($this->domTree->createElement('specialpricedaterange', htmlspecialchars($specialPriceDateRange)));
        }

        unset($totalPrice);
        unset($upcharges);
        unset($priceRange);
        unset($min);
        unset($max);

        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simpleProduct->getId());
        if ($stockItem->getId()) {
            $this->currentProduct->appendChild($this->domTree->createElement('available', htmlspecialchars($stockItem->getData('is_in_stock'))));
        }

        $i = 0;
        $statusId = '0';
        foreach ($valuesCollection as $item) {
            $curStockStatusValue = $this->getAttributeOptionId("custom_stock_status", $item->getValue());
            $customStockStatus = $simpleProduct->getCustomStockStatus();
            if ($customStockStatus == $curStockStatusValue) {
                $attrText = $simpleProduct->getAttributeText('custom_stock_status');
                $statusId = $this->getOutgoingStatusId($attrText);
                break;
            }
            $i++;
        }
        $this->currentProduct->appendChild($this->domTree->createElement('stockstatus', htmlspecialchars($statusId)));

        $stockQty = intval($stockItem->getQty());
        $this->currentProduct->appendChild($this->domTree->createElement('stockqty', htmlspecialchars($stockQty)));

        $this->currentProduct->appendChild($this->domTree->createElement('productid', htmlspecialchars($simpleProduct->getId())));

        // Misha: Under question...
        $this->currentProduct->appendChild($this->domTree->createElement('coloractive', htmlspecialchars($productactive)));
        $this->currentProduct->appendChild($this->domTree->createElement('swatchid', $parentProduct ? htmlspecialchars($simpleProduct->getColor()) : htmlspecialchars($simpleProduct->getColorgroup())));

        $currentRow++;
        if ($currentRow % 50 == 0) {
            $this->logger->info($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", currentRow [" . $currentRow . "]");
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // process()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function process()
    {
        if ($this->checkConnection() && $this->checkDatabase()) {
            $feedErrors = '';
            $feedInfo = '';
            $currentRow = 1;
            $this->setFeedStart();
            $this->setStatus(Constants::FEEDSTATUS_R);
            $attributes = null;

            try {
                // Get all of the products in the catalog.
                $collection = Mage::getResourceModel('catalog/product_collection')
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('visibility', array('in' => array(
                        Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                        Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                        Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
                    )))
                    ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));

                $this->domTree = new DOMDocument('1.0', 'UTF-8');
                $this->domTree->formatOutput = true;
                $this->domTree->preserveWhiteSpace = false;

                $xmlRoot = $this->domTree->createElement("variations");
                $xmlRoot = $this->domTree->appendChild($xmlRoot);
                $xmlRoot->setAttribute("ts", date('n/j/Y H:i:s A'));
                $xmlRoot->setAttribute("number", $collection->count());
                $xmlRoot->setAttribute("db", "LIVE");

                foreach ($collection as $product) {
                    if ($product->getTypeId() == "configurable") {
                        $ids = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());
                        if (count($ids[0]) > 0) {
                            $childProducts = Mage::getModel('catalog/product')->getCollection()
                                ->addAttributeToSelect('*')
                                ->addAttributeToFilter('entity_id', $ids);

                                // As per PROD-2995 we will send all child products whether enabled or disabled and simply mark them as such.
                                // But this could change so let's record that we did this.
                                //->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));

                            $this->logger->info($this->feedName . " collection query [" . $childProducts->getSelect() . "]");
                            foreach ($childProducts as $child) {
                                $this->currentProduct = $xmlRoot->appendChild($this->domTree->createElement("variation"));
                                $this->getVariationNode($child, $product, $currentRow);
                            }
                        } else {
                            $this->logger->info($this->feedName . " getChildrenIds count(ids[0]) [" . count($ids[0]) . "] sku [" . $product->getSku() . "] ids [" . print_r($ids, true) . "]");
                        }
                    } else {
                        $this->currentProduct = $xmlRoot->appendChild($this->domTree->createElement("variation"));
                        $this->getVariationNode($product, false, $currentRow);
                    }
                }

                $this->xml = $this->domTree->saveXML();

                $status = Constants::FEEDSTATUS_S;
            } catch (Exception $e) {
                $feedErrors .= 'Exception thrown in ' . __FILE__ . ' - message [' . $e->getMessage() . ']';
                $this->logger->error($feedErrors);

                $status = Constants::FEEDSTATUS_F;
            }

            // Record what happened.
            $feedInfo = $this->addFeedInfo($feedInfo, "products_sent", $currentRow);
            $feedInfo = $this->addFeedInfo($feedInfo, "output_file", $this->filename);
            $this->setFeedComplete($status, $feedErrors, $feedInfo);

            if (!empty($feedErrors)) {
                return false;
            } else {
                return true;
            }

        } else {
            $this->logger->error($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection");
            return false;
        }
    }
}
