<?php
require_once(__ROOT__.'/constants.php');
require_once(Constants::ROOTDIR . Constants::SITEROOT . "/app/Mage.php");
require_once(__ROOT__.'/feederBase.php');

class sendOpenOrders extends feederBase {
    public $xml;

    ////////////////////////////////////////////////////////////////////////////////////////
    //
    // constructor
    //
    ////////////////////////////////////////////////////////////////////////////////////////
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion  = $magentoVersion;
        $this->feedType        = $feedType;
        $this->feedName        = Constants::OUT_OPEN_ORDERS_NAME;
        $this->feedUrl         = Constants::PUBLISHURL."/".Constants::OUT_OPEN_ORDERS;

        $this->logger          = new Logger ( Constants::LOGDIR."/".$this->feedName );
        parent::__construct();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // process()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function process()
    {
        if($this->checkConnection() && $this->checkDatabase()) {

            $feedErrors = '';
            $feedInfo   = '';
            $this->setFeedStart();
            $this->setStatus(Constants::FEEDSTATUS_R);

            try {
                $orders = Mage::getModel('sales/order')->getCollection()
                    ->addAttributeToFilter('status', array('in' => array("unprocessed", "processed", "magento-new")))
                    ->addAttributeToFilter('confirmation', array('null' => true));


                $domTree = new DOMDocument('1.0', 'UTF-8');
                $domTree->formatOutput = true;
                $domTree->preserveWhiteSpace = false;

                $xmlRoot = $domTree->createElement("weborders");
                $xmlRoot = $domTree->appendChild($xmlRoot);
                $xmlRoot->setAttribute("ts", date('n/j/Y H:i:s A'));
                $i = 0;
                foreach($orders as $order) {
                    $i++;
                    $currentOrder = $domTree->createElement("weborder");
                    $currentOrder = $xmlRoot->appendChild($currentOrder);
                    $currentOrder->appendChild($domTree->createElement('OrderNum',      $i));
                    $currentOrder->appendChild($domTree->createElement('Confirmation',  $order->getIncrementId()));
                    $currentOrder->appendChild($domTree->createElement('Status',        $order->getStatus() == "unprocessed" ? 0 : 1));
                }

                // Resulting XML
                $this->xml =  $domTree->saveXML();

                $status = Constants::FEEDSTATUS_S;
            } catch (Exception $e) {
                $feedErrors .= ' Exception thrown in ' . __FILE__ . ' - message [' . $e->getMessage() . ']';
                $this->logger->error($feedErrors);

                $status = Constants::FEEDSTATUS_F;
            }

            // Record what happened.
            $feedInfo = $this->addFeedInfo($feedInfo, "num_open_orders_generated", $i);
            $this->setFeedComplete($status, $feedErrors, $feedInfo);

            if(! empty($feedErrors)) {
                return false;
            } else {
                return true;
            }
        } else {
             echo (__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection.\n");
            return false;
        }
    }
}
