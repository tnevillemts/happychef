<?php
require_once(__ROOT__ . '/constants.php');
require_once(Constants::ROOTDIR . Constants::SITEROOT . "/app/Mage.php");
require_once(__ROOT__ . '/feederBase.php');

class sendLogos extends feederBase
{
    public $xml;
    protected $filename;

    ////////////////////////////////////////////////////////////////////////////////////////
    //
    // constructor
    //
    ////////////////////////////////////////////////////////////////////////////////////////
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion = $magentoVersion;
        $this->feedType = $feedType;
        $this->feedName = Constants::OUT_LOGO_DATA_NAME;
        $this->logger = new Logger (Constants::LOGDIR . "/" . $this->feedName);
        $this->filename = Constants::DATADIR . "/sendLogos-" . date("Y-m-d-H-i-s") . ".csv";

        parent::__construct();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // process()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function process()
    {
        if ($this->checkConnection() && $this->checkDatabase()) {
            $feedErrors = '';
            $feedInfo = '';
            $currentRow = 1;
            $this->setFeedStart();
            $this->setStatus(Constants::FEEDSTATUS_R);
            $attributes = null;

            try {
                // Get all of the products in the catalog.
                $logos = Mage::getModel('embroidery/logo')->getCollection();

                $domTree = new DOMDocument('1.0', 'UTF-8');
                $domTree->formatOutput = true;
                $domTree->preserveWhiteSpace = false;

                $xmlRoot = $domTree->createElement("weblogos");
                $xmlRoot = $domTree->appendChild($xmlRoot);
                $xmlRoot->setAttribute("ts", date('n/j/Y H:i:s A'));
                $xmlRoot->setAttribute("number", $logos->count());
                $xmlRoot->setAttribute("db", "LIVE");

                $status = [0=>"Pending", 1=>"Digitized", 2=>"Canceled"];
                foreach ($logos as $logo) {
                    $digitizingstatus = 0;

                    $currentProduct = $xmlRoot->appendChild($domTree->createElement("weblogo"));
                    $this->logger->info("Start product ->");
                    $currentProduct->appendChild($domTree->createElement('externallogoid', htmlspecialchars($logo->getId())));
                    $currentProduct->appendChild($domTree->createElement('externalcustid', htmlspecialchars($logo->getCustomerId())));
                    $currentProduct->appendChild($domTree->createElement('logoname', htmlspecialchars($logo->getName())));
                    $currentProduct->appendChild($domTree->createElement('logoimageurl', Mage::getBaseUrl('media').$logo->getLogo()));
                    $currentProduct->appendChild($domTree->createElement('logocreatedt', date('Y-m-d H:i:s', strtotime($logo->getCreatedAt()))));
                    $currentDigitizedLogos = $currentProduct->appendChild($domTree->createElement("digitizedlogos"));
                    if($logo->getDigitizedLogos()){
                        $digitizedLogos = json_decode($logo->getDigitizedLogos(), true);

                        //resort logos array based on category_id (asc)
                        $cats = array();
                        foreach ($digitizedLogos as $key => $row)
                        {
                            $cats[$key] = $row['category_id'];
                        }
                        array_multisort($cats, SORT_ASC, $digitizedLogos);
                        $i=0;
                        foreach($digitizedLogos as $logoSku => $digitizedLogo){
                            $currentDigitizedLogo =  $currentDigitizedLogos->appendChild($domTree->createElement("digitizedlogo"));
                            $currentDigitizedLogo->appendChild($domTree->createElement('logosku', htmlspecialchars($logoSku)));
                            $currentDigitizedLogo->appendChild($domTree->createElement('logoimagefilename', ''));
                            $currentDigitizedLogo->appendChild($domTree->createElement('logostitchcount', htmlspecialchars($digitizedLogo["stitches_count"])));
                            $currentDigitizedLogo->appendChild($domTree->createElement('appliestoversion', htmlspecialchars($digitizedLogo["category_id"])));
                            $currentDigitizedLogo->appendChild($domTree->createElement('logoupdatedt', date('Y-m-d H:i:s', strtotime($digitizedLogo["last_modified"]))));

                            if ($i == 0){
                                $digitizingstatus = $digitizedLogo["status"];
                            }
                            $i++;
                         }
                    }

                    $currentProduct->appendChild($domTree->createElement('digitizingstatus', $status[$digitizingstatus]));

                }

                $this->xml = $domTree->saveXML();
                $status = Constants::FEEDSTATUS_S;
            } catch (Exception $e) {
                $feedErrors .= 'Exception thrown in ' . __FILE__ . ' - message [' . $e->getMessage() . ']';
                $this->logger->error($feedErrors);

                $status = Constants::FEEDSTATUS_F;
            }

            // Record what happened.
            $feedInfo = $this->addFeedInfo($feedInfo, "logo_sent", $currentRow);
            $feedInfo = $this->addFeedInfo($feedInfo, "output_file", $this->filename);
            $this->setFeedComplete($status, $feedErrors, $feedInfo);

            if (!empty($feedErrors)) {
                return false;
            } else {
                return true;
            }

        } else {
            $this->logger->error($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection");
            return false;
        }
    }
}

