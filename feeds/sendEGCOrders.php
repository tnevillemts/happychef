<?php
require_once(__ROOT__.'/constants.php');
require_once(Constants::ROOTDIR . Constants::SITEROOT . "/app/Mage.php");
require_once(__ROOT__.'/feederBase.php');

class sendEGCOrders extends feederBase
{
    public $xml;

     ////////////////////////////////////////////////////////////////////////////////////////
    //
    // constructor
    //
    ////////////////////////////////////////////////////////////////////////////////////////
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion  = $magentoVersion;
        $this->feedType        = $feedType;
        $this->feedName        = Constants::OUT_EGC_ORDERS_NAME;
        $this->feedUrl         = Constants::PUBLISHURL."/".Constants::OUT_EGC_ORDERS;
        $this->logger          = new Logger ( Constants::LOGDIR."/".$this->feedName );
        parent::__construct();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // process()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function process()
    {
        if($this->checkConnection() && $this->checkDatabase()) {

            $feedErrors = '';
            $feedInfo   = '';
            $this->setFeedStart();
            $this->setStatus(Constants::FEEDSTATUS_R);

            try {
                $domTree = new DOMDocument('1.0', 'UTF-8');
                $domTree->formatOutput = true;
                $domTree->preserveWhiteSpace = false;

                $xmlRoot = $domTree->createElement("giftcards");
                $xmlRoot = $domTree->appendChild($xmlRoot);
                $xmlRoot->setAttribute("ts", date('n/j/Y H:i:s A'));

                // Get date range for collection filter.
                $toDate = date('Y-m-d H:i:s', time());
                $fromDate = date('Y-m-d H:i:s', strtotime('-1 month', time()));

                $dateRange = $domTree->createElement("dateRange");
                $dateRange = $xmlRoot->appendChild($dateRange);
                $dateRange->appendChild($domTree->createElement('fromDate', $fromDate));
                $dateRange->appendChild($domTree->createElement('toDate', $toDate));

                $productId = Mage::getModel("catalog/product")->getIdBySku('emailgc');

                $orderItemCollection = Mage::getResourceModel('sales/order_item_collection')
                    ->addFieldToSelect('order_id')
                    ->addFieldToSelect('product_options')
                    ->addFieldToSelect('created_at')
                    ->addAttributeToFilter('product_id', array('eq' => $productId))
                    ->addAttributeToFilter('created_at', array('from'=>$fromDate, 'to'=>$toDate))
                    ->addOrder('created_at', 'desc')
                    ->load();

                $numItemsFound = count($orderItemCollection);
                $this->logger->info($this->feedName . " File [" . __FILE__ . "] Function [" . __FUNCTION__ . "] numItemsFound [" . $numItemsFound . "]");

                if($numItemsFound > 0) {
                    $i = 0;
                    foreach($orderItemCollection as $orderItem) {
                        $orderId = $orderItem->getData('order_id');
                        $productOptions = $orderItem->getProductOptions();
                        $this->logger->info($this->feedName . " File [" . __FILE__ . "] Function [" . __FUNCTION__ . "] order [" . $orderId . "] productOptions [" . print_r($orderItem->getProductOptions(), true) . "]");
                        $order = Mage::getModel('sales/order')->load($orderId);

                        $currentCard = $domTree->createElement("weborder");
                        $currentCard = $xmlRoot->appendChild($currentCard);
                        $currentCard->appendChild($domTree->createElement('cardNum', $i));
                        $currentCard->appendChild($domTree->createElement('externalOrderNo', $order->getIncrementId()));
                        $currentCard->appendChild($domTree->createElement('senderName', $productOptions['aw_gc_sender_name']));
                        $currentCard->appendChild($domTree->createElement('customerEmail', $order->getCustomerEmail()));
                        $currentCard->appendChild($domTree->createElement('orderStatus', $order->getStatus()));
                        $currentCard->appendChild($domTree->createElement('createDate', $order->getCreatedAt()));
                        $currentCard->appendChild($domTree->createElement('recipientFirstName', $productOptions['aw_gc_recipient_first_name']));
                        $currentCard->appendChild($domTree->createElement('recipientLastName', $productOptions['aw_gc_recipient_last_name']));
                        $currentCard->appendChild($domTree->createElement('recipientEmailAddress', $productOptions['aw_gc_recipient_email']));
                        $currentCard->appendChild($domTree->createElement('code', $productOptions['aw_gc_created_codes'][0]));
                        $currentCard->appendChild($domTree->createElement('amount', $productOptions['aw_gc_amounts']));
                        $i++;
                    }
                }

                // Resulting XML
                $this->xml = $domTree->saveXML();

                $status = Constants::FEEDSTATUS_S;
            } catch (Exception $e) {
                $feedErrors .= ' Exception thrown in ' . __FILE__ . ' - message [' . $e->getMessage() . ']';
                $this->logger->error($feedErrors);

                $status = Constants::FEEDSTATUS_F;
            }

            // Record what happened.
            $feedInfo = $this->addFeedInfo($feedInfo, "num_egc_orders_generated", $i);
            $this->setFeedComplete($status, $feedErrors, $feedInfo);

            if(! empty($feedErrors)) {
                return false;
            } else {
                return true;
            }
        } else {
            echo (__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection.\n");
            return false;
        }
    }
}
