#!/bin/bash
#############################################
#
#	ProductCatalog.sh
#
# 	Invoke product catalog script.
#
#############################################

cd $(dirname "${BASH_SOURCE[0]}")

/usr/bin/php publish/productcatalog.php nofile
exit $?
