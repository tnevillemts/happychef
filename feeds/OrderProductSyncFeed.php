<?php
define('__ROOT__', dirname(__FILE__));

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/syncOrders.php');
require_once(__ROOT__ . '/syncProducts.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $inSyncOrdersObj = new syncOrders($magentoVersion, Constants::FEEDTYPE_SIMPLE);
    $inSyncProductsObj = new syncProducts($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if order or product sync is mutually excluded.
    $inSyncOrdersObj->checkCanRun();
    $inSyncProductsObj->checkCanRun();

    // Lock both feeds to prevent any chance of either from overlapping. Also sets status to pending.
    $inSyncOrdersObj->lock();
    $inSyncProductsObj->lock();

    $inSyncOrdersSuccess = $inSyncOrdersObj->process();
    if (! $inSyncOrdersSuccess) {
        throw new Exception($inSyncOrdersObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    $inSyncProductsSuccess = $inSyncProductsObj->process();
    if (! $inSyncProductsSuccess) {
        throw new Exception($inSyncProductsObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    $inSyncOrdersObj->unlock();
    $inSyncProductsObj->unlock();

    $logger->info("Success: " . __FILE__);
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::IN_ORDER_PRODUCT_SYNC_NAME);
try {
    process($logger);
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
