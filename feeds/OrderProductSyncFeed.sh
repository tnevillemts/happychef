#!/bin/bash
#############################################
#
#	OrderProductSyncFeed.sh
#
# 	Invoke feeder script.
#
#############################################

cd $(dirname "${BASH_SOURCE[0]}")

DATA_DIR='/var/www/html/happychef-beta/feeds/data';

echo "$(date) LOGX Start."
# Delete any sync file more than one day old.
find $DATA_DIR -type f -mtime +1 | xargs rm -Rf;

# First run the feeds if the indexers aren't running.
indexerRunning=`ps aux | grep -i "indexer.php" | grep -v "grep" | wc -l`
if [ $indexerRunning -eq 0 ]
then
    echo "$(date) LOGX result($indexerRunning) indexer is NOT running, clear to run OrderProductSyncFeed.php.";
    /usr/bin/php OrderProductSyncFeed.php
    result=$?
else
    echo "$(date) LOGX result($indexerRunning) indexer is running, cannot run feed OrderProductSyncFeed.php.";
fi

if [ -f /var/www/html/happychef-beta/feeds/data/emailText ]; then
    mail -s "Product Sync Info" drichter@ri.pn < /var/www/html/happychef-beta/feeds/data/emailText;
    mail -s "Product Sync Info" merchandising@ri.pn < /var/www/html/happychef-beta/feeds/data/emailText;
fi

echo "$(date) LOGX END."

exit $result
