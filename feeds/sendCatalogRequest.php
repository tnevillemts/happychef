<?php
require_once(__ROOT__.'/constants.php');
require_once(Constants::ROOTDIR . Constants::SITEROOT . "/app/Mage.php");
require_once(__ROOT__.'/feederBase.php');

class sendCatalogRequest extends feederBase
{
    public $startDate;
    public $endDate;

     ////////////////////////////////////////////////////////////////////////////////////////
    //
    // constructor
    //
    ////////////////////////////////////////////////////////////////////////////////////////
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion   = $magentoVersion;
        $this->feedType         = $feedType;
        $numDays = 5;
        $this->startDate = $_GET["startdate"] ? date('Y-m-d', strtotime($_GET["startdate"])) : date('Y-m-d', strtotime("-".($numDays-1)." day", time()));
        $this->endDate = $_GET["enddate"] ? date('Y-m-d', strtotime($_GET["enddate"])) : date('Y-m-d');

        $this->feedName         = Constants::OUT_CATALOG_REQUEST_NAME;
        $this->feedUrl          = Constants::PUBLISHURL."/".Constants::OUT_CATALOG_REQUEST;
        $this->logger           = new Logger ( Constants::LOGDIR."/".$this->feedName );

        parent::__construct();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // process()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function process()
    {
        $feedErrors     = '';
        $feedInfo       = '';
        $currentRow     = 0;
        $this->xml      = '';

        $this->setFeedStart();
        $this->setStatus(Constants::FEEDSTATUS_R);

        try {
                // Get all catalog requests
            $collection = Mage::getModel('catalogrequest/catalogrequest')->getCollection();
            $collection->addFieldToFilter('time_added', array(
                    'from' => $this->startDate . " 00:00:00",
                    'to' => $this->endDate . " 23:59:59",
                    'datetime' => true
            ));

            $domTree = new DOMDocument('1.0', 'UTF-8');
            $domTree->formatOutput = true;
            $domTree->preserveWhiteSpace = false;

            $xmlRoot = $domTree->createElement("requests");
            $xmlRoot = $domTree->appendChild($xmlRoot);

            $xmlRoot->setAttribute("number", $collection->count());
            $xmlRoot->setAttribute("startdate", date('n/j/Y', strtotime($this->startDate)));
            $xmlRoot->setAttribute("enddate", date('n/j/Y', strtotime($this->endDate)));
            $xmlRoot->setAttribute("ts", date('n/j/Y H:i:s A'));

            foreach($collection as $request) {
                $currentRequest = $domTree->createElement("request");
                $currentRequest = $xmlRoot->appendChild($currentRequest);

                $currentRequest->appendChild($domTree->createElement('address',     $this->prepForXml(htmlspecialchars($request->getAddress1()))));
                $currentRequest->appendChild($domTree->createElement('address2',    $this->prepForXml(htmlspecialchars($request->getAddress2()))));
                $currentRequest->appendChild($domTree->createElement('catreqid',    $this->prepForXml(htmlspecialchars($request->getCatalogrequestId()))));
                $currentRequest->appendChild($domTree->createElement('city',        $this->prepForXml(htmlspecialchars($request->getCity()))));
                $currentRequest->appendChild($domTree->createElement('comments',    $this->prepForXml(htmlspecialchars($request->getComments()))));
                $currentRequest->appendChild($domTree->createElement('company',     $this->prepForXml(htmlspecialchars($request->getCompany()))));
                $currentRequest->appendChild($domTree->createElement('country',     $this->prepForXml(htmlspecialchars($request->getCountry()))));
                $currentRequest->appendChild($domTree->createElement('dateadded',   $this->prepForXml(htmlspecialchars($request->getTimeAdded()))));
                $currentRequest->appendChild($domTree->createElement('email',       $this->prepForXml(htmlspecialchars($request->getEmail()))));
                $currentRequest->appendChild($domTree->createElement('fax',         $this->prepForXml(htmlspecialchars($request->getFax()))));
                $currentRequest->appendChild($domTree->createElement('name',        $this->prepForXml(htmlspecialchars($request->getFirstName()." ".$request->getLastName()))));
                $currentRequest->appendChild($domTree->createElement('phone',       $this->prepForXml(htmlspecialchars($request->getPhone()))));
                $currentRequest->appendChild($domTree->createElement('state',       $this->prepForXml(htmlspecialchars($request->getState()))));
                $currentRequest->appendChild($domTree->createElement('title',       $this->prepForXml(htmlspecialchars($request->getTitle()))));
                $currentRequest->appendChild($domTree->createElement('zip',         $this->prepForXml(htmlspecialchars($request->getZip()))));

                $currentRow++;
            }

            // Resulting XML
            $this->xml =  $domTree->saveXML();

            $status = Constants::FEEDSTATUS_S;
        } catch (Exception $e) {
            $feedErrors .= ' Exception thrown in ' . __FILE__ . ' - message [' . $e->getMessage() . ']';
            $this->logger->error($feedErrors);

            $status = Constants::FEEDSTATUS_F;
        }

        // Record what happened.
        $feedInfo = $this->addFeedInfo($feedInfo, "num_catalog_requests_generated", $currentRow);
        $this->setFeedComplete($status, $feedErrors, $feedInfo);

        if(! empty($feedErrors)) {
            return false;
        } else {
            return true;
        }
    }
}
