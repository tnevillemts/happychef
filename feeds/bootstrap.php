<?php
/**
 * This file configures global PHP settings and requires code common to all feeds.
 */

if (! defined('__ROOT__')) define('__ROOT__', dirname(__FILE__));

require_once(__ROOT__ . '/constants.php');
require_once(__ROOT__ . '/Logger.php');
require_once(Constants::ROOTDIR . "/app/Mage.php");

date_default_timezone_set(Constants::TIME_ZONE_STR);

// Output PHP fatal errors to stderr and, if set up, logger.
register_shutdown_function(function () {
    $error = error_get_last();
    if ($error['type'] !== E_ERROR) return;

    $message = "PHP Fatal Error: [{$error['message']} in {$error['file']}:{$error['line']}]";
    fwrite(STDERR, $message . "\n");

    global $logger;
    if (isset($logger) and $logger instanceof \Logger) $logger->fatal($message);
});

// Do not limit memory for feed processing.
ini_set('memory_limit', '-1');

// Limit PHP processing to three hours. Note that this excludes external calls like MySQL.
// By default CLI commands have no time limit, so goal is simply to set some finite ceiling.
set_time_limit(3 * 60 * 60);
