<?php
//////////////////////////////////////////////////////////////////////////////////
//
//	Constants.php
//
//	Program level control.
//
//////////////////////////////////////////////////////////////////////////////////

class Constants {
    ///////////////////////////////
    // DATABASE SETTINGS
    ///////////////////////////////

    const DATABASENAME  = '{{db_name}}';
    const DB_SERVER     = '{{db_host}}';
    const DB_USER       = '{{db_user}}';
    const DB_PASSWORD   = '{{db_pass}}';
    const DB_PORT       = '3306';

    ///////////////////////////////
    // FEED URLS
    ///////////////////////////////

    const SITEURL       = '{{app_url}}';
    const PUBLISHURL    = '{{app_feeds_out_base}}';
    const REMOTEURL     = '{{app_feeds_in_base}}';

    // Generates XML with new paper catalog requests to be consumed by HC
    const OUT_CATALOG_REQUEST = self::PUBLISHURL . '/catalogrequests';

    // Sends catalog to GoDataFeed for omnichannel sales (via FTP).
    const OUT_CATALOG_DATA = 'https://ws-web2.happychefuniforms.com/catalog';

    // Generates CSV with product catalog data to be consumed by HC
    const OUT_PRODUCT_DATA = self::PUBLISHURL . '/productcatalog';

    // Generates XML with open orders information to be consumed by HC
    const OUT_OPEN_ORDERS = self::PUBLISHURL . '/openorders';

    // Generates order to email gift card mappings.
    const OUT_EGC_ORDERS = self::PUBLISHURL . '/egcorders';

    // Generates XML with unprocessed orders information to be consumed by HC
    const OUT_UNPROCESSED_ORDERS = self::PUBLISHURL . '/unprocessedorders';

    // Order information and status updates
    // PROD: 'https://ws-core.happychef.com/hcweb/weborders-out.xml';
    const IN_ORDER_SYNC = self::REMOTEURL . '/weborders-out.xml';

    // Stock/inventory status for products
    // PROD: 'https://ws-core.happychef.com/hcweb/webproducts-out.xml';
    const IN_PRODUCT_SYNC = self::REMOTEURL . '/webproducts-out.xml';

    // Sync logo digitization status
    // PROD: 'https://ws-core.happychef.com/hcweb/weblogos-out.xml';
    const IN_LOGO_SYNC = self::REMOTEURL . '/weblogos-out.xml';

    ///////////////////////////////
    // FILE PATHS
    ///////////////////////////////

    const ROOTDIR = '{{app_documentroot}}';
    const SITEROOT = '';  // deprecated
    const LOGDIR = self::ROOTDIR . '/feeds/logs';
    const DATADIR = self::ROOTDIR . '/feeds/data';

    // PROD: '/mnt/drive2/';
    const PATH_TO_DIGITIZED_LOGOS = '{{app_logo_path}}';

    ///////////////////////////////
    // FEED NAMES
    ///////////////////////////////

    const IN_ORDER_PRODUCT_SYNC_NAME = 'IN_ORDER_PRODUCT_SYNC';
    const IN_ORDER_SYNC_NAME = 'IN_ORDER_SYNC';
    const IN_PRODUCT_SYNC_NAME = 'IN_PRODUCT_SYNC';
    const IN_LOGO_SYNC_NAME = 'IN_LOGO_SYNC';
    const OUT_CATALOG_REQUEST_NAME = 'OUT_CATALOG_REQUEST';
    const OUT_CATALOG_DATA_NAME = 'OUT_CATALOG_DATA';
    const OUT_PRODUCT_DATA_NAME = 'OUT_PRODUCT_DATA';
    const OUT_OPEN_ORDERS_NAME = 'OUT_OPEN_ORDERS';
    const OUT_EGC_ORDERS_NAME = 'OUT_EGC_ORDERS';
    const OUT_UNPROCESSED_ORDERS_NAME = 'OUT_UNPROCESSED_ORDERS';
    const OUT_LOGO_DATA_NAME = 'OUT_LOGO_DATA';

    ///////////////////////////////
    // FEED TYPES
    ///////////////////////////////
    const FEEDTYPE_SIMPLE = "SIMPLE";
    const FEEDTYPE_NESTED = "NESTED";
    const FEEDTYPE_NESTED_ENTRY = "NESTED_ENTRY";

    ///////////////////////////////
    // STATUS CODES
    ///////////////////////////////
    const FEEDSTATUS_S = 'SUCCESS';
    const FEEDSTATUS_F = 'FAILURE';
    const FEEDSTATUS_N = 'NEW';
    const FEEDSTATUS_P = 'PENDING';
    const FEEDSTATUS_R = 'RUNNING';
    const FEEDSTATUS_B = 'BLOCKED';

    ///////////////////////////////
    // FEED CONFIGURATION
    ///////////////////////////////

    // Web site Id and Store id.
    const WEBSITEID		= 1;
    const STOREID		= 1;

    // Size of input buffer.
    const BUFFERSIZE	= 16384;

    // Unique prefix.
    const UNIQUEPREFIX      = "RIPENUNIQUE-";

    // Export file suffix.
    const FILESUFFIX        = '.xml';

    // Tag for last data file name processed.
    const LAST_DATA_TAG     = 'last_data';

    // Tag for last data file name processed.
    const LAST_DATE_TAG     = 'last_date';

    // Time zone string used by magento.
    const TIME_ZONE_STR     = 'America/New_York';

    const SYNC_WITH_PENDING_PAYMENTS = false;
    const SYNC_WITH_UNPROCESSED = false;
    const MAX_NUMBER_TO_SYNC = 1000000;

    // GoDataFeed FTP connection info.
    const GODATAFEED_HOST = 'ftp.godatafeed.com';
    const GODATAFEED_PATH = 'shopping_feed.xml';
    const GODATAFEED_USER = '{{godatafeed_user}}';
    const GODATAFEED_PASS = '{{godatafeed_pass}}';
}
