<?php
require_once(__ROOT__.'/constants.php');
require_once(Constants::ROOTDIR . Constants::SITEROOT . "/app/Mage.php");
require_once(__ROOT__.'/feederBase.php');

class sendShoppingFeed extends feederBase
{
    /**
     * @param string $magentoVersion
     * @param string $feedType
     */
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion  = $magentoVersion;
        $this->feedType        = $feedType;
        $this->feedName        = Constants::OUT_CATALOG_DATA_NAME;
        $this->feedUrl         = Constants::PUBLISHURL."/".Constants::OUT_CATALOG_DATA;
        $this->logger          = new Logger ( Constants::LOGDIR."/".$this->feedName );

        parent::__construct();
    }

    /**
     * @param string $feedXml
     * @return void
     * @throws Exception
     */
    public function uploadCsv($feedXml)
    {
        // Simulate a file as source of FTP data to upload.
        $xmlStream = fopen('php://memory', 'r+');
        fwrite($xmlStream, $feedXml);
        rewind($xmlStream);

        // FTP file to godatafeed.com
        $ftpConnection = ftp_ssl_connect(Constants::GODATAFEED_HOST, 21);
        ftp_login($ftpConnection, Constants::GODATAFEED_USER, Constants::GODATAFEED_PASS);
        ftp_pasv($ftpConnection, true);
        $uploadSuccess = ftp_fput($ftpConnection, Constants::GODATAFEED_PATH, $xmlStream, FTP_ASCII);
        fclose($xmlStream);
        if (! $uploadSuccess) {
            throw new Exception("Error uploading XML file to " . Constants::GODATAFEED_HOST);
        }
    }

    /**
     * @return bool
     */
    public function process()
    {
        if (! $this->checkConnection() || ! $this->checkDatabase()) {
            $this->logger->fatal($this->feedName . ": " . __FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection.");
            return false;
        }

        $this->setFeedStart();
        $this->setStatus(Constants::FEEDSTATUS_R);

        $feedErrors = [];
        $feedInfo   = [];
        $finalStatus = Constants::FEEDSTATUS_F;

        try {
            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('type_id','simple')
                ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                // Lines like these can be uncommented in debugging to process specific test products:
                // ->addAttributeToFilter('sku', array('like' => '%505%'))
                // ->addAttributeToFilter('sku', array('eq' => '505-RUS/BLK-4XL'))
                // A line like this can be uncommented in debugging to process only the first 20 products:
                // ->setPage(1, 20);
                ;

            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);

            $domTree = new DOMDocument('1.0', 'UTF-8');
            $domTree->formatOutput = true;
            $domTree->preserveWhiteSpace = false;

            $xmlRoot = $domTree->createElement("GoDataFeed");
            $xmlRoot = $domTree->appendChild($xmlRoot);

            $fieldsEl = $xmlRoot->appendChild($domTree->createElement("Fields"));

            $fields = array(
                "Name",
                "Parentsku",
                "Sku",
                "Image",
                "Description",
                "Url",
                "Qty_in_stock",
                "Color",
                "Size",
                "Price",
                "Taxable",
                "Category",
                "Gender",
                "Expiration");

            foreach($fields as $field){
                $currentField = $domTree->createElement("Field");
                $currentField = $fieldsEl->appendChild($currentField);
                $currentField->setAttribute("name", $field);
            }

            $productsEl = $domTree->createElement("Products");
            $productsEl = $xmlRoot->appendChild($productsEl);

            $i = 0;
            $count = count($products);
            foreach($products as $product) {
                $i++;

                $p = Mage::getModel('catalog/product_type_configurable');
                $parentIds = $p->getParentIdsByChild($product->getId());
                $hasParents = count($parentIds) ? true : false;

                $parentSku = "";
                $url = "";
                $pid = $parentIds[0];

                if($hasParents) {
                    $parentProduct = Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter('entity_id', $pid)
                        ->addAttributeToSelect('sku')
                        ->addAttributeToSelect('old_sku')
                        ->addAttributeToSelect('short_description')
                        ->addAttributeToSelect('price')
                        ->getFirstItem();

                    if($parentProduct->getData('old_sku')){
                        $parentSku = $parentProduct->getData('old_sku');
                    } else {
                        $parentSku = $parentProduct->getData('sku');
                    }

                    $url = $parentProduct->getProductUrl(false);
                }

                if(!$hasParents){
                    $url = $product->getProductUrl(false);
                }
                $stockStatus = $product->getAttributeText('custom_stock_status');
                $stockQty = (int) Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
                $shipLeadTimedaysAmazon = (int) $product->getData('shipleadtimedaysamazon');
                $isDropShipItem = (($stockStatus == 'In Stock') && ($shipLeadTimedaysAmazon === 0) && ($stockQty === 0)) ? 'Yes' : 'No';
                $categories = $hasParents ? $parentProduct->getCategoryIds() : $product->getCategoryIds();
                $categoryNames = array();
                foreach($categories as $category_id) {
                    $_cat = Mage::getModel('catalog/category')->load($category_id) ;
                    $categoryNames[] = $_cat->getName();
                }

                $gender = "";
                $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', 'tailored');
                $v = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product->getId(), 'tailored', 1);
                if($v) {
                    $attributeValues = explode(",", $v);
                    foreach($attributeValues as $attributeValue){
                        $genderModel =  Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
                        $gender .= $genderModel->getSource()->getOptionText($attributeValue).", ";
                    }
                }
                $gender = substr($gender, 0, -2);

                $currentProduct = $domTree->createElement("Product");
                $currentProduct = $productsEl->appendChild($currentProduct);

                // Simple product price.
                if($hasParents) {
                    // First get the simple product upcharges for color and size.
                    $upcharge = 0;
                    $attributes = $parentProduct->getTypeInstance(true)->getConfigurableAttributes($parentProduct);
                    $pricesByAttributeValues = array();
                    $basePrice = $parentProduct->getFinalPrice();
                    foreach($attributes as $attribute) {
                        $aprices = $attribute->getPrices();
                        if(is_array($aprices)) {
                            foreach($aprices as $aprice) {
                                if($aprice['is_percent']) {
                                    // If the price is specified in percents.
                                    $pricesByAttributeValues[$aprice['value_index']] = (float)$aprice['pricing_value'] * $basePrice / 100;
                                } else {
                                    // If the price is absolute value.
                                    $pricesByAttributeValues[$aprice['value_index']] = (float)$aprice['pricing_value'];
                                }
                            }
                        }
                    }
                    foreach($attributes as $attribute) {
                        // Get the value for a specific attribute for a simple product.
                        $value = $product->getData($attribute->getProductAttribute()->getAttributeCode());
                        // Add the price adjustment to the total price of the simple product.
                        if(isset($pricesByAttributeValues[$value])) {
                                $upcharge += $pricesByAttributeValues[$value];
                        }
                    }
                    $price = number_format((float) $parentProduct->getPrice() + $upcharge, 2, '.', '');
                } else {
                    $price = number_format((float) $product->getPrice(), 2, '.', '');
                }

                $desc = $hasParents ? $parentProduct->getShortDescription() : $product->getShortDescription();
                $desc = preg_replace('/<[^>]*>/', '', $desc);
                $desc = str_replace('&', '', $desc);
                $taxable = $product->getTaxClassId() == 2 ? '1' : '0';

                $currentProduct->appendChild($domTree->createElement('Name'))->appendChild($domTree->createCDATASection($product->getName()));
                $currentProduct->appendChild($domTree->createElement('Parentsku'))->appendChild($domTree->createCDATASection($parentSku));
                $currentProduct->appendChild($domTree->createElement('Sku'))->appendChild($domTree->createCDATASection($product->getSku()));
                $currentProduct->appendChild($domTree->createElement('Image'))->appendChild($domTree->createCDATASection(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product'.$product->getImage()));
                $currentProduct->appendChild($domTree->createElement('Description'))->appendChild($domTree->createCDATASection($desc));
                $currentProduct->appendChild($domTree->createElement('Url'))->appendChild($domTree->createCDATASection($url));
                $currentProduct->appendChild($domTree->createElement('Qty_in_stock'))->appendChild($domTree->createCDATASection($stockQty));
                $currentProduct->appendChild($domTree->createElement('Color'))->appendChild($domTree->createCDATASection($product->getAttributeText("color")));
                $currentProduct->appendChild($domTree->createElement('Size'))->appendChild($domTree->createCDATASection($product->getAttributeText('size')));
                $currentProduct->appendChild($domTree->createElement('Price'))->appendChild($domTree->createCDATASection($price));
                $currentProduct->appendChild($domTree->createElement('Taxable'))->appendChild($domTree->createCDATASection($taxable));
                $currentProduct->appendChild($domTree->createElement('Category'))->appendChild($domTree->createCDATASection($categoryNames[0]));
                $currentProduct->appendChild($domTree->createElement('Gender'))->appendChild($domTree->createCDATASection($gender));
                $currentProduct->appendChild($domTree->createElement('Expiration'))->appendChild($domTree->createCDATASection(date('Y-m-d', strtotime('+30 days'))));
                $currentProduct->appendChild($domTree->createElement('Dropship'))->appendChild($domTree->createCDATASection($isDropShipItem));

                $this->logger->info("[{$i}/{$count}] Product [{$product->getSku()}] successfully added");
            }

            // Resulting XML
            $feedXml = $domTree->saveXML();
            if (! $feedXml) {
                throw new Exception("Not uploading data to GoDataFeed: No data to write.");
            }

            $this->uploadCsv($feedXml);

            $finalStatus = Constants::FEEDSTATUS_S;
            return true;
        } catch (Exception $e) {
            $message = "{$this->feedName}: Exception thrown in " . $e->getFile() . ":" . $e->getLine() . " - message [" . $e->getMessage() . "]";
            $feedErrors[] = $message;
            $this->logger->fatal($message);
            return false;
        } finally {
            // Record what happened.
            $feedInfo = $this->addFeedInfo($feedInfo, "num_products_generated", $i);
            $this->setFeedComplete($finalStatus, $feedErrors, $feedInfo);
        }
    }
}
