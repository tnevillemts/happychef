<?php
define('__ROOT__', dirname(__FILE__));

require_once(__ROOT__ . '/bootstrap.php');
require_once(__ROOT__ . '/syncLogos.php');

/**
 * @var Logger $logger
 * @return void
 * @throws Exception
 */
function process($logger)
{
    Mage::init();
    Mage::app()->setCurrentStore(Constants::STOREID);

    $logger->info("Start: " . __FILE__);

    $magentoVersion = Mage::getVersion();

    $inSyncLogosObj = new syncLogos($magentoVersion, Constants::FEEDTYPE_SIMPLE);

    // Check if mutually excluded.
    $inSyncLogosObj->checkCanRun();

    $inSyncLogosObj->lock();
    $inSyncLogosSuccess = $inSyncLogosObj->process();
    $inSyncLogosObj->unlock();

    if (! $inSyncLogosSuccess) {
        throw new Exception($inSyncLogosObj::PROCESS_FAILURE_MSG_PREFIX . 'At ' . __FILE__ . ':' . __LINE__);
    }

    $logger->info("Success: " . __FILE__);
}

$logger = new Logger(Constants::LOGDIR . "/" . Constants::IN_LOGO_SYNC_NAME);
try {
    process($logger);
} catch (MutexException $e) {
    $logger->warn("Exception: [" . $e->getMessage() . "]");
} catch (Exception $e) {
    $logger->fatal("Exception: [" . $e->getMessage() . "]");
    exit(1);
}
