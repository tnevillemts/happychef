<?php
require_once(__ROOT__ . '/constants.php');
require_once(Constants::ROOTDIR . Constants::SITEROOT . "/app/Mage.php");
require_once(__ROOT__ . '/feederBase.php');

class sendUnprocessedOrders extends feederBase
{
    public $xml;

    const SHIPPING_CODES = [
        "Standard" => "STD",
        "3 Day" => "SEL",
        "3 Business Day Air" => "SEL",
        "2 Day" => "PRI",
        "2 Business Day Air" => "PRI",
        "Overnight" => "EXP",
        "Next Business Day Air" => "EXP",
        "Free Standard Shipping" => "STD"
    ];

    ////////////////////////////////////////////////////////////////////////////////////////
    //
    // constructor
    //
    ////////////////////////////////////////////////////////////////////////////////////////
    public function __construct($magentoVersion, $feedType)
    {
        $this->magentoVersion = $magentoVersion;
        $this->feedType = $feedType;
        $this->feedName = Constants::OUT_UNPROCESSED_ORDERS_NAME;
        $this->feedUrl = Constants::PUBLISHURL . "/" . Constants::OUT_UNPROCESSED_ORDERS;
        $this->logger = new Logger (Constants::LOGDIR . "/" . $this->feedName);
        parent::__construct();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // process()
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function process()
    {
        date_default_timezone_set('EST');

        if ($this->checkConnection() && $this->checkDatabase()) {

            $feedErrors = '';
            $feedInfo = '';
            $this->setFeedStart();
            $this->setStatus(Constants::FEEDSTATUS_R);

            try {
                $orders = Mage::getModel('sales/order')->getCollection()
                    ->addAttributeToFilter('status', array('in' => array("unprocessed", "magento-new")))
                    ->addAttributeToFilter('confirmation', array('null' => true))//->addAttributeToFilter('increment_id', array('in' => array('103830852', '103830847')))
                ;

                $domTree = new DOMDocument('1.0', 'UTF-8');
                $domTree->formatOutput = true;
                $domTree->preserveWhiteSpace = false;

                $xmlRoot = $domTree->createElement("orders");
                $xmlRoot = $domTree->appendChild($xmlRoot);
                $xmlRoot->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                $xmlRoot->setAttribute("xsi:noNamespaceSchemaLocation", Constants::PUBLISHURL . "/schema/orders.xsd");

                $i = 0;

                foreach ($orders as $order) {
                    $i++;
                    $discounttaxable = 0;
                    $discountnontaxable = 0;

                    if ($order->getBillingAddress())
                        $billingData = $order->getBillingAddress()->getData();
                    else
                        $billingData = array();

                    if ($order->getShippingAddress())
                        $shippingData = $order->getShippingAddress()->getData();
                    else
                        $shippingData = $billingData;

                    $billingStreet = explode("\n", $billingData["street"]);
                    $billingData["telephone"] = preg_replace('/\D/', '', $billingData["telephone"]);

                    $shippingStreet = explode("\n", $shippingData["street"]);
                    $shippingData["telephone"] = preg_replace('/\D/', '', $shippingData["telephone"]);

                    $shippingMethod = explode(' - ', $order->getShippingDescription(), 2)[1];
                    $shippingCustomQuote = (strpos(strtolower($shippingMethod), "quote") !== false);
                    $shippingMethod =  trim(str_replace(Mage::helper('bettercheckout')::CUSTOM_QUOTE_SUFFIX, '', $shippingMethod));
                    $shippingMethodCode = isset(self::SHIPPING_CODES[$shippingMethod]) ? self::SHIPPING_CODES[$shippingMethod] : "EMAIL";

                    $shippingRegionModel = Mage::getModel('directory/region')->load($shippingData["region_id"]);
                    $shippingState = $shippingRegionModel ? $shippingRegionModel->getCode() : "";

                    $billingRegionModel = Mage::getModel('directory/region')->load($billingData["region_id"]);
                    $billingState = $billingRegionModel ? $billingRegionModel->getCode() : "";

                    $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

                    $paymentData = $order->getPayment()->getData();
                    $poNumber = $paymentData["po_number"];
                    $tokens = explode("-", $poNumber);  // Ex. value: 134171286-126454226-288879-0
                    // customer_token - payment_token -approval_code - shipment_token

                    $giftcards = Mage::getModel('aw_giftcard/history')->getCollection();
                    $giftcards->addFieldToFilter('additional_info', array('like' => array("%" . $order->getIncrementId() . "%")));
                    $giftCardsTotal = 0;
                    if ($giftcards) {
                        foreach ($giftcards as $giftcard) {
                            $giftCardsTotal += abs($giftcard->getBalanceDelta());
                        }
                    }

                    $ordertotal = $order->getGrandTotal();

                    $currentOrder = $domTree->createElement("order");
                    $currentOrder = $xmlRoot->appendChild($currentOrder);

                    $currentOrder->appendChild($domTree->createElement('externalorderno', htmlspecialchars($order->getIncrementId())));
                    $currentOrder->appendChild($domTree->createElement('internalorderno', ''));

                    $oStoreId = $order->getStoreId();
                    $oWebsiteId = Mage::getModel('core/store')->load($oStoreId)->getWebsiteId();
                    $currentOrder->appendChild($domTree->createElement('externalstoreid', $oWebsiteId . "-" . $oStoreId));

                    $currentOrder->appendChild($domTree->createElement('ordertype', 'W'));
                    $currentOrder->appendChild($domTree->createElement('sourceorderno', ''));
                    $currentOrder->appendChild($domTree->createElement('processbatchid', ''));
                    $currentOrder->appendChild($domTree->createElement('orderdate', htmlspecialchars(date('Y-m-d', strtotime($order->getCreatedAt())))));
                    $currentOrder->appendChild($domTree->createElement('summaryorderstatus', 0));
                    $currentOrder->appendChild($domTree->createElement('detailorderstatus', ''));

                    $billtocust = $currentOrder->appendChild($domTree->createElement('billtocust'));

                    if ($order->getCustomerId()) {
                        $billingExternalCustId = $order->getCustomerId() . ($billingData["customer_address_id"] ? $billingData["customer_address_id"] : $billingData["entity_id"]);
                        $shippingExternalCustId = $order->getCustomerId() . ($shippingData["customer_address_id"] ? $shippingData["customer_address_id"] : $shippingData["entity_id"]);
                    } else {
                        $billingExternalCustId = $shippingExternalCustId = "";
                    }

                    $billtocust->appendChild($domTree->createElement('externalcustid', htmlspecialchars($billingExternalCustId)));
                    $billtocust->appendChild($domTree->createElement('externalcustid-previous', htmlspecialchars($customer->getOldCustomerId())));

                    $billtocust->appendChild($domTree->createElement('internalcustid', htmlspecialchars($customer->getMomcustid())));
                    $billtocust->appendChild($domTree->createElement('corpcustid', htmlspecialchars($customer->getCorpCustomerId())));
                    $billtocust->appendChild($domTree->createElement('lastname', htmlspecialchars($billingData["lastname"])));
                    $billtocust->appendChild($domTree->createElement('firstname', htmlspecialchars($billingData["firstname"])));
                    $billtocust->appendChild($domTree->createElement('title', htmlspecialchars($customer->getJobtitle())));
                    $billtocust->appendChild($domTree->createElement('company', htmlspecialchars($billingData["company"])));
                    $billtocust->appendChild($domTree->createElement('address1', htmlspecialchars($billingStreet[0])));
                    $billtocust->appendChild($domTree->createElement('address2', $billingStreet[1] != $billingStreet[0] ? htmlspecialchars($billingStreet[1]) : ""));
                    $billtocust->appendChild($domTree->createElement('address3', htmlspecialchars($billingStreet[3])));

                    $billtocust->appendChild($domTree->createElement('city', htmlspecialchars($billingData["city"])));
                    $billtocust->appendChild($domTree->createElement('state', htmlspecialchars($billingState)));
                    $billtocust->appendChild($domTree->createElement('postalcode', htmlspecialchars($billingData["postcode"])));
                    $billtocust->appendChild($domTree->createElement('countrycode', htmlspecialchars($billingData["country_id"])));
                    $billtocust->appendChild($domTree->createElement('phone', htmlspecialchars($billingData["telephone"])));
                    $billtocust->appendChild($domTree->createElement('phoneext', ''));
                    $billtocust->appendChild($domTree->createElement('fax', htmlspecialchars($billingData["fax"])));
                    $billtocust->appendChild($domTree->createElement('email', htmlspecialchars($billingData["email"])));

                    $shiptocust = $currentOrder->appendChild($domTree->createElement('shiptocust'));
                    $shiptocust->appendChild($domTree->createElement('externalcustid', htmlspecialchars($shippingExternalCustId)));
                    $shiptocust->appendChild($domTree->createElement('externalcustid-previous', htmlspecialchars($customer->getOldCustomerId())));
                    $shiptocust->appendChild($domTree->createElement('internalcustid', ''));
                    $shiptocust->appendChild($domTree->createElement('lastname', htmlspecialchars($shippingData["lastname"])));
                    $shiptocust->appendChild($domTree->createElement('firstname', htmlspecialchars($shippingData["firstname"])));
                    $shiptocust->appendChild($domTree->createElement('title', htmlspecialchars($shippingData["jobtitle"])));
                    $shiptocust->appendChild($domTree->createElement('company', htmlspecialchars($shippingData["company"])));
                    $shiptocust->appendChild($domTree->createElement('address1', htmlspecialchars($shippingStreet[0])));
                    $shiptocust->appendChild($domTree->createElement('address2', $shippingStreet[1] != $shippingStreet[0] ? htmlspecialchars($shippingStreet[1]) : ""));
                    $shiptocust->appendChild($domTree->createElement('address3', htmlspecialchars($shippingStreet[2])));
                    $shiptocust->appendChild($domTree->createElement('city', htmlspecialchars($shippingData["city"])));
                    $shiptocust->appendChild($domTree->createElement('state', htmlspecialchars($shippingState)));
                    $shiptocust->appendChild($domTree->createElement('postalcode', htmlspecialchars($shippingData["postcode"])));
                    $shiptocust->appendChild($domTree->createElement('countrycode', htmlspecialchars($shippingData["country_id"])));
                    $shiptocust->appendChild($domTree->createElement('phone', htmlspecialchars($shippingData["telephone"])));
                    $shiptocust->appendChild($domTree->createElement('phoneext', ''));
                    $shiptocust->appendChild($domTree->createElement('fax', htmlspecialchars($shippingData["fax"])));
                    $shiptocust->appendChild($domTree->createElement('email', htmlspecialchars($shippingData["email"])));

                    $currentOrder->appendChild($domTree->createElement('shipmethod', htmlspecialchars($shippingMethodCode)));
                    $currentOrder->appendChild($domTree->createElement('subtotal', htmlspecialchars($order->getSubtotal())));
                    $currentOrder->appendChild($domTree->createElement('shiptotal', htmlspecialchars($order->getShippingAmount())));
                    $currentOrder->appendChild($domTree->createElement('salestaxtotal', htmlspecialchars($order->getTaxAmount())));
                    $currentOrder->appendChild($domTree->createElement('ordertotal', htmlspecialchars($ordertotal)));
                    $currentOrder->appendChild($domTree->createElement('orderinstructions', htmlspecialchars($order->getCustomerNote())));
                    $currentOrder->appendChild($domTree->createElement('customeraccountused', $order->getCustomerId() ? "True" : "False"));
                    $currentOrder->appendChild($domTree->createElement('customshippingquoteneeded', $shippingCustomQuote ? "True" : "False"));

                    $paymethods = $currentOrder->appendChild($domTree->createElement('paymethods'));

                    $ccExpM = (strlen($paymentData["cc_exp_month"]) == 1) ? "0" . $paymentData["cc_exp_month"] : $paymentData["cc_exp_month"];
                    $ccExpY = (strlen($paymentData["cc_exp_year"]) == 4) ? substr($paymentData["cc_exp_year"], 2, 2) : $paymentData["cc_exp_year"];
                    $ccType = array('AE' => 'AM', 'VI' => 'VI', 'MC' => 'MC', 'DI' => 'DI');
                    $ccexpdate = (!$ccExpM || !$ccExpY) ? "" : $ccExpM . "/" . $ccExpY;

                    // Record magento payment method (if any)
                    if ($paymentData["method"] != "free") {
                        $paymethod = $paymethods->appendChild($domTree->createElement('paymethod'));

                        // drichter
                        // For authorizecim
                        //$paymethod->appendChild($domTree->createElement('paytype', $paymentData["method"] == "authorizecimsoap" ? "CC" : htmlspecialchars($paymentData["method"])));
                        // For authorizenetcim
                        $paymethod->appendChild($domTree->createElement('paytype', $paymentData["method"] == "authnetcim" ? "CC" : htmlspecialchars($paymentData["method"])));

                        $paymethod->appendChild($domTree->createElement('payamount', htmlspecialchars($order->getGrandTotal())));
                        $paymethod->appendChild($domTree->createElement('cctype', htmlspecialchars($ccType[$paymentData["cc_type"]])));
                        $paymethod->appendChild($domTree->createElement('ccholder', htmlspecialchars(ucwords(strtolower($billingData["firstname"] . " " . $billingData["lastname"])))));
                        $paymethod->appendChild($domTree->createElement('ccno', htmlspecialchars($paymentData["cc_last4"])));
                        $paymethod->appendChild($domTree->createElement('ccexpdate', htmlspecialchars($ccexpdate)));
                        $paymethod->appendChild($domTree->createElement('ccseccode', ""));
                        $paymethod->appendChild($domTree->createElement('gcno', ""));

                        // drichter
                        // For authorizecim
                        //$paymethod->appendChild($domTree->createElement('ccrcode', 		1)); // 1 for all approved transactions.
                        //$paymethod->appendChild($domTree->createElement('ccapproval', 	$tokens[2]));
                        // For authorizenetcim
                        $paymethod->appendChild($domTree->createElement('ccrcode', "")); // There are no transactions now.
                        $paymethod->appendChild($domTree->createElement('ccapproval', ""));

                        $paymethod->appendChild($domTree->createElement('ccavs', htmlspecialchars($paymentData["cc_avs_status"])));
                        $paymethod->appendChild($domTree->createElement('ccantransid', htmlspecialchars($paymentData["cc_trans_id"])));
                        $paymethod->appendChild($domTree->createElement('ccauthamt', htmlspecialchars($paymentData["amount_ordered"])));
                        $paymethod->appendChild($domTree->createElement('ccauthtime', htmlspecialchars(str_replace(" ", "T", $order->getCreatedAt()))));
                        $paymethod->appendChild($domTree->createElement('cccusttoken', htmlspecialchars($tokens[0])));
                        $paymethod->appendChild($domTree->createElement('ccpaytoken', htmlspecialchars($tokens[1])));
                        $paymethod->appendChild($domTree->createElement('ccloginid', ""));
                    }

                    $giftcards = Mage::getModel('aw_giftcard/history')->getCollection();
                    $giftcards->addFieldToFilter('additional_info', array('like' => array("%" . $order->getIncrementId() . "%")));
                    $giftcards->addFieldToFilter('action', array('eq' => 3));

                    // Record gift card payment method (if any)
                    if ($giftcards) {

                        foreach ($giftcards as $giftcard) {

                            $giftCardModel = Mage::getModel('aw_giftcard/giftcard')->load($giftcard->getGiftcardId());
                            $giftCardNumber = $giftCardModel->getCode();

                            $paymethod = $paymethods->appendChild($domTree->createElement('paymethod'));
                            $paymethod->appendChild($domTree->createElement('paytype', "GC"));
                            $paymethod->appendChild($domTree->createElement('payamount', htmlspecialchars(abs($giftcard->getBalanceDelta()))));
                            $paymethod->appendChild($domTree->createElement('cctype', ""));
                            $paymethod->appendChild($domTree->createElement('ccholder', ""));
                            $paymethod->appendChild($domTree->createElement('ccno', ""));
                            $paymethod->appendChild($domTree->createElement('ccexpdate', ""));
                            $paymethod->appendChild($domTree->createElement('ccseccode', ""));
                            $paymethod->appendChild($domTree->createElement('gcno', htmlspecialchars($giftCardNumber)));
                            $paymethod->appendChild($domTree->createElement('ccrcode', ""));
                            $paymethod->appendChild($domTree->createElement('ccapproval', ""));
                            $paymethod->appendChild($domTree->createElement('ccavs', ""));
                            $paymethod->appendChild($domTree->createElement('ccantransid', ""));
                            $paymethod->appendChild($domTree->createElement('ccauthamt', ""));
                            $paymethod->appendChild($domTree->createElement('ccauthtime', htmlspecialchars("1/1/1900")));
                            $paymethod->appendChild($domTree->createElement('cccusttoken', ""));
                            $paymethod->appendChild($domTree->createElement('ccpaytoken', ""));
                            $paymethod->appendChild($domTree->createElement('ccloginid', ""));
                        }
                    }

                    $lineitems = $currentOrder->appendChild($domTree->createElement('lineitems'));

                    $orderItems = $order->getAllVisibleItems();

                    foreach ($orderItems as $orderItem) {

                        if ($orderItem->getTaxAmount() > 0) {
                            $discounttaxable += $orderItem->getBaseDiscountAmount();
                        } else {
                            $discountnontaxable += $orderItem->getBaseDiscountAmount();
                        }

                        $totalItemEmbroideryPrice = 0;

                        if ($orderItem->getHasChildren()) {
                            $children = array();
                            foreach ($orderItem->getChildrenItems() as $orderItemSimple) {
                                $children[] = $orderItemSimple;
                            }
                            // Always assumes that configurable product has only one generation of children
                            $productId = $children[0]->getProductId();
                            $orderItemSimple = $children[0];
                            $externalSku = "{$orderItem->getProductId()}-{$productId}";
                        } else {
                            $productId = $orderItem->getProductId();
                            $orderItemSimple = $orderItem;
                            $externalSku = "0-{$productId}";
                        }

                        $item = $lineitems->appendChild($domTree->createElement('item'));

                        $arr = explode("-", $orderItemSimple->getSku());
                        $newSku = "";
                        foreach ($arr as $el) {
                            if (strpos($el, "_") !== false) {
                                break;
                            } else {
                                $newSku .= $el . "-";
                            }
                        }
                        $newSku = substr($newSku, 0, -1);

                        if (in_array($newSku, array('mailgc', 'emailgc'))) {
                            $newSku = "GIFTCERT-" . (int)$orderItem->getPrice();
                        }

                        $storeId = Mage::app()->getStore()->getStoreId();

                        $oldSku = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'old_sku', $storeId);
                        $productOldSku = ($oldSku) ? $oldSku : $newSku;

                        $item->appendChild($domTree->createElement('externallineitemid', htmlspecialchars($orderItem->getItemId())));
                        $item->appendChild($domTree->createElement('internallineitemid', ''));
                        $item->appendChild($domTree->createElement('externalsku', htmlspecialchars($externalSku)));
                        $item->appendChild($domTree->createElement('internalsku', htmlspecialchars($productOldSku)));
                        $item->appendChild($domTree->createElement('itemstatus', ''));
                        $item->appendChild($domTree->createElement('orderqty', (int)$orderItem->getQtyOrdered()));
                        $item->appendChild($domTree->createElement('unitprice-configured', htmlspecialchars($orderItem->getPrice())));

                        // Jim: "This field is not significant for new order imports, you can leave it blank."
                        $item->appendChild($domTree->createElement('estshipdate', ''));

                        $item->appendChild($domTree->createElement('ordertime-stockstatus', htmlspecialchars($orderItemSimple->getStockStatus())));
                        $item->appendChild($domTree->createElement('ordertime-stockmessage', htmlspecialchars($orderItemSimple->getStatusUponOrdering())));

                        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                        $inventoryQty = $stockItem ? $stockItem->getQty() : "";
                        $item->appendChild($domTree->createElement('ordertime-qtyavailable', (int)$inventoryQty));

                        $item->appendChild($domTree->createElement('ordertime-shipleadtime', htmlspecialchars($orderItemSimple->getShipLeadTime())));

                        // mag_catalog_product_option: PK option_id
                        // mag_custom_options_option_description: option_id -> description
                        // mag_sales_flat_quote_item_option: item_id -> chosen option -> value
                        // mag_catalog_product_option_type_value: option_id -> value
                        // mag_custom_options_relation -> relations between option_id and group_id

                        $options = $orderItem->getProductOptions();

                        $embBuyRequest = $options["info_buyRequest"];
                        $customInstructions = "";

                        $gcCode = $options['aw_gc_created_codes'][0];
                        $gcDesign = $options['giftcard_design'];

                        // Retrieve embroidery instructions.
                        foreach ($embBuyRequest as $k => $v) {
                            if (strpos($k, "embroideryInstructions") !== false) {
                                $customInstructions = $v;
                                break;
                            }

                            if (strpos($k, "aw_gc_recipient_first_name") !== false) {
                                $gcFirstName = $v;
                            }
                            if (strpos($k, "aw_gc_recipient_last_name") !== false) {
                                $gcLastName = $v;
                            }
                            if (strpos($k, "aw_gc_recipient_email") !== false) {
                                $gcRecipientEmail = $v;
                            }
                            if (strpos($k, "aw_gc_sender_name") !== false) {
                                $gcSenderName = $v;
                            }
                            if (strpos($k, "aw_gc_message") !== false) {
                                $gcMessage = $v;
                            }
                            if (strpos($k, "aw_gc_recipient_address") !== false) {
                                $gcRecipientAddress = $v;
                            }
                            if (strpos($k, "aw_gc_recipient_address2") !== false) {
                                $gcRecipientAddress2 = $v;
                            }
                            if (strpos($k, "aw_gc_recipient_city") !== false) {
                                $gcRecipientCity = $v;
                            }
                            if (strpos($k, "aw_gc_recipient_state") !== false) {
                                $gcRecipientState = $v;
                            }
                            if (strpos($k, "aw_gc_recipient_zipcode") !== false) {
                                $gcRecipientZip = $v;
                            }
                        }

                        if ($orderItemSimple->getSku() == 'mailgc') {
                            $customInstructions = "Gift Card Number: {$gcCode}. Mail gift card to: {$gcFirstName} {$gcLastName}, {$gcRecipientAddress} {$gcRecipientAddress2}, {$gcRecipientCity} {$gcRecipientState}{$gcRecipientZip}. From: {$gcSenderName}. Message: {$gcMessage}. Design: {$gcDesign}";
                        }

                        if ($orderItemSimple->getSku() == 'emailgc') {
                            $customInstructions = "Gift Card Number: {$gcCode}. Gift card will be automatically emailed to: {$gcRecipientEmail}. From: {$gcSenderName}. Message: {$gcMessage}";
                        }

                        $embroideryObject = json_decode($options["info_buyRequest"]["product-embroidery-options"], true);
                        $types = $embroideryObject[$orderItem->getProductId()]["types"];

                        if (count($types)) {
                            $customInstructions = $embroideryObject[$orderItem->getProductId()]["instructions"];
                        }

                        $item->appendChild($domTree->createElement('customitem', count($types) ? "True" : "False"));
                        $item->appendChild($domTree->createElement('custominstructions', htmlspecialchars($customInstructions)));
                        $newcustomspecs = $item->appendChild($domTree->createElement('customspecs'));

                        foreach ($types as $typeId => $type) {
                            $typeModel = Mage::getModel('embroidery/type')->load($typeId);

                            $locations = $type["locations"];
                            foreach ($locations as $locationId => $location) {

                                $locationModel = Mage::getModel('embroidery/location')->load($locationId);

                                $selectedOptions = $location["selectedOption"];
                                if (count($selectedOptions)) {

                                    $customunitprice = 0;
                                    $emblocationcode = "";
                                    $textline = array();
                                    if ($type["name"] == 'text') {
                                        $i = 0;
                                        foreach ($selectedOptions as $selectedOption) {
                                            $textline[$i]["color"] = $selectedOption["textColorValue"];

                                            if ($textline[$i]["color"] == "Silver Gray") {
                                                $textline[$i]["color"] = "Silver Grey";
                                            }

                                            $textline[$i]["style"] = $selectedOption["textStyleValue"];
                                            $textline[$i]["text"] = $selectedOption["textLine"];
                                            $customunitprice += $selectedOption["price"];
                                            $i++;
                                        }
                                        $emblocationcode = $locationModel ? $locationModel->getCode() : "";
                                    }

                                    $flag = "";
                                    $flaglocationcode = "";
                                    if ($type["name"] == 'flags') {
                                        $flag = $selectedOptions[0]["name"];
                                        $flaglocationcode = $locationModel ? $locationModel->getCode() : "";
                                        $customunitprice = $selectedOptions[0]["price"];
                                    }

                                    $ribbon = "";
                                    $ribbonlocationcode = "";
                                    if ($type["name"] == 'ribbons') {
                                        $ribbon = $selectedOptions[0]["name"];
                                        if (strpos($ribbon, "Green") !== false) {
                                            $ribbon = str_replace("Green", "Grn", $ribbon);
                                        }

                                        $ribbonlocationcode = $locationModel ? $locationModel->getCode() : "";
                                        $customunitprice = $selectedOptions[0]["price"];
                                    }

                                    $logo = $logolocationcode = $logoimage = $logosku = $logocolor = $externallogoid = "";
                                    if ($type["name"] == 'logo') {
                                        $logo = $selectedOptions[0]["name"];
                                        $logolocationcode = $locationModel ? $locationModel->getCode() : "";
                                        $logoimage = $selectedOptions[0]["image"];
                                        $logosku = "LOGO";

                                        if ($selectedOptions[0]["id"]) {
                                            $externallogoid = $selectedOptions[0]["id"];
                                            $logoModel = Mage::getModel('embroidery/logo')->load($externallogoid);
                                            if ($logoModel) {
                                                $logosku = $logoModel->getSku();
                                            }
                                        } else { // for newly uploaded logo get record ID
                                            $logoModel = Mage::getModel('embroidery/logo')->getCollection()
                                                ->addFieldToFilter('logo', str_replace(Mage::getBaseUrl('media'), '', $selectedOptions[0]["image"]))
                                                ->getLastItem();
                                            if ($logoModel) {
                                                $externallogoid = $logoModel->getId();
                                            }
                                        }
                                        $customunitprice = $selectedOptions[0]["price"];
                                    }

                                    if ($type["name"] == 'acfLogos') {
                                        $logo = $selectedOptions[0]["name"];
                                        $logolocationcode = $locationModel ? $locationModel->getCode() : "";
                                        $logoimage = "";
                                        $logosku = Mage::getModel('embroidery/option')->load($selectedOptions[0]["id"])->getNameOld();

                                        $product = Mage::getModel('catalog/product')->load($orderItem->getProductId());
                                        $categoryIds = $product->getCategoryIds();
                                        // For hats, use different SKU for ACF logos
                                        if (in_array(18, $categoryIds)) {
                                            $logosku = "LOGO-ACFHAT";
                                        }

                                        $customunitprice = $selectedOptions[0]["price"];
                                    }

                                    if ($type["name"] == 'artwork') {
                                        $logo = $selectedOptions[0]["name"];
                                        $logolocationcode = $locationModel ? $locationModel->getCode() : "";
                                        $logoimage = "";
                                        $logosku = Mage::getModel('embroidery/option')->load($selectedOptions[0]["id"])->getNameOld();
                                        $logocolor = $selectedOptions[0]["artworkColorValue"];
                                        $logocolor = str_replace("Gray", "Grey", $logocolor);
                                        $customunitprice = $selectedOptions[0]["price"];
                                    }

                                    if ($type["name"] == 'patches') {
                                        $patchsku = Mage::getModel('embroidery/option')->load($selectedOptions[0]["id"])->getNameOld();
                                        $patchlocation = $locationModel ? $locationModel->getCode() : "";
                                        $customunitprice = $selectedOptions[0]["price"];
                                    }


                                    $locationName = $location["name"];
                                    $typeName = $typeModel ? $typeModel->getNameOld() : $type["name"];
                                    $totalItemEmbroideryPrice += $customunitprice;

                                    $newcustomspec = $newcustomspecs->appendChild($domTree->createElement('customspec'));
                                    $newcustomspec->appendChild($domTree->createElement('customtype', htmlspecialchars($typeName)));
                                    $newcustomspec->appendChild($domTree->createElement('emblocation', htmlspecialchars($emblocationcode)));
                                    $newcustomspec->appendChild($domTree->createElement('embline1color', htmlspecialchars($textline[0]["color"])));
                                    $newcustomspec->appendChild($domTree->createElement('embline2color', htmlspecialchars($textline[1]["color"])));
                                    $newcustomspec->appendChild($domTree->createElement('embline3color', htmlspecialchars($textline[2]["color"])));
                                    $newcustomspec->appendChild($domTree->createElement('embline1font', htmlspecialchars($textline[0]["style"])));
                                    $newcustomspec->appendChild($domTree->createElement('embline2font', htmlspecialchars($textline[1]["style"])));
                                    $newcustomspec->appendChild($domTree->createElement('embline3font', htmlspecialchars($textline[2]["style"])));
                                    $newcustomspec->appendChild($domTree->createElement('embline1', htmlspecialchars(str_replace('&', '&amp;', $textline[0]["text"]))));
                                    $newcustomspec->appendChild($domTree->createElement('embline2', htmlspecialchars(str_replace('&', '&amp;', $textline[1]["text"]))));
                                    $newcustomspec->appendChild($domTree->createElement('embline3', htmlspecialchars(str_replace('&', '&amp;', $textline[2]["text"]))));
                                    $newcustomspec->appendChild($domTree->createElement('flag', htmlspecialchars(ucwords(strtolower($flag)))));
                                    $newcustomspec->appendChild($domTree->createElement('flaglocation', htmlspecialchars($flaglocationcode)));
                                    $newcustomspec->appendChild($domTree->createElement('ribbon', htmlspecialchars(ucwords(strtolower($ribbon)))));
                                    $newcustomspec->appendChild($domTree->createElement('ribbonlocation', htmlspecialchars($ribbonlocationcode)));

                                    $newcustomspec->appendChild($domTree->createElement('patchsku', $patchsku));
                                    $newcustomspec->appendChild($domTree->createElement('patchlocation', htmlspecialchars($patchlocation)));

                                    $newcustomspec->appendChild($domTree->createElement('logosku', $logosku));
                                    $newcustomspec->appendChild($domTree->createElement('logoname', htmlspecialchars($logo)));
                                    $newcustomspec->appendChild($domTree->createElement('logolocation', htmlspecialchars($logolocationcode)));
                                    $newcustomspec->appendChild($domTree->createElement('logoimageurl', $logoimage));
                                    $newcustomspec->appendChild($domTree->createElement('externallogoid', $externallogoid));
                                    $newcustomspec->appendChild($domTree->createElement('logocolor', $logocolor));

                                    $newcustomspec->appendChild($domTree->createElement('location', htmlspecialchars($locationName)));
                                    $newcustomspec->appendChild($domTree->createElement('customprice', htmlspecialchars($customunitprice)));
                                }
                            }
                        }
                        $item->appendChild($domTree->createElement('unitprice-base', ($orderItem->getPrice() - $totalItemEmbroideryPrice)));
                    }

                    $currentOrder->appendChild($domTree->createElement('discounttotal', htmlspecialchars($order->getDiscountAmount())));
                    $currentOrder->appendChild($domTree->createElement('discounttaxable', $discounttaxable > 0 ? htmlspecialchars("-" . $discounttaxable) : htmlspecialchars($discounttaxable)));
                    $currentOrder->appendChild($domTree->createElement('discountnontaxable', htmlspecialchars($discountnontaxable > 0 ? "-" . $discountnontaxable : $discountnontaxable)));
                }

                // Resulting XML
                $this->xml = $domTree->saveXML();

                $status = Constants::FEEDSTATUS_S;
            } catch (Exception $e) {
                $feedErrors .= ' Exception thrown in ' . __FILE__ . ' - message [' . $e->getMessage() . ']';
                $this->logger->error($feedErrors);

                $status = Constants::FEEDSTATUS_F;
            }

            // Record what happened.
            $feedInfo = $this->addFeedInfo($feedInfo, "num_unprocessed_orders_generated", $i);
            $this->setFeedComplete($status, $feedErrors, $feedInfo);

            if (!empty($feedErrors)) {
                return false;
            } else {
                return true;
            }
        } else {
            echo(__FILE__ . ", line " . __LINE__ . ", function " . __FUNCTION__ . ", Error: No mysql connection.\n");
            return false;
        }
    }
}
