<cfmailpart type="text">
Hi,

We noticed that you did not complete your order, and we wanted to do everything we can to help.

To complete your order in two minutes or less, just finish the secure checkout process:

http://www.happychefuniforms.com/adtrack/?Page=Cart&AdTrack=#AdTrack#&EmailID=#EmailID#

If you have any questions about our products or services, please contact us using one of the following methods:

(800) 347-0288
info@happychefuniforms.com
http://www.happychefuniforms.com

Please let us know if there is any other way we can help.

Thanks,

Happy Chef Customer Service

------------------------------------------------------------
Periodically, HappyChefUniforms.com sends email about specials and other information that may be of interest to you. If you prefer not to receive these promotional emails, you may unsubscribe here:
http://www.happychefuniforms.com/MyAccount/profile_email.cfm?AdTrack=#AdTrack#&EmailID=#EmailID#&Email=#Email#

Or if you prefer to unsubscribe by regular mail, please send a letter that includes your name and email address to:

The Happy Chef, Inc.
22 Park Place
Butler, NJ 0740
(800) 347-0288

This message was sent to the following e-mail address: #Email#

Copyright &copy; <cfoutput>#DateFormat(now(),'yyyy')#</cfoutput> The Happy Chef, Inc. 22 Park Place, Butler, New Jersey 07405. All rights reserved.
</cfmailpart>

<cfmailpart type="html">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Happy Chef Uniforms</title>
<style>
div {font-family: Lucida Grande, Arial, Helvetica, Geneva, Verdana, sans-serif; text-align:left;}
.SeeAll {text-decoration:none}
.SeeAll:hover {text-decoration:underline}
</style>
</head>

<body bgcolor="#b3effe" style="margin:0;padding:0;">						

<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#b3effe" align="center">
  <tr>
    <td bgcolor="#b3effe" style="padding-top:10px; padding-bottom:25px;" align="center">

	<table width="650" border="0" cellpadding="0" cellspacing="0" align="center" style="margin-bottom:5px;">
		<tr><td bgcolor="#b3effe"><div style="font-size:11px; color:#073f94; padding-left:8px;"><a href="http://www.happychefuniforms.com/adtrack/?CatID=5&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Aprons</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=2&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Chef Coats</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=1&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Chef Pants</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=8&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Footwear</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=35&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Headwear</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=10&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Knives & Cases</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=18&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Server Shirts</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=39&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Womens</a></div></td><td bgcolor="#b3effe" align="right"><div style="font-size:11px; color:#333; padding-right:8px;">(800) 347-0288</div></td>
		</tr>
	<tr>
		<td colspan="2"><a href="http://www.happychefuniforms.com/adtrack/?Page=Cart&AdTrack=#AdTrack#&EmailID=#EmailID#"><img src="http://www.happychefuniforms.com/email/abandoned/happy_chef_uniforms.jpg" alt="Secure Checkout" width="650" height="127" border="0" style="display:block;"></a></td>
	</tr>
	<tr>
		<td colspan="2" bgcolor="#FFFFFF">
      <div style="padding:0px 10px 10px 10px; font-size:12px; background:url(http://www.happychefuniforms.com/email/abandoned/how_can_we_help.jpg); background-repeat:no-repeat;">
      Hi,<br><br>
      
      We noticed that you did not complete your order, and we wanted to do everything we can to help.<br><br>
      
      To complete your order in two minutes or less, just finish the <a href="http://www.happychefuniforms.com/adtrack/?Page=Cart&AdTrack=#AdTrack#&EmailID=#EmailID#">secure checkout</a> process.<br><br>
      
      If you have any questions about our products or services, please contact us using one of the following methods:<br><br>
      
      Toll Free - (800) 347-0288<br>
      E-mail - <a href="mailto:info@happychefuniforms.com">info@happychefuniforms.com</a><br>
      WWW - <a href="http://www.happychefuniforms.com/adtrack/?Page=Homepage&AdTrack=#AdTrack#&EmailID=#EmailID#">http://www.happychefuniforms.com</a><br><br>
      
      Please let us know if there is any other way we can help.<br><br>
      
      Thanks,<br><br>
      
      Happy Chef Customer Service
      </div>
    </td>
	</tr>
	<tr>
		<td colspan="2"><img src="http://www.happychefuniforms.com/email/abandoned/footer.jpg" width="650" height="13" alt="" style="display:block;"></td>
	</tr>
</table>

	<table width="640" cellpadding="0" cellspacing="0" align="center">
		<tr><td align="left"><div style="color:#333; font-size:10px; padding-top:10px; text-align:justify;"><div style="font-size:14px; font-weight:bold;">Need assistance? Call toll free! (800) 347-0288</div><br />We don't want you to miss out on any of the Happy Chef specials you requested to receive by e-mail. To ensure future mailings are delivered to your inbox, please add <span style="color: #073f94">HappyChefUniforms@mail82.subscribermail.com</span> to your safe sender list, or mark this e-mail as 'Not Spam'.<br /><br />This e-mail was sent to you by <a href="http://www.happychefuniforms.com/adtrack/?Page=Homepage&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Happy Chef Uniforms</a> because you have previously made a purchase from Happy Chef Uniforms.<br /><br /><a href="http://app.subscribermail.com/send_friend.cfm?template=%_tempid%&mailid=%_mailid%" style="color:#073f94;">Forward</a> this e-mail to a friend.<br /><br />Custom embroidery available. Contact customer service to help you determine appropriate placement of your logo and costs.<br /><br />Extraordinary service is the hallmark of The Happy Chef. Our standard of excellence begins with the great people in our customer service department who are trained to be knowledgeable in our product lines. This translates to our ability to offer "what you need, when you need it", on the web. If you need help selecting a product, call us at (800) 347-0288 from 9 AM to 5:30 PM Eastern U.S. time, Monday through Friday. We will be HAPPY to assist you.<br /><br />Copyright &copy; <cfoutput>#DateFormat(now(),'yyyy')#</cfoutput> The Happy Chef, Inc. 22 Park Place, Butler, New Jersey 07405. All rights reserved. <a href="http://www.happychefuniforms.com/adtrack/?Page=Privacy&AdTrack=MM20090701&EmailID=%_EmailID%" style="color:#073f94;">Privacy Policy</a><br /><br />The HAPPY CHEF&reg; LOGO is a registered trademark of the Happy Chef, Inc.</div></td>
		</tr>
	</table>
	
		</td>
	</tr>
</table>

</body>
</html>
</cfmailpart>