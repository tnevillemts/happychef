<cfsetting requesttimeout="1800">

<cfparam name="SourceID" default="">
<cfparam name="CampaignID" default="">
<cfparam name="AdgroupID" default="">
<cfparam name="AdID" default="">
<cfparam name="Headline" default="RECOMMENDED FOR YOU">
<cfparam name="P" default="24,109,4880">
<cfparam name="C" default="Black">


<cfquery name="GetProduct" datasource="#dsn#" cachedwithin="#createTimeSpan(0,0,10,0)#" maxrows="1">
    SELECT P.*, R.*, C.*, R.New AS NewColor
    FROM StoreProducts P, SwatchRollover R, StoreCategories C, ProductCats PC
    WHERE R.ProductID=P.ID
    AND P.ID = '#ProductID#'
    AND R.Color = '#Color#'
    AND C.CatID = PC.CatID
    AND C.Active = '1'
    AND PC.ProductID = P.ID
    ORDER BY C.CatID
</cfquery>

<cfif GetProduct.Special EQ '1'>
<cfquery name="GetSpecialPrice" datasource="#dsn#" cachedwithin="#createTimeSpan(0,0,10,0)#" maxrows="3">
	SELECT *
	FROM ProductOptions
	WHERE ProductID = '#ProductID#'
	ORDER BY Min_Quantity
</cfquery>
</cfif>

<cfquery name="GetAlsoBought" datasource="#dsn#" cachedwithin="#createTimeSpan(0,0,10,0)#" maxrows="3">
	SELECT P.Title, P.Special, P.New, P.PriceFeature, P.Price, P.Special_Off, P.Description, P.Feature1, P.Feature2, P.Model, A.Also AS ThisID, A.Quantity
	FROM StoreAlsoBought A, StoreProducts P
	WHERE A.ProductID = '#ProductID#'
	AND P.ID = A.Also
	ORDER BY A.Quantity DESC
</cfquery>


<cfquery name="GetAlsoBought" datasource="#dsn#" cachedwithin="#createTimeSpan(0,0,10,0)#">
	SELECT P.Title, P.Special, P.New, P.PriceFeature, P.Price, P.Special_Off, P.Description, P.Feature1, P.Feature2, P.Model, P.ID AS ThisID
	FROM StoreProducts P
	WHERE ID = '0'
    <cfloop index="i" list="#P#">
	OR ID = '#i#'
    </cfloop>
    ORDER BY P.ID
</cfquery>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" >
<title><cfoutput query="GetProduct">#Title#</cfoutput></title>
<style type="text/css">
/* iPad Text Smoother */

div, p, a, li, td {
	-webkit-text-size-adjust: none;
}
.ReadMsgBody {
	width: 100%;
	background-color: #ffffff;
}
.ExternalClass {
	width: 100%;
	background-color: #ffffff;
}
body {
	width: 100%;
	background-color: #ffffff;
	margin: 0;
	padding: 0;
	-webkit-font-smoothing: antialiased;
	mso-margin-top-alt: 0px;
	mso-margin-bottom-alt: 0px;
	mso-padding-alt: 0px 0px 0px 0px;
}
html {
	width: 100%;
	background-color: #f2f2f2 !important;
}
img {
	border: 0px;
	outline: none;
	text-decoration: none;
	display: block;
}
table {
	border-collapse: collapse;
}
 @media only screen and (max-width: 640px) {
body {
	width: auto!important;
}
/* Box Wrap */
.BoxWrap, table[id=TopLinks], img[id=FeatureImage], img[class=Banner] {
	width: 440px !important;
}
table[id=TopLinks_Left] {
	width: 100% !important;
}
img[id=FeatureImage] {
	opacity: 0.0;
!important;
}
/* Show/Hide  */
.ResponsiveHide {
	display: none !important;
}
/* When activated, this will hide the top navigation on mobile screens */
  /* tr[id=preheader] {display: none !important;} */
  
img[class=Responsive] {
	width: 440px!important;
	height: auto !important;
}
table[id=OneThirdBox] {
	margin-bottom: 20px;
}
table[id=Logo] {
	text-align: center !important;
}
/* Center product copy on mobile */
td[class=mobilecenter] {
	text-align: center !important;
}
table[class=mobilecenter] {
	margin: auto !important;
}
img[class=mobilecenter] {
	margin: auto !important;
}
}
 @media only screen and (max-width: 479px) {
body {
	width: auto!important;
}
/* Box Wrap */
.BoxWrap, table[id=TopLinks], img[id=FeatureImage], img[class=Banner], img[class=SocialBanner], table[class=RespoShowSocial] {
	width: 280px !important;
}
/* Hide  */
.ResponsiveHide {
	display: none !important;
}
img[class=Responsive] {
	width: 280px !important;
	height: auto !important;
}
}
</style>
<link href='http://fonts.googleapis.com/css?family=Sanchez' rel='stylesheet' type='text/css'>
</head>
<body style="width: 100%;background-color: #fff;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;mso-margin-top-alt: 0px;mso-margin-bottom-alt: 0px;mso-padding-alt: 0px 0px 0px 0px;">
<span style="font-size: 0px; color: #f2f2f2;">*|IF:FNAME|*Hi *|TITLE:FNAME|*, we*|ELSE:|* We*|END:IF|* recommend these products for you.</span>
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" align="center">
  <tbody>
    <tr>
      <td valign="middle" height="0" style="height: 0px; overflow: hidden; font-size: 0px;"><span style="font-size: 0px; overflow: hidden; color: #ffffff;">Recommended for You</span></td>
    </tr>
    <!-- ********************************************************************************************************* -->
    <tr bgcolor="#6f7073">
      <td><!-- *** Top Links *** -->
        
        <table ID="TopLinks" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:auto;">
          <tr>
            <td>
       	<!-- *** Top Links *** -->
       	<table ID="TopLinks" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:auto;">
       	  <tr>
       	    <td><!-- Right -->
       	      <table ID="TopLinks_Right" border="0" cellspacing="0" cellpadding="0" align="right" style="margin: 2px 0 7px 0;">
       	        <tr>
       	          <td align="right" valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color:#666666; font-size:12px; padding: 5px 20px 0 0;">
                  <a href="*|FORWARD|*" style="color:#ffffff;text-decoration:none;">*|IF:FNAME|*Hi *|TITLE:FNAME|*, share*|ELSE:|* Share*|END:IF|* this email with a friend</a>
                  </td>
                  <td>
                  <a href="*|FORWARD|*" style="color:#ffffff;text-decoration:none;"><img src="http://www.happychefuniforms.com/email/template/HCShare.png" style="display:block;" border="0" alt="Forward to a friend"/></a>
                  </td>
   	            </tr>
   	          </table>
       	      <!-- /Right -->
            </td>
   	      </tr>
     	  </table>
          <!-- *** / Top Links*** -->
        </td>
          </tr>
        </table>
        
        <!-- *** / Top Links*** --></td>
    </tr>
    <tr> 
      
      <!-- Begin Module: Header -->
      <td valign="top" bgcolor="#ffffff" style="-webkit-text-size-adjust: none;"><table border="0" width="600" cellspacing="0" cellpadding="0" class="BoxWrap" align="center">
          <tr><td height="10"></td></tr>
          <tr>
            <td width="100%">
            
             <!--Left Container -->
             <table class="BoxWrap" align="left" width="75" border="0" cellpadding="0" cellspacing="0">
               <tr>
                  <td style="-webkit-text-size-adjust: none;">
                    <!-- N -->
                    <table class="BoxWrap ProductCopy" width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                              <td align="center">
                              	<cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email">
                                	<img src="http://www.happychefuniforms.com/email/template/logo.png" alt="Happy Chef" width="75" height="33" hspace="0" vspace="0" style="display:block;" border="0">
                                </a></cfoutput>
                              </td>
                            </tr>
                    </table>
                    <!-- / Product Copy -->
                  </td>
               </tr>
             </table>
             <!-- / Left Container -->
             
             
             <!--Middle Container -->
             <table class="BoxWrap" align="left" width="450" border="0" cellpadding="0" cellspacing="0">
               <tr>
                 <td style="-webkit-text-size-adjust: none;">
                    <!-- Product Copy -->
                    <table class="BoxWrap ProductCopy" width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                              <td align="center" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color:#666666; font-size:12px; padding-top: 15px;">
                              <cfoutput><span class="ResponsiveHide"><a href="http://www.happychefuniforms.com/adtrack/?CatID=54&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##666666;text-decoration:none;">##SMART</a> : </span><a href="http://www.happychefuniforms.com/adtrack/?CatID=5&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##666666;text-decoration:none;">Aprons</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=2&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##666666;text-decoration:none;">Coats</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=43&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##666666;text-decoration:none;">CookCool</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=35&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##666666;text-decoration:none;">Hats</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=1&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##666666;text-decoration:none;">Pants</a> : <a href="http://www.happychefuniforms.com/adtrack/?CatID=39&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##666666;text-decoration:none;">Women's</a></cfoutput>
                              </td>
                            </tr>
                    </table>
                    <!-- / Product Copy -->
                 </td>
               </tr>
             </table>
             <!-- / Middle Container -->
         
            </td>
          </tr>
		</table>
        </td>
    </tr>
    <!-- End Module: Header --> 
    <!-- ********************************************************************************************************* --> 
    
    <!-- ********************************************************************************************************* --> 
    <!-- Begin Module: Feature Banner -->
    <tr>
      <td bgcolor="#ffffff" style="-webkit-text-size-adjust: none;"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="clear:both;">
          <tbody>
            <tr>
              <td style="-webkit-text-size-adjust: none;"><table width="100%" align="center" cellpadding="0" cellspacing="0">
                  <tbody>
                    <!-- Begin Module: Header_Text -->
                    <tr>
                      <td valign="top" style="-webkit-text-size-adjust: none;"><table class="BoxWrap" width="600"  bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td width="100%" height="10" style="-webkit-text-size-adjust: none;"></td>
                            </tr>
                            
                            <!-- HEADER -->
                            <tr>
                              <td height="20" valign="top" align="center" style="font-family: 'Sanchez','Times New Roman',serif;color:#333333;text-decoration:none;font-size:29px;line-height:29px;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="font-family: 'Sanchez','Times New Roman',serif;color:##333333;text-decoration:none;font-size:29px;line-height:29px;">#Headline#</a></cfoutput></td>
                            <tr>
                              <td align="center" style="font-family: 'Lucida Grande',Arial,Helvetica,Verdana,sans-serif;color:#333333; font-size:20px;text-decoration:none;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="font-family: 'Sanchez','Times New Roman',serif;color:##333333;text-decoration:none;">Based On Previously Viewed #Category#</a></cfoutput></td>
                              <!-- SUBHEAD -->
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <!-- End Module: Feature Banner --> 
    <!-- ********************************************************************************************************* --> 
    
    <!-- ********************************************************************************************************* --> 
    <!-- Begin Module: BigProducts -->
    <tr>
      <td valign="top" style="-webkit-text-size-adjust: none;"><table width="600" class="BoxWrap" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" border="0">
          <tbody>
            <!-- _____________________________________________ -->
            <tr>
              <td style="-webkit-text-size-adjust: none;"><!-- ************************** --> 
                <!-- Table Spacer -->
                
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td width="100%" height="20" style="-webkit-text-size-adjust: none;"></td>
                    </tr>
                  </tbody>
                </table>
                <table class="Content_Color" bgcolor="#ffffff" width="100%" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="-webkit-text-size-adjust: none;"><!-- ************************** --> 
                        <!-- Image -->
                        
                        <table class="BoxWrap" width="600" align="center" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td class="RespoImage_OneToOneW" align="center" style="-webkit-text-size-adjust: none;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="border: none;-webkit-text-size-adjust: none;text-decoration: none;text-transform: capitalize;line-height: 30px;color: ##ffffff;"><img class="Responsive" width="400" src="http://www.happychefuniforms.com/store_images/rollover/#ProductID#/#Replace(Image,'_sm','')#" alt="" border="0" style="width: 450px;height: auto;display: block;border: 0px;outline: none;text-decoration: none;"></a></cfoutput></td>
                            </tr>
                          </tbody>
                        </table>
                        
                        <!-- ************************** --> 
                        <!-- Product Copy -->
                        
                        <table class="BoxWrap ProductCopy" width="500" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                              <td align="center" class="mobilecenter" style="font-family: 'Lucida Grande',Arial,Helvetica,Verdana,sans-serif;-webkit-text-size-adjust: none;font-size:13px; color: #ab0000;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="text-decoration:none;color: ##ab0000;"><cfif GetProduct.Special EQ '1'>- #GetProduct.Special_Off#% OFF Save #DollarFormat(GetSpecialPrice.Cost-Special_Price)# each-<cfelseif GetProduct.New EQ '1'>- NEW PRODUCT -<cfelseif GetProduct.NewColor EQ '1'>- NEW COLOR -</cfif></a></cfoutput></td>
                            </tr>
                          <tr>
                            <td height="10" style="-webkit-text-size-adjust: none;"></td>
                          </tr>
                          <tr>
                            <td align="center" class="mobilecenter" style="font-family: 'Lucida Grande',Arial,Helvetica,Verdana,sans-serif;color: #333; font-size:18px; line-height:18px;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Title#<br>
                                <span style="font-size:13px;">###Model#</span></a></cfoutput></td>
                          </tr>
                          <tr>
                            <td height="10" style="-webkit-text-size-adjust: none;"></td>
                          </tr>
                          <tr>
                            <td class="mobilecenter" align="center" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: #333;font-size:12px;line-height:14px; -webkit-text-size-adjust: none;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Description#</a></cfoutput> 
                              <!-- Bullet Point Copy Structure -->
                              
                              <table  class="mobilecenter" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td colspan="2" height="10"></td>
                                </tr>
                                <tr>
                                  <td width="10" valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: #333;font-size:12px;">&#8226;</td>
                                  <td valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: #333;font-size:12px;line-height:14px; -webkit-text-size-adjust: none;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Feature1#</a></cfoutput></td>
                                </tr>
                              </table>
                              
                              <!-- Bullet Point Copy Structure --> 
                              <!-- Bullet Point Copy Structure -->
                              
                              <table  class="mobilecenter" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td colspan="2" height="10"></td>
                                </tr>
                                <tr>
                                  <td width="10" valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: #333;font-size:12px;">&#8226;</td>
                                  <td valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: #333;font-size:12px;line-height:14px; -webkit-text-size-adjust: none;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Feature2#</a></cfoutput></td>
                                </tr>
                              </table>
                              
                              <!-- Bullet Point Copy Structure --></td>
                          </tr>
                          <tr>
                            <td height="10" style="-webkit-text-size-adjust: none;"></td>
                          </tr>
                          <tr>
                            <td align="center" class="mobilecenter"  style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: #ab0000;font-size:12px;line-height:12px;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##ab0000;text-decoration:none;"><cfif Special EQ '1'>#Special_PriceFeature#<cfelse>#PriceFeature#</cfif> <span style="font-size:14px;"><cfif Special EQ '1'>#DollarFormat(Special_Price)#<cfelse>#DollarFormat(Price)#</cfif></span></a></cfoutput></td>
                          </tr>
                          <tr>
                            <td height="10" style="-webkit-text-size-adjust: none;"></td>
                          </tr>
                          <tr>
                            <td align="center"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ProductID#&Color=#Color#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" title="Shop Now" style="color: ##ab0000;text-decoration:none;"><img src="http://www.happychefuniforms.com/email/template/button.png" width="71" height="19" vspace="0" hspace="0" border="0" style="display:block;margin:0;" class="mobilecenter" alt="Shop Now"></a></cfoutput></td>
                          </tr>
                        </table>
                        
                        <!-- / Product Copy --></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <!-- End Module: BigProducts --> 
    <!-- ********************************************************************************************************* --> 
    
    <!-- ********************************************************************************************************* --> 
    <!-- Begin Module: Horizontal Seperation -->
    <tr>
      <td><table width="600" class="BoxWrap" align="center" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td width="100%" height="20" style="-webkit-text-size-adjust: none;"></td>
          </tr>
          <tr>
            <td width="100%" height="1" bgcolor="#dadada" style="-webkit-text-size-adjust: none;"></td>
          </tr>
        </table></td>
    </tr>
    <!-- End Module: Horizontal Seperation --> 
    <!-- ********************************************************************************************************* --> 
    
    <!-- ********************************************************************************************************* --> 
    <!-- Begin Module: Medium Products -->
    <tr>
      <td valign="top" style="-webkit-text-size-adjust: none;"><!-- Begin Module: Section Subhead Insert -->
        
        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="BoxWrap">
          <tr>
            <td width="100%" height="20" style="-webkit-text-size-adjust: none;"></td>
          </tr>
          <tr>
            <td align="left" style="font-family: 'Lucida Grande',Arial,Helvetica,Verdana,sans-serif;color:#545454; font-size:20px; font-weight: bold;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##545454;text-decoration:none;">You might also like...</a></cfoutput></td>
          </tr>
        </table>
        
        <!-- End Module: Section Subhead Insert -->
        
        <table width="600" class="BoxWrap" align="center" cellpadding="0" cellspacing="0" border="0">
          <tbody>
            <!-- _____________________________________________ --> 
            
<cfoutput query="GetAlsoBought">

<cfset SD = dateAdd("D",-60,now())>
<cfset ED = dateAdd("D",0,now())>

<cfparam name="StartDate" default="#DateFormat(SD,'M-D-YY')#">
<cfparam name="EndDate" default="#DateFormat(ED,'M-D-YY')#">

<cfquery name="GetMaxProduct" datasource="#dsn#" maxrows="1" cachedwithin="#createTimeSpan(0,0,0,0)#">
    SELECT   SUM(O.Quantity) AS TotalNum, SUM(O.Quantity*Cost) AS TotalSales, O.ProductID, R.Color, R.Image
    FROM StoreOrders O, SwatchRollover R
    WHERE O.ProductID = '#ThisID#'
    AND O.DateAdded BETWEEN '#StartDate# 12:00:00 AM' AND '#EndDate# 11:59:59 PM'
    AND O.SwatchID = R.SwatchID
    AND R.Color LIKE '%#C#%'
    GROUP BY O.ProductID, R.Color, R.Image
    ORDER BY TotalSales DESC, O.ProductID, R.Color
</cfquery>

<cfif GetMaxProduct.RecordCount NEQ '1'>
<cfquery name="GetMaxProduct" datasource="#dsn#" maxrows="1" cachedwithin="#createTimeSpan(0,0,0,0)#">
    SELECT   SUM(O.Quantity) AS TotalNum, SUM(O.Quantity*Cost) AS TotalSales, O.ProductID, R.Color, R.Image
    FROM StoreOrders O, SwatchRollover R
    WHERE O.ProductID = '#ThisID#'
    AND O.DateAdded BETWEEN '#StartDate# 12:00:00 AM' AND '#EndDate# 11:59:59 PM'
    AND O.SwatchID = R.SwatchID
    GROUP BY O.ProductID, R.Color, R.Image
    ORDER BY TotalSales DESC, O.ProductID, R.Color
</cfquery>
</cfif>
              <tr>
                <td style="-webkit-text-size-adjust: none;"><!-- ************************** --> 
                  <!-- Table Spacer -->
                  
                  <table width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td width="100%" height="20" style="-webkit-text-size-adjust: none;"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table class="Content_Color" bgcolor="##ffffff" width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td style="-webkit-text-size-adjust: none;"><!-- ************************** --> 
                          <!-- Image & Button -->
                          
                          <table class="BoxWrap" width="300" align="left" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td width="182" class="RespoImage_OneToOneW" style="-webkit-text-size-adjust: none;"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ThisID#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="border: none;-webkit-text-size-adjust: none;text-decoration: none;text-transform: capitalize;line-height: 30px;color: ##ffffff;"><img class="Responsive" width="300" src="http://www.happychefuniforms.com/store_images/rollover/#ThisID#/#Replace(GetMaxProduct.Image,'_sm','')#" alt="" border="0" style="width: 300px;height: auto;display: block;border: 0px;outline: none;text-decoration: none;"></a></td>
                              </tr>
                            </tbody>
                          </table>
                          
                          <!-- ************************** --> 
                          <!-- Product Copy -->
                          
                          <table class="BoxWrap ProductCopy" width="284" border="0" cellspacing="0" cellpadding="0" align="right">
                            <tr>
                              <td height="10" style="-webkit-text-size-adjust: none;"></td>
                            </tr>
                            <tr>
                              <td  class="mobilecenter" align="left" style="font-family: 'Lucida Grande',Arial,Helvetica,Verdana,sans-serif;color: ##333; font-size:18px; line-height:18px;"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ThisID#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Title#<br>
                                <span style="font-size:13px;">###Model#</span></a></td>
                            </tr>
                            <tr>
                              <td height="10" style="-webkit-text-size-adjust: none;"></td>
                            </tr>
                            <tr>
                              <td class="mobilecenter" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: ##333;font-size:12px;line-height:14px; -webkit-text-size-adjust: none;"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ThisID#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Description#</a> 
                                <!-- Bullet Point Copy Structure -->
                                
                                <table class="mobilecenter" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td colspan="2" height="10"></td>
                                  </tr>
                                  <tr>
                                    <td width="10" valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: ##333;font-size:12px;">&##8226;</td>
                                    <td valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: ##333;font-size:12px;line-height:14px; -webkit-text-size-adjust: none;"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ThisID#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Feature1#</a></td>
                                  </tr>
                                </table>
                                
                                <!-- Bullet Point Copy Structure --> 
                                <!-- Bullet Point Copy Structure -->
                                
                                <table class="mobilecenter" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td colspan="2" height="10"></td>
                                  </tr>
                                  <tr>
                                    <td width="10" valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: ##333;font-size:12px;">&##8226;</td>
                                    <td valign="top" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: ##333;font-size:12px;line-height:14px; -webkit-text-size-adjust: none;"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ThisID#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##333;text-decoration:none;">#Feature2#</a></td>
                                  </tr>
                                </table>
                                
                                <!-- Bullet Point Copy Structure --></td>
                            </tr>
                            <tr>
                              <td height="10" style="-webkit-text-size-adjust: none;"></td>
                            </tr>
                            <tr>
                              <td  class="mobilecenter" align="left" style="font-family: 'Lucida Grande','Lucida Sans','Lucida Sans Unicode',Arial,Helvetica,Verdana,sans-serif;color: ##ab0000;font-size:12px;line-height:12px;"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ThisID#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color: ##ab0000;text-decoration:none;">#PriceFeature# <span style="font-size:14px;">#DollarFormat(Price)#</span></a></td>
                            </tr>
                            <tr>
                              <td height="10" style="-webkit-text-size-adjust: none;"></td>
                            </tr>
                            <tr>
                              <td align="left"><a href="http://www.happychefuniforms.com/adtrack/?ProductID=#ThisID#&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" title="Shop Now" style="color: ##ab0000;text-decoration:none;"><img src="http://www.happychefuniforms.com/email/template/button.png" width="71" height="19" vspace="0" hspace="0" border="0" style="display:block;margin:0;" alt="Shop Now" class="mobilecenter"></a></td>
                            </tr>
                          </table>
                          
                          <!-- / Product Copy -->
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <!-- _____________________________________________ --> 
</cfoutput>
          </tbody>
        </table></td>
    </tr>
    <!-- End Module: Medium Products --> 
    <!-- ********************************************************************************************************* --> 
    
    <!-- ********************************************************************************************************* --> 
    <!-- Begin Module: Banner -->
    <tr>
      <td valign="top" style="-webkit-text-size-adjust: none;"><table class="BoxWrap" width="600" align="center" cellpadding="0" cellspacing="0">
          <tbody>
            <!-- _____________________________________________ -->
            <tr>
              <td style="-webkit-text-size-adjust: none;"><!-- ************************** --> 
                <!-- Banner -->
                
                <table class="BoxWrap" width="100%" align="right" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td width="100%" height="20" style="-webkit-text-size-adjust: none;"></td>
                    </tr>
                    <tr>
                      <td style="-webkit-text-size-adjust: none;"><table width="100%" bgcolor="#f3f3f3" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td style="-webkit-text-size-adjust: none;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?CustomPage=?Catalog&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email"><img class="Responsive" src="http://happychefuniforms.com/email/20140325_505_305/images/NewCatalog_Banner.jpg" alt="" width="600" hspace="0" vspace="0" style="display:block;" border="0"></a></cfoutput></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table>
                
                <!-- /Banner --> 
                <!-- ************************** --></td>
            </tr>
            <!-- _____________________________________________ -->
          </tbody>
        </table></td>
    </tr>
    <!-- End Module: Banner --> 
    <!-- ********************************************************************************************************* --> 
    
    <!-- ********************************************************************************************************* --> 
    <!-- Begin Module: Social -->
    <tr>
      <td valign="top" style="-webkit-text-size-adjust: none;"><table class="BoxWrap" width="600" align="center" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td width="100%" height="20" style="-webkit-text-size-adjust: none;"></td>
            </tr>
            <!-- _____________________________________________ -->
            <tr>
              <td align="center" bgcolor="#6f7073" style="font-family: 'Sanchez','Times New Roman',serif;color:#ffffff; font-size:17px; padding-top: 5px;"><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?facebook=find&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" style="color:##ffffff;text-decoration:none;">JOIN 200,000+ HAPPY CHEF FANS ONLINE!</a></cfoutput></td>
            </tr>
            <tr>
              <td bgcolor="#6f7073"><table width="163" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tr>
                    <td><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?facebook=find&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" title="Facebook"><img src="http://www.happychefuniforms.com/email/template/Social_Facebook.png" width="38" height="44" alt="Facebook" vspace="0" hspace="0" border="0" style="display:block;"></a></cfoutput></td>
                    <td><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?twitter=follow&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" title="Twitter"><img src="http://www.happychefuniforms.com/email/template/Social_Twitter.png" width="41" height="44" alt="Twitter" vspace="0" hspace="0" border="0" style="display:block;"></a></cfoutput></td>
                    <td><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?pintrest=&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" title="Pinterest"><img src="http://www.happychefuniforms.com/email/template/Social_Pinterest.png" width="39" height="44" alt="Twitter" vspace="0" hspace="0" border="0" style="display:block;"></a></cfoutput></td>
                    <td><cfoutput query="GetProduct"><a href="http://www.happychefuniforms.com/adtrack/?youtube=&SourceID=#SourceID#&CampaignID=#CampaignID#&AdGroupID=#AdGroupID#&AdID=#AdID#&utm_source=HC&utm_medium=email&utm_term=&utm_campaign=Dynamic%20Email" title="Youtube"><img src="http://www.happychefuniforms.com/email/template/Social_Youtube.png" width="49" height="44" alt="Youtube" vspace="0" hspace="0" border="0" style="display:block;"></a></cfoutput></td>
                  </tr>
                </table></td>
            </tr>
            <!-- _____________________________________________ -->
          </tbody>
        </table></td>
    </tr>
    <!-- End Module: Social--> 
    <!-- ********************************************************************************************************* --> 
    
    <!-- ********************************************************************************************************* --> 
    <!-- Begin Module: Footer -->
    <tr>
      <td valign="top" bgcolor="#ffffff" style="-webkit-text-size-adjust: none;"><table class="BoxWrap" width="600" align="center" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td style="-webkit-text-size-adjust: none;"><!--Footer-->
                
                <table border="0" cellspacing="0" cellpadding="0" align="center" id="footer">
                  <tr>
                    <td style="padding:20px 0px 10px 0px;"><div style="font-family: Geneva,Verdana,Arial,Helvetica,sans-serif;color:#999999; font-size:11px; line-height: 1.6em; color:#999999;">
                        <div style="font-size:14px; font-weight:bold;">Need assistance? Call toll free! <a href="tel:800-347-0288" style="color:#999999;text-decoration:none;">(800) 347-0288</a></div>
                        <p><br />
                          Happy Chef has proudly served culinary professionals for over 25 years. Our standard of excellence begins with the great people in our customer service department who are always happy to assist you. Customer service is available 9AM&ndash;5:30PM EST, Monday through Friday, at (800) 347-0288. For calls outside the United States, dial +1-973-492-2525. Or, if you want to send a message online, use our <a href="http://www.happychefuniforms.com/cs/contact.cfm" style="color: #999999;text-decoration:underline;">contact form</a> at any time. <br />
                          <br />
                          &copy;2014 The Happy Chef, Inc. 22 Park Place, Butler, New Jersey 07405. All rights reserved. Happy Chef, CookCool, Chefs Sharp, #SMART, and their respective logos are registered trademarks of The Happy Chef, Inc. and cannot be reproduced for commercial use without expressed permission. </p>
                      </div></td>
                  </tr>
                </table>
                
                <!--Footer--></td>
            <tr>
              <td height="20" style="-webkit-text-size-adjust: none;"></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <!-- End Module: Footer --> 
    <!-- ********************************************************************************************************* -->
  </tbody>
</table>
</body>
</html>