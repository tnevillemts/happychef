// This fxn allows all accordion toggles to be closed
// Comment out corresponding section in udesign/scripts/script.js
$(document).ready(function(){
$('.accordion-container').hide();
var rotationFwd = 90;
var rotationRev = 0;
$('.accordion-toggle').click(function(){
	
var clickId = $(this).attr('id');
if($('#' + clickId).hasClass('active')){
	
	$('.' + clickId).animate({'opacity':'0'}, 300, function(){
		var prevOldSrc = $('.' + clickId).attr('src');
		var prevNewSrc = prevOldSrc.replace(/02.png/, '01.png');
		$('.' + clickId).attr('src', prevNewSrc);
		$('.' + clickId).animate({'opacity':'1'}, 300);
		$("#" + clickId).removeClass('active');
	});
}
else {
	activeIdObj = $('#accord h3.active');
	var activeId = activeIdObj.attr('id');
	$('#accord').find('.' + activeId).animate({'opacity':'0'}, 300, function(){
		var prevOldSrc = $('#accord').find('.' + activeId).attr('src');
		var prevNewSrc = prevOldSrc.replace(/02.png/, '01.png');
		$('#accord').find('.' + activeId).attr('src', prevNewSrc);
		$('#accord').find('.' + activeId).animate({'opacity':'1'}, 300, function(){
			$('#' + activeId).removeClass('active');
		});
	});
	$('#accord').find('.' + clickId).animate({'opacity':'0'}, 300, function(){
		var curOldSrc = $('.' + clickId).attr('src');
		var curNewSrc = curOldSrc.replace(/01.png/, '02.png');
		$('.' + clickId).attr('src', curNewSrc);
		$('.' + clickId).animate({'opacity':'1'}, 300, function(){
			$("#" + clickId).addClass('active');
		});
	});
}

if( $(this).next().is(':hidden') ) {
	$('.accordion-toggle').next().slideUp();
	$(this).next().slideDown();
}
else {
	$('.accordion-toggle').next().slideUp();
}

return false; // Prevent the browser jump to the link anchor
});
});
jQuery(document).ready(function($){
$("[name='clientProjUpload']").on('change', function(){
var uploadText = $("[name='clientProjUpload']").val();
$('#browseField').val(uploadText);
});
});
function chgBkgd(el, url){
jQuery(el).css('background-image', 'url("'+url+'")');
};
