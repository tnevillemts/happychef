// This fxn allows all accordion toggles to be closed
// Comment out corresponding section in udesign/scripts/script.js
$(document).ready(function(){
$('.accordion-container').hide();
var rotationFwd = 90;
var rotationRev = 0;
$('.accordion-toggle').click(function(){
if( $(this).next().is(':hidden') ) {
	$('.accordion-toggle').next().slideUp();
	$(this).next().slideDown();
	//find the child div of the selected accordion toggle with class="calloutA-AccordArrow"
	//$('.calloutA-AccordArrow').css({'opacity' : '1'})
	
	// find the current 02 img and fade it out
	/*$('#accord').find("img[src*='02.png']").parent().animate({'opacity':'0'}, 1000, function(){
		var accSrc = $(this).children().attr('src');
		var newAccSrc = accSrc.replace(/02.png/, '01.png');
		var something = $('#accord').find("img[src*='02.png']");
		something.attr('src', newAccSrc);
	});
	*/
	

	// change the img src to 01 and fade it back in
	
	$(this).find('.calloutA-AccordArrow').stop().animate({'opacity' : '0'}, 1000, function(){
		var prevAccSrc = $(this).find('img').attr('src');
		var prevAccNewSrc = prevAccSrc.replace(/01.png/, '02.png');
		$(this).find('img').attr('src', prevAccNewSrc);
		$(this).stop().animate({'opacity' : '1'}, 1000);
		//$('#accord').children('.calloutA-AccordArrow[opacity="0"]').animate({'opacity':'1'}, 1000);
	});
}

else {
	$('.accordion-toggle').next().slideUp(); 
	$(this).find('.calloutA-AccordArrow').stop().animate({'opacity' : '0'}, 1000, function(){
		var prevAccSrc = $(this).find('img').attr('src');
		var prevAccNewSrc = prevAccSrc.replace(/02.png/, '01.png');
		$(this).find('img').attr('src', prevAccNewSrc);
		$(this).stop().animate({'opacity' : '1'}, 1000);
	});
}
return false; // Prevent the browser jump to the link anchor
});
});
jQuery(document).ready(function($){
$("[name='clientProjUpload']").on('change', function(){
var uploadText = $("[name='clientProjUpload']").val();
$('#browseField').val(uploadText);
});
});
function chgBkgd(el, url){
jQuery(el).css('background-image', 'url("'+url+'")');
};
