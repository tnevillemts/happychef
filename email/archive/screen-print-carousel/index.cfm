<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Integra Identity</title>
<link href="css/reset.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/master.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.qtip-1.0.0-rc3.min.js"></script>
<script type="text/javascript" src="js/cufon.js"></script>
<script type="text/javascript" src="js/global.js"></script>
<script type="text/javascript" src="js/Gotham_Light.font.js"></script>
<script type="text/javascript" src="js/jquery.cycle.lite.min.js"></script>
<script type="text/javascript">
    //<![CDATA[
 

//setup scrolling
$(function() {
	
	//add listeners
	$('.left_arrow a').click(scroll);
	$('.right_arrow a').click(scroll);
	$('#about_people .person').click(scroll);
	$('#about_people .person').hover(function() { $(this).addClass('over'); }, 
						             function() { $(this).removeClass('over'); });
	
});

//called on scroll click
function scroll(e) {
	
	//vars
	var w = window.innerWidth == 320 ? 320 : 822;
	var pages = $('#slide-container div.slide').length;
	if (!(scroll.pos >= 0)) scroll.pos = 0;
	var prev_pos = scroll.pos;
	
	//stop click
	e.preventDefault();

	//pick direction
	var dir = 0;
	if ($(this).parent().hasClass('right_arrow'))  {
		scroll.pos++;
		if (scroll.pos > pages - 1) scroll.pos = 0;
		dir = 1;
	} else if ($(this).parent().hasClass('left_arrow'))  {
		scroll.pos--;
		if (scroll.pos < 0) scroll.pos = pages-1;
		dir = -1;
	} else if ($(e.target).parents('#about_people').length) {
		scroll.pos = $('#about_people .person').index($(this));
		if (scroll.pos == prev_pos) return;
		dir = scroll.pos > prev_pos ? 1 : -1;
	}
	
	//update the active nav
	$('#about_people .person').removeClass('current');
	$('#about_people .person').eq(scroll.pos).addClass('current');
	
	//reorganize slides.  if the offsets change because of moving shit around, move slides to accomodate
	var x1 = $(".slide_"+prev_pos).position().left;
	if (dir == 1) $(".slide_"+scroll.pos).insertAfter(".slide_"+prev_pos);
	else if (dir == -1) $(".slide_"+scroll.pos).insertBefore(".slide_"+prev_pos);
	var x2 = $(".slide_"+prev_pos).position().left;
	$("#slide-container").animate({'left': '+='+(x1-x2)+'px'}, 0);
	
	//scroll
	move = (dir == 1 ? '-=' : '+=')+w+'px';
	$("#slide-container").animate({'left': move}, 500);
	
	//scroll
	//move = (-w*scroll.pos)+'px';
	//$("#slide-container").animate({'left': move}, 500);
	
}

Cufon.replace('#slide-container h2', {fontFamily: 'Gotham-Light'});

    //]]>
</script>
<script type="text/javascript">
    //<![CDATA[
 

//on dom, setup slideshow
$(function() {
	setupSlideshow('sacramento');
	setupSlideshow('seattle');
});

//common slideshow code
function setupSlideshow(city) {
	$('#'+city+' .photos').cycle({ 
		timeout : 3000,
		pause : 1,
		next : '#'+city+' .photos',
		speed :  500
	});
}

    //]]>
</script>
<style>
.copy {
	color: #999999;
	font-size:22px;
	line-height:30px;
	text-align:justify;
}
h2 {
	padding-bottom:10px;
}
#expertise_slides .slides {
	width: 822px;
	height: 580px;
	overflow: hidden;
	margin: 0 30px;
	float: left;
	position: relative;
}
#expertise_slides .slide .meet_your_team {
	width: 822px;
	margin-left: 0px;
	margin-top: 28px;
	text-align:justify
}
#expertise_slides .slide .meet_your_team h2 {
	margin-bottom: 20px;
	font-size:32px;
}
.slide p {
	padding: 0;
	font: 16px/24px Georgia, serif;
	color: #AAA;
}
#p_about .slide p {
	font: 13px/24px Georgia, serif;
	color: #AAA;
}
.slide h2 {
	font-size:24px;
}
/*------------------POPUPS------------------------*/
#fade {
	display: none;
	background: #000;
	position: fixed;
	left: 0;
	top: 0;
	z-index: 10;
	width: 100%;
	height: 100%;
	opacity: .80;
	z-index: 9999;
}
.popup_block {
	display: none;
	background: #fff;
	padding: 20px;
	border: 10px solid #333;
	float: left;
	font-size: 1.2em;
	position: fixed;
	top: 50%;
	left: 50%;
	z-index: 99999;
	-webkit-box-shadow: 0px 0px 20px #000;
	-moz-box-shadow: 0px 0px 20px #000;
	box-shadow: 0px 0px 20px #000;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}
img.btn_close {
	float: right;
	margin: -55px -55px 0 0;
}
.popup p {
	padding: 5px 10px;
	margin: 5px 0;
}
/*--Making IE6 Understand Fixed Positioning--*/
*html #fade {
	position: absolute;
}
*html .popup_block {
	position: absolute;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
						   		   
	//When you click on a link with class of poplight and the href starts with a # 
	$('a.poplight[href^=#]').click(function() {
		var popID = $(this).attr('rel'); //Get Popup Name
		var popURL = $(this).attr('href'); //Get Popup href to define size
				
		//Pull Query & Variables from href URL
		var query= popURL.split('?');
		var dim= query[1].split('&');
		var popWidth = dim[0].split('=')[1]; //Gets the first query string value

		//Fade in the Popup and add close button
		$('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="images/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>');
		
		//Define margin for center alignment (vertical + horizontal) - we add 80 to the height/width to accomodate for the padding + border width defined in the css
		var popMargTop = ($('#' + popID).height() + 80) / 2;
		var popMargLeft = ($('#' + popID).width() + 80) / 2;
		
		//Apply Margin to Popup
		$('#' + popID).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		//Fade in Background
		$('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
		$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer 
		
		return false;
	});
	
	
	//Close Popups and Fade Layer
	$('a.close, #fade').live('click', function() { //When clicking on the close or fade layer...
	  	$('#fade , .popup_block').fadeOut(function() {
			$('#fade, a.close').remove();  
	}); //fade them both out
		
		return false;
	});

	
});
</script>
</head>
<body>
<div id="wrapper">
  <div id="expertise_slides" class="clearfix">
    <div class="left_arrow"><a href="#">Back</a></div>
    <div class="slides">
      <div id="slide-container">
        <div id="popup1" class="popup_block"> <img src="1a.jpg" width="800" height="339" border="0" /> </div>
        <div id="popup2" class="popup_block"> <img src="2a.jpg" width="800" height="340" border="0" /> </div>
        <div id="popup3" class="popup_block"> <img src="3a.jpg" width="800" height="339" border="0" /> </div>
        <div id="popup4" class="popup_block"> <img src="4a.jpg" width="800" height="340" border="0" /> </div>
        <div id="popup5" class="popup_block"> <img src="5a.jpg" width="32" height="32" border="0" /> </div>
        <div class="slide slide_0 meet_your_team">
          <div class="meet_your_team">
            <h2>Integra Identity</h2>
            <br /><br /><br />
            <p style="font-style:italic; font-size:14px; line-height:18px;">Please note that business card examples are included for context only and are not intended to reflect final design. Colors shown are for presentation only and are open to further consideration during the logo selection process.</p>
            <br /><br /><br /><br />
            <p align="center"><img src="ri.png" width="118" height="50" /></p>
            <p align="center" style="font-size:11px; font-family:Verdana, Geneva, sans-serif; line-height:16px; padding-top:15px; color:#999;">117 Rockingham Row Princeton NJ 08540&nbsp;&nbsp;(609) 520-8820</p>
          </div>
        </div>
        <div class="slide slide_1">
          <div id="logo1" align="center" style="height:340px;"><a href="#?w=800" rel="popup1" class="poplight"><img src="1.jpg" width="800" height="311" border="0" /></a></div>
          <h2>Logo #1 <a href="#?w=1000" rel="popup1" class="poplight" style="font-size:14px; padding-left:25px;">Click logo to see business card</a></h2>
          <p>Nested in a bold red circle, this stylized "i" is an abstract representation of the digital circuitry Integra works with every day. The flowing feel of the letter alludes to the flexible, adaptable nature of the company, as well as to the flow of information between secure backup locations.</p>
        </div>
        <div class="slide slide_2">
          <div id="logo1" align="center" style="height:340px;"><a href="#?w=800" rel="popup2" class="poplight"><img src="2.jpg" width="800" height="357" border="0" /></a></div>
          <h2>Logo #2 <a href="#?w=1000" rel="popup2" class="poplight" style="font-size:14px; padding-left:25px;">Click logo to see business card</a></h2>
          <p>The image of a floating "i" and its corresponding shadow represent the ever-present backup provided by Integra. The organic design of the letters and the space between them conveys a sense of fluidity and speed. Finally, the strong, simple typography carries an air of reliability and professionalism.</p>
        </div>
        <div class="slide slide_3">
          <div id="logo1" align="center" style="height:340px;"><a href="#?w=800" rel="popup3" class="poplight"><img src="3.jpg" width="800" height="308" border="0" /></a></div>
          <h2>Logo #3 <a href="#?w=1000" rel="popup3" class="poplight" style="font-size:14px; padding-left:25px;">Click logo to see business card</a></h2>
          <p>The converging paths of blue and green elements illustrate system integration and redundant backup while also resembling the type of small business office a potential client might be using. The bright, welcoming colors exude a friendly persona, and the even, solid typography complete the clean-cut look.</p>
        </div>
        <div class="slide slide_4">
          <div id="logo1" align="center" style="height:340px;"><a href="#?w=800" rel="popup4" class="poplight"><img src="4.jpg" width="800" height="362" border="0" /></a></div>
          <h2>Logo #4 <a href="#?w=1000" rel="popup4" class="poplight" style="font-size:14px; padding-left:25px;">Click logo to see business card</a></h2>
          <p>Simple and bold, this logo represents not only the "i" in Integra, but also the reliable people who supply the company's services. Combined with a warm yellow glow, this design highlights friendliness, promptness, and personal attention. The circle itself could easily be used in a variety of media.</p>
        </div>
        <div class="slide slide_5">
          <h2 align="center" style="padding-top:140px; font-size:36px; color:#C60; padding-bottom:170px;">Thank You.</h2>
            <br />
            <p align="center"><img src="ri.png" width="118" height="50" /></p>
            <p align="center" style="font-size:11px; font-family:Verdana, Geneva, sans-serif; line-height:16px; padding-top:15px; color:#999;">117 Rockingham Row Princeton NJ 08540&nbsp;&nbsp;(609) 520-8820</p>
        </div>
      </div>
    </div>
    <div class="right_arrow"><a href="#">Forward</a></div>
  </div>
</div>
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>
