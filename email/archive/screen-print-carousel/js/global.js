function hideURLbar(){
    window.scrollTo(0,1);
}

//--TOOLTIPS-------------

//on dom, add avatar rollovers
$(function() {
	setTimeout(hideURLbar, 0);
	
	$('.tip_rollover').each(function() {
		$(this).qtip({
			content: getContent($(this).attr('alt')),
			position: {
				corner: {
					target: 'topMiddle',
					tooltip: 'bottomMiddle'
				},
				//target: 'mouse',
				adjust: {
					y: -1
				}
			},
			show: {
				when: 'mouseover',
				solo: false,
				delay: 0,
				effect: {
					length: 100
				}
			},
			hide: { 
				effect: {
					length: 200
				}
			},
			style: {
				tip: {
					corner: 'bottomMiddle',
					size : {
						x: 6,
						y: 6
					}
				},
				border: {
					width: 1,
					radius: 1
				},
				padding: 5,
				width: 'auto',
				textAlign: 'left',
				name: 'light'
			}
		});
	});
});

//split title and subtitle for rollovers
function getContent(txt) {
	var parts = txt.split(',');
	return '<p class="name">'+parts[0]+'</p><p class="title">'+parts[1]+'</p>';
}

//--OTHERS------

//make links for background image titles where a doesn't receieve focus
$(function() {
	var el = $('h2:has(a.manual), h1:has(a.manual)');
	el.css('cursor', 'pointer');
	el.click(function() {
		var url = $(this).find('a').attr('href');
		document.location = url;
	});
});


//link a thumbnail and the text title associated with it.  Like in work and news
$(function() {
	$('a.linked').hover(
		function() {
			if ($(this).find('img').length) {
				$(this).parents().next().find('a.linked').eq(0).css({color: '#333'});
				$(this).find('img').css({'border-color': '#00a7e5'});
			} else $(this).parents().prev().find('a.linked img').eq(0).css({'border-color': '#00a7e5'});
		},
		function() {
			if ($(this).find('img').length) {
				$(this).parents().next().find('a.linked').eq(0).css({color: ''});
				$(this).find('img').css({'border-color': ''});
			} else $(this).parents().prev().find('a.linked img').eq(0).css({'border-color': ''});
		}
	);
});

