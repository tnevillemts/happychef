$(document).ready(function() {
jQuery.organicTabs = function(el, options) {
var rotationRev = 0;
var rotationFwd = 90;
var base = this;
base.$el = jQuery(el);
base.$nav = base.$el.find(".nav");
base.init = function() {
base.options = jQuery.extend({},jQuery.organicTabs.defaultOptions, options);

// Accessible hiding fix
	jQuery(".hide").css({
		"position": "relative",
		"top": 0,
		"left": 0,
		"display": "none"
	});

base.$nav.delegate("li > a", "click", function() {
// Figure out current list via CSS class
var curList = base.$el.find("a.current").attr("href").substring(1);
/////////////////////////////////////////////////////
// Get ID of the arrow for the current list
var curArrow = base.$el.find("a.current div.calloutA-Arrow").attr("id");
/////////////////////////////////////////////////////
// List moving to
$newList = jQuery(this),
// Figure out ID of new list
listID = $newList.attr("href").substring(1),
/////////////////////////////////////////////////////
// Get ID of the arrow for the new list
arrowId = jQuery(this).find("div.calloutA-Arrow").attr("id");
/////////////////////////////////////////////////////
// Set outer wrapper height to (static) height of current inner list
$allListWrap = base.$el.find(".list-wrap"),
curListHeight = $allListWrap.height();
$allListWrap.height(curListHeight);


if ((listID != curList) && ( base.$el.find(":animated").length === 0)) {
	// Fade out current list
	base.$el.find("#"+curList).fadeOut(base.options.speed, function() {
	
	// Fade in new list on callback
	base.$el.find("#"+listID).fadeIn(base.options.speed);
	
	// Adjust outer wrapper to fit new list snuggly
	var newHeight = base.$el.find("#"+listID).height();
	$allListWrap.animate({
	height: newHeight
	});
	// Remove highlighting - Add to just-clicked tab
	base.$el.find(".nav li a").removeClass("current");
	$newList.addClass("current");
	base.$el.find(".nav li").removeClass("sel");
	$newList.parent().addClass("sel");
	});
}

else if ((listID == curList) && ( base.$el.find(":animated").length === 0)) {

// Fade out current list
	base.$el.find("#"+curList).fadeOut(base.options.speed, function() {
	var newHeight = 0;
	$allListWrap.animate({height: newHeight});
	
	base.$el.find(".nav li a").removeClass("current");
	$newList.parent().toggleClass("sel");
	//base.$el.find(".nav li a.current").toggleClass("current");
	base.$el.find(".nav a[href='#blank']").addClass("current");
});
}
//////////////////////////////////////////////////////////////////////
// Don't behave like a regular link
// Stop propegation and bubbling
return false;
});
};
base.init();
};
jQuery.organicTabs.defaultOptions = {
"speed": 300
};
jQuery.fn.organicTabs = function(options) {
return this.each(function() {
(new jQuery.organicTabs(this, options));
});
};
});