// This fxn allows all accordion toggles to be closed
// Comment out corresponding section in udesign/scripts/script.js
$(document).ready(function(){
$('.accordion-container').hide();
var rotationFwd = 90;
var rotationRev = 0;
$('.accordion-toggle').click(function(){
if( $(this).next().is(':hidden') ) {
$('.accordion-toggle').removeClass('active').next().slideUp();
$(this).toggleClass('active').next().slideDown();
//find the child div of the selected accordion toggle with class="calloutA-AccordArrow"
$('.calloutA-AccordArrow').css({
'-webkit-transform' : 'rotate('+ rotationRev +'deg)',
'-moz-transform' : 'rotate('+ rotationRev +'deg)',
'-ms-transform' : 'rotate('+ rotationRev +'deg)',
'transform' : 'rotate('+ rotationRev +'deg)'
});
$(this).find('.calloutA-AccordArrow').css({
'-webkit-transform' : 'rotate('+ rotationFwd +'deg)',
'-moz-transform' : 'rotate('+ rotationFwd +'deg)',
'-ms-transform' : 'rotate('+ rotationFwd +'deg)',
'transform' : 'rotate('+ rotationFwd +'deg)'
});
}
else {
$('.accordion-toggle').removeClass('active').next().slideUp();
$(this).find('.calloutA-AccordArrow').css({
'-webkit-transform' : 'rotate('+ rotationRev +'deg)',
'-moz-transform' : 'rotate('+ rotationRev +'deg)',
'-ms-transform' : 'rotate('+ rotationRev +'deg)',
'transform' : 'rotate('+ rotationRev +'deg)'
});
}
return false; // Prevent the browser jump to the link anchor
});
});
jQuery(document).ready(function($){
$("[name='clientProjUpload']").on('change', function(){
var uploadText = $("[name='clientProjUpload']").val();
$('#browseField').val(uploadText);
});
});
function chgBkgd(el, url){
jQuery(el).css('background-image', 'url("'+url+'")');
};
