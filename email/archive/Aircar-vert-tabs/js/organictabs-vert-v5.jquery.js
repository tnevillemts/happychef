
jQuery(document).ready(function() {
jQuery.organicTabs = function(el, options) {
var base = this;
base.$el = jQuery(el);
base.$nav = base.$el.find(".nav");
base.init = function() {
base.options = jQuery.extend({},jQuery.organicTabs.defaultOptions, options);

// Accessible hiding fix
	jQuery(".hide").css({
		"position": "relative",
		"top": 0,
		"left": 0,
		"display": "none"
	});

// Copy the contents of the tab panels to the accordion
// REMOVE IF ACCORDION PANEL CONTENT IS DIFFERENT FROM TAB PANEL CONTENT
var tabContents = base.$el.find('.tabContentWrap');
var accContents = base.$el.find('.accContentWrap');

tabContents.each(function(i){
	if(tabContents.length == accContents.length){
		var tabHtml = tabContents[i].innerHTML;
		accContents[i].innerHTML = tabHtml;
	} else {
		alert("Different number of tabs and accordion panes.");
	}
});
//

base.$nav.delegate("a", "click", function() {

// Figure out current list via CSS class
var curList = base.$el.find("a.current").attr("name").substring(1);
var curListId = base.$el.find("a.current").attr("id");

// List moving to
$newList = jQuery(this),

// Figure out ID of new list
listID = $newList.attr("name").substring(1),
newListId = $newList.attr("id");

// Set outer wrapper height to (static) height of current inner list
$allListWrap = base.$el.find(".list-wrap"),
curListHeight = $allListWrap.height();
$allListWrap.height(curListHeight);


if ((listID != curList) && ( base.$el.find(":animated").length === 0)) {

	// Fade out current list
	base.$el.find("#"+curList).fadeOut(base.options.speed, function() {
		// Fade in new list on callback
		base.$el.find("#"+listID).fadeIn(base.options.speed);
		
		// Adjust outer wrapper to fit new list snuggly
		var newHeight = base.$el.find("#"+listID).height();
		$allListWrap.animate({
		height: newHeight
		});
		// Remove highlighting - Add to just-clicked tab
		base.$el.find(".nav a").removeClass("current");
		$newList.addClass("current");
		base.$el.find(".nav li").parent().removeClass("sel");
		$newList.parent().parent().addClass("sel");
	});
}

// Don't behave like a regular link
// Stop propegation and bubbling
return false;
});
};
base.init();
};
jQuery.organicTabs.defaultOptions = {
"speed": 300
};
jQuery.fn.organicTabs = function(options) {
return this.each(function() {
(new jQuery.organicTabs(this, options));
});
};
});