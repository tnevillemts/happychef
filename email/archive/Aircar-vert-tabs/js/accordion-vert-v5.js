// This fxn allows all accordion toggles to be closed
// Comment out corresponding section in udesign/scripts/script.js
jQuery(document).ready(function(){
jQuery('.accContentWrap').hide();
var rotationFwd = 90;
var rotationRev = 0;
jQuery('.accordionToggle').click(function(){
	
var clickId = jQuery(this).attr('id');
if(jQuery('#' + clickId).hasClass('active')){
	jQuery("#" + clickId).removeClass('active');
	jQuery('#' + clickId).next().slideUp();
}
else {
	var activeIdObj = jQuery('#accord h3.active');
	var activeId = activeIdObj.attr('id');
	if(activeId){
		jQuery('#' + activeId).removeClass('active');
		jQuery("#" + clickId).addClass('active');
		jQuery('#' + activeId).next().slideUp();
		jQuery('#' + clickId).next().slideDown()
	}
	else{
		jQuery("#" + clickId).addClass('active');
		jQuery("#" + clickId).next().slideDown()
	}
}


return false; // Prevent the browser jump to the link anchor
});
});
jQuery(document).ready(function(){
jQuery("[name='clientProjUpload']").on('change', function(){
var uploadText = jQuery("[name='clientProjUpload']").val();
jQuery('#browseField').val(uploadText);
});
});
function chgBkgd(el, url){
jQuery(el).css('background-image', 'url("'+url+'")');
};
